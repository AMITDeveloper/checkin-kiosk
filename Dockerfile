FROM docker.amlab7.com/e4/wildfly_aeromexico_base:12_alpine_oraclejdk8

MAINTAINER Ivan Villa "ivilla@innomius.com"

ARG ARTIFACT_VERSION

ADD target/checkIn.war /opt/wildfly-12.0.0.Final/standalone/deployments/checkIn.war