
package com.am.checkin.web.config;

public final class Version {

	private static final String VERSION = "${project.version}";
	private static final String GROUPID = "${project.groupId}";
	private static final String ARTIFACTID = "${project.artifactId}";
	private static final String SCM = "${project.scm.developerConnection}";
	private static final String SCM_BRANCH = "${scmBranch}";
	private static final String REVISION = "${build.number}";

	public static String getVersion() {
		return VERSION;
	}

	public static String getArtifactId() {
		return ARTIFACTID;
	}

	public static String getGroupId() {
		return GROUPID;
	}

	public static String getSCM() {
		return SCM;
	}

	public static String getRevision() {
		return REVISION;
	}

	public static String getSCMBranch() {
		return SCM_BRANCH;
	}

}
