package com.am.checkin.web.filters;

import com.aeromexico.commons.web.util.MaskConfidentialDataUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author adrian
 */
@Provider
public class RequestLoggingFilter implements ContainerRequestFilter {

    private static final Logger LOG = LoggerFactory.getLogger(RequestLoggingFilter.class);
    private static final Logger LOG_PING = LoggerFactory.getLogger("PING_LOGGER");

    private static final Gson GSON = new GsonBuilder()
            //.serializeNulls()
            .excludeFieldsWithoutExposeAnnotation()
            .create();

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        if (isJson(requestContext)) {
            String json = "";
            try {
                json = IOUtils.toString(requestContext.getEntityStream(), "UTF-8");

                StringBuilder sb = new StringBuilder();
                sb.append("\n");
                sb.append("REQUEST Path: ").append(requestContext.getUriInfo().getRequestUri().getPath());
                if (null != requestContext.getUriInfo().getRequestUri().getRawQuery()) {
                    sb.append("?").append(requestContext.getUriInfo().getRequestUri().getRawQuery());
                }
                sb.append("\n");
                sb.append("Headers: ").append(requestContext.getHeaders()).append("\n");
                sb.append("Body: ").append(removeBlanks(json)).append("\n");

                if ( sb.toString().contains("REQUEST Path: /checkIn/ping") ) {
                	LOG_PING.info(sb.toString());
                } else {
                	LOG.info(sb.toString());
                }

                // replace input stream for Jersey as we've already read it
                InputStream in = IOUtils.toInputStream(json, "UTF-8");
                requestContext.setEntityStream(in);

            } catch (IOException ex) {
                try {
                    // replace input stream for Jersey as we've already read it
                    InputStream in = IOUtils.toInputStream(json, "UTF-8");
                    requestContext.setEntityStream(in);
                } catch (Exception e) {
                    //
                }

                throw new RuntimeException(ex);
            }
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("\n");
            sb.append("REQUEST Path: ").append(requestContext.getUriInfo().getRequestUri().getPath());
            if (null != requestContext.getUriInfo().getRequestUri().getRawQuery()) {
                sb.append("?").append(requestContext.getUriInfo().getRequestUri().getRawQuery());
            }
            sb.append("\n");
            sb.append("Headers: ").append(requestContext.getHeaders()).append("\n");

            if ( sb.toString().contains("REQUEST Path: /checkIn/ping") ) {
            	LOG_PING.info(sb.toString());
            } else {
            	LOG.info(sb.toString());
            }

        }

    }

    private String removeBlanks(String source) throws IOException {
        JsonObject jsonObject = GSON.fromJson(source, JsonObject.class);

        MaskConfidentialDataUtil.maskConfidentialDataOnPurchaseOrder(jsonObject);

        return GSON.toJson(jsonObject);
    }

    boolean isJson(ContainerRequestContext request) {
        // define rules when to read body
        if (null == request.getMediaType()) {
            return false;
        }

        return request.getMediaType().toString().contains("application/json");
    }
}
