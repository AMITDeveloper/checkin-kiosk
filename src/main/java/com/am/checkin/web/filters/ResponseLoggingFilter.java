package com.am.checkin.web.filters;

import java.io.IOException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adrian
 */
@Provider
public class ResponseLoggingFilter implements ContainerResponseFilter {

    private static final Logger LOG = LoggerFactory.getLogger(ResponseLoggingFilter.class);
    private static final Logger LOG_PING = LoggerFactory.getLogger("PING_LOGGER");

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
        // Use the ContainerRequestContext to extract information from the HTTP request
        // Use the ContainerResponseContext to extract information from the HTTP response                        

        try {
            StringBuilder sb = new StringBuilder();
            sb.append("\n");
            sb.append("RESPONSE Path: ").append(requestContext.getUriInfo().getRequestUri().getPath());
            if (null != requestContext.getUriInfo().getRequestUri().getRawQuery()) {
                sb.append("?").append(requestContext.getUriInfo().getRequestUri().getRawQuery());
            }
            sb.append("\n");
            sb.append("Headers: ").append(responseContext.getHeaders()).append("\n");
            sb.append("Body: ").append(responseContext.getEntity()).append("\n");

            if ( sb.toString().contains("RESPONSE Path: /checkIn/ping") ) {
            	LOG_PING.info(sb.toString());
            } else {
            	LOG.info(sb.toString());
            }

        } catch (Exception ex) {
            //
        }

    }

}
