package com.am.checkin.web.exception.handler;

import com.aeromexico.commons.exception.handler.AMExceptionHandler;
import com.aeromexico.commons.model.Incident;
import com.aeromexico.commons.web.types.PosType;
import com.am.checkin.web.v2.util.SeamlessIncidentUtil;
import com.am.seamless.checkin.common.models.exception.SeamlessException;

import javax.annotation.PostConstruct;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * @author adrian
 */
@Provider
public class SeamlessCheckinExceptionHandler extends AMExceptionHandler implements ExceptionMapper<SeamlessException> {

    private PosType posType;

    @PostConstruct
    public void init() {

        try {
            String channel = System.getProperty("amChannel");

            if (null == channel || channel.isEmpty()) {
                posType = PosType.WEB;
            } else if (channel.contains("WEB")) {
                posType = PosType.WEB;
            } else if (channel.contains("MOBILE")) {
                posType = PosType.WEB;
            } else if (channel.contains("KIOSK")) {
                posType = PosType.KIOSK;
            } else {
                posType = PosType.WEB;
            }
        } catch (Exception ex) {
            posType = PosType.WEB;
        }

    }

    @Override
    public Response toResponse(SeamlessException exception) {

        Incident incident = SeamlessIncidentUtil.getSeamlessIncident(exception, posType.getCode());

        GenericEntity<String> genericEntity = new GenericEntity<>(getGson().toJson(incident), String.class);
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(genericEntity).type(MediaType.APPLICATION_JSON).build();
    }

}
