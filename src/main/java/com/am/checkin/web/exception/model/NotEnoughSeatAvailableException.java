package com.am.checkin.web.exception.model;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.exception.model.RetryCheckinException;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.types.PosType;
import com.sabre.services.acs.bso.checkinpassenger.v3.ACSCheckInPassengerRSACS;
import javax.ws.rs.core.Response;

/**
 *
 * @author adrian
 */
public class NotEnoughSeatAvailableException extends GenericException {

    private ACSCheckInPassengerRSACS checkInPassengerRS;

    public NotEnoughSeatAvailableException(RetryCheckinException retryCheckinException) {
        super(retryCheckinException);
    }

    public NotEnoughSeatAvailableException(String extraInfo, String msg) {
        super(extraInfo, msg);
    }

    public NotEnoughSeatAvailableException(Response.Status status, ErrorType errorCodeType) {
        super(status, errorCodeType);
    }

    public NotEnoughSeatAvailableException(Response.Status status, ErrorType errorCodeType, String msg) {
        super(status, errorCodeType, msg);
    }

    public NotEnoughSeatAvailableException(Response.Status status, ErrorType errorCodeType, String msg, String recordLocator, String cartId) {
        super(status, errorCodeType, msg, recordLocator, cartId);
    }

    public NotEnoughSeatAvailableException(Response.Status status, ErrorType errorCodeType, String msg, String extraInfo) {
        super(status, errorCodeType, msg, extraInfo);
    }

    public NotEnoughSeatAvailableException(Response.Status status, ErrorType errorCodeType, PosType pos) {
        super(status, errorCodeType, pos);
    }

    public NotEnoughSeatAvailableException(Response.Status status, ErrorType errorCodeType, String msg, PosType pos) {
        super(status, errorCodeType, msg, pos);
    }

    public NotEnoughSeatAvailableException(Response.Status status, ErrorType errorCodeType, String msg, String extraInfo, PosType pos) {
        super(status, errorCodeType, msg, extraInfo, pos);
    }

    public NotEnoughSeatAvailableException(Response.Status status, ErrorType errorCodeType, String msg, String extraInfo, PosType pos, String recordLocator, String cartId) {
        super(status, errorCodeType, msg, extraInfo, pos, recordLocator, cartId);
    }

    public NotEnoughSeatAvailableException(Response.Status status, ErrorType errorCodeType, String msg, String extraInfo, String recordLocator, String cartId) {
        super(status, errorCodeType, msg, extraInfo, recordLocator, cartId);
    }

    public GenericException getGenericException() {
        if(PosType.WEB.equals(this.getPos())){
            GenericException genericException = new GenericException(
                    this.getStatus(),ErrorType.NOT_ENOUGH_SEATS_AVAILABLE_WEB, this.getMsg(), this.getExtraInfo(), this.getPos(), this.getRecordLocator(), this.getCartId()
            );
            return genericException;
        }else{
            GenericException genericException = new GenericException(
                    this.getStatus(), this.getErrorCodeType(), this.getMsg(), this.getExtraInfo(), this.getPos(), this.getRecordLocator(), this.getCartId()
            );
            return genericException;
        }
    }

    /**
     * @return the checkInPassengerRS
     */
    public ACSCheckInPassengerRSACS getCheckInPassengerRS() {
        return checkInPassengerRS;
    }

    /**
     * @param checkInPassengerRS the checkInPassengerRS to set
     */
    public void setCheckInPassengerRS(ACSCheckInPassengerRSACS checkInPassengerRS) {
        this.checkInPassengerRS = checkInPassengerRS;
    }

}
