package com.am.checkin.web.exception.handler;

import com.aeromexico.commons.exception.handler.AMExceptionHandler;
import com.aeromexico.commons.model.ExtraInfo;
import com.aeromexico.commons.model.Incident;
import com.aeromexico.commons.web.types.ActionableType;
import com.aeromexico.commons.web.util.Constants;
import com.aeromexico.sabre.api.session.exception.SabreEmptyResponseException;
import org.aeromexico.commons.exception.config.ReadProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * @author adrian
 */
@Provider
public class SabreEmptyResponseExceptionHandler extends AMExceptionHandler implements ExceptionMapper<SabreEmptyResponseException> {
    private static final Logger LOG = LoggerFactory.getLogger(SabreEmptyResponseExceptionHandler.class);
    @Inject
    private ReadProperties readProperties;

    @Override
    public Response toResponse(SabreEmptyResponseException exception) {
        LOG.error(readProperties.getMessagesError(Constants.CODE_12110302).toString(), exception.fillInStackTrace());
        Incident incident = new Incident();
        incident.setCode(readProperties.getMessagesError(Constants.CODE_12110302).getCode());
        incident.setActionable(ActionableType.valueOf(readProperties.getMessagesError(Constants.CODE_12110302).getActionable()));
        String hostname = systemPropertiesFactory.getInstance().getHostname();
        String debug = "";
        try {
            if (null != exception.getStackTrace() && exception.getStackTrace().length > 0) {
                debug = hostname + " " + exception.getStackTrace()[0].toString();
            } else {
                debug = hostname;
            }
        } catch (Exception e) {
            debug = hostname;
        }
        ExtraInfo extraInfo = new ExtraInfo(debug);
        String extra = "HTTP code: " + Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
        extraInfo.setExtra(extra);
        incident.setExtraInfo(extraInfo);
        incident.setMsg(readProperties.getMessagesError(Constants.CODE_12110302).getMessage());
        GenericEntity<String> genericEntity = new GenericEntity<>(getGson().toJson(incident), String.class);
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(genericEntity).type(MediaType.APPLICATION_JSON).build();
    }
}