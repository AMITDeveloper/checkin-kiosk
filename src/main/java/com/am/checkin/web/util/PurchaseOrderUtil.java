package com.am.checkin.web.util;

import com.aeromexico.commons.clubpremier.model.CreditCard;
import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.AbstractAncillary;
import com.aeromexico.commons.model.AbstractSegmentChoice;
import com.aeromexico.commons.model.Address;
import com.aeromexico.commons.model.Ancillary;
import com.aeromexico.commons.model.AncillaryOffer;
import com.aeromexico.commons.model.BaggageRoute;
import com.aeromexico.commons.model.BaggageRouteList;
import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedLegCollection;
import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.BookedSegmentCollection;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.BookingClass;
import com.aeromexico.commons.model.BookingClassMap;
import com.aeromexico.commons.model.CabinUpgradeAncillary;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.CartPNRCollection;
import com.aeromexico.commons.model.CheckinConfirmation;
import com.aeromexico.commons.model.CreditCardPaymentRequest;
import com.aeromexico.commons.model.CreditCardPaymentSummary;
import com.aeromexico.commons.model.CurrencyValue;
import com.aeromexico.commons.model.ExtraWeightAncillary;
import com.aeromexico.commons.model.FormsOfPaymentWrapper;
import com.aeromexico.commons.model.OnHoldWrapper;
import com.aeromexico.commons.model.PaidTravelerAncillary;
import com.aeromexico.commons.model.PaymentSummary;
import com.aeromexico.commons.model.PaymentSummaryCollection;
import com.aeromexico.commons.model.PectabBoardingPass;
import com.aeromexico.commons.model.PurchaseOrder;
import com.aeromexico.commons.model.SeatChoice;
import com.aeromexico.commons.model.SeatChoiceUpsell;
import com.aeromexico.commons.model.Segment;
import com.aeromexico.commons.model.SegmentChoice;
import com.aeromexico.commons.model.TicketedTravelerAncillary;
import com.aeromexico.commons.model.TravelerAncillary;
import com.aeromexico.commons.model.TravelerAncillaryBaseProps;
import com.aeromexico.commons.model.UatpPaymentRequest;
import com.aeromexico.commons.model.UatpPaymentSummary;
import com.aeromexico.commons.model.UpgradeAncillary;
import com.aeromexico.commons.model.Warning;
import com.aeromexico.commons.model.WarningCollection;
import com.aeromexico.commons.model.utils.CurrencyUtil;
import com.aeromexico.commons.web.types.*;
import com.aeromexico.commons.web.util.FlightNumberUtil;
import com.aeromexico.commons.web.util.ListsUtil;
import com.aeromexico.commons.web.util.MaskCreditCardUtil;
import com.aeromexico.commons.web.util.SegmentCodeUtil;
import com.aeromexico.commons.web.util.TimeUtil;
import com.aeromexico.dao.util.CommonUtil;
import com.sabre.services.acs.bso.checkinpassenger.v3.ACSCheckInPassengerRSACS;
import com.sabre.services.acs.bso.checkinpassenger.v3.ItineraryACS;
import com.sabre.services.stl.v3.ItineraryPassengerACS;
import com.sabre.services.stl.v3.PECTABDataListACS;
import com.sabre.services.stl.v3.PassengerDetailACS;
import com.sabre.services.stl.v3.TravelDocDataACS;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.opentravel.ota.payments.ProductDetailType;

/**
 * @author adrian
 */
public class PurchaseOrderUtil {

	private static final Logger LOG = LoggerFactory.getLogger(PurchaseOrderUtil.class);

    /**
     * @param segmentCode
     * @param upgradeAncillaryList
     * @return
     */
    public static UpgradeAncillary getUpgradeAncillary(String segmentCode, List<UpgradeAncillary> upgradeAncillaryList) {
        if (null == upgradeAncillaryList || upgradeAncillaryList.isEmpty() || null == segmentCode || segmentCode.trim().isEmpty()) {
            return null;
        }

        for (UpgradeAncillary upgradeAncillary : upgradeAncillaryList) {
            if (SegmentCodeUtil.compareSegmentCodeWithoutTime(segmentCode, upgradeAncillary.getSegmentCode())) {
                return upgradeAncillary;
            }
        }

        return null;
    }

    /**
     * @param bookedTraveler
     * @param bookedSegmentCollection
     * @return
     */
    public static Map<String, Segment> getSegmentIdsWithUpgrade(BookedTraveler bookedTraveler, BookedSegmentCollection bookedSegmentCollection) {
        Map<String, Segment> segmentIds = new HashMap<>();

        if (null != bookedTraveler && null != bookedTraveler.getAncillaries()) {
            List<CabinUpgradeAncillary> cabinUpgradeAncillaryList = getUpgradeUnpaidAncillaries(bookedTraveler.getAncillaries().getCollection());

            for (CabinUpgradeAncillary cabinUpgradeAncillary : cabinUpgradeAncillaryList) {
                if (null != cabinUpgradeAncillary.getSegmentCode()
                        && !segmentIds.containsKey(
                        SegmentCodeUtil.removeTimeFromSegmentCode(cabinUpgradeAncillary.getSegmentCode())
                )) {

                    BookedSegment bookedSegment = bookedSegmentCollection.getBookedSegment(cabinUpgradeAncillary.getSegmentCode());

                    if (null != bookedSegment && null != bookedSegment.getSegment()) {
                        try {
                            segmentIds.put(SegmentCodeUtil.removeTimeFromSegmentCode(
                                    cabinUpgradeAncillary.getSegmentCode()), bookedSegment.getSegment()
                            );
                        } catch (Exception ex) {
                            //
                        }
                    }
                }
            }
        }

        return segmentIds;
    }

    /**
     * @param bookedTraveler
     * @return
     */
    public static List<AbstractAncillary> getAllAncillaries(BookedTraveler bookedTraveler) {
        List<AbstractAncillary> abstractAncillaryList = new ArrayList<>();

        if (null != bookedTraveler.getAncillaries()) {
            abstractAncillaryList.addAll(bookedTraveler.getAncillaries().getCollection());
        }

        if (null != bookedTraveler.getSeatAncillaries()) {
            abstractAncillaryList.addAll(bookedTraveler.getSeatAncillaries().getCollection());
        }

        return abstractAncillaryList;
    }

    /**
     * @param travelerAncillaryList
     * @return
     */
    public static List<TravelerAncillary> getNotBenefitAncillaries(List<TravelerAncillary> travelerAncillaryList) {
        List<TravelerAncillary> notBenefitTravelerAncillaryList = new ArrayList<>();

        for (TravelerAncillary travelerAncillary : travelerAncillaryList) {  
            if (!travelerAncillary.isApplyBenefit()) {
                notBenefitTravelerAncillaryList.add(travelerAncillary);
            }
        }

        return notBenefitTravelerAncillaryList;
    }

    /**
     * @param travelerAncillaryList
     * @return
     */
    public static List<TravelerAncillary> getBenefitAncillaries(List<TravelerAncillary> travelerAncillaryList) {
        List<TravelerAncillary> benefitTravelerAncillaryList = new ArrayList<>();

        for (TravelerAncillary travelerAncillary : travelerAncillaryList) {
            if (travelerAncillary.isApplyBenefit()) {
                benefitTravelerAncillaryList.add(travelerAncillary);
            }
        }

        return benefitTravelerAncillaryList;
    }

    /**
     * @param travelerAncillaryList
     * @return
     */
    public static List<TravelerAncillary> getNonZeroCostAncillaries(List<TravelerAncillary> travelerAncillaryList) {
        List<TravelerAncillary> nonZeroCostTravelerAncillaryList = new ArrayList<>();

        for (TravelerAncillary travelerAncillary : travelerAncillaryList) {
            if (1 == travelerAncillary.getAncillary().getCurrency().getTotal().compareTo(BigDecimal.ZERO)) {
                nonZeroCostTravelerAncillaryList.add(travelerAncillary);
            }
        }

        return nonZeroCostTravelerAncillaryList;
    }

    /**
     * @param travelerAncillaryList
     * @return
     */
    public static List<TravelerAncillary> getZeroCostAncillaries(List<TravelerAncillary> travelerAncillaryList) {
        List<TravelerAncillary> zeroCostTravelerAncillaryList = new ArrayList<>();

        for (TravelerAncillary travelerAncillary : travelerAncillaryList) {
            if (0 == travelerAncillary.getAncillary().getCurrency().getTotal().compareTo(BigDecimal.ZERO)) {
                zeroCostTravelerAncillaryList.add(travelerAncillary);
            }
        }

        return zeroCostTravelerAncillaryList;
    }

    /**
     * @param abstractAncillaryList
     * @return
     */
    public static List<ExtraWeightAncillary> getExtraWeightUnpaidAncillaries(List<AbstractAncillary> abstractAncillaryList) {
        List<ExtraWeightAncillary> extraWeightAncillaryList = new ArrayList<>();
        if (null != abstractAncillaryList) {
            for (AbstractAncillary abstractAncillary : abstractAncillaryList) {
                if (abstractAncillary instanceof ExtraWeightAncillary) {

                    ExtraWeightAncillary extraWeightAncillary = (ExtraWeightAncillary) abstractAncillary;

                    LOG.info( "getExtraWeightUnpaidAncillaries: {}", extraWeightAncillary);

                    if ( ! extraWeightAncillary.isPartOfReservation()
                            && ActionCodeType.HD.toString().equalsIgnoreCase(extraWeightAncillary.getActionCode())) {

                    	LOG.info( "getExtraWeightUnpaidAncillaries: Is not part of reservation: {}", extraWeightAncillary);

                    	extraWeightAncillaryList.add(extraWeightAncillary);
                    }
                }
            }
        }
        return extraWeightAncillaryList;
    }

    /**
     * @param abstractAncillaryList
     * @return
     */
    public static List<CabinUpgradeAncillary> getUpgradeUnpaidAncillaries(List<AbstractAncillary> abstractAncillaryList) {
        List<CabinUpgradeAncillary> cabinUpgradeAncillaryList = new ArrayList<>();
        if (null != abstractAncillaryList) {
            for (AbstractAncillary abstractAncillary : abstractAncillaryList) {
                if (abstractAncillary instanceof CabinUpgradeAncillary) {

                    CabinUpgradeAncillary cabinUpgradeAncillary = (CabinUpgradeAncillary) abstractAncillary;

                    if (!cabinUpgradeAncillary.isPartOfReservation()
                            && ActionCodeType.HD.toString().equalsIgnoreCase(cabinUpgradeAncillary.getActionCode())) {

                        cabinUpgradeAncillaryList.add(cabinUpgradeAncillary);
                    }
                }
            }
        }
        return cabinUpgradeAncillaryList;
    }

    /**
     * @param abstractAncillaryList
     * @return
     */
    public static List<ExtraWeightAncillary> getExtraWeightUnpaidAncillariesOnReservation(List<AbstractAncillary> abstractAncillaryList) {
        List<ExtraWeightAncillary> extraWeightAncillaryList = new ArrayList<>();
        if (null != abstractAncillaryList) {
            for (AbstractAncillary abstractAncillary : abstractAncillaryList) {
                if (abstractAncillary instanceof ExtraWeightAncillary) {

                    ExtraWeightAncillary extraWeightAncillary = (ExtraWeightAncillary) abstractAncillary;

                    if (extraWeightAncillary.isPartOfReservation()
                            && ActionCodeType.HD.toString().equalsIgnoreCase(extraWeightAncillary.getActionCode())) {
                        extraWeightAncillaryList.add(extraWeightAncillary);
                    }
                }
            }
        }
        return extraWeightAncillaryList;
    }

    /**
     * @param abstractAncillaryList
     * @return
     */
    public static List<CabinUpgradeAncillary> getUpgradeUnpaidAncillariesOnReservation(List<AbstractAncillary> abstractAncillaryList) {
        List<CabinUpgradeAncillary> cabinUpgradeAncillaryList = new ArrayList<>();
        if (null != abstractAncillaryList) {
            for (AbstractAncillary abstractAncillary : abstractAncillaryList) {
                if (abstractAncillary instanceof CabinUpgradeAncillary) {

                    CabinUpgradeAncillary cabinUpgradeAncillary = (CabinUpgradeAncillary) abstractAncillary;

                    if (cabinUpgradeAncillary.isPartOfReservation()
                            && ActionCodeType.HD.toString().equalsIgnoreCase(cabinUpgradeAncillary.getActionCode())) {
                        cabinUpgradeAncillaryList.add(cabinUpgradeAncillary);
                    }
                }
            }
        }
        return cabinUpgradeAncillaryList;
    }

    /**
     * @param abstractAncillaryList
     * @return
     */
    public static List<ExtraWeightAncillary> getExtraWeightPaidAncillaries(List<AbstractAncillary> abstractAncillaryList) {
        List<ExtraWeightAncillary> extraWeightAncillaryList = new ArrayList<>();
        if (null != abstractAncillaryList) {
            for (AbstractAncillary abstractAncillary : abstractAncillaryList) {
                if (abstractAncillary instanceof ExtraWeightAncillary) {

                    ExtraWeightAncillary extraWeightAncillary = (ExtraWeightAncillary) abstractAncillary;

                    if (ActionCodeType.HI.toString().equalsIgnoreCase(extraWeightAncillary.getActionCode())
                            || ActionCodeType.HK.toString().equalsIgnoreCase(extraWeightAncillary.getActionCode())) {
                        extraWeightAncillaryList.add(extraWeightAncillary);
                    }
                }
            }
        }
        return extraWeightAncillaryList;
    }

    /**
     * @param abstractAncillaryList
     * @return
     */
    public static List<CabinUpgradeAncillary> getUpgradePaidAncillaries(List<AbstractAncillary> abstractAncillaryList) {
        List<CabinUpgradeAncillary> cabinUpgradeAncillaryList = new ArrayList<>();
        if (null != abstractAncillaryList) {
            for (AbstractAncillary abstractAncillary : abstractAncillaryList) {
                if (abstractAncillary instanceof CabinUpgradeAncillary) {

                    CabinUpgradeAncillary cabinUpgradeAncillary = (CabinUpgradeAncillary) abstractAncillary;

                    if (ActionCodeType.HI.toString().equalsIgnoreCase(cabinUpgradeAncillary.getActionCode())
                            || ActionCodeType.HK.toString().equalsIgnoreCase(cabinUpgradeAncillary.getActionCode())) {
                        cabinUpgradeAncillaryList.add(cabinUpgradeAncillary);
                    }
                }
            }
        }
        return cabinUpgradeAncillaryList;
    }

    /**
     * @param abstractAncillaryList
     * @return
     */
    public static List<TravelerAncillary> getNewAncillaries(List<AbstractAncillary> abstractAncillaryList) {
        List<TravelerAncillary> travelerAncillaryList = new ArrayList<>();
        if (null != abstractAncillaryList) {
            for (AbstractAncillary abstractAncillary : abstractAncillaryList) {
                if (abstractAncillary instanceof TravelerAncillary) {

                    TravelerAncillary travelerAncillary = (TravelerAncillary) abstractAncillary;

                    if (travelerAncillary.isApplyBenefit()
                            && ActionCodeType.HK.toString().equalsIgnoreCase(travelerAncillary.getActionCode())) {
                        travelerAncillaryList.add((TravelerAncillary) abstractAncillary);
                    } else if (!travelerAncillary.isPartOfReservation()
                            && ActionCodeType.HD.toString().equalsIgnoreCase(travelerAncillary.getActionCode())) {
                        travelerAncillaryList.add((TravelerAncillary) abstractAncillary);
                    }
                }
            }
        }
        return travelerAncillaryList;
    }

    /**
     * @param abstractAncillaryList
     * @return
     */
    public static List<TravelerAncillary> getFreeAncillaries(List<AbstractAncillary> abstractAncillaryList) {
        List<TravelerAncillary> travelerAncillaryList = new ArrayList<>();
        if (null != abstractAncillaryList) {
            for (AbstractAncillary abstractAncillary : abstractAncillaryList) {
                if (abstractAncillary instanceof TravelerAncillary) {

                    TravelerAncillary travelerAncillary = (TravelerAncillary) abstractAncillary;

                    if (!travelerAncillary.isPartOfReservation()
                            && (ActionCodeType.HI.toString().equalsIgnoreCase(travelerAncillary.getActionCode())
                            || ActionCodeType.HK.toString().equalsIgnoreCase(travelerAncillary.getActionCode()))) {
                        travelerAncillaryList.add((TravelerAncillary) abstractAncillary);
                    }
                }
            }
        }
        return travelerAncillaryList;
    }

    /**
     * @param abstractAncillaryList
     * @return
     */
    public static List<TravelerAncillary> getUnpaidAncillariesOnReservation(List<AbstractAncillary> abstractAncillaryList) {
        List<TravelerAncillary> travelerAncillaryList = new ArrayList<>();
        if (null != abstractAncillaryList) {
            for (AbstractAncillary abstractAncillary : abstractAncillaryList) {
                if (abstractAncillary instanceof TravelerAncillary) {
                    TravelerAncillary travelerAncillary = (TravelerAncillary) abstractAncillary;
                    if (travelerAncillary.isPartOfReservation()
                            && ActionCodeType.HD.toString().equalsIgnoreCase(travelerAncillary.getActionCode())) {
                        travelerAncillaryList.add((TravelerAncillary) abstractAncillary);
                    }
                }
            }
        }
        return travelerAncillaryList;
    }

    /**
     * @param travelerAncillaryList
     * @return
     */
    public static List<TravelerAncillary> getUnpaidAncillariesWithCost(final List<TravelerAncillary> travelerAncillaryList) {
        List<TravelerAncillary> travelerAncillaryListWithCost = new ArrayList<>();
        if (null != travelerAncillaryList) {
            for (TravelerAncillary travelerAncillary : travelerAncillaryList) {
                try {
                    if (1 == travelerAncillary.getAncillary().getCurrency().getTotal().compareTo(BigDecimal.ZERO)
                            && ActionCodeType.HD.toString().equalsIgnoreCase(travelerAncillary.getActionCode()) //&& ("SA".equalsIgnoreCase(travelerAncillary.getAncillary().getGroupCode())
                        //|| "BG".equalsIgnoreCase(travelerAncillary.getAncillary().getGroupCode()))
                    ) {

                        travelerAncillaryListWithCost.add(travelerAncillary);
                    }
                } catch (Exception ex) {
                    //
                }
            }
        }
        return travelerAncillaryListWithCost;
    }

    /**
     * @param abstractAncillaryList
     * @return
     */
    public static List<TravelerAncillary> getUnpaidAncillaries(List<AbstractAncillary> abstractAncillaryList) {
        List<TravelerAncillary> travelerAncillaryList = new ArrayList<>();
        if (null != abstractAncillaryList) {
            for (AbstractAncillary abstractAncillary : abstractAncillaryList) {
                try {
                    if (abstractAncillary instanceof TravelerAncillary) {

                        TravelerAncillary travelerAncillary = (TravelerAncillary) abstractAncillary;

                        if (1 == travelerAncillary.getAncillary().getCurrency().getTotal().compareTo(BigDecimal.ZERO)
                                && ActionCodeType.HD.toString().equalsIgnoreCase(travelerAncillary.getActionCode()) //&& ("SA".equalsIgnoreCase(travelerAncillary.getAncillary().getGroupCode())
                            //|| "BG".equalsIgnoreCase(travelerAncillary.getAncillary().getGroupCode()))
                        ) {

                            travelerAncillaryList.add((TravelerAncillary) abstractAncillary);
                        }
                    }
                } catch (Exception ex) {
                    //
                }
            }
        }
        return travelerAncillaryList;
    }

    /**
     * @param abstractAncillaryList
     * @return
     */
    public static List<TravelerAncillary> getWaiveAncillaries(List<AbstractAncillary> abstractAncillaryList) {
        List<TravelerAncillary> travelerAncillaryList = new ArrayList<>();
        if (null != abstractAncillaryList) {
            for (AbstractAncillary abstractAncillary : abstractAncillaryList) {
                try {
                    if (abstractAncillary instanceof TravelerAncillary) {

                        TravelerAncillary travelerAncillary = (TravelerAncillary) abstractAncillary;

                        if (travelerAncillary.isApplyBenefit()) {
                            travelerAncillaryList.add((TravelerAncillary) abstractAncillary);
                        }
                        Ancillary firstBagAncillary = travelerAncillary.getAncillary();
                        if(null != firstBagAncillary && firstBagAncillary.getType().equalsIgnoreCase("0IA") 
                            && firstBagAncillary.getCurrency().getTotal().equals(BigDecimal.ZERO)){
                            travelerAncillaryList.add((TravelerAncillary) abstractAncillary);
                        }
                    }
                } catch (Exception ex) {
                    //
                }
            }
        }
        return travelerAncillaryList;
    }

    /**
     * @param abstractSegmentChoiceList
     * @return
     */
    public static List<SegmentChoice> getUnpaidSeats(List<AbstractSegmentChoice> abstractSegmentChoiceList) {
        List<SegmentChoice> segmentChoiceList = new ArrayList<>();
        if (null != abstractSegmentChoiceList) {
            for (AbstractSegmentChoice abstractSegmentChoice : abstractSegmentChoiceList) {
                if (abstractSegmentChoice instanceof SegmentChoice && !((SegmentChoice) abstractSegmentChoice).isPartOfReservation()) {
                    segmentChoiceList.add((SegmentChoice) abstractSegmentChoice);
                }
            }
        }
        return segmentChoiceList;
    }

    /**
     * @param abstractAncillaryList
     * @return
     */
    public static boolean thereAreQtyZeroAncillaries(List<AbstractAncillary> abstractAncillaryList) {
        List<TravelerAncillary> travelerAncillaryList = getUnpaidAncillariesOnReservation(abstractAncillaryList);
        for (TravelerAncillary travelerAncillary : travelerAncillaryList) {
            if (travelerAncillary.getQuantity() <= 0) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param abstractSegmentChoiceList
     * @return
     */
    public static boolean thereAreUnreservedSeats(List<AbstractSegmentChoice> abstractSegmentChoiceList) {
        return null == abstractSegmentChoiceList || abstractSegmentChoiceList.isEmpty();
    }

    /**
     * @param segment
     * @param bookingClassMap
     * @return
     */
    public static BookingClass getBookingClassBySegment(Segment segment, BookingClassMap bookingClassMap) {
        if (null == bookingClassMap || null == bookingClassMap.getCollection() || bookingClassMap.getCollection().isEmpty()) {
            return null;
        }

        try {
            String date;
            date = CommonUtil.getOnlyDatePart(segment.getDepartureDateTime());

            for (BookingClass bookingClass : bookingClassMap.getCollection()) {

                Integer flightNumberBG = Integer.valueOf(bookingClass.getFlight());
                Integer flightNumber = Integer.valueOf(segment.getOperatingFlightCode());

                if (0 == flightNumberBG.compareTo(flightNumber)
                        && SegmentCodeUtil.compareSegmentCodeWithoutTime(segment.getSegmentCode(), bookingClass.getSegmentCode())) {

                    return bookingClass;
                }
            }

            return null;
        } catch (NumberFormatException ex) {
            return null;
        }
    }

    /**
     * @param segment
     * @param baggageRouteList
     * @return
     */
    public static BaggageRoute getBaggageRouteBySegment(Segment segment, BaggageRouteList baggageRouteList) {
        if (null == baggageRouteList || baggageRouteList.isEmpty()) {
            return null;
        }

        try {
            String date;
            date = CommonUtil.getOnlyDatePart(segment.getDepartureDateTime());

            for (BaggageRoute baggageRoute : baggageRouteList) {

                Integer flightNumberBG = Integer.valueOf(baggageRoute.getFlight());
                Integer flightNumber = Integer.valueOf(segment.getOperatingFlightCode());
                Integer flightNumberMK = Integer.valueOf(segment.getMarketingFlightCode());

                if ((baggageRoute.getAirline().equalsIgnoreCase(segment.getOperatingCarrier())
                        || baggageRoute.getAirline().equalsIgnoreCase(segment.getMarketingCarrier()))
                        && baggageRoute.getOrigin().equalsIgnoreCase(segment.getDepartureAirport())
                        && (0 == flightNumberBG.compareTo(flightNumber)
                        || 0 == flightNumberBG.compareTo(flightNumberMK))
                        && baggageRoute.getDepartureDate().equalsIgnoreCase(date)) {

                    return baggageRoute;
                }
            }

            return null;
        } catch (NumberFormatException ex) {
            return null;
        }
    }

    /**
     * @param onHoldWrapper
     * @return
     */
    public static ProductDetailType getProductsDetails(OnHoldWrapper onHoldWrapper) {
        if (null == onHoldWrapper || null == onHoldWrapper.getTotal()) {
            return null;
        }

        ProductDetailType productDetailType = new ProductDetailType();

        productDetailType.setCurrencyCode(onHoldWrapper.getTotal().getCurrency().getCurrencyCode());
        productDetailType.setUnitPrice(onHoldWrapper.getTotal().getCurrency().getTotal());
        productDetailType.setTaxes(onHoldWrapper.getTotal().getCurrency().getTotalTax());
        productDetailType.setQuantity(1);
        productDetailType.setProductID(SabreProductType._0001.getCode());

        return productDetailType;
    }

    /**
     * @param bookedTravelerList
     * @param abstractAncillaryList
     * @param toPaidItemsType
     * @return
     */
    public static List<ProductDetailType> getProductsDetails(
            List<BookedTraveler> bookedTravelerList,
            List<AbstractAncillary> abstractAncillaryList,
            ItemsToBePaidType toPaidItemsType
    ) {
        List<ProductDetailType> productsDetail = new ArrayList<>();

        if (null == toPaidItemsType) {
            return productsDetail;
        }

        for (BookedTraveler bookedTraveler : bookedTravelerList) {

            switch (toPaidItemsType) {
                case UPGRADE:
                    for (CabinUpgradeAncillary cabinUpgradeAncillary : getUpgradeUnpaidAncillaries(bookedTraveler.getAncillaries().getCollection())) {

                        abstractAncillaryList.add(cabinUpgradeAncillary);

                        ProductDetailType productDetailType = new ProductDetailType();
                        productDetailType.setCurrencyCode(cabinUpgradeAncillary.getCurrency().getCurrencyCode());
                        productDetailType.setUnitPrice(cabinUpgradeAncillary.getCurrency().getTotal());
                        productDetailType.setTaxes(cabinUpgradeAncillary.getCurrency().getTotalTax());
                        productDetailType.setQuantity(cabinUpgradeAncillary.getQuantity());
                        productDetailType.setProductID(SabreProductType._1004.getCode());
                        productsDetail.add(productDetailType);
                    }
                    break;
                case EXTRAWEIGHT:
                    for (ExtraWeightAncillary extraWeightAncillary : getExtraWeightUnpaidAncillaries(bookedTraveler.getAncillaries().getCollection())) {

                        abstractAncillaryList.add(extraWeightAncillary);

                        ProductDetailType productDetailType = new ProductDetailType();
                        productDetailType.setCurrencyCode(extraWeightAncillary.getAncillary().getCurrency().getCurrencyCode());
                        productDetailType.setUnitPrice(extraWeightAncillary.getAncillary().getCurrency().getTotal());
                        productDetailType.setTaxes(extraWeightAncillary.getAncillary().getCurrency().getTotalTax());
                        productDetailType.setQuantity(extraWeightAncillary.getQuantity());
                        productDetailType.setProductID(SabreProductType._1004.getCode());
                        productsDetail.add(productDetailType);
                    }
                    break;
                case BAGGAGE:
                    for (TravelerAncillary travelerAncillary : getUnpaidAncillaries(bookedTraveler.getAncillaries().getCollection())) {

                        abstractAncillaryList.add(travelerAncillary);

                        ProductDetailType productDetailType = new ProductDetailType();
                        productDetailType.setCurrencyCode(travelerAncillary.getAncillary().getCurrency().getCurrencyCode());
                        productDetailType.setUnitPrice(travelerAncillary.getAncillary().getCurrency().getTotal());
                        productDetailType.setTaxes(travelerAncillary.getAncillary().getCurrency().getTotalTax());
                        productDetailType.setQuantity(travelerAncillary.getQuantity());
                        productDetailType.setProductID(SabreProductType._1004.getCode());
                        productsDetail.add(productDetailType);
                    }
                    break;
                case SEAT:
                    for (TravelerAncillary travelerAncillary : getUnpaidAncillaries(bookedTraveler.getSeatAncillaries().getCollection())) {

                        abstractAncillaryList.add(travelerAncillary);

                        ProductDetailType productDetailType = new ProductDetailType();
                        productDetailType.setCurrencyCode(travelerAncillary.getAncillary().getCurrency().getCurrencyCode());
                        productDetailType.setUnitPrice(travelerAncillary.getAncillary().getCurrency().getTotal());
                        productDetailType.setTaxes(travelerAncillary.getAncillary().getCurrency().getTotalTax());
                        productDetailType.setQuantity(travelerAncillary.getQuantity());
                        productDetailType.setProductID(SabreProductType._1004.getCode());
                        productsDetail.add(productDetailType);
                    }
                    break;
                case SEAT_AND_BAGGAGE:
                    List<AbstractAncillary> allAncillaries = getAllAncillaries(bookedTraveler);
                    for (TravelerAncillary travelerAncillary : getUnpaidAncillaries(allAncillaries)) {

                        abstractAncillaryList.add(travelerAncillary);

                        ProductDetailType productDetailType = new ProductDetailType();
                        productDetailType.setCurrencyCode(travelerAncillary.getAncillary().getCurrency().getCurrencyCode());
                        productDetailType.setUnitPrice(travelerAncillary.getAncillary().getCurrency().getTotal());
                        productDetailType.setTaxes(travelerAncillary.getAncillary().getCurrency().getTotalTax());
                        productDetailType.setQuantity(travelerAncillary.getQuantity());
                        productDetailType.setProductID(SabreProductType._1004.getCode());
                        productsDetail.add(productDetailType);
                    }
                    break;
                default:
                    break;
            }
        }

        return productsDetail;
    }

    /**
     * @param linkedId
     * @param travelerAncillaryList
     * @return
     */
    public static boolean notExistsAncillaryEntry(String linkedId, List<TravelerAncillary> travelerAncillaryList) {
        for (TravelerAncillary travelerAncillary : travelerAncillaryList) {
            if (null != linkedId && linkedId.equalsIgnoreCase(travelerAncillary.getLinkedID())) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param segmentCode
     * @param travelerAncillaryList
     * @return
     */
    public static boolean existAncillaryEntryBySegmentCode(String segmentCode, List<AbstractAncillary> travelerAncillaryList) {
        for (AbstractAncillary abstractAncillary : travelerAncillaryList) {
            if (abstractAncillary instanceof TravelerAncillaryBaseProps) {
                TravelerAncillaryBaseProps travelerAncillaryBaseProps = (TravelerAncillaryBaseProps) abstractAncillary;

                if (null != segmentCode && SegmentCodeUtil.compareSegmentCodeWithoutTime(segmentCode, travelerAncillaryBaseProps.getSegmentCodeAux())) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param segmentCode
     * @param abstractSegmentChoiceList
     * @return
     */
    public static boolean existSeatEntryBySegmentCode(String segmentCode, List<AbstractSegmentChoice> abstractSegmentChoiceList) {
        for (AbstractSegmentChoice abstractSegmentChoice : abstractSegmentChoiceList) {
            if (null != segmentCode && SegmentCodeUtil.compareSegmentCodeWithoutTime(segmentCode, abstractSegmentChoice.getSegmentCode())) {
                return true;
            }

        }

        return false;
    }

    /**
     * @param segmentCode
     * @param abstractSegmentChoiceList
     * @return
     */
    public static AbstractSegmentChoice getSeatEntryBySegmentCode(String segmentCode, List<AbstractSegmentChoice> abstractSegmentChoiceList) {
        for (AbstractSegmentChoice abstractSegmentChoice : abstractSegmentChoiceList) {
            if (null != segmentCode && SegmentCodeUtil.compareSegmentCodeWithoutTime(segmentCode, abstractSegmentChoice.getSegmentCode())) {
                return abstractSegmentChoice;
            }

        }

        return null;
    }

    /**
     * @param legCode
     * @return
     */
    public static String getAirlineCodeFromLegcode(String legCode) {
        try {

            String[] parts = legCode.split("_");
            return parts[1];

        } catch (Exception ex) {
            throw ex;
        }
    }

    /**
     * @param legCode
     * @return
     */
    public static String getFlightNumberFromLegcode(String legCode) {
        try {

            String[] parts = legCode.split("_");
            return parts[2];

        } catch (Exception ex) {
            throw ex;
        }
    }

    /**
     * @param bookedSegmentList
     * @param segmentCode
     * @return
     */
    public static BookedSegment getBookedSegment(List<BookedSegment> bookedSegmentList, String segmentCode) {
        if (null == bookedSegmentList || bookedSegmentList.isEmpty() || null == segmentCode || segmentCode.isEmpty()) {
            return null;
        }

        for (BookedSegment bookedSegment : bookedSegmentList) {
            try {
                if (SegmentCodeUtil.compareSegmentCodeWithoutTime(segmentCode, bookedSegment.getSegment().getSegmentCode())) {
                    return bookedSegment;
                }
            } catch (Exception ex) {
                //
            }
        }

        return null;
    }

    /**
     * @param bookedLeg
     * @return
     */
    public static List<ItineraryACS> getItineraryListWithoutClassFromBookedLeg(BookedLeg bookedLeg) {
        List<ItineraryACS> itineraryACSList = new ArrayList<>();

        BookedSegment firstSegment = ListsUtil.getFirst(bookedLeg.getSegments().getCollection());

        ItineraryACS itineraryACS = new ItineraryACS();
        itineraryACSList.add(itineraryACS);

        String flight = firstSegment.getSegment().getOperatingFlightCode();

        itineraryACS.setOrigin(firstSegment.getSegment().getDepartureAirport());
        itineraryACS.setDestination(firstSegment.getSegment().getArrivalAirport());
        itineraryACS.setAirline(firstSegment.getSegment().getOperatingCarrier());
        itineraryACS.setFlight(flight);
        itineraryACS.setDepartureDate(CommonUtil.getOnlyDatePart(firstSegment.getSegment().getDepartureDateTime()));
        //itineraryACS.setBookingClass(bookingClass);

        for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {
            if (FlightNumberUtil.compareFlightNumbers(flight, bookedSegment.getSegment().getOperatingFlightCode())) {
                itineraryACS.setDestination(bookedSegment.getSegment().getArrivalAirport());
            } else {
                itineraryACS = new ItineraryACS();
                itineraryACSList.add(itineraryACS);

                flight = bookedSegment.getSegment().getOperatingFlightCode();

                itineraryACS.setOrigin(bookedSegment.getSegment().getDepartureAirport());
                itineraryACS.setDestination(bookedSegment.getSegment().getArrivalAirport());
                itineraryACS.setAirline(bookedSegment.getSegment().getOperatingCarrier());
                itineraryACS.setFlight(flight);
                itineraryACS.setDepartureDate(CommonUtil.getOnlyDatePart(bookedSegment.getSegment().getDepartureDateTime()));
                //itineraryACS.setBookingClass(bookingClass);
            }
        }

        return itineraryACSList;
    }

    /**
     * @param bookedLeg
     * @param flight
     * @return
     */
    public static BookedSegment getBookedSegmentByFlightNumber(BookedLeg bookedLeg, String flight) {

        for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {
            if (FlightNumberUtil.compareFlightNumbers(flight, bookedSegment.getSegment().getOperatingFlightCode())) {
                return bookedSegment;
            }
        }

        return null;
    }

    /**
     * @param bookedSegmentListWithSameFlight
     * @param bookingClass
     * @return
     */
    public static ItineraryACS getItineraryListFromSameSegments(
            List<BookedSegment> bookedSegmentListWithSameFlight,
            String bookingClass
    ) {

        BookedSegment firstSegment = ListsUtil.getFirst(bookedSegmentListWithSameFlight);
        BookedSegment lastSegment = ListsUtil.getLast(bookedSegmentListWithSameFlight);

        ItineraryACS itineraryACS = new ItineraryACS();

        String flight = firstSegment.getSegment().getOperatingFlightCode();

        itineraryACS.setOrigin(firstSegment.getSegment().getDepartureAirport());
        itineraryACS.setDestination(lastSegment.getSegment().getArrivalAirport());
        itineraryACS.setAirline(firstSegment.getSegment().getOperatingCarrier());
        itineraryACS.setFlight(flight);
        itineraryACS.setDepartureDate(CommonUtil.getOnlyDatePart(firstSegment.getSegment().getDepartureDateTime()));
        itineraryACS.setBookingClass(bookingClass);

        return itineraryACS;

    }

    /**
     * @param bookedLeg
     * @param bookingClass
     * @return
     */
    public static ItineraryACS getItineraryFromBookedLegFirstSegment(BookedLeg bookedLeg, String bookingClass) {

        try {

            BookedSegment firstSegment = ListsUtil.getFirst(bookedLeg.getSegments().getCollection());

            ItineraryACS itineraryACS = new ItineraryACS();
            itineraryACS.setOrigin(firstSegment.getSegment().getDepartureAirport());
            //itineraryACS.setDestination(firstSegment.getItinerary().getArrivalAirport());
            itineraryACS.setAirline(firstSegment.getSegment().getOperatingCarrier());
            itineraryACS.setFlight(firstSegment.getSegment().getOperatingFlightCode());
            itineraryACS.setDepartureDate(CommonUtil.getOnlyDatePart(firstSegment.getSegment().getDepartureDateTime()));
            itineraryACS.setBookingClass(bookingClass);

            return itineraryACS;

        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * @param bookedLeg
     * @param bookingClass
     * @param airlineCodeType
     * @return
     */
    public static ItineraryACS getItineraryFromBookedLegFirstSegmentOperatedBy(
            BookedLeg bookedLeg, String bookingClass, AirlineCodeType airlineCodeType
    ) {
        if (null == airlineCodeType) {
            return null;
        }

        try {
            BookedSegment firstSegment = bookedLeg.getSegments().getFirstOpenSegmentForCheckinOperatedBy(airlineCodeType);

            if (null != firstSegment) {
                ItineraryACS itineraryACS = new ItineraryACS();
                itineraryACS.setOrigin(firstSegment.getSegment().getDepartureAirport());
                //itineraryACS.setDestination(firstSegment.getItinerary().getDepartureAirport());
                itineraryACS.setAirline(firstSegment.getSegment().getOperatingCarrier());
                itineraryACS.setFlight(firstSegment.getSegment().getOperatingFlightCode());
                itineraryACS.setDepartureDate(CommonUtil.getOnlyDatePart(firstSegment.getSegment().getDepartureDateTime()));
                itineraryACS.setBookingClass(bookingClass);

                return itineraryACS;
            }
        } catch (Exception ex) {
        }

        return null;
    }

    /**
     * @param bookedSegments
     * @return
     */
    public static BookedSegment getFirstOpenSegment(List<BookedSegment> bookedSegments) {
        if (null == bookedSegments || bookedSegments.isEmpty()) {
            return null;
        }

        try {
            for (BookedSegment bookedSegment : bookedSegments) {
                if (BookedLegCheckinStatusType.OPEN.equals(bookedSegment.getSegment().getCheckinStatus())
                        && (BookedLegFlightStatusType.ON_TIME.equals(bookedSegment.getSegment().getFlightStatus())
                        || BookedLegFlightStatusType.DELAYED.equals(bookedSegment.getSegment().getFlightStatus()))) {
                    return bookedSegment;
                }
            }
        } catch (Exception ex) {
        }

        return null;
    }

    /**
     * @param bookedTravelerList
     * @return
     * @throws GenericException
     */
    public static boolean areThereUpgradeUnpaidItems(List<BookedTraveler> bookedTravelerList) throws GenericException {
        if (null != bookedTravelerList) {
            for (BookedTraveler bookedTraveler : bookedTravelerList) {
                if (null != bookedTraveler.getAncillaries()) {

                    List<CabinUpgradeAncillary> cabinUpgradeAncillaryList;
                    cabinUpgradeAncillaryList = getUpgradeUnpaidAncillaries(bookedTraveler.getAncillaries().getCollection());

                    if (!cabinUpgradeAncillaryList.isEmpty()) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * @param bookedTravelerList
     * @return
     * @throws GenericException
     */
    public static boolean areThereExtraWeightUnpaidItems(List<BookedTraveler> bookedTravelerList) throws GenericException {
        if (null != bookedTravelerList) {
            for (BookedTraveler bookedTraveler : bookedTravelerList) {
                if (null != bookedTraveler.getAncillaries()) {

                    List<ExtraWeightAncillary> extraWeightAncillaryList = getExtraWeightUnpaidAncillaries( bookedTraveler.getAncillaries().getCollection() );

                    if ( ! extraWeightAncillaryList.isEmpty() ) {
                    	LOG.info( "areThereExtraWeightUnpaidItems: YES, there are items in extraWeightAncillaryList {}",  extraWeightAncillaryList.size());
                    	for ( ExtraWeightAncillary extraWeightAncillary: extraWeightAncillaryList) {
                    		LOG.info( "areThereExtraWeightUnpaidItems.ExtraWeightAncillary: {}", extraWeightAncillary);
                    	}
                        return true;
                    }
                }
            }
        }

        LOG.info( "areThereExtraWeightUnpaidItems: NO, there are no items in extraWeightAncillaryList" );

        return false;
    }

    /**
     * @param bookedTraveler
     * @return
     * @throws GenericException
     */
    public static boolean areThereUpgradeUnpaidItems(BookedTraveler bookedTraveler) throws GenericException {

        if (null != bookedTraveler && null != bookedTraveler.getAncillaries()) {

            List<CabinUpgradeAncillary> cabinUpgradeAncillaryList = getUpgradeUnpaidAncillaries(bookedTraveler.getAncillaries().getCollection());

            if (!cabinUpgradeAncillaryList.isEmpty()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param bookedTravelerList
     * @return
     * @throws GenericException
     */
    public static boolean areThereUnpaidItems(List<BookedTraveler> bookedTravelerList) throws GenericException {
        for (BookedTraveler bookedTraveler : bookedTravelerList) {
            if (null != bookedTraveler.getAncillaries()) {
                List<AbstractAncillary> allAncillaries = getAllAncillaries(bookedTraveler);

                List<TravelerAncillary> travelerAncillaryList = getUnpaidAncillaries(allAncillaries);

                if (!travelerAncillaryList.isEmpty()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param bookedTravelerList
     * @return
     * @throws GenericException
     */
    public static boolean areThereWaiveItems(List<BookedTraveler> bookedTravelerList) throws GenericException {
        for (BookedTraveler bookedTraveler : bookedTravelerList) {
            if (null != bookedTraveler.getAncillaries()) {
                List<AbstractAncillary> allAncillaries = getAllAncillaries(bookedTraveler);

                List<TravelerAncillary> travelerAncillaryList = getWaiveAncillaries(allAncillaries);

                if (!travelerAncillaryList.isEmpty()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param bookedTravelerList
     * @return
     * @throws GenericException
     */
    public static boolean areThereWaiveSeats(List<BookedTraveler> bookedTravelerList) throws GenericException {
        for (BookedTraveler bookedTraveler : bookedTravelerList) {
            if (null != bookedTraveler.getAncillaries()) {
                List<AbstractAncillary> allAncillaries = bookedTraveler.getSeatAncillaries().getCollection();

                List<TravelerAncillary> travelerAncillaryList = getWaiveAncillaries(allAncillaries);

                if (!travelerAncillaryList.isEmpty()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * As seats are treated as if they were ancillaries, this method add an
     * ancillary entry to the ancillaries collection only if it is not already
     * added
     *
     * @param nameRefNumber
     * @param segmentCode
     * @param legCode
     * @param currencyCode
     * @param seatCode
     * @param equipmentType
     * @return
     */
    public static TravelerAncillary createSeatAncillaryEntryForUnreservedSeats(String nameRefNumber, String segmentCode, String legCode, String currencyCode, String seatCode, String equipmentType) throws Exception {

        TravelerAncillary travelerAncillary = new TravelerAncillary();

        Ancillary ancillary = new Ancillary();

        AncillaryPostBookingType seatAncillary = AncillaryPostBookingType._0B5;
        CurrencyValue currency = CurrencyUtil.getCurrencyValue(currencyCode);
        ancillary.setCurrency(currency);
        ancillary.setSsrCode(seatAncillary.getSsr());
        ancillary.setType(seatAncillary.getType());
        ancillary.setCommercialName(seatAncillary.getCommercialName());
        ancillary.setEmdType(seatAncillary.getEmdType());
        ancillary.setFeeApplicationIndicator(seatAncillary.getFeeApplicationIndicator());
        ancillary.setGroupCode(seatAncillary.getGroup());
        ancillary.setOwningCarrierCode(seatAncillary.getOwningCarrierCode());
        ancillary.setPoints(0);
        ancillary.setRficCode(seatAncillary.getRficCode());
        ancillary.setRficSubcode(seatAncillary.getType());
        ancillary.setSegmentIndicator(seatAncillary.getSegmentIndicator());
        ancillary.setVendor(seatAncillary.getVendor());

        ancillary.setBookingIndicator(" ");
        ancillary.setRefundIndicator("R");
        ancillary.setCommisionIndicator("N");
        ancillary.setInterlineIndicator("N");
        ancillary.setFeeApplicationIndicator(" ");
        ancillary.setRefundIndicator(" ");
        ancillary.setRefundFormIndicator(" ");
        ancillary.setFareGuaranteedIndicator("T");
        ancillary.setTicketingIndicator("0");
        ancillary.setBookingSource("0");
        ancillary.setTaxExemption("N");
        ancillary.setTicketUsedForEMDPricing("N");
        ancillary.setEquipmentType(equipmentType);

        travelerAncillary.setAncillary(ancillary);
        travelerAncillary.setActionCode(ActionCodeType.HK.toString());
        travelerAncillary.setLegCode(legCode);
        travelerAncillary.setLinkedID(UUID.randomUUID().toString());
        travelerAncillary.setNameNumber(nameRefNumber);
        travelerAncillary.setIsPartOfReservation(false);
        travelerAncillary.setQuantity(1);
        travelerAncillary.setSegmentCodeAux(segmentCode);
        travelerAncillary.setSeatCode(seatCode);
        travelerAncillary.setDefaultFreeSeat(true);

        return travelerAncillary;

    }

    /**
     * @param bookedTraveler
     * @param travelerAncillaryUpdate
     * @param ancillaryOfferDB
     * @return
     */
    public static TravelerAncillary createUpgradeAncillary(BookedTraveler bookedTraveler, CabinUpgradeAncillary travelerAncillaryUpdate, AncillaryOffer ancillaryOfferDB) {

        //CheckinAncillaryType upgradeAncillary = CheckinAncillaryType._UCV;
        Ancillary ancillary = new Ancillary();

        String currencyCode = ancillaryOfferDB.getAncillary().getCurrency().getCurrencyCode();
        CurrencyValue currencyValue = new CurrencyValue();
        currencyValue.setCurrencyCode(currencyCode);
        currencyValue.setBase(CurrencyUtil.getAmount(ancillaryOfferDB.getAncillary().getCurrency().getBase(), currencyCode));
        currencyValue.setTotal(CurrencyUtil.getAmount(ancillaryOfferDB.getAncillary().getCurrency().getTotal(), currencyCode));
        currencyValue.setTotalTax(CurrencyUtil.getAmount(ancillaryOfferDB.getAncillary().getCurrency().getTotalTax(), currencyCode));
        currencyValue.setTaxDetails(CurrencyUtil.getTaxDetails(ancillaryOfferDB.getAncillary().getCurrency().getTaxDetails(), currencyCode));

        ancillary.setCurrency(currencyValue);
        ancillary.setType(ancillaryOfferDB.getAncillary().getType());
        ancillary.setCommercialName(ancillaryOfferDB.getAncillary().getCommercialName());
        ancillary.setEmdType(ancillaryOfferDB.getAncillary().getEmdType());
        ancillary.setFeeApplicationIndicator(ancillaryOfferDB.getAncillary().getFeeApplicationIndicator());
        ancillary.setGroupCode(ancillaryOfferDB.getAncillary().getGroupCode());
        ancillary.setOwningCarrierCode(ancillaryOfferDB.getAncillary().getOwningCarrierCode());
        ancillary.setPoints(ancillaryOfferDB.getAncillary().getPoints());
        ancillary.setRficCode(ancillaryOfferDB.getAncillary().getRficCode());
        ancillary.setRficSubcode(ancillaryOfferDB.getAncillary().getRficSubcode());
        ancillary.setSegmentIndicator(ancillaryOfferDB.getAncillary().getSegmentIndicator());
        ancillary.setVendor(ancillaryOfferDB.getAncillary().getVendor());

        TravelerAncillary travelerAncillary = new TravelerAncillary();
        travelerAncillary.setSegmentCodeAux(travelerAncillaryUpdate.getSegmentCode());
        travelerAncillary.setAncillary(ancillary);
        travelerAncillary.setActionCode(ActionCodeType.HD.toString());
        travelerAncillary.setNameNumber(bookedTraveler.getNameRefNumber());
        travelerAncillary.setIsPartOfReservation(false);
        travelerAncillary.setQuantity(1);

        return travelerAncillary;

    }

    /**
     * As seats are treated as if they were ancillaries, this method add an
     * ancillary entry to the ancillaries collection only if it is not already
     * added
     *
     * @param bookedTraveler
     * @param segmentChoice
     * @param legCode
     * @param seatCode
     * @return
     */
    public static TravelerAncillary createSeatAncillaryEntryForPaidSeats(
            BookedTraveler bookedTraveler,
            SegmentChoice segmentChoice,
            String legCode,
            String seatCode
    ) {

        if (null == segmentChoice.getLinkedID() || segmentChoice.getLinkedID().trim().isEmpty()) {
            segmentChoice.setLinkedID(UUID.randomUUID().toString());
        }

        boolean notExist = notExistsAncillaryEntry(
                segmentChoice.getLinkedID(),
                getNewAncillaries(bookedTraveler.getSeatAncillaries().getCollection())
        );

        if (notExist && null == segmentChoice.getActionCode()) {
            TravelerAncillary travelerAncillary = new TravelerAncillary();

            segmentChoice.getSegmentCode();

            Ancillary ancillary = new Ancillary();

            boolean isChargeable = false;

            if (segmentChoice.getSeat() instanceof SeatChoice) {
                SeatChoice seatChoice = (SeatChoice) segmentChoice.getSeat();
                if (seatChoice.isSubjectToSeatSelectionFee() && null != segmentChoice.getSeatSelectionFee()) {

                    CurrencyValue currencyValue = null;

                    if (!seatChoice.isApplyBenefit() && BigDecimal.ZERO.compareTo(segmentChoice.getSeatSelectionFee().getTotal()) <= 0) {
                        currencyValue = CurrencyUtil.getTestCurrencyValue(segmentChoice.getSeatSelectionFee().getCurrencyCode());
                    }

                    if (null == currencyValue) {
                        currencyValue = segmentChoice.getSeatSelectionFee();
                    }

                    ancillary.setCurrency(currencyValue);
                    isChargeable = true;
                }
            } else if (segmentChoice.getSeat() instanceof SeatChoiceUpsell) {
                SeatChoiceUpsell seatChoiceUpsell = (SeatChoiceUpsell) segmentChoice.getSeat();
                if (null != seatChoiceUpsell.getCurrency()) {

                    CurrencyValue currencyValue = null;

                    if (!seatChoiceUpsell.isApplyBenefit() && BigDecimal.ZERO.compareTo(seatChoiceUpsell.getCurrency().getTotal()) <= 0) {
                        currencyValue = CurrencyUtil.getTestCurrencyValue(seatChoiceUpsell.getCurrency().getCurrencyCode());
                    }

                    if (null == currencyValue) {
                        currencyValue = seatChoiceUpsell.getCurrency();
                    }

                    ancillary.setCurrency(currencyValue);
                    ancillary.setPriceCalcList(seatChoiceUpsell.getPriceCalcList());

                    isChargeable = true;
                }

                travelerAncillary.setApplyBenefit(seatChoiceUpsell.isApplyBenefit());
            }

            if (isChargeable) {
                AncillaryPostBookingType seatAncillary = AncillaryPostBookingType._0B5;

                //ancillary.setBookingIndicator(null);
                //ancillary.setCommisionIndicator("N");
                //ancillary.setInterlineIndicator("N");
                //ancillary.setRefundIndicator("R");
                ancillary.setSsrCode(seatAncillary.getSsr());
                ancillary.setType(seatAncillary.getType());
                ancillary.setCommercialName(seatAncillary.getCommercialName());
                ancillary.setEmdType("2");
                ancillary.setFeeApplicationIndicator("4");
                ancillary.setGroupCode(seatAncillary.getGroup());
                ancillary.setOwningCarrierCode("AM");
                ancillary.setPoints(0);
                ancillary.setRficCode(seatAncillary.getRficCode());
                ancillary.setRficSubcode(seatAncillary.getType());
                ancillary.setSegmentIndicator(seatAncillary.getSegmentIndicator());
                ancillary.setVendor(seatAncillary.getVendor());

                //travelerAncillary.setAncillaryId(null);
                //travelerAncillary.setEmd(null);
                travelerAncillary.setAncillary(ancillary);
                travelerAncillary.setActionCode(ActionCodeType.HD.toString());
                travelerAncillary.setLegCode(legCode);
                travelerAncillary.setLinkedID(segmentChoice.getLinkedID());
                travelerAncillary.setNameNumber(bookedTraveler.getNameRefNumber());
                travelerAncillary.setIsPartOfReservation(false);
                travelerAncillary.setQuantity(1);
                travelerAncillary.setSegmentCodeAux(segmentChoice.getSegmentCode());

                travelerAncillary.setSeatCode(seatCode);

                //bookedTraveler.getSeatAncillaries().getCollection().add(travelerAncillary);
                return travelerAncillary;
            }
        }

        return null;

    }

    /**
     * As seats are treated as if they were ancillaries, this method add an
     * ancillary entry to the ancillaries collection only if it is not already
     * added
     *
     * @param bookedTravelerList
     * @param legCode
     */
    public static void createSeatAncillariesEntryForPaidSeats(List<BookedTraveler> bookedTravelerList, String legCode) {
        for (BookedTraveler bookedTraveler : bookedTravelerList) {
            for (SegmentChoice segmentChoice : getUnpaidSeats(bookedTraveler.getSegmentChoices().getCollection())) {

                if (null == segmentChoice.getLinkedID() || segmentChoice.getLinkedID().trim().isEmpty()) {
                    segmentChoice.setLinkedID(UUID.randomUUID().toString());
                }

                if (notExistsAncillaryEntry(segmentChoice.getLinkedID(), getNewAncillaries(bookedTraveler.getSeatAncillaries().getCollection()))
                        && null == segmentChoice.getActionCode()) {
                    TravelerAncillary travelerAncillary = new TravelerAncillary();

                    segmentChoice.getSegmentCode();

                    Ancillary ancillary = new Ancillary();

                    boolean isChargeable = false;

                    if (segmentChoice.getSeat() instanceof SeatChoice) {
                        SeatChoice seatChoice = (SeatChoice) segmentChoice.getSeat();
                        if (seatChoice.isSubjectToSeatSelectionFee() && null != segmentChoice.getSeatSelectionFee()) {

                            CurrencyValue currencyValue = null;

                            if (!seatChoice.isApplyBenefit() && BigDecimal.ZERO.compareTo(segmentChoice.getSeatSelectionFee().getTotal()) <= 0) {
                                currencyValue = CurrencyUtil.getTestCurrencyValue(segmentChoice.getSeatSelectionFee().getCurrencyCode());
                            }

                            if (null == currencyValue) {
                                currencyValue = segmentChoice.getSeatSelectionFee();
                            }

                            ancillary.setCurrency(currencyValue);
                            isChargeable = true;
                        }
                    } else if (segmentChoice.getSeat() instanceof SeatChoiceUpsell) {
                        SeatChoiceUpsell seatChoiceUpsell = (SeatChoiceUpsell) segmentChoice.getSeat();
                        if (null != seatChoiceUpsell.getCurrency()) {

                            CurrencyValue currencyValue = null;

                            if (!seatChoiceUpsell.isApplyBenefit() && BigDecimal.ZERO.compareTo(seatChoiceUpsell.getCurrency().getTotal()) <= 0) {
                                currencyValue = CurrencyUtil.getTestCurrencyValue(seatChoiceUpsell.getCurrency().getCurrencyCode());
                            }

                            if (null == currencyValue) {
                                currencyValue = seatChoiceUpsell.getCurrency();
                            }

                            ancillary.setCurrency(currencyValue);
                            isChargeable = true;
                        }
                    }

                    if (isChargeable) {
                        AncillaryPostBookingType seatAncillary = AncillaryPostBookingType._0B5;

                        //ancillary.setBookingIndicator(null);
                        //ancillary.setCommisionIndicator("N");
                        //ancillary.setInterlineIndicator("N");
                        //ancillary.setRefundIndicator("R");
                        ancillary.setSsrCode(seatAncillary.getSsr());
                        ancillary.setType(seatAncillary.getType());
                        ancillary.setCommercialName(seatAncillary.getCommercialName());
                        ancillary.setEmdType("2");
                        ancillary.setFeeApplicationIndicator("4");
                        ancillary.setGroupCode(seatAncillary.getGroup());
                        ancillary.setOwningCarrierCode("AM");
                        ancillary.setPoints(0);
                        ancillary.setRficCode(seatAncillary.getRficCode());
                        ancillary.setRficSubcode(seatAncillary.getType());
                        ancillary.setSegmentIndicator(seatAncillary.getSegmentIndicator());
                        ancillary.setVendor(seatAncillary.getVendor());

                        //travelerAncillary.setAncillaryId(null);
                        //travelerAncillary.setEmd(null);
                        travelerAncillary.setAncillary(ancillary);
                        travelerAncillary.setActionCode(ActionCodeType.HD.toString());
                        travelerAncillary.setLegCode(legCode);
                        travelerAncillary.setLinkedID(segmentChoice.getLinkedID());
                        travelerAncillary.setNameNumber(bookedTraveler.getNameRefNumber());
                        travelerAncillary.setIsPartOfReservation(false);
                        travelerAncillary.setQuantity(1);
                        travelerAncillary.setSegmentCodeAux(segmentChoice.getSegmentCode());

                        bookedTraveler.getSeatAncillaries().getCollection().add(travelerAncillary);
                    }
                }
            }
        }
    }

    /**
     * @param missingField
     * @param missingCheckinRequiredFields
     * @return
     */
    public static boolean hasMissingProperty(String missingField, List<String> missingCheckinRequiredFields) {
        for (String missingCheckinRequiredField : missingCheckinRequiredFields) {
            if (null != missingField && missingCheckinRequiredField.equalsIgnoreCase(missingField)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param bookedTravelerList
     * @throws GenericException
     */
    public static void checkMissingProperties(List<BookedTraveler> bookedTravelerList) throws GenericException {
        for (BookedTraveler bookedTraveler : bookedTravelerList) {
            if (null != bookedTraveler.getMissingCheckinRequiredFields() && !bookedTraveler.getMissingCheckinRequiredFields().isEmpty()) {

                for (String ineligibleReason : bookedTraveler.getMissingCheckinRequiredFields()) {

                    BookedTravelerCheckinIneligibleReasonsType bookedTravelerCheckinIneligibleReasonsType = BookedTravelerCheckinIneligibleReasonsType.getType(ineligibleReason);
                    CheckinIneligibleReasons checkinIneligibleReasons = CheckinIneligibleReasons.getType(ineligibleReason);

                    if (null != bookedTravelerCheckinIneligibleReasonsType) {
                        switch (bookedTravelerCheckinIneligibleReasonsType) {
                            case MISSING_GENDER:
                                //throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.INELIGIBLE_REASON_MISSING_GENDER, "passenger: " + bookedTraveler.getNameRefNumber());
                                break;
                            case MISSING_EMERGENCY_CONTACT:
                                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.INELIGIBLE_REASON_MISSING_EMERGENCY_CONTACT, "passenger: " + bookedTraveler.getNameRefNumber());
                            case MISSING_RESIDENCE_ADDRESS:
                                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.INELIGIBLE_REASON_MISSING_RESIDENCE_ADDRESS, "passenger: " + bookedTraveler.getNameRefNumber());
                            case MISSING_DESTINATION_ADDRESS:
                                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.INELIGIBLE_REASON_MISSING_DESTINATION_ADDRESS, "passenger: " + bookedTraveler.getNameRefNumber());
                            case MISSING_RESIDENCE_COUNTRY:
                                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.INELIGIBLE_REASON_MISSING_RESIDENCE_COUNTRY, "passenger: " + bookedTraveler.getNameRefNumber());
                            case BLOCKED:
                                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.INELIGIBLE_REASON_BLOCKED, "passenger: " + bookedTraveler.getNameRefNumber());
                            case MISSING_TRAVEL_DOC:
                                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.INELIGIBLE_REASON_MISSING_TRAVEL_DOC, "passenger: " + bookedTraveler.getNameRefNumber());
                            case MISSING_DOCUMENT_VERIFICATION:
                                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.INELIGIBLE_REASON_MISSING_DOCUMENT_VERIFICATION, "passenger: " + bookedTraveler.getNameRefNumber());
                        }
                    } else if (null != checkinIneligibleReasons) {
                        switch (checkinIneligibleReasons) {
                            case MEC:
                                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.INELIGIBLE_REASON_MISSING_EMERGENCY_CONTACT, "passenger: " + bookedTraveler.getNameRefNumber());
                            case MRA:
                                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.INELIGIBLE_REASON_MISSING_RESIDENCE_ADDRESS, "passenger: " + bookedTraveler.getNameRefNumber());
                            case MDA:
                                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.INELIGIBLE_REASON_MISSING_DESTINATION_ADDRESS, "passenger: " + bookedTraveler.getNameRefNumber());
                            case MG:
                                //throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.INELIGIBLE_REASON_MISSING_GENDER, "passenger: " + bookedTraveler.getNameRefNumber());
                                break;
                            case MRC:
                                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.INELIGIBLE_REASON_MISSING_RESIDENCE_COUNTRY, "passenger: " + bookedTraveler.getNameRefNumber());
                            case MRD:
                                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.INELIGIBLE_REASON_MISSING_DOCUMENT_VERIFICATION, "passenger: " + bookedTraveler.getNameRefNumber());
                            case MRT:
                                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.INELIGIBLE_REASON_MISSING_TRAVEL_DOC, "passenger: " + bookedTraveler.getNameRefNumber());
                            case MDHS:
                                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.INELIGIBLE_REASON_DHS_NOT_CLEARED, "passenger: " + bookedTraveler.getNameRefNumber());
                            case MESTA:
                                //
                                break;
                            case MRAI:
                                ///throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.INELIGIBLE_REASON_MISSING_RESIDENCE_ADDRESS_INFANT, "passenger: " + bookedTraveler.getNameRefNumber());
                                break;
                            case MDAI:
                                //throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.INELIGIBLE_REASON_MISSING_DESTINATION_ADDRESS_INFANT, "passenger: " + bookedTraveler.getNameRefNumber());
                                break;
                            case B:
                                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.INELIGIBLE_REASON_BLOCKED, "passenger: " + bookedTraveler.getNameRefNumber());
                        }
                    } else {
                        throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.BAD_REQUEST, "INELIGIBLE REASON: " + ineligibleReason + ", passenger: " + bookedTraveler.getNameRefNumber());
                    }
                }
            }
        }
    }

    /**
     * @param passengersWithSameClassList
     * @return
     */
    public static boolean hasReturnFlight(List<BookedTraveler> passengersWithSameClassList) {
        for (BookedTraveler bookedTraveler : passengersWithSameClassList) {
            if (null != bookedTraveler.getSsrRemarks() && null != bookedTraveler.getSsrRemarks().getSsrCodes()) {
                for (String ssrCode : bookedTraveler.getSsrRemarks().getSsrCodes()) {
                    if ("RT".equalsIgnoreCase(ssrCode)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * @param acsCheckInPassengerRSACSList
     * @param recordLocator
     * @return
     * @throws Exception
     */
    public static List<PectabBoardingPass> getPectacBoardingPassList(
            List<ACSCheckInPassengerRSACS> acsCheckInPassengerRSACSList,
            String recordLocator
    ) throws Exception {
        List<PectabBoardingPass> pectabDataList = new ArrayList<>();
        for (ACSCheckInPassengerRSACS aCSCheckInPassengerRSACS : acsCheckInPassengerRSACSList) {
            if (null != aCSCheckInPassengerRSACS.getPECTABDataList()) {
                for (PECTABDataListACS.PECTABData pectabData : aCSCheckInPassengerRSACS.getPECTABDataList().getPECTABData()) {
                    PectabBoardingPass pectabDataLocal = new PectabBoardingPass();
                    pectabDataLocal.setValue(pectabData.getValue());
                    pectabDataLocal.setVersion(pectabData.getVersion());

                    if (!pectabDataList.contains(pectabDataLocal)) {
                        pectabDataList.add(pectabDataLocal);
                    }
                }
            }
        }

        return pectabDataList;
    }

    /**
     * @param creditCardPaymentRequestLit
     * @param transactionTime
     * @return
     */
    public static List<CreditCardPaymentSummary> getCreditCardPaymentSummary(
            List<CreditCardPaymentRequest> creditCardPaymentRequestLit,
            String transactionTime
    ) {

        List<CreditCardPaymentSummary> creditCardPaymentSummaryList = new ArrayList<>();

        //use de default mask (XXXXXXXXXXXX####)
        MaskCreditCardUtil maskCreditCard = new MaskCreditCardUtil();
        for (CreditCardPaymentRequest creditCardPaymentRequest : creditCardPaymentRequestLit) {
            CreditCardPaymentSummary creditCardPaymentSummary = new CreditCardPaymentSummary();
            creditCardPaymentSummary.setBankName(creditCardPaymentRequest.getBankName());
            creditCardPaymentSummary.setBillingAddress(creditCardPaymentRequest.getBillingAddress());
            creditCardPaymentSummary.setCardHolderName(creditCardPaymentRequest.getCardHolderName());
            creditCardPaymentSummary.setCardType(creditCardPaymentRequest.getCardType());
            creditCardPaymentSummary.setInstallments(creditCardPaymentRequest.getInstallments());
            creditCardPaymentSummary.setMaskedCcNumber(maskCreditCard.maskCardNumber(creditCardPaymentRequest.getCcNumber()));
            creditCardPaymentSummary.setTransactionTime(transactionTime);
            if ((null != creditCardPaymentRequest.getBankName()
                    && !creditCardPaymentRequest.getBankName().trim().isEmpty())
                    && null != creditCardPaymentRequest.getInstallments()
                    && null != creditCardPaymentRequest.getInstallments().getNumberOfMonths()
                    && creditCardPaymentRequest.getInstallments().getNumberOfMonths() > 0) {
                try {
                    BigDecimal monthly;
                    monthly = creditCardPaymentRequest.getPaymentAmount().getTotal().divide(
                            new BigDecimal(creditCardPaymentRequest.getInstallments().getNumberOfMonths().toString()),
                            2,
                            RoundingMode.HALF_UP
                    );
                    creditCardPaymentSummary.setMonthlyAmount(monthly);
                } catch (Exception ex) {
                    creditCardPaymentSummary.setMonthlyAmount(null);
                }
            } else {
                creditCardPaymentSummary.setMonthlyAmount(null);
            }
            creditCardPaymentSummary.setPaymentAmount(creditCardPaymentRequest.getPaymentAmount());

            creditCardPaymentSummaryList.add(creditCardPaymentSummary);
        }

        return creditCardPaymentSummaryList;
    }

    /**
     * @param uatpPaymentRequestLit
     * @param total
     * @return
     */
    public static List<UatpPaymentSummary> getUatpPaymentSummary(
            List<UatpPaymentRequest> uatpPaymentRequestLit,
            CurrencyValue total
    ) {

        List<UatpPaymentSummary> uatpPaymentSummaryList = new ArrayList<>();

        MaskCreditCardUtil maskCreditCard = new MaskCreditCardUtil();
        for (UatpPaymentRequest uatpPaymentRequest : uatpPaymentRequestLit) {
            UatpPaymentSummary uatpPaymentSummary = new UatpPaymentSummary();
            uatpPaymentSummary.setCardCode(maskCreditCard.maskCardNumber(uatpPaymentRequest.getCardCode()));

            uatpPaymentSummary.setPaymentAmount(total.getTotal().doubleValue());

            uatpPaymentSummaryList.add(uatpPaymentSummary);
        }

        return uatpPaymentSummaryList;
    }

    /**
     * @param total
     * @param formsOfPaymentWrapper
     * @param transactionTime
     * @return
     */
    public static PaymentSummaryCollection getPaymentSummary(
            CurrencyValue total,
            FormsOfPaymentWrapper formsOfPaymentWrapper,
            String transactionTime
    ) {

        PaymentSummaryCollection paymentSummaryCollection = new PaymentSummaryCollection();

        List<PaymentSummary> collection = new ArrayList<>();

        collection.addAll(
                getCreditCardPaymentSummary(
                        formsOfPaymentWrapper.getCreditCardPaymentRequestList(), transactionTime
                )
        );

        collection.addAll(
                getUatpPaymentSummary(
                        formsOfPaymentWrapper.getUatpPaymentRequestList(),
                        total
                )
        );

        paymentSummaryCollection.setCollection(collection);

        return paymentSummaryCollection;
    }

    /**
     * @param purchaseOrder
     * @param checkInPassengerRSList
     * @param cartPNRCollection
     * @param checkedInLegs
     * @param pnrLocator
     * @param transactionTime
     * @param pos
     * @param legCode
     * @param checkForCheckin
     * @return
     */
    public static CheckinConfirmation getCheckInResponse(
            PurchaseOrder purchaseOrder,
            List<ACSCheckInPassengerRSACS> checkInPassengerRSList,
            CartPNRCollection cartPNRCollection,
            BookedLegCollection checkedInLegs,
            String pnrLocator,
            long transactionTime,
            String pos,
            String legCode,
            boolean checkForCheckin
    ) {

        CheckinConfirmation checkinConfirmation = new CheckinConfirmation();
        checkinConfirmation.setTransactionId("");
        checkinConfirmation.setItineraryPdf("");
        checkinConfirmation.setPosCode(pos);
        checkinConfirmation.setBoardingPassId(pnrLocator);

        if (checkForCheckin && (null == checkInPassengerRSList || checkInPassengerRSList.isEmpty())) {
            Warning warning = new Warning();
            warning.setCode(ErrorType.ALL_PASSENGERS_ARE_ALREADY_CHECKED_IN.getErrorCode());
            warning.setMsg(ErrorType.ALL_PASSENGERS_ARE_ALREADY_CHECKED_IN.getFullDescription());
            WarningCollection _warnings = new WarningCollection();
            _warnings.getCollection().add(warning);
            checkinConfirmation.setWarnings(_warnings);
        }

        checkinConfirmation.setCheckedInLegs(checkedInLegs);

        if (null != purchaseOrder.getPaymentInfo()
                && null != purchaseOrder.getPaymentInfo().getCollection()
                && !purchaseOrder.getPaymentInfo().getCollection().isEmpty()) {

            FormsOfPaymentWrapper formsOfPaymentWrapper;
            formsOfPaymentWrapper = new FormsOfPaymentWrapper(
                    purchaseOrder.getPaymentInfo()
            );

            PaymentSummaryCollection paymentSummaryCollection;
            paymentSummaryCollection = getPaymentSummary(
                    purchaseOrder.getPaymentAmount(),
                    formsOfPaymentWrapper,
                    TimeUtil.getStrTime(transactionTime)
            );

            checkinConfirmation.setPaymentSummary(paymentSummaryCollection);
        }

        for (CartPNR cartPNR : cartPNRCollection.getCollection()) {
            if (cartPNR.isTouched()) {
                checkinConfirmation.getCheckedInCarts().getCollection().add(cartPNR.getCartPNRConfirmation());
            }
        }

        checkinConfirmation.setTransactionTime(TimeUtil.getStrTime(transactionTime));

        checkinConfirmation.setLegCode(legCode);
        return checkinConfirmation;
    }

    /**
     * @param cartPNRCollection
     */
    public static void removeEmdConsummedAtIssuance(CartPNRCollection cartPNRCollection) {
        for (CartPNR cartPNR : cartPNRCollection.getCollection()) {
            if (null != cartPNR.getTravelerInfo() && null != cartPNR.getTravelerInfo().getCollection()) {

                for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {

                    if (null != bookedTraveler.getAncillaries() && null != bookedTraveler.getAncillaries().getCollection()) {

                        Iterator<AbstractAncillary> abstractAncillaryIterator = bookedTraveler.getAncillaries().getCollection().iterator();

                        while (abstractAncillaryIterator.hasNext()) {
                            AbstractAncillary abstractAncillary = abstractAncillaryIterator.next();

                            if (abstractAncillary instanceof TicketedTravelerAncillary) {
                                TicketedTravelerAncillary ticketedTravelerAncillary = (TicketedTravelerAncillary) abstractAncillary;

                                if ("Y".equalsIgnoreCase(ticketedTravelerAncillary.getAncillary().getEmdConsummedAtIssuance())) {
                                    abstractAncillaryIterator.remove();
                                }
                            } else if (abstractAncillary instanceof PaidTravelerAncillary) {
                                PaidTravelerAncillary paidTravelerAncillary = (PaidTravelerAncillary) abstractAncillary;

                                if ("Y".equalsIgnoreCase(paidTravelerAncillary.getEmdConsummedAtIssuance())) {
                                    abstractAncillaryIterator.remove();
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @param formsOfPaymentWrapper
     * @param totalInCart
     * @return
     */
    public static BigDecimal getTotal(final FormsOfPaymentWrapper formsOfPaymentWrapper, final BigDecimal totalInCart) {

        BigDecimal totalInPurchase = BigDecimal.ZERO;

        String currencyCode;

        if (!formsOfPaymentWrapper.getCreditCardPaymentRequestList().isEmpty()) {
            currencyCode = CurrencyUtil.getCurrencyCodeFromCreditCardPayment(
                    formsOfPaymentWrapper.getCreditCardPaymentRequestList()
            );

            totalInPurchase = CurrencyUtil.getTotalCreditCardPayment(
                    formsOfPaymentWrapper.getCreditCardPaymentRequestList(),
                    currencyCode
            );
        } else if (!formsOfPaymentWrapper.getRemotePaymentRequestList().isEmpty()) {
            currencyCode = CurrencyUtil.getCurrencyCodeFromRemotePayment(
                    formsOfPaymentWrapper.getRemotePaymentRequestList()
            );

            totalInPurchase = CurrencyUtil.getTotalRemotePayment(
                    formsOfPaymentWrapper.getRemotePaymentRequestList(),
                    currencyCode
            );
        } else if (!formsOfPaymentWrapper.getPaypalPaymentRequestList().isEmpty()) {
            currencyCode = CurrencyUtil.getCurrencyCodeFromPaypalPayment(
                    formsOfPaymentWrapper.getPaypalPaymentRequestList()
            );

            totalInPurchase = CurrencyUtil.getTotalPaypalPayment(
                    formsOfPaymentWrapper.getPaypalPaymentRequestList(),
                    currencyCode
            );
        } else if (!formsOfPaymentWrapper.getUatpPaymentRequestList().isEmpty()) {
            //in this case the total is taken from cart
            //these are inmutable objects
            totalInPurchase = null == totalInCart ? BigDecimal.ZERO : totalInCart;
        } else if (!formsOfPaymentWrapper.getMcoPaymentRequestList().isEmpty()) {
            //in this case the total is taken from cart
            //these are inmutable objects
            totalInPurchase = null == totalInCart ? BigDecimal.ZERO : totalInCart;
        } else if (!formsOfPaymentWrapper.getVisaCheckoutPaymentRequestList().isEmpty()) {
            //in this case the total is taken from cart
            //these are inmutable objects
            totalInPurchase = null == totalInCart ? BigDecimal.ZERO : totalInCart;
        }


        return totalInPurchase;
    }

    /**
     * @param expiryDateString
     * @param isPaymentRQ
     * @return
     */
    public static String getExpiryDate(String expiryDateString, boolean isPaymentRQ) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
            Date expiryDate = simpleDateFormat.parse(expiryDateString);
            simpleDateFormat = new SimpleDateFormat("MMyy");
            String exDate = simpleDateFormat.format(expiryDate);

            if (isPaymentRQ) {
                return exDate.substring(0, 2) + "20" + exDate.substring(2);
            } else {
                return exDate;
            }

        } catch (ParseException ex) {
            // set expiry date to december of the next year
            Calendar now = Calendar.getInstance(); // Gets the current
            // date and time
            int year = now.get(Calendar.YEAR) + 1; // The current year
            // as an int

            String yearStr = String.valueOf(year);
            if (yearStr.length() >= 3) {
                if (isPaymentRQ) {
                    return "12" + year;
                } else {
                    return "12" + yearStr.substring(2);
                }
            } else {
                if (isPaymentRQ) {
                    return "1220" + yearStr;
                } else {
                    return "12" + yearStr;
                }
            }
        }
    }

    /**
     * @param acsCheckInPassengerRSACSList
     * @return
     */
    public static boolean thereAreBoardingPasses(ACSCheckInPassengerRSACS acsCheckInPassengerRSACSList) {

        try {
            List<ItineraryPassengerACS> itineraryPassengerACSList = acsCheckInPassengerRSACSList.getItineraryPassengerList().getItineraryPassenger();

            if (null == itineraryPassengerACSList || itineraryPassengerACSList.isEmpty()) {
                return false;
            }

            for (ItineraryPassengerACS itineraryPassengerACS : itineraryPassengerACSList) {
                List<PassengerDetailACS> passengerDetailACSList = itineraryPassengerACS.getPassengerDetailList().getPassengerDetail();

                if (null == passengerDetailACSList || passengerDetailACSList.isEmpty()) {
                    return false;
                }

                for (PassengerDetailACS passengerDetailACS : passengerDetailACSList) {
                    List<TravelDocDataACS> travelDocDataACSList = passengerDetailACS.getTravelDocDataList().getTravelDocData();

                    if (null == travelDocDataACSList || travelDocDataACSList.isEmpty()) {
                        return false;
                    }

                    for (TravelDocDataACS travelDocDataACS : travelDocDataACSList) {
                        if (null == travelDocDataACS.getTravelDoc() || travelDocDataACS.getTravelDoc().trim().isEmpty()) {
                            return false;
                        }
                    }
                }
            }

        } catch (Exception ex) {
            return false;
        }

        return true;
    }

    /**
     * @param msg
     * @param acsCheckInPassengerRSACSList
     * @return
     */
    public static boolean searchInBoardingPasses(
            String msg,
            List<ACSCheckInPassengerRSACS> acsCheckInPassengerRSACSList
    ) {

        if (null == msg || msg.trim().isEmpty()) {
            return false;
        }

        try {
            for (ACSCheckInPassengerRSACS aCSCheckInPassengerRSACS : acsCheckInPassengerRSACSList) {

                List<ItineraryPassengerACS> itineraryPassengerACSList = aCSCheckInPassengerRSACS.getItineraryPassengerList().getItineraryPassenger();

                if (null != itineraryPassengerACSList && !itineraryPassengerACSList.isEmpty()) {
                    for (ItineraryPassengerACS itineraryPassengerACS : itineraryPassengerACSList) {
                        List<PassengerDetailACS> passengerDetailACSList = itineraryPassengerACS.getPassengerDetailList().getPassengerDetail();

                        if (null != passengerDetailACSList && !passengerDetailACSList.isEmpty()) {
                            for (PassengerDetailACS passengerDetailACS : passengerDetailACSList) {
                                List<TravelDocDataACS> travelDocDataACSList = passengerDetailACS.getTravelDocDataList().getTravelDocData();

                                if (null != travelDocDataACSList && !travelDocDataACSList.isEmpty()) {
                                    for (TravelDocDataACS travelDocDataACS : travelDocDataACSList) {
                                        if (null != travelDocDataACS.getTravelDoc() && !travelDocDataACS.getTravelDoc().trim().isEmpty()) {
                                            if (travelDocDataACS.getTravelDoc().contains(msg)) {
                                                return true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception ex) {
            //
        }

        return false;
    }

    /**
     * @param msg
     * @param acsCheckInPassengerRSACSList
     * @param warningCollection
     * @return
     */
    public static boolean searchInAllBoardingPasses(
            String msg,
            List<ACSCheckInPassengerRSACS> acsCheckInPassengerRSACSList,
            WarningCollection warningCollection
    ) {
        boolean added = false;
        boolean isInAll = true;

        if (null == msg || msg.trim().isEmpty()) {
            return isInAll;
        }

        try {
            for (ACSCheckInPassengerRSACS aCSCheckInPassengerRSACS : acsCheckInPassengerRSACSList) {

                List<ItineraryPassengerACS> itineraryPassengerACSList = aCSCheckInPassengerRSACS.getItineraryPassengerList().getItineraryPassenger();

                if (null != itineraryPassengerACSList && !itineraryPassengerACSList.isEmpty()) {
                    for (ItineraryPassengerACS itineraryPassengerACS : itineraryPassengerACSList) {
                        List<PassengerDetailACS> passengerDetailACSList = itineraryPassengerACS.getPassengerDetailList().getPassengerDetail();

                        if (null != passengerDetailACSList && !passengerDetailACSList.isEmpty()) {
                            for (PassengerDetailACS passengerDetailACS : passengerDetailACSList) {
                                List<TravelDocDataACS> travelDocDataACSList = passengerDetailACS.getTravelDocDataList().getTravelDocData();

                                if (null != travelDocDataACSList && !travelDocDataACSList.isEmpty()) {
                                    for (TravelDocDataACS travelDocDataACS : travelDocDataACSList) {
                                        if (null != travelDocDataACS.getTravelDoc() && !travelDocDataACS.getTravelDoc().trim().isEmpty()) {
                                            if (travelDocDataACS.getTravelDoc().contains(msg)) {

                                                if (!added) {
                                                    Warning warning = new Warning(
                                                            ErrorType.NOT_VALID_WITHOUT_FLIGHT_COUPON.getErrorCode(),
                                                            ErrorType.NOT_VALID_WITHOUT_FLIGHT_COUPON.getFullDescription()
                                                    );
                                                    warningCollection.addToList(warning);
                                                    added = true;
                                                }

                                            } else {
                                                isInAll = false;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception ex) {
            //
        }

        return isInAll;
    }

    /**
     * @param cardId
     * @param cards
     * @return
     */
    public static CreditCard getCreditCard(Integer cardId, List<CreditCard> cards) {
        if (null == cards || cards.isEmpty()) {
            return null;
        }

        for (CreditCard card : cards) {
            if (cardId == card.getId()) {
                return card;
            }
        }
        return null;
    }

    /**
     * @param current
     * @param creditCard
     */
    public static void changePaymentData(UatpPaymentRequest current, CreditCard creditCard) {
        current.setEmail(creditCard.getEmail());
        current.setCardCode(creditCard.getCardCode());
        current.setCardCode(creditCard.getCardNumber());
        current.setExpiryDate(creditCard.getExpirationDate());
    }

    /**
     * @param current
     * @param creditCard
     */
    public static void changePaymentData(CreditCardPaymentRequest current, CreditCard creditCard) {
        current.setCardHolderName(creditCard.getPrintedName());

        if (null != creditCard.getPhone()) {
            current.setPhone(creditCard.getPhone());
        }

        if (null != creditCard.getBillingAddress()) {
            current.setBillingAddress(changeAddress(creditCard.getBillingAddress()));
        }
        current.setEmail(creditCard.getEmail());
        if (creditCard.getCardType() == null && creditCard.getCardCode() != null) {
            switch (creditCard.getCardCode()) {
                case "CA":
                    current.setCardType(CreditCardType.MASTER);
                    break;
                case "VI":
                    current.setCardType(CreditCardType.VISA);
                    break;
                case "AX":
                default:
                    current.setCardType(CreditCardType.AMEX);
                    break;
            }
        } else if (creditCard.getCardType() != null) {
            current.setCardType(CreditCardType.getType(creditCard.getCardType()));
        }
        current.setCardCode(creditCard.getCardCode());
        current.setCcNumber(creditCard.getCardNumber());
        current.setExpiryDate(creditCard.getExpirationDate());

    }

    /**
     * @param address
     * @return
     */
    public static Address changeAddress(com.aeromexico.commons.clubpremier.model.Address address) {
        Address newAddress = new Address();
        newAddress.setAddressOne(address.getDestinationAddress1());
        newAddress.setAddressTwo(address.getDestinationAddress2());

        String addr2 = "";
        if (null != address.getDestinationAddress2()) {
            addr2 += address.getDestinationAddress2();
        }

        if (null != address.getDestinationAddress3()) {
            if (!addr2.isEmpty()) {
                addr2 += " ";
            }

            addr2 += address.getDestinationAddress3();
        }

        newAddress.setAddressTwo(addr2);

        newAddress.setCity(address.getCity());
        newAddress.setCountry(address.getCountryCode());
        newAddress.setState(address.getStateCode());
        newAddress.setZipCode(address.getZipCode());

        return newAddress;
    }

    public static void setCardCodeByCardPrefix(CreditCardPaymentRequest creditCardPaymentRequest) {
        if (creditCardPaymentRequest.getCcNumber().startsWith("5")) {
            creditCardPaymentRequest.setCardType(CreditCardType.MASTER);
            creditCardPaymentRequest.setCardCode(CreditCardType.MASTER.getCardCode());
        } else if (creditCardPaymentRequest.getCcNumber().startsWith("3")) {
            creditCardPaymentRequest.setCardType(CreditCardType.AMEX);
            creditCardPaymentRequest.setCardCode(CreditCardType.AMEX.getCardCode());
        } else {
            creditCardPaymentRequest.setCardType(CreditCardType.VISA);
            creditCardPaymentRequest.setCardCode(CreditCardType.VISA.getCardCode());
        }
    }

}
