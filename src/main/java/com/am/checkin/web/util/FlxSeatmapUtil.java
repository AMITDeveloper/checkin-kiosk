//package com.am.checkin.web.util;
//
//import com.aeromexico.commons.model.BookedLeg;
//import com.aeromexico.commons.model.BookedSegment;
//import com.aeromexico.commons.model.Itinerary;
//import static com.aeromexico.commons.seatmaps.util.ConstantSeatMap.AIRLINE_CODE_AEROMEXICO;
//import static com.aeromexico.commons.seatmaps.util.ConstantSeatMap.COUCH_CABIN_CLASS;
//import com.aeromexico.farelogix.commons.utils.DatePrototype;
//import com.farelogix.flx.servicelistrq.Flight;
//import com.farelogix.flx.servicelistrq.FlightNumberWithSuffixType;
//import java.util.ArrayList;
//import java.util.List;
//
///**
// *
// * @author adrianleal
// */
//public class FlxSeatmapUtil {
//    //The RQ is formed for the seating service call
//    public static List<Flight> getSeatAvailabilityFlightListRQ(BookedLeg bookedLeg) throws Exception {
//        //Request Flight
//        List<Flight> listFlights = new ArrayList<>();
//        for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {
//            listFlights.add(fillRQ(bookedSegment.getItinerary(), COUCH_CABIN_CLASS));//Add the RQ of the cabin COACH
//        }
//        return listFlights;
//    }
//    
//    protected Flight fillRQ(Itinerary segment, String classOfService) throws Exception {
//        Flight flight = new Flight();
//        
//        
//        
//        
//        String[] arrayDeparture;
//        String[] arrayArrival;
//        Flight.AirlineCode airlineCode = new Flight.AirlineCode();
//        airlineCode.setValue(AIRLINE_CODE_AEROMEXICO);
//        FlightNumberWithSuffixType flightNumberWithSuffixType = new FlightNumberWithSuffixType();
//        flightNumberWithSuffixType.setValue(Integer.parseInt(segment.getOperatingFlightCode()));
//        flight.setAirlineCode(airlineCode);
//        flight.setFlightNumber(flightNumberWithSuffixType);
//        flight.setClassOfService(classOfService);
//        arrayDeparture = segment.getDepartureDateTime().split("T");
//        arrayArrival = segment.getArrivalDateTime().split("T");
//        flight.setDepartureCityCode(segment.getDepartureAirport());
//        flight.setDepartureDate(DatePrototype.getDate(arrayDeparture[0]));
//        flight.setDepartureTime(arrayDeparture[1]);
//        flight.setArrivalCityCode(segment.getArrivalAirport());
//        flight.setArrivalDate(DatePrototype.getDate(arrayArrival[0]));
//        flight.setArrivalTime(arrayArrival[1]);
//        return flight;
//    }
//}
