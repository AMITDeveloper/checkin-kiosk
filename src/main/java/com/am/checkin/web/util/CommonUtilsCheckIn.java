
/**
 * 
 */
package com.am.checkin.web.util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeromexico.commons.model.Segment;
import com.sabre.services.acs.bso.checkinpassenger.v3.ItineraryACS;

import java.util.List;

/**
 * @author jclun
 *
 */
public class CommonUtilsCheckIn {

	private static final Logger LOG = LoggerFactory.getLogger(CommonUtilsCheckIn.class);

//	public static String getMarketingAirline(FlightSegment flightSegment) {
//
//		String marketingAirline = null;
//
//		MarketingAirline mktAirline = flightSegment.getMarketingAirline();
//		if (mktAirline != null) {
//			marketingAirline = mktAirline.getCode();
//		} else {
//			marketingAirline = "";
//		}
//
//		return marketingAirline;
//	}

//	public static OperatingAirline getOperatingAirline(FlightSegment flightSegment) {
//
//		OperatingAirline operatingAirline = null;
//
//		List<OperatingAirline> operatingAirlineList = flightSegment.getOperatingAirline();
//		if (operatingAirlineList != null && operatingAirlineList.size() > 0) {
//			operatingAirline = operatingAirlineList.get(0);
//		}
//
//		return operatingAirline;
//	}
	
	/**
	 *
	 * @param legCode
	 * @return
	 *
	 * 		Expected format of legCode: 0 1 2 3 DFW_AM_2683_2016-03-04
	 */
	public static ItineraryACS transformItinerary(String legCode) throws Exception {

		if (legCode == null) {
			throw new Exception("Invalid LegCode");
		}

		try {

			String[] parts = legCode.split("_");

			ItineraryACS itineraryRQACS = new ItineraryACS();
			itineraryRQACS.setOrigin(parts[0]);
			itineraryRQACS.setAirline(parts[1]);
			itineraryRQACS.setFlight(parts[2]);
			itineraryRQACS.setDepartureDate(parts[3]);

			return itineraryRQACS;

		} catch (Exception ex) {
			throw ex;
		}

	}

	/**
	 * Validation flag by arrival
	 */
	public static boolean isDestinationValid(List<String> airportsCodeOvs, String arrivalAirport){
		Boolean isFlagDestinationValid = false;
		if(null != airportsCodeOvs && !airportsCodeOvs.isEmpty()) {
			for(String aip: airportsCodeOvs){
				if(aip.equalsIgnoreCase(arrivalAirport)){
					LOG.info("CONVERTION_TRUE");
					isFlagDestinationValid = true;
				}
			}
		}
        return isFlagDestinationValid;
    }
	
	/**
	 * 
	 */
	public static boolean isRestrictionByDepartureArrival(String departureArrivalRestriction, Segment segment) {
		String []restrictions = null;
		String []aux = null;
		String []restrictionsSegment = null;
		int i, total;
		if (departureArrivalRestriction != null && departureArrivalRestriction != "") {
			try {
				restrictions = departureArrivalRestriction.split(";");
				//departureArrivalRestriction=segment:MEX-OAX,CUN-OAX;departure:MEX,CUN,MTY;arrival:MEX
				aux = restrictions[0].split(":");
				restrictionsSegment = aux[1].split(",");
				total = restrictionsSegment.length;
				//by segment
				for (i = 0; i < total; i++) {
					String []departureArrival = restrictionsSegment[i].split("-");
					if (departureArrival[0].equalsIgnoreCase(segment.getDepartureAirport())
							&& departureArrival[1].equalsIgnoreCase(segment.getArrivalAirport())) {
						return true;
					}
				}
				//by origin
				aux = restrictions[1].split(":");
				String []restrictionsDeparture = aux[1].split(",");
				total = restrictionsSegment.length;
				for (i = 0; i < total; i++) {
					if (restrictionsDeparture[i].equalsIgnoreCase(segment.getDepartureAirport())) {
						return true;
					}
				}
				//by arrival
				aux = restrictions[2].split(":");
				String []restrictionsArrival = aux[1].split(",");
				total = restrictionsArrival.length;
				for (i = 0; i < total; i++) {
					if (restrictionsArrival[i].equalsIgnoreCase(segment.getArrivalAirport())) {
						return true;
					}
				}
			} catch (Exception ex) {
				
			}
		}
		return false;
	}

}
