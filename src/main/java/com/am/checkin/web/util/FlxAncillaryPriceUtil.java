package com.am.checkin.web.util;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.Ancillary;
import com.aeromexico.commons.model.AncillaryOffer;
import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.CurrencyValue;
import com.aeromexico.commons.model.FlxPriceWrapper;
import com.aeromexico.commons.model.TravelerAncillary;
import com.aeromexico.commons.model.TravelerAncillaryBaseProps;
import com.aeromexico.commons.web.types.AncillaryPostBookingType;
import com.aeromexico.commons.web.types.ErrorType;
import com.farelogix.flx.servicelistrs.Service;
import com.farelogix.flx.servicelistrs.ServiceListRS;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author adrianleal
 */
public class FlxAncillaryPriceUtil {

    private static final Logger LOG = LoggerFactory.getLogger(FlxAncillaryPriceUtil.class);

    public static List<BookedSegment> getSegmentsWithAncillaries(
            List<TravelerAncillaryBaseProps> travelerAncillaryBasePropsList,
            BookedLeg bookedLeg
    ) {
        List<BookedSegment> openBookedSegments = new ArrayList<>();
        for (TravelerAncillaryBaseProps travelerAncillaryBaseProps : travelerAncillaryBasePropsList) {
            if (travelerAncillaryBaseProps instanceof TravelerAncillary) {
                TravelerAncillary travelerAncillary = (TravelerAncillary) travelerAncillaryBaseProps;

                if (null != travelerAncillary.getSegmentCodeAux()) {

                    String segmentCode = travelerAncillary.getSegmentCodeAux();

                    BookedSegment bookedSegment = bookedLeg.getBookedSegment(segmentCode);

                    if (null != bookedSegment && "AM".equalsIgnoreCase(bookedSegment.getSegment().getOperatingCarrier())) {
                        openBookedSegments.add(bookedSegment);
                    }
                }
            } else if (travelerAncillaryBaseProps instanceof AncillaryOffer) {
                AncillaryOffer ancillaryOffer = (AncillaryOffer) travelerAncillaryBaseProps;

                if (null != ancillaryOffer.getSegmentCodeAux()) {

                    String segmentCode = ancillaryOffer.getSegmentCodeAux();

                    BookedSegment bookedSegment = bookedLeg.getBookedSegment(segmentCode);

                    if (null != bookedSegment && "AM".equalsIgnoreCase(bookedSegment.getSegment().getOperatingCarrier())) {
                        openBookedSegments.add(bookedSegment);
                    }
                }
            }
        }
        return openBookedSegments;
    }

    /**
     * Get price per sections from Farelogix
     *
     * @param segmentReferences
     * @param serviceListRS
     * @param currencyCode
     * @param nameRef
     * @return
     * @throws java.lang.Exception
     */
    public static Map<String, Map<String, FlxPriceWrapper>> getFLXPricesPerSegment(
            Map<String, String> segmentReferences,
            ServiceListRS serviceListRS,
            String currencyCode,
            String nameRef
    ) throws Exception {

        Map<String, Map<String, FlxPriceWrapper>> pricePerSegmentCodeList = new HashMap<>();

        for (Map.Entry<String, String> entry : segmentReferences.entrySet()) {
            String segmentCode = entry.getKey();
            String segmentRef = entry.getValue();

            Map<String, FlxPriceWrapper> pricesPerSection = getPricesPerAncillary(
                    serviceListRS,
                    segmentRef,
                    nameRef,
                    currencyCode
            );

            if (null != pricesPerSection && !pricesPerSection.isEmpty()) {
                pricePerSegmentCodeList.put(segmentCode, pricesPerSection);
            }
        }

        return pricePerSegmentCodeList;
    }

    private static Map<String, FlxPriceWrapper> getPricesPerAncillary(
            ServiceListRS serviceListRS,
            String segmentRef,
            String nameRef,
            String currencyCode
    ) {
        Map<String, FlxPriceWrapper> prices = new HashMap<>();

        if (null != serviceListRS.getOptionalServices()
                && null != serviceListRS.getOptionalServices().getService()
                && !serviceListRS.getOptionalServices().getService().isEmpty()) {

            List<Service> services = FlxServiceListUtil.getServices(
                    serviceListRS.getOptionalServices(), segmentRef
            );

            if (null != services) {
                for (Service service : services) {
                    if (nameRef.equalsIgnoreCase(service.getTravelerIDRef().getValue())) {
                        if ("Surcharge".equalsIgnoreCase(service.getType()) && null != service.getSubCode()) {

                            if (!currencyCode.equalsIgnoreCase(serviceListRS.getOptionalServices().getCurrencyCode().getValue())) {
                                throw new GenericException(javax.ws.rs.core.Response.Status.BAD_REQUEST, ErrorType.CURRENCY_CODE_DOES_NOT_MATCH_PCC, "Currency code does not match pcc");
                            }

                            AncillaryPostBookingType ancillaryPostBookingType = AncillaryPostBookingType.getType(service.getSubCode());

                            if (null != ancillaryPostBookingType && !prices.containsKey(service.getSubCode())) {
                                FlxPriceWrapper flxSeatWrapper = FlxServiceListUtil.getCurrencyValue(
                                        serviceListRS.getOptionalServices().getCurrencyCode(),
                                        service.getServicePrice(),
                                        service.getServicePriceCalc()
                                );

                                prices.put(service.getSubCode(), flxSeatWrapper);
                            }

                        } else if ("Included".equalsIgnoreCase(service.getType()) && null != service.getSubCode()) {

                            if (!currencyCode.equalsIgnoreCase(serviceListRS.getOptionalServices().getCurrencyCode().getValue())) {
                                throw new GenericException(javax.ws.rs.core.Response.Status.BAD_REQUEST, ErrorType.CURRENCY_CODE_DOES_NOT_MATCH_PCC, "Currency code does not match pcc");
                            }

                            AncillaryPostBookingType ancillaryPostBookingType = AncillaryPostBookingType.getType(service.getSubCode());

                            if (null != ancillaryPostBookingType && !prices.containsKey(service.getSubCode())) {
                                FlxPriceWrapper flxSeatWrapper = FlxServiceListUtil.getCurrencyValue(
                                        serviceListRS.getOptionalServices().getCurrencyCode(),
                                        service.getServicePrice(),
                                        service.getServicePriceCalc()
                                );

                                prices.put(service.getSubCode(), flxSeatWrapper);
                            }
                        }
                    }
                }
            }
        }

        return prices;
    }

    /**
     * @param flxCurrencyValue
     * @param ancillary
     * @return
     */
    public static boolean isFlxPriceDifferentThanAmPrice(
            CurrencyValue flxCurrencyValue,
            Ancillary ancillary
    ) {
        if (null != ancillary.getCurrency()) { //const will be zero if unavailable

            CurrencyValue currencyValue = ancillary.getCurrency();

            boolean isDifferent = isDifferentPrice(flxCurrencyValue, currencyValue);

            LOG.info("CURRENCY FLX: " + flxCurrencyValue.getTotal() + " " + flxCurrencyValue.getCurrencyCode() + " CURRENCY AM : " + currencyValue.getTotal() + " " + currencyValue.getCurrencyCode() + " " + ancillary.getType());

            return isDifferent;
        }

        return true;
    }

    private static boolean isDifferentPrice(CurrencyValue flxCurrencyValue, CurrencyValue currencyValue) {
        boolean isDifferent = true;
        if (flxCurrencyValue.getTotal().compareTo(currencyValue.getTotal()) == 0) {
            isDifferent = false;
        }
        return isDifferent;
    }
}
