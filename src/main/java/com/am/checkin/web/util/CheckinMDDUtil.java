package com.am.checkin.web.util;

import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.BookingClass;
import com.aeromexico.commons.model.CreditCardPaymentRequest;
import com.aeromexico.commons.model.Phone;
import com.aeromexico.commons.model.Segment;
import com.aeromexico.commons.web.types.PhoneType;
import com.aeromexico.commons.web.util.ListsUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 *
 * @author adrian
 */
public class CheckinMDDUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(CheckinMDDUtil.class);

    private CheckinMDDUtil() {
    }

    public static String getThirdPartyBooking(
            List<BookedTraveler> bookedTravelersRequested,
            CreditCardPaymentRequest creditCard
    ) {
        String thirdPartyBooking = "Y";

        String holderName = (null != creditCard && creditCard.getCardHolderName() != null)
                ? creditCard.getCardHolderName().trim() : "";

        if (null != bookedTravelersRequested) {
            for (BookedTraveler bookedTraveler : bookedTravelersRequested) {
                String firstName = bookedTraveler.getFirstName().trim();
                String lastName = bookedTraveler.getLastName().trim();

                if (StringUtils.containsIgnoreCase(holderName, firstName)
                        && StringUtils.containsIgnoreCase(holderName, lastName)) {
                    thirdPartyBooking = "N";
                    break;
                }
            }
        }

        return thirdPartyBooking;
    }

    public static String getDepartureCity(List<BookedLeg> bookedLegList) {
        try {
            BookedLeg bookedLeg = ListsUtil.getFirst(bookedLegList);
            BookedSegment bookedSegment = ListsUtil.getFirst(bookedLeg.getSegments().getCollection());
            return bookedSegment.getSegment().getDepartureAirport();
        } catch (Exception ex) {
            LOGGER.info("Error getting departure city: " + ex.getMessage());
            return "";
        }
    }

    public static String getFinalDestination(List<BookedLeg> bookedLegList) {
        try {
            BookedLeg bookedLeg = ListsUtil.getLast(bookedLegList);
            BookedSegment bookedSegment = ListsUtil.getLast(bookedLeg.getSegments().getCollection());
            return bookedSegment.getSegment().getArrivalAirport();
        } catch (Exception ex) {
            LOGGER.info("Error getting final destination: " + ex.getMessage());
            return "";
        }
    }

    public static String getFullItinerary(List<BookedLeg> bookedLegList) {
        String fullItinerary = "";

        if (null != bookedLegList) {
            for (BookedLeg bookedLeg : bookedLegList) {
                for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {

                    fullItinerary += bookedSegment.getSegment().getDepartureAirport();
                    fullItinerary += "-";
                    fullItinerary += bookedSegment.getSegment().getArrivalAirport();
                    fullItinerary += ":";
                }
            }

            return fullItinerary.substring(0, fullItinerary.length() - 1);
        } else {
            return "";
        }

    }

    public static String getOneWayRoundTrip(List<BookedLeg> bookedLegList) {
        if (null == bookedLegList) {
            return "YES";
        }

        return (bookedLegList.size() > 1) ? "NO" : "YES";
    }

    public static String getFQTVNumber(BookedTraveler bookedTraveler) {
        String fQTVNumber = "";
        if (null != bookedTraveler && null != bookedTraveler.getFrequentFlyerNumber()) {
            fQTVNumber = bookedTraveler.getFrequentFlyerNumber();
        }

        return fQTVNumber;
    }

    public static String getFQTVStatus(BookedTraveler bookedTraveler) {
        String fQTVStatus = "";
        if (null != bookedTraveler && null != bookedTraveler.getFrequentFlyerProgram()) {
            fQTVStatus = bookedTraveler.getFrequentFlyerProgram();
        }

        return fQTVStatus;
    }

    public static String getNumOfPaxs(List<BookedTraveler> bookedTravelersRequested) {
        if (null == bookedTravelersRequested) {
            return "0";
        }
        return bookedTravelersRequested.size() > 0 ? Integer.toString(bookedTravelersRequested.size()) : "0";
    }

    public static int getHoursTillDeparture(String transactionTime, Segment segment) {

        SimpleDateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        try {
            Date fechaCompra = FORMATTER.parse(transactionTime);
            Date fechaVuelo = FORMATTER.parse(segment.getDepartureDateTime());
            Calendar cal1 = Calendar.getInstance();
            Calendar cal2 = Calendar.getInstance();
            cal1.setTime(fechaCompra);
            cal2.setTime(fechaVuelo);

            long milis1 = cal1.getTimeInMillis();
            long milis2 = cal2.getTimeInMillis();
            long diff = milis2 - milis1;
            long diffHours = diff / (60 * 60 * 1000);

            return (int) diffHours;
        } catch (Exception e) {
            LOGGER.info("Error getting HoursTillDeparture: " + e.getMessage());
            return 0;
        }
    }

    public static int getDaysTillDeparture(String transactionTime, Segment segment) {
        SimpleDateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        try {
            Date fechaCompra = FORMATTER.parse(transactionTime);
            Date fechaVuelo = FORMATTER.parse(segment.getDepartureDateTime());

            Calendar cal1 = Calendar.getInstance();
            Calendar cal2 = Calendar.getInstance();
            cal1.setTime(fechaCompra);
            cal2.setTime(fechaVuelo);

            long milis1 = cal1.getTimeInMillis();
            long milis2 = cal2.getTimeInMillis();
            long diff = milis2 - milis1;
            long diffDays = diff / (24 * 60 * 60 * 1000);

            return (int) diffDays;
        } catch (Exception e) {
            LOGGER.info("Error getting DaysTillDeparture: " + e.getMessage());
            return 0;
        }
    }

    public static String getClassService(BookedTraveler bookedTraveler, Segment segment) {
        try {
            BookingClass bookingClass = bookedTraveler.getBookingClasses().getBookingClass(segment.getSegmentCode());

            return bookingClass != null ? bookingClass.getBookingClass() : "";
        } catch (Exception ex) {
            LOGGER.info("Error getting class of service: " + ex.getMessage());
            return "";
        }
    }

    public static String getDayOfFlight(String transactionTime) {
        SimpleDateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        try {
            Date fechaCompra = FORMATTER.parse(transactionTime);
            Calendar cal = Calendar.getInstance();
            cal.setTime(fechaCompra);
            String dayOfFlight = cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.US).toUpperCase();
            return dayOfFlight;
        } catch (Exception e) {
            LOGGER.info("Error getting DayOfFlight: " + e.getMessage());
            return "";
        }
    }

    public static int getWeekOfYear(String transactionTime) {
        SimpleDateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        try {
            Date fechaCompra = FORMATTER.parse(transactionTime);
            Calendar cal = Calendar.getInstance();
            cal.setFirstDayOfWeek(Calendar.SUNDAY);
            cal.setTime(fechaCompra);
            int weekOfYear = cal.get(Calendar.WEEK_OF_YEAR);

            return weekOfYear;
        } catch (Exception e) {
            LOGGER.info("Error getting WeekOfYear: " + e.getMessage());
            return 0;
        }
    }

    public static String getChannelOfBoking(String storeCode) {
        String channelOfBoking = "SHARE KIOSKS";
        return channelOfBoking;
    }

    public static String getBinNumber(String ccNumber) {

        if (ccNumber != null && ccNumber.length() >= 6) {
            return ccNumber.substring(0, 6);
        }

        return "";
    }

    public static List<String> getPassengerMobilePhones(
            List<BookedTraveler> bookedTravelersRequested,
            PhoneType phoneType
    ) {
        List<String> phonesList = new ArrayList<>();

        for (BookedTraveler bookedTraveler : bookedTravelersRequested) {

            if (null != bookedTraveler.getPhones() && null != bookedTraveler.getPhones().getCollection()) {

                List<Phone> phones = bookedTraveler.getPhones().getCollection();
                for (Phone phone : phones) {
                    if (phone != null && null != phoneType) {
                        if (phoneType.equals(phone.getType())) {
                            phonesList.add(phone.getNumber().trim());
                        }
                    } else if (phone != null) {
                        phonesList.add(phone.getNumber().trim());
                    }
                }
            }
        }

        return phonesList;
    }

    public static List<String> getPassengerEmails(
            List<BookedTraveler> bookedTravelersRequested
    ) {
        List<String> emailsList = new ArrayList<>();

        if (null != bookedTravelersRequested) {
            for (BookedTraveler bookedTraveler : bookedTravelersRequested) {
                String email = bookedTraveler.getEmail();
                if (email != null && !email.trim().isEmpty()) {
                    emailsList.add(email.trim());
                }
            }
        }

        return emailsList;
    }

    public static String getPNRCreationDate(String transactionTime) {
        SimpleDateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("ddMMyy");
            Date date = FORMATTER.parse(transactionTime);
            return simpleDateFormat.format(date);
        } catch (Exception e) {
            LOGGER.error("Error getting FechaCreacionPNR: " + e);
            return "";
        }

    }

}
