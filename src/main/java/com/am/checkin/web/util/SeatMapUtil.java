package com.am.checkin.web.util;

import com.aeromexico.commons.model.AbstractSeatMap;
import com.aeromexico.commons.model.SeatMap;
import com.aeromexico.commons.model.SeatmapCollection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeromexico.commons.model.SeatmapRow;
import com.aeromexico.commons.model.SeatmapSeat;
import com.aeromexico.commons.model.SeatmapSection;
import com.aeromexico.commons.model.Segment;
import com.aeromexico.commons.web.types.BookedLegCheckinStatusType;
import com.aeromexico.commons.web.types.PosType;
import com.aeromexico.commons.web.types.SeatSectionCodeType;
import com.aeromexico.commons.web.util.FlightNumberUtil;
import com.aeromexico.commons.web.util.SegmentCodeUtil;
import com.aeromexico.sharedservices.util.BrandedFareRulesUtil;
import com.sabre.services.acs.bso.checkinpassenger.v3.ACSCheckInPassengerRSACS;
import com.sabre.services.stl.v3.FlightSeatMapDetailACS.SeatMapDetail;
import com.sabre.services.stl.v3.FlightSeatMapInfoACS;
import com.sabre.services.stl.v3.RowDetailListACS.Row;
import com.sabre.services.stl.v3.SeatDetailACS;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

/**
 *
 * @author jeortiz@aeromexico.com | Aeromexico
 *
 */
@Named
@ApplicationScoped
public class SeatMapUtil {

    private static final Logger LOG = LoggerFactory.getLogger(BrandedFareRulesUtil.class);

    private static final List<String> FREE_SEAT_CODES = Arrays.asList(
            "*", "-"
    );

    public static List<SeatmapSection> getSeatMapSectionList(SeatmapCollection seatmapCollection, String segmentCode) {

        if (null == seatmapCollection || null == seatmapCollection.getCollection() || null == segmentCode) {
            return null;
        }

        for (AbstractSeatMap abstractSeatMap : seatmapCollection.getCollection()) {
            if (abstractSeatMap instanceof SeatMap) {
                SeatMap seatMap = (SeatMap) abstractSeatMap;
                if (null != seatMap.getSegment()
                        && SegmentCodeUtil.compareSegmentCodeWithoutTime(segmentCode, seatMap.getSegment().getSegmentCode())) {
                    return seatMap.getSections().getCollection();
                }
            }
        }

        return null;
    }

    /**
     *
     * @param checkInPassengerRS
     * @param numPasajeros
     * @param seatmapSectionList
     * @param pos
     * @return
     * @throws Exception
     */
    public static Map<String, List<String>> getSeatMaps(
            ACSCheckInPassengerRSACS checkInPassengerRS,
            List<SeatmapSection> seatmapSectionList,
            int numPasajeros,
            String pos
    ) throws Exception {
        List<SeatSectionCodeType> validSectionList = new ArrayList<>();
        validSectionList.add(SeatSectionCodeType.COACH);

        if (PosType.KIOSK.toString().equalsIgnoreCase(pos)) {
            validSectionList.add(SeatSectionCodeType.EXIT_ROW);
            validSectionList.add(SeatSectionCodeType.PREFERRED);
        }

        LOG.info("getting seatmap for " + numPasajeros + " passengers");
        Map<String, List<String>> seatFound = new HashMap<>();

        int countPasajeros = 0;

        if (checkInPassengerRS.getFlightSeatMapInfoList() != null || !checkInPassengerRS.getFlightSeatMapInfoList().isEmpty()) {
            for (FlightSeatMapInfoACS flightSeatMapInfoACS : checkInPassengerRS.getFlightSeatMapInfoList()) {
                if (BookedLegCheckinStatusType.OPENCI.toString().equals(flightSeatMapInfoACS.getFlightInfo().getStatus())) {
                    List<String> seatsFoundList = new ArrayList<>();
                    List<SeatMapDetail> seatMapDetailList = flightSeatMapInfoACS.getFlightSeatMapDetail().getSeatMapDetail();

                    Collections.reverse(seatMapDetailList);

                    for (SeatMapDetail seatMapDetail : seatMapDetailList) {
                        if (seatMapDetail.getSeatDetail() != null || !seatMapDetail.getSeatDetail().isEmpty()) {
                            for (SeatDetailACS seatDetailACS : seatMapDetail.getSeatDetail()) {
                                if (seatDetailACS.getRowDetailList() != null && !seatDetailACS.getRowDetailList().getRow().isEmpty()) {
                                    for (Row row : seatDetailACS.getRowDetailList().getRow()) {
                                        if (row.getColumn() != null || row.getColumn().isEmpty()) {
                                            for (com.sabre.services.stl.v3.RowDetailListACS.Row.Column column : row.getColumn()) {
                                                if (FREE_SEAT_CODES.contains(column.getBlockCode())) {
                                                    String seatCodeAvailable = row.getNumber() + column.getLetter();

                                                    SeatSectionCodeType seatSectionCodeType = isSectionSeatValid(
                                                            seatCodeAvailable,
                                                            getMapFromSeatmapCollection(seatmapSectionList, validSectionList),
                                                            validSectionList
                                                    );

                                                    if (null != seatSectionCodeType) {
                                                        seatsFoundList.add(seatCodeAvailable);
                                                        countPasajeros++;
                                                    }

                                                    if (countPasajeros == numPasajeros) {
                                                        seatFound.put(
                                                                flightSeatMapInfoACS.getFlightInfo().getOrigin() + "_"
                                                                + flightSeatMapInfoACS.getFlightInfo().getDestination(),
                                                                seatsFoundList);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return seatFound;
    }

    /**
     *
     * @param seatMapDetailList
     * @param classesOfService
     * @param validBlockCodes
     * @param seatmapSectionMap
     * @param validSectionList
     * @return
     */
    public static Queue<String> getQueueFromSeatMapDetailList(
            List<SeatMapDetail> seatMapDetailList,
            List<String> classesOfService,
            List<String> validBlockCodes,
            Map<SeatSectionCodeType, Map<String, SeatmapSeat>> seatmapSectionMap,
            List<SeatSectionCodeType> validSectionList
    ) {

        Map<SeatSectionCodeType, Queue<String>> queueMap = new HashMap<>();

        for (SeatSectionCodeType seatSectionCodeType : validSectionList) {
            queueMap.put(seatSectionCodeType, new LinkedList<>());
        }

        for (int i = seatMapDetailList.size() - 1; i >= 0; i--) {
            SeatMapDetail seatMapDetail = seatMapDetailList.get(i);

            if (classesOfService.contains(seatMapDetail.getClassOfService())) {
                for (int j = seatMapDetail.getSeatDetail().size() - 1; j >= 0; j--) {
                    SeatDetailACS seatDetailACS = seatMapDetail.getSeatDetail().get(j);

                    if (null != seatDetailACS.getRowDetailList() && null != seatDetailACS.getRowDetailList().getRow()) {
                        for (Row row : seatDetailACS.getRowDetailList().getRow()) {
                            if (null != row.getColumn()) {
                                for (Row.Column column : row.getColumn()) {
                                    if (validBlockCodes.contains(column.getBlockCode())) {
                                        String code = row.getNumber() + column.getLetter().toUpperCase();

                                        SeatSectionCodeType seatSectionCodeType = containSeatOn(
                                                code, seatmapSectionMap
                                        );

                                        if (null != seatSectionCodeType) {
                                            queueMap.get(seatSectionCodeType).add(code);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        //order the list in the incomming valid sections order
        Queue<String> queue = new LinkedList<>();

        for (SeatSectionCodeType seatSectionCodeType : validSectionList) {
            Queue<String> value = queueMap.get(seatSectionCodeType);

            queue.addAll(value);
        }

        return queue;
    }

    /**
     *
     * @param seatmapSectionList
     * @param validSectionListOrdered
     * @return
     */
    public static Map<SeatSectionCodeType, Map<String, SeatmapSeat>> getMapFromSeatmapCollection(
            List<SeatmapSection> seatmapSectionList, List<SeatSectionCodeType> validSectionListOrdered
    ) {

        Map<SeatSectionCodeType, Map<String, SeatmapSeat>> seatSectionsMap = new HashMap<>();

        Map<String, SeatmapSeat> seatsMap;

        for (SeatmapSection seatmapSection : seatmapSectionList) {

            if (validSectionListOrdered.contains(seatmapSection.getCode())) {
                seatsMap = seatSectionsMap.get(seatmapSection.getCode());

                if (null == seatsMap) {
                    seatsMap = new HashMap<>();

                    seatSectionsMap.put(seatmapSection.getCode(), seatsMap);
                }

                for (SeatmapRow SeatmapRow : seatmapSection.getRows().getCollection()) {
                    for (SeatmapSeat seatmapSeat : SeatmapRow.getSeats().getCollection()) {

                        seatsMap.put(seatmapSeat.getCode().toUpperCase(), seatmapSeat);
                    }
                }
            }
        }

        return seatSectionsMap;
    }

    /**
     *
     * @param seatSectionsMap
     * @param seatCodeAvailable
     * @param validSectionList
     * @return
     */
    private static SeatSectionCodeType isSectionSeatValid(
            String seatCodeAvailable,
            Map<SeatSectionCodeType, Map<String, SeatmapSeat>> seatSectionsMap,
            List<SeatSectionCodeType> validSectionList
    ) {
        if (null == seatSectionsMap
                || seatSectionsMap.isEmpty()
                || null == seatCodeAvailable
                || seatCodeAvailable.trim().isEmpty()
                || null == validSectionList
                || validSectionList.isEmpty()) {

            return null;
        }

        seatCodeAvailable = seatCodeAvailable.trim().toUpperCase();

        for (SeatSectionCodeType seatSectionCodeType : validSectionList) {
            if (seatSectionsMap.containsKey(seatSectionCodeType)) {
                if (seatSectionsMap.get(seatSectionCodeType).containsKey(seatCodeAvailable)) {
                    return seatSectionCodeType;
                }
            }
        }

        return null;

    }

    /**
     *
     * @param seatSectionsMap
     * @param seatCodeAvailable
     * @param validSectionList
     * @return
     */
    private static SeatSectionCodeType containSeatOn(
            String seatCodeAvailable,
            Map<SeatSectionCodeType, Map<String, SeatmapSeat>> seatSectionsMap
    ) {
        if (null == seatSectionsMap
                || seatSectionsMap.isEmpty()
                || null == seatCodeAvailable
                || seatCodeAvailable.trim().isEmpty()) {

            return null;
        }

        seatCodeAvailable = seatCodeAvailable.trim().toUpperCase();

        for (Map.Entry<SeatSectionCodeType, Map<String, SeatmapSeat>> entry : seatSectionsMap.entrySet()) {
            Map<String, SeatmapSeat> seatmapSeatMap = entry.getValue();

            if (seatmapSeatMap.containsKey(seatCodeAvailable)) {
                return entry.getKey();
            }

        }

        return null;
    }

    /**
     *
     * @param flightSeatMapInfoList
     * @param segment
     * @return FlightSeatMapInfoACS
     */
    public static FlightSeatMapInfoACS getSeatMapBySegment(
            List<FlightSeatMapInfoACS> flightSeatMapInfoList,
            Segment segment
    ) {

        if (null == flightSeatMapInfoList || flightSeatMapInfoList.isEmpty() || null == segment) {
            return null;
        }

        for (FlightSeatMapInfoACS flightSeatMapInfoACS : flightSeatMapInfoList) {

            if (null != flightSeatMapInfoACS.getFlightInfo()
                    && flightSeatMapInfoACS.getFlightInfo().getOrigin().equalsIgnoreCase(segment.getDepartureAirport())
                    && flightSeatMapInfoACS.getFlightInfo().getDestination().equalsIgnoreCase(segment.getArrivalAirport())
                    && (flightSeatMapInfoACS.getFlightInfo().getAirline().equalsIgnoreCase(segment.getOperatingCarrier())
                    || flightSeatMapInfoACS.getFlightInfo().getAirline().equalsIgnoreCase(segment.getMarketingCarrier()))
                    && (FlightNumberUtil.compareFlightNumbers(flightSeatMapInfoACS.getFlightInfo().getFlight(), segment.getOperatingFlightCode())
                    || FlightNumberUtil.compareFlightNumbers(flightSeatMapInfoACS.getFlightInfo().getFlight(), segment.getMarketingFlightCode()))) {

                return flightSeatMapInfoACS;

            }
        }

        return null;
    }

    /**
     * @return the FREE_SEAT_CODES
     */
    public static List<String> getFREE_SEAT_CODES() {
        return FREE_SEAT_CODES;
    }

}
