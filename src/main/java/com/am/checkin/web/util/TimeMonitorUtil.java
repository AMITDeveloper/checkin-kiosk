package com.am.checkin.web.util;

import com.aeromexico.sabre.api.session.common.ServiceCall;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adrian
 */
public class TimeMonitorUtil {

    private static final Logger LOG = LoggerFactory.getLogger(TimeMonitorUtil.class);

    /**
     *
     * @param serviceCallList
     * @param action
     * @param startTime
     */
    public static void addCallToMonitor(List<ServiceCall> serviceCallList, String action, long startTime) {
        if (null == serviceCallList) {
            serviceCallList = new ArrayList<>();
        }

        //This call should NOT cause this service to fail.
        try {
            serviceCallList.add(new ServiceCall(action, Calendar.getInstance().getTimeInMillis() - startTime));
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
    }
}
