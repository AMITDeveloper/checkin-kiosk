package com.am.checkin.web.util;

import com.aeromexico.codes.ICodesGenerator;
import com.aeromexico.codes.impl.CodesGenerator;
import com.aeromexico.commons.model.BaggageRoute;
import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.BookingClass;
import com.aeromexico.commons.model.Segment;
import com.aeromexico.commons.model.SegmentDocument;
import com.aeromexico.commons.model.SegmentDocumentList;
import com.aeromexico.commons.model.weather.AirportWeather;
import com.aeromexico.commons.web.types.AirlineCodeType;
import com.aeromexico.commons.web.util.TimeUtil;
import com.aeromexico.commons.web.util.ValidateCheckinIneligibleReason;
import com.am.checkin.web.v2.services.AirportService;

import java.text.ParseException;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adrian
 */
public class BoardingPassUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(BoardingPassUtil.class);
    private static final ICodesGenerator CODES_GENERATOR = new CodesGenerator();

    @Inject
    private static AirportService airportService;

    /**
     *
     * @param bookedTraveler
     * @param bookedSegment
     * @param recordlocator
     * @return
     */
    public static String getStandbyBoardingpassPectab(
            BookedTraveler bookedTraveler,
            BookedSegment bookedSegment,
            String recordlocator
    ) {

        String departuredate = "";
        String departuretime = "";
        String classofservice = "";
        String origin = "";
        String destination = "";
        String flightnumber = "";
        String operatingcarrier = "";

        if (null != bookedSegment) {
            Segment segment = bookedSegment.getSegment();

            departuredate = getDepartureDate(segment);

            departuretime = getDepartureTime(segment);

            classofservice = getClassOfService(bookedTraveler, segment);

            origin = getOrigin(segment);

            destination = getDestination(segment);

            flightnumber = getFlightNumber(segment);

            operatingcarrier = getOperatingCarrier(segment);
        }

        String control = "";//001
        String nameinpnr = "";//EBERLEY/NORINE
        String zone = "";//3
        String gate = "";//A8
        String seat = "STB";
        String eticket = "";//1392199567159
        String digitchecker = "";
        String operatedby = "AEROMEXICO";

        nameinpnr = getNameInPnr(bookedTraveler);

        eticket = getEticket(bookedTraveler);

        String travelDoc = getTravelDoc(nameinpnr, recordlocator, origin, destination, operatingcarrier, flightnumber, classofservice, bookedTraveler.getTicketNumber());

        String pectab = "CP^A^01B^CP^1C01^01W^021"
                + "^03NOMBRE/NAME"
                + "^06VUELO/FLIGHT"
                + "^07CLASE"
                + "^08FECHA/DATE"
                + "^09DE/FROM"
                + "^11OPERADO POR/OPERATED BY"
                + "^12CONTROL"
                + "^13A/TO"
                + "^14SALA/GATE"
                + "^18HORA/TIME"
                + "^21FQTV:"
                + "^31" + departuredate//DDMMM ex. 29APR
                + "^32"
                + "^34" + classofservice
                + "^37" + control
                + "^39"
                + "^40"
                + "^41"
                + "^42"
                + "^43"
                + "^44" + nameinpnr//EBERLEY/NORINE
                + "^45"
                + "^46"
                + "^47"
                + "^48"
                + "^50" + nameinpnr//EBERLEY/NORINE
                + "^60" + origin//CANCUN           
                + "^64" + flightnumber//540 
                + "^67" + operatingcarrier//AM 
                + "^70" + destination//MEXICO CITY      
                + "^80ZONA/ZONE"
                + "^81ZONA"
                + "^82" + zone//3       
                + "^B0ASIENTO"
                + "^B2" + seat//30B
                + "^B3"
                + "^C3-"
                + "^C4" + gate//A8 
                + "^C5-"
                + "^C6" + departuretime//01:30
                + "^G4ETICKET   "
                + "^I3" + eticket//1392199567159
                + "^I4" + digitchecker//5
                + "^K4" + operatedby//AEROMEXICO
                + "^P0"
                + "^P1"
                + "^P2"
                + travelDoc
                + "^P3"
                + travelDoc
                + "^"
                + "";

        return pectab;
    }

    private static String getEticket(BookedTraveler bookedTraveler) {
        try {
            String eticket = bookedTraveler.getTicketNumber();
            return eticket;
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        return "";
    }

    private static String getNameInPnr(BookedTraveler bookedTraveler) {
        try {
            String nameinpnr = (bookedTraveler.getLastName() + "/" + bookedTraveler.getFirstName()).toUpperCase();
            return nameinpnr;
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        return "";
    }

    private static String getOperatingCarrier(Segment segment) {
        try {
            String operatingcarrier = segment.getOperatingCarrier();
            return operatingcarrier;
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        return "";
    }

    private static String getFlightNumber(Segment segment) {
        try {
            String flightnumber = Integer.valueOf(segment.getOperatingFlightCode()).toString();
            return flightnumber;
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        return "";
    }

    private static String getDestination(Segment segment) {
        try {
            String destination = segment.getArrivalAirport();
            LOGGER.info("getDestination getCityNameByIATACode - ( " + destination.toUpperCase() + " )");
            try {
                AirportWeather airportWeather = airportService.getAirportFromIata(destination);
                if (null != airportWeather) {
                	return airportWeather.getAirport().getCity().toUpperCase();
                } else {
                	LOGGER.info("getDestination getCityNameByIATACode - returned null");
                }
            } catch (Exception e) {
            	LOGGER.info("getDestination getCityNameByIATACode - ( " + e.getMessage() + " )");
            }

            return destination;
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        return "";
    }

    private static String getOrigin(Segment segment) {
        try {
            String origin = segment.getDepartureAirport();
            LOGGER.info("getOrigin getCityNameByIATACode - ( " + origin.toUpperCase() + " )");
            try {
                AirportWeather airportWeather = airportService.getAirportFromIata(origin.toUpperCase());
                if (null != airportWeather) {
                	return airportWeather.getAirport().getCity().toUpperCase();
                } else {
                	LOGGER.info("getOrigin getCityNameByIATACode - returned null");
                }
            } catch (Exception e) {
            	LOGGER.info("getOrigin getCityNameByIATACode - ( " + e.getMessage() + " )");
            }

            return origin;
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        return "";
    }

    private static String getClassOfService(BookedTraveler bookedTraveler, Segment segment) {
        try {
            BookingClass bookingClass = bookedTraveler.getBookingClasses().getBookingClass(segment.getSegmentCode());
            String classofservice = bookingClass.getBookingClass();
            return classofservice;
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        return "";
    }

    private static String getDepartureTime(Segment segment) {
        try {
            String departuretime = TimeUtil.getStrTime(
                    segment.getDepartureDateTime(),
                    "yyyy-MM-dd'T'HH:mm:ss",
                    "HH:mm"
            );

            return departuretime;
        } catch (ParseException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        return "";
    }

    private static String getDepartureDate(Segment segment) {
        try {
            String departuredate = TimeUtil.getStrTime(
                    segment.getDepartureDateTime(),
                    "yyyy-MM-dd'T'HH:mm:ss",
                    "ddMMM"
            );

            return departuredate;
        } catch (ParseException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        return "";
    }

    /**
     *
     * @param nameinpnr
     * @param recordlocator
     * @param origin
     * @param destination
     * @param operatingcarrier
     * @param flightnumber
     * @param classofservice
     * @param ticketNumber
     * @return
     */
    private static String getTravelDoc(
            String nameinpnr,
            String recordlocator,
            String origin,
            String destination,
            String operatingcarrier,
            String flightnumber,
            String classofservice,
            String ticketNumber) {

        String travelDoc = "M1" 
        		+ ((nameinpnr.length() <= 21) ? StringUtils.leftPad(nameinpnr, 21, " ") : nameinpnr.substring(0, 21))
                + recordlocator + " "
                + origin + destination + operatingcarrier + " "
                + StringUtils.leftPad(flightnumber, 4, "0")
                + " 000" + classofservice + "000 0000 000" + "&lt;" + "0000  0000BAM"
                + StringUtils.rightPad("", 40, " ")
                + "2A"
                + StringUtils.rightPad(ticketNumber, 13, " ") + " 0"
                + StringUtils.rightPad("", 26, " ")
                + "N";

        return travelDoc;
    }

    public static void setStandByBoardingPasses(
            BookedTraveler bookedTraveler,
            BookedLeg bookedLeg,
            String recordlocator
    ) {
        bookedTraveler.setSegmentDocumentsList(new SegmentDocumentList());

        List<BookedSegment> bookedSegmentList = bookedLeg.getSegments().getSegmentsOperatedBy(AirlineCodeType.AM);

        int itinerary = 0;
        for (BookedSegment bookedSegment : bookedSegmentList) {
            itinerary++;

            if (null != bookedSegment) {
                String segmentCode = bookedSegment.getSegment().getSegmentCode();
                BaggageRoute baggageRoute;
                baggageRoute = PnrCollectionUtil.getBaggageRoute(bookedTraveler.getBaggageRouteList(), segmentCode);

                short segmentId;

                if (null != baggageRoute) {
                    segmentId = baggageRoute.getSegmentID();
                } else {
                    segmentId = (short) itinerary;
                }

                Segment segment = bookedSegment.getSegment();

                String departuredate = getDepartureDate(segment);

                String departuretime = getDepartureTime(segment);

                String classofservice = getClassOfService(bookedTraveler, segment);

                String origin = getOrigin(segment);

                String destination = getDestination(segment);

                String flightnumber = getFlightNumber(segment);

                String operatingcarrier = getOperatingCarrier(segment);

                String control = "";//001                
                String zone = "";//3
                String gate = "";//A8
                String seat = "STB";
                String digitchecker = "";
                String operatedby = "AEROMEXICO";

                String nameinpnr = getNameInPnr(bookedTraveler);

                String eticket = getEticket(bookedTraveler);

                String boardingPass = getTravelDoc(nameinpnr, recordlocator, origin, destination, operatingcarrier, flightnumber, classofservice, bookedTraveler.getTicketNumber());

                SegmentDocument segmentInfo = new SegmentDocument();
                try {
                    segmentInfo.setBarcode(boardingPass);
                    segmentInfo.setBarcodeImage(
                            CODES_GENERATOR.codesGenerator2D(boardingPass)
                    );
                    segmentInfo.setQrcodeImage(
                            CODES_GENERATOR.codesGeneratorQR(boardingPass)
                    );
                } catch (Exception ex) {
                    LOGGER.error(ex.getMessage());
                }

                segmentInfo.setSegmentId(segmentId);
                segmentInfo.setBoardingZone(zone);
                segmentInfo.setGate(gate);
                segmentInfo.setCheckInNumber("");
                segmentInfo.setSegmentCode(segmentCode);
                segmentInfo.setCheckinStatus(
                        true
                );
                segmentInfo.setTsaPreCheck(false);
                segmentInfo.setSkyPriority(false);
                bookedTraveler.getSegmentDocumentsList().addOrReplace(segmentInfo);
            }
        }
    }
}
