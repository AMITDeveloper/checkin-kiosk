package com.am.checkin.web.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URL;

/**
 *
 * @author adrian
 */
public class FileUtils {

    private static final String BASE_PATH = "/com/am/checkin";

    private static File getFileFromPath(String fileName) throws Exception {
        URL resource = FileUtils.class.getResource(BASE_PATH + fileName);
        URI uri = new URI(resource.toString());
        return new File(uri.getPath());
    }

    public static String readFileAsString(String filename) {
        try {
            File file = FileUtils.getFileFromPath(filename);

            String json = readFileAsString(file);
            return json;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static String readFileAsString(File file) throws Exception {

            FileInputStream fis = new FileInputStream(file);
            BufferedReader in = new BufferedReader(new InputStreamReader(fis, "ISO-8859-1"));
            StringBuilder strBuf = new StringBuilder();
        try {
            String buf = in.readLine();

            while (buf != null) {
                strBuf.append(buf.trim());
                buf = in.readLine();
            }

            try {
                in.close();
            } catch (Exception e) {
                //ignore exception
            }

            try {
                fis.close();
            } catch (Exception e) {
                //ignore exception
            }
            return strBuf.toString();
        }finally {
            in.close();
            fis.close();
        }


    }
}
