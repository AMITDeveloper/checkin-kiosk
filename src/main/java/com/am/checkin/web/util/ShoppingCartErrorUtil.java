package com.am.checkin.web.util;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.Warning;
import com.aeromexico.commons.model.WarningCollection;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.util.ErrorCodeDescriptions;
import com.sabre.services.acs.bso.editpassengercharacteristics.v3.ACSEditPassengerCharacteristicsRSACS;
import com.sabre.services.checkin.updatesecurityinfo.v4.UpdateSecurityInfoRSType;
import com.sabre.services.sp.pd.v3_3.PassengerDetailsRS;
import com.sabre.services.stl.v3.ResultRecord;
import com.sabre.services.stl.v3.SystemSpecificResults;
import com.sabre.services.stl_payload.v02_01.ProblemInformation;
import org.apache.commons.lang.StringUtils;
import org.drools.javaparser.utils.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;

public class ShoppingCartErrorUtil {

    private static final Logger log = LoggerFactory.getLogger(ShoppingCartErrorUtil.class);

    public static void sabreErrorPassengerSecurity(UpdateSecurityInfoRSType acsPassengerSecurityRSACS) {

        String msg = "";

        for (SystemSpecificResults systemSpecificResult : acsPassengerSecurityRSACS.getResult().getSystemSpecificResults()) {
            msg = msg + systemSpecificResult.getErrorMessage().getValue() + " | ";
        }

        if (!msg.isEmpty()) {
            msg = msg.substring(0, msg.length() - 3);
        }

        String passId = "";

        if (null != acsPassengerSecurityRSACS.getPassengerInfoResponseList()
                && null != acsPassengerSecurityRSACS.getPassengerInfoResponseList().getPassengerInfoResponse()
                && !acsPassengerSecurityRSACS.getPassengerInfoResponseList().getPassengerInfoResponse().isEmpty()
                && null != acsPassengerSecurityRSACS.getPassengerInfoResponseList().getPassengerInfoResponse().get(0).getPassengerID()) {
            passId = acsPassengerSecurityRSACS.getPassengerInfoResponseList().getPassengerInfoResponse().get(0).getPassengerID();
        }

        if (StringUtils.contains(msg, ErrorType.INVALID_COUNTRY_OR_COUNTRY_CODE_SEE_HCCC.getSearchableCode())) {
            log.error(ErrorType.INVALID_COUNTRY_OR_COUNTRY_CODE_SEE_HCCC.getFullDescription()
                    + getSabreError(acsPassengerSecurityRSACS.getResult()));

            throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.INVALID_COUNTRY_OR_COUNTRY_CODE_SEE_HCCC,
                    ErrorType.INVALID_COUNTRY_OR_COUNTRY_CODE_SEE_HCCC.getFullDescription() + "PASSENGER ID: "
                    + passId);
        } else if (StringUtils.contains(msg, ErrorType.INVALID_BIRTH_DATE.getSearchableCode())) {
            log.error(ErrorCodeDescriptions.INVALID_BIRTH_DATE
                    + "PASSENGER ID: " + passId
                    + getSabreError(acsPassengerSecurityRSACS.getResult()));
            throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.INVALID_BIRTH_DATE,
                    ErrorCodeDescriptions.INVALID_BIRTH_DATE);

        } else if (StringUtils.contains(msg, ErrorType.ENTER_DOCA_RESIDENCE_INFO.getSearchableCode())) {
            log.error(ErrorType.ENTER_DOCA_RESIDENCE_INFO.getFullDescription()
                    + getSabreError(acsPassengerSecurityRSACS.getResult()));
            throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.ENTER_DOCA_RESIDENCE_INFO,
                    ErrorType.ENTER_DOCA_RESIDENCE_INFO.getFullDescription());

        } else if (StringUtils.contains(msg, ErrorType.ENTER_DOCA_DESTINATION_INFO.getSearchableCode())) {
            log.error(ErrorType.ENTER_DOCA_DESTINATION_INFO.getFullDescription()
                    + getSabreError(acsPassengerSecurityRSACS.getResult()));
            throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.ENTER_DOCA_DESTINATION_INFO,
                    ErrorType.ENTER_DOCA_DESTINATION_INFO.getFullDescription());

        } else if (StringUtils.contains(msg, ErrorType.VERIFY_TVL_DOCUMENT_NUMBER.getSearchableCode())) {
            log.error(ErrorType.VERIFY_TVL_DOCUMENT_NUMBER.getFullDescription()
                    + getSabreError(acsPassengerSecurityRSACS.getResult()));
            throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.VERIFY_TVL_DOCUMENT_NUMBER,
                    ErrorType.VERIFY_TVL_DOCUMENT_NUMBER.getFullDescription());

        } else if (StringUtils.contains(msg, ErrorType.NO_RESPONSE_PSS_CHECK_IN_SERVICE_ERROR.getSearchableCode())) {
            log.error(ErrorType.NO_RESPONSE_PSS_CHECK_IN_SERVICE_ERROR.getFullDescription()
                    + getSabreError(acsPassengerSecurityRSACS.getResult()));
            throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.NO_RESPONSE_PSS_CHECK_IN_SERVICE_ERROR,
                    ErrorType.NO_RESPONSE_PSS_CHECK_IN_SERVICE_ERROR.getFullDescription());

        } else if (StringUtils.contains(msg, "Validation failed for: ")) {

        	log.error(ErrorType.VALIDATION_ERROR.getFullDescription() + getSabreError(acsPassengerSecurityRSACS.getResult()));
            throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.VALIDATION_ERROR, getSpecificErrorMessage(msg));

        } else if (StringUtils.contains(msg, ErrorType.UNABLE_TO_CONTACT_TIMATIC.getSearchableCode() )) {

        	log.error(ErrorType.UNABLE_TO_CONTACT_TIMATIC.getFullDescription() + getSabreError(acsPassengerSecurityRSACS.getResult()));
        	throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.UNABLE_TO_CONTACT_TIMATIC, ErrorType.UNABLE_TO_CONTACT_TIMATIC.getFullDescription());

        } else {
            log.error(ErrorType.UNKNOWN_CODE + getSabreError(acsPassengerSecurityRSACS.getResult()));
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNKNOWN_CODE, msg);
        }

    }

    /**
     * Looks for a specific user friendly error message from Sabre error response:
     * 
     * @return
     */
    private static String getSpecificErrorMessage(String msg) {

    	// First pattern when there is a value validation error
/*
Validation failed for: com.sun.org.apache.xerces.internal.jaxp.validation.XMLSchema@6056e945
				errors: [
				org.xml.sax.SAXParseException: cvc-pattern-valid: Value '1993-01-30' is not facet-valid with respect to pattern '(([2]  	
*/
    	if ( msg.contains( "cvc-pattern-valid: Value" ) && msg.contains( "is not facet-valid with respect to pattern" ) ) {
    		return msg.substring( msg.indexOf( "cvc-pattern-valid: Value" ) );
    	}

    	return msg;
    }

    /**
     *
     * @param resultRecord
     * @return
     */
    public static String getSabreError(ResultRecord resultRecord) {
        try {
            return resultRecord.getSystemSpecificResults().get(0).getErrorMessage().getValue();
        } catch (Exception ex) {
            // Ignore exception.
        }
        return null;
    }

    public static String getSabreErrorPassengerDetails(ProblemInformation problemInformation) {
        try {
            return problemInformation.getSystemSpecificResults().get(0).getMessage().get(0).getValue();
        } catch (Exception ex) {
            // Ignore exception.
        }
        return null;
    }

    /**
     *
     * @param editPassengerCharacteristicsRSACS
     */
    public static void checkErrorPassengerService(
            ACSEditPassengerCharacteristicsRSACS editPassengerCharacteristicsRSACS
    ) throws GenericException {

        String msg = "";

        for (SystemSpecificResults systemSpecificResult : editPassengerCharacteristicsRSACS.getResult()
                .getSystemSpecificResults()) {
            msg = msg + systemSpecificResult.getErrorMessage().getValue() + " | ";
        }

        if (!msg.isEmpty()) {
            msg = msg.substring(0, msg.length() - 3);
        }

        if (StringUtils.contains(msg, ErrorType.PRE_RESERVED_SEAT_NOT_SUCCESSFUL.getSearchableCode())) {

            throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.PRE_RESERVED_SEAT_NOT_SUCCESSFUL,
                    ErrorCodeDescriptions.MSG_CODE_UNABLE_SEAT);

        } else if (StringUtils.contains(msg, ErrorType.INVALID_REQUEST_SEAT.getSearchableCode())) {

            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.INVALID_REQUEST_SEAT,
                    ErrorCodeDescriptions.MSG_CODE_UNABLE_SEAT);
        } else if (StringUtils.contains(msg, ErrorType.RESERVATION_ORCHESTRATION_ERROR_CAUSED_BY.getSearchableCode())) {
            throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.RESERVATION_ORCHESTRATION_ERROR_CAUSED_BY,
                    ErrorType.RESERVATION_ORCHESTRATION_ERROR_CAUSED_BY.getFullDescription());
        } else {
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNKNOWN_CODE, msg);
        }

    }

    public static void frequentFlyerWarning(
            PassengerDetailsRS passengerDetailsRS,
            WarningCollection warningCollection
    ) {

        if (null == warningCollection) {
            warningCollection = new WarningCollection();
        }

        String msg = "";
        //com.sabre.services.stl_payload.v02_01
        for (ProblemInformation problemInformation : passengerDetailsRS.getApplicationResults().getWarning()) {
            msg = msg + problemInformation.getSystemSpecificResults().get(0).getMessage().get(0).getValue() + " | ";
        }

        if (!msg.isEmpty()) {
            msg = msg.substring(0, msg.length() - 3);
        }

        if (StringUtils.contains(msg, ErrorType.DATA_EXISTS_FOR_THIS_PASSENGER.getFullDescription())) {
            return;

        } else if (StringUtils.contains(msg, ErrorType.DATA_EXISTS_FOR_THIS_PASSENGER_DL.getFullDescription())) {
            return;
        } else if (StringUtils.contains(msg, ErrorType.FREQUENT_TRAVELER_NUMBER_DOES_NOT_EXIST_FOR_THIS_AIRLINE.getFullDescription())) {

            log.error(ErrorCodeDescriptions.MSG_CODE_UNABLE_FREQUENT_FLYER_NUMBER + msg);

            Warning warning = new Warning();
            warning.setCode(ErrorType.FREQUENT_TRAVELER_NUMBER_DOES_NOT_EXIST_FOR_THIS_AIRLINE.getErrorCode());
            warning.setMsg(ErrorType.FREQUENT_TRAVELER_NUMBER_DOES_NOT_EXIST_FOR_THIS_AIRLINE.getFullDescription());
            warning.setActionable(ErrorType.FREQUENT_TRAVELER_NUMBER_DOES_NOT_EXIST_FOR_THIS_AIRLINE.getActionable().toString());
            warningCollection.addToList(warning);

        } else if (StringUtils.contains(msg, ErrorType.NAME_DOES_NOT_MATCH_FREQUENT_TRAVELER_NUMBER.getFullDescription())) {

            log.error(ErrorCodeDescriptions.MSG_CODE_UNABLE_FREQUENT_FLYER_NUMBER_NAME_DOES_NOT_MATCH + msg);

            Warning warning = new Warning();
            warning.setCode(ErrorType.NAME_DOES_NOT_MATCH_FREQUENT_TRAVELER_NUMBER.getErrorCode());
            warning.setMsg(ErrorType.NAME_DOES_NOT_MATCH_FREQUENT_TRAVELER_NUMBER.getFullDescription());
            warning.setActionable(ErrorType.NAME_DOES_NOT_MATCH_FREQUENT_TRAVELER_NUMBER.getActionable().toString());
            warningCollection.addToList(warning);

        } else if (StringUtils.contains(msg, ErrorType.FREQUENT_TRAVELER_AGREEMENT_DOES_NOT_EXIST_BETWEEN_GA.getFullDescription())) {

            log.error(ErrorCodeDescriptions.MSG_CODE_UNABLE_FREQUENT_TRAVELER_AGREEMENT_DOES_NOT_EXIST_BETWEEN_GA + msg);

            Warning warning = new Warning();
            warning.setCode(ErrorType.FREQUENT_TRAVELER_AGREEMENT_DOES_NOT_EXIST_BETWEEN_GA.getErrorCode());
            warning.setMsg(ErrorType.FREQUENT_TRAVELER_AGREEMENT_DOES_NOT_EXIST_BETWEEN_GA.getFullDescription());
            warning.setActionable(ErrorType.FREQUENT_TRAVELER_AGREEMENT_DOES_NOT_EXIST_BETWEEN_GA.getActionable().toString());
            warningCollection.addToList(warning);

        } else if (StringUtils.contains(msg, ErrorType.UNABLE_TO_ACCEPT_FQTV.getFullDescription())) {

            log.error(ErrorCodeDescriptions.MSG_CODE_UNABLE_TO_ACCEPT_FQTV_NUMBERS + msg);

            Warning warning = new Warning();
            warning.setCode(ErrorType.UNABLE_TO_ACCEPT_FQTV.getErrorCode());
            warning.setMsg(ErrorType.UNABLE_TO_ACCEPT_FQTV.getFullDescription());
            warning.setActionable(ErrorType.UNABLE_TO_ACCEPT_FQTV.getActionable().toString());
            warningCollection.addToList(warning);

        } else {
            log.error(ErrorCodeDescriptions.MSG_CODE_UNKNOWN_ERROR + msg);

            Warning warning = new Warning();
            warning.setCode(ErrorType.UNKNOWN_CODE.getErrorCode());
            warning.setMsg(ErrorCodeDescriptions.MSG_CODE_UNKNOWN_ERROR + " | " + msg);
            warning.setActionable(ErrorType.UNKNOWN_CODE.getActionable().toString());
            warningCollection.addToList(warning);
        }
    }

    
	public static void checkErrorToCreateAE(GenericException msg, WarningCollection warningCollection) {
//		String msgError = msg.getMsg().substring(23, msg.getMsg().length() - 1);
//		System.out.println("aaaa" + msgError);
		if (msg.getMsg().contains(ErrorType.ET_PROCESSING_ERROR_NEED_PHONE_FIELD.getSearchableCode())) {
			Warning warning = new Warning();
			warning.setCode(ErrorType.ET_PROCESSING_ERROR_NEED_PHONE_FIELD.getErrorCode());
			warning.setMsg(ErrorType.ET_PROCESSING_ERROR_NEED_PHONE_FIELD.getFullDescription());
			warningCollection.addToList(warning);
		}else{
			Warning warning = new Warning();
            warning.setCode(ErrorType.UNKNOWN_CODE.getErrorCode());
            warning.setMsg(ErrorCodeDescriptions.MSG_CODE_UNKNOWN_ERROR + " | " + msg.getMessage());
            warningCollection.addToList(warning);
		}

	}
}
