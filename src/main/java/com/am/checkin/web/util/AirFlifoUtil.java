package com.am.checkin.web.util;

import com.aeromexico.commons.model.Segment;
import com.aeromexico.commons.model.SegmentStatus;
import com.aeromexico.commons.web.types.BookedLegFlightStatusType;
import com.aeromexico.commons.web.types.StatusType;
import com.aeromexico.commons.web.util.TimeUtil;
import com.sabre.webservices.sabrexml._2011._10.flifo.OTAAirFlifoRS;
import com.sabre.webservices.sabrexml._2011._10.schedule.OTAAirScheduleRS;
import com.sabre.webservices.sabrexml._2011._10.schedule.OTAAirScheduleRS.OriginDestinationOptions.OriginDestinationOption.FlightSegment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AirFlifoUtil {

    private static final Logger log = LoggerFactory.getLogger(AirFlifoUtil.class);

    /**
     * *
     * <p>
     * Convierte String a Date y agrega el year a la fecha
     *
     * @param dateString
     * @param year
     * @return
     * @throws Exception
     */
    public static Date getStringToDatetime(String dateString, String year) throws Exception {

        log.info(" + AirFlifoUtil.getStringToDatetime: " + dateString);

        Date date = null;

        try {
            if (!dateString.contains(year)) {
                dateString = year + "-" + dateString.replace("T", " ") + ":00";
            } else {
                dateString = dateString.replace("T", " ");
            }

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            date = formatter.parse(dateString);

        } catch (Exception e) {
            log.error(" + AirFlifoUtil.getStringToDatetime: " + e.getMessage());
            throw e;
        }

        log.info(" - AirFlifoUtil.getStringToDatetime: " + date);

        return date;
    }

    /**
     * *
     * <p>
     * Realiza la resta entre fechas para obtener el tiempo de vuelo por
     * segmento o por vuelo completo
     *
     * @param originDate
     * @param destinatioDate
     * @return
     */
    public static Long restDatesMinutes(Date originDate, Date destinatioDate) {

        log.info(" + AirFlifoUtil.restDatesMinutes ( " + originDate + ", " + destinatioDate + ")");

        long diferenciaMinutos = 0;

        try {
            Calendar departureDate = Calendar.getInstance();
            Calendar arrivalDate = Calendar.getInstance();
            departureDate.setTime(originDate);
            arrivalDate.setTime(destinatioDate);
            long dif = 0;

            if (arrivalDate.before(departureDate)) {
                arrivalDate.add(Calendar.YEAR, 1);
            }

            if (departureDate.before(arrivalDate)) {
                dif = arrivalDate.getTimeInMillis() - departureDate.getTimeInMillis();
            } else {
                dif = departureDate.getTimeInMillis() - arrivalDate.getTimeInMillis();
            }
            diferenciaMinutos = dif / (60 * 1000);
        } catch (Exception e) {
            diferenciaMinutos = 0;
            throw e;
        }

        log.info(" - AirFlifoUtil.restDatesMinutes: " + diferenciaMinutos);

        return diferenciaMinutos;
    }

    /**
     * *
     * <p>
     * Calcula el tiempo total considerando los segmentos
     *
     * @param listSegments
     * @param year
     * @return
     * @throws Exception
     */
    public static int calculteTimeTotal(List<OTAAirScheduleRS.OriginDestinationOptions.OriginDestinationOption.FlightSegment> listSegments, String year) throws Exception {

        log.info(" + AirFlifoUtil.calculteTimeTotal");

        int total = 0;
        String timeDeparture = "";
        String timeArrival = "";

        for (OTAAirScheduleRS.OriginDestinationOptions.OriginDestinationOption.FlightSegment segment : listSegments) {
            timeDeparture = segment.getDepartureDateTime();
            timeArrival = segment.getArrivalDateTime();
            try {
                total += restDatesMinutes(getStringToDatetime(timeDeparture, year), getStringToDatetime(timeArrival, year))
                        .intValue();
            } catch (Exception e) {
                total = 0;
                throw e;
            }
        }

        for (int i = 0; i < listSegments.size() - 1; i++) {
            try {
                total += calculateTimeLayoverToNextSegmentsInMinutes(listSegments.get(i), listSegments.get(i + 1), year);
            } catch (Exception e) {
                total = 0;
            }
        }

        log.info(" - AirFlifoUtil.calculteTimeTotal: " + total);

        return total;
    }

    /**
     * *
     * <p>
     * Calcula el tiempo entre segmentos
     *
     * @param segmentOne
     * @param segmentTwo
     * @param year
     * @return
     * @throws Exception
     */
    public static int calculateTimeLayoverToNextSegmentsInMinutes(FlightSegment segmentOne, FlightSegment segmentTwo, String year) throws Exception {

        log.info(" + AirFlifoUtil.calculateTimeLayoverToNextSegmentsInMinutes");

        int minutos = 0;
        String timeArrival = "";
        String timeDeparture = "";

        try {
            timeArrival = segmentOne.getArrivalDateTime();
            if (segmentTwo == null) {
                timeDeparture = segmentOne.getArrivalDateTime();
            } else {
                timeDeparture = segmentTwo.getDepartureDateTime();
            }

            minutos = restDatesMinutes(getStringToDatetime(timeArrival, year), getStringToDatetime(timeDeparture, year)).intValue();
        } catch (Exception e) {
            minutos = 0;
            throw e;
        }

        log.info(" + AirFlifoUtil.calculateTimeLayoverToNextSegmentsInMinutes: " + minutos);

        return minutos;
    }

    /**
     * *
     *
     * @param segmentOne
     * @param segmentTwo
     * @param year
     * @return
     * @throws Exception
     */
    public static int calculateTimeLayoverToNextSegmentsInMinutes(SegmentStatus segmentOne, SegmentStatus segmentTwo, String year) throws Exception {

        log.info(" + AirFlifoUtil.calculateTimeLayoverToNextSegmentsInMinutes");

        int minutos = 0;
        String timeArrival = "";
        String timeDeparture = "";

        try {
            timeArrival = segmentOne.getSegment().getArrivalDateTime();
            if (segmentTwo == null) {
                timeDeparture = segmentOne.getSegment().getArrivalDateTime();
            } else {
                timeDeparture = segmentTwo.getSegment().getDepartureDateTime();
            }

            minutos = restDatesMinutes(getStringToDatetime(timeArrival, year), getStringToDatetime(timeDeparture, year)).intValue();
        } catch (Exception e) {
            minutos = 0;
        }

        log.info(" - AirFlifoUtil.calculateTimeLayoverToNextSegmentsInMinutes: " + minutos);

        return minutos;
    }

    // Calcula el tiempo total del vuelo
    public static int calculteFlightTime(String fechaDeparture, String fechaArrival) throws Exception {

        log.info(" + AirFlifoUtil.calculteFlightTime ( " + fechaDeparture + ", " + fechaArrival + ")");

        int total = 0;
        Date arrival = null;
        Date departure = null;

        try {

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            arrival = formatter.parse(fechaArrival.replace("T", " "));
            departure = formatter.parse(fechaDeparture.replace("T", " "));
            total = restDatesMinutes(departure, arrival).intValue();

        } catch (Exception e) {
            total = 0;
        }

        log.info(" - AirFlifoUtil.calculateTimeLayoverToNextSegmentsInMinutes: " + total);

        return total;
    }

    public static void completeSegment(OTAAirFlifoRS otaAirFlifoRS, Segment segment) {

        String carrier = otaAirFlifoRS.getFlightInfo().getAirlineCode();
        String flightNumber = otaAirFlifoRS.getFlightInfo().getFlightNumber();

        segment.setMarketingFlightCode(flightNumber);
        segment.setMarketingCarrier(carrier);
        segment.setOperatingFlightCode(flightNumber);
        int flightNumber2 = Integer.parseInt(flightNumber);
        if (flightNumber2 >= 5000 && flightNumber2 <= 5999) {
            segment.setOperatingCarrier("DL");
        } else if (flightNumber2 >= 6000 && flightNumber2 <= 6499) {
            segment.setOperatingCarrier("AF");
        } else if (flightNumber2 >= 6500 && flightNumber2 <= 6599) {
            segment.setOperatingCarrier("KL");
        } else if (flightNumber2 >= 6600 && flightNumber2 <= 6699) {
            segment.setOperatingCarrier("AZ");
        } else if (flightNumber2 >= 6700 && flightNumber2 <= 6799) {
            segment.setOperatingCarrier("KE");
        } else if (flightNumber2 >= 6800 && flightNumber2 <= 6999) {
            segment.setOperatingCarrier("UX");
        } else if (flightNumber2 >= 7000 && flightNumber2 <= 7099) {
            segment.setOperatingCarrier("OK");
        } else if (flightNumber2 >= 7900 && flightNumber2 <= 7999) {
            segment.setOperatingCarrier("CM");
        } else if (flightNumber2 >= 8220 && flightNumber2 <= 8269) {
            segment.setOperatingCarrier("AV");
        } else if (flightNumber2 >= 8270 && flightNumber2 <= 8319) {
            segment.setOperatingCarrier("AS");
        } else {
            segment.setOperatingCarrier(carrier);
        }
        segment.setAircraftType("N/A");

        segment.setSegmentCode(getSegmentCode(segment));

        try {
            segment.setFlightDurationInMinutes(calculteFlightTime(segment.getDepartureDateTime(), segment.getArrivalDateTime()));
        } catch (Exception e) {
            segment.setFlightDurationInMinutes(0);
        }

    }

    public static void setStatus(OTAAirFlifoRS otaAirFlifoRS, SegmentStatus segmentStatus, String year, String originTimeZone, String destinationTimeZone) throws Exception {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        String status = "";
        Date dateNow = new Date();

        Date departureEstimate = null;
        Date arrivalEstimate = null;

        Date departureScheduled = null;
        Date arrivalScheduled = null;

        Date departureOffGate = null;
        Date arrivalOnGate = null;

        if (otaAirFlifoRS.getFlightInfo().getActualInfo() != null) {

            for (OTAAirFlifoRS.FlightInfo.ActualInfo.FlightLeg flightLeg : otaAirFlifoRS.getFlightInfo().getActualInfo().getFlightLeg()) {

                for (String texto : flightLeg.getText()) {
                    if (null != texto) {
                        if (texto.toUpperCase().contains("CANCELED")) {
                            status = String.valueOf(StatusType.fromCode("CANCELLED"));
                        }
                    }

                }

                if (flightLeg.getLocationCode() != null) {
                    if (flightLeg.getArrivalDateTime() != null
                            && segmentStatus.getSegment().getArrivalAirport().equalsIgnoreCase(flightLeg.getLocationCode())) {

                        if (flightLeg.getArrivalDateTime().getEstimated() != null) {
                            arrivalEstimate = getStringToDatetime(flightLeg.getArrivalDateTime().getEstimated(), year);
                        } else if (flightLeg.getArrivalDateTime().getOnGate() != null) {
                            arrivalOnGate = getStringToDatetime(flightLeg.getArrivalDateTime().getOnGate(), year);
                        }

                    }
                }

                if (flightLeg.getLocationCode() != null) {
                    if (flightLeg.getDepartureDateTime() != null
                            && segmentStatus.getSegment().getDepartureAirport().equalsIgnoreCase(flightLeg.getLocationCode())) {

                        if (flightLeg.getDepartureDateTime().getEstimated() != null) {
                            departureEstimate = getStringToDatetime(flightLeg.getDepartureDateTime().getEstimated(), year);
                        } else if (flightLeg.getDepartureDateTime().getOffGate() != null) {
                            departureOffGate = getStringToDatetime(flightLeg.getDepartureDateTime().getOffGate(), year);
                        }

                    }
                }
            }
        }

        boolean isDeleyed = false;
        int totalMinutes = 0;

        if (status.isEmpty()) {
            if (segmentStatus.getSegment().getDepartureDateTime() != null) {
                departureScheduled = getStringToDatetime(segmentStatus.getSegment().getDepartureDateTime(), year);

                if (departureEstimate != null) {
                    if (departureEstimate.before(departureScheduled)) {
                        departureScheduled = departureEstimate;
                    } else if (departureEstimate.after(departureScheduled)) {

                        totalMinutes = minutesBetweenTwoDateTimes(
                                convertDateToLocalDateTime(departureScheduled),
                                convertDateToLocalDateTime(departureEstimate)
                        );
                        log.info("totalMinutes: " + totalMinutes);

                        if (departureEstimate.after(dateNow)) {
                            //Flights delayed more than 15 minutes
                            if (totalMinutes > 15) {
                                status = BookedLegFlightStatusType.DELAYED.toString();
                                isDeleyed = true;

                                //Agregar el tiempo estimado
                                segmentStatus.setEstimatedDepartureTime(formatter.format(departureEstimate));
                                segmentStatus.setDelayInMinutes(totalMinutes);

                            } else {
                                departureScheduled = departureEstimate;
                            }
                        } else {
                            departureScheduled = departureEstimate;
                        }
                    }
                } else if (departureOffGate != null && departureOffGate.before(departureScheduled)) {
                    departureScheduled = departureOffGate;
                }
            }

            if (segmentStatus.getSegment().getArrivalDateTime() != null) {
                arrivalScheduled = getStringToDatetime(segmentStatus.getSegment().getArrivalDateTime(), year);

                if (arrivalEstimate != null) {
                    if (isDeleyed) {
                        segmentStatus.setEstimatedArrivalTime(formatter.format(arrivalEstimate));
                    } else {

                        //TODO error response sabre
                        int minutesbefore = minutesBetweenTwoDateTimes(
                                convertDateToLocalDateTime(arrivalEstimate),
                                convertDateToLocalDateTime(arrivalScheduled)
                        );

                        if (minutesbefore < 180) {
                            arrivalScheduled = arrivalEstimate;
                        }
                    }
                } else if (arrivalOnGate != null) {

                    //TODO error response sabre
                    int minutesbefore = minutesBetweenTwoDateTimes(
                            convertDateToLocalDateTime(arrivalOnGate),
                            convertDateToLocalDateTime(arrivalScheduled)
                    );

                    if (minutesbefore < 180) {
                        arrivalScheduled = arrivalOnGate;
                    }
                } else {
                    Date arrivalDelay = convertLocalDateTimeToDate(
                            convertDateToLocalDateTime(arrivalScheduled).plusMinutes(totalMinutes)
                    );
                    if (isDeleyed) {
                        segmentStatus.setEstimatedArrivalTime(formatter.format(arrivalDelay));
                    } else {
                        arrivalScheduled = arrivalDelay;
                    }
                }
            }

            if (status.isEmpty()) {
                if (departureEstimate != null && arrivalEstimate != null) {
                    status = calculateStatus(dateNow, departureEstimate, arrivalEstimate, originTimeZone, destinationTimeZone);
                } else if (departureScheduled != null && arrivalScheduled != null) {
                    status = calculateStatus(dateNow, departureScheduled, arrivalScheduled, originTimeZone, destinationTimeZone);
                } else {
                    status = String.valueOf(StatusType.fromCode("CANCELLED"));
                }
            }

            //Agregar las fechas del vuelo
            if (departureScheduled != null && arrivalScheduled != null) {
                segmentStatus.getSegment().setDepartureDateTime(formatter.format(departureScheduled));
                segmentStatus.getSegment().setArrivalDateTime(formatter.format(arrivalScheduled));
                segmentStatus.getSegment().setFlightDurationInMinutes(
                        AirFlifoUtil.calculteFlightTime(
                                segmentStatus.getSegment().getDepartureDateTime(),
                                segmentStatus.getSegment().getArrivalDateTime()
                        )
                );

                segmentStatus.getSegment().setSegmentCode(getSegmentCode(segmentStatus.getSegment()));
                segmentStatus.setBoardingTime(segmentStatus.getSegment().getDepartureDateTime().split("T")[1].substring(0, 5));
            }

        }

        log.info("Status:: " + status);
        segmentStatus.setStatus(status);
    }

    public static String calculateStatus(Date dateNow, Date departure, Date arrival, String originTimeZone, String destinationTimeZone) throws ParseException {
        String status = "";
        LocalDateTime now = LocalDateTime.ofInstant(dateNow.toInstant(), ZoneId.systemDefault());

        ZonedDateTime origin = TimeUtil.getZonedDateTime(departure.getTime(), originTimeZone, "yyyy-MM-dd HH:mm:ss");
        LocalDateTime originInSystemZone = origin.withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime();

        ZonedDateTime destination = TimeUtil.getZonedDateTime(arrival.getTime(), destinationTimeZone, "yyyy-MM-dd HH:mm:ss");
        LocalDateTime destinationInSystemZone = destination.withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime();

        if (now.isAfter(originInSystemZone) && now.isBefore(destinationInSystemZone)) {
            status = String.valueOf(StatusType.fromCode("FLOWN"));
        } else if (now.isBefore(originInSystemZone)) {
            status = String.valueOf(StatusType.fromCode("ON_TIME"));
        } else if (now.isAfter(destinationInSystemZone)) {
            status = String.valueOf(StatusType.fromCode("ARRIVED"));
        }

        return status;
    }

    public static String getSegmentCode(Segment segment) {
        return segment.getDepartureAirport() + "_"
                + segment.getArrivalAirport() + "_"
                + segment.getMarketingCarrier() + "_"
                + segment.getDepartureDateTime().split("T")[0] + "_"
                + segment.getDepartureDateTime().split("T")[1].replaceAll(":", "").substring(0, 4);
    }

    /**
     * Method to return the minutes between two date times
     *
     * @param startDateTime
     * @param finishDateTime
     * @return
     */
    private static int minutesBetweenTwoDateTimes(LocalDateTime startDateTime, LocalDateTime finishDateTime) {
        return (int) ChronoUnit.MINUTES.between(startDateTime, finishDateTime);
    }

    /**
     * Method to convert string to local date time
     *
     * @param dateTime
     * @return
     */
    public static LocalDateTime getDateTimeFromString(String dateTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime localDateTime = null;

        try {
            localDateTime = LocalDateTime.parse(dateTime, formatter);
        } catch (Exception e) {

        }
        return localDateTime;
    }

    private static LocalDateTime convertDateToLocalDateTime(Date date) {
        return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
    }

    private static Date convertLocalDateTimeToDate(LocalDateTime localDateTime) {
        ZonedDateTime zdt = localDateTime.atZone(ZoneId.systemDefault());
        return Date.from(zdt.toInstant());
    }
}
