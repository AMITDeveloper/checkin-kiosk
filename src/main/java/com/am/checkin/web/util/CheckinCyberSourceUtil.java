package com.am.checkin.web.util;

import com.aeromexico.commons.web.util.ErrorCodeDescriptions;
import com.aeromexico.cybersource.util.CybersourceConstants;
import com.aeromexico.commons.exception.model.CustomBadRequestException;
import com.aeromexico.commons.model.AbstractAncillary;
import com.aeromexico.commons.model.Address;
import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.CabinUpgradeAncillary;
import com.aeromexico.commons.model.CreditCardPaymentRequest;
import com.aeromexico.commons.model.ExtraWeightAncillary;
import com.aeromexico.commons.model.FreeBagAllowanceDB;
import com.aeromexico.commons.model.Segment;
import com.aeromexico.commons.model.TravelerAncillary;
import com.aeromexico.commons.web.types.ItemsToBePaidType;
import com.aeromexico.commons.web.types.PhoneType;

import com.cybersource.schemas.transaction_data_1.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.*;

import com.aeromexico.dao.services.IBaggageAllowanceDao;
import com.aeromexico.webe4.web.common.types.ErrorCodeType;

/**
 *
 * @author adrian
 */
public class CheckinCyberSourceUtil {

    private static final Logger log = LoggerFactory.getLogger(CheckinCyberSourceUtil.class);

    private static final SimpleDateFormat FORMATTER_SABRE = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    private static final SimpleDateFormat FORMATTER_CYBERSOURCE = new SimpleDateFormat("yyyy-MM-dd HH:mm z");

    private final IBaggageAllowanceDao compBagAllowDao;

    public CheckinCyberSourceUtil(IBaggageAllowanceDao compBagAllowAPI) {
        this.compBagAllowDao = compBagAllowAPI;
    }

    private String getRouteType(String departureAirport, String arriveAirport) {
        try {
            FreeBagAllowanceDB compBagAllow = compBagAllowDao.getFreeBagAllowInfo(departureAirport, arriveAirport);

            String routeType;
            if ("NAL".equalsIgnoreCase(compBagAllow.getRegionKey())) {
                routeType = "D";
            } else {
                routeType = "I";
            }

            return routeType;
        } catch (Exception e) {
            e.getMessage();
            return "";
        }
    }

    /**
     *
     * @param date in format yyyy-mm
     * @return
     */
    private int extractExpirationMonth(String date) {
        try {
            return new Integer(date.substring(5));
        } catch (Exception ex) {
            return 0;
        }
    }

    /**
     *
     * @param date in format yyyy-mm
     * @return
     */
    private int extractExpirationYear(String date) {
        try {
            return new Integer(date.substring(0, 4));
        } catch (Exception ex) {
            return 0;
        }
    }

    /**
     *
     * @param bookedTravelersRequested
     * @return
     */
    private Item[] getPassengersItemsForCybersource(
            List<BookedTraveler> bookedTravelersRequested,
            ItemsToBePaidType toPaidItemsType
    ) {

        if (null == bookedTravelersRequested) {
            throw new CustomBadRequestException(Response.Status.BAD_REQUEST.getStatusCode(),
                    ErrorCodeType.BAD_REQUEST.getErrorCode(), null,
                    ErrorCodeDescriptions.MSG_CODE_PASSENGERS_NOT_FOUND_FOR_CYBERSOURCE_REQUEST);
        }

        List<Item> itemsList = new ArrayList<>();

        int i = 0;
        for (BookedTraveler bookedTraveler : bookedTravelersRequested) {
            switch (toPaidItemsType) {
                case UPGRADE:
                    for (CabinUpgradeAncillary cabinUpgradeAncillary : PurchaseOrderUtil.getUpgradeUnpaidAncillaries(bookedTraveler.getAncillaries().getCollection())) {
                        Item item = new Item();
                        item.setId(new BigInteger(String.valueOf(i++)));
                        item.setPassengerFirstName(bookedTraveler.getFirstName()); // item_#_passengerFirstName
                        item.setPassengerLastName(bookedTraveler.getLastName()); // item_#_passengerLastName
                        item.setPassengerType(bookedTraveler.getPaxType().getCode()); // item_#_passengerType
                        if (bookedTraveler.getEmail() != null
                                && !bookedTraveler.getEmail().isEmpty()) {
                            item.setPassengerEmail(bookedTraveler.getEmail()); // item_#_passengerEmail
                        }
                        item.setPassengerID(bookedTraveler.getId()); // item_#_passengerID
                        if (null != bookedTraveler.getPhones()
                                && null != bookedTraveler.getPhones().getCollection()
                                && !bookedTraveler.getPhones().getCollection().isEmpty()) {
                            item.setPassengerPhone(bookedTraveler.getPhones().getCollection().get(0).getNumber()); // item_#_passengerPhone
                        }

                        try {
                            item.setProductCode(cabinUpgradeAncillary.getAncillary().getType());
                            item.setProductDescription(cabinUpgradeAncillary.getAncillary().getCommercialName());
                            item.setProductName(cabinUpgradeAncillary.getAncillary().getType());
                            item.setQuantity(String.valueOf(cabinUpgradeAncillary.getQuantity()));
                            item.setProductSKU(cabinUpgradeAncillary.getAncillary().getType());
                            item.setUnitPrice(cabinUpgradeAncillary.getAncillary().getCurrency().getTotal().toString());
                        } catch (Exception ex) {
                            log.error("Error getting unitary price: " + ex.getMessage(), ex);
                        }

                        itemsList.add(item);
                    }
                    break;
                case EXTRAWEIGHT:
                    for (ExtraWeightAncillary extraWeightAncillary : PurchaseOrderUtil.getExtraWeightUnpaidAncillaries(bookedTraveler.getAncillaries().getCollection())) {
                        Item item = new Item();
                        item.setId(new BigInteger(String.valueOf(i++)));
                        item.setPassengerFirstName(bookedTraveler.getFirstName()); // item_#_passengerFirstName
                        item.setPassengerLastName(bookedTraveler.getLastName()); // item_#_passengerLastName
                        item.setPassengerType(bookedTraveler.getPaxType().getCode()); // item_#_passengerType
                        if (bookedTraveler.getEmail() != null
                                && !bookedTraveler.getEmail().isEmpty()) {
                            item.setPassengerEmail(bookedTraveler.getEmail()); // item_#_passengerEmail
                        }
                        item.setPassengerID(bookedTraveler.getId()); // item_#_passengerID
                        if (null != bookedTraveler.getPhones()
                                && null != bookedTraveler.getPhones().getCollection()
                                && !bookedTraveler.getPhones().getCollection().isEmpty()) {
                            item.setPassengerPhone(bookedTraveler.getPhones().getCollection().get(0).getNumber()); // item_#_passengerPhone
                        }

                        try {
                            item.setProductCode(extraWeightAncillary.getAncillary().getType());
                            item.setProductDescription(extraWeightAncillary.getAncillary().getCommercialName());
                            item.setProductName(extraWeightAncillary.getAncillary().getType());
                            item.setQuantity(String.valueOf(extraWeightAncillary.getQuantity()));
                            item.setProductSKU(extraWeightAncillary.getAncillary().getType());
                            item.setUnitPrice(extraWeightAncillary.getAncillary().getCurrency().getTotal().toString());
                        } catch (Exception ex) {
                            log.error("Error getting unitary price: " + ex.getMessage(), ex);
                        }

                        itemsList.add(item);
                    }
                    break;
                case BAGGAGE:
                    for (TravelerAncillary travelerAncillary : PurchaseOrderUtil.getUnpaidAncillaries(bookedTraveler.getAncillaries().getCollection())) {
                        Item item = new Item();
                        item.setId(new BigInteger(String.valueOf(i++)));
                        item.setPassengerFirstName(bookedTraveler.getFirstName()); // item_#_passengerFirstName
                        item.setPassengerLastName(bookedTraveler.getLastName()); // item_#_passengerLastName
                        item.setPassengerType(bookedTraveler.getPaxType().getCode()); // item_#_passengerType
                        if (bookedTraveler.getEmail() != null
                                && !bookedTraveler.getEmail().isEmpty()) {
                            item.setPassengerEmail(bookedTraveler.getEmail()); // item_#_passengerEmail
                        }
                        item.setPassengerID(bookedTraveler.getId()); // item_#_passengerID
                        if (null != bookedTraveler.getPhones()
                                && null != bookedTraveler.getPhones().getCollection()
                                && !bookedTraveler.getPhones().getCollection().isEmpty()) {
                            item.setPassengerPhone(bookedTraveler.getPhones().getCollection().get(0).getNumber()); // item_#_passengerPhone
                        }

                        try {
                            item.setProductCode(travelerAncillary.getAncillary().getType());
                            item.setProductDescription(travelerAncillary.getAncillary().getCommercialName());
                            item.setProductName(travelerAncillary.getAncillary().getType());
                            item.setQuantity(String.valueOf(travelerAncillary.getQuantity()));
                            item.setProductSKU(travelerAncillary.getAncillary().getType());
                            item.setUnitPrice(travelerAncillary.getAncillary().getCurrency().getTotal().toString());
                        } catch (Exception ex) {
                            log.error("Error getting unitary price: " + ex.getMessage(), ex);
                        }

                        itemsList.add(item);
                    }
                    break;
                case SEAT:
                    for (TravelerAncillary travelerAncillary : PurchaseOrderUtil.getUnpaidAncillaries(bookedTraveler.getSeatAncillaries().getCollection())) {
                        Item item = new Item();
                        item.setId(new BigInteger(String.valueOf(i++)));
                        item.setPassengerFirstName(bookedTraveler.getFirstName()); // item_#_passengerFirstName
                        item.setPassengerLastName(bookedTraveler.getLastName()); // item_#_passengerLastName
                        item.setPassengerType(bookedTraveler.getPaxType().getCode()); // item_#_passengerType
                        if (bookedTraveler.getEmail() != null
                                && !bookedTraveler.getEmail().isEmpty()) {
                            item.setPassengerEmail(bookedTraveler.getEmail()); // item_#_passengerEmail
                        }
                        item.setPassengerID(bookedTraveler.getId()); // item_#_passengerID
                        if (null != bookedTraveler.getPhones()
                                && null != bookedTraveler.getPhones().getCollection()
                                && !bookedTraveler.getPhones().getCollection().isEmpty()) {
                            item.setPassengerPhone(bookedTraveler.getPhones().getCollection().get(0).getNumber()); // item_#_passengerPhone
                        }

                        try {
                            item.setProductCode(travelerAncillary.getAncillary().getType());
                            item.setProductDescription(travelerAncillary.getAncillary().getCommercialName());
                            item.setProductName(travelerAncillary.getAncillary().getType());
                            item.setQuantity(String.valueOf(travelerAncillary.getQuantity()));
                            item.setProductSKU(travelerAncillary.getAncillary().getType());
                            item.setUnitPrice(travelerAncillary.getAncillary().getCurrency().getTotal().toString());
                        } catch (Exception ex) {
                            log.error("Error getting unitary price: " + ex.getMessage(), ex);
                        }

                        itemsList.add(item);
                    }
                    break;
                case SEAT_AND_BAGGAGE:
                    List<AbstractAncillary> allAncillaries = PurchaseOrderUtil.getAllAncillaries(bookedTraveler);
                    for (TravelerAncillary travelerAncillary : PurchaseOrderUtil.getUnpaidAncillaries(allAncillaries)) {
                        Item item = new Item();
                        item.setId(new BigInteger(String.valueOf(i++)));
                        item.setPassengerFirstName(bookedTraveler.getFirstName()); // item_#_passengerFirstName
                        item.setPassengerLastName(bookedTraveler.getLastName()); // item_#_passengerLastName
                        item.setPassengerType(bookedTraveler.getPaxType().getCode()); // item_#_passengerType
                        if (bookedTraveler.getEmail() != null
                                && !bookedTraveler.getEmail().isEmpty()) {
                            item.setPassengerEmail(bookedTraveler.getEmail()); // item_#_passengerEmail
                        }
                        item.setPassengerID(bookedTraveler.getId()); // item_#_passengerID
                        if (null != bookedTraveler.getPhones()
                                && null != bookedTraveler.getPhones().getCollection()
                                && !bookedTraveler.getPhones().getCollection().isEmpty()) {
                            item.setPassengerPhone(bookedTraveler.getPhones().getCollection().get(0).getNumber()); // item_#_passengerPhone
                        }

                        try {
                            item.setProductCode(travelerAncillary.getAncillary().getType());
                            item.setProductDescription(travelerAncillary.getAncillary().getCommercialName());
                            item.setProductName(travelerAncillary.getAncillary().getType());
                            item.setQuantity(String.valueOf(travelerAncillary.getQuantity()));
                            item.setProductSKU(travelerAncillary.getAncillary().getType());
                            item.setUnitPrice(travelerAncillary.getAncillary().getCurrency().getTotal().toString());
                        } catch (Exception ex) {
                            log.error("Error getting unitary price: " + ex.getMessage(), ex);
                        }

                        itemsList.add(item);
                    }
                    break;
                default:
                    break;
            }

        }

        // iterate on list of items
        Item[] items = new Item[itemsList.size()];

        return itemsList.toArray(items);
    }

    /**
     *
     * @param creditCardPaymentRequest
     * @param bookedTravelersRequested
     * @param bookedLegList
     * @param recordLocator
     * @param creationDate
     * @param storeCode
     * @return
     */
    private MDDField[] callMDDFields(
            CreditCardPaymentRequest creditCardPaymentRequest,
            List<BookedTraveler> bookedTravelersRequested,
            List<BookedLeg> bookedLegList,
            String recordLocator,
            String creationDate,
            String transactionTime,
            String storeCode,
            String kioskId
    ) {

        List<String> mddsList = new ArrayList<>();

        String binNumber;
        String billingEmail;
        try {
            if (null != creditCardPaymentRequest) {
                binNumber = CheckinMDDUtil.getBinNumber(creditCardPaymentRequest.getCcNumber());

                if (null != creditCardPaymentRequest.getEmail() && !creditCardPaymentRequest.getEmail().trim().isEmpty()) {
                    billingEmail = creditCardPaymentRequest.getEmail();
                } else {
                    billingEmail = "";
                }
            } else {
                billingEmail = "";
                binNumber = "";
            }
        } catch (Exception ex) {
            billingEmail = "";
            binNumber = "";
        }

        Segment segment = bookedLegList.get(0).getSegments().getCollection().get(0).getSegment();

        // 1
        String thirdPartyBooking = CheckinMDDUtil.getThirdPartyBooking(bookedTravelersRequested, creditCardPaymentRequest);
        mddsList.add(thirdPartyBooking);
        // 2
        String departureCity = CheckinMDDUtil.getDepartureCity(bookedLegList);
        mddsList.add(departureCity);
        // 3
        String finalDestination = CheckinMDDUtil.getFinalDestination(bookedLegList);
        mddsList.add(finalDestination);
        // 4
        String routeType = getRouteType(departureCity, finalDestination);
        mddsList.add(routeType);
        // 5
        String fullItinerary = CheckinMDDUtil.getFullItinerary(bookedLegList);
        mddsList.add(fullItinerary);
        // 6
        String oneWayRoundTrip = CheckinMDDUtil.getOneWayRoundTrip(bookedLegList);
        mddsList.add(oneWayRoundTrip);
        // 7
        mddsList.add(CheckinMDDUtil.getFQTVNumber(bookedTravelersRequested.get(0)));// pendiente
        // 8
        mddsList.add(CheckinMDDUtil.getFQTVStatus(bookedTravelersRequested.get(0)));// pendiente
        // 9
        String numOfPaxs = CheckinMDDUtil.getNumOfPaxs(bookedTravelersRequested);
        mddsList.add(numOfPaxs);
        // 10
        int hoursToFlight = CheckinMDDUtil.getHoursTillDeparture(transactionTime, segment);
        mddsList.add(String.valueOf(hoursToFlight));
        // 11
        int daysToFlight = CheckinMDDUtil.getDaysTillDeparture(transactionTime, segment);
        mddsList.add(String.valueOf(daysToFlight));
        // 12
        String cabinClass = CheckinMDDUtil.getClassService(bookedTravelersRequested.get(0), segment);
        mddsList.add(cabinClass);
        // 13
        String dayOfFlight = CheckinMDDUtil.getDayOfFlight(segment.getDepartureDateTime());
        mddsList.add(dayOfFlight);
        // 14
        int weekOfYear = CheckinMDDUtil.getWeekOfYear(segment.getDepartureDateTime());
        mddsList.add(String.valueOf(weekOfYear));
        // 15
        String channelOfBoking = CheckinMDDUtil.getChannelOfBoking(storeCode);
        mddsList.add(channelOfBoking);
        // 16
        mddsList.add(binNumber);

        List<String> emails = CheckinMDDUtil.getPassengerEmails(bookedTravelersRequested);

        // 17
        if (null != billingEmail && !billingEmail.isEmpty()) {
            mddsList.add(billingEmail);
        } else if (!emails.isEmpty()) {
            mddsList.add(emails.get(0));
        } else {
            mddsList.add("");
        }

        List<String> mobilePhones = CheckinMDDUtil.getPassengerMobilePhones(bookedTravelersRequested, PhoneType.MOBILE);

        // 18
        if (mobilePhones.isEmpty()) {
            mddsList.add("");
        } else {
            mddsList.add(mobilePhones.get(0));
        }

        List<String> otherOhones = CheckinMDDUtil.getPassengerMobilePhones(bookedTravelersRequested, null);
        if (otherOhones.isEmpty()) {
            // 19
            mddsList.add("");
            // 20
            mddsList.add("");
        } else {
            // 19
            mddsList.add(otherOhones.get(0));

            if (otherOhones.size() > 1) {
                // 20
                mddsList.add(otherOhones.get(1));
            } else {
                // 20
                mddsList.add("");
            }

        }

        // 21        
        mddsList.add(recordLocator);
        // 22
        String fechaCreacionPNR = CheckinMDDUtil.getPNRCreationDate(creationDate);
        mddsList.add(fechaCreacionPNR);
        // 23
        mddsList.add("");
        // 24
        mddsList.add("");
        // 25 International Flight
        if ("I".equalsIgnoreCase(routeType)) {
            mddsList.add("YES");
        } else {
            mddsList.add("NO");
        }

        if (emails.isEmpty()) {
            // 26
            mddsList.add("");
            // 27
            mddsList.add("");
        } else {
            // 26
            mddsList.add(emails.get(0));
            if (emails.size() > 1) {
                // 27
                mddsList.add(emails.get(1));
            } else {
                // 27
                mddsList.add("");
            }
        }

        // 28
        mddsList.add("");

        // 29
        mddsList.add(kioskId);

        // fill the return value
        // the size must be 29
        MDDField[] mddField = new MDDField[mddsList.size()];
        int i = 0;
        for (String mdd : mddsList) {
            mddField[i] = new MDDField();
            mddField[i].setId(new BigInteger(String.valueOf(i + 1)));
            mddField[i].setValue(mdd);
            i++;
        }

        return mddField;
    }

    private DecisionManagerTravelLeg[] getDecisionManagerTravelLeg(String FullItinerary) {
        try {
            String[] part = FullItinerary.split(":");
            DecisionManagerTravelLeg[] legs = new DecisionManagerTravelLeg[part.length];

            for (int i = 0; i < part.length; i++) {
                String[] partCities = part[i].split("-");

                DecisionManagerTravelLeg decisionManagerTravelLeg = new DecisionManagerTravelLeg();
                decisionManagerTravelLeg.setId(new BigInteger(String.valueOf(i + 1)));
                decisionManagerTravelLeg.setOrigin(partCities[0]); // decisionManager_travelData_leg_#_origin
                decisionManagerTravelLeg.setDestination(partCities[1]); // decisionManager_travelData_leg_#_destination
                legs[i] = decisionManagerTravelLeg;

            }
            return legs;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     *
     * @param creditCardPaymentRequestList
     * @param address
     * @param bookedTravelersRequested
     * @param bookedLegList
     * @param toPaidItemsType
     * @param recordLocator
     * @param creationDate
     * @param cartId
     * @param fingerprintSessionId
     * @param ipAddr
     * @param transactionTime
     * @param storeCode
     * @return
     * @throws Exception
     */
    public List<RequestMessage> getTransformedRequestForCybersource(
            List<CreditCardPaymentRequest> creditCardPaymentRequestList,
            Address address,
            List<BookedTraveler> bookedTravelersRequested,
            List<BookedLeg> bookedLegList,
            ItemsToBePaidType toPaidItemsType,
            String recordLocator,
            String creationDate,
            String cartId,
            String fingerprintSessionId,
            String ipAddr,
            String transactionTime,
            String storeCode,
            String kioskId
    ) throws Exception {

        if (creditCardPaymentRequestList == null || creditCardPaymentRequestList.isEmpty()) {
            return new ArrayList<>();
        }

        try {

            List<RequestMessage> requestList = new ArrayList<>();
            for (CreditCardPaymentRequest creditCardPaymentRequest : creditCardPaymentRequestList) {
                if (null != creditCardPaymentRequest.getCcNumber()
                        && !creditCardPaymentRequest.getCcNumber().trim().isEmpty()) {
                    RequestMessage request = new RequestMessage();

                    request.setMerchantID(CybersourceConstants.MERCHANT_ID);

                    //Date current = FORMATTER_SABRE.parse(transactionTime);
                    //String strDate = new SimpleDateFormat("ddMMyy").format(current);
                    // FIXME booking pnr
                    request.setMerchantReferenceCode(cartId); // Aeromexico PNR
                    // To help us troubleshoot any problems that you may
                    // encounter,
                    // please include the following information about your
                    // application.
                    request.setClientLibrary("Java Axis WSS4J");
                    request.setClientLibraryVersion(CybersourceConstants.LIB_VERSION);
                    request.setClientEnvironment(System.getProperty("os.name") + "/" + System.getProperty("os.version")
                            + "/" + System.getProperty("java.vendor") + "/" + System.getProperty("java.version"));

                    //System.setProperty("axis.ClientConfigFile", "SampleDeploy.wsdd");
                    AFSService afsService = new AFSService();
                    afsService.setRun("true");
                    request.setAfsService(afsService);
                    // Device Fingerprint id
                    request.setDeviceFingerprintID((null == fingerprintSessionId || fingerprintSessionId.trim().isEmpty()) ? cartId : fingerprintSessionId.replace("aeromexico", ""));

                    // set items
                    List<Item> itemList = request.getItem();
                    itemList.addAll(
                            Arrays.asList(
                                    getPassengersItemsForCybersource(
                                            bookedTravelersRequested,
                                            toPaidItemsType
                                    )
                            )
                    );

                    String email;
                    if (!itemList.isEmpty()) {
                        email = itemList.get(0).getPassengerEmail();
                    } else {
                        email = "";
                    }

                    // Info to test - START
                    BillTo billTo = new BillTo();
                    String firstname;
                    String lastname;
                    try {
                        if (creditCardPaymentRequest.getCardHolderName().trim().contains(" ")) {
                            String aux = creditCardPaymentRequest.getCardHolderName().trim();
                            int index = aux.indexOf(" ");
                            firstname = aux.substring(0, index).trim();
                            lastname = aux.substring(index).trim();
                        } else {
                            firstname = creditCardPaymentRequest.getCardHolderName().trim();
                            lastname = firstname;
                        }
                    } catch (Exception ex) {
                        firstname = creditCardPaymentRequest.getCardHolderName().trim();
                        lastname = firstname;
                    }

                    billTo.setFirstName(firstname);
                    billTo.setLastName(lastname);
                    // billTo.setName(creditCardPaymentRequest.getCardHolderName());
                    billTo.setStreet1(address.getAddressOne());
                    billTo.setStreet2(address.getAddressTwo());
                    billTo.setCity(address.getCity());
                    billTo.setState(address.getState());
                    billTo.setPostalCode(address.getZipCode());
                    billTo.setCountry(address.getCountry());
                    billTo.setEmail((null == creditCardPaymentRequest.getEmail() || creditCardPaymentRequest.getEmail().trim().isEmpty()) ? email : creditCardPaymentRequest.getEmail());
                    billTo.setIpAddress(ipAddr);
                    if (null != creditCardPaymentRequest.getPhone()) {
                        billTo.setPhoneNumber(creditCardPaymentRequest.getPhone().getNumber());
                    } else {
                        billTo.setPhoneNumber("");
                    }
                    request.setBillTo(billTo);

                    Card card = new Card();
                    card.setAccountNumber(creditCardPaymentRequest.getCcNumber());
                    card.setExpirationMonth(new BigInteger(
                            String.valueOf(extractExpirationMonth(creditCardPaymentRequest.getExpiryDate()))));
                    card.setExpirationYear(new BigInteger(
                            String.valueOf(extractExpirationYear(creditCardPaymentRequest.getExpiryDate()))));
                    request.setCard(card);

                    PurchaseTotals purchaseTotals = new PurchaseTotals();
                    purchaseTotals.setCurrency(creditCardPaymentRequest.getPaymentAmount().getCurrencyCode());
                    purchaseTotals.setGrandTotalAmount(
                            String.valueOf(creditCardPaymentRequest.getPaymentAmount().getTotal()));
                    request.setPurchaseTotals(purchaseTotals);

                    MDDField[] mddField = callMDDFields(
                            creditCardPaymentRequestList.get(0),
                            bookedTravelersRequested,
                            bookedLegList,
                            recordLocator,
                            creationDate,
                            transactionTime,
                            storeCode,
                            kioskId
                    );

                    MerchantDefinedData merchantDefinedData = new MerchantDefinedData();

                    List<MDDField> MDDFIeldList = merchantDefinedData.getMddField();
                    MDDFIeldList.addAll(Arrays.asList(mddField));

                    request.setMerchantDefinedData(merchantDefinedData);

                    String completeRoute = mddField[4].getValue(); // complete
                    // route
                    DecisionManagerTravelData decisionManagerTravelData = new DecisionManagerTravelData();
                    decisionManagerTravelData.setCompleteRoute(completeRoute); // decisionManager_travelData_completeRoute

                    Segment segment = bookedLegList.get(0).getSegments().getCollection().get(0).getSegment();

                    Date departure = FORMATTER_SABRE.parse(segment.getDepartureDateTime());
                    decisionManagerTravelData.setDepartureDateTime(FORMATTER_CYBERSOURCE.format(departure)); // decisionManager_travelData_departureDateTime

                    if (bookedLegList.size() > 1) {
                        decisionManagerTravelData.setJourneyType("round trip"); // decisionManager_travelData_journeyType
                    } else {
                        decisionManagerTravelData.setJourneyType("one way"); // decisionManager_travelData_journeyType
                    }

                    DecisionManagerTravelLeg[] legs = getDecisionManagerTravelLeg(completeRoute);
                    List<DecisionManagerTravelLeg> legList = decisionManagerTravelData.getLeg();
                    legList.addAll(Arrays.asList(legs));

                    DecisionManager decisionManager = new DecisionManager();
                    decisionManager.setTravelData(decisionManagerTravelData);

                    request.setDecisionManager(decisionManager);

                    requestList.add(request);
                }
            }
            return requestList;
        } catch (Exception ex) {
            log.info("Error transforming cybersource request: " + ex.getMessage());
            throw ex;
        }
    }
}
