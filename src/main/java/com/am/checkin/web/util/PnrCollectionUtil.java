package com.am.checkin.web.util;

import com.aeromexico.codes.ICodesGenerator;
import com.aeromexico.codes.impl.CodesGenerator;
import com.aeromexico.commons.model.AbstractAncillary;
import com.aeromexico.commons.model.AbstractPaymentRequest;
import com.aeromexico.commons.model.AbstractSegmentChoice;
import com.aeromexico.commons.model.AuthorizationResult;
import com.aeromexico.commons.model.BaggageRoute;
import com.aeromexico.commons.model.BaggageRouteList;
import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.CabinUpgradeAncillary;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.CheckInStatus;
import com.aeromexico.commons.model.CreditCardPaymentRequest;
import com.aeromexico.commons.model.CurrencyValue;
import com.aeromexico.commons.model.EmdItem;
import com.aeromexico.commons.model.ExtraWeightAncillary;
import com.aeromexico.commons.model.Infant;
import com.aeromexico.commons.model.IssuedEmds;
import com.aeromexico.commons.model.PNR;
import com.aeromexico.commons.model.PNRCollection;
import com.aeromexico.commons.model.PaidSegmentChoice;
import com.aeromexico.commons.model.PaymentLinked;
import com.aeromexico.commons.model.PaymentRequestCollection;
import com.aeromexico.commons.model.SeatChoice;
import com.aeromexico.commons.model.SegmentChoice;
import com.aeromexico.commons.model.SegmentDocument;
import com.aeromexico.commons.model.TicketedSegmentChoice;
import com.aeromexico.commons.model.TicketedTravelerAncillary;
import com.aeromexico.commons.model.Total;
import com.aeromexico.commons.model.TravelerAncillary;
import com.aeromexico.commons.model.Warning;
import com.aeromexico.commons.model.WarningCollection;
import com.aeromexico.commons.model.utils.CurrencyUtil;
import com.aeromexico.commons.web.types.ActionCodeType;
import com.aeromexico.commons.web.types.AirlineCodeType;
import com.aeromexico.commons.web.types.AncillaryPostBookingType;
import com.aeromexico.commons.web.types.CheckinIneligibleReasons;
import com.aeromexico.commons.web.types.EmdReceiptType;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.types.PassengerFareType;
import com.aeromexico.commons.web.types.PosType;
import com.aeromexico.commons.web.types.PriorityCodeType;
import com.aeromexico.commons.web.types.SeatCharacteristicsType;
import com.aeromexico.commons.web.types.WeightUnitType;
import com.aeromexico.commons.web.util.FilterPassengersUtil;
import com.aeromexico.commons.web.util.FlightNumberUtil;
import com.aeromexico.commons.web.util.SegmentCodeUtil;
import com.aeromexico.commons.web.util.TimeUtil;
import com.sabre.services.acs.bso.checkinpassenger.v3.ACSCheckInPassengerRSACS;
import com.sabre.services.acs.bso.seatchange.v3.ACSSeatChangeRSACS;
import com.sabre.services.checkin.getpassengerdata.v4.PassengerDataResponseACS;
import com.sabre.services.micellaneous.FeeLinked;
import com.sabre.services.micellaneous.FeesLinked;
import com.sabre.services.micellaneous.FeesType;
import com.sabre.services.micellaneous.FlightMisc;
import com.sabre.services.stl.v3.BoardingPassItineraryDetailACS;
import com.sabre.services.stl.v3.FreeTextInfoACS;
import com.sabre.services.stl.v3.FreeTextInfoListACS;
import com.sabre.services.stl.v3.ItineraryDetailACS;
import com.sabre.services.stl.v3.ItineraryPassengerACS;
import com.sabre.services.stl.v3.PassengerDetailACS;
import com.sabre.services.stl.v3.TravelDocDataACS;
import com.sabre.webservices.pnrbuilder.updatereservation.AncillaryServicesPNRB;
import com.sabre.webservices.pnrbuilder.updatereservation.PassengerPNRB;
import com.sabre.webservices.pnrbuilder.updatereservation.ReservationPNRB;
import com.sabre.webservices.pnrbuilder.updatereservation.ResultsPNRB;
import com.sabre.webservices.pnrbuilder.updatereservation.UpdateReservationRS;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;

/**
 * @author adrian
 */
public class PnrCollectionUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(PnrCollectionUtil.class);
    private static final ICodesGenerator CODES_GENERATOR = new CodesGenerator();

    /**
     * @param paymentRequestCollection
     */
    public static void setDefaultCardInfo(PaymentRequestCollection paymentRequestCollection) {

        int index = 0;
        for (AbstractPaymentRequest abstractPaymentRequest : paymentRequestCollection.getCollection()) {
            if (abstractPaymentRequest instanceof CreditCardPaymentRequest) {
                CreditCardPaymentRequest creditCardPaymentRequest = (CreditCardPaymentRequest) abstractPaymentRequest;
                if (null == creditCardPaymentRequest.getExpiryDate()
                        || creditCardPaymentRequest.getExpiryDate().trim().isEmpty()
                        || !creditCardPaymentRequest.getExpiryDate().matches("\\d{4}-\\d{2}")) {

                    //set expiry date to december of the next year
                    Calendar now = Calendar.getInstance();          // Gets the current date and time
                    int year = now.get(Calendar.YEAR) + 1;        // The current year as an int

                    creditCardPaymentRequest.setExpiryDate("" + year + "-12");
                    creditCardPaymentRequest.setDefaultExpirationDateUsed(true);
                }

                if (null == creditCardPaymentRequest.getSecurityCode()) {
                    creditCardPaymentRequest.setSecurityCode("123");
                }

                if (null == creditCardPaymentRequest.getCardType()
                        || null == creditCardPaymentRequest.getCardCode()
                        || creditCardPaymentRequest.getCardCode().trim().isEmpty()) {

                    PurchaseOrderUtil.setCardCodeByCardPrefix(creditCardPaymentRequest);
                }

                paymentRequestCollection.getCollection().set(index, creditCardPaymentRequest);
            }
            index++;
        }
    }

    /**
     * @param segment
     * @return
     */
    public static String getLegCode(FlightMisc segment) {
        try {
            String flightNumber = segment.getFlightNumber();// (segment.getFlightNumber().length()
            // > 3 ?
            // segment.getFlightNumber()
            // : "0" +
            // segment.getFlightNumber());
            Date departureDate = segment.getDepartureDate().toGregorianCalendar().getTime();
            String departureDateStr = TimeUtil.getStrTime(departureDate.getTime(), "yyyy-MM-dd");
            String legCode = segment.getDepartureCity() + "_" + segment.getOperatingCarrierCode() + "_"
                    + StringUtils.leftPad(flightNumber, 4, "0") + "_" + departureDateStr;
            return legCode;
        } catch (Exception ex) {
            return "";
        }
    }

    /**
     * @param itineraryDetail
     * @return
     */
    public static String getLegCodeItineraryDetail(ItineraryDetailACS itineraryDetail) {
        try {
            String flightNumber = itineraryDetail.getFlight();// (itineraryDetail.getFlight().length()
            // > 3 ?
            // itineraryDetail.getFlight()
            // : "0" +
            // itineraryDetail.getFlight());
            String legCode = itineraryDetail.getOrigin() + "_" + itineraryDetail.getAirline() + "_"
                    + StringUtils.leftPad(flightNumber, 4, "0") + "_" + itineraryDetail.getDepartureDate();
            return legCode;
        } catch (Exception ex) {
            return "";
        }
    }

    /**
     * @param segment
     * @return
     */
    public static String getSegmentCode(FlightMisc segment) {
        try {
            Date departureDate = segment.getDepartureDate().toGregorianCalendar().getTime();
            String departureDateStr = TimeUtil.getStrTime(departureDate.getTime(), "yyyy-MM-dd_HHmm");
            String segmentCode = segment.getDepartureCity() + "_" + segment.getArrivalCity() + "_"
                    + segment.getOperatingCarrierCode() + "_" + departureDateStr;
            return segmentCode;
        } catch (Exception ex) {
            return "";
        }
    }

    /**
     * @param itineraryDetailACS
     * @param destination
     * @return
     */
    public static String getSegmentCode(ItineraryDetailACS itineraryDetailACS, String destination) {
        try {
            String arrival = (null != itineraryDetailACS.getDestination()) ? itineraryDetailACS.getDestination() : destination;
            if (null == arrival) {
                arrival = "XXX";
            }

            String departureDate = itineraryDetailACS.getDepartureDate();
            String segmentCode = itineraryDetailACS.getOrigin() + "_" + arrival + "_"
                    + itineraryDetailACS.getAirline() + "_" + departureDate;
            return segmentCode;
        } catch (Exception ex) {
            return "";
        }
    }

    /**
     * @param itineraryDetailACS
     * @param bookedSegmentList
     * @return
     */
    public static boolean matchOneSegment(ItineraryDetailACS itineraryDetailACS, List<BookedSegment> bookedSegmentList) {
        if (null == bookedSegmentList || bookedSegmentList.isEmpty() || null == itineraryDetailACS) {
            return false;
        }

        try {
            for (BookedSegment bookedSegment : bookedSegmentList) {
                String departureDateStr = TimeUtil.getStrTime(
                        bookedSegment.getSegment().getDepartureDateTime(),
                        "yyyy-MM-dd'T'HH:mm:ss",
                        "yyyy-MM-dd"
                );

                if (itineraryDetailACS.getOrigin().equalsIgnoreCase(bookedSegment.getSegment().getDepartureAirport())
                        && itineraryDetailACS.getDepartureDate().equalsIgnoreCase(departureDateStr)
                        && FlightNumberUtil.compareFlightNumbers(
                        itineraryDetailACS.getFlight(),
                        bookedSegment.getSegment().getOperatingFlightCode())) {
                    return true;
                }
            }

            return false;
        } catch (Exception ex) {
            return false;
        }
    }

    /**
     * @param itineraryPassengerList
     * @param baggageRoute
     * @return
     */
    public static ItineraryPassengerACS getItineraryPassengerACS(
            List<ItineraryPassengerACS> itineraryPassengerList,
            BaggageRoute baggageRoute
    ) {
        if (null == itineraryPassengerList || null == baggageRoute) {
            return null;
        }
        for (ItineraryPassengerACS itineraryPassengerACS : itineraryPassengerList) {
            try {
                if (baggageRoute.getOrigin().equalsIgnoreCase(itineraryPassengerACS.getItineraryDetail().getOrigin())
                        && baggageRoute.getDepartureDate().equalsIgnoreCase(
                        itineraryPassengerACS.getItineraryDetail().getDepartureDate()
                )) {
                    return itineraryPassengerACS;
                }
            } catch (Exception ex) {
                // ignore
                LOGGER.error(ex.getMessage());
            }
        }
        return null;
    }

    /**
     * @param baggageRouteList
     * @param itineraryPassengerACS
     * @return
     */
    public static BaggageRoute getBaggageRoute(
            BaggageRouteList baggageRouteList,
            ItineraryPassengerACS itineraryPassengerACS
    ) {
        if (null == baggageRouteList || null == itineraryPassengerACS) {
            return null;
        }
        for (BaggageRoute baggageRoute : baggageRouteList) {
            try {
                if (baggageRoute.getOrigin().equalsIgnoreCase(itineraryPassengerACS.getItineraryDetail().getOrigin())
                        && baggageRoute.getDepartureDate().equalsIgnoreCase(
                        itineraryPassengerACS.getItineraryDetail().getDepartureDate()
                )) {
                    return baggageRoute;
                }
            } catch (Exception ex) {
                // ignore
                LOGGER.error(ex.getMessage());
            }
        }
        return null;
    }

    /**
     * @param baggageRouteList
     * @param segmentCode
     * @return
     */
    public static BaggageRoute getBaggageRoute(
            BaggageRouteList baggageRouteList,
            String segmentCode
    ) {
        if (null == baggageRouteList || null == segmentCode) {
            return null;
        }
        for (BaggageRoute baggageRoute : baggageRouteList) {
            try {
                if (SegmentCodeUtil.compareSegmentCodeWithoutTime(segmentCode, baggageRoute.getSegmentCode())) {
                    return baggageRoute;
                }
            } catch (Exception ex) {
                // ignore
                LOGGER.error(ex.getMessage());
            }
        }
        return null;
    }

    /**
     * @param acsCheckInPassengerRSACSList
     * @param baggageRoute
     * @return
     */
    public static PassengerDetailACS getPassengerDetailACS(
            List<ACSCheckInPassengerRSACS> acsCheckInPassengerRSACSList,
            BaggageRoute baggageRoute
    ) {
        try {
            for (ACSCheckInPassengerRSACS aCSCheckInPassengerRSACS : acsCheckInPassengerRSACSList) {
                ItineraryPassengerACS itineraryPassengerACS = getItineraryPassengerACS(
                        aCSCheckInPassengerRSACS.getItineraryPassengerList().getItineraryPassenger(),
                        baggageRoute
                );
                if (null != itineraryPassengerACS) {
                    for (PassengerDetailACS passengerDetailACS : itineraryPassengerACS.getPassengerDetailList().getPassengerDetail()) {
                        try {
                            if (passengerDetailACS.getPassengerID().equalsIgnoreCase(baggageRoute.getPassengerID())) {
                                return passengerDetailACS;
                            }
                        } catch (Exception ex) {
                            //
                            LOGGER.error(ex.getMessage());
                        }
                    }
                }
            }
        } catch (Exception ex) {
            // ignore
        }
        return null;
    }

    /**
     * @param paxType
     * @param TravelDocDataACSList
     * @return
     */
    public static List<TravelDocDataACS> getTravelDocDataACS(List<String> paxType, List<TravelDocDataACS> TravelDocDataACSList) {
        if (null == TravelDocDataACSList || null == paxType || paxType.isEmpty()) {
            return null;
        }
        List<TravelDocDataACS> travelDocDataACSList = new ArrayList<>();
        for (TravelDocDataACS travelDocDataACS : TravelDocDataACSList) {
            if (paxType.contains(travelDocDataACS.getType())) {
                travelDocDataACSList.add(travelDocDataACS);
            }
        }
        return travelDocDataACSList;
    }

    /**
     * @param freeTextInfoListACS
     * @return
     */
    public static String getZone(FreeTextInfoListACS freeTextInfoListACS) {
        try {
            for (FreeTextInfoACS freeTextInfoACS : freeTextInfoListACS.getFreeTextInfo()) {
                try {
                    for (String text : freeTextInfoACS.getTextLine().getText()) {
                        if (StringUtils.containsIgnoreCase(text, "ZONE") || StringUtils.containsIgnoreCase(text, "ZONA")) {
                            return text.replaceAll("\\D+", "");
                        }
                    }
                } catch (Exception ex) {
                    // ignore
                }
            }
        } catch (Exception ex) {
            // ignore
        }
        return "";
    }

    /**
     * @param baggageRoute
     * @param boardingPassItineraryDetailACS
     * @return
     */
    public static boolean isSameSegment(BaggageRoute baggageRoute,
                                        BoardingPassItineraryDetailACS boardingPassItineraryDetailACS
    ) {
        try {
            if (!baggageRoute.getDepartureDate()
                    .equalsIgnoreCase(boardingPassItineraryDetailACS.getEstimatedDepartureDate())) {
                return false;
            }
            if (!baggageRoute.getOrigin().equalsIgnoreCase(boardingPassItineraryDetailACS.getOrigin())) {
                return false;
            }
            return baggageRoute.getDestination().equalsIgnoreCase(boardingPassItineraryDetailACS.getDestination());
        } catch (Exception ex) {
            //
        }
        return false;
    }

    /**
     * @param departureDate
     * @param origin
     * @param destination
     * @param boardingPassItineraryDetailACS
     * @return
     */
    public static boolean isSameSegment(
            String departureDate,
            String origin,
            String destination,
            BoardingPassItineraryDetailACS boardingPassItineraryDetailACS
    ) {
        try {
            if (!departureDate
                    .equalsIgnoreCase(boardingPassItineraryDetailACS.getEstimatedDepartureDate())) {
                return false;
            }
            if (!origin.equalsIgnoreCase(boardingPassItineraryDetailACS.getOrigin())) {
                return false;
            }
            return destination.equalsIgnoreCase(boardingPassItineraryDetailACS.getDestination());
        } catch (Exception ex) {
            //
        }
        return false;
    }

    /**
     * @param baggageRoute
     * @param boardingPassItineraryDetailACSList
     * @return
     */
    public static BoardingPassItineraryDetailACS getBoardingPassItineraryDetail(
            BaggageRoute baggageRoute,
            List<BoardingPassItineraryDetailACS> boardingPassItineraryDetailACSList
    ) {
        if (null == boardingPassItineraryDetailACSList || null == baggageRoute) {
            return null;
        }
        for (BoardingPassItineraryDetailACS boardingPassItineraryDetailACS : boardingPassItineraryDetailACSList) {
            if (isSameSegment(baggageRoute, boardingPassItineraryDetailACS)) {
                return boardingPassItineraryDetailACS;
            }
        }
        return null;
    }

    /**
     * @param departureDate
     * @param origin
     * @param destination
     * @param boardingPassItineraryDetailACSList
     * @return
     */
    public static BoardingPassItineraryDetailACS getBoardingPassItineraryDetail(
            String departureDate, String origin, String destination,
            List<BoardingPassItineraryDetailACS> boardingPassItineraryDetailACSList
    ) {
        if (null == boardingPassItineraryDetailACSList || null == departureDate || null == origin || null == destination) {
            return null;
        }

        for (BoardingPassItineraryDetailACS boardingPassItineraryDetailACS : boardingPassItineraryDetailACSList) {
            if (isSameSegment(departureDate, origin, destination, boardingPassItineraryDetailACS)) {
                return boardingPassItineraryDetailACS;
            }
        }
        return null;
    }

    /**
     * @param segmentCode
     * @param collection
     * @return
     */
    public static AbstractSegmentChoice getAbstractSegmentChoice(
            String segmentCode, List<AbstractSegmentChoice> collection
    ) {
        for (AbstractSegmentChoice abstractSegmentChoice : collection) {
            if (SegmentCodeUtil.compareSegmentCodeWithoutTime(abstractSegmentChoice.getSegmentCode(),
                    segmentCode)) {
                return abstractSegmentChoice;
            }
        }
        return null;
    }

    /**
     * @return
     */
    public static List<SeatCharacteristicsType> getSeatCharacteristics() {
        List<SeatCharacteristicsType> seatCharacteristics = new ArrayList<>();
        // TODO: Set here the logic to put the seat characteristics.
        seatCharacteristics.add(SeatCharacteristicsType.A);
        return seatCharacteristics;
    }

    /**
     * @param bookedTravelerList
     * @param refName
     * @return
     */
    public static BookedTraveler getBookedTravelerByRefName(
            List<BookedTraveler> bookedTravelerList, String refName
    ) {
        if (null == bookedTravelerList || null == refName || refName.trim().isEmpty()) {
            return null;
        }
        for (BookedTraveler bookedTraveler : bookedTravelerList) {
            if (refName.equalsIgnoreCase(bookedTraveler.getNameRefNumber())) {
                return bookedTraveler;
            }
        }
        return null;
    }

    /**
     * @param bookedTravelerList
     * @param refName
     * @return
     */
    public static int getBookedTravelerIndexByRefName(List<BookedTraveler> bookedTravelerList, String refName) {
        if (null == bookedTravelerList || null == refName || refName.trim().isEmpty()) {
            return -1;
        }
        int index = 0;
        for (BookedTraveler bookedTraveler : bookedTravelerList) {
            if (refName.equalsIgnoreCase(bookedTraveler.getNameRefNumber())) {
                return index;
            }
            index++;
        }
        return -1;
    }

    /**
     * @param acsSeatChangeRSACS
     * @return
     */
    public static ACSCheckInPassengerRSACS transformCheckInPassengerRS(ACSSeatChangeRSACS acsSeatChangeRSACS) {
        ACSCheckInPassengerRSACS acsCheckInPassengerRSACS = new ACSCheckInPassengerRSACS();
        acsCheckInPassengerRSACS.setFreeTextInfoList(acsSeatChangeRSACS.getFreeTextInfoList());
        acsCheckInPassengerRSACS.setItineraryPassengerList(acsSeatChangeRSACS.getItineraryPassengerList());
        acsCheckInPassengerRSACS.setPECTABDataList(acsSeatChangeRSACS.getPECTABDataList());
        acsCheckInPassengerRSACS.getFlightSeatMapInfoList().addAll(acsSeatChangeRSACS.getFlightSeatMapInfoList());
        acsCheckInPassengerRSACS.setResult(acsSeatChangeRSACS.getResult());
        return acsCheckInPassengerRSACS;
    }

    /**
     * @param from
     * @param to
     * @throws Exception
     */
    public static void copyTouchedTravelers(List<BookedTraveler> from, List<BookedTraveler> to) throws Exception {
        if (null == from || from.isEmpty()) {
            return;
        }

        for (BookedTraveler bookedTraveler : from) {
            try {
                if (null != bookedTraveler && bookedTraveler.isTouched()) {
                    bookedTraveler.setTouched(false);
                    int index = getBookedTravelerIndexByRefName(to, bookedTraveler.getNameRefNumber());
                    if (index >= 0) {
                        to.set(index, bookedTraveler);
                    }
                }
            } catch (Exception ex) {
                //
            }
        }
    }

    /**
     * @param passengersSuccessfulCheckedIn
     * @param from
     * @param to
     * @throws Exception
     */
    public static void copyCheckinInfoFromCheckedInTravelers(
            Set<BookedTraveler> passengersSuccessfulCheckedIn,
            List<BookedTraveler> from,
            List<BookedTraveler> to
    ) throws Exception {

        if (null == passengersSuccessfulCheckedIn || passengersSuccessfulCheckedIn.isEmpty() || null == from
                || from.isEmpty() || null == to || to.isEmpty()) {
            return;
        }

        for (BookedTraveler bookedTravelerSuccessfulChecked : passengersSuccessfulCheckedIn) {
            try {
                BookedTraveler bookedTravelerLocal = getBookedTravelerByRefName(
                        from,
                        bookedTravelerSuccessfulChecked.getNameRefNumber()
                );

                BookedTraveler bookedTravelerLocalPersisted = getBookedTravelerByRefName(
                        to,
                        bookedTravelerSuccessfulChecked.getNameRefNumber()
                );

                if (null != bookedTravelerLocal && null != bookedTravelerLocalPersisted) {
                    bookedTravelerLocalPersisted.setCheckinStatus(bookedTravelerLocal.isCheckinStatus());

                    //review the override of segments choices
                    for (AbstractSegmentChoice abstractSegmentChoice : bookedTravelerLocal.getSegmentChoices().getCollection()) {
                        int index = bookedTravelerLocalPersisted.getSegmentChoices().getIndexBySegmentCode(abstractSegmentChoice.getSegmentCode());

                        if (index >= 0) {
                            bookedTravelerLocalPersisted.getSegmentChoices().getCollection().set(index, abstractSegmentChoice);
                        } else {
                            bookedTravelerLocalPersisted.getSegmentChoices().getCollection().add(abstractSegmentChoice);
                        }
                    }

                    for (SegmentDocument segmentDocument : bookedTravelerLocal.getSegmentDocumentsList()) {
                        bookedTravelerLocalPersisted.getSegmentDocumentsList().addOrReplace(segmentDocument);
                    }
                }
            } catch (Exception ex) {
                //
            }
        }
    }

    /**
     * @param passengerDetailACS
     * @param bookedTravelerList
     * @return
     */
    private static BookedTraveler getBookedTraveler(
            PassengerDetailACS passengerDetailACS,
            List<BookedTraveler> bookedTravelerList
    ) {
        for (BookedTraveler bookedTraveler : bookedTravelerList) {
            if (null != bookedTraveler.getBaggageRouteList()) {
                for (BaggageRoute baggageRoute : bookedTraveler.getBaggageRouteList()) {
                    if (passengerDetailACS.getPassengerID().equalsIgnoreCase(baggageRoute.getPassengerID())) {
                        return bookedTraveler;
                    }
                }
            }

            if (passengerDetailACS.getLastName().equalsIgnoreCase(bookedTraveler.getLastName())
                    && passengerDetailACS.getFirstName().equalsIgnoreCase(bookedTraveler.getFirstName())) {
                return bookedTraveler;
            }
        }

        return null;
    }

    public static void setCheckInStatusBySegment(BookedTraveler bookedTraveler, String segmentCode, boolean checkinStatus) {
        CheckInStatus checkInStatus = bookedTraveler.getCheckinStatusBySegment().getCheckInStatus(
                segmentCode
        );

        if (null != checkInStatus) {
            checkInStatus.setCheckinStatus(checkinStatus);
        } else {
            checkInStatus = new CheckInStatus();
            checkInStatus.setSegmentCode(segmentCode);
            checkInStatus.setCheckinStatus(checkinStatus);
            bookedTraveler.getCheckinStatusBySegment().getCollection().add(checkInStatus);
        }
    }

    public static void setCheckInStatusBySegment(Infant bookedTraveler, String segmentCode, boolean checkinStatus) {
        CheckInStatus checkInStatus = bookedTraveler.getCheckinStatusBySegment().getCheckInStatus(
                segmentCode
        );

        if (null != checkInStatus) {
            checkInStatus.setCheckinStatus(checkinStatus);
        } else {
            checkInStatus = new CheckInStatus();
            checkInStatus.setSegmentCode(segmentCode);
            checkInStatus.setCheckinStatus(checkinStatus);
            bookedTraveler.getCheckinStatusBySegment().getCollection().add(checkInStatus);
        }
    }

    /**
     * @param passengersSuccessfulCheckedIn
     * @param acsCheckInPassengerRSACSList
     * @param bookedTravelerList
     * @param bookedLeg
     * @param pos
     * @param recordLocator
     * @param tsaRequired
     * @param warningCollection
     * @return
     * @throws Exception
     */
    public static boolean updateAfterSuccessCheckin(
            Set<BookedTraveler> passengersSuccessfulCheckedIn,
            List<ACSCheckInPassengerRSACS> acsCheckInPassengerRSACSList,
            List<BookedTraveler> bookedTravelerList,
            BookedLeg bookedLeg,
            String pos,
            String recordLocator,
            boolean tsaRequired,
            WarningCollection warningCollection
    ) throws Exception {

        boolean cartTouched = false;

        if (null == acsCheckInPassengerRSACSList) {
            LOGGER.error("No checkin response");
            return cartTouched;
        }

        if (null == passengersSuccessfulCheckedIn || passengersSuccessfulCheckedIn.isEmpty()) {
            LOGGER.error("No passengers successfully checked in");
            return cartTouched;
        }

        if (null == bookedLeg) {
            LOGGER.error("BookedLeg required in order to associate boarding passes");
            return cartTouched;
        }

        for (ACSCheckInPassengerRSACS aCSCheckInPassengerRSACS : acsCheckInPassengerRSACSList) {
            int itinerary = 0;

            if (null != aCSCheckInPassengerRSACS.getItineraryPassengerList()
                    && null != aCSCheckInPassengerRSACS.getItineraryPassengerList().getItineraryPassenger()) {

                for (ItineraryPassengerACS itineraryPassengerACS : aCSCheckInPassengerRSACS.getItineraryPassengerList().getItineraryPassenger()) {

                    ItineraryDetailACS itineraryDetailACS = itineraryPassengerACS.getItineraryDetail();
                    itinerary++;

                    if (matchOneSegment(itineraryDetailACS, bookedLeg.getSegments().getCollection())) {

                        String origin = itineraryDetailACS.getOrigin();
                        String destination = itineraryDetailACS.getDestination();
                        String departureDate = itineraryDetailACS.getDepartureDate();

                        for (PassengerDetailACS passengerDetailACS : itineraryPassengerACS.getPassengerDetailList().getPassengerDetail()) {
                            if (!searchForText(
                                    passengerDetailACS,
                                    ErrorType.PASSENGER_ALREADY_CHECKED.getSearchableCode()
                            )) {

                                BookedTraveler bookedTraveler = getBookedTraveler(passengerDetailACS, bookedTravelerList);

                                if (null != bookedTraveler) {
                                    BaggageRoute baggageRoute;
                                    baggageRoute = getBaggageRoute(
                                            bookedTraveler.getBaggageRouteList(),
                                            itineraryPassengerACS
                                    );

                                    short segmentId;
                                    String segmentCode;

                                    if (null != baggageRoute) {
                                        segmentId = baggageRoute.getSegmentID();
                                        segmentCode = baggageRoute.getSegmentCode();
                                        if (null == destination) {
                                            destination = baggageRoute.getDestination();
                                        }
                                    } else {
                                        segmentId = (short) itinerary;
                                        segmentCode = getSegmentCode(itineraryDetailACS, passengerDetailACS.getDestination());
                                        if (null == destination) {
                                            destination = passengerDetailACS.getDestination();
                                        }
                                    }

                                    cartTouched = true;
                                    AbstractSegmentChoice abstractSegmentChoice;
                                    abstractSegmentChoice = getAbstractSegmentChoice(
                                            segmentCode,
                                            bookedTraveler.getSegmentChoices().getCollection()
                                    );

                                    boolean flagPassengerOvs = (PassengerFareType.F.getCode().equalsIgnoreCase(bookedTraveler.getPassengerType()) && bookedTraveler.isOnStandByList());

                                    if (null == abstractSegmentChoice) {
                                        SeatChoice seatChoice = new SeatChoice();

                                        if (PassengerFareType.E.getCode().equalsIgnoreCase(bookedTraveler.getPassengerType()) || flagPassengerOvs ){
                                            seatChoice.setCode("STB");
                                        } else {
                                            seatChoice.setCode(passengerDetailACS.getSeat());
                                        }

                                        PaidSegmentChoice paidSegmentChoice = new PaidSegmentChoice();
                                        paidSegmentChoice.setSeat(seatChoice);
                                        paidSegmentChoice.setSegmentCode(segmentCode);
                                        bookedTraveler.getSegmentChoices().getCollection().add(paidSegmentChoice);
                                    } else {

                                        if (PassengerFareType.E.getCode().equalsIgnoreCase(bookedTraveler.getPassengerType()) || flagPassengerOvs) {
                                            abstractSegmentChoice.getSeat().setCode("STB");
                                        } else {
                                            abstractSegmentChoice.getSeat().setCode(passengerDetailACS.getSeat());
                                        }
                                        abstractSegmentChoice.setSegmentCode(segmentCode);
                                    }

                                    boolean checkInStatus = "*".equalsIgnoreCase(passengerDetailACS.getBoardingPassFlag())
                                            || "-".equalsIgnoreCase(passengerDetailACS.getBoardingPassFlag());

                                    if (1 == itinerary) {
                                        if (PassengerFareType.E.getCode().equalsIgnoreCase(bookedTraveler.getPassengerType())) {
                                            bookedTraveler.setCheckinStatus(true);
                                        } else {
                                            bookedTraveler.setCheckinStatus(
                                                    checkInStatus
                                            );
                                        }
                                    }

                                    setCheckInStatusBySegment(bookedTraveler, segmentCode, checkInStatus);

                                    if (null != passengerDetailACS.getTravelDocDataList()
                                            && null != passengerDetailACS.getTravelDocDataList().getTravelDocData()
                                            && !passengerDetailACS.getTravelDocDataList().getTravelDocData().isEmpty()) {

                                        List<TravelDocDataACS> travelDocDataACSList;
                                        travelDocDataACSList = getTravelDocDataACS(
                                                Arrays.asList("BPD", "PVC"),
                                                passengerDetailACS.getTravelDocDataList().getTravelDocData()
                                        );

                                        if (null != travelDocDataACSList && !travelDocDataACSList.isEmpty()) {
                                            for (TravelDocDataACS travelDocDataACS : travelDocDataACSList) {
                                                SegmentDocument segmentInfo = new SegmentDocument();
                                                segmentInfo.setSegmentId(segmentId);
                                                String zone = getZone(passengerDetailACS.getFreeTextInfoList());
                                                segmentInfo.setBoardingZone(zone);
                                                segmentInfo.setGate(passengerDetailACS.getDepartureGate());
                                                segmentInfo.setCheckInNumber(String.valueOf(passengerDetailACS.getCheckInNumber()));
                                                segmentInfo.setSegmentCode(segmentCode);
                                                segmentInfo.setCheckinStatus(
                                                        "*".equalsIgnoreCase(passengerDetailACS.getBoardingPassFlag())
                                                                || "-".equalsIgnoreCase(passengerDetailACS.getBoardingPassFlag())
                                                );
                                                segmentInfo.setTsaPreCheck(false);
                                                segmentInfo.setSkyPriority(false);
                                                String boardingPass = null;

                                                try {
                                                    if (null != travelDocDataACS.getXMLDigitalSignatureData()) {
                                                        segmentInfo.setTravelDoc(travelDocDataACS.getXMLDigitalSignatureData().getBoardingPassSource());
                                                        boardingPass = travelDocDataACS.getXMLDigitalSignatureData().getBarcodeInfo().getBarcode();

                                                        try {

                                                            BoardingPassItineraryDetailACS boardingPassItineraryDetailACS;
                                                            boardingPassItineraryDetailACS = getBoardingPassItineraryDetail(
                                                                    departureDate, origin, destination,
                                                                    travelDocDataACS.getXMLDigitalSignatureData().getBoardingPassItineraryDetail()
                                                            );

                                                            if (null != boardingPassItineraryDetailACS) {
                                                                segmentInfo.setGroupZone(
                                                                        boardingPassItineraryDetailACS.getGroupZone());
                                                                segmentInfo
                                                                        .setTerminal(boardingPassItineraryDetailACS.getTerminal());
                                                                segmentInfo.setGate(boardingPassItineraryDetailACS.getGate());
                                                            }

                                                        } catch (Exception ex) {
                                                            LOGGER.error(ex.getMessage());
                                                        }

                                                    } else if (null != travelDocDataACS.getTravelDoc()) {

                                                        try {
                                                            String travelDoc = travelDocDataACS.getTravelDoc();
                                                            segmentInfo.setTravelDoc(travelDoc);
                                                            int start = travelDoc.indexOf("^P3");
                                                            boardingPass = travelDoc.substring(start + 3);
                                                            int end = boardingPass.indexOf("^");
                                                            boardingPass = boardingPass.substring(0, end);
                                                        } catch (Exception ex) {
                                                            LOGGER.error(ex.getMessage());
                                                            boardingPass = null;
                                                        }

                                                        try {
                                                            String travelDoc = travelDocDataACS.getTravelDoc();
                                                            int tsaString = travelDoc.indexOf("^32");
                                                            String tsaPreCheckString = travelDoc.substring(tsaString + 3);
                                                            segmentInfo.setTsaPreCheck(tsaPreCheckString.contains("TSA PRECHK"));
                                                            int skyStart = travelDoc.indexOf("^45");
                                                            String skyPriorityString = travelDoc.substring(skyStart + 3);
                                                            segmentInfo.setSkyPriority(skyPriorityString.contains("SKY"));
                                                        } catch (Exception ex) {
                                                            LOGGER.error(ex.getMessage());
                                                        }

                                                    } else if (PassengerFareType.E.getCode().equalsIgnoreCase(bookedTraveler.getPassengerType())
                                                            && isPassengerOnPriorityList(passengerDetailACS)) {
                                                        segmentInfo = getStbSegmentDocument(
                                                                bookedTraveler,
                                                                bookedLeg,
                                                                recordLocator,
                                                                segmentCode
                                                        );
                                                    }
                                                } catch (Exception ex) {
                                                    LOGGER.error(ex.getMessage());
                                                }

                                                if (null != boardingPass
                                                        && !boardingPass.trim().isEmpty()
                                                        && !boardingPass.contains("NOT VALID WITHOUT FLIGHT COUPON")) {

                                                    try {
                                                        segmentInfo.setBarcode(boardingPass);
                                                        segmentInfo.setBarcodeImage(
                                                                CODES_GENERATOR.codesGenerator2D(boardingPass)
                                                        );
                                                        segmentInfo.setQrcodeImage(
                                                                CODES_GENERATOR.codesGeneratorQR(boardingPass)
                                                        );
                                                    } catch (Exception ex) {
                                                        LOGGER.error(ex.getMessage());
                                                    }

                                                } else {
                                                    LOGGER.error("Boarding pass parser error, passengerID: " + passengerDetailACS.getPassengerID());
                                                }

                                                if (tsaRequired) {
                                                    bookedTraveler.getSegmentDocumentsListTSA().addOrReplace(segmentInfo);
                                                    if (PosType.KIOSK.toString().equalsIgnoreCase(pos)) {
                                                        Warning warning = new Warning(ErrorType.TSA_VALIDATION_REQUIRED.getErrorCode(), ErrorType.TSA_VALIDATION_REQUIRED.getFullDescription());
                                                        warningCollection.getCollection().add(warning);
                                                    }
                                                } else {
                                                    bookedTraveler.getSegmentDocumentsList().addOrReplace(segmentInfo);
                                                }
                                            }
                                        } else {
                                            LOGGER.error("No traveler doc found for adult, passengerID: " + passengerDetailACS.getPassengerID());
                                        }

                                        if (null != bookedTraveler.getInfant()) {
                                            travelDocDataACSList = getTravelDocDataACS(
                                                    Arrays.asList("INF"),
                                                    passengerDetailACS.getTravelDocDataList().getTravelDocData()
                                            );

                                            if (null != travelDocDataACSList && !travelDocDataACSList.isEmpty()) {
                                                for (TravelDocDataACS travelDocDataACS : travelDocDataACSList) {
                                                    SegmentDocument segmentBoardingPassInf = new SegmentDocument();
                                                    segmentBoardingPassInf.setSegmentId(segmentId);
                                                    String zone = getZone(passengerDetailACS.getFreeTextInfoList());
                                                    segmentBoardingPassInf.setBoardingZone(zone);
                                                    segmentBoardingPassInf.setGate(passengerDetailACS.getDepartureGate());
                                                    segmentBoardingPassInf.setCheckinStatus(
                                                            "*".equalsIgnoreCase(passengerDetailACS.getBoardingPassFlag())
                                                                    || "-".equalsIgnoreCase(passengerDetailACS.getBoardingPassFlag())
                                                    );
                                                    segmentBoardingPassInf.setSegmentCode(segmentCode);
                                                    String boardingPass = null;
                                                    try {
                                                        if (null != travelDocDataACS.getXMLDigitalSignatureData()
                                                                && null != travelDocDataACS.getXMLDigitalSignatureData()
                                                                .getBarcodeInfo()
                                                                && null != travelDocDataACS.getXMLDigitalSignatureData()
                                                                .getBarcodeInfo().getBarcode()
                                                                && !travelDocDataACS.getXMLDigitalSignatureData().getBarcodeInfo()
                                                                .getBarcode().isEmpty()) {
                                                            segmentBoardingPassInf.setTravelDoc(travelDocDataACS.getXMLDigitalSignatureData().getBoardingPassSource());
                                                            boardingPass = travelDocDataACS.getXMLDigitalSignatureData()
                                                                    .getBarcodeInfo().getBarcode();
                                                        } else if (null != travelDocDataACS.getTravelDoc()) {
                                                            try {
                                                                String travelDoc = travelDocDataACS.getTravelDoc();
                                                                segmentBoardingPassInf.setTravelDoc(travelDoc);
                                                                int start = travelDoc.indexOf("^P3");
                                                                boardingPass = travelDoc.substring(start + 3);
                                                                int end = boardingPass.indexOf("^");
                                                                boardingPass = boardingPass.substring(0, end);
                                                            } catch (Exception ex) {
                                                                LOGGER.error(ex.getMessage());
                                                                boardingPass = null;
                                                            }
                                                            try {
                                                                String travelDoc = travelDocDataACS.getTravelDoc();
                                                                int tsaString = travelDoc.indexOf("^32");
                                                                String tsaPreCheckString = travelDoc.substring(tsaString + 3);
                                                                segmentBoardingPassInf
                                                                        .setTsaPreCheck(tsaPreCheckString.contains("TSA PRECHK"));
                                                                int skyStart = travelDoc.indexOf("^45");
                                                                String skyPriorityString = travelDoc.substring(skyStart + 3);
                                                                segmentBoardingPassInf.setSkyPriority(skyPriorityString.contains("SKY"));
                                                            } catch (Exception e) {
                                                                LOGGER.error(e.getMessage());
                                                            }
                                                        } else if (PassengerFareType.E.getCode().equalsIgnoreCase(bookedTraveler.getPassengerType())) {

                                                        }
                                                    } catch (Exception ex) {
                                                        LOGGER.error(ex.getMessage());
                                                    }

                                                    if (null != boardingPass
                                                            && !boardingPass.trim().isEmpty()
                                                            && !boardingPass.contains("NOT VALID WITHOUT FLIGHT COUPON")) {
                                                        try {
                                                            segmentBoardingPassInf.setBarcode(boardingPass);
                                                            segmentBoardingPassInf.setBarcodeImage(
                                                                    CODES_GENERATOR.codesGenerator2D(boardingPass)
                                                            );
                                                            segmentBoardingPassInf.setQrcodeImage(
                                                                    CODES_GENERATOR.codesGeneratorQR(boardingPass)
                                                            );
                                                        } catch (Exception ex) {
                                                            LOGGER.error(ex.getMessage());
                                                        }
                                                    } else {
                                                        LOGGER.error("Boarding pass parser error, passengerID: " + passengerDetailACS.getPassengerID());
                                                    }

                                                    if (tsaRequired) {
                                                        bookedTraveler.getInfant().getSegmentDocumentsListTSA().addOrReplace(segmentBoardingPassInf);
                                                        if (PosType.KIOSK.toString().equalsIgnoreCase(pos)) {
                                                            Warning warning = new Warning(ErrorType.TSA_VALIDATION_REQUIRED.getErrorCode(), ErrorType.TSA_VALIDATION_REQUIRED.getFullDescription());
                                                            warningCollection.getCollection().add(warning);
                                                        }
                                                    } else {
                                                        bookedTraveler.getInfant().getSegmentDocumentsList().addOrReplace(segmentBoardingPassInf);
                                                    }
                                                }
                                            } else {
                                                LOGGER.error("No traveler doc found for infant, passengerID: " + passengerDetailACS.getPassengerID());
                                            }
                                        }
                                    } else {
                                        LOGGER.error("No traveler doc returned, passengerID: " + passengerDetailACS.getPassengerID());
                                    }
                                } else {
                                    LOGGER.error("Passenger checked in not found in travellers collection, passengerID: " + passengerDetailACS.getPassengerID());
                                }
                            } else {
                                LOGGER.error("Passenger already checked in, passengerID: " + passengerDetailACS.getPassengerID());
                                cartTouched = true;
                            }
                        }
                    }
                }
            }
        }

        return cartTouched;
    }

    private static boolean isPassengerOnPriorityList(PassengerDetailACS passengerDetailACS) {
        if (searchForText(
                passengerDetailACS,
                ErrorType.PASSENGERS_ADDED_TO_THE_PRIORITY_LIST.getSearchableCode()
        )) {
            return true;
        }

        if (searchForText(
                passengerDetailACS,
                ErrorType.PASSENGER_ALREADY_ON_PRIORITY_LIST.getSearchableCode()
        )) {
            return true;
        }

        return false;
    }

    /**
     * @param bookedTravelerWithClassE
     * @param bookedLeg
     * @param recordLocator
     * @param segmentCode
     * @return
     */
    private static SegmentDocument getStbSegmentDocument(
            BookedTraveler bookedTravelerWithClassE,
            BookedLeg bookedLeg,
            String recordLocator,
            String segmentCode
    ) {

        BookedSegment bookedSegment = bookedLeg.getSegments().getFirstSegmentOperatedBy(AirlineCodeType.AM);

        String travelDoc = BoardingPassUtil.getStandbyBoardingpassPectab(
                bookedTravelerWithClassE, bookedSegment, recordLocator
        );

        SegmentDocument segmentDocument = new SegmentDocument();
        segmentDocument.setSegmentCode(segmentCode);
        segmentDocument.setTravelDoc(travelDoc);
        segmentDocument.setCheckinStatus(true);
        String boardingPass;
        try {
            int start = travelDoc.indexOf("^P3");
            boardingPass = travelDoc.substring(start + 3);
            int end = boardingPass.indexOf("^");
            boardingPass = boardingPass.substring(0, end);
        } catch (Exception ex1) {
            LOGGER.error(ex1.getMessage());
            boardingPass = null;
        }
        if (null != boardingPass
                && !boardingPass.trim().isEmpty()
                && !boardingPass.contains("NOT VALID WITHOUT FLIGHT COUPON")) {
            try {
                segmentDocument.setBarcode(boardingPass);
                segmentDocument.setBarcodeImage(
                        CODES_GENERATOR.codesGenerator2D(boardingPass)
                );
                segmentDocument.setQrcodeImage(
                        CODES_GENERATOR.codesGeneratorQR(boardingPass)
                );
            } catch (Exception ex1) {
                LOGGER.error(ex1.getMessage());
            }
        }
        return segmentDocument;
    }

    /**
     * @param passengerDetailACS
     * @param search
     * @return
     */
    public static boolean searchForText(PassengerDetailACS passengerDetailACS, String search) {
        if (null == search) {
            return false;
        }
        try {
            if (null != passengerDetailACS.getFreeTextInfoList()
                    && null != passengerDetailACS.getFreeTextInfoList().getFreeTextInfo()
                    && !passengerDetailACS.getFreeTextInfoList().getFreeTextInfo().isEmpty()) {
                for (FreeTextInfoACS freeTextInfoACS : passengerDetailACS.getFreeTextInfoList().getFreeTextInfo()) {
                    if (null != freeTextInfoACS.getTextLine() && null != freeTextInfoACS.getTextLine().getText()
                            && !freeTextInfoACS.getTextLine().getText().isEmpty()) {
                        for (String text : freeTextInfoACS.getTextLine().getText()) {
                            if (StringUtils.containsIgnoreCase(text, search)) {
                                return true;
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            //
        }
        return false;
    }

    /**
     * @param bookedTravelersEligibleForCheckin
     * @param passengersSuccessfulEdited
     * @param missingInformation
     * @throws Exception
     */
    public static void removeMissingInformation(
            List<BookedTraveler> bookedTravelersEligibleForCheckin,
            Set<BookedTraveler> passengersSuccessfulEdited,
            Set<String> missingInformation
    ) throws Exception {
        if (null == passengersSuccessfulEdited || passengersSuccessfulEdited.isEmpty() || null == missingInformation
                || missingInformation.isEmpty()) {
            return;
        }
        for (BookedTraveler bookedTravelerSuccessfulChecked : passengersSuccessfulEdited) {
            for (BookedTraveler bookedTraveler : bookedTravelersEligibleForCheckin) {
                if (bookedTravelerSuccessfulChecked.getNameRefNumber()
                        .equalsIgnoreCase(bookedTraveler.getNameRefNumber())) {
                    if (null != bookedTraveler.getMissingCheckinRequiredFields()
                            && !bookedTraveler.getMissingCheckinRequiredFields().isEmpty()) {
                        Iterator<String> i = bookedTraveler.getMissingCheckinRequiredFields().iterator();
                        while (i.hasNext()) {
                            String ineligibleReason = i.next(); // must be
                            // called before
                            // you can call
                            // i.remove()
                            if (missingInformation.contains(ineligibleReason)) {
                                i.remove();
                            }
                        }
                    }
                    if (null != bookedTraveler.getMissingCheckinRequiredFields()
                            && bookedTraveler.getMissingCheckinRequiredFields().isEmpty()) {
                        bookedTraveler.setMissingCheckinRequiredFields(null);
                    }
                    break;
                }
            }
        }
    }

    /**
     * @param bookedTraveler
     * @param missingInformation
     * @throws Exception
     */
    public static void removeMissingInformation(BookedTraveler bookedTraveler, Set<String> missingInformation)
            throws Exception {
        if (null == bookedTraveler || null == bookedTraveler.getMissingCheckinRequiredFields()
                || bookedTraveler.getMissingCheckinRequiredFields().isEmpty() || null == missingInformation
                || missingInformation.isEmpty()) {
            return;
        }

        Iterator<String> i = bookedTraveler.getMissingCheckinRequiredFields().iterator();
        while (i.hasNext()) {
            String ineligibleReason = i.next(); // must be called before you can
            // call i.remove()
            if (missingInformation.contains(ineligibleReason)) {
                i.remove();
                //break;
            }
        }
    }

    /**
     * @param bookedTraveler
     * @throws Exception
     */
    public static void removeTimaticIneligible(
            BookedTraveler bookedTraveler
    ) {

        if (null != bookedTraveler.getMissingCheckinRequiredFields()
                && !bookedTraveler.getMissingCheckinRequiredFields().isEmpty()) {

            for (String missingCheckinRequiredField : bookedTraveler.getMissingCheckinRequiredFields()) {
                if (CheckinIneligibleReasons.TIM.getCheckinIneligibleReason().equalsIgnoreCase(missingCheckinRequiredField)
                        || CheckinIneligibleReasons.TIMI.getCheckinIneligibleReason().equalsIgnoreCase(missingCheckinRequiredField)) {
                    return;
                }
            }
        }

        if (null != bookedTraveler.getIneligibleReasons()
                && !bookedTraveler.getIneligibleReasons().isEmpty()) {
            Iterator<String> i = bookedTraveler.getIneligibleReasons().iterator();
            while (i.hasNext()) {
                String ineligibleReason = i.next(); // must be called before you can
                // call i.remove()
                if (CheckinIneligibleReasons.TIM_NOT_OK.getCheckinIneligibleReason().equalsIgnoreCase(ineligibleReason)
                        || CheckinIneligibleReasons.TIMI_NOT_OK.getCheckinIneligibleReason().equalsIgnoreCase(ineligibleReason)
                        || CheckinIneligibleReasons.TIM_CONDITIONAL.getCheckinIneligibleReason().equalsIgnoreCase(ineligibleReason)
                        || CheckinIneligibleReasons.TIM_INF_CONDITIONAL.getCheckinIneligibleReason().equalsIgnoreCase(ineligibleReason)
                ) {
                    i.remove();
                }
            }
        }
    }

    /**
     * @param bookedTraveler
     * @param ineligibleReasonToRemove
     * @throws Exception
     */
    public static void removeMissingInformation(
            BookedTraveler bookedTraveler, String ineligibleReasonToRemove
    )  {
        if (null == ineligibleReasonToRemove || ineligibleReasonToRemove.isEmpty() || null == bookedTraveler.getMissingCheckinRequiredFields() || bookedTraveler.getMissingCheckinRequiredFields().isEmpty()) {
            return;
        }

        Iterator<String> i = bookedTraveler.getMissingCheckinRequiredFields().iterator();
        while (i.hasNext()) {
            String ineligibleReason = i.next(); // must be called before you can
            // call i.remove()
            if (ineligibleReasonToRemove.equalsIgnoreCase(ineligibleReason)) {
                i.remove();
                //break;
            }
        }
    }

    /**
     * @param bookedTraveler
     * @param ineligibleReasonToRemove
     * @throws Exception
     */
    public static void removeIneligibleReasonVerification(BookedTraveler bookedTraveler, String ineligibleReasonToRemove) {
        if (null == bookedTraveler.getIneligibleReasons() || bookedTraveler.getIneligibleReasons().isEmpty() || null == ineligibleReasonToRemove || ineligibleReasonToRemove.isEmpty()) {
            return;
        }

        Iterator<String> i = bookedTraveler.getIneligibleReasons().iterator();
        while (i.hasNext()) {
            String ineligibleReason = i.next(); // must be called before you can
            // call i.remove()
            if (ineligibleReasonToRemove.equalsIgnoreCase(ineligibleReason)) {
                i.remove();
                //break;
            }
        }
    }

    /**
     * @param bookedTraveler
     * @param ineligibleReasonToRemove
     * @return
     * @throws Exception
     */
    public static boolean findIneligibleReasonVerification(BookedTraveler bookedTraveler, String ineligibleReasonToRemove)
            throws Exception {
        if (null == bookedTraveler.getIneligibleReasons() || null == ineligibleReasonToRemove) {
            return false;
        }

        for (String ineligibleReason : bookedTraveler.getIneligibleReasons()) {
            if (ineligibleReasonToRemove.equalsIgnoreCase(ineligibleReason)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param bookedTraveler
     * @param missingCheckin
     * @return
     * @throws Exception
     */
    public static boolean findMissingInformation(BookedTraveler bookedTraveler, String missingCheckin) throws Exception {
        if (null == bookedTraveler.getMissingCheckinRequiredFields() || null == missingCheckin) {
            return false;
        }

        for (String missingCheckinRequiredField : bookedTraveler.getMissingCheckinRequiredFields()) {
            if (missingCheckin.equalsIgnoreCase(missingCheckinRequiredField)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param id
     * @param ancillaries
     * @return
     */
    public static AncillaryServicesPNRB getAncillary(String id, List<AncillaryServicesPNRB> ancillaries) {
        if (null == ancillaries || null == id) {
            return null;
        }
        for (AncillaryServicesPNRB ancillary : ancillaries) {
            if (id.equalsIgnoreCase(ancillary.getId())) {
                return ancillary;
            }
        }
        return null;
    }

    /**
     * @param type
     * @param ancillaries
     * @return
     */
    public static AncillaryServicesPNRB getAncillaryByType(String type, List<AncillaryServicesPNRB> ancillaries) {
        if (null == ancillaries || null == type) {
            return null;
        }
        for (AncillaryServicesPNRB ancillary : ancillaries) {
            if (type.equalsIgnoreCase(ancillary.getRficSubcode())) {
                return ancillary;
            }
        }
        return null;
    }

    /**
     * @param type
     * @param ancillaries
     * @return
     */
    public static AncillaryServicesPNRB getUnpaidAncillaryByType(String type, List<AncillaryServicesPNRB> ancillaries) {
        if (null == ancillaries || null == type) {
            return null;
        }
        for (AncillaryServicesPNRB ancillary : ancillaries) {
            if (ActionCodeType.HD.toString().equalsIgnoreCase(ancillary.getActionCode())
                    && type.equalsIgnoreCase(ancillary.getRficSubcode())) {
                return ancillary;
            }
        }
        return null;
    }

    /**
     * @param updateId
     * @param results
     * @return
     */
    public static ResultsPNRB.UpdateResult getUpdateResult(String updateId, List<ResultsPNRB.UpdateResult> results) {
        if (null == results || null == updateId) {
            return null;
        }
        for (ResultsPNRB.UpdateResult result : results) {
            if (updateId.equalsIgnoreCase(result.getUpdateId())) {
                return result;
            }
        }
        return null;
    }

    /**
     * @param nameRef
     * @param passengers
     * @return
     */
    public static PassengerPNRB getPassenger(String nameRef, List<PassengerPNRB> passengers) {
        if (null == passengers || null == nameRef) {
            return null;
        }
        for (PassengerPNRB passenger : passengers) {
            if (nameRef.equalsIgnoreCase(passenger.getNameId())) {
                return passenger;
            }
        }
        return null;
    }

    /**
     * @param travelerAncillariesAdded
     * @throws Exception
     */
    public static void removeDefaultAncillaryId(List<TravelerAncillary> travelerAncillariesAdded) throws Exception {
        if (null != travelerAncillariesAdded) {
            for (TravelerAncillary travelerAncillary : travelerAncillariesAdded) {
                travelerAncillary.setAncillaryId(null);
                travelerAncillary.setIsPartOfReservation(false);
            }
        }
    }

    /**
     * @param extraWeightAncillariesAdded
     * @throws Exception
     */
    public static void removeDefaultExtraWeightAncillaryId(List<ExtraWeightAncillary> extraWeightAncillariesAdded) throws Exception {
        if (null != extraWeightAncillariesAdded) {
            for (ExtraWeightAncillary extraWeightAncillary : extraWeightAncillariesAdded) {
            	LOGGER.info( "removeDefaultExtraWeightAncillaryId: {}", extraWeightAncillary );
                extraWeightAncillary.setAncillaryId(null);
                extraWeightAncillary.setIsPartOfReservation(false);
            }
        }
    }

    /**
     * @param cabinUpgradeAncillariesAdded
     * @throws Exception
     */
    public static void removeDefaultUpgradeAncillaryId(
            List<CabinUpgradeAncillary> cabinUpgradeAncillariesAdded
    ) throws Exception {
        if (null != cabinUpgradeAncillariesAdded) {
            for (CabinUpgradeAncillary cabinUpgradeAncillary : cabinUpgradeAncillariesAdded) {
                cabinUpgradeAncillary.setAncillaryId(null);
                cabinUpgradeAncillary.setIsPartOfReservation(false);
            }
        }
    }

    /**
     * @param linkedId
     * @param abstractSegmentChoiceList
     * @return
     */
    public static SegmentChoice getSegmentChoice(String linkedId, List<AbstractSegmentChoice> abstractSegmentChoiceList) {
        if (null == linkedId || linkedId.isEmpty() || null == abstractSegmentChoiceList
                || abstractSegmentChoiceList.isEmpty()) {
            return null;
        }
        for (AbstractSegmentChoice abstractSegmentChoice : abstractSegmentChoiceList) {
            if (abstractSegmentChoice instanceof SegmentChoice) {
                SegmentChoice segmentChoice = (SegmentChoice) abstractSegmentChoice;
                if (linkedId.equalsIgnoreCase(segmentChoice.getLinkedID())) {
                    return segmentChoice;
                }
            }
        }
        return null;
    }

    /**
     * @param linkedId
     * @param abstractSegmentChoiceList
     * @return
     */
    public static int getSegmentChoiceIndex(String linkedId, List<AbstractSegmentChoice> abstractSegmentChoiceList) {
        if (null == abstractSegmentChoiceList || abstractSegmentChoiceList.isEmpty()) {
            return -1;
        }
        int index = 0;
        for (AbstractSegmentChoice abstractSegmentChoice : abstractSegmentChoiceList) {
            if (abstractSegmentChoice instanceof SegmentChoice) {
                SegmentChoice segmentChoice = (SegmentChoice) abstractSegmentChoice;
                if (linkedId.equalsIgnoreCase(segmentChoice.getLinkedID())) {
                    return index;
                }
            }
            index++;
        }
        return -1;
    }

    /**
     * @param bookedTravelerList
     * @param travelerAncillariesAdded
     */
    public static void removeSeatAncillariesAdded(List<BookedTraveler> bookedTravelerList,
                                                  List<TravelerAncillary> travelerAncillariesAdded) {
        if (null == bookedTravelerList || null == travelerAncillariesAdded) {
            return;
        }
        for (BookedTraveler bookedTraveler : bookedTravelerList) {
            if (null != bookedTraveler.getSeatAncillaries()
                    || null != bookedTraveler.getSeatAncillaries().getCollection()) {
                Iterator<AbstractAncillary> i = bookedTraveler.getSeatAncillaries().getCollection().iterator();
                while (i.hasNext()) {
                    AbstractAncillary abstractAncillary = i.next(); // must be
                    // called
                    // before
                    // you can
                    // call
                    // i.remove()
                    if (abstractAncillary instanceof TravelerAncillary) {
                        TravelerAncillary travelerAncillary = (TravelerAncillary) abstractAncillary;
                        if (null != travelerAncillary.getAncillaryId()) {
                            for (TravelerAncillary travelerAncillaryAdded : travelerAncillariesAdded) {
                                if (travelerAncillary.getAncillaryId()
                                        .equalsIgnoreCase(travelerAncillaryAdded.getAncillaryId())) {
                                    AbstractSegmentChoice abstractSegmentChoice = PurchaseOrderUtil
                                            .getSeatEntryBySegmentCode(travelerAncillary.getSegmentCodeAux(),
                                                    bookedTraveler.getSegmentChoices().getCollection());
                                    abstractSegmentChoice.setAncillaryId(null);
                                    i.remove();
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @param reservation
     * @param id
     * @return
     */
    public static AncillaryServicesPNRB getAncillaryReturned(ReservationPNRB reservation, String id) {
        try {
            for (PassengerPNRB passengerPNRB : reservation.getPassengerReservation().getPassengers().getPassenger()) {
                if (null != passengerPNRB.getAncillaryServices()
                        && null != passengerPNRB.getAncillaryServices().getAncillaryService()
                        && !passengerPNRB.getAncillaryServices().getAncillaryService().isEmpty()) {
                    for (AncillaryServicesPNRB ancillaryServicesPNRB : passengerPNRB.getAncillaryServices()
                            .getAncillaryService()) {
                        if (null != id && id.equalsIgnoreCase(ancillaryServicesPNRB.getId())) {
                            return ancillaryServicesPNRB;
                        }
                    }
                }
            }
        } catch (Exception ex) {
            //
        }
        return null;
    }

    /**
     * @param travelerAncillariesAdded
     * @param updateReservationRS
     * @throws Exception
     */
    public static void setAncillaryId(
            List<TravelerAncillary> travelerAncillariesAdded,
            UpdateReservationRS updateReservationRS
    ) throws Exception {
        if (null == travelerAncillariesAdded || travelerAncillariesAdded.isEmpty()) {
            return;
        }

        if (null != updateReservationRS.getResults()
                && null != updateReservationRS.getResults().getUpdateResult()
                && !updateReservationRS.getResults().getUpdateResult().isEmpty()) {

            List<ResultsPNRB.UpdateResult> results = updateReservationRS.getResults().getUpdateResult();
            for (TravelerAncillary travelerAncillary : travelerAncillariesAdded) {
                ResultsPNRB.UpdateResult result = getUpdateResult(travelerAncillary.getAncillaryId(), results);
                if (null != result && null != result.getItem() && !result.getItem().isEmpty()) {
                    AncillaryServicesPNRB ancillaryServicesPNRB;
                    ancillaryServicesPNRB = getAncillaryReturned(
                            updateReservationRS.getReservation(), result.getItem().get(0).getId()
                    );

                    if ("SUCCESS".equalsIgnoreCase(result.getStatus())) {
                        travelerAncillary.setAncillaryId(result.getItem().get(0).getId());
                        travelerAncillary.setIsPartOfReservation(true);
                        if (null != ancillaryServicesPNRB) {
                            travelerAncillary.setActionCode(ancillaryServicesPNRB.getActionCode());
                        }
                    } else {
                        travelerAncillary.setAncillaryId(null);
                        travelerAncillary.setIsPartOfReservation(false);
                    }
                }
            }
        } else {
            try {
                List<PassengerPNRB> passengers = updateReservationRS.getReservation().getPassengerReservation().getPassengers().getPassenger();

                if (null != passengers && !passengers.isEmpty()) {
                    for (TravelerAncillary travelerAncillary : travelerAncillariesAdded) {
                        PassengerPNRB passenger = getPassenger(travelerAncillary.getNameNumber(), passengers);

                        if (null != passenger && null != passenger.getNameId()
                                && passenger.getNameId().equalsIgnoreCase(travelerAncillary.getNameNumber())) {

                            List<AncillaryServicesPNRB> ancillaries = passenger.getAncillaryServices().getAncillaryService();

                            AncillaryServicesPNRB ancillary = getUnpaidAncillaryByType(travelerAncillary.getAncillary().getType(), ancillaries);

                            if (null != ancillary) {

                                travelerAncillary.setAncillaryId(ancillary.getId());
                                travelerAncillary.setIsPartOfReservation(true);
                                travelerAncillary.setActionCode(ancillary.getActionCode());

                            } else {
                                travelerAncillary.setAncillaryId(null);
                                travelerAncillary.setIsPartOfReservation(false);
                                travelerAncillary.setActionCode(ActionCodeType.HD.toString());
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                LOGGER.error("Not AE items found in response {}", ex.getMessage());
            }
        }
    }

    /**
     * @param extraWeightAncillariesAdded
     * @param updateReservationRS
     * @throws Exception
     */
    public static void setExtraWeightAncillaryId(
            List<ExtraWeightAncillary> extraWeightAncillariesAdded,
            UpdateReservationRS updateReservationRS
    ) throws Exception {
        if (null == extraWeightAncillariesAdded || extraWeightAncillariesAdded.isEmpty()) {
            return;
        }

        if (null != updateReservationRS.getResults()
                && null != updateReservationRS.getResults().getUpdateResult()
                && !updateReservationRS.getResults().getUpdateResult().isEmpty()) {

            List<ResultsPNRB.UpdateResult> results = updateReservationRS.getResults().getUpdateResult();
            for (ExtraWeightAncillary extraWeightAncillary : extraWeightAncillariesAdded) {
                ResultsPNRB.UpdateResult result = getUpdateResult(extraWeightAncillary.getAncillaryId(), results);
                if (null != result && null != result.getItem() && !result.getItem().isEmpty()) {
                    AncillaryServicesPNRB ancillaryServicesPNRB;
                    ancillaryServicesPNRB = getAncillaryReturned(
                            updateReservationRS.getReservation(), result.getItem().get(0).getId()
                    );

                    if ("SUCCESS".equalsIgnoreCase(result.getStatus())) {
                        extraWeightAncillary.setAncillaryId(result.getItem().get(0).getId());
                        extraWeightAncillary.setIsPartOfReservation(true);
                        if (null != ancillaryServicesPNRB) {
                            extraWeightAncillary.setActionCode(ancillaryServicesPNRB.getActionCode());
                        }
                    } else {
                        extraWeightAncillary.setAncillaryId(null);
                        extraWeightAncillary.setIsPartOfReservation(false);
                    }
                }

                LOGGER.info( "setExtraWeightAncillaryId: {}", extraWeightAncillary);

            }
        } else {
            try {
                List<PassengerPNRB> passengers = updateReservationRS.getReservation().getPassengerReservation().getPassengers().getPassenger();

                if (null != passengers && !passengers.isEmpty()) {
                    for (ExtraWeightAncillary extraWeightAncillary : extraWeightAncillariesAdded) {
                        PassengerPNRB passenger = getPassenger(extraWeightAncillary.getNameNumber(), passengers);

                        if (null != passenger && null != passenger.getNameId()
                                && passenger.getNameId().equalsIgnoreCase(extraWeightAncillary.getNameNumber())) {

                            List<AncillaryServicesPNRB> ancillaries = passenger.getAncillaryServices().getAncillaryService();

                            AncillaryServicesPNRB ancillary = getUnpaidAncillaryByType(extraWeightAncillary.getAncillary().getType(), ancillaries);

                            if (null != ancillary) {

                                extraWeightAncillary.setAncillaryId(ancillary.getId());
                                extraWeightAncillary.setIsPartOfReservation(true);
                                extraWeightAncillary.setActionCode(ancillary.getActionCode());

                            } else {
                                extraWeightAncillary.setAncillaryId(null);
                                extraWeightAncillary.setIsPartOfReservation(false);
                                extraWeightAncillary.setActionCode(ActionCodeType.HD.toString());
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                LOGGER.error("Not AE items found in response {}", ex.getMessage());
            }
        }
    }

    /**
     * @param cabinUpgradeAncillariesAdded
     * @param updateReservationRS
     * @throws Exception
     */
    public static void setUpgradeAncillaryId(
            List<CabinUpgradeAncillary> cabinUpgradeAncillariesAdded,
            UpdateReservationRS updateReservationRS
    ) throws Exception {

        if (null == cabinUpgradeAncillariesAdded || cabinUpgradeAncillariesAdded.isEmpty()) {
            return;
        }

        if (null != updateReservationRS.getResults()
                && null != updateReservationRS.getResults().getUpdateResult()
                && !updateReservationRS.getResults().getUpdateResult().isEmpty()) {

            List<ResultsPNRB.UpdateResult> results = updateReservationRS.getResults().getUpdateResult();
            for (CabinUpgradeAncillary cabinUpgradeAncillary : cabinUpgradeAncillariesAdded) {
                ResultsPNRB.UpdateResult result = getUpdateResult(cabinUpgradeAncillary.getAncillaryId(), results);
                if (null != result && null != result.getItem() && !result.getItem().isEmpty()) {
                    AncillaryServicesPNRB ancillaryServicesPNRB;
                    ancillaryServicesPNRB = getAncillaryReturned(
                            updateReservationRS.getReservation(), result.getItem().get(0).getId()
                    );

                    if ("SUCCESS".equalsIgnoreCase(result.getStatus())) {
                        cabinUpgradeAncillary.setAncillaryId(result.getItem().get(0).getId());
                        cabinUpgradeAncillary.setIsPartOfReservation(true);
                        if (null != ancillaryServicesPNRB) {
                            cabinUpgradeAncillary.setActionCode(ancillaryServicesPNRB.getActionCode());
                        }
                    } else {
                        cabinUpgradeAncillary.setAncillaryId(null);
                        cabinUpgradeAncillary.setIsPartOfReservation(false);
                    }
                }
            }
        } else {
            try {
                List<PassengerPNRB> passengers = updateReservationRS.getReservation().getPassengerReservation().getPassengers().getPassenger();

                if (null != passengers && !passengers.isEmpty()) {
                    for (CabinUpgradeAncillary cabinUpgradeAncillary : cabinUpgradeAncillariesAdded) {
                        PassengerPNRB passenger = getPassenger(cabinUpgradeAncillary.getNameNumber(), passengers);

                        if (null != passenger && null != passenger.getNameId()
                                && passenger.getNameId().equalsIgnoreCase(cabinUpgradeAncillary.getNameNumber())) {

                            List<AncillaryServicesPNRB> ancillaries = passenger.getAncillaryServices().getAncillaryService();

                            AncillaryServicesPNRB ancillary = getUnpaidAncillaryByType(cabinUpgradeAncillary.getType(), ancillaries);

                            if (null != ancillary) {

                                cabinUpgradeAncillary.setAncillaryId(ancillary.getId());
                                cabinUpgradeAncillary.setIsPartOfReservation(true);
                                cabinUpgradeAncillary.setActionCode(ancillary.getActionCode());

                            } else {
                                cabinUpgradeAncillary.setAncillaryId(null);
                                cabinUpgradeAncillary.setIsPartOfReservation(false);
                                cabinUpgradeAncillary.setActionCode(ActionCodeType.HD.toString());
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                LOGGER.error("Not AE items found in response {}", ex.getMessage());
            }
        }
    }

    /**
     * @param bookedTravelerList
     * @param passengers
     * @throws Exception
     */
    public static void updateAncillaryInfo(List<BookedTraveler> bookedTravelerList, List<PassengerPNRB> passengers)
            throws Exception {
        for (BookedTraveler bookedTraveler : bookedTravelerList) {
            PassengerPNRB passenger = getPassenger(bookedTraveler.getNameRefNumber(), passengers);
            for (AbstractAncillary abstractAncillary : bookedTraveler.getAncillaries().getCollection()) {
                if (abstractAncillary instanceof TravelerAncillary) {
                    TravelerAncillary travelerAncillary = (TravelerAncillary) abstractAncillary;
                    if (null != travelerAncillary.getAncillaryId()
                            && !travelerAncillary.getAncillaryId().trim().isEmpty()) {
                        AncillaryServicesPNRB ancillaryServicesPNRB = getAncillary(
                                travelerAncillary.getAncillaryId(),
                                passenger.getAncillaryServices().getAncillaryService()
                        );
                        if (null != ancillaryServicesPNRB) {
                            travelerAncillary.getAncillary()
                                    .setCommisionIndicator(ancillaryServicesPNRB.getCommisionIndicator());
                            travelerAncillary.getAncillary()
                                    .setInterlineIndicator(ancillaryServicesPNRB.getInterlineIndicator());
                            travelerAncillary.getAncillary()
                                    .setRefundIndicator(ancillaryServicesPNRB.getRefundIndicator());
                            // to update segment choice
                            if (null != travelerAncillary.getLinkedID()
                                    && !travelerAncillary.getLinkedID().trim().isEmpty()) {
                                SegmentChoice segmentChoice = getSegmentChoice(
                                        travelerAncillary.getLinkedID(),
                                        bookedTraveler.getSegmentChoices().getCollection()
                                );
                                if (null != segmentChoice) {
                                    segmentChoice.setPartOfReservation(travelerAncillary.isPartOfReservation());
                                    segmentChoice.setActionCode(travelerAncillary.getActionCode());
                                    segmentChoice.setAncillaryId(travelerAncillary.getAncillaryId());
                                }
                            }
                        }
                    }
                } else if (abstractAncillary instanceof ExtraWeightAncillary) {
                    ExtraWeightAncillary extraWeightAncillary = (ExtraWeightAncillary) abstractAncillary;
                    if (null != extraWeightAncillary.getAncillaryId()
                            && !extraWeightAncillary.getAncillaryId().trim().isEmpty()) {
                        AncillaryServicesPNRB ancillaryServicesPNRB = getAncillary(extraWeightAncillary.getAncillaryId(),
                                passenger.getAncillaryServices().getAncillaryService()
                        );
                        if (null != ancillaryServicesPNRB) {
                            extraWeightAncillary.getAncillary()
                                    .setCommisionIndicator(ancillaryServicesPNRB.getCommisionIndicator());
                            extraWeightAncillary.getAncillary()
                                    .setInterlineIndicator(ancillaryServicesPNRB.getInterlineIndicator());
                            extraWeightAncillary.getAncillary()
                                    .setRefundIndicator(ancillaryServicesPNRB.getRefundIndicator());
                        }
                    }
                } else if (abstractAncillary instanceof CabinUpgradeAncillary) {
                    CabinUpgradeAncillary cabinUpgradeAncillary = (CabinUpgradeAncillary) abstractAncillary;
                    if (null != cabinUpgradeAncillary.getAncillaryId()
                            && !cabinUpgradeAncillary.getAncillaryId().trim().isEmpty()) {
                        //nothing to be updated for now
                    }
                }
            }

            for (AbstractAncillary abstractAncillary : bookedTraveler.getSeatAncillaries().getCollection()) {
                if (abstractAncillary instanceof TravelerAncillary) { // Just
                    // for
                    // unpaid
                    // ancillaries
                    TravelerAncillary travelerAncillary = (TravelerAncillary) abstractAncillary;
                    if (null != travelerAncillary.getAncillaryId()
                            && !travelerAncillary.getAncillaryId().trim().isEmpty()) {
                        AncillaryServicesPNRB ancillaryServicesPNRB = getAncillary(travelerAncillary.getAncillaryId(),
                                passenger.getAncillaryServices().getAncillaryService());
                        if (null != ancillaryServicesPNRB) {
                            travelerAncillary.getAncillary()
                                    .setCommisionIndicator(ancillaryServicesPNRB.getCommisionIndicator());
                            travelerAncillary.getAncillary()
                                    .setInterlineIndicator(ancillaryServicesPNRB.getInterlineIndicator());
                            travelerAncillary.getAncillary()
                                    .setRefundIndicator(ancillaryServicesPNRB.getRefundIndicator());
                            // to update segment choice
                            if (null != travelerAncillary.getLinkedID()
                                    && !travelerAncillary.getLinkedID().trim().isEmpty()) {
                                SegmentChoice segmentChoice = getSegmentChoice(
                                        travelerAncillary.getLinkedID(),
                                        bookedTraveler.getSegmentChoices().getCollection()
                                );
                                if (null != segmentChoice) {
                                    segmentChoice.setPartOfReservation(travelerAncillary.isPartOfReservation());
                                    segmentChoice.setActionCode(travelerAncillary.getActionCode());
                                    segmentChoice.setAncillaryId(travelerAncillary.getAncillaryId());
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @param nameRef
     * @param feesTypesList
     * @return
     */
    public static FeesLinked getCustomer(String nameRef, List<FeesType> feesTypesList) {
        try {
            BigDecimal refNumber = new BigDecimal(nameRef);
            for (FeesType feesType : feesTypesList) {
                if (0 == feesType.getLinked().getCustomer().getCustomerDetails().getNameRefNumber()
                        .compareTo(refNumber)) {
                    return feesType.getLinked();
                }
            }
            return null;
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * @param ancillaryId
     * @param feeList
     * @return
     */
    public static FeeLinked getFee(String ancillaryId, List<FeeLinked> feeList) {
        if (null == ancillaryId) {
            return null;
        }
        try {
            BigInteger itemId = new BigInteger(ancillaryId);
            for (FeeLinked feeLinked : feeList) {
                if (0 == feeLinked.getOptionalService().getAirExtraItemNumber().compareTo(itemId)) {
                    return feeLinked;
                }
            }
            return null;
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * @param cartPNR
     * @param bookedTravelerList
     * @param currencyCode
     * @throws java.lang.Exception
     */
    public static void updateTotal(
            CartPNR cartPNR,
            List<BookedTraveler> bookedTravelerList,
            String currencyCode
    ) throws Exception {
        cartPNR.getMeta().getTotal().setCurrency(CurrencyUtil.getCurrencyValue(currencyCode, bookedTravelerList));
    }

    /**
     * @param pnrCollection
     * @param currencyCode
     * @param store
     * @param pos
     */
    public static void setTotal(
            PNRCollection pnrCollection,
            String currencyCode,
            String store,
            String pos
    ) {
        try {
            for (PNR pnr : pnrCollection.getCollection()) {
                for (CartPNR cartPNR : pnr.getCarts().getCollection()) {
                    List<BookedTraveler> bookedTravelersSelectForCheckin;

                    bookedTravelersSelectForCheckin = FilterPassengersUtil.getMarkedForCheckin(
                            cartPNR.getTravelerInfo().getCollection()
                    );

                    cartPNR.getMeta().setTotal(
                            getTotal(
                                    bookedTravelersSelectForCheckin,
                                    currencyCode,
                                    store,
                                    pos
                            )
                    );
                }
            }
        } catch (Exception ex) {
            LOGGER.error("Error setting shopping cart total: " + ex.getMessage(), ex);
        }
    }

    /**
     * @param bookedTravelersSelectForCheckin
     * @param store
     * @param pos
     * @return
     * @throws Exception
     */
    private static Total getTotal(
            List<BookedTraveler> bookedTravelersSelectForCheckin,
            String currencyCode,
            String store,
            String pos
    ) throws Exception {
        CurrencyValue currencyValue = CurrencyUtil.getCurrencyValue(currencyCode, bookedTravelersSelectForCheckin);

        Total t = new Total();
        t.setCurrency(currencyValue);
        return t;
    }


    /**
     * @param bookedTravelerList
     * @param feesTypesList
     * @param issuedEmds
     * @throws Exception
     */
    public static void changeUnpaidAncillariesToPaid(
            List<BookedTraveler> bookedTravelerList,
            List<FeesType> feesTypesList,
            IssuedEmds issuedEmds
    ) throws Exception {
        for (BookedTraveler bookedTraveler : bookedTravelerList) {
            FeesLinked feeLinked = getCustomer(bookedTraveler.getNameRefNumber(), feesTypesList);
            if (null != feeLinked) {
                bookedTraveler.setTouched(true);
                int index = -1;
                for (AbstractAncillary abstractAncillary : bookedTraveler.getAncillaries().getCollection()) {
                    index++;
                    if (abstractAncillary instanceof TravelerAncillary) {
                        TravelerAncillary travelerAncillary = (TravelerAncillary) abstractAncillary;
                        FeeLinked fee = getFee(travelerAncillary.getAncillaryId(), feeLinked.getFee());
                        if (null != fee) {
                            String currencyCode = travelerAncillary.getAncillary().getCurrency().getCurrencyCode();

                            BigDecimal qty = CurrencyUtil.getAmount(
                                    new BigDecimal(travelerAncillary.getQuantity()),
                                    currencyCode
                            );

                            TicketedTravelerAncillary ticketedTravelerAncillary;
                            ticketedTravelerAncillary = travelerAncillary.transformToTicketedAncillary();
                            BigDecimal total = CurrencyUtil.getAmount(
                                    travelerAncillary.getAncillary().getCurrency().getTotal().multiply(qty),
                                    currencyCode
                            );

                            String name = travelerAncillary.getAncillary().getCommercialName();
                            ticketedTravelerAncillary.setNameNumber(bookedTraveler.getNameRefNumber());
                            ticketedTravelerAncillary.setActionCode(ActionCodeType.HK.toString());
                            String emd = null;
                            if (null != fee.getIssuedDocumentNumber()) {
                                emd = fee.getIssuedDocumentNumber().getValue();
                            } else if (null != fee.getAssociatedFlight() && !fee.getAssociatedFlight().isEmpty()) {
                                for (FlightMisc flightMisc : fee.getAssociatedFlight()) {
                                    if (getLegCode(flightMisc)
                                            .equalsIgnoreCase(ticketedTravelerAncillary.getLegCode())) {
                                        if (null != flightMisc.getIssuedDocumentNumber()) {
                                            emd = flightMisc.getIssuedDocumentNumber().getValue();
                                        }
                                        break;
                                    }
                                }
                            }
                            if (null != emd) {
                                ticketedTravelerAncillary.setEmd(emd);
                                ticketedTravelerAncillary.setActionCode(ActionCodeType.HI.toString());
                                EmdItem emdItem = new EmdItem();
                                emdItem.setQuantity(1);
                                emdItem.setAmount(total);
                                emdItem.setName(name);

                                if (AncillaryPostBookingType._0IA.getRficSubCode().equalsIgnoreCase(travelerAncillary.getAncillary().getRficSubcode())) {
                                    issuedEmds.add(emd, emdItem, EmdReceiptType.EMD_ADMIN_FEE);

                                } else if (AncillaryPostBookingType._0MJ.getRficSubCode().equalsIgnoreCase(travelerAncillary.getAncillary().getRficSubcode())) {
                                        issuedEmds.add(emd, emdItem, EmdReceiptType.EMD_CARRY_ON_BAGGAGE);
                                } else if (AncillaryPostBookingType._0CZ.getRficSubCode().equalsIgnoreCase(travelerAncillary.getAncillary().getRficSubcode())) {
                                    issuedEmds.add(emd, emdItem, EmdReceiptType.EMD_CARRY_ON_BAGGAGE);
                                } else if ("BG".equalsIgnoreCase(travelerAncillary.getAncillary().getGroupCode())) {
                                    issuedEmds.add(emd, emdItem, EmdReceiptType.EMD_EXTRA_BAGGAGE);
                                } else {
                                    issuedEmds.add(emd, emdItem, EmdReceiptType.EMD_OTHER);
                                }
                            }
                            ticketedTravelerAncillary.setIsPartOfReservation(true);
                            bookedTraveler.getAncillaries().getCollection().set(index, ticketedTravelerAncillary);
                        }
                    } else if (abstractAncillary instanceof ExtraWeightAncillary) {
                        ExtraWeightAncillary extraWeightAncillary = (ExtraWeightAncillary) abstractAncillary;
                        FeeLinked fee = getFee(extraWeightAncillary.getAncillaryId(), feeLinked.getFee());
                        if (null != fee) {
                            String currencyCode = extraWeightAncillary.getAncillary().getCurrency().getCurrencyCode();

                            BigDecimal qty = CurrencyUtil.getAmount(
                                    new BigDecimal(extraWeightAncillary.getQuantity()),
                                    currencyCode
                            );

                            TicketedTravelerAncillary ticketedTravelerAncillary;
                            ticketedTravelerAncillary = extraWeightAncillary.transformToTicketedAncillary();
                            BigDecimal total = CurrencyUtil.getAmount(
                                    extraWeightAncillary.getAncillary().getCurrency().getTotal().multiply(qty),
                                    currencyCode
                            );

                            String name = extraWeightAncillary.getAncillary().getCommercialName();
                            ticketedTravelerAncillary.setNameNumber(bookedTraveler.getNameRefNumber());
                            ticketedTravelerAncillary.setActionCode(ActionCodeType.HK.toString());
                            String emd = null;
                            if (null != fee.getIssuedDocumentNumber()) {
                                emd = fee.getIssuedDocumentNumber().getValue();
                            } else if (null != fee.getAssociatedFlight() && !fee.getAssociatedFlight().isEmpty()) {
                                for (FlightMisc flightMisc : fee.getAssociatedFlight()) {
                                    if (getLegCode(flightMisc)
                                            .equalsIgnoreCase(ticketedTravelerAncillary.getLegCode())) {
                                        if (null != flightMisc.getIssuedDocumentNumber()) {
                                            emd = flightMisc.getIssuedDocumentNumber().getValue();
                                        }
                                        break;
                                    }
                                }
                            }
                            if (null != emd) {
                                ticketedTravelerAncillary.setEmd(emd);
                                ticketedTravelerAncillary.setActionCode(ActionCodeType.HI.toString());
                                EmdItem emdItem = new EmdItem();
                                emdItem.setQuantity(ticketedTravelerAncillary.getQuantity());
                                emdItem.setAmount(total);
                                emdItem.setName(name);
                                emdItem.setWeightUnitType(WeightUnitType.getType(extraWeightAncillary.getWeightUnit()));
                                issuedEmds.add(emd, emdItem, EmdReceiptType.EMD_EXTRA_WEIGHT);
                            }
                            ticketedTravelerAncillary.setIsPartOfReservation(true);
                            bookedTraveler.getAncillaries().getCollection().set(index, ticketedTravelerAncillary);
                        }
                    } else if (abstractAncillary instanceof CabinUpgradeAncillary) {
                        CabinUpgradeAncillary cabinUpgradeAncillary = (CabinUpgradeAncillary) abstractAncillary;
                        FeeLinked fee = getFee(cabinUpgradeAncillary.getAncillaryId(), feeLinked.getFee());
                        if (null != fee) {
                            String currencyCode = cabinUpgradeAncillary.getCurrency().getCurrencyCode();

                            BigDecimal qty = CurrencyUtil.getAmount(
                                    new BigDecimal(cabinUpgradeAncillary.getQuantity()),
                                    currencyCode
                            );
                            TicketedTravelerAncillary ticketedTravelerAncillary;
                            ticketedTravelerAncillary = cabinUpgradeAncillary.transformToTicketedAncillary();
                            BigDecimal total = CurrencyUtil.getAmount(
                                    cabinUpgradeAncillary.getCurrency().getTotal().multiply(qty),
                                    currencyCode
                            );

                            String name = cabinUpgradeAncillary.getName();
                            ticketedTravelerAncillary.setNameNumber(bookedTraveler.getNameRefNumber());
                            ticketedTravelerAncillary.setActionCode(ActionCodeType.HK.toString());
                            String emd = null;
                            if (null != fee.getIssuedDocumentNumber()) {
                                emd = fee.getIssuedDocumentNumber().getValue();
                            } else if (null != fee.getAssociatedFlight() && !fee.getAssociatedFlight().isEmpty()) {
                                for (FlightMisc flightMisc : fee.getAssociatedFlight()) {
                                    if (SegmentCodeUtil.compareSegmentCodeWithoutTime(getSegmentCode(flightMisc),
                                            ticketedTravelerAncillary.getSegmentCodeAux())) {
                                        if (null != flightMisc.getIssuedDocumentNumber()) {
                                            emd = flightMisc.getIssuedDocumentNumber().getValue();
                                        }
                                        break;
                                    }
                                }
                            }
                            if (null != emd) {
                                ticketedTravelerAncillary.setEmd(emd);
                                ticketedTravelerAncillary.setActionCode(ActionCodeType.HI.toString());
                                EmdItem emdItem = new EmdItem();
                                emdItem.setQuantity(1);
                                emdItem.setAmount(total);
                                emdItem.setName(name);
                                issuedEmds.add(emd, emdItem, EmdReceiptType.EMD_UPGRADE);
                            }
                            ticketedTravelerAncillary.setIsPartOfReservation(true);
                            bookedTraveler.getAncillaries().getCollection().set(index, ticketedTravelerAncillary);
                        }
                    }
                }

                index = -1;
                for (AbstractAncillary abstractAncillary : bookedTraveler.getSeatAncillaries().getCollection()) {
                    index++;
                    if (abstractAncillary instanceof TravelerAncillary) {
                        TravelerAncillary travelerAncillary = (TravelerAncillary) abstractAncillary;
                        FeeLinked fee = getFee(travelerAncillary.getAncillaryId(), feeLinked.getFee());
                        if (null != fee) {
                            TicketedTravelerAncillary ticketedTravelerAncillary = travelerAncillary
                                    .transformToTicketedAncillary();
                            BigDecimal total = travelerAncillary.getAncillary().getCurrency().getTotal();
                            String name = travelerAncillary.getAncillary().getCommercialName();
                            ticketedTravelerAncillary.setNameNumber(bookedTraveler.getNameRefNumber());
                            ticketedTravelerAncillary.setActionCode(ActionCodeType.HK.toString());
                            String emd = null;
                            if (null != fee.getIssuedDocumentNumber()) {
                                emd = fee.getIssuedDocumentNumber().getValue();
                            } else if (null != fee.getAssociatedFlight() && !fee.getAssociatedFlight().isEmpty()) {
                                for (FlightMisc flightMisc : fee.getAssociatedFlight()) {
                                    if (SegmentCodeUtil.compareSegmentCodeWithoutTime(getSegmentCode(flightMisc),
                                            ticketedTravelerAncillary.getSegmentCodeAux())) {
                                        if (null != flightMisc.getIssuedDocumentNumber()) {
                                            emd = flightMisc.getIssuedDocumentNumber().getValue();
                                        }
                                        break;
                                    }
                                }
                            }
                            if (null != emd) {
                                ticketedTravelerAncillary.setEmd(emd);
                                ticketedTravelerAncillary.setActionCode(ActionCodeType.HI.toString());
                                EmdItem emdItem = new EmdItem();
                                emdItem.setQuantity(1);
                                emdItem.setAmount(total);
                                emdItem.setName(name);
                                issuedEmds.add(emd, emdItem, EmdReceiptType.EMD_SEAT);
                            }
                            ticketedTravelerAncillary.setIsPartOfReservation(true);
                            // to update segment choice
                            if (null != travelerAncillary.getLinkedID()
                                    && !travelerAncillary.getLinkedID().trim().isEmpty()) {
                                SegmentChoice segmentChoice = getSegmentChoice(travelerAncillary.getLinkedID(),
                                        bookedTraveler.getSegmentChoices().getCollection());
                                if (null != segmentChoice) {
                                    int indexSegmentChoice = getSegmentChoiceIndex(travelerAncillary.getLinkedID(),
                                            bookedTraveler.getSegmentChoices().getCollection());
                                    if (indexSegmentChoice >= 0) {
                                        TicketedSegmentChoice ticketedSegmentChoice = segmentChoice
                                                .transformToTicketedSegmentChoice();
                                        ticketedSegmentChoice.setEmd(emd);
                                        ticketedSegmentChoice
                                                .setPartOfReservation(travelerAncillary.isPartOfReservation());
                                        ticketedSegmentChoice.setActionCode(travelerAncillary.getActionCode());
                                        ticketedSegmentChoice.setAncillaryId(travelerAncillary.getAncillaryId());
                                        bookedTraveler.getSegmentChoices().getCollection().set(indexSegmentChoice,
                                                ticketedSegmentChoice);
                                    }
                                }
                            }
                            bookedTraveler.getSeatAncillaries().getCollection().set(index, ticketedTravelerAncillary);
                        }
                    }
                }
            }
        }
    }

    /**
     * @param bookedTravelerList
     * @param storedId
     * @param paymentId
     */
    public static void addPaymentIdToOriginalCollection(
            List<BookedTraveler> bookedTravelerList,
            String storedId,
            String paymentId
    ) {
        if (null == storedId || storedId.trim().isEmpty()) {
            return;
        }
        int index;
        for (BookedTraveler bookedTraveler : bookedTravelerList) {
            index = 0;
            for (AbstractAncillary abstractAncillary : bookedTraveler.getAncillaries().getCollection()) {
                if (abstractAncillary instanceof TravelerAncillary
                        && storedId.equalsIgnoreCase(((TravelerAncillary) abstractAncillary).getStoredId())) {
                    TravelerAncillary travelerAncillary = (TravelerAncillary) abstractAncillary;
                    travelerAncillary.setPaymentId(paymentId);
                    bookedTraveler.getAncillaries().getCollection().set(index, travelerAncillary);
                    return;
                } else if (abstractAncillary instanceof ExtraWeightAncillary
                        && storedId.equalsIgnoreCase(((ExtraWeightAncillary) abstractAncillary).getStoredId())) {
                    ExtraWeightAncillary extraWeightAncillary = (ExtraWeightAncillary) abstractAncillary;
                    extraWeightAncillary.setPaymentId(paymentId);
                    bookedTraveler.getAncillaries().getCollection().set(index, extraWeightAncillary);
                    return;
                } else if (abstractAncillary instanceof CabinUpgradeAncillary
                        && storedId.equalsIgnoreCase(((CabinUpgradeAncillary) abstractAncillary).getStoredId())) {
                    CabinUpgradeAncillary cabinUpgradeAncillary = (CabinUpgradeAncillary) abstractAncillary;
                    cabinUpgradeAncillary.setPaymentId(paymentId);
                    bookedTraveler.getAncillaries().getCollection().set(index, cabinUpgradeAncillary);
                    return;
                }
                index++;
            }
            index = 0;
            for (AbstractAncillary abstractAncillary : bookedTraveler.getSeatAncillaries().getCollection()) {
                if (abstractAncillary instanceof TravelerAncillary
                        && storedId.equalsIgnoreCase(((TravelerAncillary) abstractAncillary).getStoredId())) {
                    TravelerAncillary travelerAncillary = (TravelerAncillary) abstractAncillary;
                    travelerAncillary.setPaymentId(paymentId);
                    bookedTraveler.getSeatAncillaries().getCollection().set(index, travelerAncillary);
                    return;
                }
                index++;
            }
        }
    }

    /**
     * @param bookedTravelerList
     * @param abstractAncillaryList
     * @param authorizationResult
     * @param transactionTime
     * @return
     */
    public static PaymentLinked addPaymentId(
            List<BookedTraveler> bookedTravelerList,
            List<AbstractAncillary> abstractAncillaryList, AuthorizationResult authorizationResult,
            long transactionTime
    ) {

        PaymentLinked paymentLinked = new PaymentLinked();

        for (AbstractAncillary abstractAncillary : abstractAncillaryList) {

            if (abstractAncillary instanceof TravelerAncillary) {
                TravelerAncillary travelerAncillary = (TravelerAncillary) abstractAncillary;
                addPaymentIdToOriginalCollection(bookedTravelerList, travelerAncillary.getStoredId(),
                        authorizationResult.getPaymentId());
                travelerAncillary.setPaymentId(authorizationResult.getPaymentId());
            } else if (abstractAncillary instanceof ExtraWeightAncillary) {
                ExtraWeightAncillary extraWeightAncillary = (ExtraWeightAncillary) abstractAncillary;
                addPaymentIdToOriginalCollection(bookedTravelerList, extraWeightAncillary.getStoredId(),
                        authorizationResult.getPaymentId());
                extraWeightAncillary.setPaymentId(authorizationResult.getPaymentId());
            } else if (abstractAncillary instanceof CabinUpgradeAncillary) {
                CabinUpgradeAncillary cabinUpgradeAncillary = (CabinUpgradeAncillary) abstractAncillary;
                addPaymentIdToOriginalCollection(bookedTravelerList, cabinUpgradeAncillary.getStoredId(),
                        authorizationResult.getPaymentId());
                cabinUpgradeAncillary.setPaymentId(authorizationResult.getPaymentId());
            }
        }

        paymentLinked.setPaymentId(authorizationResult.getPaymentId());
        paymentLinked.setAuthorizationResult(authorizationResult);
        paymentLinked.setTransactionDate(new Date(transactionTime));
        paymentLinked.setAncillaries(abstractAncillaryList);
        return paymentLinked;
    }

    public static boolean isGroupReservation(List<PassengerDataResponseACS> passDataList) {
        for (PassengerDataResponseACS passengerDataResponseACS : passDataList) {
            if (null != passengerDataResponseACS.getGroupCode() && passengerDataResponseACS.getGroupCode().length() > 0
                    && null == passengerDataResponseACS.getLastName()) {
                return true;
            }
        }
        return false;
    }

}
