
package com.am.checkin.web.util;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class uses CDI to alias Java EE resources, such as the logger
 * 
 * <p>
 * Inject
 * </p>
 * 
 * <pre>
 * &#064;Inject
 * </pre>
 */
public class Resources {

	@Produces
	public Logger produceLog(InjectionPoint injectionPoint) {
		return LoggerFactory.getLogger( injectionPoint.getMember().getDeclaringClass().getName() );
	}

}
