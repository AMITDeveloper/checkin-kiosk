package com.am.checkin.web.util;

import com.aeromexico.commons.model.*;
import com.aeromexico.commons.model.weather.AirportWeather;
import com.aeromexico.commons.web.types.*;
import com.aeromexico.commons.web.util.FlightNumberUtil;
import com.aeromexico.commons.web.util.TimeUtil;
import com.aeromexico.commons.web.util.ValidateCheckinIneligibleReason;
import com.aeromexico.dao.util.CommonUtil;
import com.aeromexico.sharedservices.util.TravelerItineraryUtil;
import com.am.checkin.web.service.PNRLookupService;
import com.am.checkin.web.v2.util.SystemVariablesUtil;
import com.google.gson.Gson;
import com.sabre.services.acs.bso.flightdetail.v3.ACSFlightDetailRSACS;
import com.sabre.services.checkin.getpassengerdata.v4.PassengerDataResponseACS;
import com.sabre.services.checkin.getpassengerdata.v4.PassengerItineraryACS;
import com.sabre.services.checkin.getpassengerdata.v4.VCRInfoACS;
import com.sabre.services.res.or.getreservation.OpenReservationElementType;
import com.sabre.services.res.or.getreservation.ServiceRequestType;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.sabre.services.stl.v3.BaggageRouteACS;
import com.sabre.services.stl.v3.Remarks;
import com.sabre.webservices.pnrbuilder.getreservation.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PNRLookUpServiceUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(PNRLookUpServiceUtil.class);

    // PNR
    private static final Pattern PNR_PATTERN = Pattern.compile("[A-Z]{6}");

    // TN PNR "^[a-zA-Z0-9_]*$"
    private static final Pattern PNR_TN_PATTERN = Pattern.compile("[A-Z0-9]{6}");

    // eTicket
    private static final Pattern E_TICKET_PATTERN = Pattern.compile("[0-9]{13}");

    // FFProgram
    private static final Pattern FF_PROGRAM_PATTERN = Pattern.compile("[A-Z]{2}");

    //IATA Code Airport
    private static final Pattern IATA_CODE_AIRPORT_PATTERN = Pattern.compile("[A-Z]{3}");

    private static final String PATTERN_SABRE_CONTACT_NUMBER = "[A-Z0-9]{2}--[0-9]{10,12}-[A-Z]{1}-[0-9]{1,2}\\.[0-9]{1,2}";
    private static final String CAPTURE_REMARK = "SHOW CAPTURE EMAIL PAX ";

    public static boolean isEligibleToBeAddedToTheCart(
            BookedLeg bookedLeg, String pos, boolean isLastLeg
    ) {
        return !bookedLeg.isSeamlessCheckin()
                && (BookedLegCheckinStatusType.OPEN.equals(bookedLeg.getCheckinStatus())

                || (isLastLeg && !PosType.KIOSK.toString().equalsIgnoreCase(pos)
                && (BookedLegCheckinStatusType.CLOSED.equals(bookedLeg.getCheckinStatus())
                || BookedLegCheckinStatusType.FINAL.equals(bookedLeg.getCheckinStatus())))

                || (bookedLeg.isStandByReservation()
                && SystemVariablesUtil.isEarlyCheckinEnabled()
                && bookedLeg.getCheckinStatus().toString().contains("CLOSED_GROUND_HANDLING_BY")));
    }

    public static boolean compareNames(String current, String other) {
        if (null == current && null == other) {
            return true;
        }
        if (null == current || null == other) {
            return false;
        }

        return current.trim().replaceAll("\\s+", "")
                .equalsIgnoreCase(other.trim().replaceAll("\\s+", ""));
    }

    public static String typeOfTransformation(List<BookedLeg> legs) {
        int futureFlights = 0;
        int finalFlights = 0;
        int flownFlights = 0;
        int openFlights = 0;
        int groundHandledFlights = 0;
        int notOpByAM = 0;
        int closedFlights = 0;
        int closedSeamlessFlights = 0;
        int cancelledFlights = 0;
        int openSeamlessFlights = 0;
        int futureSeamlessFlights = 0;
        int other = 0;

        for (BookedLeg bookedLeg : legs) {
            if (BookedLegFlightStatusType.FLOWN.equals(bookedLeg.getFlightStatus())
                    || BookedLegFlightStatusType.ON_AIR.equals(bookedLeg.getFlightStatus())) {
                flownFlights++;
            } else if ((BookedLegFlightStatusType.ON_TIME.equals(bookedLeg.getFlightStatus())
                    || BookedLegFlightStatusType.OPENCI.equals(bookedLeg.getFlightStatus()))
                    && BookedLegCheckinStatusType.OPEN.equals(bookedLeg.getCheckinStatus())) {
                if (bookedLeg.isSeamlessCheckin()) {
                    openSeamlessFlights++;
                } else {
                    openFlights++;
                }
            } else if (BookedLegFlightStatusType.DELAYED.equals(bookedLeg.getFlightStatus())
                    && BookedLegCheckinStatusType.OPEN.equals(bookedLeg.getCheckinStatus())) {
                if (bookedLeg.isSeamlessCheckin()) {
                    openSeamlessFlights++;
                } else {
                    openFlights++;
                }
            } else if ((BookedLegFlightStatusType.ON_TIME.equals(bookedLeg.getFlightStatus())
                    || BookedLegFlightStatusType.OPENCI.equals(bookedLeg.getFlightStatus()))
                    && (BookedLegCheckinStatusType.CLOSED_DOMESTIC_WINDOW.equals(bookedLeg.getCheckinStatus())
                    || BookedLegCheckinStatusType.CLOSED_INTERNATIONAL_WINDOW.equals(bookedLeg.getCheckinStatus()))) {
                if (bookedLeg.isSeamlessCheckin()) {
                    futureSeamlessFlights++;
                } else {
                    futureFlights++;
                }
            } else if ((BookedLegFlightStatusType.ON_TIME.equals(bookedLeg.getFlightStatus())
                    || BookedLegFlightStatusType.OPENCI.equals(bookedLeg.getFlightStatus()))
                    && (BookedLegCheckinStatusType.CLOSED_GROUND_HANDLING_BY_AF.equals(bookedLeg.getCheckinStatus())
                    || BookedLegCheckinStatusType.CLOSED_GROUND_HANDLING_BY_ATO.equals(bookedLeg.getCheckinStatus())
                    || BookedLegCheckinStatusType.CLOSED_GROUND_HANDLING_BY_DL.equals(bookedLeg.getCheckinStatus())
                    || BookedLegCheckinStatusType.CLOSED_GROUND_HANDLING_BY_KL.equals(bookedLeg.getCheckinStatus())
                    || BookedLegCheckinStatusType.CLOSED_GROUND_HANDLING_BY_LF.equals(bookedLeg.getCheckinStatus())
                    || BookedLegCheckinStatusType.CLOSED_GROUND_HANDLING_BY_LH.equals(bookedLeg.getCheckinStatus()))) {
                groundHandledFlights++;
            } else if ((BookedLegFlightStatusType.ON_TIME.equals(bookedLeg.getFlightStatus())
                    || BookedLegFlightStatusType.OPENCI.equals(bookedLeg.getFlightStatus()))
                    && BookedLegCheckinStatusType.CLOSED_NOT_OPERATED_BY_AM.equals(bookedLeg.getCheckinStatus())) {
                notOpByAM++;
            } else if (BookedLegFlightStatusType.CLOSED.equals(bookedLeg.getFlightStatus())) {

                if (BookedLegCheckinStatusType.FINAL.equals(bookedLeg.getCheckinStatus())) {
                    finalFlights++;
                }

                if (bookedLeg.isSeamlessCheckin()) {
                    closedSeamlessFlights++;
                } else {
                    closedFlights++;
                }

            } else if (BookedLegFlightStatusType.CANCELLED.equals(bookedLeg.getFlightStatus())) {
                cancelledFlights++;

                LOGGER.error("Fligh status is: " + bookedLeg.getFlightStatus().toString() + " not considered");
            } else {
                other++;

                LOGGER.error("Fligh status is: " + bookedLeg.getFlightStatus().toString() + " not considered");
            }
        }

        // If there are at least one leg open for checkIn
        if (openFlights > 0 || openSeamlessFlights > 0 || groundHandledFlights > 0 || notOpByAM > 0) {// || finalFlights > 0

            if (openFlights > 0 || openSeamlessFlights > 0) {
                return "openFlights";
            } else if (groundHandledFlights > 0) {// If there are only flights ground handled by other airlines
                return "groundHandled";
            } else if (notOpByAM > 0) {// If there are only flights operated by other airlines
                return "notOpByAM";
            }
//            else if (finalFlights > 0) {// If there are
//                // flight operated
//                // by other airlines
//                return "finalFlights";
//            }
        } else {

            // If all flight are flown
            if (legs.size() == flownFlights) {
                return "flownFlights";
            }

            // If all flights are in the future
            if (legs.size() == (futureFlights + futureSeamlessFlights)) {
                return "futureFlights";
            }

            // If all flights are closed
            if (legs.size() == (closedFlights + closedSeamlessFlights)) {
                return "closedFlights";
            }

            // If all flights are cancelled
            if (legs.size() == cancelledFlights) {
                return "cancelledFlights";
            }

            if (futureFlights > 0 || futureSeamlessFlights > 0) {
                // If there are only flown and future flights.
                if (closedFlights > 0 || closedSeamlessFlights > 0) {
                    return "closeFutureFlights";
                }

                return "futureFlights";
            } else if (closedFlights > 0) { // If there are only flown and closed flights.
                return "closedFlights";
            } else if (flownFlights > 0) { // If there are only flown and cancelled flights.
                return "flownFlights";
            } else if (cancelledFlights > 0) {
                return "cancelledFlights";
            }
        }

        return "cancelledFlights";
    }

    public static BookedLeg getLastFinalClosedFlightInLegColl(List<BookedLeg> legs) {

        for (int i = (legs.size() - 1); i >= 0; i--) {
            BookedLeg bookedLeg = legs.get(i);

            if (BookedLegFlightStatusType.CLOSED.equals(bookedLeg.getFlightStatus())
                    && BookedLegCheckinStatusType.FINAL.equals(bookedLeg.getCheckinStatus())) {
                return bookedLeg;
            }
        }

        return null;
    }

    public static BookedLeg getLastClosedFlightInLegColl(List<BookedLeg> legs) {

        for (int i = (legs.size() - 1); i >= 0; i--) {
            BookedLeg bookedLeg = legs.get(i);

            if (BookedLegFlightStatusType.CLOSED.equals(bookedLeg.getFlightStatus())) {
                return bookedLeg;
            }
        }

        return null;
    }

    public static BookedLeg getLastCancelledFlightInLegColl(List<BookedLeg> legs) {

        for (int i = (legs.size() - 1); i >= 0; i--) {
            BookedLeg bookedLeg = legs.get(i);

            if (BookedLegFlightStatusType.CANCELLED.equals(bookedLeg.getFlightStatus())) {
                return bookedLeg;
            }
        }

        return null;
    }

    public static BookedLeg getLastFlownFlightInLegColl(List<BookedLeg> legs) {

        for (int i = (legs.size() - 1); i >= 0; i--) {

            BookedLeg bookedLeg = legs.get(i);

            if (BookedLegFlightStatusType.FLOWN.equals(bookedLeg.getFlightStatus())
                    || BookedLegFlightStatusType.ON_AIR.equals(bookedLeg.getFlightStatus())) {

                return bookedLeg;
            }
        }

        return null;
    }

    public static BookedLeg getFirstFutureFlightInLegColl(List<BookedLeg> legs) {
        for (BookedLeg bookedLeg : legs) {
            if (BookedLegFlightStatusType.ON_TIME.equals(bookedLeg.getFlightStatus())
                    && (BookedLegCheckinStatusType.CLOSED_DOMESTIC_WINDOW
                    .equals(bookedLeg.getCheckinStatus())
                    || BookedLegCheckinStatusType.CLOSED_INTERNATIONAL_WINDOW
                    .equals(bookedLeg.getCheckinStatus()))) {
                return bookedLeg;
            }
        }
        return null;
    }

    public static BookedLeg getFirstGroundHandledFlightInLegColl(List<BookedLeg> legs) {
        for (BookedLeg bookedLeg : legs) {
            if (BookedLegFlightStatusType.ON_TIME.equals(bookedLeg.getFlightStatus())
                    && (BookedLegCheckinStatusType.CLOSED_GROUND_HANDLING_BY_AF
                    .equals(bookedLeg.getCheckinStatus())
                    || BookedLegCheckinStatusType.CLOSED_GROUND_HANDLING_BY_ATO
                    .equals(bookedLeg.getCheckinStatus())
                    || BookedLegCheckinStatusType.CLOSED_GROUND_HANDLING_BY_DL
                    .equals(bookedLeg.getCheckinStatus())
                    || BookedLegCheckinStatusType.CLOSED_GROUND_HANDLING_BY_KL
                    .equals(bookedLeg.getCheckinStatus())
                    || BookedLegCheckinStatusType.CLOSED_GROUND_HANDLING_BY_LF
                    .equals(bookedLeg.getCheckinStatus())
                    || BookedLegCheckinStatusType.CLOSED_GROUND_HANDLING_BY_LH
                    .equals(bookedLeg.getCheckinStatus()))) {
                return bookedLeg;
            }
        }
        return null;
    }

//    public static List<TravelItineraryReadRS.TravelItinerary.ItineraryInfo.ReservationItems.Item> getReservationFlightsItems(
//            TravelItineraryReadRS travelItineraryReadRS
//    ) {
//        List<TravelItineraryReadRS.TravelItinerary.ItineraryInfo.ReservationItems.Item> reservationItems = travelItineraryReadRS
//                .getTravelItinerary().getItineraryInfo().getReservationItems().getItem().stream()
//                .filter(i -> i.getFlightSegment() != null && !i.getFlightSegment().isEmpty())
//                .collect(Collectors.toList());
//
//        return reservationItems;
//    }

    /**
     * @param bookedLeg
     * @return
     */
    public static boolean areAllFlownFlightsFromLeg(BookedLeg bookedLeg) {

        for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {
            if (!BookedLegFlightStatusType.FLOWN
                    .equals(bookedSegment.getSegment().getFlightStatus())) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param bookedLeg
     * @return
     */
    public static boolean areAllClosedFlightsFromLeg(BookedLeg bookedLeg) {

        for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {
            if (!BookedLegFlightStatusType.CLOSED
                    .equals(bookedSegment.getSegment().getFlightStatus())) {
                return false;
            }
        }

        return true;
    }

    public static String buildMessageToBeLogged(PNRCollection pnrCollection) {
        StringBuilder message = new StringBuilder();
        for (PNR pnr : pnrCollection.getCollection()) {
            for (CartPNR cartPNR : pnr.getCarts().getCollection()) {
                for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
                    message.append(bookedTraveler.getId())
                            .append(":")
                            .append(bookedTraveler.isCheckinStatus())
                            .append(",");
                }
            }
        }
        return message.toString();
    }

    /**
     * @param cartPNR
     * @param bookedLeg
     * @return Booking class is Economy At most 2 pax without INF The next
     * segment is the final (no connection) segment.
     */
    public static boolean isEligibleForVolunteerOffer(CartPNR cartPNR, BookedLeg bookedLeg) {
        if (isEconomyCabin(cartPNR) && numberOfPassengerByCart(cartPNR) <= 2
                && !isINFByCart(cartPNR)
                && isCurrentSegmentFinalAndNoConnection(bookedLeg)
                && passengerTypeAllowedOverBooking(cartPNR)
                //&& validateEditCodesAllowed(cartPNR) passenger.
                && !hasIneligiblePassengersForCkIn(cartPNR)) {
            return true;

        }

        return false;
    }

    public static boolean hasIneligiblePassengersForCkIn(CartPNR cartPNR) {
        for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
            if (!bookedTraveler.isEligibleToCheckin()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param cartPNR
     * @return Allows all except E and P Types.
     */
    public static boolean passengerTypeAllowedOverBooking(CartPNR cartPNR) {
        for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
            if ("E".equalsIgnoreCase(bookedTraveler.getPassengerType())
                    || "P".equalsIgnoreCase(bookedTraveler.getPassengerType())) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param cartPNR
     * @return
     */
    public static boolean validateEditCodesAllowed(CartPNR cartPNR) {
        for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
            if (bookedTraveler.getSsrRemarks().getSsrCodes().contains("VOL")
                    || bookedTraveler.getSsrRemarks().getSsrCodes().contains("VOLX")) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param cartPNR
     * @return
     */
    public static boolean isEconomyCabin(CartPNR cartPNR) {
        if (null != cartPNR.getTravelerInfo().getCollection() && cartPNR.getTravelerInfo().getCollection().size() > 0) {
            BookedTraveler bookedTraveler = cartPNR.getTravelerInfo().getCollection().get(0);
            if (null != bookedTraveler.getBookingClasses().getCollection()
                    && bookedTraveler.getBookingClasses().getCollection().size() > 0) {
                if ("Y".equalsIgnoreCase(bookedTraveler.getBookingClasses().getCollection().get(0).getBookingCabin())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @param cartPNR
     * @return
     */
    public static int numberOfPassengerByCart(CartPNR cartPNR) {
        int countADTPassengers = 0;
        for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
            if (null != bookedTraveler.getPaxType()
                    && PaxType.ADULT.toString().equalsIgnoreCase(bookedTraveler.getPaxType().toString())) {
                ++countADTPassengers;
            }
        }
        return countADTPassengers;
    }

    /**
     * @param cartPNR
     * @return
     */
    public static boolean isINFByCart(CartPNR cartPNR) {
        return cartPNR.getTravelerInfo().getCollection().stream()
                .anyMatch((bookedTraveler) -> (null != bookedTraveler.getInfant()));
    }

    /**
     * @param bookedLeg
     * @return
     */
    public static boolean isCurrentSegmentFinalAndNoConnection(BookedLeg bookedLeg) {
        if (BookedLegFlightStatusType.ON_TIME.toString().equalsIgnoreCase(bookedLeg.getFlightStatus().toString())) {
            if (bookedLeg.getSegments().getLegType().equalsIgnoreCase(LegType.NONSTOP.toString())) {
                for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {
                    if (BookedLegFlightStatusType.ON_TIME.toString()
                            .equalsIgnoreCase(bookedSegment.getSegment().getFlightStatus().toString())) {
                        if (bookedLeg.getLatestSegmentInLeg().getSegment().getSegmentCode()
                                .equalsIgnoreCase(bookedSegment.getSegment().getSegmentCode())) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;

    }

    /**
     * @param origin
     * @param destination
     * @param overBookingOfferList
     * @return
     */
    public static OverBooking getOverBookingOffer(AirportWeather origin, AirportWeather destination,
                                                  List<OverBookingOffer> overBookingOfferList) {
        if (null != overBookingOfferList && overBookingOfferList.size() > 0) {
            OverBookingOffer overBookingOffer = overBookingOfferList.get(0);

            for (RegionOverBooking regionOverBooking : overBookingOffer.getByRegionOverBooking()
                    .getRegionOverBooking()) {
                if (regionOverBooking.getFromRegion().equalsIgnoreCase(origin.getAirport().getRegion())
                        && regionOverBooking.getToRegion().equalsIgnoreCase(destination.getAirport().getRegion())) {

                    OverBooking overBooking = new OverBooking();
                    overBooking.setReason(regionOverBooking.getDesc());
                    AmountOfferedVolunteer amountOfferedVolunteer = new AmountOfferedVolunteer(
                            regionOverBooking.getAmountOffered().getMin().get(0).getAmount(),
                            regionOverBooking.getAmountOffered().getMax().get(0).getAmount(),
                            regionOverBooking.getAmountOffered().getMin().get(0).getCurrency());
                    overBooking.setAmountOffered(amountOfferedVolunteer);

                    return overBooking;
                }
            }

            for (RouteOverBooking routeOverBooking : overBookingOffer.getByRouteOverBooking().getRouteOverBooking()) {
                if (routeOverBooking.getFrom().equalsIgnoreCase(origin.getAirport().getCode())
                        && routeOverBooking.getTo().equalsIgnoreCase(destination.getAirport().getCode())) {
                    OverBooking overBooking = new OverBooking();
                    overBooking.setReason(routeOverBooking.getDesc());
                    AmountOfferedVolunteer amountOfferedVolunteer = new AmountOfferedVolunteer(
                            routeOverBooking.getAmountOffered().getMin().get(0).getAmount(),
                            routeOverBooking.getAmountOffered().getMax().get(0).getAmount(),
                            routeOverBooking.getAmountOffered().getMin().get(0).getCurrency());
                    overBooking.setAmountOffered(amountOfferedVolunteer);

                    return overBooking;
                }
            }
        }
        return null;
    }

    /**
     * @param segment
     * @param flightDetailRS
     */
    public static void addCapacityInfo(Segment segment, ACSFlightDetailRSACS flightDetailRS) {
        if (null != flightDetailRS
                && null != flightDetailRS.getPassengerCounts()
                && !flightDetailRS.getPassengerCounts().isEmpty()) {
            ArrayList<CabinCapacity> listCabinCapacity = new ArrayList<>();
            for (ACSFlightDetailRSACS.PassengerCounts passengerCounts : flightDetailRS.getPassengerCounts()) {
                if ("Y".equalsIgnoreCase(passengerCounts.getClassOfService())) {
                    listCabinCapacity
                            .add(new CabinCapacity("main", passengerCounts.getBooked(), passengerCounts.getAuthorized(),
                                    passengerCounts.getTotalBoardingPassIssued(), passengerCounts.getTotalOnBoard()));
                } else {// if
                    // ("J".equalsIgnoreCase(passengerCounts.getClassOfService()))
                    listCabinCapacity.add(
                            new CabinCapacity("premier", passengerCounts.getBooked(), passengerCounts.getAuthorized(),
                                    passengerCounts.getTotalBoardingPassIssued(), passengerCounts.getTotalOnBoard()));
                }
            }
            segment.setCapacity(listCabinCapacity);
        } else {
            LOGGER.info("Empty flightDetailRS, can not set capacity");
        }
    }

    /**
     * @param firstNamePaxData
     * @param lastNamePaxData
     * @param passengerDetails
     * @return
     */
//    public static String getFirstNameFromTravelIti(
//            String firstNamePaxData,
//            String lastNamePaxData,
//            List<TravelItineraryReadRS.TravelItinerary.CustomerInfo.PersonName> passengerDetails
//    ) {
//        for (TravelItinerary.CustomerInfo.PersonName personName : passengerDetails) {
//            if (PNRLookUpServiceUtil.compareNames(personName.getGivenName(), firstNamePaxData)
//                    && PNRLookUpServiceUtil.compareNames(personName.getSurname(), lastNamePaxData)
//            ) {
//
//                return personName.getGivenName();
//            }
//        }
//        return firstNamePaxData;
//    }

    /**
     * @param nameId
     * @param passengerDetails
     * @return
     */
//    public static String getFirstNameFromTravelIti(
//            String nameId,
//            List<TravelItineraryReadRS.TravelItinerary.CustomerInfo.PersonName> passengerDetails
//    ) {
//        for (TravelItinerary.CustomerInfo.PersonName personName : passengerDetails) {
//            if (nameId.equalsIgnoreCase(personName.getNameNumber())
//            ) {
//
//                return personName.getGivenName();
//            }
//        }
//        return null;
//    }

    /**
     * @param firstNamePaxData
     * @param lastNamePaxData
     * @param passengerDetails
     * @return
     */
//    public static String getEmailFromTravelIti(
//            String firstNamePaxData,
//            String lastNamePaxData,
//            List<TravelItineraryReadRS.TravelItinerary.CustomerInfo.PersonName> passengerDetails
//    ) {
//        for (TravelItinerary.CustomerInfo.PersonName personName : passengerDetails) {
//            try {
//                if (PNRLookUpServiceUtil.compareNames(personName.getGivenName(), firstNamePaxData)
//                        && PNRLookUpServiceUtil.compareNames(personName.getSurname(), lastNamePaxData)
//                ) {
//
//                    if (null != personName.getEmail() && !personName.getEmail().isEmpty()) {
//                        return personName.getEmail().get(0).getValue();
//                    } else {
//                        return null;
//                    }
//                }
//            } catch (Exception ex) {
//                LOGGER.error(ex.getMessage(), ex);
//            }
//        }
//        return null;
//    }

    /**
     * Get email list by first and last name given
     *
     * @param firstNamePaxData
     * @param lastNamePaxData
     * @param passengerDetails
     * @return
     */
//    public static List<String> getEmailsPerPassenger(
//            String firstNamePaxData,
//            String lastNamePaxData,
//            List<TravelItineraryReadRS.TravelItinerary.CustomerInfo.PersonName> passengerDetails
//    ) {
//
//        TravelItineraryReadRS.TravelItinerary.CustomerInfo.PersonName personName = passengerDetails.stream()
//                .filter(pax -> (
//                        PNRLookUpServiceUtil.compareNames(pax.getGivenName(), firstNamePaxData)
//                                && PNRLookUpServiceUtil.compareNames(pax.getSurname(), lastNamePaxData)
//                ))
//                .findAny()
//                .orElse(null);
//
//        if (personName != null) {
//            try {
//                return personName.getEmail().stream()
//                        .map(TravelItineraryReadRS.TravelItinerary.CustomerInfo.PersonName.Email::getValue)
//                        .collect(Collectors.toList());
//            } catch (Exception e) {
//                return null;
//            }
//        }
//
//        return null;
//    }

    /**
     * Get email list by first and last name given
     *
     * @param nameId
     * @param passengerDetails
     * @return
     */
    public static List<String> getEmailsPerPassenger(
            String nameId,
            List<PassengerPNRB> passengerDetails
    ) {

        PassengerPNRB passenger = passengerDetails.stream()
                .filter(pax -> (
                        nameId.equalsIgnoreCase(pax.getReferenceNumber())
                ))
                .findAny()
                .orElse(null);

        if (passenger != null) {
            try {
                return passenger.getEmailAddress().stream()
                        .map(EmailAddressPNRB::getAddress)
                        .collect(Collectors.toList());
            } catch (Exception e) {
                return null;
            }
        }

        return null;
    }

    /**
     * Get all phones numbers by nameRefNumber
     *
     * @param nameRefNumber  String. e.g 1.1
     * @param contactNumbers PhoneNumbersPNRB. {@link PhoneNumbersPNRB}
     * @return
     */
    public static List<Phone> getPhonesPerPassenger(
            String nameRefNumber,
            PhoneNumbersPNRB contactNumbers
    ) {
        List<Phone> phones = new ArrayList<>();

        if (null == nameRefNumber || nameRefNumber.trim().isEmpty()) {
            return phones;
        }

        Pattern patternSabrePhone = Pattern.compile(PATTERN_SABRE_CONTACT_NUMBER);

        if (null != contactNumbers && null != contactNumbers.getPhoneNumber()) {
            for (PhoneNumberPNRB cn : contactNumbers.getPhoneNumber()) {
                if (cn.getNumber() != null && cn.getNumber().contains(getSabreHostPaxId(nameRefNumber))) {

                    Matcher matcherSabrePhone = patternSabrePhone.matcher(cn.getNumber());

                    if (matcherSabrePhone.find()) {
                        phones.add(createPhone(cn.getNumber()));
                    } else {
                        if (cn.getNumber().contains("/")) {
                            String[] parts = cn.getNumber().split("/");

                        }
                    }
                }
            }
        }

        return phones;
    }

    /**
     * Create Phone Object from sabre contact number string
     *
     * @param phoneNumber String. e.g MX--525591327555-M-2.1
     * @return Phone. {@link Phone}
     */
    private static Phone createPhone(String phoneNumber) {
        Phone phone = new Phone();

        phone.setCountry(phoneNumber.substring(0, 2));
        phoneNumber = phoneNumber.substring(4); //525591327555-M-2.1        

        int indexScore = phoneNumber.indexOf("-");
        String pn = phoneNumber.substring(0, indexScore);

        phone.setNumber(pn.length() > 10 ? pn.substring(2, pn.length()) : pn);
        phone.setType(PhoneType.fromCode(phoneNumber.substring(indexScore + 1, indexScore + 2)));

        return phone;
    }

    /**
     * Trime zeros to pax id from sabre service
     *
     * @param nameRefNumber String. e.g 01.01
     * @return String. e.g 1.1
     */
    private static String getSabreHostPaxId(String nameRefNumber) {
        String[] paxArray = nameRefNumber.split("\\.");
        String firstPart = paxArray[0].substring(0, 1).equals("0")
                ? paxArray[0].substring(1, paxArray[0].length())
                : paxArray[0];
        String secondPart = paxArray[1].substring(0, 1).equals("0")
                ? paxArray[1].substring(1, paxArray[1].length())
                : paxArray[1];

        return firstPart + "." + secondPart;
    }

    /**
     * Find remark with show capture email text
     *
     * @param nameRefNumber String. e.g 01.01
     * @param remarkInfo    RemarkInfo. {@link RemarksPNRB}
     * @return boolean. false if there is remark, true in other case
     */
    public static boolean showCaptureEmailRemark(String nameRefNumber,
                                                 RemarksPNRB remarkInfo) {
        try {
            return  !remarkInfo.getRemark().stream().anyMatch(remark ->
                        remark.getRemarkLines().getRemarkLine().stream()
                                .anyMatch(remarkLine -> (CAPTURE_REMARK + nameRefNumber).equals(remarkLine.getText())));
        } catch (Exception e) {
            return true;
        }
    }

    /**
     * @param tierBenefitOffer
     * @param origin
     * @param arrival
     * @param legCode
     * @param bookingClassMap
     * @return
     */
    public static BookedTravelerBenefit getTierBenefit(TierBenefit tierBenefitOffer, AirportWeather origin, AirportWeather arrival, String legCode, BookingClassMap bookingClassMap) {
        BookedTravelerBenefit bookedTravelerBenefit = new BookedTravelerBenefit(new Meta("TierBenefit"));
        //Setting SeatInfo
        bookedTravelerBenefit.setSeat(
                getBookedTravelerSeatBenefit(
                        tierBenefitOffer.getSeat(),
                        tierBenefitOffer.getTier()
                )
        );

        //Setting BagInfo
        bookedTravelerBenefit.setBag(
                getBagBenefit(
                        tierBenefitOffer.getBag(),
                        origin, arrival, legCode,
                        isEconomyCabin(bookingClassMap),
                        tierBenefitOffer.getTier()
                )
        );

        //Setting Code
        bookedTravelerBenefit.setCode(tierBenefitOffer.getTier());

        return bookedTravelerBenefit;
    }

    public static BookedTravelerSeatBenefit getBookedTravelerSeatBenefit(SeatBenefit seatBenefit, String reason) {
        BookedTravelerSeatBenefit bookedTravelerSeatBenefit = new BookedTravelerSeatBenefit();
        bookedTravelerSeatBenefit.setReason(reason);
        bookedTravelerSeatBenefit.setFeeRequired(seatBenefit.getFeeRequired());
        return bookedTravelerSeatBenefit;
    }

    /**
     * @param bagBenefit
     * @param origin
     * @param arrival
     * @param legCode
     * @param isEconomyCabin
     * @param reason
     * @return
     */
    public static BookedTravelerBagBenefit getBagBenefit(
            BagBenefit bagBenefit,
            AirportWeather origin,
            AirportWeather arrival,
            String legCode,
            boolean isEconomyCabin,
            String reason
    ) {
        //Search bySpecificRoute
        if (null != bagBenefit.getByRoute().getRoute()) {
            for (RouteBenefit routeBenefit : bagBenefit.getByRoute().getRoute()) {
                if (routeBenefit.getFrom().equalsIgnoreCase(origin.getAirport().getCode())
                        && routeBenefit.getTo().equalsIgnoreCase(arrival.getAirport().getCode())) {
                    if (isEconomyCabin) {
                        for (CabinBenefit cabinBenefit : routeBenefit.getCabin()) {
                            if ("main".equalsIgnoreCase(cabinBenefit.getType())) {
                                return getBookedTravelerBagBenefit(cabinBenefit, legCode, reason);
                            }
                        }
                    } else {
                        for (CabinBenefit cabinBenefit : routeBenefit.getCabin()) {
                            if ("premier".equalsIgnoreCase(cabinBenefit.getType())) {
                                return getBookedTravelerBagBenefit(cabinBenefit, legCode, reason);
                            }
                        }
                    }
                }
            }
        }

        //Search by Country
        if (null != bagBenefit.getByCountry().getCountry()) {
            for (CountryBenefit countryBenefit : bagBenefit.getByCountry().getCountry()) {
                if (null != countryBenefit.getFromCountry() && countryBenefit.getFromCountry().equalsIgnoreCase(origin.getAirport().getCountry())
                        && null != countryBenefit.getToCountry() && countryBenefit.getToCountry().equalsIgnoreCase(arrival.getAirport().getCountry())) {
                    if (isEconomyCabin) {
                        for (CabinBenefit cabinBenefit : countryBenefit.getCabin()) {
                            if ("main".equalsIgnoreCase(cabinBenefit.getType())) {
                                return getBookedTravelerBagBenefit(cabinBenefit, legCode, reason);
                            }
                        }
                    } else {
                        for (CabinBenefit cabinBenefit : countryBenefit.getCabin()) {
                            if ("premier".equalsIgnoreCase(cabinBenefit.getType())) {
                                return getBookedTravelerBagBenefit(cabinBenefit, legCode, reason);
                            }
                        }
                    }
                }
            }
        }

        //Search by Region
        if (null != bagBenefit.getByRegion().getRegion()) {
            for (RegionBenefit regionBenefit : bagBenefit.getByRegion().getRegion()) {
                if (regionBenefit.getFromRegion().equalsIgnoreCase(origin.getAirport().getRegion())
                        && regionBenefit.getToRegion().equalsIgnoreCase(arrival.getAirport().getRegion())) {
                    if (isEconomyCabin) {
                        for (CabinBenefit cabinBenefit : regionBenefit.getCabin()) {
                            if ("main".equalsIgnoreCase(cabinBenefit.getType())) {
                                return getBookedTravelerBagBenefit(cabinBenefit, legCode, reason);
                            }
                        }
                    } else {
                        for (CabinBenefit cabinBenefit : regionBenefit.getCabin()) {
                            if ("premier".equalsIgnoreCase(cabinBenefit.getType())) {
                                return getBookedTravelerBagBenefit(cabinBenefit, legCode, reason);
                            }
                        }
                    }
                }
            }
        }

        //By Deafult if exists
        if (null != bagBenefit.getByDefault()) {
            if (isEconomyCabin) {
                for (CabinBenefit cabinBenefit : bagBenefit.getByDefault().getCabinCollection()) {
                    if ("main".equalsIgnoreCase(cabinBenefit.getType())) {
                        return getBookedTravelerBagBenefit(cabinBenefit, legCode, reason);
                    }
                }
            } else {
                for (CabinBenefit cabinBenefit : bagBenefit.getByDefault().getCabinCollection()) {
                    if ("premier".equalsIgnoreCase(cabinBenefit.getType())) {
                        return getBookedTravelerBagBenefit(cabinBenefit, legCode, reason);
                    }
                }
            }

        }
        return null;
    }

    /**
     * @param bookingClassMap
     * @return
     */
    public static boolean isEconomyCabin(BookingClassMap bookingClassMap) {

        if (null != bookingClassMap.getCollection() && bookingClassMap.getCollection().size() > 0) {
            if ("Y".equalsIgnoreCase(bookingClassMap.getCollection().get(0).getBookingCabin())) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param cabinBenefit
     * @param legCode
     * @param reason
     * @return
     */
    public static BookedTravelerBagBenefit getBookedTravelerBagBenefit(CabinBenefit cabinBenefit, String legCode, String reason) {
        List<CheckedWeight> checkedWeightList = getValidWeight(cabinBenefit.getExtraBag().getWeight());

        FreeBaggageAllowanceDetail freeBaggageAllowance = new FreeBaggageAllowanceDetail(0,
                0,
                ((Double) cabinBenefit.getExtraBag().getChecked()).intValue(),
                checkedWeightList);

        BookedTravelerBagBenefit BookedTravelerBagBenefit = new BookedTravelerBagBenefit(reason, legCode, freeBaggageAllowance);
        return BookedTravelerBagBenefit;
    }

    /**
     * @param listWeight
     * @return
     */
    public static List<CheckedWeight> getValidWeight(List<WeightDB> listWeight) {
        List<CheckedWeight> checkedWeightList = new LinkedList<>();
        for (WeightDB weightDB : listWeight) {
            CheckedWeight checkedWeight = new CheckedWeight(0, weightDB.getValue().intValue(), weightDB.getUnit());
            checkedWeightList.add(checkedWeight);
        }
        return checkedWeightList;
    }

    /**
     * @param bookedTraveler
     * @param listCustLoyalty
     */
    public static void addFrequentFlyerIfNotExistsInPaxData(
            BookedTraveler bookedTraveler,
            List<FrequentFlyerPNRB> listCustLoyalty
    ) {
        if (null == bookedTraveler.getFrequentFlyerNumber()) {
            for (FrequentFlyerPNRB custLoyalty : listCustLoyalty) {
                if (custLoyalty.getNameNumber().equalsIgnoreCase(bookedTraveler.getNameRefNumber())) {
                    bookedTraveler.setFrequentFlyerProgram(custLoyalty.getSupplierCode());
                    bookedTraveler.setFrequentFlyerNumber(custLoyalty.getNumber());
                }
            }
        }
    }

    public static TierLevelType getTierLevelType(
            PassengerDataResponseACS passengerDataResponse,
            List<FrequentFlyerPNRB> listCustLoyalty,
            BookedTraveler bookedTraveler
    ) {

        TierLevelType tierLevelType;
        try {

            tierLevelType = getTierLevelTypeFromItinerary(
                    passengerDataResponse.getPassengerItineraryList().getPassengerItinerary()
            );

            if (null == tierLevelType) {
                for (FrequentFlyerPNRB custLoyalty : listCustLoyalty) {
                    System.out.println("custLoyalty.getNameNumber() = " + custLoyalty.getNameNumber());
                    if (custLoyalty.getNameNumber().equalsIgnoreCase(bookedTraveler.getNameRefNumber())) {
                        if ("AM".equalsIgnoreCase(custLoyalty.getSupplierCode())
                                || "DL".equalsIgnoreCase(custLoyalty.getSupplierCode())) {
                            if (null != custLoyalty.getShortText()) {
                                if (custLoyalty.getShortText().contains("/")) {
                                    List<String> tierLevel = Arrays.asList(custLoyalty.getShortText().split("/"));
                                    if (!tierLevel.isEmpty()) {
                                        tierLevelType = TierLevelType.getType(tierLevel.get(0));
                                    }
                                } else {
                                    tierLevelType = TierLevelType.getType(custLoyalty.getShortText());
                                }
                            }
                        } else {
                            tierLevelType = null;
                        }
                    }
                }
            }
        } catch (Exception ex) {
            tierLevelType = null;
        }

        return tierLevelType;
    }

    /**
     * @param passengerItinerary
     * @return
     */
    public static TierLevelType getTierLevelTypeFromItinerary(
            List<com.sabre.services.checkin.getpassengerdata.v4.PassengerItineraryACS> passengerItinerary
    ) {

        for (PassengerItineraryACS passengerItineraryACS : passengerItinerary) {
            if (null != passengerItineraryACS.getTierLevel()
                    && passengerItineraryACS.getTierLevel().length() > 0) {
                return TierLevelType.getType(passengerItineraryACS.getTierLevel());
            }
        }

        return null;
    }

    /**
     * @param travelItineraryReadRS
     * @return
     */
//    public static List<TravelItineraryReadRS.TravelItinerary.CustomerInfo.CustLoyalty> getCustLoyalty(
//            TravelItineraryReadRS travelItineraryReadRS) {
//        List<TravelItineraryReadRS.TravelItinerary.CustomerInfo.CustLoyalty> listCustLoyalty = new ArrayList<>();
//
//        TravelItinerary travelItinerary = travelItineraryReadRS.getTravelItinerary();
//        if (travelItinerary != null) {
//            TravelItinerary.CustomerInfo customerInfo = travelItinerary.getCustomerInfo();
//            if (customerInfo != null) {
//                listCustLoyalty = customerInfo.getCustLoyalty();
//            }
//        }
//        return listCustLoyalty;
//    }
 /**
     * @param getReservationRS
     * @return
     */
    public static List<FrequentFlyerPNRB> getCustLoyalty(GetReservationRS getReservationRS) {
        List<FrequentFlyerPNRB> listCustLoyalty = new ArrayList<>();

        for(PassengerPNRB passenger : getReservationRS.getReservation().getPassengerReservation().getPassengers().getPassenger()) {
            if (passenger != null) {
               for(FrequentFlyerPNRB customerInfo : passenger.getFrequentFlyer()) {
                   if (customerInfo != null) {
                       customerInfo.setNameNumber(passenger.getNameId());
                       listCustLoyalty.add(customerInfo);
                   }
               }
            }
        }


        return listCustLoyalty;
    }

    /**
     * @param bookedTraveler
     * @return
     */
    public static boolean isOverbookingEligible(BookedTraveler bookedTraveler) {
        try {
            if (null != bookedTraveler.getSsrRemarks() && null != bookedTraveler.getSsrRemarks().getSsrCodes()
                    && !bookedTraveler.getSsrRemarks().getSsrCodes().isEmpty()) {
                if (bookedTraveler.getSsrRemarks().getSsrCodes().contains("VOL")
                        || bookedTraveler.getSsrRemarks().getSsrCodes().contains("VOLX")) {
                    LOGGER.info("isOverbookingEligible false CUZ VOL: {}", bookedTraveler.getId());
                    return false;
                }
            }

            if (null != bookedTraveler.getSeatAncillaries()
                    && null != bookedTraveler.getSeatAncillaries().getCollection()) {
                for (AbstractAncillary abstractAncillary : bookedTraveler.getSeatAncillaries().getCollection()) {
                    if (abstractAncillary instanceof TravelerAncillary) {
                        TravelerAncillary travelerAncillary = (TravelerAncillary) abstractAncillary;
                        if (("SEAT ASSIGNMENT".equalsIgnoreCase(travelerAncillary.getAncillary().getCommercialName())
                                || "ASIENTO PAGADO PAIDSEAT"
                                .equalsIgnoreCase(travelerAncillary.getAncillary().getCommercialName()))
                                && 1 == travelerAncillary.getAncillary().getCurrency().getTotal()
                                .compareTo(BigDecimal.ZERO)
                                && !travelerAncillary.isDefaultFreeSeat() && travelerAncillary.getTotalPrice() != null
                                && travelerAncillary.getTotalPrice().getTotal() != null
                                && -1 == BigDecimal.ZERO.compareTo(travelerAncillary.getTotalPrice().getTotal())) {
                            LOGGER.info("isOverbookingEligible false CUZ PAID seat 1: {}", bookedTraveler.getId());
                            return false;
                        }
                    } else if (abstractAncillary instanceof TicketedTravelerAncillary) {
                        TicketedTravelerAncillary ticketedTravelerAncillary = (TicketedTravelerAncillary) abstractAncillary;
                        if (("SEAT ASSIGNMENT"
                                .equalsIgnoreCase(ticketedTravelerAncillary.getAncillary().getCommercialName())
                                || "ASIENTO PAGADO PAIDSEAT"
                                .equalsIgnoreCase(ticketedTravelerAncillary.getAncillary().getCommercialName()))
                                && 1 == ticketedTravelerAncillary.getAncillary().getCurrency().getTotal()
                                .compareTo(BigDecimal.ZERO)
                                && !ticketedTravelerAncillary.isDefaultFreeSeat()
                                && ticketedTravelerAncillary.getTotalPrice() != null
                                && ticketedTravelerAncillary.getTotalPrice().getTotal() != null && -1 == BigDecimal.ZERO
                                .compareTo(ticketedTravelerAncillary.getTotalPrice().getTotal())) {
                            LOGGER.info("isOverbookingEligible false CUZ PAID seat 2: {}", bookedTraveler.getId());
                            return false;
                        }
                    } else if (abstractAncillary instanceof PaidTravelerAncillary) {
                        PaidTravelerAncillary paidTravelerAncillary = (PaidTravelerAncillary) abstractAncillary;
                        if (("SEAT ASSIGNMENT".equalsIgnoreCase(paidTravelerAncillary.getCommercialName())
                                || "ASIENTO PAGADO PAIDSEAT"
                                .equalsIgnoreCase(paidTravelerAncillary.getCommercialName()))
                                && !paidTravelerAncillary.isDefaultFreeSeat()
                                && paidTravelerAncillary.getTotalPrice() != null
                                && paidTravelerAncillary.getTotalPrice().getTotal() != null
                                && -1 == BigDecimal.ZERO.compareTo(paidTravelerAncillary.getTotalPrice().getTotal())) {
                            LOGGER.info("isOverbookingEligible false CUZ PAID seat 3: {}", bookedTraveler.getId());
                            return false;
                        }
                    }
                }
            }

            return true;
        } catch (Exception ex) {
            LOGGER.error("ERROR in isOverbookingEligible: " + ex.getMessage(), ex);
            return false;
        }
    }

    /**
     * @param bookedTraveler
     * @return
     */
    public static boolean isSkyPriority(BookedTraveler bookedTraveler) {
        try {
            if (null != bookedTraveler.getSsrRemarks() && null != bookedTraveler.getSsrRemarks().getSsrCodes()
                    && !bookedTraveler.getSsrRemarks().getSsrCodes().isEmpty()) {
                if (bookedTraveler.getSsrRemarks().getSsrCodes().contains("ST")) {
                    return true;
                }
            }

            return false;

        } catch (Exception ex) {
            LOGGER.error("ERROR in isSkyPriority: " + ex.getMessage(), ex);
            return false;
        }
    }

//    public static void filterTravelItinerarySegments(TravelItineraryReadRS travelItineraryReadRS) {
//
//        try {
//            Map<String, Integer> segments = new HashMap<>();
//
//            for (Item item : travelItineraryReadRS.getTravelItinerary().getItineraryInfo().getReservationItems()
//                    .getItem()) {
//
//                List<FlightSegment> flightSegmentList = item.getFlightSegment();
//
//                for (FlightSegment flightSegment : flightSegmentList) {
//                    String key = flightSegment.getOriginLocation().getLocationCode()
//                            + flightSegment.getDestinationLocation().getLocationCode();
//
//                    if (segments.containsKey(key)) {
//                        segments.put(key, segments.get(key) + 1);
//                    } else {
//                        segments.put(key, 1);
//                    }
//                }
//
//            }
//
//            Iterator<Item> itItem = travelItineraryReadRS.getTravelItinerary().getItineraryInfo().getReservationItems()
//                    .getItem().iterator();
//
//            while (itItem.hasNext()) {
//
//                Item item = itItem.next();
//
//                boolean remove = false;
//
//                List<FlightSegment> flightSegmentList = item.getFlightSegment();
//
//                for (FlightSegment flightSegment : flightSegmentList) {
//                    String key = flightSegment.getOriginLocation().getLocationCode()
//                            + flightSegment.getDestinationLocation().getLocationCode();
//                    if (segments.get(key) > 1 && !TravelerItineraryUtil.isValidStatus(flightSegment.getStatus())) {
//                        remove = true;
//                    }
//                }
//
//                if (remove) {
//                    itItem.remove();
//                }
//            }
//
//        } catch (Exception e) {
//            LOGGER.error(e.getMessage(), e);
//        }
//    }

    /**
     * @param cartPNR
     */
    public static void turnOffFlagPerPassngerOverBooking(CartPNR cartPNR) {
        for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
            bookedTraveler.setIsOverBookingEligible(false);
        }
    }

    public static String getDateOfBirthFormatToCP(String dateOfBirth) {
        if (null == dateOfBirth) {
            return null;
        }
        String dateOfBirthArray[] = dateOfBirth.split("-");
        String dateOfBirthformat = dateOfBirthArray[2] + "/" + dateOfBirthArray[1] + "/" + dateOfBirthArray[0];
        return dateOfBirthformat;
    }

    public static String getRemarkBenefit(RemarksPNRB remarkInfo, String ffnumber, String legCode) throws Exception {
        if (null == remarkInfo) {
            return null;
        } else {
            /**
             * TBCCB cobrant benefit TBCPLB club premier benefit GBBB
             */
            for (RemarkPNRB remark : remarkInfo.getRemark()) {
                for(RemarkLinePNRB remarkLine: remark.getRemarkLines().getRemarkLine()){

                    if (remarkLine.getText().contains("TBCCB")
                            || remarkLine.getText().contains("TBCPLB")
                            || remarkLine.getText().contains("GBBB")) {
                        String[] partsRemark = remarkLine.getText().split("/");
                        String legCodeRemark = partsRemark[partsRemark.length - 1];
                        String legformat = (legCodeRemark.substring(0, legCodeRemark.length() - 10)).replace("-", "_");
                        String fecha = legCodeRemark.substring(legCodeRemark.length() - 10, legCodeRemark.length());
                        String legCodeFormat = legformat + fecha;

                        if (partsRemark[1].equals(ffnumber) && legCodeFormat.equals(legCode)) {
                            return partsRemark[2];
                        }
                    }
                }
            }
        }
        return null;

    }

    public static boolean existBenefitRedemed(BookedTravelerAncillaryCollection bookedTravelerAncillaryCollection) {
        if (null == bookedTravelerAncillaryCollection || bookedTravelerAncillaryCollection.getCollection().isEmpty()) {
            return false;
        }

        for (AbstractAncillary abstractAncillary : bookedTravelerAncillaryCollection.getCollection()) {

            if (abstractAncillary instanceof PaidTravelerAncillary) {
                PaidTravelerAncillary paidTravelerAncillary = (PaidTravelerAncillary) abstractAncillary;
                if (paidTravelerAncillary.isRedeemed()) {
                    return true;
                }

            } else if (abstractAncillary instanceof TravelerAncillary) {
                TravelerAncillary travelerAncillary = (TravelerAncillary) abstractAncillary;
                if (travelerAncillary.getAncillary().isRedeemed()) {
                    return true;
                }
            }
        }
        return false;
    }

    public static BenefitRedeemed setBenefitInfo(BookedTraveler bookedTraveler, String codeBenefitRedeemed, String legCode) {
        BenefitRedeemed benefitRedeemed = new BenefitRedeemed();

        if (null == bookedTraveler.getBookedTravellerBenefitCollection()) {
            return null;
        }
        for (BookedTravelerBenefit bookedTravelerBenefit : bookedTraveler.getBookedTravellerBenefitCollection()
                .getCollection()) {
            if (null != bookedTravelerBenefit.getBag()) {
                if (bookedTravelerBenefit.getBag().getReason().equalsIgnoreCase(codeBenefitRedeemed)) {
                    benefitRedeemed.setCobrandCode(bookedTravelerBenefit.getBag().getReason());
                    benefitRedeemed.setDescription("");
                    benefitRedeemed.setLegCode(legCode);
                }
            }
            if (null != bookedTravelerBenefit.getBagBenefitCobrand()
                    && null != bookedTravelerBenefit.getBagBenefitCobrand().getFreeBagAllowanceByCardList()) {
                for (FreeBaggageAllowanceDetailByCard freeBaggageAllowanceDetailByCard : bookedTravelerBenefit
                        .getBagBenefitCobrand().getFreeBagAllowanceByCardList()) {
                    if (freeBaggageAllowanceDetailByCard.getCobrandCode().equalsIgnoreCase(codeBenefitRedeemed)) {
                        benefitRedeemed.setCobrandCode(freeBaggageAllowanceDetailByCard.getCobrandCode());
                        benefitRedeemed.setDescription(freeBaggageAllowanceDetailByCard.getDescription());
                        benefitRedeemed.setLegCode(legCode);
                    }

                }

            }

        }
        return benefitRedeemed;
    }

    public static boolean isSameNameRef(String nameAssociationId, String nameRef) {
        if (null == nameAssociationId || null == nameRef) {
            return false;
        }

        try {
            Double ref = Double.valueOf(nameRef.substring(0, 2));
            Integer id = Integer.valueOf(nameAssociationId);

            if (ref.intValue() == id.intValue()) {
                return true;
            }
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }

        return false;
    }

    public static String getSegmentCodeWithFlightNumber(PassengerItineraryACS passengerItin) {

        String operatingAirline = null == passengerItin.getOperatingAirline() ? AirlineCodeType.AM.getCode()
                : passengerItin.getOperatingAirline().toUpperCase();

        String flightNumber = "";
        if (null == passengerItin.getFlight()) {
            flightNumber = "0000";
        } else {
            flightNumber = passengerItin.getFlight();
        }

        String segmentCode
                = passengerItin.getOrigin() + "_"
                + passengerItin.getDestination() + "_"
                + operatingAirline + "_"
                + (passengerItin.getDepartureDate() == null ? "" : passengerItin.getDepartureDate())
                + "_0000" + "_"
                + "0000".substring(flightNumber.length())
                + flightNumber;

        return segmentCode;
    }

//    public static String getSegmentCode(
//            TravelItineraryReadRS.TravelItinerary.ItineraryInfo.ReservationItems.Item.Ancillaries.AncillaryService.Segment segment
//    ) {
//
//        String origin = segment.getBoardPoint();
//        String destination = segment.getOffPoint();
//        String airline = segment.getAirlineCode();
//        Date departureDate = segment.getDepartureDate().toGregorianCalendar().getTime();
//
//        SimpleDateFormat parseFormat = new SimpleDateFormat("yyyy-MM-dd_HHmm");
//        String departureDateStr = parseFormat.format(departureDate);
//
//        return origin + "_" + destination + "_" + airline + "_" + departureDateStr;
//    }

    public static String getSegmentCode(
            SegmentOrTravelPortionType segment
    ) {

        String origin = segment.getBoardPoint();
        String destination = segment.getOffPoint();
        String airline = segment.getAirlineCode();
        Date departureDate = segment.getDepartureDate().toGregorianCalendar().getTime();

        SimpleDateFormat parseFormat = new SimpleDateFormat("yyyy-MM-dd_HHmm");
        String departureDateStr = parseFormat.format(departureDate);

        return origin + "_" + destination + "_" + airline + "_" + departureDateStr;
    }

    public static String getSegmentCode(BaggageRouteACS baggageRoute) {

        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mma");
        SimpleDateFormat parseFormatHH = new SimpleDateFormat("HHmm");
        String time = "";
        try {
            Date scheduledDepartureDate = parseFormat.parse(baggageRoute.getDepartureTime());
            time = parseFormatHH.format(scheduledDepartureDate);
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(PNRLookupService.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (time.isEmpty()) {
            time = "0000";
        }

        String operatingAirline = null == baggageRoute.getAirline() ? AirlineCodeType.AM.getCode()
                : baggageRoute.getAirline().toUpperCase();

        String segmentCode = baggageRoute.getOrigin() + "_" + baggageRoute.getDestination() + "_" + operatingAirline
                + "_" + baggageRoute.getDepartureDate() + "_" + time;

        return segmentCode;
    }

    public static String getLegCode(Segment segment) {

        try {
            DecimalFormat decimalFormat = new DecimalFormat("0000");
            String flightNumber = decimalFormat.format(Integer.valueOf(segment.getOperatingFlightCode()));

            String legCode = segment.getDepartureAirport()
                    + "_" + segment.getOperatingCarrier()
                    + "_" + flightNumber
                    + "_" + CommonUtil.getOnlyDatePart(segment.getDepartureDateTime());

            return legCode;
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            return "";
        }
    }

//    public static String getLegCode(
//            TravelItineraryReadRS.TravelItinerary.ItineraryInfo.ReservationItems.Item.Ancillaries.AncillaryService.Segment segment
//    ) {
//
//        try {
//            DecimalFormat decimalFormat = new DecimalFormat("0000");
//            String flightNumber = decimalFormat.format(Integer.valueOf(segment.getFlightNumber()));
//
//            Date departureDate = segment.getDepartureDate().toGregorianCalendar().getTime();
//            String departureDateStr = TimeUtil.getStrTime(departureDate.getTime(), "yyyy-MM-dd");
//
//            String legCode = segment.getBoardPoint() + "_" + segment.getAirlineCode() + "_" + flightNumber + "_"
//                    + departureDateStr;
//
//            return legCode;
//        } catch (Exception ex) {
//            return "";
//        }
//    }

    public static String getLegCode( SegmentOrTravelPortionType segment ) {

        try {
            DecimalFormat decimalFormat = new DecimalFormat("0000");
            String flightNumber = decimalFormat.format(Integer.valueOf(segment.getFlightNumber()));
            Date departureDate = segment.getDepartureDate().toGregorianCalendar().getTime();
            String departureDateStr = TimeUtil.getStrTime(departureDate.getTime(), "yyyy-MM-dd");
            String legCode = segment.getBoardPoint() + "_" + segment.getAirlineCode() + "_" + flightNumber + "_" + departureDateStr;
            return legCode;
        } catch (Exception ex) {
            return "";
        }
    }

    public static PassengerPNRB getPassenger(GetReservationRS getReservationRS, String ticketNumber) {

        if (null == ticketNumber
                || ticketNumber.trim().isEmpty()
                || null == getReservationRS
                || null == getReservationRS.getReservation()
                || null == getReservationRS.getReservation().getPassengerReservation()
                || null == getReservationRS.getReservation().getPassengerReservation().getPassengers()
                || null == getReservationRS.getReservation().getPassengerReservation().getPassengers().getPassenger()
        ) {
            return null;
        }

        for (PassengerPNRB passengerPNRB : getReservationRS.getReservation().getPassengerReservation().getPassengers().getPassenger()) {
            for (GenericSpecialRequestPNRB genericSpecialRequestPNRB : passengerPNRB.getSpecialRequests().getGenericSpecialRequest()) {
                if ("TKNE".equalsIgnoreCase(genericSpecialRequestPNRB.getCode())
                        && null != genericSpecialRequestPNRB.getTicketNumber()
                        && ticketNumber.equalsIgnoreCase(genericSpecialRequestPNRB.getTicketNumber())
                ) {
                    return passengerPNRB;
                }
            }
        }


        return null;
    }

    /**
     * @param passengerDataResponse
     * @param bookedLeg
     * @param bookedTraveler
     */
    public static void setVcr(
            PassengerDataResponseACS passengerDataResponse,
            BookedLeg bookedLeg,
            BookedTraveler bookedTraveler
    ) {
        String vcr = null;
        String couponNumber = null;

        try {
            boolean exit = false;

            if (null != passengerDataResponse
                    && null != passengerDataResponse.getPassengerItineraryList()
                    && null != passengerDataResponse.getPassengerItineraryList().getPassengerItinerary()) {

                for (PassengerItineraryACS passengerItineraryACS : passengerDataResponse.getPassengerItineraryList().getPassengerItinerary()) {

                    for (BookedSegment bookedSegmentBS : bookedLeg.getSegments().getCollection()) {
                        if (FlightNumberUtil.compareFlightNumbers(bookedSegmentBS.getSegment().getOperatingFlightCode(), passengerItineraryACS.getFlight())
                                || FlightNumberUtil.compareFlightNumbers(bookedSegmentBS.getSegment().getMarketingFlightCode(), passengerItineraryACS.getFlight())) {

                            exit = true;

                            if (null != passengerDataResponse.getVCRNumber()) {
                                vcr = passengerDataResponse.getVCRNumber().getValue();
                                couponNumber = String.valueOf(passengerDataResponse.getVCRNumber().getCouponNumber());
                            }

                            break;
                        }
                    }

                    if (exit) {
                        break;
                    }
                }
            }
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }

        if (null == vcr) {
            if (null != passengerDataResponse.getVCRNumber()) {
                vcr = passengerDataResponse.getVCRNumber().getValue();
                couponNumber = String.valueOf(passengerDataResponse.getVCRNumber().getCouponNumber());
            } else {
                try {
                    for (PassengerItineraryACS passengerItineraryACS : passengerDataResponse.getPassengerItineraryList().getPassengerItinerary()) {
                        if (null != passengerItineraryACS.getVCRInfoList() && null != passengerItineraryACS.getVCRInfoList().getVCRInfo()) {
                            for (VCRInfoACS vCRInfoACS : passengerItineraryACS.getVCRInfoList().getVCRInfo()) {
                                couponNumber = String.valueOf(vCRInfoACS.getVCRNumber().getCouponNumber());
                                vcr = vCRInfoACS.getVCRNumber().getValue();
                                break;
                            }
                        }
                    }
                } catch (Exception ex) {
                    LOGGER.error(ex.getMessage(), ex);
                }
            }
        }

        bookedTraveler.setTicketNumber(vcr);
        bookedTraveler.setCouponNumber(couponNumber);
    }

    /**
     * @param passengerDataResponse
     * @param bookedLeg
     * @param bookedTraveler
     */
    public static void setAllVcrs(
            PassengerDataResponseACS passengerDataResponse,
            BookedLeg bookedLeg,
            BookedTraveler bookedTraveler
    ) {
        if (null == passengerDataResponse) {
            return;
        }

        List<String> vcrs = new ArrayList<>();

        try {

            if (null != passengerDataResponse.getVCRNumber()) {
                vcrs.add(passengerDataResponse.getVCRNumber().getValue());
            }

            if (null != passengerDataResponse.getPassengerItineraryList()
                    && null != passengerDataResponse.getPassengerItineraryList().getPassengerItinerary()) {

                for (PassengerItineraryACS passengerItineraryACS : passengerDataResponse.getPassengerItineraryList().getPassengerItinerary()) {

                    for (BookedSegment bookedSegmentBS : bookedLeg.getSegments().getCollection()) {
                        if (FlightNumberUtil.compareFlightNumbers(bookedSegmentBS.getSegment().getOperatingFlightCode(), passengerItineraryACS.getFlight())
                                || FlightNumberUtil.compareFlightNumbers(bookedSegmentBS.getSegment().getMarketingFlightCode(), passengerItineraryACS.getFlight())) {

                            if (null != passengerItineraryACS.getVCRInfoList() && null != passengerItineraryACS.getVCRInfoList().getVCRInfo()) {
                                for (VCRInfoACS vcrInfoACS : passengerItineraryACS.getVCRInfoList().getVCRInfo()) {
                                    vcrs.add(vcrInfoACS.getVCRNumber().getValue());
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }

        bookedTraveler.addAllTicketNumbers(vcrs);
    }

    /**
     * @param passengerDataResponse
     * @param bookedLeg
     */
    public static String getVcr(
            PassengerDataResponseACS passengerDataResponse,
            BookedLeg bookedLeg
    ) {

        try {
            boolean exit = false;

            if (null != passengerDataResponse && null != passengerDataResponse.getPassengerItineraryList() && null != passengerDataResponse.getPassengerItineraryList().getPassengerItinerary()) {

                for (PassengerItineraryACS passengerItineraryACS : passengerDataResponse.getPassengerItineraryList().getPassengerItinerary()) {

                    for (BookedSegment bookedSegmentBS : bookedLeg.getSegments().getCollection()) {
                        if (FlightNumberUtil.compareFlightNumbers(bookedSegmentBS.getSegment().getOperatingFlightCode(), passengerItineraryACS.getFlight())
                                || FlightNumberUtil.compareFlightNumbers(bookedSegmentBS.getSegment().getMarketingFlightCode(), passengerItineraryACS.getFlight())) {

                            exit = true;

                            if (null != passengerDataResponse.getVCRNumber()) {
                                String vcr = passengerDataResponse.getVCRNumber().getValue();

                                return vcr;
                            }

                            break;
                        }
                    }

                    if (exit) {
                        break;
                    }
                }
            }
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }

        if (null != passengerDataResponse.getVCRNumber()) {
            String vcr = passengerDataResponse.getVCRNumber().getValue();
            return vcr;
        } else {
            try {
                for (PassengerItineraryACS passengerItineraryACS : passengerDataResponse.getPassengerItineraryList().getPassengerItinerary()) {
                    if (null != passengerItineraryACS.getVCRInfoList() && null != passengerItineraryACS.getVCRInfoList().getVCRInfo()) {
                        for (VCRInfoACS vCRInfoACS : passengerItineraryACS.getVCRInfoList().getVCRInfo()) {

                            String vcr = vCRInfoACS.getVCRNumber().getValue();
                            return vcr;
                        }
                    }
                }
            } catch (Exception ex) {
                LOGGER.error(ex.getMessage(), ex);
            }
        }

        return null;
    }

    public static List<PassengerPNRB> getPassengerDetails(GetReservationRS getReservationRS) {
        List<PassengerPNRB> passengerDetails = new ArrayList<>();

        PassengersPNRB passengersPNRB = getReservationRS.getReservation().getPassengerReservation().getPassengers();
        if (passengersPNRB != null) {
                passengerDetails = passengersPNRB.getPassenger()
                        .stream().filter(i -> null != i.getFirstName() && null != i.getLastName())
                        .collect(Collectors.toList());
            }

        for(PassengerPNRB passengerPNRB : passengerDetails){
            if(null != passengerPNRB.getSpecialRequests() ){
                for(GenericSpecialRequestPNRB ssr : passengerPNRB.getSpecialRequests().getGenericSpecialRequest()){
                    if(null== passengerPNRB.getReferenceNumber()){
                        if(ssr.getFreeText().equalsIgnoreCase("INF")){
                            passengerPNRB.setReferenceNumber("INF");
                        }else if(ssr.getFreeText().contains("CHD")){
                            passengerPNRB.setReferenceNumber("CHD");
                        }else{
                            passengerPNRB.setReferenceNumber("ADT");
                        }
                    }
                }
            }
        }
        return passengerDetails;
    }

    public static PaxType getPaxType(String nameAssocID, List<PassengerPNRB> passengerDetails) {

        LOGGER.info("passengerDetails: " + new Gson().toJson(passengerDetails));

        String nameRef = "";

        for (PassengerPNRB passenger : passengerDetails) {
            if (passenger != null && nameAssocID.equalsIgnoreCase(passenger.getReferenceNumber())) {
                nameRef = passenger.getReferenceNumber();
                break;
            }
        }

        if (null == nameRef) {
            nameRef = "ADT";
        }

        LOGGER.info("getPassengerType: " + nameRef);

        return PaxType.getType(nameRef);
    }

    public static PaxType getClasificationPaxType(String nameAssocID, List<PassengerPNRB> passengerDetails, BookedTraveler bookedTraveler) {

        LOGGER.info("passengerDetails: " + new Gson().toJson(passengerDetails));

        String travelerDateOfBirth = bookedTraveler.getDateOfBirth();
        LocalDate fechaNac = null;
        LocalDate ahora = null;
        Period  periodo = null;
        DateTimeFormatter fmt = null;

        String nameRef = "";

        if(null != travelerDateOfBirth) {
            fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            fechaNac = LocalDate.parse(travelerDateOfBirth, fmt);
            ahora = LocalDate.now();
            periodo = Period.between(fechaNac, ahora);


            if(periodo.getYears() >= 12){
                LOGGER.info("ADT");
                nameRef = "ADT";
            }else if(periodo.getYears() >= 2 && periodo.getYears() < 12 ){
                LOGGER.info("CHD");
                nameRef = "CHD";
            }else if(periodo.getYears() < 2){
                LOGGER.info("INF");
                nameRef = "INF";
            }

        }else{

            for (PassengerPNRB passenger : passengerDetails) {
                if (passenger != null && nameAssocID.equalsIgnoreCase(passenger.getNameId())) {
                    nameRef = passenger.getReferenceNumber();
                    break;
                }
            }

        }

        if (null == nameRef) {
            nameRef = "ADT";
        }

        LOGGER.info("getPassengerType: " + nameRef);

        return PaxType.getType(nameRef);
    }

    public static String[] getCorporateNumbers(GetReservationRS getReservationRS) {
        String[] result = new String[2];
        if (null != getReservationRS
                && null != getReservationRS.getReservation()
                && null != getReservationRS.getReservation().getGenericSpecialRequests()) {
            if (!getReservationRS.getReservation().getGenericSpecialRequests().isEmpty()) {
                for(GenericSpecialRequestPNRB ssr : getReservationRS.getReservation().getGenericSpecialRequests()){
                if("OSI".equalsIgnoreCase(ssr.getCode())){
                    String text = ssr.getFullText();
                    if(text.contains("FQTC")){
                        String textArray[] = ssr.getFullText().split(" ");
                        for (String textX : textArray) {
                            if (textX.length() >= 9) {
                                text = textX.replaceAll("[^0-9]", "");
                                if (text.length() == 9) {
                                    result[0] = text;
                                }
                            } else if (textX.length() <= 2) {
                                text = textX.replaceAll("[AZ]", "");
                                if (text.length() == 2) {
                                    result[1] = text;
                                }

                            }
                        }
                    }
                }

                }
            }
        }
        return result;
    }


    /**
     * @param bookedSegmentList
     * @param segment
     * @return
     */
    public static boolean isForThisLeg(
            List<BookedSegment> bookedSegmentList,
            SegmentOrTravelPortionType segment
    ) {

        if (null == bookedSegmentList || bookedSegmentList.isEmpty() || null == segment) {
            return false;
        }

        for (BookedSegment bookedSegment : bookedSegmentList) {
            try {
                Segment segmentLocal = bookedSegment.getSegment();

                if (segmentLocal.getOperatingCarrier().equalsIgnoreCase(segment.getAirlineCode())
                        && segmentLocal.getDepartureAirport().equalsIgnoreCase(segment.getBoardPoint())
                        && segmentLocal.getArrivalAirport().equalsIgnoreCase(segment.getOffPoint())) {
                    return true;
                }
            } catch (Exception ex) {
                //
            }

        }

        return false;
    }

    /**
     * @param bookedSegmentList
     * @param segment
     * @return
     * SegmentOrTravelPortionType segment
     */
    public static boolean isForThisLegCarryOn(
            List<BookedSegment> bookedSegmentList,
            AncillaryServicesPNRB ancillaryServices
    ) {

        if (null == ancillaryServices) {
            return false;
        }

        if(ancillaryServices.getRficSubcode().equalsIgnoreCase("0CZ") || ancillaryServices.getRficSubcode().equalsIgnoreCase("0MJ") ){

            if (null == ancillaryServices.getTravelPortions()) {
                return true;
            }
    
            AncillaryServicesPNRB.TravelPortions travelerPointsServiceList = ancillaryServices.getTravelPortions();
            List<SegmentOrTravelPortionType> pointsList = travelerPointsServiceList.getTravelPortion();
            for (BookedSegment bookedSegment : bookedSegmentList) {
                try {
                    Segment segmentLocal = bookedSegment.getSegment();
                    for(SegmentOrTravelPortionType trp: pointsList){
                        if (segmentLocal.getOperatingCarrier().equalsIgnoreCase(trp.getAirlineCode())
                                && segmentLocal.getDepartureAirport().equalsIgnoreCase(trp.getBoardPoint())
                                && segmentLocal.getArrivalAirport().equalsIgnoreCase(trp.getOffPoint())) {
                            return true;
                        }
                    }
                } catch (Exception ex) {
                    //
                }
    
            }

        }else{
            return false;
        }

        return false;
    }



    /**
     *
     * @param ancillaryList
     * @param bookedLeg
     * @return
     */
    public static List<com.sabre.webservices.pnrbuilder.getreservation.AncillaryServicesPNRB> getAncillariesForLeg(
            List<com.sabre.webservices.pnrbuilder.getreservation.AncillaryServicesPNRB> ancillaryList,
            BookedLeg bookedLeg
    ) {
        List<com.sabre.webservices.pnrbuilder.getreservation.AncillaryServicesPNRB> ancillariesListForThisPassenger = new ArrayList<>();
        for (com.sabre.webservices.pnrbuilder.getreservation.AncillaryServicesPNRB ancillary : ancillaryList) {
            if (ancillary != null
                    && ("1".equalsIgnoreCase(ancillary.getEMDType())
                    || null == ancillary.getSegment()
                    || PNRLookUpServiceUtil.getLegCode(ancillary.getSegment()).equalsIgnoreCase(bookedLeg.getSegments().getLegCode())
                    || PNRLookUpServiceUtil.isForThisLeg(bookedLeg.getSegments().getCollection(), ancillary.getSegment()))) {
                ancillariesListForThisPassenger.add(ancillary);
            }
        }
        return ancillariesListForThisPassenger;
    }

    public static void autoCheckinValidation(PNR pnr, GetReservationRS getReservationRS) {
        try {
            RemarksPNRB remarkInfo = getReservationRS.getReservation().getRemarks();

            List<RemarkPNRB> aciRemarks = remarkInfo.getRemark().stream()
                    .filter(r -> r.getRemarkLines().getRemarkLine().get(0).getText().contains("ACI"))
                    .filter(r -> r.getRemarkLines().getRemarkLine().get(0).getText().contains("SUC"))
                    .collect(Collectors.toList());
            RemarkPNRB latestACIRemark = aciRemarks.stream()
                    .max(Comparator.comparingInt(a -> Integer.parseInt(a.getId())))
                    .orElseThrow(NoSuchElementException::new);
            LOGGER.info("LatestACIRemark: {}",  new Gson().toJson(latestACIRemark));
            pnr.setAutoCheckin(latestACIRemark.getRemarkLines().getRemarkLine().get(0).getText());
        } catch (Exception ex) {
            pnr.setAutoCheckin("false");
        }


    }

    public static List<OpenReservationElementType> getSSRInformation(GetReservationRS getReservationRS) {
        List<OpenReservationElementType> specialServiceInfo = new ArrayList<>();

        ReservationPNRB reservation = getReservationRS.getReservation();
        if (reservation != null) {
            specialServiceInfo = reservation.getOpenReservationElements().getOpenReservationElement();
        }
        return specialServiceInfo;
    }

    public static boolean isSsrNonEligibleForCheckIn(ServiceRequestType service) {
        if (service != null) {
            for (String ssrCode : PNRLookupSSRCodes.getListOfSSRPNRLookup()) {
                if ("SSR".equalsIgnoreCase(service.getServiceType())
                        && ssrCode.equalsIgnoreCase(service.getCode())) {
                    return true;
                }
            }
        }

        return false;
    }

    public static String getSsrNonEligibleForCheckIn(ServiceRequestType service) {
        if (service != null) {
            for (String ssrCode : PNRLookupSSRCodes.getListOfSSRPNRLookup()) {
                if ("SSR".equalsIgnoreCase(service.getServiceType())
                        && ssrCode.equalsIgnoreCase(service.getCode())) {
                    return ssrCode;
                }
            }
        }

        return "";
    }

    /**
     * @param bookedLeg
     * @param bookedTraveler If the passenger has an Infant on an international flight add an extra bag
     */
    public static void addExtraBaggageForInfant(BookedLeg bookedLeg, BookedTraveler bookedTraveler) {
        try {
            if (null != bookedTraveler.getInfant()) {
                if (!bookedLeg.isDomestic()) {
                    bookedTraveler.getFreeBaggageAllowance().setChecked(
                            bookedTraveler.getFreeBaggageAllowance().getChecked() + 1
                    );

                    if (null != bookedTraveler.getFreeBaggageAllowancePerLegsArray()) {
                        for (FreeBaggageAllowancePerLeg freeBaggageAllowancePerLeg : bookedTraveler.getFreeBaggageAllowancePerLegsArray()) {
                            if (bookedLeg.getSegments().getLegCode().equalsIgnoreCase(freeBaggageAllowancePerLeg.getLegCode())) {
                                freeBaggageAllowancePerLeg.getFreeBaggageAllowance().setChecked(
                                        freeBaggageAllowancePerLeg.getFreeBaggageAllowance().getChecked() + 1
                                );
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    /***
     * This function will do the following:
     *  - First validated that the booked leg is an international flight
     *  - If so, add DOCS as a missing and required
     */
    public static void setRequiredTravelDocs(BookedTraveler bookedTraveler, BookedLeg bookedLeg) {

        // This passenger already have the missing DOCS added or passenger is already checked in (in this case is not needed)
        if (bookedTraveler.isCheckinStatus()
                || (bookedTraveler.getMissingCheckinRequiredFields().contains(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("DOCS"))
                && (null == bookedTraveler.getInfant()
                || bookedTraveler.getMissingCheckinRequiredFields().contains(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("DOCS/INF"))))) {

            return;
        }

        boolean isInternational = false;

        for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {
            if (!bookedSegment.getSegment().isDomestic()) {
                isInternational = true;
                break;
            }
        }

        if (isInternational) {
            if (!bookedTraveler.getMissingCheckinRequiredFields().contains(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("DOCS"))) {
                bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("DOCS"));
            }

            if (bookedTraveler.getInfant() != null) {
                if (!bookedTraveler.getMissingCheckinRequiredFields().contains(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("DOCS/INF"))) {
                    bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("DOCS/INF"));
                }
            }
        }
    }


    /**
     * @return the PNR_PATTERN
     */
    public static Pattern getPNR_PATTERN() {
        return PNR_PATTERN;
    }

    /**
     * @return the PNR_TN_PATTERN
     */
    public static Pattern getPNR_TN_PATTERN() {
        return PNR_TN_PATTERN;
    }

    /**
     * @return the E_TICKET_PATTERN
     */
    public static Pattern getE_TICKET_PATTERN() {
        return E_TICKET_PATTERN;
    }

    /**
     * @return the FF_PROGRAM_PATTERN
     */
    public static Pattern getFF_PROGRAM_PATTERN() {
        return FF_PROGRAM_PATTERN;
    }

    /**
     * @return the IATA_CODE_AIRPORT_PATTERN
     */
    public static Pattern getIATA_CODE_AIRPORT_PATTERN() {
        return IATA_CODE_AIRPORT_PATTERN;
    }

}
