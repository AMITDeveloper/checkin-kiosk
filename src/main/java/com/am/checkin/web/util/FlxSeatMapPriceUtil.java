package com.am.checkin.web.util;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.AbstractSeatMap;
import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.CurrencyValue;
import com.aeromexico.commons.model.FlxPriceWrapper;
import com.aeromexico.commons.model.SeatChoiceUpsell;
import com.aeromexico.commons.model.SeatDistribution;
import com.aeromexico.commons.model.SeatMap;
import com.aeromexico.commons.model.SeatmapCollection;
import com.aeromexico.commons.model.SeatmapRow;
import com.aeromexico.commons.model.SeatmapRowCollection;
import com.aeromexico.commons.model.SeatmapSeat;
import com.aeromexico.commons.model.SeatmapSection;
import com.aeromexico.commons.seatmaps.util.ConstantSeatMap;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.types.SeatCharacteristicsType;
import com.aeromexico.commons.web.types.SeatSectionCodeType;
import com.aeromexico.commons.web.types.SeatmapStatusType;
import com.aeromexico.commons.web.util.Constants;
import com.aeromexico.commons.web.util.SegmentCodeUtil;
import com.farelogix.flx.servicelistrq.Characteristic;
import com.farelogix.flx.servicelistrq.Columns;
import com.farelogix.flx.servicelistrq.Flight;
import com.farelogix.flx.servicelistrq.Row;
import com.farelogix.flx.servicelistrq.Rows;
import com.farelogix.flx.servicelistrq.SeatDetails;
import com.farelogix.flx.servicelistrq.SeatDisplay;
import com.farelogix.flx.servicelistrs.Seats;
import com.farelogix.flx.servicelistrs.Service;
import com.farelogix.flx.servicelistrs.ServiceListRS;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author adrianleal
 */
public class FlxSeatMapPriceUtil {

    private static final Logger LOG = LoggerFactory.getLogger(FlxSeatMapPriceUtil.class);

    private static final Set<String> SEAT_POSITIONS = new HashSet<>(Arrays.asList("Window", "Middle", "Aisle"));
    private static final Set<String> SEAT_FLX_POSITIONS = new HashSet<>(Arrays.asList("W", "M", "A")); // change M for C

    /**
     * @param seatmapCollection
     * @param pricePerSegmentMap
     */
    public static void updateSeatMapsWithFarelogixPrices(
            SeatmapCollection seatmapCollection,
            Map<String, Map<String, Map<String, FlxPriceWrapper>>> pricePerSegmentMap
    ) {
        if (null != seatmapCollection && !seatmapCollection.getCollection().isEmpty()
                && null != pricePerSegmentMap && !pricePerSegmentMap.isEmpty()) {

            for (AbstractSeatMap abstractSeatMap : seatmapCollection.getCollection()) {
                if (abstractSeatMap instanceof SeatMap) {
                    SeatMap seatMap = (SeatMap) abstractSeatMap;
                    //if (null != seatMap) { // si entra aqui es porque es una instancia de SeatMap por lo tanto no es null

                    String keySegmentCode = seatMap.getSegment().getSegmentCode();
                    Map<String, Map<String, FlxPriceWrapper>> mapSections = pricePerSegmentMap.get(keySegmentCode);

                    if (null != mapSections && !mapSections.isEmpty()) {
                        for (SeatmapSection seatmapSection : seatMap.getSections().getCollection()) {
                            if (!SeatSectionCodeType.FIRST_CLASS.equals(seatmapSection.getCode())
                                    && !SeatSectionCodeType.COACH.equals(seatmapSection.getCode())) {

                                Map<String, FlxPriceWrapper> mapSeats = mapSections.get(seatmapSection.getCode().toString());

                                if (null != mapSeats && !mapSeats.isEmpty()
                                        && null != seatmapSection.getRows()
                                        && null != seatmapSection.getRows().getCollection()) {

                                    for (SeatmapRow seatmapRow : seatmapSection.getRows().getCollection()) {
                                        if (null != seatmapRow.getSeats() && null != seatmapRow.getSeats().getCollection()) {
                                            for (SeatmapSeat seatmapSeat : seatmapRow.getSeats().getCollection()) {
                                                if (null != seatmapSeat.getChoice()
                                                        && seatmapSeat.getChoice() instanceof SeatChoiceUpsell) {
                                                    SeatChoiceUpsell seatChoiceUpsell = (SeatChoiceUpsell) seatmapSeat.getChoice();

                                                    String code = seatChoiceUpsell.getCode();
                                                    code = StringUtils.leftPad(code, 3, '0');

                                                    FlxPriceWrapper flxPriceWrapper = mapSeats.get(code);

                                                    if (null != flxPriceWrapper) {

                                                        CurrencyValue flxCurrencyValue = flxPriceWrapper.getCurrencyValue();

                                                        if (null != flxCurrencyValue) {

                                                            boolean isDifferent = isDifferentPrice(
                                                                    flxCurrencyValue,
                                                                    seatChoiceUpsell.getCurrency()
                                                            );

                                                            if (isDifferent) {
                                                                seatChoiceUpsell.setCurrency(flxCurrencyValue);
                                                                seatChoiceUpsell.setPriceCalcList(flxPriceWrapper.getPriceCalcList());
                                                                seatChoiceUpsell.setFarelogixPrice(true);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @param flxCurrencyValue
     * @param seatmapRowCollection
     * @param seatSectionCodeType
     * @return
     */
    private static boolean isFlxPriceDifferentThanAmPrice(
            CurrencyValue flxCurrencyValue,
            SeatmapRowCollection seatmapRowCollection,
            SeatSectionCodeType seatSectionCodeType
    ) {
        for (SeatmapRow seatmapRow : seatmapRowCollection.getCollection()) {
            for (SeatmapSeat seatmapSeat : seatmapRow.getSeats().getCollection()) {
                if (SeatmapStatusType.AVAILABLE.equals(seatmapSeat.getStatus())) { //const will be zero if unavailable
                    if (seatmapSeat.getChoice() instanceof SeatChoiceUpsell) {

                        SeatChoiceUpsell seatChoiceUpsell = (SeatChoiceUpsell) seatmapSeat.getChoice();
                        CurrencyValue currencyValue = seatChoiceUpsell.getCurrency();

                        boolean isDifferent = isDifferentPrice(flxCurrencyValue, currencyValue);

                        LOG.info("CURRENCY FLX: " + flxCurrencyValue.getTotal() + " " + flxCurrencyValue.getCurrencyCode() + " CURRENCY AM : " + currencyValue.getTotal() + " " + currencyValue.getCurrencyCode() + " " + seatSectionCodeType + " " + (isDifferent ? "DIFFERENT PRICE" : "SAME PRICE"));

                        return isDifferent;
                    } else {
                        //return true;
                    }
                }
            }
        }

        return true;
    }

    private static boolean isDifferentPrice(CurrencyValue flxCurrencyValue, CurrencyValue currencyValue) {
        boolean isDifferent = true;
        if (flxCurrencyValue.getTotal().compareTo(currencyValue.getTotal()) == 0) {
            isDifferent = false;
        }
        return isDifferent;
    }

    private static void replacePriceFLX(CurrencyValue flxCurrencyValue, SeatmapRowCollection seatmapRowCollection) {
        for (SeatmapRow seatmapRow : seatmapRowCollection.getCollection()) {
            for (SeatmapSeat seatmapSeat : seatmapRow.getSeats().getCollection()) {
                if (seatmapSeat.getChoice() instanceof SeatChoiceUpsell) {
                    SeatChoiceUpsell seatChoiceUpsell = (SeatChoiceUpsell) seatmapSeat.getChoice();
                    seatChoiceUpsell.setCurrency(flxCurrencyValue);
                    seatChoiceUpsell.setFarelogixPrice(true);
                }
            }
        }
    }

    /**
     * Get price per sections from Farelogix
     *
     * @param segmentReferences
     * @param serviceListRS
     * @param currencyCode
     * @param nameRef
     * @return
     * @throws java.lang.Exception
     */
    public static Map<String, Map<String, Map<String, FlxPriceWrapper>>> getFLXPricePerSections(
            Map<String, String> segmentReferences,
            ServiceListRS serviceListRS,
            String currencyCode,
            String nameRef
    ) throws Exception {

        Map<String, Map<String, Map<String, FlxPriceWrapper>>> pricePerSegmentCodeList = new HashMap<>();

        for (Map.Entry<String, String> entry : segmentReferences.entrySet()) {
            String segmentCode = entry.getKey();
            String segmentRef = entry.getValue();

            Map<String, Map<String, FlxPriceWrapper>> pricesPerSection = getSeatMapPriceSections(
                    serviceListRS,
                    segmentRef,
                    currencyCode,
                    nameRef
            );

            if (null != pricesPerSection && !pricesPerSection.isEmpty()) {
                pricePerSegmentCodeList.put(segmentCode, pricesPerSection);
            }

        }

        return pricePerSegmentCodeList;
    }

    private static Map<String, Map<String, FlxPriceWrapper>> getSeatMapPriceSections(
            ServiceListRS serviceListRS,
            String segmentRef,
            String currencyCode,
            String nameRef
    ) {
        //Get the IDRef of the sections of the flight

        Map<String, Map<String, FlxPriceWrapper>> prices = new HashMap<>();

        if (null != serviceListRS.getOptionalServices()
                && null != serviceListRS.getOptionalServices().getService()
                && !serviceListRS.getOptionalServices().getService().isEmpty()) {

            List<Service> services = FlxServiceListUtil.getServices(
                    serviceListRS.getOptionalServices(), segmentRef
            );

            if (null != services) {
                for (Service service : services) {

                    if (nameRef.equalsIgnoreCase(service.getTravelerIDRef().getValue())) {

                        if ("Surcharge".equalsIgnoreCase(service.getType())) {

                            if (!currencyCode.equalsIgnoreCase(serviceListRS.getOptionalServices().getCurrencyCode().getValue())) {
                                throw new GenericException(javax.ws.rs.core.Response.Status.BAD_REQUEST, ErrorType.CURRENCY_CODE_DOES_NOT_MATCH_PCC, "Currency code does not match pcc");
                            }

                            SeatSectionCodeType seatSectionCodeType = null;

                            if (service.getDescription() != null) {
                                if ("AM Plus".equalsIgnoreCase(service.getDescription())) {
                                    seatSectionCodeType = SeatSectionCodeType.AM_PLUS;
                                } else if ("Preferred Seat".equalsIgnoreCase(service.getDescription())) {
                                    seatSectionCodeType = SeatSectionCodeType.PREFERRED;
                                } else if ("Exit Row".equalsIgnoreCase(service.getDescription())) {
                                    seatSectionCodeType = SeatSectionCodeType.EXIT_ROW;
                                } else if ("Seat Choice".equalsIgnoreCase(service.getDescription())) {
                                    seatSectionCodeType = SeatSectionCodeType.COACH;
                                }
                            }

                            if (null != seatSectionCodeType) {
                                Map<String, FlxPriceWrapper> seatPrices;

                                if (prices.containsKey(seatSectionCodeType.toString())) {
                                    seatPrices = prices.get(seatSectionCodeType.toString());
                                } else {
                                    seatPrices = new HashMap<>();
                                    prices.put(seatSectionCodeType.toString(), seatPrices);
                                }

                                FlxPriceWrapper flxPriceWrapper = FlxServiceListUtil.getCurrencyValue(
                                        serviceListRS.getOptionalServices().getCurrencyCode(),
                                        service.getServicePrice(),
                                        service.getServicePriceCalc()
                                );

                                if (null != service.getSeats() && null != service.getSeats().getSeat()) {
                                    for (Seats.Seat seat : service.getSeats().getSeat()) {
                                        String seatCode = seat.getRow().toString() + seat.getColumn();

                                        seatCode = StringUtils.leftPad(seatCode, 3, '0');

                                        if (!seatPrices.containsKey(seatCode)) {
                                            seatPrices.put(seatCode, flxPriceWrapper);
                                        }
                                    }
                                }
                            }

                        } else if ("Included".equalsIgnoreCase(service.getType())) {

                            if (!currencyCode.equalsIgnoreCase(serviceListRS.getOptionalServices().getCurrencyCode().getValue())) {
                                throw new GenericException(javax.ws.rs.core.Response.Status.BAD_REQUEST, ErrorType.CURRENCY_CODE_DOES_NOT_MATCH_PCC, "Currency code does not match pcc");
                            }

                            SeatSectionCodeType seatSectionCodeType = null;

                            if (service.getDescription() != null) {
                                if ("AM Plus".equalsIgnoreCase(service.getDescription())) {
                                    seatSectionCodeType = SeatSectionCodeType.AM_PLUS;
                                } else if ("Preferred Seat".equalsIgnoreCase(service.getDescription())) {
                                    seatSectionCodeType = SeatSectionCodeType.PREFERRED;
                                } else if ("Exit Row".equalsIgnoreCase(service.getDescription())) {
                                    seatSectionCodeType = SeatSectionCodeType.EXIT_ROW;
                                } else if ("Seat Choice".equalsIgnoreCase(service.getDescription())) {
                                    seatSectionCodeType = SeatSectionCodeType.COACH;
                                }
                            }

                            if (null != seatSectionCodeType) {
                                Map<String, FlxPriceWrapper> seatPrices;

                                if (prices.containsKey(seatSectionCodeType.toString())) {
                                    seatPrices = prices.get(seatSectionCodeType.toString());
                                } else {
                                    seatPrices = new HashMap<>();
                                    prices.put(seatSectionCodeType.toString(), seatPrices);
                                }

                                FlxPriceWrapper flxPriceWrapper = FlxServiceListUtil.getCurrencyValue(
                                        serviceListRS.getOptionalServices().getCurrencyCode(),
                                        service.getServicePrice(),
                                        service.getServicePriceCalc()
                                );

                                if (null != service.getSeats() && null != service.getSeats().getSeat()) {
                                    for (Seats.Seat seat : service.getSeats().getSeat()) {
                                        String seatCode = seat.getRow().toString() + seat.getColumn();

                                        seatCode = StringUtils.leftPad(seatCode, 3, '0');

                                        if (!seatPrices.containsKey(seatCode)) {
                                            seatPrices.put(seatCode, flxPriceWrapper);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return prices;
    }

    public static SeatMap getSeatMapSection(SeatmapCollection seatmapCollection, String segmentCode) {
        for (AbstractSeatMap abstractSeatMap : seatmapCollection.getCollection()) {
            if (abstractSeatMap instanceof SeatMap) {
                SeatMap seatMap = (SeatMap) abstractSeatMap;

                if (SegmentCodeUtil.compareSegmentCodeWithoutTime(segmentCode, seatMap.getSegment().getSegmentCode())) {
                    return seatMap;
                }
            }
        }

        return null;
    }

    public static List<BookedSegment> getSegmentsWithSeatmap(SeatmapCollection seatmapCollection, BookedLeg bookedLeg) {
        List<BookedSegment> openBookedSegments = new ArrayList<>();
        for (AbstractSeatMap abstractSeatMap : seatmapCollection.getCollection()) {
            if (abstractSeatMap instanceof SeatMap) {
                SeatMap seatMap = (SeatMap) abstractSeatMap;

                if (SeatmapStatusType.AVAILABLE.equals(seatMap.getStatus())
                        && null != seatMap.getSections()
                        && null != seatMap.getSections().getCollection()) {

                    String segmentCode = seatMap.getSegment().getSegmentCode();

                    BookedSegment bookedSegment = bookedLeg.getBookedSegment(segmentCode);

                    if (null != bookedSegment
                            && "AM".equalsIgnoreCase(bookedSegment.getSegment().getOperatingCarrier())
                            && ("Y".equalsIgnoreCase(bookedSegment.getSegment().getCabin())
                            || "main".equalsIgnoreCase(bookedSegment.getSegment().getCabin()))) {
                        openBookedSegments.add(bookedSegment);
                    }
                }
            }
        }
        return openBookedSegments;
    }

    public static boolean isFirstClassCabin(String bookingClass) {
        if (bookingClass == null) {
            bookingClass = ConstantSeatMap.COUCH_CABIN_CLASS;
        }

        String[] bookingClassPremier = Constants.bookingClassPremier;
        for (String aBookingClassPremier : bookingClassPremier) {
            if (bookingClass.equalsIgnoreCase(aBookingClassPremier)) {
                return true;
            }
        }
        return false;
    }

    public static Flight.SeatMap transformSeatMap(SeatMap seatMap) {
        Flight.SeatMap flightSeatMap = new Flight.SeatMap();

        for (SeatmapSection seatmapSection : seatMap.getSections().getCollection()) {
            seatmapSection.getRows();
        }

        if (null != seatMap.getSeatDistributionCollection()
                && null != seatMap.getSeatDistributionCollection().getSeatDistributions()) {

            List<SeatDisplay> seatDisplayList = flightSeatMap.getSeatDisplay();

            for (SeatDistribution seatDistribution : seatMap.getSeatDistributionCollection().getSeatDistributions()) {

                    String cabinCode = isFirstClassCabin(seatDistribution.getCabinClass())?"J":"Y";

                    SeatDisplay seatDisplay = new SeatDisplay();
                    seatDisplay.setCabinClass(cabinCode);
                    Rows flightRows = new Rows();

                    List<Columns> flightColumns = seatDisplay.getColumns();

                    for (SeatDistribution.Column column : seatDistribution.getColumns()) {
                        Columns flightColumn = new Columns();
                        flightColumn.setValue(column.getCode());

                        if (null != column.getSeatCharacteristics() && !column.getSeatCharacteristics().isEmpty()) {
                            for (String seatCharacteristic : column.getSeatCharacteristics()) {
                                if (SEAT_POSITIONS.contains(seatCharacteristic.trim())) {
                                    String position;

                                    switch (seatCharacteristic.trim()) {
                                        case "Window":
                                            position = "W";
                                            break;
                                        case "Aisle":
                                            position = "A";
                                            break;
                                        case "Middle":
                                            position = "C";
                                            break;
                                        default:
                                            position = "C";
                                            break;
                                    }

                                    flightColumn.setPosition(position);
                                    break;
                                } else {
                                    flightColumn.setPosition("C");
                                }
                            }
                        } else {
                            flightColumn.setPosition("C");
                        }

                        flightColumns.add(flightColumn);
                    }

                    flightRows.setFirst(Integer.valueOf(seatDistribution.getRow().getFirstRow()));
                    flightRows.setLast(Integer.valueOf(seatDistribution.getRow().getLastRow()));

                    seatDisplay.setRows(flightRows);

                    seatDisplayList.add(seatDisplay);
            }
        }

        if (null != seatMap.getSections() && null != seatMap.getSections().getCollection()) {
            List<SeatDetails> firstClassSeatDetailsList = flightSeatMap.getSeatDetails();
            List<SeatDetails> touristClassSeatDetailsList = flightSeatMap.getSeatDetails();

            for (SeatmapSection seatmapSection : seatMap.getSections().getCollection()) {
                if (SeatSectionCodeType.FIRST_CLASS.equals(seatmapSection.getCode())) {
                    if (null != seatmapSection.getRows().getCollection()) {
                        for (SeatmapRow seatmapRow : seatmapSection.getRows().getCollection()) {

                            for (SeatmapSeat seatmapSeat : seatmapRow.getSeats().getCollection()) {
                                SeatDetails seatDetails = new SeatDetails();
                                seatDetails.setColumn(seatmapSeat.getColumn());

                                Row row = new Row();
                                row.setNumber(seatmapSeat.getRow());
                                row.setStatus(SeatmapStatusType.AVAILABLE.equals(seatmapSeat.getStatus()) ? "A" : "U");
                                List<Characteristic> characteristics = row.getCharacteristic();

                                List<SeatCharacteristicsType> seatCharacteristics = seatmapSeat.getChoice().getSeatCharacteristics();

                                for (SeatCharacteristicsType seatCharacteristic : seatCharacteristics) {
                                    if (SEAT_FLX_POSITIONS.contains(seatCharacteristic.name())) {
                                        String position;

                                        switch (seatCharacteristic.name()) {
                                            case "A":
                                                position = "A";
                                                break;
                                            case "W":
                                                position = "W";
                                                break;
                                            case "M":
                                                position = "C";
                                                break;
                                            default:
                                                position = "C";
                                                break;
                                        }

                                        Characteristic characteristic = new Characteristic();
                                        characteristic.setValue(position);
                                        //characteristic.setDescription("");
                                        characteristics.add(characteristic);
                                    } else {
                                        Characteristic characteristic = new Characteristic();
                                        characteristic.setValue(seatCharacteristic.name());
                                        //characteristic.setDescription("");
                                        characteristics.add(characteristic);
                                    }
                                }

                                List<String> seatFacilities = seatmapSeat.getChoice().getSeatFacilities();

                                for (String facility : seatFacilities) {
                                    Characteristic characteristic = new Characteristic();
                                    characteristic.setValue(facility);
                                    //characteristic.setDescription("");
                                    characteristics.add(characteristic);
                                }

                                seatDetails.setRow(row);

                                firstClassSeatDetailsList.add(seatDetails);
                            }
                        }
                    }
                } else {
                    if (null != seatmapSection.getRows().getCollection()) {
                        for (SeatmapRow seatmapRow : seatmapSection.getRows().getCollection()) {

                            for (SeatmapSeat seatmapSeat : seatmapRow.getSeats().getCollection()) {
                                SeatDetails seatDetails = new SeatDetails();
                                seatDetails.setColumn(seatmapSeat.getColumn());

                                Row row = new Row();
                                row.setNumber(seatmapSeat.getRow());
                                row.setStatus(SeatmapStatusType.AVAILABLE.equals(seatmapSeat.getStatus()) ? "A" : "U");
                                List<Characteristic> characteristics = row.getCharacteristic();

                                List<SeatCharacteristicsType> seatCharacteristics = seatmapSeat.getChoice().getSeatCharacteristics();

                                for (SeatCharacteristicsType seatCharacteristic : seatCharacteristics) {
                                    if (SEAT_FLX_POSITIONS.contains(seatCharacteristic.name())) {
                                        String position;

                                        switch (seatCharacteristic.name()) {
                                            case "A":
                                                position = "A";
                                                break;
                                            case "W":
                                                position = "W";
                                                break;
                                            case "M":
                                                position = "C";
                                                break;
                                            default:
                                                position = "C";
                                                break;
                                        }

                                        Characteristic characteristic = new Characteristic();
                                        characteristic.setValue(position);
                                        //characteristic.setDescription("");
                                        characteristics.add(characteristic);
                                    } else {
                                        Characteristic characteristic = new Characteristic();
                                        characteristic.setValue(seatCharacteristic.name());
                                        //characteristic.setDescription("");
                                        characteristics.add(characteristic);
                                    }
                                }

                                List<String> seatFacilities = seatmapSeat.getChoice().getSeatFacilities();

                                for (String facility : seatFacilities) {
                                    Characteristic characteristic = new Characteristic();
                                    characteristic.setValue(facility);
                                    //characteristic.setDescription("");

                                    characteristics.add(characteristic);
                                }

                                seatDetails.setRow(row);

                                touristClassSeatDetailsList.add(seatDetails);
                            }
                        }
                    }
                }
            }
        }

        return flightSeatMap;
    }
}
