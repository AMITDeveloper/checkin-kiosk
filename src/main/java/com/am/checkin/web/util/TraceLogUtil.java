package com.am.checkin.web.util;

import com.aeromexico.commons.model.AbstractAncillary;
import com.aeromexico.commons.model.Ancillaries;
import com.aeromexico.commons.model.Ancillary;
import com.aeromexico.commons.model.CheckinConfirmation;
import com.aeromexico.commons.model.CreditCardPaymentSummary;
import com.aeromexico.commons.model.EMDVoucherPaymentSummary;
import com.aeromexico.commons.model.PNRCollection;
import com.aeromexico.commons.model.PaymentSummary;
import com.aeromexico.commons.model.PaypalPaymentSummary;
import com.aeromexico.commons.model.TicketNumber;
import com.aeromexico.commons.model.TicketedTravelerAncillary;
import com.aeromexico.commons.model.TicketedTravelerAncillaryCollection;
import com.aeromexico.commons.model.rq.BoardingPassesRQ;
import com.aeromexico.commons.model.rq.PnrRQ;
import com.aeromexico.commons.model.rq.PurchaseOrderRQ;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.aeromexico.commons.model.TraceLogDTO;
import com.aeromexico.commons.model.UatpPaymentSummary;

import javax.ws.rs.core.GenericEntity;
import java.util.Set;

public class TraceLogUtil {

	/**
     * Metodo general para realizar guardado de logs
     * de endpoints de chekin
     *
     * @param origen objeto con origen de informacion
     * @return boolean
     */
    public static TraceLogDTO getTraceLog(Object origen, GenericEntity<PNRCollection> genericEntity, String nameEndPoint){
    	TraceLogDTO result = null;
        if(origen instanceof PnrRQ){
            result = loggerPnRQ( origen, genericEntity, nameEndPoint);
        }
        return result;
    }
    
    /**
     * Metodo general para realizar guardado de logs
     * de endpoints de purchase order
     *
     * @param origen objeto con origen de informacion
     * @return boolean
     */
    public static TraceLogDTO getTraceLog(PurchaseOrderRQ origen, GenericEntity<CheckinConfirmation> genericEntity, String nameEndPoint){
    	Date date = new Date();
    	DateFormat hourdateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	TraceLogDTO traceLogDto = new TraceLogDTO();
    	traceLogDto.setStatusPurchaseOrder("SUCCESSFUL");
        traceLogDto.setComponent("PurchaseOrderController");
    	traceLogDto.setEndPointName(nameEndPoint);
        traceLogDto.setMsgError("NA");
        traceLogDto.setCodeError("NA");
        traceLogDto.setBookingClass("NA");
        traceLogDto.setDestination("NA");
        traceLogDto.setTotalPassenger("0");
        traceLogDto.setNumberFlight("NA");
        traceLogDto.setOrigin("NA");
        traceLogDto.setFormOfPayment("NA");
        traceLogDto.setDate("" + hourdateFormat.format(date).replaceAll("\\s","_"));
        traceLogDto.setPos(origen.getPos());
        traceLogDto.setPnr(origen.getRecordLocator());
        traceLogDto.setStore(origen.getStore());
        traceLogDto.setCurrency("NA");
        if (genericEntity != null) {
        	Ancillaries ancillaries = new Ancillaries();
        	traceLogDto.setBookingClass(genericEntity.getEntity().getCheckedInLegs().getCollection().get(0).getSegments().getCollection().get(0).getSegment().getBookingClass());
        	traceLogDto.setOrigin(genericEntity.getEntity().getCheckedInLegs().getCollection().get(0).getSegments().getCollection().get(0).getSegment().getDepartureAirport());
        	traceLogDto.setDestination(genericEntity.getEntity().getCheckedInLegs().getCollection().get(0).getLatestSegmentInLeg().getSegment().getArrivalAirport());
        	traceLogDto.setRoute("");
        	genericEntity.getEntity().getCheckedInLegs().getCollection().get(0).getSegments().getCollection().forEach( segment -> {
        		traceLogDto.setRoute(traceLogDto.getRoute() + segment.getSegment().getDepartureAirport() + "_" + segment.getSegment().getArrivalAirport() + ".");
        	});
        	if (traceLogDto.getRoute().length() > 0) {
        		traceLogDto.setRoute(traceLogDto.getRoute().substring(0, traceLogDto.getRoute().length() - 1));
        	}
        	String numberFlight = genericEntity.getEntity().getCheckedInLegs().getCollection().get(0).getSegments().getCollection().get(0).getSegment().getMarketingFlightCode();
        	try {
        		numberFlight = "" + Integer.parseInt(numberFlight);
        	} catch (NumberFormatException ex) {
        		
        	}
        	traceLogDto.setNumberFlight(numberFlight);
        	if (genericEntity.getEntity().getPaymentSummary() != null && genericEntity.getEntity().getPaymentSummary().getCollection() != null
        			&& genericEntity.getEntity().getPaymentSummary().getCollection().size() > 0) {
	        	PaymentSummary PaymentSummary = genericEntity.getEntity().getPaymentSummary().getCollection().get(0);
	        	if (PaymentSummary instanceof CreditCardPaymentSummary) {
	        		CreditCardPaymentSummary creditCardPaymentSummary = (CreditCardPaymentSummary) PaymentSummary;
	        		traceLogDto.setFormOfPayment(creditCardPaymentSummary.getCardType().getCardType());
	        		traceLogDto.setCurrency(creditCardPaymentSummary.getPaymentAmount().getCurrencyCode());
	        	}
	        	if (PaymentSummary instanceof UatpPaymentSummary) {
	        		traceLogDto.setFormOfPayment("UATP");
	        	}
	        	if (PaymentSummary instanceof EMDVoucherPaymentSummary) {
	        		traceLogDto.setFormOfPayment("EMDVoucher");
	        	}
	        	if (PaymentSummary instanceof PaypalPaymentSummary) {
	        		traceLogDto.setFormOfPayment("PayPal");
	        	}
        	}
        	genericEntity.getEntity().getCheckedInCarts().getCollection().get(0).getTravelerInfo().getCollection().forEach(
        		o -> {
        			if (o.getCheckinStatusBySegment().getCollection().get(0).isCheckinStatus()) {
        				traceLogDto.setTotalPassenger("" + (Integer.parseInt(traceLogDto.getTotalPassenger()) + 1));
        			}
        			List<AbstractAncillary> ancillary = o.getAncillaries().getCollection();
        			if (ancillary.size() > 0 && ancillary.get(0) instanceof  TicketedTravelerAncillary) {
        				ancillary.forEach( aa -> {
        					TicketedTravelerAncillary a = (TicketedTravelerAncillary) aa;
        					Ancillary auxAncillaries = (Ancillary) a.getAncillary();
        					switch (auxAncillaries.getType()) {
	                    		case "0CC": ancillaries.set_0CC(ancillaries.get_0CC().add(auxAncillaries.getCurrency().getTotal())); break;
	                    		case "0CD": ancillaries.set_0CD(ancillaries.get_0CD().add(auxAncillaries.getCurrency().getTotal())); break;
	                    		case "0CE": ancillaries.set_0CE(ancillaries.get_0CE().add(auxAncillaries.getCurrency().getTotal())); break;
	                    		case "0CF": ancillaries.set_0CF(ancillaries.get_0CF().add(auxAncillaries.getCurrency().getTotal())); break;
	                    		case "0CG": ancillaries.set_0CG(ancillaries.get_0CG().add(auxAncillaries.getCurrency().getTotal())); break;
	                    		case "0CH": ancillaries.set_0CH(ancillaries.get_0CH().add(auxAncillaries.getCurrency().getTotal())); break;
	                    		case "0CI": ancillaries.set_0CI(ancillaries.get_0CI().add(auxAncillaries.getCurrency().getTotal())); break;
	                    		case "0CJ": ancillaries.set_0CJ(ancillaries.get_0CJ().add(auxAncillaries.getCurrency().getTotal())); break;
	                    		case "0CK": ancillaries.set_0CK(ancillaries.get_0CK().add(auxAncillaries.getCurrency().getTotal())); break;
	                    		case "0EN": ancillaries.set_0EN(ancillaries.get_0EN().add(auxAncillaries.getCurrency().getTotal())); break;
	                    		case "0D6": ancillaries.set_0D6(ancillaries.get_0D6().add(auxAncillaries.getCurrency().getTotal())); break;
	                    		case "0DC": ancillaries.set_0DC(ancillaries.get_0DC().add(auxAncillaries.getCurrency().getTotal())); break;
	                    		case "0DD": ancillaries.set_0DD(ancillaries.get_0DD().add(auxAncillaries.getCurrency().getTotal())); break;
	                    		case "0EE": ancillaries.set_0EE(ancillaries.get_0EE().add(auxAncillaries.getCurrency().getTotal())); break;
	                    		case "0FR": ancillaries.set_0FR(ancillaries.get_0FR().add(auxAncillaries.getCurrency().getTotal())); break;
	                    		case "0KO": ancillaries.set_0KO(ancillaries.get_0KO().add(auxAncillaries.getCurrency().getTotal())); break;
	                    		case "0KG": ancillaries.set_0KG(ancillaries.get_0KG().add(auxAncillaries.getCurrency().getTotal())); break;
	                    		case "0NS": ancillaries.set_0NS(ancillaries.get_0NS().add(auxAncillaries.getCurrency().getTotal())); break;
	                    		case "0N8": ancillaries.set_0N8(ancillaries.get_0N8().add(auxAncillaries.getCurrency().getTotal())); break;
	                    		case "08K": ancillaries.set_08K(ancillaries.get_08K().add(auxAncillaries.getCurrency().getTotal())); break;
	                    		case "0MJ": ancillaries.set_0MJ(ancillaries.get_0MJ().add(auxAncillaries.getCurrency().getTotal())); break;
	                    		case "AMS": ancillaries.setAMS(ancillaries.getAMS().add(auxAncillaries.getCurrency().getTotal())); break;
	                    		case "AMK": ancillaries.setAMK(ancillaries.getAMK().add(auxAncillaries.getCurrency().getTotal())); break;
	                    		case "DPM": ancillaries.setDPM(ancillaries.getDPM().add(auxAncillaries.getCurrency().getTotal())); break;
	                    		case "DPI": ancillaries.setDPI(ancillaries.getDPI().add(auxAncillaries.getCurrency().getTotal())); break;
	                    		case "KEI": ancillaries.setKEI(ancillaries.getKEI().add(auxAncillaries.getCurrency().getTotal())); break;
	                    		case "KEN": ancillaries.setKEN(ancillaries.getKEN().add(auxAncillaries.getCurrency().getTotal())); break;
	                    		case "CO2": ancillaries.setCO2(ancillaries.getCO2().add(auxAncillaries.getCurrency().getTotal())); break;
	                    		case "029": ancillaries.set_029(ancillaries.get_029().add(auxAncillaries.getCurrency().getTotal())); break;
	                    		case "0IA": ancillaries.set_0IA(ancillaries.get_0IA().add(auxAncillaries.getCurrency().getTotal())); break;
	                    		case "UPG": ancillaries.setUPG(ancillaries.getUPG().add(auxAncillaries.getCurrency().getTotal())); break;
	                    	}
        				});
        			}
        		});
        	traceLogDto.setTotalPurchase(traceLogDto.getTotalPurchase().add(ancillaries.get_0CC()));
			traceLogDto.setTotalPurchase(traceLogDto.getTotalPurchase().add(ancillaries.get_0CD()));
			traceLogDto.setTotalPurchase(traceLogDto.getTotalPurchase().add(ancillaries.get_0CE()));
			traceLogDto.setTotalPurchase(traceLogDto.getTotalPurchase().add(ancillaries.get_0CF()));
			traceLogDto.setTotalPurchase(traceLogDto.getTotalPurchase().add(ancillaries.get_0CG()));
			traceLogDto.setTotalPurchase(traceLogDto.getTotalPurchase().add(ancillaries.get_0CH()));
			traceLogDto.setTotalPurchase(traceLogDto.getTotalPurchase().add(ancillaries.get_0CI()));
			traceLogDto.setTotalPurchase(traceLogDto.getTotalPurchase().add(ancillaries.get_0CJ()));
			traceLogDto.setTotalPurchase(traceLogDto.getTotalPurchase().add(ancillaries.get_0CK()));
			traceLogDto.setTotalPurchase(traceLogDto.getTotalPurchase().add(ancillaries.get_0EN()));
			traceLogDto.setTotalPurchase(traceLogDto.getTotalPurchase().add(ancillaries.get_0D6()));
			traceLogDto.setTotalPurchase(traceLogDto.getTotalPurchase().add(ancillaries.get_0DC()));
			traceLogDto.setTotalPurchase(traceLogDto.getTotalPurchase().add(ancillaries.get_0DD()));
			traceLogDto.setTotalPurchase(traceLogDto.getTotalPurchase().add(ancillaries.get_0EE()));
			traceLogDto.setTotalPurchase(traceLogDto.getTotalPurchase().add(ancillaries.get_0FR()));
			traceLogDto.setTotalPurchase(traceLogDto.getTotalPurchase().add(ancillaries.get_0KO()));
			traceLogDto.setTotalPurchase(traceLogDto.getTotalPurchase().add(ancillaries.get_0KG()));
			traceLogDto.setTotalPurchase(traceLogDto.getTotalPurchase().add(ancillaries.get_0NS()));
			traceLogDto.setTotalPurchase(traceLogDto.getTotalPurchase().add(ancillaries.get_0N8()));
			traceLogDto.setTotalPurchase(traceLogDto.getTotalPurchase().add(ancillaries.get_08K()));
			traceLogDto.setTotalPurchase(traceLogDto.getTotalPurchase().add(ancillaries.get_0MJ()));
			traceLogDto.setTotalPurchase(traceLogDto.getTotalPurchase().add(ancillaries.getAMS()));
			traceLogDto.setTotalPurchase(traceLogDto.getTotalPurchase().add(ancillaries.getAMK()));
			traceLogDto.setTotalPurchase(traceLogDto.getTotalPurchase().add(ancillaries.getDPM()));
			traceLogDto.setTotalPurchase(traceLogDto.getTotalPurchase().add(ancillaries.getDPI()));
			traceLogDto.setTotalPurchase(traceLogDto.getTotalPurchase().add(ancillaries.getKEI()));
			traceLogDto.setTotalPurchase(traceLogDto.getTotalPurchase().add(ancillaries.getKEN()));
			traceLogDto.setTotalPurchase(traceLogDto.getTotalPurchase().add(ancillaries.getCO2()));
			traceLogDto.setTotalPurchase(traceLogDto.getTotalPurchase().add(ancillaries.get_029()));
			traceLogDto.setTotalPurchase(traceLogDto.getTotalPurchase().add(ancillaries.get_0IA()));
        	traceLogDto.setTotalPurchase(traceLogDto.getTotalPurchase().add(ancillaries.getUPG()));
        	traceLogDto.setAncillaries(ancillaries);
        }
        return traceLogDto;
    }
    
    /**
     * Metodo general para realizar guardado de logs
     * de endpoints de boarding passes
     *
     * @param origen objeto con origen de informacion
     * @return boolean
     */
    public static TraceLogDTO getTraceLog(BoardingPassesRQ origen, String nameEndPoint){
    	Date date = new Date();
    	DateFormat hourdateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	TraceLogDTO traceLogDto = new TraceLogDTO();
    	traceLogDto.setEndPointName(nameEndPoint);
        traceLogDto.setMsgError("NA");
        traceLogDto.setBookingClass("NA");
        traceLogDto.setDestination("NA");
        traceLogDto.setTotalPassenger("NA");
        traceLogDto.setNumberFlight("NA");
        traceLogDto.setOrigin("NA");
        traceLogDto.setDate("" + hourdateFormat.format(date).replaceAll("\\s","_"));
        traceLogDto.setPos(origen.getPos());
        traceLogDto.setPnr(origen.getRecordLocator());
        traceLogDto.setStore(origen.getStore());
        traceLogDto.setComponent("NA");
        return traceLogDto;
    }

    /**
     * Metodo general para realizar el loggeo cuando
     * el la fuente de los datos proviene de una clase de
     * tipo PnrRQ.
     *
     * @param origen objeto con origen de informacion
     * @return TraceLogDTO
     */
    private static TraceLogDTO loggerPnRQ(Object origen,GenericEntity<PNRCollection> genericEntity, String nameEndPoint){
    	Date date = new Date();
    	DateFormat hourdateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        PnrRQ pnrRQ = (PnrRQ) origen;
        TraceLogDTO logTraceDTO = new TraceLogDTO();
        logTraceDTO.setCodeError("NA");
        logTraceDTO.setEndPointName(nameEndPoint);
        Set<TicketNumber> ticketNumber = genericEntity.getEntity().getCollection().get(0).getCarts().getCollection()
                .get(0).getTravelerInfo().getCollection().get(0).getTicketNumbers();
        logTraceDTO.setNumberFlight("");
        logTraceDTO.setDestination("");
        logTraceDTO.setOrigin("");
        ticketNumber.stream().forEach( o ->{
            logTraceDTO.setNumberFlight(logTraceDTO.getNumberFlight() + o.getFlightNumber() + ",");
            logTraceDTO.setDestination(o.getEndLocation());
            logTraceDTO.setOrigin(logTraceDTO.getOrigin() + o.getStartLocation() + "_" + o.getEndLocation() + ".");
        });
        if (logTraceDTO.getOrigin().length() > 0) {
        	logTraceDTO.setRoute(logTraceDTO.getOrigin().substring(0, logTraceDTO.getOrigin().length() - 1));
        }
        logTraceDTO.setOrigin("");
        ticketNumber.stream().forEach( o ->{
        	if (logTraceDTO.getOrigin() == "") {
        		logTraceDTO.setOrigin(o.getStartLocation());
        	}
        });
        if (logTraceDTO.getNumberFlight().length() > 0) {
        	logTraceDTO.setNumberFlight(logTraceDTO.getNumberFlight().substring(0, logTraceDTO.getNumberFlight().length() - 1));
        }
        logTraceDTO.setBookingClass(genericEntity.getEntity().getCollection().get(0).getLegs().getCollection().get(0).
                getSegments().getCollection().get(0).getSegment().getBookingClass());
        logTraceDTO.setTotalPassenger("NA");
        logTraceDTO.setPos(pnrRQ.getPos());
        logTraceDTO.setPnr(pnrRQ.getRecordLocator());
        logTraceDTO.setDate("" + hourdateFormat.format(date).replaceAll("\\s","_"));
        logTraceDTO.setStore(pnrRQ.getStore());
        logTraceDTO.setMsgError("NA");
        return logTraceDTO;
    }
	
}
