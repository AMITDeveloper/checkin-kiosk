package com.am.checkin.web.util;

import com.aeromexico.commons.model.AuthorizationResult;
import com.aeromexico.commons.model.CardReaderResult;
import com.aeromexico.commons.model.CardToken;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.aeromexico.commons.model.T3DSResult;
import org.opentravel.ota.payments.AuthorizationResultType;
import org.opentravel.ota.payments.TokensType;

/**
 * @author adrian
 */
public class AuthorizationResultUtil {

    /**
     * @param authorizationResultTypeList
     * @return
     */
    public static List<AuthorizationResult> getAuthorizationResult(List<AuthorizationResultType> authorizationResultTypeList) {

        List<AuthorizationResult> authorizationResultList = new ArrayList<>();

        for (AuthorizationResultType authorizationResultType : authorizationResultTypeList) {
            AuthorizationResult authorizationResult = new AuthorizationResult();

            if (null != authorizationResultType.getT3DSResult()) {
                T3DSResult t3dsResult = new T3DSResult();
                t3dsResult.setCavvResultCode(authorizationResultType.getT3DSResult().getCAVVResultCode());
                t3dsResult.setIssuerURL(authorizationResultType.getT3DSResult().getIssuerURL());
                t3dsResult.setMd(authorizationResultType.getT3DSResult().getMD());
                t3dsResult.setPaRequest(authorizationResultType.getT3DSResult().getPARequest());
                t3dsResult.setT3DSRemarks(authorizationResultType.getT3DSResult().getT3DSRemarks());
                authorizationResult.setT3dsResult(t3dsResult);
            }
            authorizationResult.setRedirectForm(authorizationResultType.getRedirectHTML());
            authorizationResult.setResponseCode(authorizationResultType.getResponseCode());
            authorizationResult.setDescription(authorizationResultType.getDescription());
            authorizationResult.setApprovalCode(authorizationResultType.getApprovalCode());
            authorizationResult.setSupplierId(authorizationResultType.getSupplierID());
            authorizationResult.setSupplierResponseCode(authorizationResultType.getSupplierResponseCode());
            authorizationResult.setMerchantAccountNumber(authorizationResultType.getMerchantAccountNumber());
            authorizationResult.setPaymentId(authorizationResultType.getSupplierTransID());

            if (null != authorizationResultType.getCSCResult() && null != authorizationResultType.getCSCResult().getCSCRemarks()) {
                authorizationResult.getRemarks().add(authorizationResultType.getCSCResult().getCSCRemarks().trim());
            }

            if (null != authorizationResultType.getAVSResult() && null != authorizationResultType.getAVSResult().getAVSRemarks()) {
                authorizationResult.getRemarks().add(authorizationResultType.getAVSResult().getAVSRemarks().trim());
            }

            if (null != authorizationResultType.getAuthRemarks1()) {
                authorizationResult.getRemarks().add(authorizationResultType.getAuthRemarks1().trim());
            }

            if (null != authorizationResultType.getAuthRemarks2()) {
                authorizationResult.getRemarks().add(authorizationResultType.getAuthRemarks2().trim());
            }

            BigDecimal approvedAmount = null;
            try {
                String parts[] = authorizationResultType.getAuthRemarks2().trim().split("/");
                if (parts[0].contains("AUTH-APV")) {
                    approvedAmount = new BigDecimal(parts[parts.length - 1].replaceAll("[^\\d.]", ""));
                }
            } catch (Exception ex) {
                //
            }

            if (null != authorizationResultType.getCardReaderResult()) {
                CardReaderResult cardReaderResult = new CardReaderResult();
                cardReaderResult.setResultCode(authorizationResultType.getCardReaderResult().getResultCode());
                cardReaderResult.setRemarks(authorizationResultType.getCardReaderResult().getRemarks());

                if (null != authorizationResultType.getCardReaderResult().getTokens() && null != authorizationResultType.getCardReaderResult().getTokens().getToken()) {
                    for (TokensType.Token token : authorizationResultType.getCardReaderResult().getTokens().getToken()) {
                        CardToken tokenLocal = new CardToken();
                        tokenLocal.setName(token.getName());
                        tokenLocal.setValue(token.getValue());
                        cardReaderResult.getTokens().add(tokenLocal);
                    }
                }

                authorizationResult.setCardReaderResult(cardReaderResult);
            }

            if (null == authorizationResultType.getApprovedAmount()) {
                authorizationResult.setApprovedAmount(approvedAmount);
            } else {
                authorizationResult.setApprovedAmount(authorizationResultType.getApprovedAmount());
            }

            authorizationResultList.add(authorizationResult);
        }

        return authorizationResultList;

    }
}
