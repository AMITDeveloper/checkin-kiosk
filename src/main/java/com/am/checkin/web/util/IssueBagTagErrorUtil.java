package com.am.checkin.web.util;

import javax.ws.rs.core.Response;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.IssueBagRP;
import com.aeromexico.commons.model.IssuedBagTagList;
import com.aeromexico.commons.web.types.ErrorType;
import com.sabre.services.checkin.issuebagtag.v4.IssueBagTagRSACS;
import com.sabre.services.stl.v3.BagTagInfoListACS;
import com.sabre.services.stl.v3.ResultRecord;
import com.sabre.services.stl.v3.SystemSpecificResults;


/**
 * 
 * @author Waleed Saleh
 *
 */

public class IssueBagTagErrorUtil {

	private static final Logger log = LoggerFactory.getLogger(ShoppingCartErrorUtil.class);

	public static void IssueBagTagSabreError(IssueBagTagRSACS issueBagTags) {

		String msg = issueBagTags.getResult().getSystemSpecificResults().get(0).getErrorMessage().getValue();
		
		
		if (StringUtils.contains(msg, ErrorType.INVALID_DATE.getSearchableCode())) {
			log.error(ErrorType.INVALID_DATE.getFullDescription()
					+ getSabreError(issueBagTags.getResult()));
			throw new GenericException(Response.Status.METHOD_NOT_ALLOWED,ErrorType.INVALID_DATE,
					ErrorType.INVALID_DATE.getFullDescription(), msg);
		} else {
			log.error(ErrorType.UNKNOWN_CODE.getFullDescription()
					+ getSabreError(issueBagTags.getResult()));
			throw new GenericException(Response.Status.BAD_REQUEST,ErrorType.UNKNOWN_CODE,msg);
		}
		

	}

	/**
	 *
	 * @param resultRecord
	 * @return
	 */
	public static String getSabreError(ResultRecord resultRecord) {
		try {
			return resultRecord.getSystemSpecificResults().get(0).getErrorMessage().getValue();
		} catch (Exception ex) {
			// Ignore exception.
		}
		return null;
	}

}
