/**
 * 
 */
package com.am.checkin.web.util;

import java.net.InetAddress;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeromexico.slack.models.Attachment;
import com.aeromexico.slack.models.Attachments;
import com.aeromexico.slack.models.Field;
import com.aeromexico.slack.models.Fields;

/**
 * @author jclun
 *
 */
public class AttachmentUtil {
	
	private static final Logger logger = LoggerFactory.getLogger(AttachmentUtil.class);

    public static Attachments getAttachment(String pnr, String url, int timeOut, long processed) {
		Attachments attach = new Attachments();
		Attachment attachment = new Attachment();
		attachment.setColor("#FFBF00");
		attachment.setFallback("Complete details.");
		Fields fields = new Fields();
		Field field = new Field();
		field.setTitle("Exception");
		field.setValue("Transaction is taking more than " + timeOut + " seconds in the response");
		fields.add(field);
		field = new Field();
		field.setTitle("TRX TIME");
		field.setValue(processed + " ms");
		fields.add(field);
		field = new Field();
		field.setTitle("PNR");
		field.setValue(pnr);
		fields.add(field);
		field = new Field();
		field.setTitle("URL");
		field.setValue(url);
		fields.add(field);
		field = new Field();
		field.setTitle("ENV");
		try {
			field.setValue(InetAddress.getLocalHost().getHostName());			
		} catch(Exception e) {
			logger.error("Cannot get hostname: {}", e);
		}
		fields.add(field);
		attachment.setFields(fields);
		attach.add(attachment);
		return attach;
	}
	
	public static Attachments getAttachmentException(String pnr, String url, String ex) {
		Attachments attach = new Attachments();
		Attachment attachment = new Attachment();
		attachment.setColor("danger");
		attachment.setFallback("Complete details.");
		Fields fields = new Fields();
		Field field = new Field();
		field.setTitle("Exception");
		field.setValue(ex);
		fields.add(field);
		field = new Field();
		field.setTitle("PNR");
		field.setValue(pnr);
		fields.add(field);
		field = new Field();
		field.setTitle("URL");
		field.setValue(url);
		fields.add(field);
		field = new Field();
		field.setTitle("ENV");
		try {
			field.setValue(InetAddress.getLocalHost().getHostName());			
		} catch(Exception e) {
			logger.error("Cannot get hostname: {}", e);
			field.setValue(e.getMessage());
		}
		fields.add(field);
		attachment.setFields(fields);
		attach.add(attachment);
		return attach;
	}

}
