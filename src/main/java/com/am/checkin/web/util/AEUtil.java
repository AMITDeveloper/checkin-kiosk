package com.am.checkin.web.util;

import com.aeromexico.commons.model.AbstractAncillary;
import com.aeromexico.commons.model.AbstractSeat;
import com.aeromexico.commons.model.AbstractSegmentChoice;
import com.aeromexico.commons.model.Ancillary;
import com.aeromexico.commons.model.AncillaryOffer;
import com.aeromexico.commons.model.BaggageRoute;
import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.CabinUpgradeAncillary;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.ExtraWeightAncillary;
import com.aeromexico.commons.model.PNR;
import com.aeromexico.commons.model.PaidTravelerAncillary;
import com.aeromexico.commons.model.TicketedTravelerAncillary;
import com.aeromexico.commons.model.TravelerAncillary;
import com.aeromexico.commons.model.UpgradeAncillary;
import com.aeromexico.commons.model.utils.CurrencyUtil;
import com.aeromexico.commons.web.types.ActionCodeType;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.util.FilterPassengersUtil;
import com.aeromexico.commons.web.util.ListsUtil;
import com.aeromexico.commons.web.util.SegmentCodeUtil;
import com.aeromexico.commons.web.util.TimeUtil;
import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.BookingClass;
import com.aeromexico.commons.web.types.AirlineCodeType;
import com.aeromexico.commons.web.types.AncillaryPostBookingType;
import com.aeromexico.commons.web.types.MarketType;
import com.aeromexico.sharedservices.util.AncillaryUtil;
import com.sabre.services.micellaneous.EmdType;
import com.sabre.services.micellaneous.SegmentIndicator;
import com.sabre.services.model.ReservationUpdateDeleteItem;
import com.sabre.webservices.pnrbuilder.updatereservation.AncillaryPricePNRB;
import com.sabre.webservices.pnrbuilder.updatereservation.AncillaryServiceActionCodeUpdatePNRB;
import com.sabre.webservices.pnrbuilder.updatereservation.AncillaryServicesPNRB;
import com.sabre.webservices.pnrbuilder.updatereservation.AncillaryServicesPartialUpdatePNRB;
import com.sabre.webservices.pnrbuilder.updatereservation.AncillaryServicesUpdatePNRB;
import com.sabre.webservices.pnrbuilder.updatereservation.AncillaryTaxPNRB;
import com.sabre.webservices.pnrbuilder.updatereservation.IndicatorType;
import com.sabre.webservices.pnrbuilder.updatereservation.LocatorWithPartitionType;
import com.sabre.webservices.pnrbuilder.updatereservation.NameAssociationListPNRB;
import com.sabre.webservices.pnrbuilder.updatereservation.NameAssociationTagPNRB;
import com.sabre.webservices.pnrbuilder.updatereservation.OperationTypePNRB;
import com.sabre.webservices.pnrbuilder.updatereservation.ReceivedFromPNRB;
import com.sabre.webservices.pnrbuilder.updatereservation.RequestEnumerationType;
import com.sabre.webservices.pnrbuilder.updatereservation.RequestType;
import com.sabre.webservices.pnrbuilder.updatereservation.ReservationUpdateItemType;
import com.sabre.webservices.pnrbuilder.updatereservation.ReservationUpdateListType;
import com.sabre.webservices.pnrbuilder.updatereservation.ReturnOptions;
import com.sabre.webservices.pnrbuilder.updatereservation.SegmentAssociationListPNRB;
import com.sabre.webservices.pnrbuilder.updatereservation.SegmentAssociationTagPNRB;
import com.sabre.webservices.pnrbuilder.updatereservation.UpdateErrorsPNRB;
import com.sabre.webservices.pnrbuilder.updatereservation.UpdateReservationRQ;
import com.sabre.webservices.pnrbuilder.updatereservation.UpdateReservationRS;
import com.sabre.webservices.pnrbuilder.updatereservation.UpdateWarningsPNRB;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.ws.rs.core.Response;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author adrian
 */
public class AEUtil {

    private static final Logger LOG = LoggerFactory.getLogger(AEUtil.class);

    private static final String VERSION = "1.19.0";
    private static final String AGENT_NAME = "CHECKINAPI";
    private static final String PARTITION = "AM";

    /**
     * @param pnr
     * @param ancillaryIDs
     * @return
     */
    public static List<ReservationUpdateItemType> getUnpaidSeatItemsToBeRemoved(PNR pnr, Set<Integer> ancillaryIDs) {
        if (null == ancillaryIDs) {
            ancillaryIDs = new HashSet<>();
        }

        List<ReservationUpdateItemType> reservationUpdateItemTypeList = new ArrayList<>();

        //forming request
        int updateId = 1;
        for (CartPNR cartPNR : pnr.getCarts().getCollection()) {
            for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
                for (AbstractAncillary abstractAncillary : bookedTraveler.getSeatAncillaries().getCollection()) {
                    if (abstractAncillary instanceof TravelerAncillary) {
                        TravelerAncillary travelerAncillary = (TravelerAncillary) abstractAncillary;

                        if (travelerAncillary.isNotPaid()) {
                            try {
                                AbstractSegmentChoice abstractSegmentChoice;
                                abstractSegmentChoice = bookedTraveler.getSegmentChoices().getBySegmentCode(
                                        travelerAncillary.getSegmentCodeAux()
                                );

                                if (null == abstractSegmentChoice) {
                                    try {
                                        ancillaryIDs.add(new Integer(travelerAncillary.getAncillaryId()));
                                    } catch (NumberFormatException ex) {
                                        //
                                    }

                                    ReservationUpdateItemType reservationUpdateItemType;
                                    reservationUpdateItemType = getDeleteItem(
                                            updateId, travelerAncillary.getAncillaryId(), bookedTraveler
                                    );

                                    reservationUpdateItemTypeList.add(reservationUpdateItemType);

                                    updateId++;
                                } else {
                                    String seatCodeAE = StringUtils.leftPad(travelerAncillary.getSeatCode(), 3, "0");
                                    String seatCode = StringUtils.leftPad(abstractSegmentChoice.getSeat().getCode(), 3, "0");

                                    if (seatCodeAE.equalsIgnoreCase(seatCode)) {
                                        if (!travelerAncillary.getAncillary().getCurrency().isAmountCorrect()) {
                                            try {
                                                ancillaryIDs.add(new Integer(travelerAncillary.getAncillaryId()));
                                            } catch (NumberFormatException ex) {
                                                //
                                            }

                                            ReservationUpdateItemType reservationUpdateItemType;
                                            reservationUpdateItemType = getDeleteItem(
                                                    updateId, travelerAncillary.getAncillaryId(), bookedTraveler
                                            );

                                            reservationUpdateItemTypeList.add(reservationUpdateItemType);

                                            travelerAncillary.setManuallyDeleted(true);

                                            updateId++;
                                        }
                                    } else {
                                        try {
                                            ancillaryIDs.add(new Integer(travelerAncillary.getAncillaryId()));
                                        } catch (NumberFormatException ex) {
                                            //
                                        }

                                        ReservationUpdateItemType reservationUpdateItemType;
                                        reservationUpdateItemType = getDeleteItem(
                                                updateId, travelerAncillary.getAncillaryId(), bookedTraveler
                                        );

                                        reservationUpdateItemTypeList.add(reservationUpdateItemType);

                                        travelerAncillary.setManuallyDeleted(true);

                                        updateId++;
                                    }
                                }
                            } catch (Exception ex) {
                                //
                            }
                        }
                    }
                }
            }
        }

        return reservationUpdateItemTypeList;
    }

    /**
     * @param pnr
     * @param ancillaryIDs
     * @return
     */
    public static List<ReservationUpdateItemType> getUnpaidExtraWeightItemsToBeRemoved(PNR pnr, Set<Integer> ancillaryIDs) {
        if (null == ancillaryIDs) {
            ancillaryIDs = new HashSet<>();
        }

        List<ReservationUpdateItemType> reservationUpdateItemTypeList = new ArrayList<>();

        //forming request
        int updateId = 1;
        for (CartPNR cartPNR : pnr.getCarts().getCollection()) {
            for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {

                for (ExtraWeightAncillary extraWeightAncillary : PurchaseOrderUtil.getExtraWeightUnpaidAncillariesOnReservation(bookedTraveler.getAncillaries().getCollection())) {
                    if (extraWeightAncillary.isNotPaid()) {
                        try {

                            try {
                                ancillaryIDs.add(new Integer(extraWeightAncillary.getAncillaryId()));
                            } catch (NumberFormatException ex) {
                                //
                            }

                            ReservationUpdateItemType reservationUpdateItemType;
                            reservationUpdateItemType = getDeleteItem(
                                    updateId, extraWeightAncillary.getAncillaryId(), bookedTraveler
                            );

                            reservationUpdateItemTypeList.add(reservationUpdateItemType);

                            updateId++;
                        } catch (Exception ex) {
                            //
                        }
                    }
                }
            }
        }

        return reservationUpdateItemTypeList;
    }

    /**
     * @param pnr
     * @param ancillaryIDs
     */
    public static void removeDeletedItemsFromCollection(PNR pnr, Set<Integer> ancillaryIDs) {
        //remove deleted ancillaries from collection
        for (CartPNR cartPNR : pnr.getCarts().getCollection()) {
            for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {

                Iterator<AbstractAncillary> itr = bookedTraveler.getAncillaries().getCollection().iterator();

                while (itr.hasNext()) {
                    AbstractAncillary abstractAncillary = itr.next(); // must be called before you

                    if (abstractAncillary instanceof TravelerAncillary) {
                        TravelerAncillary travelerAncillary = (TravelerAncillary) abstractAncillary;

                        try {
                            if (ancillaryIDs.contains(new Integer(travelerAncillary.getAncillaryId()))) {
                                itr.remove();
                            }
                        } catch (Exception ex) {
                            LOG.error("Error removing seat from collection. " + ex.getMessage());
                        }
                    } else if (abstractAncillary instanceof ExtraWeightAncillary) {
                        ExtraWeightAncillary extraWeightAncillary = (ExtraWeightAncillary) abstractAncillary;

                        try {
                            if (ancillaryIDs.contains(new Integer(extraWeightAncillary.getAncillaryId()))) {
                                itr.remove();
                            }
                        } catch (Exception ex) {
                            LOG.error("Error removing seat from collection. " + ex.getMessage());
                        }
                    } else if (abstractAncillary instanceof CabinUpgradeAncillary) {
                        CabinUpgradeAncillary cabinUpgradeAncillary = (CabinUpgradeAncillary) abstractAncillary;

                        try {
                            if (ancillaryIDs.contains(new Integer(cabinUpgradeAncillary.getAncillaryId()))) {
                                itr.remove();
                            }
                        } catch (Exception ex) {
                            LOG.error("Error removing seat from collection. " + ex.getMessage());
                        }
                    }
                }

                itr = bookedTraveler.getSeatAncillaries().getCollection().iterator();

                while (itr.hasNext()) {
                    AbstractAncillary abstractAncillary = itr.next(); // must be called before you
                    // can call itr.remove()

                    if (abstractAncillary instanceof TravelerAncillary) {
                        TravelerAncillary travelerAncillary = (TravelerAncillary) abstractAncillary;

                        try {
                            if (ancillaryIDs.contains(new Integer(travelerAncillary.getAncillaryId()))) {
                                int index;
                                index = bookedTraveler.getSegmentChoices().getIndexBySegmentCode(
                                        travelerAncillary.getSegmentCodeAux()
                                );

                                if (index >= 0) {
                                    bookedTraveler.getSegmentChoices().getCollection().remove(index);
                                }

                                itr.remove();
                            }
                        } catch (Exception ex) {
                            LOG.error("Error removing seat from collection. " + ex.getMessage());
                        }
                    }
                }

            }
        }
    }

    /**
     * @param ancillaryId
     * @param bookedTraveler
     * @param reservationUpdateItemTypeList
     */
    public static void addDeleteItem(
            String ancillaryId,
            BookedTraveler bookedTraveler,
            List<ReservationUpdateItemType> reservationUpdateItemTypeList
    ) {

        if (null == reservationUpdateItemTypeList) {
            reservationUpdateItemTypeList = new ArrayList<>();
        }

        int updateId = reservationUpdateItemTypeList.size() + 1;

        reservationUpdateItemTypeList.add(getDeleteItem(updateId, ancillaryId, bookedTraveler));

    }

    /**
     * @param updateId
     * @param ancillaryId
     * @param bookedTraveler
     * @return
     */
    public static ReservationUpdateItemType getDeleteItem(int updateId, String ancillaryId, BookedTraveler bookedTraveler) {
        //// Reservation Update Item
        ReservationUpdateItemType reservationUpdateItemType = new ReservationUpdateItemType();
        reservationUpdateItemType.setUpdateId(String.valueOf(updateId));
        AncillaryServicesUpdatePNRB ancillaryServicesUpdatePNRB = new AncillaryServicesUpdatePNRB();
        ancillaryServicesUpdatePNRB.setId(ancillaryId);
        ancillaryServicesUpdatePNRB.setOp(OperationTypePNRB.D);
        NameAssociationListPNRB nameAssociationListPNRB = new NameAssociationListPNRB();
        NameAssociationTagPNRB nameAssociationTagPNRB = new NameAssociationTagPNRB();
        nameAssociationTagPNRB.setFirstName(bookedTraveler.getFirstName());
        nameAssociationTagPNRB.setLastName(bookedTraveler.getLastName());
        nameAssociationTagPNRB.setNameRefNumber(bookedTraveler.getNameRefNumber());
        nameAssociationListPNRB.getNameAssociationTag().add(nameAssociationTagPNRB);
        ancillaryServicesUpdatePNRB.setNameAssociationList(nameAssociationListPNRB);
        reservationUpdateItemType.setAncillaryServicesUpdate(ancillaryServicesUpdatePNRB);
        return reservationUpdateItemType;
    }

    /**
     * @param segmentCode
     * @param abstractSegmentChoiceList
     * @return
     */
    public static boolean existSegmentChoice(String segmentCode, List<AbstractSegmentChoice> abstractSegmentChoiceList) {
        if (null == abstractSegmentChoiceList || abstractSegmentChoiceList.isEmpty()) {
            return false;
        }

        for (AbstractSegmentChoice abstractSegmentChoice : abstractSegmentChoiceList) {
            if (SegmentCodeUtil.compareSegmentCodeWithoutTime(segmentCode, abstractSegmentChoice.getSegmentCode())) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param segmentCode
     * @param abstractSegmentChoiceList
     * @return
     */
    public static String getSeatCode(String segmentCode, List<AbstractSegmentChoice> abstractSegmentChoiceList) {
        if (null == segmentCode || segmentCode.trim().isEmpty() || null == abstractSegmentChoiceList || abstractSegmentChoiceList.isEmpty()) {
            return null;
        }

        for (AbstractSegmentChoice abstractSegmentChoice : abstractSegmentChoiceList) {

            if (SegmentCodeUtil.compareSegmentCodeWithoutTime(segmentCode, abstractSegmentChoice.getSegmentCode())) {

                AbstractSeat abstractSeat = abstractSegmentChoice.getSeat();

                if (null != abstractSeat) {
                    return abstractSeat.getCode();
                }
            }
        }

        return null;
    }

    /**
     * @param flightnUmber
     * @param allBookendSegment
     * @return
     */
    public static List<BookedSegment> getSegmentsWithSameFLightNumber(
            String flightnUmber,
            List<BookedSegment> allBookendSegment
    ) {

        List<BookedSegment> bookedSegmentList = allBookendSegment.stream().filter(
                (bookedSegment) -> (bookedSegment.getSegment().getOperatingFlightCode().equalsIgnoreCase(flightnUmber)))
                .collect(Collectors.toList());

        return bookedSegmentList;
    }

    /**
     * @param bookedLeg
     * @param cabinUpgradeAncillary
     * @param cabinClass
     * @return
     * @throws DatatypeConfigurationException
     * @throws ParseException
     */
    public static List<SegmentAssociationTagPNRB> getItineraryInfo(
            BookedLeg bookedLeg,
            CabinUpgradeAncillary cabinUpgradeAncillary,
            String cabinClass
    ) throws DatatypeConfigurationException, ParseException {

        List<SegmentAssociationTagPNRB> segmentAssociationTagPNRBList = new ArrayList<>();

        if (null == bookedLeg
                || null == bookedLeg.getSegments()
                || null == bookedLeg.getSegments().getCollection()
                || bookedLeg.getSegments().getCollection().isEmpty()) {
            return segmentAssociationTagPNRBList;
        }

        for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {
            if (SegmentCodeUtil.compareSegmentCodeWithoutTime(
                    cabinUpgradeAncillary.getSegmentCode(), bookedSegment.getSegment().getSegmentCode()
            )) {

                List<BookedSegment> bookedSegmentList = AEUtil.getSegmentsWithSameFLightNumber(
                        bookedSegment.getSegment().getOperatingFlightCode(), bookedLeg.getSegments().getCollection()
                );

                BookedSegment firstSegment = ListsUtil.getFirst(bookedSegmentList);
                BookedSegment lastSegment = ListsUtil.getLast(bookedSegmentList);

                SegmentAssociationTagPNRB segmentAssociationTagPNRB = new SegmentAssociationTagPNRB();
                segmentAssociationTagPNRB.setBoardPoint(firstSegment.getSegment().getDepartureAirport());
                if (null != firstSegment.getSegment().getSegmentStatus() && !firstSegment.getSegment().getSegmentStatus().isEmpty()) {
                    segmentAssociationTagPNRB.setBookingStatus(firstSegment.getSegment().getSegmentStatus());
                } else {
                    segmentAssociationTagPNRB.setBookingStatus("HK");
                }
                segmentAssociationTagPNRB.setCarrierCode(firstSegment.getSegment().getOperatingCarrier());
                segmentAssociationTagPNRB.setClassOfService(cabinClass);

                GregorianCalendar c = new GregorianCalendar();
                c.setTimeInMillis(TimeUtil.getTime(firstSegment.getSegment().getDepartureDateTime()));
                XMLGregorianCalendar xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
                xmlGregorianCalendar.setMillisecond(DatatypeConstants.FIELD_UNDEFINED);
                xmlGregorianCalendar.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
                segmentAssociationTagPNRB.setDepartureDate(xmlGregorianCalendar);

                segmentAssociationTagPNRB.setFlightNumber(firstSegment.getSegment().getOperatingFlightCode());
                segmentAssociationTagPNRB.setOffPoint(lastSegment.getSegment().getArrivalAirport());

                segmentAssociationTagPNRBList.add(segmentAssociationTagPNRB);

                break;
            }
        }

        return segmentAssociationTagPNRBList;
    }

    /**
     * @param bookedTraveler
     * @param bookedLeg
     * @return
     * @throws DatatypeConfigurationException
     * @throws ParseException
     */
    public static List<SegmentAssociationTagPNRB> getItineraryInfo(
            BookedTraveler bookedTraveler,
            BookedLeg bookedLeg
    ) throws DatatypeConfigurationException, ParseException {
        List<SegmentAssociationTagPNRB> segmentAssociationTagPNRBList = new ArrayList<>();

        if (null == bookedLeg
                || null == bookedLeg.getSegments()
                || null == bookedLeg.getSegments().getCollection()
                || bookedLeg.getSegments().getCollection().isEmpty()) {
            return segmentAssociationTagPNRBList;
        }

        List<BookedSegment> bookedSegmentList = bookedLeg.getSegments().getSegmentsOperatedBy(AirlineCodeType.AM);

        BaggageRoute baggageRoute;
        BookingClass bookingClass;
        String cabinClass;
        GregorianCalendar c;
        XMLGregorianCalendar xmlGregorianCalendar;
        if (null != bookedSegmentList
                && !bookedSegmentList.isEmpty()) {

            String flightNumber = "-1"; //helpful data on purpose
            SegmentAssociationTagPNRB segmentAssociationTagPNRB = new SegmentAssociationTagPNRB();

            for (BookedSegment bookedSegment : bookedSegmentList) {

                baggageRoute = PurchaseOrderUtil.getBaggageRouteBySegment(
                        bookedSegment.getSegment(), bookedTraveler.getBaggageRouteList()
                );

                if (null == baggageRoute) {
                    bookingClass = PurchaseOrderUtil.getBookingClassBySegment(
                            bookedSegment.getSegment(),
                            bookedTraveler.getBookingClasses()
                    );
                    if (null != bookingClass) {
                        cabinClass = bookingClass.getBookingClass();
                    } else {
                        cabinClass = "";
                    }
                } else {
                    cabinClass = baggageRoute.getBookingClass();
                }

                if (FilterPassengersUtil.compareFlightNumber(
                        flightNumber, bookedSegment.getSegment().getOperatingFlightCode()
                )) {
                    segmentAssociationTagPNRB.setOffPoint(bookedSegment.getSegment().getArrivalAirport());
                } else {
                    flightNumber = bookedSegment.getSegment().getOperatingFlightCode();

                    segmentAssociationTagPNRB = new SegmentAssociationTagPNRB();
                    segmentAssociationTagPNRB.setBoardPoint(bookedSegment.getSegment().getDepartureAirport());
                    if (null != bookedSegment.getSegment().getSegmentStatus() && !bookedSegment.getSegment().getSegmentStatus().isEmpty()) {
                        segmentAssociationTagPNRB.setBookingStatus(bookedSegment.getSegment().getSegmentStatus());
                    } else {
                        segmentAssociationTagPNRB.setBookingStatus("HK");
                    }
                    segmentAssociationTagPNRB.setCarrierCode(bookedSegment.getSegment().getOperatingCarrier());
                    segmentAssociationTagPNRB.setClassOfService(cabinClass);

                    c = new GregorianCalendar();
                    c.setTimeInMillis(TimeUtil.getTime(bookedSegment.getSegment().getDepartureDateTime()));
                    xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
                    xmlGregorianCalendar.setMillisecond(DatatypeConstants.FIELD_UNDEFINED);
                    xmlGregorianCalendar.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
                    segmentAssociationTagPNRB.setDepartureDate(xmlGregorianCalendar);

                    segmentAssociationTagPNRB.setFlightNumber(flightNumber);
                    segmentAssociationTagPNRB.setOffPoint(bookedSegment.getSegment().getArrivalAirport());

                    segmentAssociationTagPNRBList.add(segmentAssociationTagPNRB);
                }
            }
        }

        return segmentAssociationTagPNRBList;
    }

    /**
     * @param bookedTraveler
     * @param bookedLeg
     * @param travelerAncillary
     * @return
     * @throws DatatypeConfigurationException
     * @throws ParseException
     */
    public static List<SegmentAssociationTagPNRB> getItineraryInfo(
            BookedTraveler bookedTraveler,
            BookedLeg bookedLeg,
            TravelerAncillary travelerAncillary
    ) throws DatatypeConfigurationException, ParseException {

        List<SegmentAssociationTagPNRB> segmentAssociationTagPNRBList = new ArrayList<>();

        if (null == bookedLeg
                || null == bookedLeg.getSegments()
                || null == bookedLeg.getSegments().getCollection()
                || bookedLeg.getSegments().getCollection().isEmpty()) {
            return segmentAssociationTagPNRBList;
        }

        EmdType emdType;
        if ("2".equals(travelerAncillary.getAncillary().getEmdType())) {
            emdType = EmdType.ASSOCIATED;
        } else {
            emdType = EmdType.STANDALONE;
        }

        if (EmdType.STANDALONE.equals(emdType)) {
            return segmentAssociationTagPNRBList;
        }

        SegmentIndicator segmentIndicator;
        try {
            segmentIndicator = SegmentIndicator.fromValue(
                    travelerAncillary.getAncillary().getSegmentIndicator().toUpperCase()
            );
        } catch (Exception ex) {
            segmentIndicator = SegmentIndicator.P;//by default
        }

        if ((SegmentIndicator.S.equals(segmentIndicator)
                && null == travelerAncillary.getSegmentCodeAux())
                || "1".equals(travelerAncillary.getAncillary().getEmdType())) {

            return segmentAssociationTagPNRBList;
        }

        String cabinClass;
        BaggageRoute baggageRoute;
        BookingClass bookingClass;
        GregorianCalendar c;
        XMLGregorianCalendar xmlGregorianCalendar;

        switch (segmentIndicator) {
            case S:
                for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {
                    if (SegmentCodeUtil.compareSegmentCodeWithoutTime(
                            travelerAncillary.getSegmentCodeAux(), bookedSegment.getSegment().getSegmentCode()
                    )) {

                        List<BookedSegment> bookedSegmentList;
                        bookedSegmentList = AEUtil.getSegmentsWithSameFLightNumber(
                                bookedSegment.getSegment().getOperatingFlightCode(), bookedLeg.getSegments().getCollection()
                        );

                        BookedSegment firstSegment = ListsUtil.getFirst(bookedSegmentList);
                        BookedSegment lastSegment = ListsUtil.getLast(bookedSegmentList);

                        baggageRoute = PurchaseOrderUtil.getBaggageRouteBySegment(
                                firstSegment.getSegment(), bookedTraveler.getBaggageRouteList()
                        );

                        if (null == baggageRoute) {
                            bookingClass = PurchaseOrderUtil.getBookingClassBySegment(
                                    firstSegment.getSegment(),
                                    bookedTraveler.getBookingClasses()
                            );
                            if (null != bookingClass) {
                                cabinClass = bookingClass.getBookingClass();
                            } else {
                                cabinClass = "";
                            }
                        } else {
                            cabinClass = baggageRoute.getBookingClass();
                        }

                        SegmentAssociationTagPNRB segmentAssociationTagPNRB = new SegmentAssociationTagPNRB();
                        segmentAssociationTagPNRB.setBoardPoint(firstSegment.getSegment().getDepartureAirport());
                        if (null != firstSegment.getSegment().getSegmentStatus() && !firstSegment.getSegment().getSegmentStatus().isEmpty()) {
                            segmentAssociationTagPNRB.setBookingStatus(firstSegment.getSegment().getSegmentStatus());
                        } else {
                            segmentAssociationTagPNRB.setBookingStatus("HK");
                        }
                        segmentAssociationTagPNRB.setCarrierCode(firstSegment.getSegment().getOperatingCarrier());
                        segmentAssociationTagPNRB.setClassOfService(cabinClass);

                        c = new GregorianCalendar();
                        c.setTimeInMillis(TimeUtil.getTime(firstSegment.getSegment().getDepartureDateTime()));
                        xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
                        xmlGregorianCalendar.setMillisecond(DatatypeConstants.FIELD_UNDEFINED);
                        xmlGregorianCalendar.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
                        segmentAssociationTagPNRB.setDepartureDate(xmlGregorianCalendar);

                        segmentAssociationTagPNRB.setFlightNumber(firstSegment.getSegment().getOperatingFlightCode());
                        segmentAssociationTagPNRB.setOffPoint(lastSegment.getSegment().getArrivalAirport());

                        segmentAssociationTagPNRBList.add(segmentAssociationTagPNRB);

                        break;
                    }
                }
                break;

            case P:
            default:
                List<BookedSegment> bookedSegmentList = bookedLeg.getSegments().getSegmentsOperatedBy(AirlineCodeType.AM);

                if (null != bookedSegmentList
                        && !bookedSegmentList.isEmpty()) {

                    String flightNumber = "-1"; //helpful data on purpose
                    SegmentAssociationTagPNRB segmentAssociationTagPNRB = new SegmentAssociationTagPNRB();

                    for (BookedSegment bookedSegment : bookedSegmentList) {

                        baggageRoute = PurchaseOrderUtil.getBaggageRouteBySegment(
                                bookedSegment.getSegment(), bookedTraveler.getBaggageRouteList()
                        );

                        if (null == baggageRoute) {
                            bookingClass = PurchaseOrderUtil.getBookingClassBySegment(
                                    bookedSegment.getSegment(),
                                    bookedTraveler.getBookingClasses()
                            );
                            if (null != bookingClass) {
                                cabinClass = bookingClass.getBookingClass();
                            } else {
                                cabinClass = "";
                            }
                        } else {
                            cabinClass = baggageRoute.getBookingClass();
                        }

                        if (FilterPassengersUtil.compareFlightNumber(
                                flightNumber, bookedSegment.getSegment().getOperatingFlightCode()
                        )) {
                            segmentAssociationTagPNRB.setOffPoint(bookedSegment.getSegment().getArrivalAirport());
                        } else {
                            flightNumber = bookedSegment.getSegment().getOperatingFlightCode();

                            segmentAssociationTagPNRB = new SegmentAssociationTagPNRB();
                            segmentAssociationTagPNRB.setBoardPoint(bookedSegment.getSegment().getDepartureAirport());
                            if (null != bookedSegment.getSegment().getSegmentStatus() && !bookedSegment.getSegment().getSegmentStatus().isEmpty()) {
                                segmentAssociationTagPNRB.setBookingStatus(bookedSegment.getSegment().getSegmentStatus());
                            } else {
                                segmentAssociationTagPNRB.setBookingStatus("HK");
                            }
                            segmentAssociationTagPNRB.setCarrierCode(bookedSegment.getSegment().getOperatingCarrier());
                            segmentAssociationTagPNRB.setClassOfService(cabinClass);

                            c = new GregorianCalendar();
                            c.setTimeInMillis(TimeUtil.getTime(bookedSegment.getSegment().getDepartureDateTime()));
                            xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
                            xmlGregorianCalendar.setMillisecond(DatatypeConstants.FIELD_UNDEFINED);
                            xmlGregorianCalendar.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
                            segmentAssociationTagPNRB.setDepartureDate(xmlGregorianCalendar);

                            segmentAssociationTagPNRB.setFlightNumber(flightNumber);
                            segmentAssociationTagPNRB.setOffPoint(bookedSegment.getSegment().getArrivalAirport());

                            segmentAssociationTagPNRBList.add(segmentAssociationTagPNRB);
                        }
                    }

                    break;
                }

                break;
        }

        return segmentAssociationTagPNRBList;
    }

    /**
     * @param bookedTraveler
     * @param extraWeightAncillary
     * @param segmentAssociationTagPNRB
     * @param store
     * @return
     */
    private static AncillaryServicesUpdatePNRB getAncillaryItemForExtraWeight(
            BookedTraveler bookedTraveler,
            ExtraWeightAncillary extraWeightAncillary,
            List<SegmentAssociationTagPNRB> segmentAssociationTagPNRB,
            String store
    ) {
        AncillaryServicesUpdatePNRB ancillaryServicesUpdatePNRB = new AncillaryServicesUpdatePNRB();
        ancillaryServicesUpdatePNRB.setOp(OperationTypePNRB.C);

        NameAssociationListPNRB nameAssociationListPNRB = new NameAssociationListPNRB();
        NameAssociationTagPNRB nameAssociationTagPNRB = new NameAssociationTagPNRB();
        nameAssociationTagPNRB.setFirstName(bookedTraveler.getFirstName());
        nameAssociationTagPNRB.setLastName(bookedTraveler.getLastName());
        nameAssociationTagPNRB.setNameRefNumber(bookedTraveler.getNameRefNumber());
        nameAssociationListPNRB.getNameAssociationTag().add(nameAssociationTagPNRB);
        ancillaryServicesUpdatePNRB.setNameAssociationList(nameAssociationListPNRB);

        if (null != segmentAssociationTagPNRB && !segmentAssociationTagPNRB.isEmpty()) {
            SegmentAssociationListPNRB segmentAssociationListPNRB = new SegmentAssociationListPNRB();
            segmentAssociationListPNRB.getSegmentAssociationTag().addAll(segmentAssociationTagPNRB);
            ancillaryServicesUpdatePNRB.setSegmentAssociationList(segmentAssociationListPNRB);
        }

        ancillaryServicesUpdatePNRB.setGroupCode(extraWeightAncillary.getAncillary().getGroupCode());
        ancillaryServicesUpdatePNRB.setRficCode(extraWeightAncillary.getAncillary().getRficCode());
        ancillaryServicesUpdatePNRB.setSegmentIndicator(extraWeightAncillary.getAncillary().getSegmentIndicator());
        ancillaryServicesUpdatePNRB.setOwningCarrierCode(extraWeightAncillary.getAncillary().getOwningCarrierCode());
        ancillaryServicesUpdatePNRB.setRefundIndicator(extraWeightAncillary.getAncillary().getRefundIndicator());
        ancillaryServicesUpdatePNRB.setCommisionIndicator(extraWeightAncillary.getAncillary().getCommisionIndicator());
        ancillaryServicesUpdatePNRB.setInterlineIndicator(extraWeightAncillary.getAncillary().getInterlineIndicator());
        ancillaryServicesUpdatePNRB.setPassengerTypeCode(bookedTraveler.getPaxType().getCode());

        if (null != segmentAssociationTagPNRB && !segmentAssociationTagPNRB.isEmpty()) {
            SegmentAssociationTagPNRB segment = segmentAssociationTagPNRB.get(0);
            ancillaryServicesUpdatePNRB.setBoardPoint(segment.getBoardPoint());
            ancillaryServicesUpdatePNRB.setOffPoint(segment.getOffPoint());
        }

        ancillaryServicesUpdatePNRB.setCommercialName(extraWeightAncillary.getAncillary().getCommercialName());
        ancillaryServicesUpdatePNRB.setRficSubcode(extraWeightAncillary.getAncillary().getRficSubcode());
        ancillaryServicesUpdatePNRB.setVendor(extraWeightAncillary.getAncillary().getVendor());

        if (null == extraWeightAncillary.getAncillary().getEmdConsummedAtIssuance()
                || extraWeightAncillary.getAncillary().getEmdConsummedAtIssuance().isEmpty()) {
            extraWeightAncillary.getAncillary().setEmdConsummedAtIssuance("Y");
            ancillaryServicesUpdatePNRB.setEMDConsummedAtIssuance("Y");
        } else {
            ancillaryServicesUpdatePNRB.setEMDConsummedAtIssuance(extraWeightAncillary.getAncillary().getEmdConsummedAtIssuance());
        }

        ancillaryServicesUpdatePNRB.setEMDType("2");
        ancillaryServicesUpdatePNRB.setFeeApplicationIndicator("4");
        String actionCode = null == extraWeightAncillary.getActionCode() ? ActionCodeType.HD.toString() : extraWeightAncillary.getActionCode();
        ancillaryServicesUpdatePNRB.setActionCode(actionCode);

        String currencyCode = extraWeightAncillary.getAncillary().getCurrency().getCurrencyCode();
        String equivalentCurrencyCode = extraWeightAncillary.getAncillary().getCurrency().getEquivalentCurrencyCode();
        AncillaryPricePNRB ancillaryPricePNRB;

        ancillaryPricePNRB = new AncillaryPricePNRB();
        ancillaryPricePNRB.setCurrency(currencyCode);
        ancillaryPricePNRB.setPrice(
                CurrencyUtil.getAmount(
                        extraWeightAncillary.getAncillary().getCurrency().getTotal(),
                        currencyCode
                )
        );
        ancillaryServicesUpdatePNRB.setTTLPrice(ancillaryPricePNRB);

        if (null != extraWeightAncillary.getAncillary().getCurrency().getEquivalent()
                && 1 == extraWeightAncillary.getAncillary().getCurrency().getEquivalent().compareTo(BigDecimal.ZERO)) {
            ancillaryPricePNRB = new AncillaryPricePNRB();
            ancillaryPricePNRB.setCurrency(currencyCode);
            ancillaryPricePNRB.setPrice(
                    CurrencyUtil.getAmount(
                            extraWeightAncillary.getAncillary().getCurrency().getBase(),
                            currencyCode
                    )
            );
            ancillaryServicesUpdatePNRB.setEquivalentPrice(ancillaryPricePNRB);

            ancillaryPricePNRB = new AncillaryPricePNRB();
            ancillaryPricePNRB.setCurrency(equivalentCurrencyCode);
            ancillaryPricePNRB.setPrice(
                    CurrencyUtil.getAmount(
                            extraWeightAncillary.getAncillary().getCurrency().getEquivalent(),
                            equivalentCurrencyCode
                    )
            );
            ancillaryServicesUpdatePNRB.setOriginalBasePrice(ancillaryPricePNRB);
        } else {
            ancillaryPricePNRB = new AncillaryPricePNRB();
            ancillaryPricePNRB.setCurrency(currencyCode);
            ancillaryPricePNRB.setPrice(
                    CurrencyUtil.getAmount(
                            extraWeightAncillary.getAncillary().getCurrency().getBase(),
                            currencyCode
                    )
            );
            ancillaryServicesUpdatePNRB.setOriginalBasePrice(ancillaryPricePNRB);
        }

        if (null != extraWeightAncillary.getAncillary().getCurrency().getTaxDetails()
                && !extraWeightAncillary.getAncillary().getCurrency().getTaxDetails().isEmpty()) {

            AncillaryServicesPNRB.Taxes taxes = new AncillaryServicesPNRB.Taxes();
            AncillaryServicesPNRB.TotalTaxes totalTaxes = new AncillaryServicesPNRB.TotalTaxes();
            List<AncillaryTaxPNRB> taxesList = taxes.getTax();
            List<AncillaryTaxPNRB> totalTaxesList = totalTaxes.getTax();

            Map<String, BigDecimal> mapTaxes = extraWeightAncillary.getAncillary().getCurrency().getTaxDetails();
            for (Map.Entry<String, BigDecimal> entry : mapTaxes.entrySet()) {
                String key = entry.getKey();
                BigDecimal value = CurrencyUtil.getAmount(entry.getValue(), currencyCode);
                AncillaryTaxPNRB ancillaryTaxPNRB = new AncillaryTaxPNRB();
                ancillaryTaxPNRB.setTaxAmount(value);
                ancillaryTaxPNRB.setTaxCode(key);
                taxesList.add(ancillaryTaxPNRB);
                totalTaxesList.add(ancillaryTaxPNRB);
            }

            ancillaryServicesUpdatePNRB.setTotalTaxes(totalTaxes);
            ancillaryServicesUpdatePNRB.setTaxes(taxes);
            ancillaryServicesUpdatePNRB.setTaxesIncluded(Boolean.TRUE);
        }

        ancillaryServicesUpdatePNRB.setNumberOfItems(String.valueOf(extraWeightAncillary.getQuantity()));

        return ancillaryServicesUpdatePNRB;
    }

    /**
     * @param bookedTraveler
     * @param travelerAncillary
     * @param segmentAssociationTagPNRB
     * @param isUnreserved
     * @return
     */
    private static AncillaryServicesUpdatePNRB getAncillaryItem(
            BookedTraveler bookedTraveler,
            TravelerAncillary travelerAncillary,
            List<SegmentAssociationTagPNRB> segmentAssociationTagPNRB,
            String store,
            boolean isUnreserved
    ) {

        AncillaryServicesUpdatePNRB ancillaryServicesUpdatePNRB = new AncillaryServicesUpdatePNRB();
        ancillaryServicesUpdatePNRB.setOp(OperationTypePNRB.C);

        NameAssociationListPNRB nameAssociationListPNRB = new NameAssociationListPNRB();
        NameAssociationTagPNRB nameAssociationTagPNRB = new NameAssociationTagPNRB();
        nameAssociationTagPNRB.setFirstName(bookedTraveler.getFirstName());
        nameAssociationTagPNRB.setLastName(bookedTraveler.getLastName());
        nameAssociationTagPNRB.setNameRefNumber(bookedTraveler.getNameRefNumber());
        nameAssociationListPNRB.getNameAssociationTag().add(nameAssociationTagPNRB);
        ancillaryServicesUpdatePNRB.setNameAssociationList(nameAssociationListPNRB);

        if ("2".equals(travelerAncillary.getAncillary().getEmdType())
                && null != segmentAssociationTagPNRB
                && !segmentAssociationTagPNRB.isEmpty()) {
            SegmentAssociationListPNRB segmentAssociationListPNRB = new SegmentAssociationListPNRB();
            segmentAssociationListPNRB.getSegmentAssociationTag().addAll(segmentAssociationTagPNRB);
            ancillaryServicesUpdatePNRB.setSegmentAssociationList(segmentAssociationListPNRB);

            SegmentAssociationTagPNRB segment = segmentAssociationTagPNRB.get(0);
            ancillaryServicesUpdatePNRB.setBoardPoint(segment.getBoardPoint());
            ancillaryServicesUpdatePNRB.setOffPoint(segment.getOffPoint());
        }

        if (AncillaryPostBookingType._UCV.getGroup().equalsIgnoreCase(travelerAncillary.getAncillary().getGroupCode())
                || AncillaryPostBookingType._UPG.getGroup().equalsIgnoreCase(travelerAncillary.getAncillary().getGroupCode())) {

            ancillaryServicesUpdatePNRB.setGroupCode(travelerAncillary.getAncillary().getGroupCode());
            ancillaryServicesUpdatePNRB.setRficCode(travelerAncillary.getAncillary().getRficCode());
            ancillaryServicesUpdatePNRB.setSegmentIndicator(travelerAncillary.getAncillary().getSegmentIndicator());
            ancillaryServicesUpdatePNRB.setOwningCarrierCode(travelerAncillary.getAncillary().getOwningCarrierCode());
            if (null != travelerAncillary.getSeatCode()) {
                String seatCode = travelerAncillary.getSeatCode();
                if (seatCode.length() < 3) {
                    seatCode = "0" + seatCode;
                }
                ancillaryServicesUpdatePNRB.setPdcSeat(seatCode);
            }
            ancillaryServicesUpdatePNRB.setRefundIndicator(travelerAncillary.getAncillary().getRefundIndicator());
            ancillaryServicesUpdatePNRB.setCommisionIndicator(travelerAncillary.getAncillary().getCommisionIndicator());
            ancillaryServicesUpdatePNRB.setInterlineIndicator(travelerAncillary.getAncillary().getInterlineIndicator());
            ancillaryServicesUpdatePNRB.setEmdPaperIndicator("E");
            ancillaryServicesUpdatePNRB.setFareGuaranteedIndicator(travelerAncillary.getAncillary().getFeeApplicationIndicator());

            if (AncillaryPostBookingType._UCV.getGroup().equalsIgnoreCase(travelerAncillary.getAncillary().getGroupCode())) {
                ancillaryServicesUpdatePNRB.setSSRCode(AncillaryPostBookingType._UCV.getSsr());
            } else {
                ancillaryServicesUpdatePNRB.setSSRCode(AncillaryPostBookingType._UPG.getSsr());
            }

        } else if (AncillaryPostBookingType._0B5.getGroup().equalsIgnoreCase(travelerAncillary.getAncillary().getGroupCode())) {
            ancillaryServicesUpdatePNRB.setGroupCode(AncillaryPostBookingType._0B5.getGroup());
            ancillaryServicesUpdatePNRB.setRficCode(AncillaryPostBookingType._0B5.getRficCode());
            ancillaryServicesUpdatePNRB.setSegmentIndicator(SegmentIndicator.S.toString());
            ancillaryServicesUpdatePNRB.setOwningCarrierCode(AncillaryPostBookingType._0B5.getOwningCarrierCode());

            if (null != travelerAncillary.getSeatCode()) {
                String seatCode = travelerAncillary.getSeatCode();
                if (seatCode.length() < 3) {
                    seatCode = "0" + seatCode;
                }
                ancillaryServicesUpdatePNRB.setPdcSeat(seatCode);
            }

            ancillaryServicesUpdatePNRB.setSSRCode(AncillaryPostBookingType._0B5.getSsr());

            if (isUnreserved) {
                ancillaryServicesUpdatePNRB.setBookingIndicator(travelerAncillary.getAncillary().getBookingIndicator());
                ancillaryServicesUpdatePNRB.setRefundIndicator(travelerAncillary.getAncillary().getRefundIndicator());
                ancillaryServicesUpdatePNRB.setCommisionIndicator(travelerAncillary.getAncillary().getCommisionIndicator());
                ancillaryServicesUpdatePNRB.setInterlineIndicator(travelerAncillary.getAncillary().getInterlineIndicator());
                ancillaryServicesUpdatePNRB.setFeeApplicationIndicator(travelerAncillary.getAncillary().getFeeApplicationIndicator());
                ancillaryServicesUpdatePNRB.setRefundFormIndicator(travelerAncillary.getAncillary().getRefundFormIndicator());
                ancillaryServicesUpdatePNRB.setFareGuaranteedIndicator(travelerAncillary.getAncillary().getFareGuaranteedIndicator());
                ancillaryServicesUpdatePNRB.setTicketingIndicator(travelerAncillary.getAncillary().getTicketingIndicator());
                ancillaryServicesUpdatePNRB.setEquipmentType(travelerAncillary.getAncillary().getEquipmentType());
                ancillaryServicesUpdatePNRB.setBookingSource(travelerAncillary.getAncillary().getBookingSource());
                ancillaryServicesUpdatePNRB.setTaxExemption(IndicatorType.fromValue(travelerAncillary.getAncillary().getTaxExemption()));
            } else {
                ancillaryServicesUpdatePNRB.setRefundIndicator("R");
                ancillaryServicesUpdatePNRB.setCommisionIndicator("N");
                ancillaryServicesUpdatePNRB.setInterlineIndicator("N");
                ancillaryServicesUpdatePNRB.setEmdPaperIndicator("E");
                ancillaryServicesUpdatePNRB.setFareGuaranteedIndicator("T");
            }

        } else {
            ancillaryServicesUpdatePNRB.setGroupCode(travelerAncillary.getAncillary().getGroupCode());
            ancillaryServicesUpdatePNRB.setRficCode(travelerAncillary.getAncillary().getRficCode());
            ancillaryServicesUpdatePNRB.setSegmentIndicator(travelerAncillary.getAncillary().getSegmentIndicator());
            ancillaryServicesUpdatePNRB.setOwningCarrierCode(travelerAncillary.getAncillary().getOwningCarrierCode());

            ancillaryServicesUpdatePNRB.setRefundIndicator(travelerAncillary.getAncillary().getRefundIndicator());
            ancillaryServicesUpdatePNRB.setCommisionIndicator(travelerAncillary.getAncillary().getCommisionIndicator());
            ancillaryServicesUpdatePNRB.setInterlineIndicator(travelerAncillary.getAncillary().getInterlineIndicator());

            ancillaryServicesUpdatePNRB.setPassengerTypeCode(bookedTraveler.getPaxType().getCode());
        }

        ancillaryServicesUpdatePNRB.setCommercialName(travelerAncillary.getAncillary().getCommercialName());
        ancillaryServicesUpdatePNRB.setRficSubcode(travelerAncillary.getAncillary().getRficSubcode());
        ancillaryServicesUpdatePNRB.setVendor(travelerAncillary.getAncillary().getVendor());

        ancillaryServicesUpdatePNRB.setEMDType(travelerAncillary.getAncillary().getEmdType());
        ancillaryServicesUpdatePNRB.setFeeApplicationIndicator(travelerAncillary.getAncillary().getFeeApplicationIndicator());

        if (travelerAncillary.isApplyBenefit()) {
            ancillaryServicesUpdatePNRB.setActionCode(ActionCodeType.HK.toString());
            //ancillaryServicesUpdatePNRB.setFeeWaiveReason("waive");
        } else {
            String actionCode = null == travelerAncillary.getActionCode() ? ActionCodeType.HD.toString() : travelerAncillary.getActionCode();
            ancillaryServicesUpdatePNRB.setActionCode(actionCode);
        }

        String currencyCode = travelerAncillary.getAncillary().getCurrency().getCurrencyCode();
        String equivalentCurrencyCode = travelerAncillary.getAncillary().getCurrency().getEquivalentCurrencyCode();
        AncillaryPricePNRB ancillaryPricePNRB;

        ancillaryPricePNRB = new AncillaryPricePNRB();
        ancillaryPricePNRB.setCurrency(currencyCode);
        ancillaryPricePNRB.setPrice(
                CurrencyUtil.getAmount(
                        travelerAncillary.getAncillary().getCurrency().getTotal(),
                        currencyCode
                )
        );

        ancillaryServicesUpdatePNRB.setTTLPrice(ancillaryPricePNRB);

        if (!travelerAncillary.isApplyBenefit() && 1 != travelerAncillary.getAncillary().getCurrency().getTotal().compareTo(BigDecimal.ZERO)) {
            ancillaryServicesUpdatePNRB.setActionCode(ActionCodeType.HI.toString());
        }

        //invert the base and equivalent prices
        if (null != travelerAncillary.getAncillary().getCurrency().getEquivalent()
                && 1 == travelerAncillary.getAncillary().getCurrency().getEquivalent().compareTo(BigDecimal.ZERO)) {

            ancillaryPricePNRB = new AncillaryPricePNRB();
            ancillaryPricePNRB.setCurrency(currencyCode);
            ancillaryPricePNRB.setPrice(
                    CurrencyUtil.getAmount(
                            travelerAncillary.getAncillary().getCurrency().getBase(),
                            currencyCode
                    )
            );
            ancillaryServicesUpdatePNRB.setEquivalentPrice(ancillaryPricePNRB);

            ancillaryPricePNRB = new AncillaryPricePNRB();
            ancillaryPricePNRB.setCurrency(equivalentCurrencyCode);
            ancillaryPricePNRB.setPrice(
                    CurrencyUtil.getAmount(
                            travelerAncillary.getAncillary().getCurrency().getEquivalent(),
                            equivalentCurrencyCode
                    )
            );

            ancillaryServicesUpdatePNRB.setOriginalBasePrice(ancillaryPricePNRB);

        } else {
            ancillaryPricePNRB = new AncillaryPricePNRB();
            ancillaryPricePNRB.setCurrency(currencyCode);
            ancillaryPricePNRB.setPrice(
                    CurrencyUtil.getAmount(
                            travelerAncillary.getAncillary().getCurrency().getBase(),
                            currencyCode
                    )
            );

            ancillaryServicesUpdatePNRB.setOriginalBasePrice(ancillaryPricePNRB);
        }

        if (null != travelerAncillary.getAncillary().getCurrency().getTaxDetails()
                && !travelerAncillary.getAncillary().getCurrency().getTaxDetails().isEmpty()) {

            AncillaryServicesPNRB.Taxes taxes = new AncillaryServicesPNRB.Taxes();
            AncillaryServicesPNRB.TotalTaxes totalTaxes = new AncillaryServicesPNRB.TotalTaxes();
            List<AncillaryTaxPNRB> taxesList = taxes.getTax();
            List<AncillaryTaxPNRB> totalTaxesList = totalTaxes.getTax();

            Map<String, BigDecimal> mapTaxes = travelerAncillary.getAncillary().getCurrency().getTaxDetails();
            for (Map.Entry<String, BigDecimal> entry : mapTaxes.entrySet()) {
                String key = entry.getKey();
                BigDecimal value = CurrencyUtil.getAmount(entry.getValue(), currencyCode);
                AncillaryTaxPNRB ancillaryTaxPNRB = new AncillaryTaxPNRB();
                ancillaryTaxPNRB.setTaxAmount(value);
                ancillaryTaxPNRB.setTaxCode(key);
                taxesList.add(ancillaryTaxPNRB);
                totalTaxesList.add(ancillaryTaxPNRB);
            }

            ancillaryServicesUpdatePNRB.setTotalTaxes(totalTaxes);
            ancillaryServicesUpdatePNRB.setTaxes(taxes);
            ancillaryServicesUpdatePNRB.setTaxesIncluded(Boolean.TRUE);
        }

        ancillaryServicesUpdatePNRB.setNumberOfItems(String.valueOf(travelerAncillary.getQuantity()));

        return ancillaryServicesUpdatePNRB;
    }

    /**
     * @param bookedTraveler
     * @param travelerAncillary
     * @param bookedLeg
     * @param pnrLocator
     * @param store
     * @param travelerAncillariesAdded
     * @return
     */
    public static UpdateReservationRQ getCreateReservationRQ(
            BookedTraveler bookedTraveler,
            TravelerAncillary travelerAncillary,
            BookedLeg bookedLeg,
            String pnrLocator,
            String store,
            List<TravelerAncillary> travelerAncillariesAdded
    ) {

        travelerAncillary.setNameNumber(bookedTraveler.getNameRefNumber());

        UpdateReservationRQ updateReservationRQ = new UpdateReservationRQ();
        updateReservationRQ.setPartition(PARTITION);

        // RequestType
        RequestType requestType = new RequestType();
        requestType.setValue(RequestEnumerationType.STATELESS);
        updateReservationRQ.setRequestType(requestType);
        updateReservationRQ.setVersion(VERSION);

        ReservationUpdateListType reservationUpdateListType = new ReservationUpdateListType();

        LocatorWithPartitionType locatorWithPartitionType = new LocatorWithPartitionType();
        locatorWithPartitionType.setValue(pnrLocator);

        reservationUpdateListType.setLocator(locatorWithPartitionType);

        List<ReservationUpdateItemType> reservationUpdateItemTypeList = reservationUpdateListType.getReservationUpdateItem();

        int itemUpdateId = 1;

        travelerAncillary.setAncillaryId(String.valueOf(itemUpdateId));
        travelerAncillariesAdded.add(travelerAncillary);

        ReservationUpdateItemType reservationUpdateItemType = new ReservationUpdateItemType();
        reservationUpdateItemType.setUpdateId(String.valueOf(itemUpdateId));
        itemUpdateId++;

        List<SegmentAssociationTagPNRB> segmentAssociationTagPNRB;
        try {
            segmentAssociationTagPNRB = getItineraryInfo(bookedTraveler, bookedLeg, travelerAncillary);
        } catch (DatatypeConfigurationException | ParseException ex) {
            segmentAssociationTagPNRB = null;
        }

        boolean isUnreserved = false;
        reservationUpdateItemType.setAncillaryServicesUpdate(
                getAncillaryItem(
                        bookedTraveler,
                        travelerAncillary,
                        segmentAssociationTagPNRB,
                        store,
                        isUnreserved
                )
        );

        reservationUpdateItemTypeList.add(reservationUpdateItemType);

        // Received from
        ReceivedFromPNRB ReceivedFromPNRB = new ReceivedFromPNRB();
        ReceivedFromPNRB.setAgentName(AGENT_NAME);
        reservationUpdateListType.setReceivedFrom(ReceivedFromPNRB);

        updateReservationRQ.setReservationUpdateList(reservationUpdateListType);

        ReturnOptions returnOptions = new ReturnOptions();
        returnOptions.setIncludeUpdateDetails(true);
        returnOptions.setRetrievePNR(true);
        updateReservationRQ.setReturnOptions(returnOptions);

        return updateReservationRQ;
    }

    /**
     * @param bookedTravelerList
     * @param bookedLeg
     * @param marketType
     * @param pnrLocator
     * @param store
     * @param travelerAncillariesAdded
     * @return
     */
    public static UpdateReservationRQ getCreateReservationRQ(
            List<BookedTraveler> bookedTravelerList,
            BookedLeg bookedLeg,
            MarketType marketType,
            String pnrLocator,
            String store,
            List<TravelerAncillary> travelerAncillariesAdded
    ) {

        UpdateReservationRQ updateReservationRQ = new UpdateReservationRQ();
        updateReservationRQ.setPartition(PARTITION);

        // RequestType
        RequestType requestType = new RequestType();
        requestType.setValue(RequestEnumerationType.STATELESS);
        updateReservationRQ.setRequestType(requestType);

        updateReservationRQ.setVersion(VERSION);

        ReservationUpdateListType reservationUpdateListType = new ReservationUpdateListType();

        LocatorWithPartitionType locatorWithPartitionType = new LocatorWithPartitionType();
        locatorWithPartitionType.setValue(pnrLocator);

        reservationUpdateListType.setLocator(locatorWithPartitionType);

        List<ReservationUpdateItemType> reservationUpdateItemTypeList;
        reservationUpdateItemTypeList = reservationUpdateListType.getReservationUpdateItem();

        boolean isUnreserved = false;

        int itemUpdateId = 1;
        for (BookedTraveler bookedTraveler : bookedTravelerList) {
            List<AbstractAncillary> allAncillaries = PurchaseOrderUtil.getAllAncillaries(bookedTraveler);

            for (TravelerAncillary travelerAncillary : PurchaseOrderUtil.getNewAncillaries(allAncillaries)) {
                travelerAncillary.setNameNumber(bookedTraveler.getNameRefNumber());
                bookedTraveler.setTouched(true);

                travelerAncillary.setAncillaryId(String.valueOf(itemUpdateId));
                travelerAncillariesAdded.add(travelerAncillary);

                ReservationUpdateItemType reservationUpdateItemType = new ReservationUpdateItemType();
                reservationUpdateItemType.setUpdateId(String.valueOf(itemUpdateId));
                itemUpdateId++;

                List<SegmentAssociationTagPNRB> segmentAssociationTagPNRB;
                try {
                    segmentAssociationTagPNRB = getItineraryInfo(bookedTraveler, bookedLeg, travelerAncillary);
                } catch (DatatypeConfigurationException | ParseException ex) {
                    segmentAssociationTagPNRB = null;
                }

                AncillaryUtil.convertCodeAncillary_2_0(travelerAncillary, marketType);

                reservationUpdateItemType.setAncillaryServicesUpdate(
                        getAncillaryItem(
                                bookedTraveler,
                                travelerAncillary,
                                segmentAssociationTagPNRB,
                                store,
                                isUnreserved
                        )
                );

                //Revertir el nuevo codigo de sabre con el codigo anterior
                AncillaryPostBookingType oldAnc = AncillaryUtil.convertNewCodeToOldCode(travelerAncillary.getAncillary().getRficSubcode());
                if (null != oldAnc) {
                    //Revertir el nuevo codigo de sabre con el codigo anterior
                    String code = oldAnc.getType();
                    travelerAncillary.getAncillary().setRficSubcode(code);
                    travelerAncillary.getAncillary().setType(code);
                }

                reservationUpdateItemTypeList.add(reservationUpdateItemType);
            }
        }

        // Received from
        ReceivedFromPNRB ReceivedFromPNRB = new ReceivedFromPNRB();
        ReceivedFromPNRB.setAgentName(AGENT_NAME);
        reservationUpdateListType.setReceivedFrom(ReceivedFromPNRB);

        updateReservationRQ.setReservationUpdateList(reservationUpdateListType);

        ReturnOptions returnOptions = new ReturnOptions();
        returnOptions.setIncludeUpdateDetails(true);
        returnOptions.setRetrievePNR(true);
        updateReservationRQ.setReturnOptions(returnOptions);

        return updateReservationRQ;
    }

    /**
     * @param bookedTravelerList
     * @param bookedLeg
     * @param pnrLocator
     * @param store
     * @param extraWeightAncillariesAdded
     * @return
     */
    public static UpdateReservationRQ getCreateReservationRQForExtraWeight(
            List<BookedTraveler> bookedTravelerList,
            BookedLeg bookedLeg,
            String pnrLocator,
            String store,
            List<ExtraWeightAncillary> extraWeightAncillariesAdded
    ) {

        UpdateReservationRQ updateReservationRQ = new UpdateReservationRQ();
        updateReservationRQ.setPartition(PARTITION);

        // RequestType
        RequestType requestType = new RequestType();
        requestType.setValue(RequestEnumerationType.STATELESS);
        updateReservationRQ.setRequestType(requestType);

        updateReservationRQ.setVersion(VERSION);

        ReservationUpdateListType reservationUpdateListType = new ReservationUpdateListType();

        LocatorWithPartitionType locatorWithPartitionType = new LocatorWithPartitionType();
        locatorWithPartitionType.setValue(pnrLocator);

        reservationUpdateListType.setLocator(locatorWithPartitionType);

        List<ReservationUpdateItemType> reservationUpdateItemTypeList;
        reservationUpdateItemTypeList = reservationUpdateListType.getReservationUpdateItem();

        int itemUpdateId = 1;
        for (BookedTraveler bookedTraveler : bookedTravelerList) {

            for (ExtraWeightAncillary extraWeightAncillary : PurchaseOrderUtil.getExtraWeightUnpaidAncillaries(bookedTraveler.getAncillaries().getCollection())) {
                extraWeightAncillary.setNameNumber(bookedTraveler.getNameRefNumber());

                bookedTraveler.setTouched(true);

                extraWeightAncillary.setAncillaryId(String.valueOf(itemUpdateId));
                extraWeightAncillariesAdded.add(extraWeightAncillary);

                ReservationUpdateItemType reservationUpdateItemType = new ReservationUpdateItemType();
                reservationUpdateItemType.setUpdateId(String.valueOf(itemUpdateId));
                itemUpdateId++;

                List<SegmentAssociationTagPNRB> segmentAssociationTagPNRB;
                try {
                    segmentAssociationTagPNRB = getItineraryInfo(bookedTraveler, bookedLeg);
                } catch (DatatypeConfigurationException | ParseException ex) {
                    segmentAssociationTagPNRB = null;
                }

                reservationUpdateItemType.setAncillaryServicesUpdate(
                        getAncillaryItemForExtraWeight(
                                bookedTraveler, extraWeightAncillary,
                                segmentAssociationTagPNRB,
                                store
                        )
                );

                reservationUpdateItemTypeList.add(reservationUpdateItemType);
            }
        }

        // Received from
        ReceivedFromPNRB ReceivedFromPNRB = new ReceivedFromPNRB();
        ReceivedFromPNRB.setAgentName(AGENT_NAME);
        reservationUpdateListType.setReceivedFrom(ReceivedFromPNRB);

        updateReservationRQ.setReservationUpdateList(reservationUpdateListType);

        ReturnOptions returnOptions = new ReturnOptions();
        returnOptions.setIncludeUpdateDetails(true);
        returnOptions.setRetrievePNR(true);
        updateReservationRQ.setReturnOptions(returnOptions);

        return updateReservationRQ;
    }

    /**
     * @param bookedTravelerList
     * @param cabinUpgradeAncillaryAdded
     * @param upgradeAncillaryList
     * @param bookedLeg
     * @param recordLocator
     * @param store
     * @return
     */
    public static UpdateReservationRQ getCreateReservationRQForUpgrades(
            List<BookedTraveler> bookedTravelerList,
            List<CabinUpgradeAncillary> cabinUpgradeAncillaryAdded,
            List<UpgradeAncillary> upgradeAncillaryList,
            BookedLeg bookedLeg,
            String recordLocator,
            String store
    ) {

        UpdateReservationRQ updateReservationRQ = new UpdateReservationRQ();
        updateReservationRQ.setPartition(PARTITION);

        // RequestType
        RequestType requestType = new RequestType();

        requestType.setValue(RequestEnumerationType.STATEFUL);

        updateReservationRQ.setRequestType(requestType);

        updateReservationRQ.setVersion(VERSION);

        ReservationUpdateListType reservationUpdateListType = new ReservationUpdateListType();

        LocatorWithPartitionType locatorWithPartitionType = new LocatorWithPartitionType();
        locatorWithPartitionType.setValue(recordLocator);

        reservationUpdateListType.setLocator(locatorWithPartitionType);

        List<ReservationUpdateItemType> reservationUpdateItemTypeList = reservationUpdateListType.getReservationUpdateItem();

        boolean isUnreserved = false;

        int itemUpdateId = 1;
        for (BookedTraveler bookedTraveler : bookedTravelerList) {

            List<CabinUpgradeAncillary> cabinUpgradeAncillaryList;
            cabinUpgradeAncillaryList = PurchaseOrderUtil.getUpgradeUnpaidAncillaries(
                    bookedTraveler.getAncillaries().getCollection()
            );

            for (CabinUpgradeAncillary cabinUpgradeAncillary : cabinUpgradeAncillaryList) {
                cabinUpgradeAncillary.setNameNumber(bookedTraveler.getNameRefNumber());

                UpgradeAncillary upgradeAncillary;
                upgradeAncillary = PurchaseOrderUtil.getUpgradeAncillary(
                        cabinUpgradeAncillary.getSegmentCode(), upgradeAncillaryList
                );

                AncillaryOffer ancillaryOfferDB;
                if (null != upgradeAncillary) {
                    ancillaryOfferDB = upgradeAncillary.getAncillaryOffer();
                } else {
                    ancillaryOfferDB = null;
                }

                TravelerAncillary travelerAncillary;
                travelerAncillary = PurchaseOrderUtil.createUpgradeAncillary(
                        bookedTraveler, cabinUpgradeAncillary, ancillaryOfferDB
                );

                bookedTraveler.setTouched(true);

                cabinUpgradeAncillary.setAncillaryId(String.valueOf(itemUpdateId));
                cabinUpgradeAncillaryAdded.add(cabinUpgradeAncillary);

                ReservationUpdateItemType reservationUpdateItemType = new ReservationUpdateItemType();
                reservationUpdateItemType.setUpdateId(String.valueOf(itemUpdateId));
                itemUpdateId++;

                List<SegmentAssociationTagPNRB> segmentAssociationTagPNRB;
                try {
                    if (null == bookedLeg
                            || null == bookedLeg.getSegments()
                            || null == bookedLeg.getSegments().getCollection()
                            || bookedLeg.getSegments().getCollection().isEmpty()) {

                        segmentAssociationTagPNRB = new ArrayList<>();

                    } else {
                        segmentAssociationTagPNRB = getItineraryInfo(bookedLeg, cabinUpgradeAncillary, "F");
                    }
                } catch (DatatypeConfigurationException | ParseException ex) {
                    segmentAssociationTagPNRB = null;
                }

                reservationUpdateItemType.setAncillaryServicesUpdate(
                        getAncillaryItem(
                                bookedTraveler,
                                travelerAncillary,
                                segmentAssociationTagPNRB,
                                store,
                                isUnreserved
                        )
                );

                reservationUpdateItemTypeList.add(reservationUpdateItemType);
            }
        }

        // Received from
        ReceivedFromPNRB ReceivedFromPNRB = new ReceivedFromPNRB();
        ReceivedFromPNRB.setAgentName(AGENT_NAME);
        reservationUpdateListType.setReceivedFrom(ReceivedFromPNRB);

        updateReservationRQ.setReservationUpdateList(reservationUpdateListType);

        ReturnOptions returnOptions = new ReturnOptions();
        returnOptions.setIncludeUpdateDetails(true);
        returnOptions.setRetrievePNR(true);
        updateReservationRQ.setReturnOptions(returnOptions);

        return updateReservationRQ;
    }

    /**
     * @param bookedTraveler
     * @param bookedLeg
     * @param pnrLocator
     * @param ancillaryAdded
     * @param bookedSegment
     * @return
     * @throws Exception
     */
    public static UpdateReservationRQ getCreateReservationRQForFirstBag(
            BookedTraveler bookedTraveler,
            BookedLeg bookedLeg,
            String pnrLocator,
            Ancillary ancillaryAdded,
            BookedSegment bookedSegment
    ) throws Exception {

        UpdateReservationRQ updateReservationRQ = new UpdateReservationRQ();
        updateReservationRQ.setPartition(PARTITION);

        RequestType requestType = new RequestType();

        requestType.setValue(RequestEnumerationType.STATELESS);

        updateReservationRQ.setRequestType(requestType);

        updateReservationRQ.setVersion(VERSION);

        ReservationUpdateListType reservationUpdateListType = new ReservationUpdateListType();

        LocatorWithPartitionType locatorWithPartitionType = new LocatorWithPartitionType();
        locatorWithPartitionType.setValue(pnrLocator);

        reservationUpdateListType.setLocator(locatorWithPartitionType);

        List<ReservationUpdateItemType> reservationUpdateItemTypeList = reservationUpdateListType.getReservationUpdateItem();

        ReservationUpdateItemType reservationUpdateItemType = new ReservationUpdateItemType();

//        PhoneNumberUpdatePNRB phoneNumberUpdatePNRB = new PhoneNumberUpdatePNRB();        
//        reservationUpdateItemType.setPhoneNumberUpdate(phoneNumberUpdatePNRB);
        int itemUpdateId = 1;
        reservationUpdateItemType.setUpdateId(String.valueOf(itemUpdateId));

        AncillaryServicesUpdatePNRB ancillaryServicesUpdatePNRB = new AncillaryServicesUpdatePNRB();
        ancillaryServicesUpdatePNRB.setOp(OperationTypePNRB.C);
        ancillaryServicesUpdatePNRB.setNameAssociationList(AncillaryWaivedUtil.getNameAssociationList(bookedTraveler));

        ancillaryServicesUpdatePNRB.setSegmentAssociationList(
                AncillaryWaivedUtil.getSegmentAssociationList(
                        bookedSegment, bookedTraveler.getBookingClasses()
                )
        );
        ancillaryServicesUpdatePNRB.setCommercialName(ancillaryAdded.getCommercialName());
        ancillaryServicesUpdatePNRB.setRficCode(ancillaryAdded.getRficCode());
        ancillaryServicesUpdatePNRB.setRficSubcode(ancillaryAdded.getRficSubcode());
        ancillaryServicesUpdatePNRB.setOwningCarrierCode(ancillaryAdded.getOwningCarrierCode());
        ancillaryServicesUpdatePNRB.setVendor(ancillaryAdded.getVendor());
        ancillaryServicesUpdatePNRB.setEMDType(ancillaryAdded.getEmdType());
        ancillaryServicesUpdatePNRB.setOriginalBasePrice(AncillaryWaivedUtil.getOriginalBasePrice());

        ancillaryServicesUpdatePNRB.setFeeApplicationIndicator(ancillaryAdded.getFeeApplicationIndicator());
        ancillaryServicesUpdatePNRB.setNumberOfItems("1");
        ancillaryServicesUpdatePNRB.setActionCode("HK");
        ancillaryServicesUpdatePNRB.setSegmentIndicator(ancillaryAdded.getSegmentIndicator());
        ancillaryServicesUpdatePNRB.setGroupCode(ancillaryAdded.getGroupCode());

        reservationUpdateItemType.setAncillaryServicesUpdate(ancillaryServicesUpdatePNRB);

        reservationUpdateItemTypeList.add(reservationUpdateItemType);

        // Received from
        ReceivedFromPNRB ReceivedFromPNRB = new ReceivedFromPNRB();
        ReceivedFromPNRB.setAgentName("WAIVEWCK");
        reservationUpdateListType.setReceivedFrom(ReceivedFromPNRB);

        updateReservationRQ.setReservationUpdateList(reservationUpdateListType);

        ReturnOptions returnOptions = new ReturnOptions();
        returnOptions.setIncludeUpdateDetails(true);
        returnOptions.setRetrievePNR(true);
        updateReservationRQ.setReturnOptions(returnOptions);

        return updateReservationRQ;
    }

    /**
     * @param bookedTravelerList
     * @param bookedLeg
     * @param pnrLocator
     * @param store
     * @param travelerAncillariesAdded
     * @return
     */
    public static UpdateReservationRQ getCreateReservationRQForUnreservedSeats(
            List<BookedTraveler> bookedTravelerList,
            BookedLeg bookedLeg,
            String pnrLocator,
            String store,
            List<TravelerAncillary> travelerAncillariesAdded
    ) {

        UpdateReservationRQ updateReservationRQ = new UpdateReservationRQ();
        updateReservationRQ.setPartition(PARTITION);

        // RequestType
        RequestType requestType = new RequestType();
        requestType.setValue(RequestEnumerationType.STATELESS);
        updateReservationRQ.setRequestType(requestType);
        updateReservationRQ.setVersion(VERSION);

        ReservationUpdateListType reservationUpdateListType = new ReservationUpdateListType();

        LocatorWithPartitionType locatorWithPartitionType = new LocatorWithPartitionType();
        locatorWithPartitionType.setValue(pnrLocator);

        reservationUpdateListType.setLocator(locatorWithPartitionType);

        List<ReservationUpdateItemType> reservationUpdateItemTypeList = reservationUpdateListType.getReservationUpdateItem();

        boolean isUnreserved = true;

        int itemUpdateId = 1;
        for (BookedTraveler bookedTraveler : bookedTravelerList) {

            for (TravelerAncillary travelerAncillary : PurchaseOrderUtil.getFreeAncillaries(bookedTraveler.getSeatAncillaries().getCollection())) {
                travelerAncillary.setNameNumber(bookedTraveler.getNameRefNumber());

                AbstractSegmentChoice abstractSegmentChoice;
                abstractSegmentChoice = PurchaseOrderUtil.getSeatEntryBySegmentCode(
                        travelerAncillary.getSegmentCodeAux(), bookedTraveler.getSegmentChoices().getCollection()
                );

                if (null != abstractSegmentChoice) {
                    abstractSegmentChoice.setAncillaryId(String.valueOf(itemUpdateId));
                }

                travelerAncillary.setAncillaryId(String.valueOf(itemUpdateId));
                travelerAncillariesAdded.add(travelerAncillary);

                ReservationUpdateItemType reservationUpdateItemType = new ReservationUpdateItemType();
                reservationUpdateItemType.setUpdateId(String.valueOf(itemUpdateId));
                itemUpdateId++;

                List<SegmentAssociationTagPNRB> segmentAssociationTagPNRB;
                try {
                    segmentAssociationTagPNRB = getItineraryInfo(bookedTraveler, bookedLeg, travelerAncillary);
                } catch (DatatypeConfigurationException | ParseException ex) {
                    segmentAssociationTagPNRB = null;
                }

                reservationUpdateItemType.setAncillaryServicesUpdate(
                        getAncillaryItem(
                                bookedTraveler,
                                travelerAncillary,
                                segmentAssociationTagPNRB,
                                store,
                                isUnreserved
                        )
                );

                reservationUpdateItemTypeList.add(reservationUpdateItemType);
            }
        }

        // Received from
        ReceivedFromPNRB ReceivedFromPNRB = new ReceivedFromPNRB();
        ReceivedFromPNRB.setAgentName(AGENT_NAME);
        reservationUpdateListType.setReceivedFrom(ReceivedFromPNRB);

        updateReservationRQ.setReservationUpdateList(reservationUpdateListType);

        ReturnOptions returnOptions = new ReturnOptions();
        returnOptions.setIncludeUpdateDetails(true);
        returnOptions.setRetrievePNR(true);
        updateReservationRQ.setReturnOptions(returnOptions);

        return updateReservationRQ;
    }

    /**
     * @param bookedTravelerList
     * @param recordLocator
     * @return
     */
    public static UpdateReservationRQ getDeleteReservationRQToChangeSeats(
            List<BookedTraveler> bookedTravelerList, String recordLocator
    ) {
        UpdateReservationRQ updateReservationRQ = new UpdateReservationRQ();

        // RequestType
        RequestType requestType = new RequestType();
        requestType.setValue(RequestEnumerationType.STATELESS);
        updateReservationRQ.setRequestType(requestType);
        updateReservationRQ.setVersion(VERSION);

        // ReservationUpdateList
        ReservationUpdateListType reservationUpdateListType = new ReservationUpdateListType();

        //// Record Locator
        LocatorWithPartitionType locatorWithPartitionType = new LocatorWithPartitionType();
        locatorWithPartitionType.setValue(recordLocator);
        reservationUpdateListType.setLocator(locatorWithPartitionType);

        int updateId = 1;
        for (BookedTraveler bookedTraveler : bookedTravelerList) {

            Map<String, List<AbstractAncillary>> ancillariesMap = bookedTraveler.getOriginalSeatAncillariesMap().getMap();

            if (null != ancillariesMap && !ancillariesMap.isEmpty()) {
                for (Map.Entry<String, List<AbstractAncillary>> entry : ancillariesMap.entrySet()) {
                    String key = entry.getKey();
                    List<AbstractAncillary> abstractAncillaryList = entry.getValue();

                    if (null != abstractAncillaryList) {
                        for (AbstractAncillary abstractAncillary : abstractAncillaryList) {
                            String ancillaryId = null;
                            boolean isPartOfReservation = false;

                            if (abstractAncillary instanceof TravelerAncillary) {
                                ancillaryId = ((TravelerAncillary) abstractAncillary).getAncillaryId();
                                isPartOfReservation = ((TravelerAncillary) abstractAncillary).isPartOfReservation();
                            } else if (abstractAncillary instanceof PaidTravelerAncillary) {
                                ancillaryId = ((PaidTravelerAncillary) abstractAncillary).getAncillaryId();
                                isPartOfReservation = ((PaidTravelerAncillary) abstractAncillary).isPartOfReservation();
                            } else if (abstractAncillary instanceof TicketedTravelerAncillary) {
                                ancillaryId = ((TicketedTravelerAncillary) abstractAncillary).getAncillaryId();
                                isPartOfReservation = ((TicketedTravelerAncillary) abstractAncillary).isPartOfReservation();
                            }

                            if (null != ancillaryId && !ancillaryId.trim().isEmpty() && isPartOfReservation) {

                                ReservationUpdateItemType reservationUpdateItemType;
                                reservationUpdateItemType = AEUtil.getDeleteItem(updateId, ancillaryId, bookedTraveler);

                                reservationUpdateListType.getReservationUpdateItem().add(reservationUpdateItemType);

                                updateId++;
                            }
                        }
                    }
                }
            }
        }

        // Received from
        ReceivedFromPNRB ReceivedFromPNRB = new ReceivedFromPNRB();
        ReceivedFromPNRB.setAgentName(AGENT_NAME);
        reservationUpdateListType.setReceivedFrom(ReceivedFromPNRB);

        updateReservationRQ.setReservationUpdateList(reservationUpdateListType);

        ReturnOptions returnOptions = new ReturnOptions();
        returnOptions.setIncludeUpdateDetails(true);
        returnOptions.setRetrievePNR(true);
        updateReservationRQ.setReturnOptions(returnOptions);

        return updateReservationRQ;
    }

    /**
     * @param bookedTraveler
     * @param ancillaryId
     * @param recordLocator
     * @return
     */
    public static UpdateReservationRQ getDeleteReservationRQ(
            BookedTraveler bookedTraveler, String ancillaryId, String recordLocator
    ) {

        //Name Association
        NameAssociationTagPNRB nameAssociationTagPNRB;
        nameAssociationTagPNRB = new NameAssociationTagPNRB();
        nameAssociationTagPNRB.setFirstName(bookedTraveler.getFirstName());
        nameAssociationTagPNRB.setLastName(bookedTraveler.getLastName());
        nameAssociationTagPNRB.setNameRefNumber(bookedTraveler.getNameRefNumber());

        //Name Association List
        NameAssociationListPNRB nameAssociationListPNRB;
        nameAssociationListPNRB = new NameAssociationListPNRB();
        nameAssociationListPNRB.getNameAssociationTag().add(nameAssociationTagPNRB);

        //Ancillary
        AncillaryServicesUpdatePNRB ancillaryServicesUpdatePNRB;
        ancillaryServicesUpdatePNRB = new AncillaryServicesUpdatePNRB();
        ancillaryServicesUpdatePNRB.setId(ancillaryId);
        ancillaryServicesUpdatePNRB.setOp(OperationTypePNRB.D);
        ancillaryServicesUpdatePNRB.setNameAssociationList(nameAssociationListPNRB);

        //Reservation Update Item
        ReservationUpdateItemType reservationUpdateItemType;
        reservationUpdateItemType = new ReservationUpdateItemType();
        reservationUpdateItemType.setUpdateId("1");
        reservationUpdateItemType.setAncillaryServicesUpdate(ancillaryServicesUpdatePNRB);

        //Update items
        List<ReservationUpdateItemType> reservationUpdateItemTypeList;
        reservationUpdateItemTypeList = new ArrayList<>();
        reservationUpdateItemTypeList.add(reservationUpdateItemType);

        UpdateReservationRQ updateReservationRQ;
        updateReservationRQ = getUpdateReservationRQ(recordLocator, reservationUpdateItemTypeList);

        return updateReservationRQ;
    }

    /**
     * @param recordLocator
     * @param reservationUpdateItemTypeList
     * @return
     */
    public static UpdateReservationRQ getUpdateReservationRQ(
            String recordLocator, List<ReservationUpdateItemType> reservationUpdateItemTypeList
    ) {
        return getUpdateReservationRQ(
                recordLocator, reservationUpdateItemTypeList, RequestEnumerationType.STATELESS
        );
    }

    /**
     * @param recordLocator
     * @param reservationUpdateItemTypeList
     * @param requestEnumerationType
     * @return
     */
    public static UpdateReservationRQ getUpdateReservationRQ(
            String recordLocator, List<ReservationUpdateItemType> reservationUpdateItemTypeList,
            RequestEnumerationType requestEnumerationType
    ) {
        //Request Type
        RequestType requestType;
        requestType = new RequestType();
        requestType.setValue(requestEnumerationType);
        //Return Options
        ReturnOptions returnOptions;
        returnOptions = new ReturnOptions();
        returnOptions.setIncludeUpdateDetails(true);
        returnOptions.setRetrievePNR(true);
        //Record Locator
        LocatorWithPartitionType locatorWithPartitionType;
        locatorWithPartitionType = new LocatorWithPartitionType();
        locatorWithPartitionType.setValue(recordLocator);
        //Received From
        ReceivedFromPNRB ReceivedFromPNRB;
        ReceivedFromPNRB = new ReceivedFromPNRB();
        ReceivedFromPNRB.setAgentName(AGENT_NAME);
        //Update Items
        ReservationUpdateListType reservationUpdateListType;
        reservationUpdateListType = new ReservationUpdateListType();
        reservationUpdateListType.setLocator(locatorWithPartitionType);
        reservationUpdateListType.getReservationUpdateItem().addAll(reservationUpdateItemTypeList);
        reservationUpdateListType.setReceivedFrom(ReceivedFromPNRB);
        //Update Reservation RQ
        UpdateReservationRQ updateReservationRQ;
        updateReservationRQ = new UpdateReservationRQ();
        updateReservationRQ.setPartition(PARTITION);
        updateReservationRQ.setVersion(VERSION);
        updateReservationRQ.setRequestType(requestType);
        updateReservationRQ.setReservationUpdateList(reservationUpdateListType);
        updateReservationRQ.setReturnOptions(returnOptions);
        return updateReservationRQ;
    }

    /**
     * @param actionCode
     * @param ancillaryId
     * @param itemUpdateId
     * @return
     */
    public static ReservationUpdateItemType getUpdateReservationActionCodeItem(
            ActionCodeType actionCode, String ancillaryId, int itemUpdateId
    ) {
        AncillaryServiceActionCodeUpdatePNRB ancillaryServiceActionCodeUpdatePNRB;
        ancillaryServiceActionCodeUpdatePNRB = new AncillaryServiceActionCodeUpdatePNRB();
        ancillaryServiceActionCodeUpdatePNRB.setCurrentActionCode(actionCode.toString());
        AncillaryServicesPartialUpdatePNRB ancillaryServicesUpdatePNRB;
        ancillaryServicesUpdatePNRB = new AncillaryServicesPartialUpdatePNRB();
        ancillaryServicesUpdatePNRB.setOp(OperationTypePNRB.U);
        ancillaryServicesUpdatePNRB.setId(ancillaryId);
        ancillaryServicesUpdatePNRB.setActionCodeUpdate(ancillaryServiceActionCodeUpdatePNRB);
        ReservationUpdateItemType reservationUpdateItemType;
        reservationUpdateItemType = new ReservationUpdateItemType();
        reservationUpdateItemType.setUpdateId(String.valueOf(itemUpdateId));
        reservationUpdateItemType.setAncillaryServicesPartialUpdate(ancillaryServicesUpdatePNRB);
        return reservationUpdateItemType;
    }

    /**
     * @param abstractAncillaryList
     * @param actionCode
     * @return
     */
    public static List<ReservationUpdateItemType> getUpdateReservationActionCodeItemList(
            List<? extends AbstractAncillary> abstractAncillaryList, ActionCodeType actionCode
    ) {

        List<ReservationUpdateItemType> reservationUpdateItemTypeList = new ArrayList<>();
        int itemUpdateId = 1;
        for (AbstractAncillary abstractAncillary : abstractAncillaryList) {
            String actionCodeLocal;
            String ancillaryIdLocal;

            if (abstractAncillary instanceof TravelerAncillary) {
                TravelerAncillary travelerAncillary = (TravelerAncillary) abstractAncillary;
                actionCodeLocal = travelerAncillary.getActionCode();
                ancillaryIdLocal = travelerAncillary.getAncillaryId();
            } else if (abstractAncillary instanceof CabinUpgradeAncillary) {
                CabinUpgradeAncillary cabinUpgradeAncillary = (CabinUpgradeAncillary) abstractAncillary;
                actionCodeLocal = cabinUpgradeAncillary.getActionCode();
                ancillaryIdLocal = cabinUpgradeAncillary.getAncillaryId();
            } else if (abstractAncillary instanceof ExtraWeightAncillary) {
                ExtraWeightAncillary extraWeightAncillary = (ExtraWeightAncillary) abstractAncillary;
                actionCodeLocal = extraWeightAncillary.getActionCode();
                ancillaryIdLocal = extraWeightAncillary.getAncillaryId();
            } else {
                actionCodeLocal = null;
                ancillaryIdLocal = null;
            }

            if (null != ancillaryIdLocal && !actionCode.toString().equalsIgnoreCase(actionCodeLocal)) {
                ReservationUpdateItemType reservationUpdateItemType;
                reservationUpdateItemType = getUpdateReservationActionCodeItem(actionCode, ancillaryIdLocal, itemUpdateId);

                reservationUpdateItemTypeList.add(reservationUpdateItemType);

                itemUpdateId++;
            }
        }

        return reservationUpdateItemTypeList;
    }

    /**
     * @param ancillaryId
     * @param recordLocator
     * @return
     */
    public static ReservationUpdateDeleteItem getDeleteReservationItem(
            String ancillaryId, String recordLocator
    ) {
        ReservationUpdateDeleteItem reservationUpdateDeleteItem = new ReservationUpdateDeleteItem();
        reservationUpdateDeleteItem.setAgentName(AGENT_NAME);
        reservationUpdateDeleteItem.setAncillaryServicesUpdateId(ancillaryId);
        reservationUpdateDeleteItem.setLocator(recordLocator);
        reservationUpdateDeleteItem.setReservationUpdateItemId("1");
        return reservationUpdateDeleteItem;
    }

    /**
     * This method check the response from update reservation service and
     * validate it. This method will throw an exception if something was wrong.
     *
     * @param updateReservationRS
     * @throws GenericException
     */
    public static void checkUpdateReservationErrors(UpdateReservationRS updateReservationRS) throws GenericException {
        if (!"OK".equalsIgnoreCase(updateReservationRS.getSuccess())
                || (null != updateReservationRS.getErrors()
                && null != updateReservationRS.getErrors().getError()
                && !updateReservationRS.getErrors().getError().isEmpty())
                || (null != updateReservationRS.getWarnings()
                && null != updateReservationRS.getWarnings().getWarning()
                && !updateReservationRS.getWarnings().getWarning().isEmpty())) {

            String msg = "";

            if (null != updateReservationRS.getErrors() && null != updateReservationRS.getErrors().getError()) {
                List<UpdateErrorsPNRB.Error> updateErrorList = updateReservationRS.getErrors().getError();
                for (UpdateErrorsPNRB.Error updateError : updateErrorList) {
                    msg = msg + "UpdateId: " + updateError.getUpdateId() + ", message: "
                            + updateError.getMessage() + " , ";
                }
            }

            if (null != updateReservationRS.getWarnings() && null != updateReservationRS.getWarnings().getWarning()) {
                List<UpdateWarningsPNRB.Warning> updateWarningList = updateReservationRS.getWarnings().getWarning();
                for (UpdateWarningsPNRB.Warning updateWarning : updateWarningList) {
                    msg = msg + "UpdateId: " + updateWarning.getUpdateId() + ", message: "
                            + updateWarning.getMessage() + " , ";
                }
            }

            if (!msg.isEmpty()) {
                msg = msg.substring(0, msg.length() - 3);
            }

            if (StringUtils.contains(msg, ErrorType.ETICKET_COUPON_INVALID_FORMAT.getSearchableCode())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.ETICKET_COUPON_INVALID_FORMAT, msg);
            }

            if (StringUtils.contains(msg, ErrorType.AE_WAS_NOT_CREATED.getSearchableCode())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.AE_WAS_NOT_CREATED, msg);
            }

            if (StringUtils.contains(msg, ErrorType.DUPLICATE_AE_DATA.getSearchableCode())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.DUPLICATE_AE_DATA, msg);
            }

            //"UNKNOWN UPDATE RESERVATION ERROR[116] CHECK PDCSEAT"
            if (StringUtils.contains(msg, ErrorType.UNKNOWN_UPDATE_RESERVATION_ERROR_CHECK_PDCSEAT.getSearchableCode())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNKNOWN_UPDATE_RESERVATION_ERROR_CHECK_PDCSEAT, msg);
            }

            if (StringUtils.contains(msg.toUpperCase(), ErrorType.RESERVATION_ORCHESTRATION_ERROR_CAUSED_BY.getSearchableCode()
                    .replace("_", " "))) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.RESERVATION_ORCHESTRATION_ERROR_CAUSED_BY, msg);
            }

            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNKNOWN_CODE, msg);
        }

//        if (null == updateReservationRS.getResults() || null == updateReservationRS.getResults().getUpdateResult() || updateReservationRS.getResults().getUpdateResult().isEmpty()) {
//            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UPDATE_RESERVATION_RESULT_EMPTY);
//        }
    }

}
