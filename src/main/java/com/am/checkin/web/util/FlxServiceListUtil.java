package com.am.checkin.web.util;

import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.BookedTravelerBenefit;
import com.aeromexico.commons.model.BookedTravelerCollection;
import com.aeromexico.commons.model.CabinCapacity;
import com.aeromexico.commons.model.CurrencyValue;
import com.aeromexico.commons.model.FlxPriceWrapper;
import com.aeromexico.commons.model.FreeBaggageAllowanceDetailByCard;
import com.aeromexico.commons.model.FrequentFlyer;
import com.aeromexico.commons.model.SeatMap;
import com.aeromexico.commons.model.SeatmapCollection;
import com.aeromexico.commons.model.utils.CurrencyUtil;
import com.aeromexico.commons.web.util.ListsUtil;
import com.farelogix.flx.servicelistrq.Arrival;
import com.farelogix.flx.servicelistrq.CityCode;
import com.farelogix.flx.servicelistrq.CustomParams;
import com.farelogix.flx.servicelistrq.Departure;
import com.farelogix.flx.servicelistrq.Equipment;
import com.farelogix.flx.servicelistrq.Flight;
import com.farelogix.flx.servicelistrq.FlightNumberWithSuffixType;
import com.farelogix.flx.servicelistrq.InfantData;
import com.farelogix.flx.servicelistrq.JCDArrival;
import com.farelogix.flx.servicelistrq.JCDDeparture;
import com.farelogix.flx.servicelistrq.JCDFlight;
import com.farelogix.flx.servicelistrq.JourneyControlData;
import com.farelogix.flx.servicelistrq.OperatingCarrier;
import com.farelogix.flx.servicelistrq.OriginDestination;
import com.farelogix.flx.servicelistrq.PricingInfo;
import com.farelogix.flx.servicelistrq.SaleInfo;
import com.farelogix.flx.servicelistrq.ServiceListRQ;
import com.farelogix.flx.servicelistrq.ServicesFilter;
import com.farelogix.flx.servicelistrq.TravelerIDRef;
import com.farelogix.flx.servicelistrq.TravelerIDs;
import com.farelogix.flx.servicelistrq.TravelerReferences;
import com.farelogix.flx.servicelistrs.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author adrianleal
 */
public class FlxServiceListUtil {
    private static final Logger LOG = LoggerFactory.getLogger(FlxServiceListUtil.class);

    public static ServiceListRQ getSeatMapServiceListRQ(
            SeatmapCollection seatmapCollection,
            List<BookedSegment> amBookedSegmentsWithSeatmap,
            BookedTravelerCollection bookedTravelerCollection,
            String recordLocator,
            String currencyCode
    ) {

        ServiceListRQ serviceListRQ = new ServiceListRQ();

        BookedSegment firstSegment = ListsUtil.getFirst(amBookedSegmentsWithSeatmap);
        BookedSegment lastSegment = ListsUtil.getLast(amBookedSegmentsWithSeatmap);

        SaleInfo saleInfo = getSaleInfo(firstSegment, lastSegment, currencyCode);

        serviceListRQ.setSaleInfo(saleInfo);

        List<OriginDestination> originDestinationList = serviceListRQ.getOriginDestination();
        JourneyControlData journeyControlData = new JourneyControlData();
        //serviceListRQ.setJourneyControlData(journeyControlData);

        setFlightsDetails(
                amBookedSegmentsWithSeatmap,
                originDestinationList,
                journeyControlData,
                seatmapCollection
        );

        serviceListRQ.setRecordLocator(recordLocator);

        List<TravelerIDs> travelerIdsList = serviceListRQ.getTravelerIDs();

        setTravelerIds(bookedTravelerCollection, travelerIdsList);

        PricingInfo pricingInfo = getPricingInfo(new ArrayList<String>(Arrays.asList("SA")), null, "WCI", "AM");

        serviceListRQ.setPricingInfo(pricingInfo);

        return serviceListRQ;
    }

    public static ServiceListRQ getAncillaryServiceListRQ(
            List<BookedSegment> amBookedSegmentsWithAncillaries,
            BookedTravelerCollection bookedTravelerCollection,
            String recordLocator,
            String currencyCode
    ) {

        ServiceListRQ serviceListRQ = new ServiceListRQ();

        BookedSegment firstSegment = ListsUtil.getFirst(amBookedSegmentsWithAncillaries);
        BookedSegment lastSegment = ListsUtil.getLast(amBookedSegmentsWithAncillaries);

        SaleInfo saleInfo = getSaleInfo(firstSegment, lastSegment, currencyCode);

        serviceListRQ.setSaleInfo(saleInfo);

        List<OriginDestination> originDestinationList = serviceListRQ.getOriginDestination();
        JourneyControlData journeyControlData = new JourneyControlData();
        //serviceListRQ.setJourneyControlData(journeyControlData);

        setFlightsDetails(
                amBookedSegmentsWithAncillaries,
                originDestinationList,
                journeyControlData
        );

        serviceListRQ.setRecordLocator(recordLocator);

        List<TravelerIDs> travelerIdsList = serviceListRQ.getTravelerIDs();

        setTravelerIds(bookedTravelerCollection, travelerIdsList);

        PricingInfo pricingInfo = getPricingInfo(null, null, "WCI", "AM");

        serviceListRQ.setPricingInfo(pricingInfo);

        return serviceListRQ;
    }

    private static void setFlightsDetails(
            List<BookedSegment> amBookedSegments,
            List<OriginDestination> originDestinationList,
            JourneyControlData journeyControlData
    ) {
        setFlightsDetails(
                amBookedSegments,
                originDestinationList,
                journeyControlData,
                null
        );
    }

    private static void setFlightsDetails(
            List<BookedSegment> amBookedSegments,
            List<OriginDestination> originDestinationList,
            JourneyControlData journeyControlData,
            SeatmapCollection seatmapCollection
    ) {
        int flightIndex = 1;
        for (BookedSegment bookedSegment : amBookedSegments) {

            OriginDestination originDestination = new OriginDestination();
            originDestinationList.add(originDestination);

            Flight flight;
            if (null != seatmapCollection) {
                flight = getFlight(bookedSegment, seatmapCollection);
            } else {
                flight = getFlight(bookedSegment);
            }
            originDestination.getFlight().add(flight);

            try {
                if (null != bookedSegment.getSegment()
                        && null != bookedSegment.getSegment().getCapacity()
                        && !bookedSegment.getSegment().getCapacity().isEmpty()) {

                    CabinCapacity cabinCapacity = getCabinCapacity("main", bookedSegment.getSegment().getCapacity());

                    if (null != cabinCapacity && cabinCapacity.getAuthorized() > 0) {

                        double loadFactor = ((double) cabinCapacity.getBooked() / (double) cabinCapacity.getAuthorized()) * 100.0;

                        CustomParams customParams = new CustomParams();
                        CustomParams.Param param = new CustomParams.Param();
                        param.setName("LoadFactor");
                        param.getValue().add(String.valueOf((int) loadFactor));
                        customParams.getParam().add(param);

                        flight.setCustomParams(customParams);
                    }
                }
            } catch (Exception ex) {
                LOG.error(ex.getMessage(), ex);
            }

            //for journeyControlData
            JCDFlight jcdFlight = new JCDFlight();

            jcdFlight.setOriginDestinationID("0" + flightIndex);

            jcdFlight.setMarriedSegment("S");

            CityCode departurelCityCode = new CityCode();
            departurelCityCode.setValue(flight.getDeparture().getAirportCode());

            JCDDeparture jcdDeparture = new JCDDeparture();
            jcdDeparture.setCityCode(departurelCityCode);
            jcdFlight.setJCDDeparture(jcdDeparture);

            CityCode arrivalCityCode = new CityCode();
            arrivalCityCode.setValue(flight.getArrival().getAirportCode());

            JCDArrival jcdArrival = new JCDArrival();
            jcdArrival.setCityCode(arrivalCityCode);
            jcdFlight.setJCDArrival(jcdArrival);

            JCDFlight.JCDDate jcdDate = new JCDFlight.JCDDate();
            jcdDate.setQualifier("DEPARTURE");
            jcdDate.setValue(flight.getDeparture().getTime());
            jcdFlight.setJCDDate(jcdDate);

            JCDFlight.Carrier carrier = new JCDFlight.Carrier();
            carrier.setAirlineCode(flight.getAirlineCode());
            carrier.setFlightNumber(flight.getFlightNumber());
            jcdFlight.setCarrier(carrier);

            jcdFlight.setClassOfService(flight.getClassOfService());

            jcdFlight.setActionCode(bookedSegment.getSegment().getSegmentStatus());

            if (null != bookedSegment.getSegment().getStops()) {
                jcdFlight.setNumberOfStops(bookedSegment.getSegment().getStops().size());
            }

            journeyControlData.getJCDFlight().add(jcdFlight);

            flightIndex++; //increase

        }
    }

    private static CabinCapacity getCabinCapacity(String cabin, List<CabinCapacity> cabinCapacityList) {
        if (null == cabin || cabin.trim().isEmpty() && null == cabinCapacityList || cabinCapacityList.isEmpty()) {
            return null;
        }

        for (CabinCapacity cabinCapacity : cabinCapacityList) {
            if (cabin.equalsIgnoreCase(cabinCapacity.getCabin())) {
                return cabinCapacity;
            }
        }

        return null;
    }

    private static Flight getFlight(BookedSegment bookedSegment) {
        Flight flight = new Flight();
        flight.setAirlineCode(bookedSegment.getSegment().getOperatingCarrier());
        FlightNumberWithSuffixType flightNumberWithSuffixType = new FlightNumberWithSuffixType();
        flightNumberWithSuffixType.setValue(Integer.valueOf(bookedSegment.getSegment().getOperatingFlightCode()));
        flightNumberWithSuffixType.setSuffix(bookedSegment.getSegment().getOperatingCarrier());
        flight.setFlightNumber(flightNumberWithSuffixType);
        flight.setClassOfService(bookedSegment.getSegment().getBookingClass());
        if (null != bookedSegment.getSegment().getFareBasisCode()) {
            Flight.FareBasisCode fareBasisCode = new Flight.FareBasisCode();
            fareBasisCode.setValue(bookedSegment.getSegment().getFareBasisCode());
            flight.setFareBasisCode(fareBasisCode);
        }
        flight.setCabin(bookedSegment.getSegment().getCabin());
        Equipment equipment = new Equipment();
        equipment.setCode(bookedSegment.getSegment().getAircraftType());
        flight.setEquipment(equipment);
        Departure departure = new Departure();
        departure.setAirportCode(bookedSegment.getSegment().getDepartureAirport());
        Departure.Date departureDate = new Departure.Date();
        departureDate.setValue(bookedSegment.getSegment().getDepartureDateTime().substring(0, 10));
        departure.setDate(departureDate);
        departure.setTime(bookedSegment.getSegment().getDepartureDateTime().substring(11, 16));
        flight.setDeparture(departure);
        Arrival arrival = new Arrival();
        arrival.setAirportCode(bookedSegment.getSegment().getArrivalAirport());
        Arrival.Date arrivalDate = new Arrival.Date();
        arrivalDate.setValue(bookedSegment.getSegment().getArrivalDateTime().substring(0, 10));
        arrival.setDate(arrivalDate);
        arrival.setTime(bookedSegment.getSegment().getArrivalDateTime().substring(11, 16));
        flight.setArrival(arrival);
        OperatingCarrier operatingCarrier = new OperatingCarrier();
        operatingCarrier.setAirlineCode(bookedSegment.getSegment().getOperatingCarrier());
        flight.setOperatingCarrier(operatingCarrier);

        return flight;

    }

    private static Flight getFlight(BookedSegment bookedSegment, SeatmapCollection seatmapCollection) {

        Flight flight = getFlight(bookedSegment);

        if (null != seatmapCollection) {
            SeatMap seatMap = FlxSeatMapPriceUtil.getSeatMapSection(seatmapCollection, bookedSegment.getSegment().getSegmentCode());
            if (null != seatMap) {
                Flight.SeatMap flightSeatMap = FlxSeatMapPriceUtil.transformSeatMap(seatMap);
                flight.setSeatMap(flightSeatMap);
            }
        }

        return flight;
    }

    private static void setTravelerIds(BookedTravelerCollection bookedTravelerCollection, List<TravelerIDs> travelerIdsList) {
        for (BookedTraveler bookedTraveler : bookedTravelerCollection.getCollection()) {
            TravelerIDs travelerIDs = new TravelerIDs();

            TravelerReferences travelerReferences = new TravelerReferences();
            travelerReferences.setSurname(bookedTraveler.getLastName());
            travelerReferences.setGivenName(bookedTraveler.getFirstName());
            if (null != bookedTraveler.getGender()) {
                travelerReferences.setGender(bookedTraveler.getGender().name());
            }
            travelerReferences.setDateOfBirth(bookedTraveler.getDateOfBirth());
            if (null != bookedTraveler.getPaxType()) {
                travelerReferences.setType(bookedTraveler.getPaxType().getCode());
            }

            if (null != bookedTraveler.getInfant()) {
                InfantData infantData = new InfantData();
                infantData.setGivenName(bookedTraveler.getInfant().getFirstName());
                infantData.setSurname(bookedTraveler.getInfant().getLastName());
                travelerReferences.setInfantData(infantData);
            }

            travelerIDs.setTravelerReferences(travelerReferences);
            TravelerIDRef travelerIDRef = new TravelerIDRef();
            travelerIDRef.setValue(bookedTraveler.getNameRefNumber());
            travelerIDs.setTravelerIDRef(travelerIDRef);
            if (null != bookedTraveler.getPaxType()) {
                travelerIDs.setPaxType(bookedTraveler.getPaxType().getCode());
            }

            if (null != bookedTraveler.getLoyaltyNumbers()) {
                for (FrequentFlyer loyaltyNumber : bookedTraveler.getLoyaltyNumbers()) {
                    TravelerIDs.FQTVInfo fqtvInfo = new TravelerIDs.FQTVInfo();
                    fqtvInfo.setFFCompanyCode(loyaltyNumber.getSupplierCode());
                    fqtvInfo.setFFNumber(loyaltyNumber.getNumber());
                    fqtvInfo.setFFStatus(loyaltyNumber.getTierTag());
                    travelerIDs.getFQTVInfo().add(fqtvInfo);
                }
            }

            try {
                boolean sendCustomParam = false;
                CustomParams customParams = new CustomParams();
                List<CustomParams.Param> customParamsList = customParams.getParam();

                if (null != bookedTraveler.getBookedTravellerBenefitCollection()
                        && null != bookedTraveler.getBookedTravellerBenefitCollection().getCollection()) {

                    CustomParams.Param param = new CustomParams.Param();
                    param.setName("cobrandedCards");
                    param.setTRef(bookedTraveler.getNameRefNumber());

                    for (BookedTravelerBenefit bookedTravelerBenefit : bookedTraveler.getBookedTravellerBenefitCollection().getCollection()) {
                        if (null != bookedTravelerBenefit.getBagBenefitCobrand()
                                && null != bookedTravelerBenefit.getBagBenefitCobrand().getFreeBagAllowanceByCardList()) {
                            for (FreeBaggageAllowanceDetailByCard freeBaggageAllowanceDetailByCard : bookedTravelerBenefit.getBagBenefitCobrand().getFreeBagAllowanceByCardList()) {
                                if (!param.getValue().contains(freeBaggageAllowanceDetailByCard.getCobrandCode())) {
                                    param.getValue().add(freeBaggageAllowanceDetailByCard.getCobrandCode());
                                    sendCustomParam = true;
                                }
                            }
                        }
                    }

                    customParamsList.add(param);
                }

                if (sendCustomParam) {
                    travelerIDs.setCustomParams(customParams);
                }
            } catch (Exception ex) {
                LOG.error(ex.getMessage(), ex);
            }

            travelerIdsList.add(travelerIDs);
        }
    }

    private static PricingInfo getPricingInfo(List<String> groupFilter, List<String> codeFilter, String find, String validatingCarrier) {
        PricingInfo pricingInfo = new PricingInfo();

        pricingInfo.setSeatSplitRows("Y");
        pricingInfo.setImages("Y");
        pricingInfo.setShowNotEligibleItems("Y");
        pricingInfo.setAllowDiscountDuplicates("Y");
        pricingInfo.setTaxes("Y");

        pricingInfo.setTouchPoint("OnlineCheckIn");
        pricingInfo.setValidatingCarrier(validatingCarrier);

        if (null != find) {
            CustomParams customParams = new CustomParams();
            CustomParams.Param param = new CustomParams.Param();
            param.setName("FIND");
            param.getValue().add(find);
            customParams.getParam().add(param);
            pricingInfo.setCustomParams(customParams);
        }

        if ((null != groupFilter && !groupFilter.isEmpty()) || (null != codeFilter && !codeFilter.isEmpty())) {
            ServicesFilter servicesFilter = new ServicesFilter();

            if (null != groupFilter && !groupFilter.isEmpty()) {
                for (String filter : groupFilter) {
                    ServicesFilter.Attributes attributes = new ServicesFilter.Attributes();
                    ServicesFilter.Attributes.Group group = new ServicesFilter.Attributes.Group();
                    group.setCode(filter);
                    attributes.setGroup(group);
                    servicesFilter.getAttributes().add(attributes);
                }
            }

            if (null != codeFilter && !codeFilter.isEmpty()) {
                for (String filter : codeFilter) {
                }
            }

            pricingInfo.setServicesFilter(servicesFilter);
        }

        return pricingInfo;
    }

    private static SaleInfo getSaleInfo(BookedSegment firstSegment, BookedSegment lastSegment, String currencyCode) {
        SaleInfo saleInfo = new SaleInfo();
        saleInfo.setPointOfOrigin(firstSegment.getSegment().getDepartureAirport());
        saleInfo.setJourneyDestination(lastSegment.getSegment().getArrivalAirport());
        SaleInfo.CurrencyCode currencyCodeObj = new SaleInfo.CurrencyCode();
        currencyCodeObj.setValue(currencyCode);
        currencyCodeObj.setNumberOfDecimals(CurrencyUtil.getNumberOfDecimals(currencyCode));
        saleInfo.setCurrencyCode(currencyCodeObj);
        CityCode cityCode = new CityCode();
        cityCode.setValue(firstSegment.getSegment().getDepartureAirport());
        saleInfo.setCityCode(cityCode);
        saleInfo.setChannel("AMD");
        return saleInfo;
    }

    public static List<Service> getServices(
            ServiceListRS.OptionalServices optionalServices,
            String segmentRef
    ) {
        if (null == segmentRef || segmentRef.trim().isEmpty()
                || null == optionalServices || null == optionalServices.getService()) {
            return null;
        }

        List<Service> services = new ArrayList<>();

        for (Service service : optionalServices.getService()) {
            if (null != service.getSegmentIDRef()) {
                for (SegmentIDRef segmentIDRef : service.getSegmentIDRef()) {
                    if (segmentIDRef.getValue().equalsIgnoreCase(segmentRef)) {
                        services.add(service);
                        break;
                    }
                }
            }
        }

        return services;
    }

    public static FlxPriceWrapper getCurrencyValue(
            CurrencyCode currencyCodeObj,
            ServicePrice servicePrice,
            ServicePriceCalc servicePriceCalc
    ) {
        FlxPriceWrapper flxPriceWrapper = new FlxPriceWrapper();

        CurrencyValue currencyValue = new CurrencyValue();
        currencyValue.setCurrencyCode(currencyCodeObj.getValue());
        currencyValue.setBase(
                getCurrency(
                        currencyCodeObj,
                        servicePrice.getBasePrice().getAmount().toString()
                )
        );
        Map<String, BigDecimal> taxDetails = new HashMap<>();
        if (servicePrice.getTaxes() != null) {
            servicePrice.getTaxes().getTax().forEach((tax) -> {
                taxDetails.put("MXA",
                        getCurrency(
                                currencyCodeObj,
                                tax.getAmount().toString()
                        )
                );
            });

            currencyValue.setTotalTax(
                    getCurrency(
                            currencyCodeObj,
                            servicePrice.getTaxes().getAmount().toString()
                    )
            );
        }
        currencyValue.setTaxDetails(taxDetails);
        currencyValue.setEquivalentCurrencyCode(null);
        currencyValue.setEquivalent(null);
        currencyValue.setTotal(
                getCurrency(
                        currencyCodeObj,
                        servicePrice.getTotal().toString()
                )
        );

        flxPriceWrapper.setCurrencyValue(currencyValue);

        if (null != servicePriceCalc && null != servicePriceCalc.getOperation()) {
            for (ServicePriceCalc.Operation operation : servicePriceCalc.getOperation()) {
                if (null != operation && null != operation.getDesignator()) {
                    FlxPriceWrapper.PriceCalc priceCalc = new FlxPriceWrapper.PriceCalc();
                    priceCalc.setDesignator(operation.getDesignator());
                    flxPriceWrapper.getPriceCalcList().add(priceCalc);
                }
            }
        }

        return flxPriceWrapper;
    }

    private static BigDecimal getCurrency(CurrencyCode currencyCode, String amount) {

        int decimals = 0;
        if (null != currencyCode.getValue()
                && null != currencyCode.getNumberOfDecimals()) {
            decimals = currencyCode.getNumberOfDecimals().intValue();
        }

        BigDecimal divisor = new BigDecimal(Math.pow(10, decimals));

        if (1 == divisor.compareTo(BigDecimal.ONE)) { //if greather than one
            return CurrencyUtil.getAmount(
                    new BigDecimal(amount).divide(divisor, 2, RoundingMode.HALF_UP), currencyCode.getValue()
            );
        } else {
            return CurrencyUtil.getAmount(new BigDecimal(amount), currencyCode.getValue());
        }
    }
}
