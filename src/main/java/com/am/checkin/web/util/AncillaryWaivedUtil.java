package com.am.checkin.web.util;

import com.aeromexico.commons.model.AbstractAncillary;
import com.aeromexico.commons.model.Ancillary;
import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.BookingClass;
import com.aeromexico.commons.model.BookingClassMap;
import com.aeromexico.commons.model.PaidTravelerAncillary;
import com.aeromexico.commons.model.weather.AirportWeather;
import com.aeromexico.commons.web.util.TimeUtil;
import com.aeromexico.dao.services.IAirportCodesDao;
import com.sabre.webservices.pnrbuilder.updatereservation.AncillaryPricePNRB;
import com.sabre.webservices.pnrbuilder.updatereservation.NameAssociationListPNRB;
import com.sabre.webservices.pnrbuilder.updatereservation.NameAssociationTagPNRB;
import com.sabre.webservices.pnrbuilder.updatereservation.SegmentAssociationListPNRB;
import com.sabre.webservices.pnrbuilder.updatereservation.SegmentAssociationTagPNRB;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.GregorianCalendar;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author Pedro Reyes
 */
public class AncillaryWaivedUtil {

    /**
     *
     * @param bookedSegment
     * @param bookedTraveler
     * @param airportCodesDao
     * @return
     */
    public static boolean isElegibleForWaiveFirstBag(BookedSegment bookedSegment, BookedTraveler bookedTraveler, IAirportCodesDao airportCodesDao) {
        //Only USA & Canada
        try {
            if (!"AM".equalsIgnoreCase(bookedSegment.getSegment().getOperatingCarrier())) {
                return false;
            }
            if (null != bookedTraveler.getBookingClasses().getCollection() && bookedTraveler.getBookingClasses().getCollection().get(0).getBookingCabin().equalsIgnoreCase("J")) {
                return false;
            }
            AirportWeather airportWeather = airportCodesDao.getAirportWeatherByIata(bookedSegment.getSegment().getDepartureAirport());
            if (airportWeather != null && (airportWeather.getAirport().getCountryCode().contains("US") || airportWeather.getAirport().getCountryCode().contains("CA"))) {
                return true;
            }
            airportWeather = airportCodesDao.getAirportWeatherByIata(bookedSegment.getSegment().getArrivalAirport());
            if (airportWeather != null && (airportWeather.getAirport().getCountryCode().contains("US") || airportWeather.getAirport().getCountryCode().contains("CA"))) {
                return true;
            }
        } catch (Exception ex) {
            //logError(recloc, cartId, title);
            return false;
        }

        return false;
    }

    public static NameAssociationListPNRB getNameAssociationList(BookedTraveler bookedTraveler) {
        NameAssociationListPNRB nameAssociationListPNRB = null;

        if (!hasFirstBag(bookedTraveler)) {
            nameAssociationListPNRB = new NameAssociationListPNRB();
            NameAssociationTagPNRB nameAssociationTagPNRB = new NameAssociationTagPNRB();
            nameAssociationTagPNRB.setFirstName(bookedTraveler.getFirstName());
            nameAssociationTagPNRB.setLastName(bookedTraveler.getLastName());
            nameAssociationTagPNRB.setNameRefNumber(bookedTraveler.getNameRefNumber());
            nameAssociationListPNRB.getNameAssociationTag().add(nameAssociationTagPNRB);
        }

        return nameAssociationListPNRB;
    }

    public static boolean hasFirstBag(BookedTraveler bookedTraveler) {
        for (AbstractAncillary abstractAncillary : bookedTraveler.getAncillaries().getCollection()) {
            if (abstractAncillary instanceof PaidTravelerAncillary) {
                PaidTravelerAncillary ancillary = (PaidTravelerAncillary) abstractAncillary;
                if (ancillary.getCommercialName().equalsIgnoreCase("1A PIEZA EXTRA 1ST XTRA PZ")) {
                    return true;
                }
            }

            if (abstractAncillary instanceof Ancillary) {
                Ancillary ancillary = (Ancillary) abstractAncillary;
                if ("0CC".equalsIgnoreCase(ancillary.getRficSubcode())) {
                    return true;
                }
            }
        }
        return false;
    }

    public static SegmentAssociationListPNRB getSegmentAssociationList(BookedSegment bookedSegment, BookingClassMap bookingClassMap) throws DatatypeConfigurationException, ParseException {
        SegmentAssociationListPNRB segmentAssociationListPNRB = new SegmentAssociationListPNRB();
        SegmentAssociationTagPNRB segmentAssociationTagPNRB = new SegmentAssociationTagPNRB();
        segmentAssociationTagPNRB.setCarrierCode(bookedSegment.getSegment().getOperatingCarrier());
        segmentAssociationTagPNRB.setFlightNumber(bookedSegment.getSegment().getOperatingFlightCode());

        GregorianCalendar c = new GregorianCalendar();
        c.setTimeInMillis(TimeUtil.getTime(bookedSegment.getSegment().getDepartureDateTime()));
        XMLGregorianCalendar xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
        xmlGregorianCalendar.setMillisecond(DatatypeConstants.FIELD_UNDEFINED);
        xmlGregorianCalendar.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
        segmentAssociationTagPNRB.setDepartureDate(xmlGregorianCalendar);

        segmentAssociationTagPNRB.setBoardPoint(bookedSegment.getSegment().getDepartureAirport());
        segmentAssociationTagPNRB.setOffPoint(bookedSegment.getSegment().getArrivalAirport());
        for (BookingClass bookingClass : bookingClassMap.getCollection()) {
            if (bookingClass.getSegmentCode().equalsIgnoreCase(bookedSegment.getSegment().getSegmentCode())) {
                segmentAssociationTagPNRB.setClassOfService(bookingClass.getBookingClass());
            }
        }
        //We use to use Sabre values
        //segmentAssociationTagPNRB.setBookingStatus("HK");
        segmentAssociationTagPNRB.setBookingStatus(bookedSegment.getSegment().getSegmentStatus());

        segmentAssociationListPNRB.getSegmentAssociationTag().add(segmentAssociationTagPNRB);

        return segmentAssociationListPNRB;
    }

    public static AncillaryPricePNRB getOriginalBasePrice() {
        AncillaryPricePNRB ancillaryPricePNRB = new AncillaryPricePNRB();
        ancillaryPricePNRB.setPrice(BigDecimal.ZERO);
        ancillaryPricePNRB.setCurrency("MXN");
        return ancillaryPricePNRB;
    }

    public static com.sabre.webservices.pnrbuilder.updatereservation.AncillaryServicesPNRB.Taxes getTaxes() {
        return null;
    }

}
