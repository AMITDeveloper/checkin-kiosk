package com.am.checkin.web.util;

import com.am.checkin.web.service.UpdateCollectionAfterCheckinOnDB;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.PNR;
import com.aeromexico.commons.model.PNRCollection;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.dao.services.IPNRLookupDao;
import com.aeromexico.commons.exception.model.GenericException;
import com.am.checkin.web.event.dispatcher.PostCheckinDispatcher;
import com.am.checkin.web.event.model.PostCheckinEvent;

/**
 *
 * @author adrian
 */
@Named
@ApplicationScoped
public class PostCheckinProcess {

    private static final Logger LOG = LoggerFactory.getLogger(PostCheckinProcess.class);

    @Inject
    private UpdateCollectionAfterCheckinOnDB updateCollectionAfterCheckinOnDB;

    @Inject
    private IPNRLookupDao pnrLookupDao;

    @Inject
    private PostCheckinDispatcher postCheckinDispatcher;

    /**
     *
     * @param pnrCollectionAfterCheckin
     * @param recordLocator
     * @param cartId
     * @param clean
     * @throws Exception
     */
    public void unasynchronousPostCheckin(
            PNRCollection pnrCollectionAfterCheckin,
            String recordLocator, String cartId,
            boolean clean
    ) throws Exception {
        if (null == recordLocator || recordLocator.isEmpty()) {
            return;
        }

        if (null == pnrCollectionAfterCheckin) {
            LOG.error("Post checkin: Pnr collection was null triying to update cart, {} {}", recordLocator, cartId);
            throw new GenericException(
                    Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION,
                    "Pnr collection was null triying to update cart"
            );
        }

        LOG.info("Post checkin: Finding PNR by recordLocator to update cart, {} {}", recordLocator, cartId);
        PNR pnr = pnrCollectionAfterCheckin.getPNRByRecordLocator(recordLocator);
        if (null == pnr) {
            LOG.error("Post checkin: Pnr not found while triying to update cart, {} {}", recordLocator, cartId);
            throw new GenericException(
                    Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION,
                    "Pnr not found while triying to update cart"
            );
        }

        if (null != cartId && !cartId.isEmpty()) {
            LOG.info("Post checkin: Update cart, {} {} ", recordLocator, cartId);

            for (CartPNR cartPNR : pnr.getCarts().getCollection()) {
                if (null != cartPNR
                        && (cartPNR.isTouched()
                        || cartId.equalsIgnoreCase(cartPNR.getMeta().getCartId()))) {
                    cartPNR.setTouched(false);

                    int result = updateCollectionAfterCheckinOnDB.synchronousCartUpdate(
                            pnrCollectionAfterCheckin, cartPNR.getLegCode(), recordLocator, cartId
                    );

                    if (result < 0) {
                        //
                    }
                } else {

                }
            }

            LOG.info("Post checkin: Cleaning collections, {} {} ", recordLocator, cartId);

            if (clean) {
                try {
                    int result = pnrLookupDao.deleteFromPnrCollecion(cartId);

                    if (result <= 0) {
                        //
                    }
                } catch (Exception ex) {
                    //
                }
            }
        }
    }

    /**
     *
     * @param pnrCollectionAfterCheckin
     * @param recordLocator
     * @param cartId
     * @param clean
     */
    public void unasynchronousMutePostCheckin(
            PNRCollection pnrCollectionAfterCheckin,
            String recordLocator,
            String cartId, boolean clean
    ) {
        try {
            unasynchronousPostCheckin(pnrCollectionAfterCheckin, recordLocator, cartId, clean);
        } catch (Exception ex) {
            LOG.error("{} {} {}", recordLocator, cartId, ex.getMessage());
        } catch (Throwable ex) {
            LOG.error("{} {} {}", recordLocator, cartId, ex.getMessage());
        }
    }

    /**
     *
     * @param pnrCollectionAfterCheckin
     * @param recordLocator
     * @param cartId
     * @param legCode
     * @param clean
     * @throws Exception
     */
    public void FirePostCheckinEvent(PNRCollection pnrCollectionAfterCheckin, String recordLocator, String cartId,
            String legCode, boolean clean) throws Exception {

        PostCheckinEvent postCheckinEvent = new PostCheckinEvent(
                pnrCollectionAfterCheckin,
                recordLocator, cartId,
                legCode, clean
        );

        postCheckinDispatcher.publish(postCheckinEvent);

    }
}
