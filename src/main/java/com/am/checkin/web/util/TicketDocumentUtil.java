package com.am.checkin.web.util;

import com.aeromexico.commons.model.Segment;
import com.sabre.ns.ticketing.dc.GetTicketingDocumentRS;
import com.sabre.ns.ticketing.dc.TicketingDocumentInfoAbbreviated;
import com.sabre.ns.ticketing.dc.TicketingDocumentServiceCouponAbbreviated;
import com.sabre.services.micellaneous.FlightMisc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adrian
 */
public class TicketDocumentUtil {

    private static final Logger LOG = LoggerFactory.getLogger(TicketDocumentUtil.class);


    public static  boolean isAmTicketingProvider(GetTicketingDocumentRS ticketingDocumentRS) {
        if (null == ticketingDocumentRS) {
            return false;
        }

        try {
            for (TicketingDocumentInfoAbbreviated ticketingDocumentInfoAbbreviated : ticketingDocumentRS.getAbbreviated()) {
                if ("TKT".equalsIgnoreCase(ticketingDocumentInfoAbbreviated.getTicketingDocument().getType())) {
                    return ticketingDocumentInfoAbbreviated.getTicketingDocument().getAccountingCode().startsWith("139");
                }
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
        }

        return false;
    }

    /**
     *
     * @param ticketingDocumentRS
     * @param segment
     * @param vcr
     * @return
     */
    public static String getCoupon(
            GetTicketingDocumentRS ticketingDocumentRS,
            Segment segment,
            String vcr
    ) {
        if (null == ticketingDocumentRS) {
            //return default coupon
            return "";
        }

        try {
            for (TicketingDocumentInfoAbbreviated ticketingDocumentInfoAbbreviated : ticketingDocumentRS.getAbbreviated()) {
                if (ticketingDocumentInfoAbbreviated.getTicketingDocument().getNumber().equalsIgnoreCase(vcr)) {
                    for (TicketingDocumentServiceCouponAbbreviated ticketingDocumentServiceCouponAbbreviated : ticketingDocumentInfoAbbreviated.getTicketingDocument().getServiceCoupon()) {
                        if (TicketDocumentUtil.compareSegmentsByOriginAndDestination(segment, ticketingDocumentServiceCouponAbbreviated)) {
                            return ticketingDocumentServiceCouponAbbreviated.getCoupon().toString();
                        }
                    }
                }
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
        }

        //return default coupon
        return "";

    }

    /**
     *
     * @param ticketingDocumentRS
     * @param flightMisc
     * @return
     */
    public static String getCoupon(
            GetTicketingDocumentRS ticketingDocumentRS,
            FlightMisc flightMisc
    ) {
        if (null == ticketingDocumentRS) {
            //return default coupon
            return "";
        }

        try {
            for (TicketingDocumentInfoAbbreviated ticketingDocumentInfoAbbreviated : ticketingDocumentRS.getAbbreviated()) {
                if (ticketingDocumentInfoAbbreviated.getTicketingDocument().getNumber().equalsIgnoreCase(flightMisc.getAssociatedTicketNumber().getValue())) {
                    for (TicketingDocumentServiceCouponAbbreviated ticketingDocumentServiceCouponAbbreviated : ticketingDocumentInfoAbbreviated.getTicketingDocument().getServiceCoupon()) {
                        if (compareSegmentsByOriginAndDestination(flightMisc, ticketingDocumentServiceCouponAbbreviated)) {
                            return ticketingDocumentServiceCouponAbbreviated.getCoupon().toString();
                        }
                    }
                }
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
        }

        //return default coupon
        return "";

    }

    /**
     * 
     * @param ticketingDocumentRS
     * @param departureCity
     * @param arrivalCity
     * @param vcr
     * @return 
     */
    public static String getCoupon(
            GetTicketingDocumentRS ticketingDocumentRS,            
            String departureCity,
            String arrivalCity,
            String vcr
    ) {
        if (null == ticketingDocumentRS) {
            //return default coupon
            return "";
        }

        try {
            for (TicketingDocumentInfoAbbreviated ticketingDocumentInfoAbbreviated : ticketingDocumentRS.getAbbreviated()) {
                if (ticketingDocumentInfoAbbreviated.getTicketingDocument().getNumber().equalsIgnoreCase(vcr)) {
                    for (TicketingDocumentServiceCouponAbbreviated ticketingDocumentServiceCouponAbbreviated : ticketingDocumentInfoAbbreviated.getTicketingDocument().getServiceCoupon()) {
                        if (compareSegmentsByOriginAndDestination(departureCity, arrivalCity, ticketingDocumentServiceCouponAbbreviated)) {
                            return ticketingDocumentServiceCouponAbbreviated.getCoupon().toString();
                        }
                    }
                }
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
        }

        //return default coupon
        return "";

    }

    /**
     *
     * @param segment
     * @param ticketingDocumentServiceCouponAbbreviated
     * @return
     */
    public static boolean compareSegmentsByOriginAndDestination(
            Segment segment,
            TicketingDocumentServiceCouponAbbreviated ticketingDocumentServiceCouponAbbreviated
    ) {
        try {
            if (!ticketingDocumentServiceCouponAbbreviated.getStartLocation().equalsIgnoreCase(segment.getDepartureAirport())) {
                return false;
            }

            return ticketingDocumentServiceCouponAbbreviated.getEndLocation().equalsIgnoreCase(segment.getArrivalAirport());
        } catch (Exception ex) {
            return false;
        }
    }

    /**
     *
     * @param flightMisc
     * @param ticketingDocumentServiceCouponAbbreviated
     * @return
     */
    public static boolean compareSegmentsByOriginAndDestination(
            FlightMisc flightMisc,
            TicketingDocumentServiceCouponAbbreviated ticketingDocumentServiceCouponAbbreviated
    ) {
        try {

            if (!ticketingDocumentServiceCouponAbbreviated.getStartLocation().equalsIgnoreCase(flightMisc.getDepartureCity())) {
                return false;
            }

            return ticketingDocumentServiceCouponAbbreviated.getEndLocation().equalsIgnoreCase(flightMisc.getArrivalCity());
        } catch (Exception ex) {
            return false;
        }
    }

    /**
     *
     * @param departureCity
     * @param arrivalCity
     * @param ticketingDocumentServiceCouponAbbreviated
     * @return
     */
    public static boolean compareSegmentsByOriginAndDestination(
            String departureCity,
            String arrivalCity,
            TicketingDocumentServiceCouponAbbreviated ticketingDocumentServiceCouponAbbreviated
    ) {
        try {

            if (!ticketingDocumentServiceCouponAbbreviated.getStartLocation().equalsIgnoreCase(departureCity)) {
                return false;
            }

            return ticketingDocumentServiceCouponAbbreviated.getEndLocation().equalsIgnoreCase(arrivalCity);
        } catch (Exception ex) {
            return false;
        }
    }
}
