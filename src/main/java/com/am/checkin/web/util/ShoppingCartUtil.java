package com.am.checkin.web.util;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.AbstractAncillary;
import com.aeromexico.commons.model.AbstractSeat;
import com.aeromexico.commons.model.AbstractSeatMap;
import com.aeromexico.commons.model.AbstractSegmentChoice;
import com.aeromexico.commons.model.Ancillary;
import com.aeromexico.commons.model.AncillaryOffer;
import com.aeromexico.commons.model.AncillaryOfferCollection;
import com.aeromexico.commons.model.BaggageAncillaryOffer;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.BookedTravelerBenefit;
import com.aeromexico.commons.model.BookingClass;
import com.aeromexico.commons.model.CabinUpgradeAncillary;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.CurrencyValue;
import com.aeromexico.commons.model.DestinationAddress;
import com.aeromexico.commons.model.EmergencyContact;
import com.aeromexico.commons.model.ExtraWeightAncillary;
import com.aeromexico.commons.model.FreeBaggageAllowancePerLeg;
import com.aeromexico.commons.model.PNR;
import com.aeromexico.commons.model.PaidTravelerAncillary;
import com.aeromexico.commons.model.PectabBoardingPass;
import com.aeromexico.commons.model.PectabBoardingPassList;
import com.aeromexico.commons.model.SeatChoice;
import com.aeromexico.commons.model.SeatChoiceUpsell;
import com.aeromexico.commons.model.SeatMap;
import com.aeromexico.commons.model.SeatmapCollection;
import com.aeromexico.commons.model.SeatmapSeat;
import com.aeromexico.commons.model.Segment;
import com.aeromexico.commons.model.SegmentChoice;
import com.aeromexico.commons.model.TravelerAncillary;
import com.aeromexico.commons.model.TravelerDocument;
import com.aeromexico.commons.model.UpgradeAncillary;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.types.SeatCharacteristicsType;
import com.aeromexico.commons.web.types.SeatUpgradeType;
import com.aeromexico.commons.web.util.ErrorCodeDescriptions;
import com.aeromexico.commons.web.util.SegmentCodeUtil;
import com.sabre.services.acs.bso.editpassengercharacteristics.v3.ACSEditPassengerCharacteristicsRQACS;
import com.sabre.services.acs.bso.editpassengercharacteristics.v3.EditCodeDetailsListACS;
import com.sabre.services.acs.bso.editpassengercharacteristics.v3.PassengerInfoListRQACS;
import com.sabre.services.acs.bso.editpassengercharacteristics.v3.PassengerInfoRQACS;
import com.sabre.services.acs.bso.seatchange.v3.ACSSeatChangeRSACS;
import com.sabre.services.stl.v3.PECTABDataListACS;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.utils.CurrencyUtil;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.types.SeatCharacteristicsType;
import com.aeromexico.commons.web.types.SeatUpgradeType;
import com.aeromexico.commons.web.util.ErrorCodeDescriptions;
import com.aeromexico.commons.web.util.SegmentCodeUtil;
import com.sabre.services.acs.bso.editpassengercharacteristics.v3.ACSEditPassengerCharacteristicsRQACS;
import com.sabre.services.acs.bso.editpassengercharacteristics.v3.EditCodeDetailsListACS;
import com.sabre.services.acs.bso.editpassengercharacteristics.v3.PassengerInfoListRQACS;
import com.sabre.services.acs.bso.editpassengercharacteristics.v3.PassengerInfoRQACS;
import com.sabre.services.acs.bso.seatchange.v3.ACSSeatChangeRSACS;
import com.sabre.services.stl.v3.PECTABDataListACS;

/**
 *
 * @author rocio
 */
public class ShoppingCartUtil {

    private static final Logger LOG = LoggerFactory.getLogger(ShoppingCartUtil.class);

    /**
     *
     * @param bookedTraveler
     * @param segmentCode
     * @return
     */
    public static BookingClass getBookingClassMap(BookedTraveler bookedTraveler, String segmentCode) {

        if (null == bookedTraveler.getBookingClasses() || null == bookedTraveler.getBookingClasses().getCollection()
                || bookedTraveler.getBookingClasses().getCollection().isEmpty()) {

            return null;
        }

        for (BookingClass bookingClass : bookedTraveler.getBookingClasses().getCollection()) {
            if (SegmentCodeUtil.compareSegmentCodeWithoutTime(segmentCode, bookingClass.getSegmentCode())) {
                return bookingClass;
            }
        }

        return null;
    }

    /**
     *
     * @param bookedTravelerList
     * @param bookedTravelerUpdateList
     * @return
     */
    public static boolean compareSegmentChoiceCollections(
            List<AbstractSegmentChoice> bookedTravelerList,
            List<AbstractSegmentChoice> bookedTravelerUpdateList
    ) {

        if (bookedTravelerList == bookedTravelerUpdateList) {
            return true;
        }

        if (null == bookedTravelerList && null == bookedTravelerUpdateList) {
            return true;
        }

        if (null == bookedTravelerList) {
            return bookedTravelerUpdateList.isEmpty();
        } else if (null == bookedTravelerUpdateList) {
            return bookedTravelerList.isEmpty();
        }

        if (bookedTravelerList.size() != bookedTravelerUpdateList.size()) {
            return false;
        }

        Set<AbstractSegmentChoice> bookedTravelerListSegmentChoices = new HashSet<>(bookedTravelerList);

        Set<AbstractSegmentChoice> bookedTravelerUpdateListSegmentChoices = new HashSet<>(bookedTravelerUpdateList);

        return bookedTravelerListSegmentChoices.equals(bookedTravelerUpdateListSegmentChoices);
    }

    /**
     *
     * @param ancillaryCollectionList
     * @param ancillaryCollectionListUpdated
     * @return
     */
    public static boolean compareAncillaryCollections(List<AbstractAncillary> ancillaryCollectionList, List<AbstractAncillary> ancillaryCollectionListUpdated) {

        if (ancillaryCollectionList == ancillaryCollectionListUpdated) {
            return true;
        }

        if (null == ancillaryCollectionList && null == ancillaryCollectionListUpdated) {
            return true;
        }

        if (null == ancillaryCollectionList) {
            return ancillaryCollectionListUpdated.isEmpty();
        } else if (null == ancillaryCollectionListUpdated) {
            return ancillaryCollectionList.isEmpty();
        }

        if (ancillaryCollectionList.size() != ancillaryCollectionList.size()) {
            return false;
        }

        Set<AbstractAncillary> bookedTravelerListAncillaries = new HashSet<>(ancillaryCollectionList);
        Set<AbstractAncillary> bookedTravelerUpdateListAncillaries = new HashSet<>(ancillaryCollectionListUpdated);
        
        boolean result = bookedTravelerListAncillaries.equals(bookedTravelerUpdateListAncillaries);

        return result;
    }

    /**
     *
     * @param segmentCode
     * @param abstractSegmentChoiceList
     * @return
     */
    public static AbstractSegmentChoice getAbstractSegmentChoiceBySegmentCode(
            String segmentCode,
            List<AbstractSegmentChoice> abstractSegmentChoiceList
    ) {

        for (AbstractSegmentChoice abstractSegmentChoice : abstractSegmentChoiceList) {
            if (SegmentCodeUtil.compareSegmentCodeWithoutTime(abstractSegmentChoice.getSegmentCode(), segmentCode)) {
                return abstractSegmentChoice;
            }
        }
        return null;

    }

    /**
     *
     * @param abstractSeat
     * @return
     */
    public static String getCodeSeat(AbstractSeat abstractSeat, String pnr) {

        String seat = null;

        if (abstractSeat instanceof SeatChoice) {
            SeatChoice seatChoice = (SeatChoice) abstractSeat;
            seat = seatChoice.getCode();
            if(null != seat) {
                //LOG.info("SELECTED_SEAT_CODE {} PRICE {} PNR {}", seat, "NA", pnr);
            	LOG.info("ANCILLARY_MAP {\"SEATMAP_PNR\":\"" + pnr + "\",\"SELECTED_SEAT_CODE\":\"" + seat + "\",\"PRICE\":\"NA\",\"FARELOGIX_PRICE\":" + false + "}" );
            }
        } else if (abstractSeat instanceof SeatChoiceUpsell) {
            SeatChoiceUpsell seatChoiceUpsell = (SeatChoiceUpsell) abstractSeat;
            seat = seatChoiceUpsell.getCode();
            if(null != seat) {
                //LOG.info("SELECTED_SEAT_CODE {} PRICE {} PNR{}", seat, seatChoiceUpsell.getCurrency().getTotal(), pnr);
            	LOG.info("ANCILLARY_MAP {\"SEATMAP_PNR\":\"" + pnr + "\",\"SELECTED_SEAT_CODE\":\"" + 
                seat + "\",\"PRICE\":\"" + seatChoiceUpsell.getCurrency().getTotal() + "\",\"FARELOGIX_PRICE\":" + seatChoiceUpsell.isFarelogixPrice() + "}" );
            }
        }
        if (null != seat) {
            seat = seat.toUpperCase();
        }
        return seat;
    }

    /**
     *
     * @param ancillary
     * @param ancillaries
     * @return
     */
    public static boolean isPartOfReservation(Ancillary ancillary, List<AbstractAncillary> ancillaries) {

        if (null == ancillary || null == ancillary.getType() || ancillary.getType().trim().isEmpty()
                || null == ancillary.getGroupCode() || ancillary.getGroupCode().trim().isEmpty()) {
            return false;
        }

        for (AbstractAncillary abstractAncillary : ancillaries) {
            if (abstractAncillary instanceof TravelerAncillary) {
                TravelerAncillary travelerAncillary = (TravelerAncillary) abstractAncillary;

                if (travelerAncillary.getAncillary().getType().equalsIgnoreCase(ancillary.getType())
                        && travelerAncillary.getAncillary().getGroupCode().equalsIgnoreCase(ancillary.getGroupCode())) {

                    return travelerAncillary.isPartOfReservation();
                }

            } else if (abstractAncillary instanceof ExtraWeightAncillary) {
                ExtraWeightAncillary extraWeightAncillary = (ExtraWeightAncillary) abstractAncillary;

                if (extraWeightAncillary.getAncillary().getType().equalsIgnoreCase(ancillary.getType())
                        && extraWeightAncillary.getAncillary().getGroupCode()
                                .equalsIgnoreCase(ancillary.getGroupCode())) {

                    return extraWeightAncillary.isPartOfReservation();
                }

            } else if (abstractAncillary instanceof PaidTravelerAncillary) {
                PaidTravelerAncillary paidTravelerAncillary = (PaidTravelerAncillary) abstractAncillary;

                if (paidTravelerAncillary.getType().equalsIgnoreCase(ancillary.getType())
                        && paidTravelerAncillary.getGroupCode().equalsIgnoreCase(ancillary.getGroupCode())) {

                    return paidTravelerAncillary.isPartOfReservation();
                }
            }
        }

        return false;
    }

    /**
     *
     * @param ancillary
     * @param ancillaries
     * @return
     */
    public static AbstractAncillary getSameAncillary(Ancillary ancillary, List<AbstractAncillary> ancillaries) {

        if (null == ancillary || null == ancillary.getType() || ancillary.getType().trim().isEmpty()
                || null == ancillary.getGroupCode() || ancillary.getGroupCode().trim().isEmpty()) {
            return null;
        }

        for (AbstractAncillary abstractAncillary : ancillaries) {
            if (abstractAncillary instanceof TravelerAncillary) {
                TravelerAncillary travelerAncillary = (TravelerAncillary) abstractAncillary;

                if (travelerAncillary.getAncillary().getType().equalsIgnoreCase(ancillary.getType())
                        && travelerAncillary.getAncillary().getGroupCode().equalsIgnoreCase(ancillary.getGroupCode())) {

                    return travelerAncillary;
                }
            } else if (abstractAncillary instanceof ExtraWeightAncillary) {
                ExtraWeightAncillary extraWeightAncillary = (ExtraWeightAncillary) abstractAncillary;

                if (extraWeightAncillary.getAncillary().getType().equalsIgnoreCase(ancillary.getType())
                        && extraWeightAncillary.getAncillary().getGroupCode()
                                .equalsIgnoreCase(ancillary.getGroupCode())) {

                    return extraWeightAncillary;
                }
            } else if (abstractAncillary instanceof PaidTravelerAncillary) {
                PaidTravelerAncillary paidTravelerAncillary = (PaidTravelerAncillary) abstractAncillary;

                if (paidTravelerAncillary.getType().equalsIgnoreCase(ancillary.getType())
                        && paidTravelerAncillary.getGroupCode().equalsIgnoreCase(ancillary.getGroupCode())) {

                    return paidTravelerAncillary;
                }
            } else if (abstractAncillary instanceof AncillaryOffer) {
                AncillaryOffer ancillaryOffer = (com.aeromexico.commons.model.AncillaryOffer) abstractAncillary;
                if (ancillaryOffer.getAncillary().getType().equalsIgnoreCase(ancillary.getType())
                        && ancillaryOffer.getAncillary().getGroupCode().equalsIgnoreCase(ancillary.getGroupCode())) {

                    return ancillaryOffer;
                }

            }
        }

        return null;
    }

    public static AbstractAncillary getSameExtraWeightAncillary(Ancillary ancillary,
            List<AbstractAncillary> ancillaries) {

        if (null == ancillary || null == ancillary.getType() || ancillary.getType().trim().isEmpty()
                || null == ancillary.getGroupCode() || ancillary.getGroupCode().trim().isEmpty()) {
            return null;
        }

        for (AbstractAncillary abstractAncillary : ancillaries) {
            if (abstractAncillary instanceof ExtraWeightAncillary) {
                ExtraWeightAncillary extraWeightAncillary = (ExtraWeightAncillary) abstractAncillary;

                if (extraWeightAncillary.getAncillary().getType().equalsIgnoreCase(ancillary.getType())
                        && extraWeightAncillary.getAncillary().getGroupCode()
                                .equalsIgnoreCase(ancillary.getGroupCode())) {

                    return extraWeightAncillary;
                }
            }
        }

        return null;
    }

    /**
     *
     * @param cabinUpgradeAncillary
     * @param ancillaries
     * @return
     */
    public static AbstractAncillary getSameUpgrade(CabinUpgradeAncillary cabinUpgradeAncillary,
            List<AbstractAncillary> ancillaries) {

        if (null == cabinUpgradeAncillary || null == cabinUpgradeAncillary.getSegmentCode()) {
            return null;
        }

        for (AbstractAncillary abstractAncillary : ancillaries) {
            if (abstractAncillary instanceof CabinUpgradeAncillary) {
                CabinUpgradeAncillary cabinUpgradeAncillaryDB = (CabinUpgradeAncillary) abstractAncillary;

                if (SegmentCodeUtil.compareSegmentCodeWithoutTime(cabinUpgradeAncillaryDB.getSegmentCode(),
                        cabinUpgradeAncillary.getSegmentCode())) {
                    return cabinUpgradeAncillaryDB;
                }
            }
        }

        return null;
    }

    /**
     *
     * @param bookedTraveler
     * @param segmentCode
     */
    public static void removeCabinUpgradeAncillaryInDB(BookedTraveler bookedTraveler, String segmentCode) {

        if (null == bookedTraveler.getAncillaries() || null == bookedTraveler.getAncillaries().getCollection()
                || bookedTraveler.getAncillaries().getCollection().isEmpty()) {
            return;
        }

        Iterator<AbstractAncillary> i = bookedTraveler.getAncillaries().getCollection().iterator();
        while (i.hasNext()) {
            // must be called before you can call i.remove()
            AbstractAncillary abstractAncillaryDB = i.next();

            if (abstractAncillaryDB instanceof CabinUpgradeAncillary) {
                CabinUpgradeAncillary cabinUpgradeAncillary = (CabinUpgradeAncillary) abstractAncillaryDB;
                if (segmentCode.equalsIgnoreCase(cabinUpgradeAncillary.getSegmentCode())) {
                    i.remove();
                }
            }
        }

    }    

    /**
     *
     * @param bookedTravelerList
     */
    public static void removeUnpaidAncillariesAndUnpaidSeat(List<BookedTraveler> bookedTravelerList) {

        if (bookedTravelerList.isEmpty()) {
            return;
        }

        for (BookedTraveler bookedTraveler : bookedTravelerList) {

            if (null != bookedTraveler.getAncillaries() && null != bookedTraveler.getAncillaries().getCollection()) {

                Iterator<AbstractAncillary> ancillary = bookedTraveler.getAncillaries().getCollection().iterator();
                while (ancillary.hasNext()) {
                    // must be called before you can call i.remove()
                    AbstractAncillary abstractAncillary = ancillary.next();

                    if (abstractAncillary instanceof TravelerAncillary) {
                        TravelerAncillary travelerAncillary = (TravelerAncillary) abstractAncillary;

                        if (!travelerAncillary.isPartOfReservation()) {

                            ancillary.remove();
                        }
                    } // if ancillary is not part of the reservation that must
                    // be an
                    // unpaid ancillary (TravelerAncillary)
                }
            }

            if (null != bookedTraveler.getSegmentChoices()
                    && null != bookedTraveler.getSegmentChoices().getCollection()) {

                Iterator<AbstractSegmentChoice> seat = bookedTraveler.getSegmentChoices().getCollection().iterator();
                while (seat.hasNext()) {
                    // must be called before you can call i.remove()
                    AbstractSegmentChoice abstractSegmentChoice = seat.next();

                    if (abstractSegmentChoice.getSeat() instanceof SeatChoiceUpsell) {
                        seat.remove();
                    }
                }
            }

        }
    }

    /**
     *
     * @param ancillary
     * @param ancillaryCollection
     */
    public static void removeAncillaryFromDB(Ancillary ancillary, List<AbstractAncillary> ancillaryCollection) {

        Iterator<AbstractAncillary> i = ancillaryCollection.iterator();

        while (i.hasNext()) {
            // must be called before you can call i.remove()
            AbstractAncillary abstractAncillaryDB = i.next();

            if (abstractAncillaryDB instanceof TravelerAncillary) {
                TravelerAncillary travelerAncillary = (TravelerAncillary) abstractAncillaryDB;

                if (travelerAncillary.getAncillary().getType().equalsIgnoreCase(ancillary.getType())
                        && travelerAncillary.getAncillary().getGroupCode().equalsIgnoreCase(ancillary.getGroupCode())) {

                    i.remove();
                    break;
                }
            }
        }
    }

    public static void removeExtraWeightAncillaryFromDB(Ancillary ancillary,
            List<AbstractAncillary> ancillaryCollection) {

        Iterator<AbstractAncillary> i = ancillaryCollection.iterator();

        while (i.hasNext()) {
            // must be called before you can call i.remove()
            AbstractAncillary abstractAncillaryDB = i.next();
            if (abstractAncillaryDB instanceof ExtraWeightAncillary) {
                ExtraWeightAncillary extraWeightAncillary = (ExtraWeightAncillary) abstractAncillaryDB;

                if (extraWeightAncillary.getAncillary().getType().equalsIgnoreCase(ancillary.getType())
                        && extraWeightAncillary.getAncillary().getGroupCode()
                                .equalsIgnoreCase(ancillary.getGroupCode())) {
                    i.remove();
                    break;
                }
            } // if ancillary is not part of the reservation that must be an
            // unpaid ancillary (TravelerAncillary)
        }
    }

    /**
     *
     * @param date
     * @return
     */
    public static String getdateWithFormat(String date) {

        SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatoFechatoRQ = new SimpleDateFormat("ddMMMyyyy", Locale.ENGLISH);
        Date fechaFormato = null;

        try {
            fechaFormato = formatoFecha.parse(date);
        } catch (ParseException e) {
            throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.YOUR_DATE_FORMAT_IS_WRONG,
                    ErrorCodeDescriptions.YOUR_DATE_FORMAT_IS_WRONG + date);
        }
        String dateOfBirthWithFormat = formatoFechatoRQ.format(fechaFormato).toUpperCase();

        return dateOfBirthWithFormat;

    }

    /**
     *
     * @param segmentChoiceUpdate
     * @return
     */
    public static boolean isExitRow(SegmentChoice segmentChoiceUpdate) {

        if (segmentChoiceUpdate.getSeat() instanceof SeatChoiceUpsell) {
            SeatChoiceUpsell seatChoiceUpsell = (SeatChoiceUpsell) segmentChoiceUpdate.getSeat();
            for (SeatCharacteristicsType seatCharacteristicsType : seatChoiceUpsell.getSeatCharacteristics()) {
                if (seatCharacteristicsType.equals(SeatCharacteristicsType.EXIT_ROW)) {
                    return true;
                }
            }
        } else if (segmentChoiceUpdate.getSeat() instanceof SeatChoice) {
            SeatChoice seatChoice = (SeatChoice) segmentChoiceUpdate.getSeat();
            for (SeatCharacteristicsType seatCharacteristicsType : seatChoice.getSeatCharacteristics()) {
                if (seatCharacteristicsType.equals(SeatCharacteristicsType.EXIT_ROW)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     *
     * @param segmentCode
     * @param bookedTraveler
     */
    public static boolean moveAncillariesFromOriginalCollectionToNormalCollection(
            String segmentCode,
            BookedTraveler bookedTraveler
    ) {
        boolean restored = false;
        try {
            List<AbstractAncillary> abstractAncillaryList;
            abstractAncillaryList = bookedTraveler.getOriginalSeatAncillariesMap().get(segmentCode);

            if (null != abstractAncillaryList && !abstractAncillaryList.isEmpty()) {
                int index = bookedTraveler.getSeatAncillaries().getIndexBySegmentCode(segmentCode);
                if (index >= 0) {
                    bookedTraveler.getSeatAncillaries().getCollection().set(index, abstractAncillaryList.get(0));
                    if (abstractAncillaryList.size() > 1) {
                        bookedTraveler.getSeatAncillaries().getCollection().add(index, abstractAncillaryList.get(1));
                    }
                } else {
                    bookedTraveler.getSeatAncillaries().getCollection().addAll(abstractAncillaryList);
                }

                restored = true;
            }
            bookedTraveler.getOriginalSeatAncillariesMap().remove(segmentCode);
        } catch (Exception ex) {
            LOG.error("Error moving original ancillary: " + ex.getMessage());
        }

        try {
            AbstractSegmentChoice abstractSegmentChoice;
            abstractSegmentChoice = bookedTraveler.getOriginalSegmentChoicesMap().get(segmentCode);

            if (null != abstractSegmentChoice) {
                int index = bookedTraveler.getSegmentChoices().getIndexBySegmentCode(segmentCode);
                if (index >= 0) {
                    bookedTraveler.getSegmentChoices().getCollection().set(index, abstractSegmentChoice);
                } else {
                    bookedTraveler.getSegmentChoices().getCollection().add(abstractSegmentChoice);
                }

                restored = true;
            }
            bookedTraveler.getOriginalSegmentChoicesMap().remove(segmentCode);
        } catch (Exception ex) {
            LOG.error("Error moving original segment choice: " + ex.getMessage());
        }

        return restored;
    }

    /**
     *
     * @param segmentCode
     * @param bookedTraveler
     * @return
     */
    public static boolean removeSegmentChoiceAndSeatAncillaryBySegmentCode(String segmentCode, BookedTraveler bookedTraveler) {

        boolean removed = bookedTraveler.getSegmentChoices().removeBySegmentCode(segmentCode) > 0;
        LOG.info("SegmentChoice: {}, for pax: {}, result: {}", segmentCode, bookedTraveler.getId(), removed);

        removed |= bookedTraveler.getSeatAncillaries().removeBySegmentCode(segmentCode) > 0;
        LOG.info("SeatAncillary: {}, for pax: {}, result: {}", segmentCode, bookedTraveler.getId(), removed);

        return removed;
    }

    /**
     *
     * @param seatmapCollection
     * @param bookedLegCode
     * @param segmentCode
     * @param newCodeseat
     * @return
     * @throws Exception
     */
    public static SeatmapSeat readSeatmapCollectionWrapper(
            SeatmapCollection seatmapCollection,
            String bookedLegCode,
            String segmentCode, String newCodeseat
    ) throws Exception {

        if (null == seatmapCollection) {
            LOG.error(ErrorType.UNABLE_TO_RETRIEVE_PRICE_FOR_SEAT.getFullDescription());
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNABLE_TO_RETRIEVE_PRICE_FOR_SEAT,
                    ErrorType.UNABLE_TO_RETRIEVE_PRICE_FOR_SEAT.getFullDescription());
        }

        // read the seatmapSeat from the seatmapCollection in DB
        AbstractSeatMap abstractSeatMap = seatmapCollection.getSeatMaptByCode(segmentCode);
        SeatMap seatMapDB = null;
        if (abstractSeatMap instanceof SeatMap) {
            seatMapDB = (SeatMap) abstractSeatMap;
        }

        if (null == seatMapDB) {
            LOG.error(ErrorType.UNABLE_TO_RETRIVE_SEATMAP.getFullDescription() + " WITH SEGMENTCODE : " + segmentCode);
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNABLE_TO_RETRIVE_SEATMAP,
                    ErrorType.UNABLE_TO_RETRIVE_SEATMAP.getFullDescription() + " WITH SEGMENTCODE : " + segmentCode);
        }

        SeatmapSeat seatmapSeatDB = seatmapCollection.getSeatByCode(seatMapDB, newCodeseat);

        LOG.info("seatmapSeatDB - " + seatmapSeatDB);

        if (null == seatmapSeatDB) {
            LOG.error(ErrorType.UNABLE_TO_RETRIEVE_SEAT.getFullDescription() + " WITH SEGMENTCODE : "
                    + " - CODE SEAT : " + newCodeseat);
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNABLE_TO_RETRIEVE_SEAT,
                    ErrorType.UNABLE_TO_RETRIEVE_SEAT.getFullDescription() + " - CODE SEAT : " + newCodeseat);
        }

        return seatmapSeatDB;

    }

    /**
     *
     * @param pnr
     * @param cartId
     * @param travelerAncillaryUpdate
     * @return
     * @throws Exception
     */
    public static AncillaryOffer readAncillaryOfferOfPNRCollection(PNR pnr, String cartId,
            CabinUpgradeAncillary travelerAncillaryUpdate) throws Exception {
        if (null != pnr) {
            CartPNR cartPNR = pnr.getCartPNRByCartId(cartId);
            if (null != cartPNR) {
                    for (UpgradeAncillary upgradeAncillary : cartPNR.getUpgradeAncillaries().getCollection()) {
                        if (SegmentCodeUtil.compareSegmentCodeWithoutTime(travelerAncillaryUpdate.getSegmentCode(),
                            upgradeAncillary.getSegmentCode())) {
                            return upgradeAncillary.getAncillaryOffer();
                        }
                    }                
            }
        }
        
        return null;
    }

    /**
     *
     * @param cartPNR
     * @param acsSeatChangeRSACS
     */
    public static void UpdatePectabInPnrCollectionAfterCheckIn(CartPNR cartPNR, ACSSeatChangeRSACS acsSeatChangeRSACS) {

        PectabBoardingPassList pectabBoardingPassList = new PectabBoardingPassList();
        if (null != acsSeatChangeRSACS.getPECTABDataList().getPECTABData()) {
            for (PECTABDataListACS.PECTABData pectaBData : acsSeatChangeRSACS.getPECTABDataList().getPECTABData()) {
                PectabBoardingPass pectabDataNew = new PectabBoardingPass();
                pectabDataNew.setValue(pectaBData.getValue());
                pectabDataNew.setVersion(pectaBData.getVersion());
                pectabBoardingPassList.add(pectabDataNew);
            }
        }
        cartPNR.setPectabBoardingPassList(pectabBoardingPassList);
    }

    /**
     * *
     *
     * @param departureDateTime in the following format: yyyy-mm-ddThh:mm:ss
     * @return date only in the following format: yyyy-mm-dd
     */
    public static String getDepartureDate(String departureDateTime) {
        // TODO: validate request parameter that matches the expected format,
        // throw an exception otherwise
        return departureDateTime.substring(0, departureDateTime.indexOf("T"));
    }

    /**
     *
     * @param destinationAddress
     */
    public static void isEmptyDestinationAddress(DestinationAddress destinationAddress) {
        if (destinationAddress.getAddressOne().trim().isEmpty()) {
            throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.EMPTY_FIELDS,
                    ErrorCodeDescriptions.MSG_CODE_EXIST_EMPTY_FIELDS_ADDRESS_ONE);
        } else if (destinationAddress.getCity().trim().isEmpty()) {
            throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.EMPTY_FIELDS,
                    ErrorCodeDescriptions.MSG_CODE_EXIST_EMPTY_FIELDS_CITY);
        } else if (destinationAddress.getCountry().trim().isEmpty()) {
            throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.EMPTY_FIELDS,
                    ErrorCodeDescriptions.MSG_CODE_EXIST_EMPTY_FIELDS_COUNTRY);
        } else if (destinationAddress.getState().trim().isEmpty()) {
            throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.EMPTY_FIELDS,
                    ErrorCodeDescriptions.MSG_CODE_EXIST_EMPTY_FIELDS_STATE);
        }
    }

    /**
     *
     * @param emergencyContact
     * @throws Exception
     */
    public static void isEmptyEmergencyContact(EmergencyContact emergencyContact) throws Exception {
        if (emergencyContact.getFirstName().trim().isEmpty()) {
            throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.EMPTY_FIELDS,
                    ErrorCodeDescriptions.MSG_CODE_EXIST_EMPTY_FIELDS_FIRSTNAME);
        } else if (emergencyContact.getLastName().trim().isEmpty()) {
            throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.EMPTY_FIELDS,
                    ErrorCodeDescriptions.MSG_CODE_EXIST_EMPTY_FIELDS_LASTNAME);
        } else if (emergencyContact.getPhone() != null && emergencyContact.getPhone().getNumber().trim().isEmpty()
                && emergencyContact.getPhone().getType().toString().trim().isEmpty()) {
            throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.EMPTY_FIELDS,
                    ErrorCodeDescriptions.MSG_CODE_EXIST_EMPTY_FIELDS_PHONE);
        } else if (emergencyContact.getCountry().trim().isEmpty()) {
            throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.EMPTY_FIELDS,
                    ErrorCodeDescriptions.MSG_CODE_EXIST_EMPTY_FIELDS_COUNTRY_CONTACT);
        } else if (emergencyContact.getRelationship().trim().isEmpty()) {
            throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.EMPTY_FIELDS,
                    ErrorCodeDescriptions.MSG_CODE_EXIST_EMPTY_FIELDS_RELATIONSHIP);
        }
    }

    /**
     *
     * @param travelDocument
     * @throws Exception
     */
    public static void isEmptyTravelDocumentInfant(TravelerDocument travelDocument) throws Exception {

        if (null == travelDocument) {
            throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.EMPTY_FIELDS,
                    ErrorCodeDescriptions.MSG_CODE_EXIST_EMPTY_FIELDS_DOCUMENT_NUMBER_INFANT);
        } else if (travelDocument.getDocumentNumber().trim().isEmpty()) {
            throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.EMPTY_FIELDS,
                    ErrorCodeDescriptions.MSG_CODE_EXIST_EMPTY_FIELDS_DOCUMENT_NUMBER_INFANT);
        } else if (travelDocument.getIssuingCountry().trim().isEmpty()) {
            throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.EMPTY_FIELDS,
                    ErrorCodeDescriptions.MSG_CODE_EXIST_EMPTY_FIELDS_ISSUING_COUNTRY_INFANT);
        } else if (travelDocument.getExpirationDate().trim().isEmpty()) {
            throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.EMPTY_FIELDS,
                    ErrorCodeDescriptions.MSG_CODE_EXIST_EMPTY_FIELDS_EXPIRATION_DATE_INFANT);
        } else if (travelDocument.getNationality().trim().isEmpty()) {
            throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.EMPTY_FIELDS,
                    ErrorCodeDescriptions.MSG_CODE_EXIST_EMPTY_FIELDS_NATIONALITY_INFANT);
        }

    }

    /**
     *
     * @param travelDocument
     * @throws Exception
     */
    public static void isEmptyTravelDocument(TravelerDocument travelDocument) throws Exception {
        if (travelDocument.getDocumentNumber().trim().isEmpty()) {
            throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.EMPTY_FIELDS,
                    ErrorCodeDescriptions.MSG_CODE_EXIST_EMPTY_FIELDS_DOCUMENT_NUMBER);
        } else if (travelDocument.getIssuingCountry().trim().isEmpty()) {
            throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.EMPTY_FIELDS,
                    ErrorCodeDescriptions.MSG_CODE_EXIST_EMPTY_FIELDS_ISSUING_COUNTRY);
        } else if (travelDocument.getExpirationDate().trim().isEmpty()) {
            throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.EMPTY_FIELDS,
                    ErrorCodeDescriptions.MSG_CODE_EXIST_EMPTY_FIELDS_EXPIRATION_DATE);
        } else if (travelDocument.getNationality().trim().isEmpty()) {
            throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.EMPTY_FIELDS,
                    ErrorCodeDescriptions.MSG_CODE_EXIST_EMPTY_FIELDS_NATIONALITY);
        }

    }

    /**
     *
     * @param bookedTraveler
     * @param segmentCode
     * @return
     */
    public static boolean FindAndSaveOriginalSeat(BookedTraveler bookedTraveler, String segmentCode) {

        for (AbstractSegmentChoice abstractSegmentChoice : bookedTraveler.getSegmentChoices().getCollection()) {
            if (SegmentCodeUtil.compareSegmentCodeWithoutTime(abstractSegmentChoice.getSegmentCode(), segmentCode)) {
                return bookedTraveler.getOriginalSegmentChoicesMap().putIfNotExist(segmentCode, abstractSegmentChoice);
            }
        }
        return false;
    }

    /**
     *
     * @param bookedTraveler
     * @param segmentCode
     * @return
     */
    public static boolean FindAndSaveOriginalAncillarySeat(BookedTraveler bookedTraveler, String segmentCode) {
        List<AbstractAncillary> abstractAncillaryList = new ArrayList<>();

        for (AbstractAncillary abstractAncillaryDB : bookedTraveler.getSeatAncillaries().getCollection()) {
            if (abstractAncillaryDB instanceof TravelerAncillary) {
                TravelerAncillary travelerAncillary = (TravelerAncillary) abstractAncillaryDB;
                if (SegmentCodeUtil.compareSegmentCodeWithoutTime(travelerAncillary.getSegmentCodeAux(), segmentCode)) {
                    abstractAncillaryList.add(travelerAncillary);
                }
            } else if (abstractAncillaryDB instanceof PaidTravelerAncillary) {
                PaidTravelerAncillary paidTravelerAncillary = (PaidTravelerAncillary) abstractAncillaryDB;
                if (SegmentCodeUtil.compareSegmentCodeWithoutTime(paidTravelerAncillary.getSegmentCodeAux(),
                        segmentCode)) {
                    abstractAncillaryList.add(paidTravelerAncillary);
                }
            }
        }

        if (!abstractAncillaryList.isEmpty()) {
            return bookedTraveler.getOriginalSeatAncillariesMap().putIfNotExist(segmentCode, abstractAncillaryList);
        }

        return false;
    }

    /**
     * @param segment
     * @param bookingClass
     * @param bookedTraveler
     * @param seat
     * @param delete
     * @return
     * @throws Exception
     */
    public static ACSEditPassengerCharacteristicsRQACS getEditPassengerCharacteristicsRQSegmentChoices(
            Segment segment,
            BookingClass bookingClass,
            BookedTraveler bookedTraveler,
            String seat, boolean delete
    ) throws Exception {

        PassengerInfoRQACS passengerInfoRQACS = getPassengerSeat(
                bookingClass.getPassengerId(), bookedTraveler, seat,
                delete
        );

        PassengerInfoListRQACS passengerInfoListRQACS = new PassengerInfoListRQACS();
        passengerInfoListRQACS.getPassengerInfoRequest().add(passengerInfoRQACS);

        ACSEditPassengerCharacteristicsRQACS editPassengerCharacteristicsRQ = new ACSEditPassengerCharacteristicsRQACS();
        editPassengerCharacteristicsRQ.setPassengerInfoRequestList(passengerInfoListRQACS);

        editPassengerCharacteristicsRQ.setItinerary(getItineraryEditPassengerACS(segment, bookingClass, true));

        return editPassengerCharacteristicsRQ;
    }

    public static ACSEditPassengerCharacteristicsRQACS getEditPassengerCharacteristicsRQCode(Segment segment,
            BookingClass bookingClass, BookedTraveler bookedTraveler, String editCode) throws Exception {

        PassengerInfoRQACS passengerInfoRQACS = getPassengerInfoRQACSForAddCode(bookedTraveler, editCode, bookingClass);

        PassengerInfoListRQACS passengerInfoListRQACS = new PassengerInfoListRQACS();
        passengerInfoListRQACS.getPassengerInfoRequest().add(passengerInfoRQACS);

        ACSEditPassengerCharacteristicsRQACS editPassengerCharacteristicsRQ = new ACSEditPassengerCharacteristicsRQACS();
        editPassengerCharacteristicsRQ.setPassengerInfoRequestList(passengerInfoListRQACS);
        editPassengerCharacteristicsRQ.setItinerary(getItineraryEditPassengerACS(segment, bookingClass, false));

        return editPassengerCharacteristicsRQ;
    }

    /**
     *
     * @param passengerID
     * @param bookedTraveler
     * @param seat
     * @param delete
     * @return
     */
    public static PassengerInfoRQACS getPassengerSeat(
            String passengerID, BookedTraveler bookedTraveler,
            String seat, boolean delete
    ) {

        // Passenger Info
        PassengerInfoRQACS passengerInfoRQACS = new PassengerInfoRQACS();

        // passengerInfoRQACS.setFirstName(bookedTraveler.getFirstName());
        passengerInfoRQACS.setLastName(bookedTraveler.getLastName());
        passengerInfoRQACS.setPassengerID(passengerID);

        EditCodeDetailsListACS editCodeDetailsListACS = new EditCodeDetailsListACS();
        EditCodeDetailsListACS.PR seatsNew = new EditCodeDetailsListACS.PR();

        // Seat
        if (delete) {
            seatsNew.setActionCode("DELETE");
        } else {
            seatsNew.setActionCode("EDIT");
        }
        seatsNew.setSeat(seat);

        editCodeDetailsListACS.setPR(seatsNew);

        passengerInfoRQACS.setEditCodeList(editCodeDetailsListACS);

        return passengerInfoRQACS;
    }

    /**
     *
     * @param bookedTraveler
     * @param editCode
     * @param bookingClass
     * @return
     */
    public static PassengerInfoRQACS getPassengerInfoRQACSForAddCode(BookedTraveler bookedTraveler, String editCode,
            BookingClass bookingClass) {

        // Passenger Info
        PassengerInfoRQACS passengerInfoRQACS = new PassengerInfoRQACS();
        passengerInfoRQACS.setLastName(bookedTraveler.getLastName());
        passengerInfoRQACS.setPassengerID(bookingClass.getPassengerId());
        com.sabre.services.acs.bso.editpassengercharacteristics.v3.EditCodeDetailsListACS editCodeDetailsListACS;
        editCodeDetailsListACS = new com.sabre.services.acs.bso.editpassengercharacteristics.v3.EditCodeDetailsListACS();
        com.sabre.services.acs.bso.editpassengercharacteristics.v3.EditCodeDetailsListACS.OtherEdit otherEdit;
        otherEdit = new com.sabre.services.acs.bso.editpassengercharacteristics.v3.EditCodeDetailsListACS.OtherEdit();

        otherEdit.setActionCode("EDIT");
        otherEdit.setCode(editCode);
        editCodeDetailsListACS.getOtherEdit().add(otherEdit);
        passengerInfoRQACS.setEditCodeList(editCodeDetailsListACS);

        return passengerInfoRQACS;
    }

    /**
     * @param segment
     * @param bookingClass
     * @param setDestination
     * @return
     * @throws Exception This method is used when adding FF number and seat only
     */
    public static com.sabre.services.acs.bso.editpassengercharacteristics.v3.ItineraryACS getItineraryEditPassengerACS(
            Segment segment, BookingClass bookingClass, boolean setDestination) throws Exception {

        com.sabre.services.acs.bso.editpassengercharacteristics.v3.ItineraryACS itineraryeditpassengerACS;
        itineraryeditpassengerACS = new com.sabre.services.acs.bso.editpassengercharacteristics.v3.ItineraryACS();

        itineraryeditpassengerACS.setAirline(segment.getOperatingCarrier());
        itineraryeditpassengerACS.setFlight(segment.getOperatingFlightCode());
        itineraryeditpassengerACS.setBookingClass(bookingClass.getBookingClass());
        itineraryeditpassengerACS.setDepartureDate(ShoppingCartUtil.getDepartureDate(segment.getDepartureDateTime()));
        itineraryeditpassengerACS.setOrigin(segment.getDepartureAirport());

        if (setDestination) {
            itineraryeditpassengerACS.setDestination(segment.getArrivalAirport());
        }

        return itineraryeditpassengerACS;
    }

    /**
     *
     * @param in
     * @param size
     * @return
     */
    public static String formatStringUpperCaseSize(String in, int size) {
        if (null == in) {
            return null;
        }

        String formatted = in.trim().toUpperCase();

        if (formatted.length() > size) {
            formatted = formatted.substring(0, size);
        }

        return formatted;
    }

    /**
     *
     * @param in
     * @param size
     * @return
     */
    public static String formatStringSize(String in, int size) {
        if (null == in) {
            return null;
        }

        String formatted = in.trim();

        if (formatted.length() > size) {
            formatted = formatted.substring(0, size);
        }

        return formatted;
    }

    /**
     *
     * @param bookedTraveler
     * @param legCode
     * @return
     */
    public static FreeBaggageAllowancePerLeg getFreeBaggageAllowancePerLeg(BookedTraveler bookedTraveler,
            String legCode) {
        if (null == bookedTraveler.getFreeBaggageAllowancePerLegsArray()
                || bookedTraveler.getFreeBaggageAllowancePerLegsArray().isEmpty()) {
            return null;
        }
        List<FreeBaggageAllowancePerLeg> freeBaggageAllowancePerLegsArray = bookedTraveler
                .getFreeBaggageAllowancePerLegsArray();
        for (FreeBaggageAllowancePerLeg freeBaggageAllowancePerLeg : freeBaggageAllowancePerLegsArray) {

            if (freeBaggageAllowancePerLeg.getLegCode().equalsIgnoreCase(legCode)) {
                return freeBaggageAllowancePerLeg;
            }

        }
        return null;
    }

    public static boolean isPaymentRequired(BookedTraveler bookedTraveler, SeatUpgradeType seatUpgradeType) {
        if (null != bookedTraveler.getBookedTravellerBenefitCollection()
                && null != bookedTraveler.getBookedTravellerBenefitCollection().getCollection()) {
            for (BookedTravelerBenefit bookedTravelerBenefit : bookedTraveler.getBookedTravellerBenefitCollection().getCollection()) {
                if (null != bookedTravelerBenefit.getSeat()) {
                    if (null != bookedTravelerBenefit.getSeat().getFeeRequired()) {
                        if (SeatUpgradeType.AMPLUS_UPGRADE == seatUpgradeType) {
                            return bookedTravelerBenefit.getSeat().getFeeRequired().isAmPlusUgrade();
                        } else if (SeatUpgradeType.PREFERRED_UPGRADE == seatUpgradeType
                                || SeatUpgradeType.EXIT_ROW_UPGRADE == seatUpgradeType) {
                            return bookedTravelerBenefit.getSeat().getFeeRequired().isPrefferedUpgrade();
                        }
                    }

                }
            }

        }
        return true;
    }
  
}
