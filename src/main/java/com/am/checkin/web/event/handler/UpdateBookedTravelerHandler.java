package com.am.checkin.web.event.handler;

import com.aeromexico.dao.services.IPNRCollectionDao;
import java.io.Serializable;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Observes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.am.checkin.web.event.model.UpdateBookedTravelerEvent;
import javax.inject.Inject;

/**
 *
 * @author adrian
 */
@Stateless(mappedName = "UpdateBookedTravelerHandler")
public class UpdateBookedTravelerHandler implements IUpdateBookedTravelerHandler, Serializable {

    /**
     * Serial version for class
     */
    private static final long serialVersionUID = -4994020217782964355L;

    private static final Logger logger = LoggerFactory.getLogger(UpdateBookedTravelerHandler.class);

    @Inject
    private IPNRCollectionDao pnrCollectionDao;

    @Override
    @Asynchronous
    public void onUpdateBookedTraveler(@Observes UpdateBookedTravelerEvent event) {
        try {
            int code = pnrCollectionDao.updateBookedTravellerOnPNRCollectionAfterCheckin(event.getBookedTraveler().getPnrPassId(), event.getPnrIndex(), event.getCartIndex(), event.getPassengerIndex(), event.getBookedTraveler().toString());
            logger.info("DB code:" + code);
        } catch (Exception ex) {
            logger.info("Error updating data base: " + ex.getMessage());
        }
    }
}
