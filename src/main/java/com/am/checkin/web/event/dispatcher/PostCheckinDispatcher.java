package com.am.checkin.web.event.dispatcher;

import com.am.checkin.web.event.model.PostCheckinEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;

@Named("PostCheckinDispatcher")
@Stateless
public class PostCheckinDispatcher {

    private static final Logger logger = LoggerFactory.getLogger(PostCheckinDispatcher.class);

    @Inject
    private Event<PostCheckinEvent> eventManager;

    @Asynchronous
    public void publish(PostCheckinEvent event) {

        if (null != eventManager) {
            logger.info("Post Checkin Event Manager Fired");
            eventManager.fire(event);
        }

    }

}
