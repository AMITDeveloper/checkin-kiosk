package com.am.checkin.web.event.handler;

import com.aeromexico.dao.services.IPNRCollectionDao;
import java.io.Serializable;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Observes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.am.checkin.web.event.model.UpdatePaymentLinkedListEvent;
import javax.inject.Inject;

/**
 *
 * @author adrian
 */
@Stateless(mappedName = "UpdatePaymentLinkedListHandler")
public class UpdatePaymentLinkedListHandler implements IUpdatePaymentLinkedListHandler, Serializable {

    /**
     * Serial version for class
     */
    private static final long serialVersionUID = -1088202373530398117L;

    private static final Logger logger = LoggerFactory.getLogger(UpdatePaymentLinkedListHandler.class);

    @Inject
    private IPNRCollectionDao pnrCollectionDao;

    @Override
    @Asynchronous
    public void onUpdatePaymentLinkedList(@Observes UpdatePaymentLinkedListEvent event) {
        try {
            int code = pnrCollectionDao.updatePaymentLinkedListOnPNRCollectionAfterCheckin(event.getRecordLocator(), event.getLegCode(), event.getPnrIndex(), event.getCartIndex(), event.getPaymentLinkedList().toString());
            logger.info("DB code:" + code);
        } catch (Exception ex) {
            logger.info("Error updating data base: " + ex.getMessage());
        }
    }
}
