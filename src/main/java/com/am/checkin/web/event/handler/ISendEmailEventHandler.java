package com.am.checkin.web.event.handler;

import com.am.checkin.web.event.model.SendEmailEvent;

public interface ISendEmailEventHandler {

    public void onSendEmailPublish(SendEmailEvent event);
}
