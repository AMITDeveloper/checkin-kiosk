package com.am.checkin.web.event.dispatcher;

import com.aeromexico.commons.model.PNRCollection;
import com.am.checkin.web.event.model.BoardingPassesEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;

@Named("BoardingPassesDispatcher")
@Stateless
public class BoardingPassesDispatcher {

    private static final Logger logger = LoggerFactory.getLogger(BoardingPassesDispatcher.class);

    @Inject
    private Event<BoardingPassesEvent> eventManager;

    @Asynchronous
    public void publish(PNRCollection pnrCollection) {

        BoardingPassesEvent event = new BoardingPassesEvent(pnrCollection);

        if (null != eventManager) {
            logger.info("Classifier Event Manager Fired" + pnrCollection.getCollection().get(0).getPnr());
            eventManager.fire(event);
        }

    }

}
