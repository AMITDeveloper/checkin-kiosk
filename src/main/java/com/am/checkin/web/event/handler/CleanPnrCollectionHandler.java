package com.am.checkin.web.event.handler;

import com.aeromexico.commons.web.util.Constants;
import com.aeromexico.dao.services.PNRLookupDao;
import com.am.checkin.web.event.model.CleanPnrCollectionEvent;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import com.am.checkin.web.event.qualifiers.CleanPnrCollectionQ;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author adrian
 */
@Stateless(mappedName = "CleanPnrCollectionHandler")
public class CleanPnrCollectionHandler implements ICleanPnrCollectionHandler, Serializable {

    private static final Logger logger = LoggerFactory.getLogger(CleanPnrCollectionHandler.class);
    private static final long serialVersionUID = 1L;

    @Inject
    private PNRLookupDao pnrLookupDao;

    public boolean CLEAN_PNR_COLLECTION_AFTER_CHECKIN;

    @PostConstruct
    public void init() {
        try {
            String clean = System.getProperty("clean_pnr_collection_after_checkin");

            if (null == clean || clean.trim().isEmpty()) {
                CLEAN_PNR_COLLECTION_AFTER_CHECKIN = true;
            } else {
                CLEAN_PNR_COLLECTION_AFTER_CHECKIN = Boolean.valueOf(clean);
            }
        } catch (Exception ex) {
            CLEAN_PNR_COLLECTION_AFTER_CHECKIN = true;
        }
    }

    @Override
    @Asynchronous
    public void onCleanPnrCollection(@Observes @CleanPnrCollectionQ CleanPnrCollectionEvent event) {

        if (CLEAN_PNR_COLLECTION_AFTER_CHECKIN) {
            try {
                List<String> expiredIdAfterCheckinList = pnrLookupDao.getExpiredPnrAfterCheckinCollections(event.getNow());
                if (null != expiredIdAfterCheckinList && !expiredIdAfterCheckinList.isEmpty()) {
                    try {
                        int code = pnrLookupDao.cleanPnrCollectionAfterCheckin(expiredIdAfterCheckinList);
                        logger.info("Collections in pnrCollectionAfterCheckin deleted: {}", code);
                    } catch (Exception ex) {
                        logger.error("Error cleaning pnrCollectionAfterCheckin collections: {} \n {}", ex, ex.getMessage());
                    }
                } else {
                    logger.info("There is no expired pnr after checkin collections");
                }
            } catch (Exception ex) {
                logger.error("Error cleaning collections: {} \n {}", ex, ex.getMessage());
            }
        }

        try {
            List<String> expiredIdList = pnrLookupDao.getExpiredPnrCollections(event.getNow());

            int code;

            if (null != expiredIdList && !expiredIdList.isEmpty()) {
                try {
                    code = pnrLookupDao.cleanPnrCollecion(expiredIdList);
                    logger.info("Collections in pnrCollection deleted: {}", code);
                } catch (Exception ex) {
                    logger.error("Error cleaning pnr collections: {} \n {} ", ex, ex.getMessage());
                }

                try {
                    code = pnrLookupDao.cleanAncillariesCollecion(expiredIdList);
                    logger.info("Collections in ancillaries wrapper deleted: {}", code);
                } catch (Exception ex) {
                    logger.error("Error cleaning ancillaries wrapper collections: {} \n {}", ex, ex.getMessage());
                }

                try {
                    code = pnrLookupDao.cleanChubbAncillariesCollection(expiredIdList);
                    logger.info("Collections in ancillaries wrapper deleted: {}", code);
                } catch (Exception ex) {
                    logger.error("Error cleaning ancillaries wrapper collections: {} \n {}", ex, ex.getMessage());
                }

                try {
                    code = pnrLookupDao.cleanSeatMapCollecion(expiredIdList);
                    logger.info("Collections in seatmap wrapper deleted: {}", code);
                } catch (Exception ex) {
                    logger.error("Error cleaning seatmap wrapper collections: {} \n {}", ex, ex.getMessage());
                }

                try {
                    code = pnrLookupDao.cleanCollecion(expiredIdList, Constants.HERTZ_COLL);
                    logger.info("Collections in seatmap wrapper deleted: {} ", code);
                } catch (Exception ex) {
                    logger.error("Error cleaning seatmap wrapper collections: {} \n {} ", ex, ex.getMessage());
                }
            } else {
                logger.info("There is no expired pnr collections");
            }

        } catch (Exception ex) {
            logger.error("Error cleaning collections: {} \n {}", ex, ex.getMessage());
        }
    }
}
