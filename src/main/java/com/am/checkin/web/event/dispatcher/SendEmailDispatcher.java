package com.am.checkin.web.event.dispatcher;

import com.am.checkin.web.event.model.SendEmailEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.MultivaluedMap;

@Named("SendEmailCheckinDispatcher")
@Stateless
public class SendEmailDispatcher {

    private static final Logger logger = LoggerFactory.getLogger(SendEmailDispatcher.class);

    @Inject
    private Event<SendEmailEvent> eventManager;

    @Asynchronous
    public void publish(String endpoint, String body, MultivaluedMap<String, Object> queryParams) {

        SendEmailEvent event = new SendEmailEvent(endpoint, body, queryParams);

        if (null != eventManager) {
            logger.info("Send Email Event Fired");
            eventManager.fire(event);
        }

    }

}
