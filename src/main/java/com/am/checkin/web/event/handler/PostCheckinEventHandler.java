package com.am.checkin.web.event.handler;

import com.am.checkin.web.event.model.PostCheckinEvent;
import com.am.checkin.web.service.UpdateCollectionAfterCheckinOnDB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import java.io.Serializable;

@Stateless(mappedName = "PostCheckinEventHandler")
public class PostCheckinEventHandler implements IPostCheckinEventHandler, Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger logger = LoggerFactory.getLogger(PostCheckinEventHandler.class);

    @Inject
    private UpdateCollectionAfterCheckinOnDB updateCollectionAfterCheckinOnDB;

    @Override
    @Asynchronous
    public void onPostCheckinPublish(@Observes PostCheckinEvent event) {
        try {

            int result = updateCollectionAfterCheckinOnDB.synchronousCartUpdate(event.getPnrCollection(), event.getLegCode(), event.getRecordLocator(), event.getCartId());

            if (event.isClean()) {
                try {
                    //TODO: delete normal collection
                } catch (Exception ex) {
                    //
                }
            }

        } catch (Exception ex) {
            logger.info("Error processing post checkin: " + ex.getMessage());
        }
    }

    /**
     * @return the updateCollectionAfterCheckinOnDB
     */
    public UpdateCollectionAfterCheckinOnDB getUpdateCollectionAfterCheckinOnDB() {
        return updateCollectionAfterCheckinOnDB;
    }

    /**
     * @param updateCollectionAfterCheckinOnDB the
     * updateCollectionAfterCheckinOnDB to set
     */
    public void setUpdateCollectionAfterCheckinOnDB(UpdateCollectionAfterCheckinOnDB updateCollectionAfterCheckinOnDB) {
        this.updateCollectionAfterCheckinOnDB = updateCollectionAfterCheckinOnDB;
    }

}
