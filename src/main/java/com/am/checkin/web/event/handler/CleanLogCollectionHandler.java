package com.am.checkin.web.event.handler;

import com.aeromexico.commons.web.util.Constants;
import com.aeromexico.dao.services.IPNRLookupDao;
import com.am.checkin.web.event.model.CleanLogCollectionEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import java.io.Serializable;

/**
 *
 * @author adrian
 */
@Stateless(mappedName = "CleanLogCollectionHandler")
public class CleanLogCollectionHandler implements ICleanLogCollectionHandler, Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(CleanLogCollectionHandler.class);
    private static final long serialVersionUID = 1L;
    private static final String LOG_SUCCESS_MSG ="Collection: {}, documents deleted: {} ";
    private static final String LOG_ERROR_MSG ="Error cleanning {} : {}";

    @Inject
    private IPNRLookupDao pnrLookupDao;

    @Override
    @Asynchronous
    public void onCleanLogCollection(@Observes CleanLogCollectionEvent event) {        
                
        try {
            int code = pnrLookupDao.cleanCollecion(event.getNow(), Constants.PNR_LOOKUP_LOG_COLLECTION);

            LOG.info(LOG_SUCCESS_MSG, Constants.PNR_LOOKUP_LOG_COLLECTION , code);
        } catch (Exception ex) {
            LOG.info(LOG_ERROR_MSG, Constants.PNR_LOOKUP_LOG_COLLECTION, ex.getMessage());
        }

        try {
            int code = pnrLookupDao.cleanCollecion(event.getNow(), Constants.PASSENGERS_LIST_LOG_COLLECTION);

            LOG.info(LOG_SUCCESS_MSG , Constants.PASSENGERS_LIST_LOG_COLLECTION , code);
        } catch (Exception ex) {
            LOG.info(LOG_ERROR_MSG , Constants.PASSENGERS_LIST_LOG_COLLECTION , ex.getMessage());
        }

        try {

            int code = pnrLookupDao.cleanCollecion(event.getNow(), Constants.CHECKIN_LOG_COLLECTION);

            LOG.info(LOG_SUCCESS_MSG , Constants.CHECKIN_LOG_COLLECTION , code);
        } catch (Exception ex) {
            LOG.info(LOG_ERROR_MSG , Constants.CHECKIN_LOG_COLLECTION , ex.getMessage());
        }

        try {

            int code = pnrLookupDao.cleanCollecion(event.getNow(), Constants.BOARDING_PASS_LOG_COLLECTION);

            LOG.info(LOG_SUCCESS_MSG , Constants.BOARDING_PASS_LOG_COLLECTION , + code);
        } catch (Exception ex) {
            LOG.info(LOG_ERROR_MSG, Constants.BOARDING_PASS_LOG_COLLECTION , ex.getMessage());
        }

        try {

            int code = pnrLookupDao.cleanCollecion(event.getNow(), Constants.SHOPPING_CART_LOG_COLLECTION);

            LOG.info(LOG_SUCCESS_MSG , Constants.SHOPPING_CART_LOG_COLLECTION , code);
        } catch (Exception ex) {
            LOG.info(LOG_ERROR_MSG, Constants.SHOPPING_CART_LOG_COLLECTION , ex.getMessage());
        }                

        try {

            int code = pnrLookupDao.cleanCollecion(event.getNow(), Constants.PRE_CHECKIN_LOG_COLLECTION);

            LOG.info(LOG_SUCCESS_MSG , Constants.PRE_CHECKIN_LOG_COLLECTION , code);
        } catch (Exception ex) {
            LOG.info(LOG_ERROR_MSG, Constants.PRE_CHECKIN_LOG_COLLECTION , ex.getMessage());
        }

        try {

            int code = pnrLookupDao.cleanCollecion(event.getNow(), Constants.PAYMENT_AUTH_LOG_COLLECTION);

            LOG.info(LOG_SUCCESS_MSG , Constants.PAYMENT_AUTH_LOG_COLLECTION , code);
        } catch (Exception ex) {
            LOG.info(LOG_ERROR_MSG, Constants.PAYMENT_AUTH_LOG_COLLECTION , ex.getMessage());
        }

        try {

            int code = pnrLookupDao.cleanCollecion(event.getNow(), Constants.EMD_LOG_COLLECTION);

            LOG.info(LOG_SUCCESS_MSG , Constants.EMD_LOG_COLLECTION , code);
        } catch (Exception ex) {
            LOG.info(LOG_ERROR_MSG, Constants.EMD_LOG_COLLECTION , ex.getMessage());
        }

        try {

            int code = pnrLookupDao.cleanCollecion(event.getNow(), Constants.AE_LOG_COLLECTION);

            LOG.info(LOG_SUCCESS_MSG , Constants.AE_LOG_COLLECTION , code);
        } catch (Exception ex) {
            LOG.info(LOG_ERROR_MSG, Constants.AE_LOG_COLLECTION , ex.getMessage());
        }

        try {

            int code = pnrLookupDao.cleanCollecion(event.getNow(), Constants.SEATMAP_LOG_COLLECTION);

            LOG.info(LOG_SUCCESS_MSG , Constants.SEATMAP_LOG_COLLECTION , code);
        } catch (Exception ex) {
            LOG.info(LOG_ERROR_MSG, Constants.SEATMAP_LOG_COLLECTION , ex.getMessage());
        }

        try {

            int code = pnrLookupDao.cleanCollecion(event.getNow(), Constants.ANCILLARIES_LOG_COLLECTION);

            LOG.info(LOG_SUCCESS_MSG , Constants.ANCILLARIES_LOG_COLLECTION , code);
        } catch (Exception ex) {
            LOG.info(LOG_ERROR_MSG, Constants.ANCILLARIES_LOG_COLLECTION , ex.getMessage());
        }

        try {

            int code = pnrLookupDao.cleanCollecion(event.getNow(), Constants.PING_LOG_COLLECTION);

            LOG.info(LOG_SUCCESS_MSG , Constants.PING_LOG_COLLECTION , code);
        } catch (Exception ex) {
            LOG.info(LOG_ERROR_MSG, Constants.PING_LOG_COLLECTION , ex.getMessage());
        }

        try {

            int code = pnrLookupDao.cleanCollecion(event.getNow(), Constants.BAGTAG_LOG_COLLECTION);

            LOG.info(LOG_SUCCESS_MSG , Constants.BAGTAG_LOG_COLLECTION , code);
        } catch (Exception ex) {
            LOG.info(LOG_ERROR_MSG, Constants.BAGTAG_LOG_COLLECTION , ex.getMessage());
        }

        try {

            int code = pnrLookupDao.cleanCollecion(event.getNow(), Constants.PINPAD_AUTH_LOG_COLLECTION);

            LOG.info(LOG_SUCCESS_MSG , Constants.PINPAD_AUTH_LOG_COLLECTION , code);
        } catch (Exception ex) {
            LOG.info(LOG_ERROR_MSG, Constants.PINPAD_AUTH_LOG_COLLECTION , ex.getMessage());
        }
        
        try {

            int code = pnrLookupDao.cleanCollecion(event.getNow(), Constants.BOOKING_PURCHASE_LOG_COLLECTION);

            LOG.info(LOG_SUCCESS_MSG , Constants.BOOKING_PURCHASE_LOG_COLLECTION, code);
        } catch (Exception ex) {
            LOG.info(LOG_ERROR_MSG, Constants.BOOKING_PURCHASE_LOG_COLLECTION , ex.getMessage());
        }
        
        try {

            int code = pnrLookupDao.cleanCollecion(event.getNow(), Constants.BOOKING_SHOPPINGCART_LOG_COLLECTION);

            LOG.info(LOG_SUCCESS_MSG, Constants.BOOKING_SHOPPINGCART_LOG_COLLECTION , code);
        } catch (Exception ex) {
            LOG.info(LOG_ERROR_MSG, Constants.BOOKING_SHOPPINGCART_LOG_COLLECTION , ex.getMessage());
        }

    }
}
