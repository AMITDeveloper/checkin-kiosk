package com.am.checkin.web.event.dispatcher;

import com.am.checkin.web.event.model.CleanPnrCollectionEvent;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;

import com.am.checkin.web.event.qualifiers.CleanPnrCollectionQ;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adrian
 */
@Named("CleanPnrCollectionDispatcher")
@Stateless
public class CleanPnrCollectionDispatcher {

    private static final Logger logger = LoggerFactory.getLogger(CleanPnrCollectionDispatcher.class);

    @Inject
    @CleanPnrCollectionQ
    private Event<CleanPnrCollectionEvent> eventManager;

    /**
     *
     * @param cleanPnrCollectionEvent
     */
    @Asynchronous
    public void publish(CleanPnrCollectionEvent cleanPnrCollectionEvent) {
        logger.info("Before clean pnr collection");

        if (null != eventManager) {
            eventManager.fire(cleanPnrCollectionEvent);
        }

        logger.info("After clean  pnr collection");

    }
}
