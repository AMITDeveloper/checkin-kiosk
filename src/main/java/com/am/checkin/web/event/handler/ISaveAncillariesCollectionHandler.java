package com.am.checkin.web.event.handler;

import com.am.checkin.web.event.model.SaveAncillariesCollectionEvent;

/**
 *
 * @author adrian
 */
public interface ISaveAncillariesCollectionHandler {

    public void onSaveAncillariesCollection(SaveAncillariesCollectionEvent event);
}
