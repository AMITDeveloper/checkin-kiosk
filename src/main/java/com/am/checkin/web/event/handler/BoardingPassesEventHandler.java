package com.am.checkin.web.event.handler;

import com.aeromexico.commons.bigquery.services.BigQueryService;
import com.aeromexico.commons.model.mailing.BigQueryTableEnum;
import com.aeromexico.commons.model.mailing.DocumentTypeEnum;
import static com.aeromexico.commons.model.mailing.MailingConstants.NULL_PNR;
import static com.aeromexico.commons.model.mailing.MailingConstants.NULL_TRANSACTION;
import com.aeromexico.commons.web.types.PosType;
import com.am.checkin.web.event.model.BoardingPassesEvent;
import com.am.checkin.web.service.EmailService;
import java.io.Serializable;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless(mappedName = "BoardingPassesEventHandler")
public class BoardingPassesEventHandler implements IBoardingPassesEventHandler, Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger logger = LoggerFactory.getLogger(BoardingPassesEventHandler.class);

    @Inject
    private EmailService emailService;

    @Override
    @Asynchronous
    public void onBoardingPassesPublish(@Observes BoardingPassesEvent event) {
            logger.info("Check pnr output: \n {}", event.getPnrCollection());

            try {
                emailService.createBoardingPass(event.getPnrCollection());
                logger.info("SEND BOARDING PASS REQUEST");
            } catch (Exception e) {
                logger.info("BOARDING PASS NOT CREATED: {}" , e.getMessage());
                sendBigQueryNotification(event, e);
                logger.info("Error sending email confirmation: {}" , e.getMessage());
            }
    }

    public void sendBigQueryNotification(BoardingPassesEvent event, Exception e){
        try {
            BigQueryService.notReceivedNotification(event.getPnrCollection().getRecordLocator()!=null?
                            event.getPnrCollection().getRecordLocator():NULL_PNR,
                    event.getPnrCollection().getCartId()!=null?
                            event.getPnrCollection().getCartId():NULL_TRANSACTION,
                    "",
                    System.currentTimeMillis(),
                    DocumentTypeEnum.getDocumentTypeEnum(
                            event.getPnrCollection().getPos() != null ?
                                    event.getPnrCollection().getPos() : PosType.WEB.toString(),
                            DocumentTypeEnum.BOARDING_PASS).toString(),
                    BigQueryTableEnum.DOCUMENT_CREATION.toString(),
                    false,
                    e.getMessage(),
                    emailService.getEnvironment());
        } catch (Exception ex) {
            logger.error("ERROR TO SEND NOTIFCATION BIG QUERY: {}", ex.getMessage());
            logger.info("Error sending email confirmation: {}" , e.getMessage());
        }
    }

    public EmailService getEmailService() {
        return emailService;
    }

    public void setEmailService(EmailService emailService) {
        this.emailService = emailService;
    }
}
