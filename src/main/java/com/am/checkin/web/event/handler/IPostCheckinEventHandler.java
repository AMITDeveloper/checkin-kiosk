package com.am.checkin.web.event.handler;

import com.am.checkin.web.event.model.PostCheckinEvent;

public interface IPostCheckinEventHandler {

    public void onPostCheckinPublish(PostCheckinEvent event);
}
