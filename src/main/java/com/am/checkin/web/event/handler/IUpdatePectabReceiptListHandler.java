package com.am.checkin.web.event.handler;

import com.am.checkin.web.event.model.UpdatePectabReceiptListEvent;

/**
 *
 * @author adrian
 */
public interface IUpdatePectabReceiptListHandler {

    public void onUpdatePectabReceiptList(UpdatePectabReceiptListEvent event);
}
