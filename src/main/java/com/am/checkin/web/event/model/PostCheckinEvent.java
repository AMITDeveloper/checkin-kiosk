package com.am.checkin.web.event.model;

import com.aeromexico.commons.model.PNRCollection;

import java.io.Serializable;

public class PostCheckinEvent implements Serializable {

    private static final long serialVersionUID = 1L;

    private final PNRCollection pnrCollection;
    private final String recordLocator;
    private final String cartId;
    private final String legCode;
    private final boolean clean;

    /**
     *
     * @param pnrCollection
     * @param recordLocator
     * @param cartId
     * @param legCode
     * @param clean
     */
    public PostCheckinEvent(PNRCollection pnrCollection, String recordLocator, String cartId, String legCode, boolean clean) {
        this.pnrCollection = pnrCollection;
        this.recordLocator = recordLocator;
        this.cartId = cartId;
        this.legCode = legCode;
        this.clean = clean;
    }

    public PNRCollection getPnrCollection() {
        return pnrCollection;
    }

    /**
     * @return the recordLocator
     */
    public String getRecordLocator() {
        return recordLocator;
    }

    /**
     * @return the cartId
     */
    public String getCartId() {
        return cartId;
    }

    /**
     * @return the legCode
     */
    public String getLegCode() {
        return legCode;
    }

    /**
     * @return the clean
     */
    public boolean isClean() {
        return clean;
    }
}
