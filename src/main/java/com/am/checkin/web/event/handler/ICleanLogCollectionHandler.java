package com.am.checkin.web.event.handler;

import com.am.checkin.web.event.model.CleanLogCollectionEvent;

/**
 *
 * @author adrian
 */
public interface ICleanLogCollectionHandler {

    public void onCleanLogCollection(CleanLogCollectionEvent event);
}
