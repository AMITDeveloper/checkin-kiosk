package com.am.checkin.web.event.model;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author adrian
 */
public class CleanAncillariesCollectionEvent implements Serializable {

    private static final long serialVersionUID = 1L;

    private final Date now;

    public CleanAncillariesCollectionEvent(Date now) {
        this.now = now;
    }

    /**
     * @return the seatmapCollectionWrapper
     */
    public Date getNow() {
        return now;
    }
}
