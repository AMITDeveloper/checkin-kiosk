package com.am.checkin.web.event.dispatcher;

import com.am.checkin.web.event.model.UpdatePNRCollectionAfterCheckinEvent;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adrian
 */
@Named("UpdatePNRCollectionAfterCheckinDispatcher")
@Stateless
public class UpdatePNRCollectionAfterCheckinDispatcher {

    private static final Logger logger = LoggerFactory.getLogger(UpdatePNRCollectionAfterCheckinDispatcher.class);

    @Inject
    private Event<UpdatePNRCollectionAfterCheckinEvent> eventManager;

    /**
     *
     * @param updatePNRCollectionAfterCheckinEvent
     */
    @Asynchronous
    public void publish(UpdatePNRCollectionAfterCheckinEvent updatePNRCollectionAfterCheckinEvent) {
        logger.info("Before update PNRCollection");

        if (null != eventManager) {
            eventManager.fire(updatePNRCollectionAfterCheckinEvent);
        }

        logger.info("After update PNRCollection");

    }
}
