package com.am.checkin.web.event.handler;

import com.aeromexico.dao.services.IPNRCollectionDao;
import java.io.Serializable;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Observes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.am.checkin.web.event.model.UpdatePectabBoardingPassListEvent;
import javax.inject.Inject;

/**
 *
 * @author adrian
 */
@Stateless(mappedName = "UpdatePectabBoardingPassListHandler")
public class UpdatePectabBoardingPassListHandler implements IUpdatePectabBoardingPassListHandler, Serializable {

    /**
     * Serial version for class
     */
    private static final long serialVersionUID = 1741751336095174239L;

    private static final Logger logger = LoggerFactory.getLogger(UpdatePectabBoardingPassListHandler.class);

    @Inject
    private IPNRCollectionDao pnrCollectionDao;

    @Override
    @Asynchronous
    public void onUpdatePectabBoardingPassList(@Observes UpdatePectabBoardingPassListEvent event) {
        try {
            int code = pnrCollectionDao.updatePectabBoardingPassListOnPNRCollectionAfterCheckin(event.getRecordLocator(), event.getLegCode(), event.getPnrIndex(), event.getCartIndex(), event.getPectabBoardingPassList().toString());
            logger.info("DB code:" + code);
        } catch (Exception ex) {
            logger.info("Error updating data base: " + ex.getMessage());
        }
    }
}
