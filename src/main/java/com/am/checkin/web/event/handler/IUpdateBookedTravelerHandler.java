package com.am.checkin.web.event.handler;

import com.am.checkin.web.event.model.UpdateBookedTravelerEvent;

/**
 *
 * @author adrian
 */
public interface IUpdateBookedTravelerHandler {

    public void onUpdateBookedTraveler(UpdateBookedTravelerEvent event);
}
