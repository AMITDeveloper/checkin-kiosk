package com.am.checkin.web.event.handler;

import com.aeromexico.dao.services.IPNRLookupDao;
import com.am.checkin.web.event.model.UpdatePNRCollectionAfterCheckinEvent;
import java.io.Serializable;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adrian
 */
@Stateless(mappedName = "UpdatePNRCollectionAfterCheckinHandler")
public class UpdatePNRCollectionAfterCheckinHandler implements IUpdatePNRCollectionAfterCheckinHandler, Serializable {

    private static final Logger logger = LoggerFactory.getLogger(UpdatePNRCollectionAfterCheckinHandler.class);
    private static final long serialVersionUID = 1L;

    @Inject
    private IPNRLookupDao pnrLookupDao;

    @Override
    @Asynchronous
    public void onUpdatePNRCollection(@Observes UpdatePNRCollectionAfterCheckinEvent event) {
        try {
            //Thread.sleep(20000);
            int code = pnrLookupDao.savePNRCollectionAfterCheckin(event.getPnrCollection());

            logger.info("DB code:" + code);
        } catch (Exception ex) {
            logger.info("Error updating data base: " + ex.getMessage());
        }
    }
}
