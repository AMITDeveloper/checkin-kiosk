package com.am.checkin.web.event.handler;

import com.am.checkin.web.event.model.CleanAncillariesCollectionEvent;

/**
 *
 * @author adrian
 */
public interface ICleanAncillariesCollectionHandler {

    public void onCleanAncillariresCollection(CleanAncillariesCollectionEvent event);
}
