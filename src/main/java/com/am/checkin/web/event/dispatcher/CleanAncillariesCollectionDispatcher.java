package com.am.checkin.web.event.dispatcher;

import com.am.checkin.web.event.model.CleanAncillariesCollectionEvent;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adrian
 */
@Named("CleanAncillariesCollectionDispatcher")
@Stateless
public class CleanAncillariesCollectionDispatcher {

    private static final Logger logger = LoggerFactory.getLogger(CleanAncillariesCollectionDispatcher.class);

    @Inject
    private Event<CleanAncillariesCollectionEvent> eventManager;

    /**
     *
     * @param cleanAncillariesCollectionEvent
     */
    @Asynchronous
    public void publish(CleanAncillariesCollectionEvent cleanAncillariesCollectionEvent) {
        logger.info("Before clean Seatmap");

        if (null != eventManager) {
            eventManager.fire(cleanAncillariesCollectionEvent);
        }

        logger.info("After clean Seatmap");

    }
}
