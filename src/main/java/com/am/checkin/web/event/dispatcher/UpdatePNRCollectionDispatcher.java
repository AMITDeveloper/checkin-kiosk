package com.am.checkin.web.event.dispatcher;

import com.am.checkin.web.event.model.UpdatePNRCollectionEvent;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adrian
 */
@Named("UpdatePNRCollectionDispatcher")
@Stateless
public class UpdatePNRCollectionDispatcher {

    private static final Logger logger = LoggerFactory.getLogger(UpdatePNRCollectionDispatcher.class);

    @Inject
    private Event<UpdatePNRCollectionEvent> eventManager;

    /**
     * 
     * @param updatePNRCollectionEvent 
     */    
    @Asynchronous
    public void publish(UpdatePNRCollectionEvent updatePNRCollectionEvent) {
        logger.info("Before update PNRCollection");


        if (null != eventManager) {
            eventManager.fire(updatePNRCollectionEvent);
        }

        logger.info("After update PNRCollection");

    }
}
