package com.am.checkin.web.event.handler;

import com.aeromexico.dao.services.IPNRLookupDao;
import com.am.checkin.web.event.model.UpdatePNRCollectionEvent;
import java.io.Serializable;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adrian
 */
@Stateless(mappedName = "UpdatePNRCollectionHandler")
public class UpdatePNRCollectionHandler implements IUpdatePNRCollectionHandler, Serializable {

    private static final Logger logger = LoggerFactory.getLogger(UpdatePNRCollectionHandler.class);
    private static final long serialVersionUID = 1L;

    @Inject
    private IPNRLookupDao pnrLookupDao;

    @Override
    @Asynchronous
    public void onUpdatePNRCollection(@Observes UpdatePNRCollectionEvent event) {
        try {
            int code = pnrLookupDao.savePNRCollection(event.getPnrCollection());

            logger.info("DB code:" + code);
        } catch (Exception ex) {
            logger.info("Error updating data base: " + ex.getMessage());
        }
    }
}
