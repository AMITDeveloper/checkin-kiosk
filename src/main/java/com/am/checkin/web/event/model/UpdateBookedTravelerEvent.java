package com.am.checkin.web.event.model;

import java.io.Serializable;

import com.aeromexico.commons.model.BookedTraveler;

/**
 *
 * @author adrian
 */
public class UpdateBookedTravelerEvent implements Serializable {

    /**
     * Serial version for class
     */
    private static final long serialVersionUID = -7944853390859926703L;

    private final BookedTraveler bookedTraveler;
    private final int pnrIndex;
    private final int cartIndex;
    private final int passengerIndex;

    public UpdateBookedTravelerEvent(BookedTraveler bookedTraveler, int pnrIndex, int cartIndex, int passengerIndex) {
        this.bookedTraveler = bookedTraveler;
        this.pnrIndex = pnrIndex;
        this.cartIndex = cartIndex;
        this.passengerIndex = passengerIndex;
    }

    /**
     * @return the bookedTraveler
     */
    public BookedTraveler getBookedTraveler() {
        return bookedTraveler;
    }

    /**
     * @return the passengerIndex
     */
    public int getPassengerIndex() {
        return passengerIndex;
    }

    /**
     * @return the pnrIndex
     */
    public int getPnrIndex() {
        return pnrIndex;
    }

    /**
     * @return the cartIndex
     */
    public int getCartIndex() {
        return cartIndex;
    }
}
