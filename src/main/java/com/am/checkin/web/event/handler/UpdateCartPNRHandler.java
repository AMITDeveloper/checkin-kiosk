package com.am.checkin.web.event.handler;

import com.aeromexico.dao.services.IPNRCollectionDao;
import java.io.Serializable;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Observes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.am.checkin.web.event.model.UpdateCartPNREvent;
import javax.inject.Inject;

/**
 *
 * @author adrian
 */
@Stateless(mappedName = "UpdateCartPNRHandler")
public class UpdateCartPNRHandler implements IUpdateCartPNRHandler, Serializable {

    /**
     * Serial version for class
     */
    private static final long serialVersionUID = 1471811747189518123L;

    private static final Logger logger = LoggerFactory.getLogger(UpdateCartPNRHandler.class);

    @Inject
    private IPNRCollectionDao pnrCollectionDao;

    @Override
    @Asynchronous
    public void onUpdateCartPNR(@Observes UpdateCartPNREvent event) {
        try {
            int code = pnrCollectionDao.updateCartPNROnPNRCollectionAfterCheckin(event.getRecordLocator(), event.getLegCode(), event.getPnrIndex(), event.getCartIndex(), event.getCartPNR().toString());
            logger.info("DB code:" + code);
        } catch (Exception ex) {
            logger.info("Error updating data base: " + ex.getMessage());
        }
    }
}
