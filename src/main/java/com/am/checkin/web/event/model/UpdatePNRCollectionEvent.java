package com.am.checkin.web.event.model;

import java.io.Serializable;

import com.aeromexico.commons.model.PNRCollection;

/**
 *
 * @author adrian
 */
public class UpdatePNRCollectionEvent implements Serializable {

    private static final long serialVersionUID = 1L;

    private final PNRCollection pnrCollection;

    public UpdatePNRCollectionEvent(PNRCollection pnrCollection) {
        this.pnrCollection = pnrCollection;
    }

    /**
     * @return the pnrCollection
     */
    public PNRCollection getPnrCollection() {
        return pnrCollection;
    }
}
