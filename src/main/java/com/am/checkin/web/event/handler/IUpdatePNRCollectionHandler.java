package com.am.checkin.web.event.handler;

import com.am.checkin.web.event.model.UpdatePNRCollectionEvent;

/**
 *
 * @author adrian
 */
public interface IUpdatePNRCollectionHandler {

    public void onUpdatePNRCollection(UpdatePNRCollectionEvent event);
}
