package com.am.checkin.web.event.dispatcher;

import com.am.checkin.web.event.model.CleanLogCollectionEvent;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adrian
 */
@Named("CleanLogCollectionDispatcher")
@Stateless
public class CleanLogCollectionDispatcher {

    private static final Logger LOG = LoggerFactory.getLogger(CleanLogCollectionDispatcher.class);

    @Inject
    private Event<CleanLogCollectionEvent> eventManager;

    /**
     *
     * @param cleanLogCollectionEvent
     */
    @Asynchronous
    public void publish(CleanLogCollectionEvent cleanLogCollectionEvent) {
        LOG.info("Before clean log collections");

        if (null != eventManager) {
            eventManager.fire(cleanLogCollectionEvent);
        }

        LOG.info("After clean log collections");

    }
}
