package com.am.checkin.web.event.handler;

import com.am.checkin.web.event.model.UpdatePectabBoardingPassListEvent;

/**
 *
 * @author adrian
 */
public interface IUpdatePectabBoardingPassListHandler {

    public void onUpdatePectabBoardingPassList(UpdatePectabBoardingPassListEvent event);
}
