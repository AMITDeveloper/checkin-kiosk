package com.am.checkin.web.event.model;

import java.io.Serializable;

import com.aeromexico.commons.model.PaymentLinkedList;

/**
 *
 * @author adrian
 */
public class UpdatePaymentLinkedListEvent implements Serializable {

    /**
     * Serial version for class
     */
    private static final long serialVersionUID = -8081710772843945147L;

    private final PaymentLinkedList paymentLinkedList;
    private final String recordLocator;
    private final String legCode;
    private final int pnrIndex;
    private final int cartIndex;

    public UpdatePaymentLinkedListEvent(PaymentLinkedList paymentLinkedList, String recordLocator, String legCode,
            int pnrIndex, int cartIndex) {
        this.paymentLinkedList = paymentLinkedList;
        this.recordLocator = recordLocator;
        this.legCode = legCode;
        this.pnrIndex = pnrIndex;
        this.cartIndex = cartIndex;
    }

    /**
     * @return the paymentLinkedList
     */
    public PaymentLinkedList getPaymentLinkedList() {
        return paymentLinkedList;
    }

    /**
     * @return the pnrIndex
     */
    public int getPnrIndex() {
        return pnrIndex;
    }

    /**
     * @return the cartIndex
     */
    public int getCartIndex() {
        return cartIndex;
    }

    /**
     * @return the legCode
     */
    public String getLegCode() {
        return legCode;
    }

    /**
     * @return the recordLocator
     */
    public String getRecordLocator() {
        return recordLocator;
    }
}
