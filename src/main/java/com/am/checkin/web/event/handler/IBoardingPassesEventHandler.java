package com.am.checkin.web.event.handler;

import com.am.checkin.web.event.model.BoardingPassesEvent;

public interface IBoardingPassesEventHandler {

    public void onBoardingPassesPublish(BoardingPassesEvent event);
}
