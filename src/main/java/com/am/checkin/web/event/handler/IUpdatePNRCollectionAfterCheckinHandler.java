package com.am.checkin.web.event.handler;

import com.am.checkin.web.event.model.UpdatePNRCollectionAfterCheckinEvent;

/**
 *
 * @author adrian
 */
public interface IUpdatePNRCollectionAfterCheckinHandler {

    public void onUpdatePNRCollection(UpdatePNRCollectionAfterCheckinEvent event);
}
