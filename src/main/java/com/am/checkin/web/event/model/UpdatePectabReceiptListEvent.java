package com.am.checkin.web.event.model;

import java.io.Serializable;

import com.aeromexico.commons.model.PectabReceiptList;

/**
 *
 * @author adrian
 */
public class UpdatePectabReceiptListEvent implements Serializable {

    /**
     * Serial version for class
     */
    private static final long serialVersionUID = 6637660902530669097L;

    private final PectabReceiptList pectabReceiptList;
    private final String recordLocator;
    private final String legCode;
    private final int pnrIndex;
    private final int cartIndex;

    public UpdatePectabReceiptListEvent(PectabReceiptList pectabReceiptList, String recordLocator, String legCode,
            int pnrIndex, int cartIndex) {
        this.pectabReceiptList = pectabReceiptList;
        this.recordLocator = recordLocator;
        this.legCode = legCode;
        this.pnrIndex = pnrIndex;
        this.cartIndex = cartIndex;
    }

    /**
     * @return the pectabReceiptList
     */
    public PectabReceiptList getPectabReceiptList() {
        return pectabReceiptList;
    }

    /**
     * @return the pnrIndex
     */
    public int getPnrIndex() {
        return pnrIndex;
    }

    /**
     * @return the cartIndex
     */
    public int getCartIndex() {
        return cartIndex;
    }

    /**
     * @return the legCode
     */
    public String getLegCode() {
        return legCode;
    }

    /**
     * @return the recordLocator
     */
    public String getRecordLocator() {
        return recordLocator;
    }
}
