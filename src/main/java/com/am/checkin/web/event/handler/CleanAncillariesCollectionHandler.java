package com.am.checkin.web.event.handler;

import com.aeromexico.dao.services.IPNRLookupDao;
import com.am.checkin.web.event.model.CleanAncillariesCollectionEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import java.io.Serializable;

/**
 *
 * @author adrian
 */
@Stateless(mappedName = "CleanAncillariesCollectionHandler")
public class CleanAncillariesCollectionHandler implements ICleanAncillariesCollectionHandler, Serializable {

    private static final Logger logger = LoggerFactory.getLogger(CleanAncillariesCollectionHandler.class);
    private static final long serialVersionUID = 1L;

    @Inject
    private IPNRLookupDao pnrLookupDao;

    @Override
    @Asynchronous
    public void onCleanAncillariresCollection(@Observes CleanAncillariesCollectionEvent event) {
        try {
            //Thread.sleep(20000);
            int code = pnrLookupDao.cleanAncillariesCollecion(event.getNow());

            logger.info("Collections ancillaries delleted: " + code);
        } catch (Exception ex) {
            logger.info("Error cleanning ancillaries collections: " + ex.getMessage());
        }
    }
}
