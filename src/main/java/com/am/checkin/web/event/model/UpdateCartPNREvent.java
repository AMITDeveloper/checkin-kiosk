package com.am.checkin.web.event.model;

import java.io.Serializable;

import com.aeromexico.commons.model.CartPNR;

/**
 *
 * @author adrian
 */
public class UpdateCartPNREvent implements Serializable {

    /**
     * Serial version for class
     */
    private static final long serialVersionUID = -2072961664023408340L;

    private final CartPNR cartPNR;
    private final String recordLocator;
    private final String legCode;
    private final int pnrIndex;
    private final int cartIndex;

    public UpdateCartPNREvent(CartPNR cartPNR, String recordLocator, String legCode, int pnrIndex, int cartIndex) {
        this.cartPNR = cartPNR;
        this.recordLocator = recordLocator;
        this.legCode = legCode;
        this.pnrIndex = pnrIndex;
        this.cartIndex = cartIndex;
    }

    /**
     * @return the paymentLinkedList
     */
    public CartPNR getCartPNR() {
        return cartPNR;
    }

    /**
     * @return the pnrIndex
     */
    public int getPnrIndex() {
        return pnrIndex;
    }

    /**
     * @return the cartIndex
     */
    public int getCartIndex() {
        return cartIndex;
    }

    /**
     * @return the legCode
     */
    public String getLegCode() {
        return legCode;
    }

    /**
     * @return the recordLocator
     */
    public String getRecordLocator() {
        return recordLocator;
    }
}
