package com.am.checkin.web.event.dispatcher;

import com.am.checkin.web.event.model.SaveAncillariesCollectionEvent;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adrian
 */
@Named("SaveAncillariesCollectionDispatcher")
@Stateless
public class SaveAncillariesCollectionDispatcher {

    private static final Logger logger = LoggerFactory.getLogger(SaveAncillariesCollectionDispatcher.class);

    @Inject
    private Event<SaveAncillariesCollectionEvent> eventManager;

    /**
     *
     * @param saveAncillariesCollectionEvent
     */
    @Asynchronous
    public void publish(SaveAncillariesCollectionEvent saveAncillariesCollectionEvent) {
        logger.info("Before save Seatmap");

        if (null != eventManager) {
            eventManager.fire(saveAncillariesCollectionEvent);
        }

        logger.info("After save Seatmap");

    }
}
