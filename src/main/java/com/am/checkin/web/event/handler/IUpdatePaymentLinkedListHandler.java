package com.am.checkin.web.event.handler;

import com.am.checkin.web.event.model.UpdatePaymentLinkedListEvent;

/**
 *
 * @author adrian
 */
public interface IUpdatePaymentLinkedListHandler {

    public void onUpdatePaymentLinkedList(UpdatePaymentLinkedListEvent event);
}
