package com.am.checkin.web.event.handler;

import com.am.checkin.web.event.model.CleanSeatmapCollectionEvent;

/**
 *
 * @author adrian
 */
public interface ICleanSeatmapCollectionHandler {

    public void onCleanSeatmapCollection(CleanSeatmapCollectionEvent event);
}
