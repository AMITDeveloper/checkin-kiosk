package com.am.checkin.web.event.handler;

import com.aeromexico.dao.services.IPNRCollectionDao;
import java.io.Serializable;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Observes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.am.checkin.web.event.model.UpdatePectabReceiptListEvent;
import javax.inject.Inject;

/**
 *
 * @author adrian
 */
@Stateless(mappedName = "UpdatePectabReceiptListHandler")
public class UpdatePectabReceiptListHandler implements IUpdatePectabReceiptListHandler, Serializable {

    /**
     * Serial version for class
     */
    private static final long serialVersionUID = -3743975781270910935L;

    private static final Logger logger = LoggerFactory.getLogger(UpdatePectabReceiptListHandler.class);

    @Inject
    private IPNRCollectionDao pnrCollectionDao;

    @Override
    @Asynchronous
    public void onUpdatePectabReceiptList(@Observes UpdatePectabReceiptListEvent event) {
        try {
            int code = pnrCollectionDao.updatePectabReceiptListOnPNRCollectionAfterCheckin(event.getRecordLocator(), event.getLegCode(), event.getPnrIndex(), event.getCartIndex(), event.getPectabReceiptList().toString());
            logger.info("DB code:" + code);
        } catch (Exception ex) {
            logger.info("Error updating data base: " + ex.getMessage());
        }
    }
}
