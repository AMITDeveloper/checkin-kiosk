package com.am.checkin.web.event.model;

import java.io.Serializable;

import com.aeromexico.commons.model.PectabBoardingPassList;

/**
 *
 * @author adrian
 */
public class UpdatePectabBoardingPassListEvent implements Serializable {

    /**
     * Serial version for class
     */
    private static final long serialVersionUID = 3637956346442234163L;

    private final PectabBoardingPassList pectabBoardingPassList;
    private final String recordLocator;
    private final String legCode;
    private final int pnrIndex;
    private final int cartIndex;

    public UpdatePectabBoardingPassListEvent(PectabBoardingPassList pectabBoardingPassList, String recordLocator,
            String legCode, int pnrIndex, int cartIndex) {
        this.pectabBoardingPassList = pectabBoardingPassList;
        this.recordLocator = recordLocator;
        this.legCode = legCode;
        this.pnrIndex = pnrIndex;
        this.cartIndex = cartIndex;
    }

    /**
     * @return the pectabBoardingPassList
     */
    public PectabBoardingPassList getPectabBoardingPassList() {
        return pectabBoardingPassList;
    }

    /**
     * @return the pnrIndex
     */
    public int getPnrIndex() {
        return pnrIndex;
    }

    /**
     * @return the cartIndex
     */
    public int getCartIndex() {
        return cartIndex;
    }

    /**
     * @return the legCode
     */
    public String getLegCode() {
        return legCode;
    }

    /**
     * @return the recordLocator
     */
    public String getRecordLocator() {
        return recordLocator;
    }
}
