package com.am.checkin.web.event.handler;

import com.am.checkin.web.event.model.UpdateCartPNREvent;

/**
 *
 * @author adrian
 */
public interface IUpdateCartPNRHandler {

    public void onUpdateCartPNR(UpdateCartPNREvent event);
}
