package com.am.checkin.web.event.handler;

import com.am.checkin.web.event.model.SendEmailEvent;
import com.am.checkin.web.service.EmailService;
import java.io.Serializable;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless(mappedName = "SendEmailCheckinEventHandler")
public class SendEmailEventHandler implements ISendEmailEventHandler, Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger logger = LoggerFactory.getLogger(SendEmailEventHandler.class);

    @Inject
    private EmailService emailService;

    @Override
    @Asynchronous
    public void onSendEmailPublish(@Observes SendEmailEvent event) {
        try {
            String response = emailService.doPostMailing(event.getEndpoint(), event.getBody(), event.getQueryParams());

            emailService.validateMailingResponse(response);
        } catch (Exception ex) {
            logger.info("Error sending email confirmation: {} " , ex.getMessage());
        }
    }

    public EmailService getEmailService() {
        return emailService;
    }

    public void setEmailService(EmailService emailService) {
        this.emailService = emailService;
    }
}
