package com.am.checkin.web.event.handler;

import com.am.checkin.web.event.model.CleanPnrCollectionEvent;

/**
 *
 * @author adrian
 */
public interface ICleanPnrCollectionHandler {

    public void onCleanPnrCollection(CleanPnrCollectionEvent event);
}
