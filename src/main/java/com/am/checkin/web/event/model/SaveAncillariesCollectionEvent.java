package com.am.checkin.web.event.model;

import com.aeromexico.commons.model.AncillariesCollectionWrapper;
import java.io.Serializable;

/**
 *
 * @author adrian
 */
public class SaveAncillariesCollectionEvent implements Serializable {

    private static final long serialVersionUID = 1L;

    private final AncillariesCollectionWrapper ancillariesCollectionWrapper;

    public SaveAncillariesCollectionEvent(AncillariesCollectionWrapper ancillariesCollectionWrapper) {
        this.ancillariesCollectionWrapper = ancillariesCollectionWrapper;
    }

    /**
     * @return the ancillariesCollectionWrapper
     */
    public AncillariesCollectionWrapper getAncillariesCollectionWrapper() {
        return ancillariesCollectionWrapper;
    }

}
