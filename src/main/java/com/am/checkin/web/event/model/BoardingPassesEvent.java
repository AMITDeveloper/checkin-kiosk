package com.am.checkin.web.event.model;

import com.aeromexico.commons.model.PNRCollection;
import java.io.Serializable;

public class BoardingPassesEvent implements Serializable {

    private static final long serialVersionUID = 1L;

    private PNRCollection pnrCollection;

    public BoardingPassesEvent(PNRCollection pnrCollection) {
        this.pnrCollection = pnrCollection;
    }

    public PNRCollection getPnrCollection() {
        return pnrCollection;
    }
}
