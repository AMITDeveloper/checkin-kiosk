package com.am.checkin.web.event.handler;

import com.aeromexico.dao.services.IPNRLookupDao;
import com.am.checkin.web.event.model.SaveAncillariesCollectionEvent;
import java.io.Serializable;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adrian
 */
@Stateless(mappedName = "SaveAncillariesCollectionHandler")
public class SaveAncillariesCollectionHandler implements ISaveAncillariesCollectionHandler, Serializable {

    private static final Logger logger = LoggerFactory.getLogger(SaveAncillariesCollectionHandler.class);
    private static final long serialVersionUID = 1L;

    @Inject
    private IPNRLookupDao pnrLookupDao;

    @Override
    @Asynchronous
    public void onSaveAncillariesCollection(@Observes SaveAncillariesCollectionEvent event) {
        try {

            int code = pnrLookupDao.saveAncillariesCollection(event.getAncillariesCollectionWrapper());

            logger.info("Saving ancillaries to DB, code:" + code);
        } catch (Exception ex) {
            logger.info("Error saving ancillaries to data base: " + ex.getMessage());
        }
    }
}
