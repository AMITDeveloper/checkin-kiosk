package com.am.checkin.web.event.model;

import java.io.Serializable;
import javax.ws.rs.core.MultivaluedMap;

public class SendEmailEvent implements Serializable {

    private static final long serialVersionUID = 1L;

    private String endpoint;
    private String body;
    private MultivaluedMap<String, Object> queryParams;

    public SendEmailEvent() {

    }

    public SendEmailEvent(String endpoint, String body, MultivaluedMap<String, Object> queryParams) {
        this.endpoint = endpoint;
        this.body = body;
        this.queryParams = queryParams;
    }

    /**
     * @return the endpoint
     */
    public String getEndpoint() {
        return endpoint;
    }

    /**
     * @param endpoint the endpoint to set
     */
    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    /**
     * @return the body
     */
    public String getBody() {
        return body;
    }

    /**
     * @param body the body to set
     */
    public void setBody(String body) {
        this.body = body;
    }

    /**
     * @return the queryParams
     */
    public MultivaluedMap<String, Object> getQueryParams() {
        return queryParams;
    }

    /**
     * @param queryParams the queryParams to set
     */
    public void setQueryParams(MultivaluedMap<String, Object> queryParams) {
        this.queryParams = queryParams;
    }

}
