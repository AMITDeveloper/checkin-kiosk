package com.am.checkin.web.event.handler;

import com.aeromexico.dao.services.IPNRLookupDao;
import com.am.checkin.web.event.model.CleanSeatmapCollectionEvent;
import java.io.Serializable;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adrian
 */
@Stateless(mappedName = "CleanSeatmapCollectionHandler")
public class CleanSeatmapCollectionHandler implements ICleanSeatmapCollectionHandler, Serializable {

    private static final Logger logger = LoggerFactory.getLogger(CleanSeatmapCollectionHandler.class);
    private static final long serialVersionUID = 1L;

    @Inject
    private IPNRLookupDao pnrLookupDao;

    @Override
    @Asynchronous
    public void onCleanSeatmapCollection(@Observes CleanSeatmapCollectionEvent event) {
        try {
            //Thread.sleep(20000);
            int code = pnrLookupDao.cleanSeatMapCollecion(event.getNow());

            logger.info("Collections seat map delleted: " + code);
        } catch (Exception ex) {
            logger.info("Error cleanning seat map collections: " + ex.getMessage());
        }
    }
}
