package com.am.checkin.web.event.dispatcher;

import com.am.checkin.web.event.model.CleanSeatmapCollectionEvent;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adrian
 */
@Named("CleanSeatmapCollectionDispatcher")
@Stateless
public class CleanSeatmapCollectionDispatcher {

    private static final Logger logger = LoggerFactory.getLogger(CleanSeatmapCollectionDispatcher.class);

    @Inject
    private Event<CleanSeatmapCollectionEvent> eventManager;

    /**
     *
     * @param cleanSeatmapCollectionEvent
     */
    @Asynchronous
    public void publish(CleanSeatmapCollectionEvent cleanSeatmapCollectionEvent) {
        logger.info("Before clean Seatmap");

        if (null != eventManager) {
            eventManager.fire(cleanSeatmapCollectionEvent);
        }

        logger.info("After clean Seatmap");

    }
}
