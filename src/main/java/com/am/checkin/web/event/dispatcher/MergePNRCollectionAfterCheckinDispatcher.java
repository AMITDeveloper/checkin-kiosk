package com.am.checkin.web.event.dispatcher;

import com.am.checkin.web.event.model.UpdatePaymentLinkedListEvent;
import com.am.checkin.web.event.model.UpdateBookedTravelerEvent;
import com.am.checkin.web.event.model.UpdateCartPNREvent;
import com.am.checkin.web.event.model.UpdatePectabBoardingPassListEvent;
import com.am.checkin.web.event.model.UpdatePectabReceiptListEvent;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adrian
 */
@Named("MergePNRCollectionAfterCheckinDispatcher")
@Stateless
public class MergePNRCollectionAfterCheckinDispatcher {

    private static final Logger logger = LoggerFactory.getLogger(MergePNRCollectionAfterCheckinDispatcher.class);

    @Inject
    private Event<UpdateBookedTravelerEvent> updateBookedTravelerEventManager;

    @Inject
    private Event<UpdatePaymentLinkedListEvent> paymentLinkedListEventManager;

    @Inject
    private Event<UpdatePectabBoardingPassListEvent> updatePectabBoardingPassListEventManager;

    @Inject
    private Event<UpdatePectabReceiptListEvent> updatePectabReceiptListEventManager;

    @Inject
    private Event<UpdateCartPNREvent> updateCartPNREventManager;

    /**
     *
     * @param updateBookedTravelerEvent
     */
    @Asynchronous
    public void publishUpdateBookedTravelerEvent(UpdateBookedTravelerEvent updateBookedTravelerEvent) {
        logger.info("Before update updateBookedTravelerEvent");

        if (null != updateBookedTravelerEventManager) {
            updateBookedTravelerEventManager.fire(updateBookedTravelerEvent);
        }

        logger.info("After update updateBookedTravelerEvent");

    }

    /**
     *
     * @param paymentLinkedListEvent
     */
    @Asynchronous
    public void publishPaymentLinkedListEvent(UpdatePaymentLinkedListEvent paymentLinkedListEvent) {
        logger.info("Before update paymentLinkedListEvent");

        if (null != paymentLinkedListEventManager) {
            paymentLinkedListEventManager.fire(paymentLinkedListEvent);
        }

        logger.info("After update paymentLinkedListEvent");

    }

    /**
     *
     * @param updatePectabBoardingPassListEvent
     */
    @Asynchronous
    public void publishUpdatePectabBoardingPassListEvent(UpdatePectabBoardingPassListEvent updatePectabBoardingPassListEvent) {
        logger.info("Before update updatePectabBoardingPassListEvent");

        if (null != updatePectabBoardingPassListEventManager) {
            updatePectabBoardingPassListEventManager.fire(updatePectabBoardingPassListEvent);
        }

        logger.info("After update updatePectabBoardingPassListEvent");

    }

    /**
     *
     * @param updatePectabReceiptListEvent
     */
    @Asynchronous
    public void publishUpdatePectabReceiptListEvent(UpdatePectabReceiptListEvent updatePectabReceiptListEvent) {
        logger.info("Before update updatePectabReceiptListEvent");

        if (null != updatePectabReceiptListEventManager) {
            updatePectabReceiptListEventManager.fire(updatePectabReceiptListEvent);
        }

        logger.info("After update updatePectabReceiptListEvent");

    }

    /**
     *
     * @param updateCartPNREvent
     */
    @Asynchronous
    public void publishUpdateCartPNREvent(UpdateCartPNREvent updateCartPNREvent) {
        logger.info("Before update updatePectabReceiptListEvent");

        if (null != updatePectabReceiptListEventManager) {
            updateCartPNREventManager.fire(updateCartPNREvent);
        }

        logger.info("After update updatePectabReceiptListEvent");

    }
}
