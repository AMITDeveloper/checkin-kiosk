package com.am.checkin.web.controller;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.aeromexico.commons.model.AirportCollection;
import com.aeromexico.commons.model.FlightCollection;
import com.aeromexico.commons.model.rq.AirportFlightListRQ;
import com.aeromexico.commons.model.rq.ArrivalFlightListRQ;
import com.aeromexico.commons.model.rq.DepartureFlightListRQ;
import com.am.checkin.web.service.AirportFlightListService;
import com.am.checkin.web.service.FlightListService;

@Path("/airportFlightList")
public class AirportFlightListController {

    @Inject
    private AirportFlightListService airportFlightListService;

    @Inject
    private FlightListService flightListService;

    @GET
    @Path("/{origin:[a-zA-Z][a-zA-Z][a-zA-Z]}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAirportList(@Valid @BeanParam AirportFlightListRQ airportFlightListRQ) throws Exception {
        AirportCollection airportCollection = airportFlightListService.getAirportList(airportFlightListRQ);
        return Response.status(Response.Status.OK).entity(airportCollection).build();
    }

    @GET
    @Path("/departure")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDepartureAirportList(@Valid @BeanParam DepartureFlightListRQ departureFlightListRQ) throws Exception {
    	FlightCollection flightCollection = flightListService.getFlightList(departureFlightListRQ);
        return Response.status(Response.Status.OK).entity(flightCollection).build();
    }

    @GET
    @Path("/arrival")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getArrivalAirportList(@Valid @BeanParam ArrivalFlightListRQ arrivalFlightListRQ) throws Exception {
    	FlightCollection flightCollection = flightListService.getFlightList(arrivalFlightListRQ);
        return Response.status(Response.Status.OK).entity(flightCollection).build();
    }

}
