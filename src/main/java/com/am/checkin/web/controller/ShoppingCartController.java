package com.am.checkin.web.controller;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.rq.ShoppingCartGETRQ;
import com.aeromexico.commons.model.rq.ShoppingCartRQ;
import com.aeromexico.commons.web.types.PosType;
import com.aeromexico.dao.client.exception.MongoDisconnectException;
import com.aeromexico.sabre.api.session.common.ServiceCall;
import com.am.checkin.web.service.LogOnDatabaseService;
import com.am.checkin.web.service.ShoppingCartService;
import java.util.ArrayList;
import java.util.List;

import org.aeromexico.commons.exception.config.ReadProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/carts")
public class ShoppingCartController {

    private static final Logger LOG = LoggerFactory.getLogger(ShoppingCartController.class);

    @Inject
    private ShoppingCartService shoppingCartService;

    @Inject
    private LogOnDatabaseService logOnDatabaseService;

    @Inject
    private ReadProperties prop;

    @DELETE
    @Path("/{cartId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response cancelShoppingCart(@Valid @BeanParam ShoppingCartGETRQ shoppingCartRQ) throws Exception {
        try {
            List<ServiceCall> serviceCallList = new ArrayList<>();
            CartPNR cartPNR = shoppingCartService.cleanShoppingCart(shoppingCartRQ, serviceCallList);
            return Response.status(Response.Status.OK).entity(cartPNR).build();
        } catch (Throwable ex) {
            LOG.error(ex.getMessage());
            throw ex;
        }
    }

    @PUT
    @Path("/{cartId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response putShoppingCart(@Valid @BeanParam ShoppingCartRQ shoppingCartRQ) throws Exception {
        try {
            List<ServiceCall> serviceCallList = new ArrayList<>();

            CartPNR cartPNR = shoppingCartService.putShoppingCart(shoppingCartRQ, serviceCallList);

            //Stop the timer
            shoppingCartRQ.processed();

            logOnDatabaseService.logShoppingCartOnSuccess(shoppingCartRQ);

            return Response.status(Response.Status.OK).entity(cartPNR).build();

        } catch (Throwable ex) {
            //Stop the timer
            shoppingCartRQ.processed();

            logOnDatabaseService.logShoppingCartOnError(shoppingCartRQ, ex);
            if(ex instanceof MongoDisconnectException){
                if(PosType.CHECKIN_MOBILE.toString().equals(shoppingCartRQ.getPos())){
                    throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getCode(),
                            prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getActionable(),
                            prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getMessage(),
                            Response.Status.INTERNAL_SERVER_ERROR);
                }
            }
            LOG.error(ex.getMessage(), ex);
            throw ex;
        }
    }

    @GET
    @Path("/{cartId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getShoppingCart(@Valid @BeanParam ShoppingCartGETRQ shoppingCartGETRQ) throws Exception {
        try {
            if (shoppingCartGETRQ.getCartId() == null || (shoppingCartGETRQ.getCartId() != null && shoppingCartGETRQ.getCartId().trim().isEmpty())) {
                LOG.error("The query param cartId is required");
                throw new NullPointerException("cartId");
            }
            ShoppingCartRQ shoppingCartRQ = new ShoppingCartRQ();
            shoppingCartRQ.setCartId(shoppingCartGETRQ.getCartId());
            shoppingCartRQ.setPos(shoppingCartGETRQ.getPos());
            shoppingCartRQ.setStore(shoppingCartGETRQ.getStore());
            CartPNR cartPNR = shoppingCartService.getShoppingCart(shoppingCartGETRQ.getCartId(), shoppingCartRQ);
            return Response.status(Response.Status.OK).entity(cartPNR).build();
        } catch (Throwable ex) {
            LOG.error(ex.getMessage());
            throw ex;
        }
    }

}
