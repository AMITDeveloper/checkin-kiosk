package com.am.checkin.web.controller;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeromexico.commons.model.AppleWalletBoardingPassInfo;
import com.am.checkin.web.service.AppleWalletService;

@Path("/apple-wallet")
public class AppleWalletController {

	private static final Logger log = LoggerFactory.getLogger(AppleWalletController.class);

	/**
	 * Apple Wallet service
	 */
	@Inject
	private AppleWalletService appleWalletService;

	/**
	 * Definition of service
	 * 
	 * @return
	 * @throws Exception
	 */
	@GET
	@Path("/boarding-pass")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public void createAppleWalletBoardingPass(@QueryParam(value = "passenger") final String passenger,
			@QueryParam(value = "flight") final String flight,
			@QueryParam(value = "departure-city") final String departureCity,
			@QueryParam(value = "departure-airport") final String departureAirport,
			@QueryParam(value = "departure-date") final String departureDate,
			@QueryParam(value = "departure-time") final String departureTime,
			@QueryParam(value = "boarding-time") final String boardingTime,
			@QueryParam(value = "boarding-group") final String boardingGroup,
			@QueryParam(value = "departure-terminal") final String departureTerminal,
			@QueryParam(value = "departure-gate") final String departureGate,
			@QueryParam(value = "arrival-city") final String arrivalCity,
			@QueryParam(value = "arrival-airport") final String arrivalAirport,
			@QueryParam(value = "arrival-date") final String arrivalDate,
			@QueryParam(value = "arrival-time") final String arrivalTime,
			@QueryParam(value = "service-class") final String serviceClass,
			@QueryParam(value = "seat-assignment") final String seatAssignment,
			@QueryParam(value = "passport") final String passport,
			@QueryParam(value = "residence") final String residence,
			@QueryParam(value = "barcodeData") final String barcodeData,
			@QueryParam(value = "skyPriority") final String skyPriority,
			@QueryParam(value = "tsaPre") final String tsaPre,
			@QueryParam(value = "relevantDate") final String relevantDate,
			@QueryParam(value = "pnr") final String pnr,
			@QueryParam(value = "ticketNumber") final String ticketNumber,
			@QueryParam(value = "clubPremier") final String clubPremier,
			@QueryParam(value = "international") final Boolean international,
			@QueryParam(value = "operatingCarrier") final String operatingCarrier,
			@QueryParam(value = "latitude") final String latitude,
			@QueryParam(value = "longitude") final String longitude,
			@Context HttpServletResponse response)
			throws Exception {

		final AppleWalletBoardingPassInfo appleWalletBoardingPassInfo = new AppleWalletBoardingPassInfo(passenger,
				flight, departureCity, departureAirport, departureDate, departureTime, boardingTime, boardingGroup,
				departureTerminal, departureGate, arrivalCity, arrivalAirport, arrivalDate, arrivalTime, serviceClass,
				seatAssignment, passport, residence, barcodeData, skyPriority, tsaPre, relevantDate, 
				pnr, ticketNumber, clubPremier, international, operatingCarrier, latitude, longitude);

		log.debug(String.format("Creating Apple Wallet Boarding Pass using: %s\n", appleWalletBoardingPassInfo));

		final byte[] data = appleWalletService.createPass(appleWalletBoardingPassInfo);

		response.setStatus(HttpStatus.SC_OK);
		response.setContentType("application/vnd.apple.pkpass");
		response.setHeader("Content-Disposition", String.format("attachment; filename=%s.pkpass", flight));
		response.setContentLength(data.length);
		response.getOutputStream().write(data);
		response.flushBuffer();
	}

}
