package com.am.checkin.web.controller;

import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.rq.ShoppingCartGETRQ;
import com.aeromexico.commons.model.rq.ShoppingCartRQ;
import com.aeromexico.sabre.api.session.common.ServiceCall;
import com.am.checkin.web.service.LogOnDatabaseService;
import com.am.checkin.web.service.ShoppingCartService;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adrianleal
 */
@Path("/travelersInfo")
public class TravelersController {

    private static final Logger LOG = LoggerFactory.getLogger(TravelersController.class);

    @Inject
    private ShoppingCartService shoppingCartService;

    @Inject
    private LogOnDatabaseService logOnDatabaseService;

    @PUT
    @Path("/{cartId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response putShoppingCartGender(@Valid @BeanParam ShoppingCartRQ shoppingCartRQ) throws Exception {
        try {
            List<ServiceCall> serviceCallList = new ArrayList<>();

            CartPNR cartPNR = shoppingCartService.putShoppingCart(shoppingCartRQ, serviceCallList);

            //Stop the timer
            shoppingCartRQ.processed();

            logOnDatabaseService.logShoppingCartOnSuccess(shoppingCartRQ);

            return Response.status(Response.Status.OK).entity(cartPNR).build();

        } catch (Throwable ex) {
            //Stop the timer
            shoppingCartRQ.processed();

            logOnDatabaseService.logShoppingCartOnError(shoppingCartRQ, ex);

            LOG.error(ex.getMessage(), ex);
            throw ex;
        }
    }
}
