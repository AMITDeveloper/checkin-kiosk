package com.am.checkin.web.controller;

import com.aeromexico.commons.model.AuthorizationResult;
import com.aeromexico.commons.model.rq.PinPadInitializationRQ;
import com.aeromexico.sabre.api.session.common.ServiceCall;
import com.am.checkin.web.service.PinpadInitializationService;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.BeanParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adrian
 */
@Path("/pinpad")
public class PinpadInitializationController {

    private static final Logger LOG = LoggerFactory.getLogger(PinpadInitializationController.class);

    @Inject
    PinpadInitializationService pinpadInitializationService;

    @POST
    @Path("/init")
    @Produces(MediaType.APPLICATION_JSON)
    public Response initialization(@Valid @BeanParam PinPadInitializationRQ pinPadInitializationRQ) throws Exception {
        try {
            List<ServiceCall> serviceCallList = new ArrayList<>();
            AuthorizationResult encryptKey = pinpadInitializationService.initializePinpad(pinPadInitializationRQ, serviceCallList);

            GenericEntity<AuthorizationResult> genericEntity = new GenericEntity<>(encryptKey, AuthorizationResult.class);
            return Response.status(Response.Status.CREATED).entity(genericEntity).build();
        } catch (Throwable ex) {
            LOG.error(ex.getMessage());
            throw ex;
        }
    }
}
