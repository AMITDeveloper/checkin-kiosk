package com.am.checkin.web.controller;

import com.aeromexico.commons.model.FlightStatusCollection;
import com.aeromexico.commons.model.rq.AirFlifoRQ;
import com.aeromexico.commons.model.rq.FlightStatusRQ;
import com.aeromexico.dao.util.CommonUtil;
import com.am.checkin.web.service.AirFlifoService;
import com.am.checkin.web.v2.services.FlightStatusService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author Franco Beltran
 */
@Path("flight-status")
public class AirFlifoController {

    private static final Logger log = LoggerFactory.getLogger(AirFlifoController.class);

    @Inject
    private AirFlifoService airFlifoService;

    @Inject
    private FlightStatusService flightStatusService;

    private boolean FLIGHT_STATUS_API_ENABLED = false;

    @PostConstruct
    public void init() {
        try {
            String flightStatusApiEnabled = System.getProperty("flight.status.api.enabled");
            if (flightStatusApiEnabled != null && !flightStatusApiEnabled.isEmpty()) {
                FLIGHT_STATUS_API_ENABLED = Boolean.valueOf(flightStatusApiEnabled);
            }
        } catch (Exception ex) {
        }
    }


    @GET
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getFlightStatus(@Valid @BeanParam AirFlifoRQ airFlifoRQ, @Context UriInfo uri) throws Exception {

        if (FLIGHT_STATUS_API_ENABLED) {
            FlightStatusCollection flightStatusCollection = null;
            FlightStatusRQ flightStatusRQ = new FlightStatusRQ();
            try {
                flightStatusRQ.setOrigin(airFlifoRQ.getOrigin());
                flightStatusRQ.setDestination(airFlifoRQ.getDestination());
                flightStatusRQ.setFlight(airFlifoRQ.getFlight());
                flightStatusRQ.setDate(airFlifoRQ.getDate());
                flightStatusRQ.setPath(CommonUtil.getLink(uri));

                flightStatusCollection = flightStatusService.callFlightStatus(flightStatusRQ);
            } catch (Exception e) {
                log.error("Flight Status GET: {}, {}", flightStatusRQ.getPath(), e.getMessage(), e);
            }
            return Response.status(Response.Status.OK).entity(flightStatusCollection).build();

        } else {

            FlightStatusCollection flightStatusCollection = null;
            try {
                airFlifoRQ.setPath(CommonUtil.getLink(uri));

                flightStatusCollection = airFlifoService.callAirFlifo(airFlifoRQ);

            } catch (Exception e) {
                log.error("AirFlifo GET: " + airFlifoRQ.getPath() + " " + e.getMessage(), e);
                throw e;
            }
            return Response.status(Response.Status.OK).entity(flightStatusCollection).build();
        }
    }

}
