package com.am.checkin.web.controller;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.StandbyList;
import com.aeromexico.commons.model.UpgradeList;
import com.aeromexico.commons.model.rq.PassengerSearchListRQ;
import com.aeromexico.commons.model.rq.PassengersListRQ;
import com.aeromexico.commons.prototypes.JSONPrototype;
import com.aeromexico.commons.web.types.PosType;
import com.aeromexico.dao.util.CommonUtil;
import com.aeromexico.sabre.api.session.common.ServiceCall;
import com.aeromexico.sabre.api.session.exception.SabreLayerUnavailableException;
import com.am.checkin.web.service.LogOnDatabaseService;
import com.am.checkin.web.service.PassengerSearchListService;
import com.am.checkin.web.service.PriorityListService;
import com.am.checkin.web.v2.util.SystemVariablesUtil;
import com.sabre.services.acs.bso.passengerlist.v3.ACSPassengerListRSACS;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.aeromexico.commons.exception.config.ReadProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author adrian
 */
@Path("/passengers")
public class PassengerListController {

    private static final Logger LOG = LoggerFactory.getLogger(PassengerListController.class);

    @Inject
    private PriorityListService priorityListService;

    @Inject
    private LogOnDatabaseService logOnDatabaseService;

    @Inject
    private PassengerSearchListService passengerSearchListService;

    @Inject
    private ReadProperties prop;

    @GET
    @Path("/standByList")
    @Produces(MediaType.APPLICATION_JSON)
    public StandbyList getStandbyList(@Valid @BeanParam PassengersListRQ passengersListRQ) throws Exception {

        try {
            StandbyList passengersListWrapper;
            if (SystemVariablesUtil.isRobotPassengerListEnabled()) {
                passengersListWrapper = priorityListService.getNewStandbyList(passengersListRQ);
                System.out.println("New Standby List");
            } else {
                System.out.println("Classic StandByList");
                List<ServiceCall> serviceCallList = new ArrayList<>();
                passengersListWrapper = priorityListService.getStandbyList(
                        passengersListRQ,
                        serviceCallList
                );
            }


            passengersListRQ.setUriStr(CommonUtil.getLink(passengersListRQ.getUri()));

            logOnDatabaseService.logPriorityListOnSuccess(passengersListRQ);

            return passengersListWrapper;
        } catch (SabreLayerUnavailableException ex) {
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(passengersListRQ.getPos())) {
                LOG.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_22110301,"PassengerListController.java:80", ex.getMessage());
                throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getCode(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getActionable(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getMessage(),
                        Response.Status.INTERNAL_SERVER_ERROR);
            } else {
                LOG.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_12110301,"PassengerListController.java:86", ex.getMessage());
                throw ex;
            }
        } catch (GenericException ex) {
            if (!ex.wasLogged()) {
                logOnDatabaseService.logPriorityListOnError(passengersListRQ, ex);
            }

            throw ex;
        } catch (Exception ex) {
            logOnDatabaseService.logPriorityListOnError(passengersListRQ, ex);
            LOG.error(ex.getMessage(), ex);
            throw ex;
        } catch (Throwable ex) {

            logOnDatabaseService.logPriorityListOnError(passengersListRQ, ex);
            LOG.error(ex.getMessage(), ex);
            throw ex;
        }
    }

    @GET
    @Path("/upgradeList")
    @Produces(MediaType.APPLICATION_JSON)
    public UpgradeList getUpgradeList(@Valid @BeanParam PassengersListRQ passengersListRQ) throws Exception {

        try {
            UpgradeList passengersListWrapper;
            if (SystemVariablesUtil.isRobotPassengerListEnabled()) {
                passengersListWrapper = priorityListService.getNewUpgradeList(passengersListRQ);
                System.out.println("New upgrade List");
            } else {
                List<ServiceCall> serviceCallList = new ArrayList<>();
                System.out.println("Classic upgrade list");
                passengersListWrapper = priorityListService.getUpgradeList(
                        passengersListRQ,
                        serviceCallList
                );
            }


            passengersListRQ.setUriStr(CommonUtil.getLink(passengersListRQ.getUri()));

            logOnDatabaseService.logPriorityListOnSuccess(passengersListRQ);

            return passengersListWrapper;
        } catch (SabreLayerUnavailableException ex) {
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(passengersListRQ.getPos())) {
                LOG.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_22110301,"PassengerListController.java:134", ex.getMessage());
                throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getCode(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getActionable(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getMessage(),
                        Response.Status.INTERNAL_SERVER_ERROR);
            } else {
                LOG.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_12110301,"PassengerListController.java:140", ex.getMessage());
                throw ex;
            }
        } catch (GenericException ex) {
            if (!ex.wasLogged()) {
                logOnDatabaseService.logPriorityListOnError(passengersListRQ, ex);
            }
            throw ex;
        } catch (Exception ex) {
            logOnDatabaseService.logPriorityListOnError(passengersListRQ, ex);
            LOG.error(ex.getMessage(), ex);
            throw ex;
        } catch (Throwable ex) {
            logOnDatabaseService.logPriorityListOnError(passengersListRQ, ex);
            LOG.error(ex.getMessage(), ex);
            throw ex;
        }
    }

    @GET
    @Path("/passengerSearchList")
    @Produces(MediaType.APPLICATION_JSON)
    public String getPassengerList(@Valid @BeanParam PassengerSearchListRQ passengersListRQ) throws Exception {
        ACSPassengerListRSACS passengerList = passengerSearchListService.getPassengerSearchList(passengersListRQ);
        return JSONPrototype.convert(passengerList);
    }

}
