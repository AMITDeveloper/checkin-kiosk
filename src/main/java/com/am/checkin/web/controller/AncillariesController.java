package com.am.checkin.web.controller;

import com.aeromexico.commons.model.AncillaryOfferCollection;
import com.aeromexico.commons.model.rq.AncillaryRQ;
import com.aeromexico.dao.util.CommonUtil;
import com.aeromexico.slack.service.SlackService;
import com.am.checkin.web.service.AncillariesService;
import com.am.checkin.web.service.CheckInSetUpConfigFactoryWrapper;
import com.am.checkin.web.service.LogOnDatabaseService;
import com.am.checkin.web.v2.util.AncillaryCollectionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.Valid;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Rocio Mendoza
 */
@Path("")
public class AncillariesController {

    private static final Logger LOG = LoggerFactory.getLogger(PNRLookupController.class);

    @Inject
    private AncillariesService ancillariesService;

    @Inject
    private LogOnDatabaseService logOnDatabaseService;

    @Inject
    private SlackService slackService;

    @Inject
    @Named("CheckInSetUpConfigFactoryWrapper")
    private CheckInSetUpConfigFactoryWrapper setUpConfigFactory;

    @PostConstruct
    public void init() {
        if (null == setUpConfigFactory) {
            setUpConfigFactory = new CheckInSetUpConfigFactoryWrapper();
        }
    }
    
    @GET
    @Path("/ancillaries")
    @Produces(MediaType.APPLICATION_JSON)
    public AncillaryOfferCollection getAncillaries_2_0(@Valid @BeanParam AncillaryRQ ancillariesRQ) throws Exception {

        AncillaryOfferCollection ancillaryCollection;

        try {
            ancillaryCollection = ancillariesService.getAncillaries_2_0(ancillariesRQ);
            ancillariesRQ.setUriStr(CommonUtil.getLink(ancillariesRQ.getUri()));            

            logOnDatabaseService.logAncillariesOnSuccess(ancillariesRQ);

            return ancillaryCollection;
        } catch (Throwable ex) {            
            logOnDatabaseService.logAncillariesOnError(ancillariesRQ, ex);
            LOG.error("Ancillaries.getAncillaryOffers Exception: {}", ex.getMessage());

            ancillaryCollection = AncillaryCollectionUtil.getAncillaryOfferCollectionEmpty();
            return ancillaryCollection;
        }
    }

    /**
     * @return the setUpConfigFactory
     */
    public CheckInSetUpConfigFactoryWrapper getSetUpConfigFactory() {
        if (null == setUpConfigFactory) {
            setUpConfigFactory = new CheckInSetUpConfigFactoryWrapper();
        }
        return setUpConfigFactory;
    }

    /**
     * @param setUpConfigFactory the setUpConfigFactory to set
     */
    public void setSetUpConfigFactory(CheckInSetUpConfigFactoryWrapper setUpConfigFactory) {
        this.setUpConfigFactory = setUpConfigFactory;
    }

}
