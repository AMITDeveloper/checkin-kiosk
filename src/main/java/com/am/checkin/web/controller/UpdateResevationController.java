package com.am.checkin.web.controller;

import com.aeromexico.commons.model.ExtraInfo;
import com.aeromexico.commons.model.Warning;
import com.aeromexico.commons.model.WarningCollection;
import com.aeromexico.commons.model.rq.UpdatePassengersRequest;
import com.aeromexico.dao.util.JSONPrototype;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.BeanParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.aeromexico.commons.model.rq.UpdatePnrRQ;
import com.aeromexico.sabre.api.session.common.ServiceCall;
import com.aeromexico.sharedservices.services.UpdatePassengerService;
import com.am.checkin.web.service.AEService;
import com.sabre.webservices.pnrbuilder.updatereservation.UpdateReservationRQ;
import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.NotNull;
import javax.ws.rs.QueryParam;
import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Carlos Luna
 *
 */
@Path("")
public class UpdateResevationController {

    private static final Logger LOG = LoggerFactory.getLogger(UpdateResevationController.class);

    @Inject
    private AEService aeService;

    @Inject
    private UpdatePassengerService updatePassengerService;

    private static final String ERROR_TEXT = "ERROR";
    
    @POST
    @Path("/update-reservation")
    @Produces(MediaType.APPLICATION_JSON)
    public Response postUpdateReservation(@Valid @BeanParam UpdateReservationRQ updateReservationRQ) throws Exception {
        try {
            List<ServiceCall> serviceCallList = new ArrayList<>();
            aeService.updateReservationOSI(updateReservationRQ, serviceCallList);
            return Response.status(Response.Status.CREATED).entity("SUCCESS").build();
        } catch (Throwable ex) {
            LOG.error(ex.getMessage());
            throw ex;
        }
    }

    @POST
    @Path("/update-ffnumber")
    @Produces(MediaType.APPLICATION_JSON)
    public Response postUpdateFFnumber(@Valid @BeanParam UpdatePnrRQ updatePnrRQ) throws Exception {
        try {
            List<ServiceCall> serviceCallList = new ArrayList<>();
            String response = aeService.createOSI(updatePnrRQ.getRecordLocator(), updatePnrRQ.getFfcProgram(), 
                    updatePnrRQ.getFfcNumber(), updatePnrRQ.getNameRefNumber(), serviceCallList
            );
            Response.Status status  = !response.startsWith(ERROR_TEXT) ? Response.Status.CREATED : Response.Status.BAD_REQUEST;
            return Response.status(status).entity(response).build();
            
        } catch (Throwable ex) {
            LOG.error(ex.getMessage());
            throw ex;
        }
    }
    
    @POST
    @Path("/update-ssrCarReservation")
    @Produces(MediaType.APPLICATION_JSON)
    public Response postUpdateSSRcarReservation(
            @Valid @QueryParam("recordLocator")
            @NotNull(message = "{recordLocator.notnull}")
            @NotEmpty(message = "{recordLocator.notempty}") String recordLocator,
            @Valid @QueryParam("ffcNumber")
            @NotNull(message = "{ffcNumber.notnull}")
            @NotEmpty(message = "{ffcNumber.notempty}") String carReferenceNumber) throws Exception {
        try {
            List<ServiceCall> serviceCallList = new ArrayList<>();
            
            StringBuilder ssr = new StringBuilder();
            if (!carReferenceNumber.startsWith("Car Rental")){
                ssr.append("Car Rental");
            }
            ssr.append(carReferenceNumber);
            
            String response = aeService.createSSR(recordLocator, ssr.toString(), serviceCallList);
            Response.Status status  = !response.startsWith(ERROR_TEXT) ? Response.Status.CREATED : Response.Status.BAD_REQUEST;
            return Response.status(status).entity(response).build();
        } catch (Throwable ex) {
            LOG.error(ex.getMessage());
            throw ex;
        }
    }
    
    @POST
    @Path("/update-remarkPayments")
    @Produces(MediaType.APPLICATION_JSON)
    public Response postUpdateRemarkReservation(
            @Valid @QueryParam("recordLocator")
            @NotNull(message = "{recordLocator.notnull}")
            @NotEmpty(message = "{recordLocator.notempty}") String recordLocator,
            @Valid @QueryParam("remark")
            @NotNull(message = "{remark.notnull}")
            @NotEmpty(message = "{remark.notempty}") String remark) throws Exception {
        try {
            boolean addedRemark = aeService.addRemarkPayments(recordLocator, remark);
            return addedRemark ? 
                    Response.status(Response.Status.CREATED).entity("SUCCESS").build() : 
                    Response.status(Response.Status.BAD_REQUEST).entity(ERROR_TEXT).build();
        } catch (Throwable ex) {
            LOG.error(ex.getMessage());
            throw ex;
        }
    }
    
    @POST
    @Path("/update-passenger")
    @Produces(MediaType.APPLICATION_JSON)
    public Response postUpdatePassenger(@Valid String request) 
            throws Exception {
        UpdatePassengersRequest updatePassengersRequest= null;
        try{            
            try{
                updatePassengersRequest= JSONPrototype.convert(request, UpdatePassengersRequest.class);
            }catch(Exception e){
                throw e;
            }
            
            if(updatePassengerService.createPhoneAndEmailPassenger(updatePassengersRequest)){
                return Response.status(Response.Status.CREATED).entity("{\"message\": \"SUCCESS\"}").build();
            }else{
                return Response.status(Response.Status.BAD_REQUEST).entity("{\"message\": \"FIX\"}").build();
            }
        }catch(Exception e){
             LOG.error("Error to create email and phone number: {} rq: {}", 
                     e, updatePassengersRequest);
             WarningCollection warningCollection= new WarningCollection();
             Warning warning= new Warning();
             warning.setMsg("ERROR TO UPDATE PASSENGER DATA (EMAIL - PN)");
             warning.setExtraInfo(new ExtraInfo(e.getMessage()));
             warningCollection.getCollection().add(warning);             
             
            return Response.status(Response.Status.CREATED).entity(warningCollection.toString()).build();
        }
    }
}
