package com.am.checkin.web.controller;

import com.aeromexico.commons.model.FormOfPaymentCollection;
import com.aeromexico.commons.model.rq.PaymentMethodRQ;
import com.am.checkin.web.service.PaymentMethodsService;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Rocio Mendoza
 */
@Path("/payment-methods")
public class PaymentMethodsController {

    @Inject
    PaymentMethodsService paymentMethodsService;

    @GET
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPaymentMethods(@Valid @BeanParam PaymentMethodRQ paymentMethodRQ) throws Exception {

        FormOfPaymentCollection formOfPaymentCollection = paymentMethodsService.getPaymentMethods(paymentMethodRQ);

        return Response.status(Response.Status.OK).entity(formOfPaymentCollection).build();
    }

}
