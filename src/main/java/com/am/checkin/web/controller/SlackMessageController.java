/**
 *
 */
package com.am.checkin.web.controller;

import com.aeromexico.commons.prototypes.JSONPrototype;
import com.aeromexico.slack.models.Attachments;
import com.aeromexico.slack.models.SlackDataResponse;
import com.aeromexico.slack.service.SlackService;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Carlos Luna
 */
@Path("/slack-message")
public class SlackMessageController {

    private static final Logger logger = LoggerFactory.getLogger(SlackMessageController.class);

    @Inject
    private SlackService slackService;

    private static final String MESSAGE_LOGGER = "Sending information to slack general channel";

    /**
     * Definition of controller service
     *
     * @return Response with entity
     * @throws Exception
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response sendMessageGeneral(@QueryParam("textMessage") String textMessage) throws Exception {
        logger.info(MESSAGE_LOGGER);
        SlackDataResponse sendMessage = slackService.sendMessageGeneral(textMessage);
        return Response.status(Response.Status.OK).entity(sendMessage).build();
    }

    /**
     * Definition of controller service
     *
     * @return Response with entity
     * @throws Exception
     */
    @GET
    @Path("/{channel}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response sendMessageChannel(@QueryParam("textMessage") String textMessage, @QueryParam("userName") String userName, @PathParam("channel") String channel) throws Exception {
        logger.info(MESSAGE_LOGGER);
        boolean sendMessage = slackService.sendMessageChannel(textMessage);
        return Response.status(Response.Status.OK).entity(sendMessage).build();
    }

    /**
     * Definition of controller service
     *
     * @param textMessage
     * @param attach
     * @return Response with entity
     * @throws Exception
     */
    @GET
    @Path("/attachment")
    @Produces(MediaType.APPLICATION_JSON)
    public Response sendMessageAttachment(@QueryParam("textMessage") String textMessage, @QueryParam("attachments") String attach) throws Exception {
        logger.info(MESSAGE_LOGGER);
        Attachments attachments = JSONPrototype.convert(attach, Attachments.class);
        boolean sendMessage = slackService.sendMessageAttachment(textMessage, attachments);
        return Response.status(Response.Status.OK).entity(sendMessage).build();
    }
}
