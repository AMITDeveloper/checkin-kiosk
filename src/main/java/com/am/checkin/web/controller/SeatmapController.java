package com.am.checkin.web.controller;

import com.aeromexico.commons.model.AbstractSeatMap;
import com.aeromexico.commons.model.SeatMap;
import com.aeromexico.commons.model.SeatmapCollection;
import com.aeromexico.commons.model.SystemPropertiesFactory;
import com.aeromexico.commons.model.rq.SeatmapCheckInRQ;
import com.aeromexico.commons.web.types.SeatmapStatusType;
import com.aeromexico.dao.util.CommonUtil;
import com.aeromexico.slack.models.Attachments;
import com.aeromexico.slack.service.SlackService;
import com.am.checkin.web.service.LogOnDatabaseService;
import com.am.checkin.web.service.SeatMapsService;
import com.am.checkin.web.util.AttachmentUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

//@Path("")
public class SeatmapController {

    private static final Logger LOG = LoggerFactory.getLogger(SeatmapController.class);

    /**
     *
     */
    @Inject
    private SeatMapsService seatmapService;

    /**
     *
     */
    @Inject
    private LogOnDatabaseService logOnDatabaseService;

    /**
     *
     */
    @Inject
    private SlackService slackService;
    
    @Inject
    private SystemPropertiesFactory systemPropertiesFactory;

    @PostConstruct
    public void init() {
        if (null == systemPropertiesFactory) {
            systemPropertiesFactory = new SystemPropertiesFactory();
        }
    }

    //@GET
    //@Path("/seatmaps")
    //@Produces(MediaType.APPLICATION_JSON)
    public SeatmapCollection getSeatMap(@Valid @BeanParam SeatmapCheckInRQ seatmapRQ) {
        SeatmapCollection seatmapCollection;
        try {
            seatmapCollection = seatmapService.getSeatMaps(seatmapRQ);
            if(null == seatmapCollection){
                LOG.error("SeatMap.getSeatMap {} null");
                seatmapRQ.setUriStr(CommonUtil.getLink(seatmapRQ.getUri()));

                //Stop the timer
                long processed = seatmapRQ.processed();
                callMessageAttachment(processed, seatmapRQ);
                logOnDatabaseService.logSeatMapOnSuccess(seatmapRQ);

                seatmapCollection = new SeatmapCollection();
                List<AbstractSeatMap> abstractSeatMapList = new ArrayList<>();
                SeatMap seatMap = new SeatMap();
                seatMap.setStatus(SeatmapStatusType.UNAVAILABLE);
                abstractSeatMapList.add(seatMap);
                seatmapCollection.setCollection(abstractSeatMapList);

                return seatmapCollection;

            }else{
                LOG.info("SeatMap.getSeatMap {}" , seatmapCollection.toString());
                seatmapRQ.setUriStr(CommonUtil.getLink(seatmapRQ.getUri()));
                //Stop the timer
                long processed = seatmapRQ.processed();
                callMessageAttachment(processed, seatmapRQ);
                logOnDatabaseService.logSeatMapOnSuccess(seatmapRQ);

                return seatmapCollection;
            }

        } catch (Throwable ex) {
            try {
                Attachments attach = AttachmentUtil.getAttachmentException(seatmapRQ.getRecordLocator(), seatmapRQ.getUriStr(), ex.getMessage());
                slackService.sendMessageAttachment("Error alert API monitor: *SeatMap.getSeatMap*", attach);
            } catch (Exception ex1) {
                LOG.error(ex1.getMessage(), ex1);
            }
            logOnDatabaseService.logSeatMapOnError(seatmapRQ, ex);
            LOG.error("SeatMap.getSeatMap {}", ex.getMessage());
            seatmapCollection = new SeatmapCollection();
            List<AbstractSeatMap> abstractSeatMapList = new ArrayList<>();
            SeatMap seatMap = new SeatMap();
            seatMap.setStatus(SeatmapStatusType.UNAVAILABLE);
            abstractSeatMapList.add(seatMap);
            seatmapCollection.setCollection(abstractSeatMapList);

            return seatmapCollection;
        }


    }

    private void callMessageAttachment(long processed, SeatmapCheckInRQ seatMapRQ){
        try {
            if (processed > (systemPropertiesFactory.getInstance().getTimeoutSabreTransaction() * 1000)) {
                Attachments attach = AttachmentUtil.getAttachment(seatMapRQ.getRecordLocator(), seatMapRQ.getUriStr(), systemPropertiesFactory.getInstance().getTimeoutSabreTransaction(), processed);
                slackService.sendMessageAttachment("Error alert API monitor: *SeatMap.getSeatMap*", attach);
            }
        } catch (Exception ex1) {
            LOG.error("Error trying to send message to Slack channel in SeatMaps {}", ex1);
        }
    }
}
