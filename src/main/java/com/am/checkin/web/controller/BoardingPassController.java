package com.am.checkin.web.controller;

import com.aeromexico.commons.model.BoardingPassCollection;
import com.aeromexico.commons.model.rq.BoardingPassesRQ;
import com.aeromexico.dao.util.CommonUtil;
import com.aeromexico.slack.models.Attachments;
import com.aeromexico.slack.service.SlackService;
import com.am.checkin.web.service.BoardingPassesService;
import com.am.checkin.web.service.CheckInSetUpConfigFactoryWrapper;
import com.am.checkin.web.service.LogOnDatabaseService;
import com.am.checkin.web.util.AttachmentUtil;
import com.am.checkin.web.v2.services.BoardingDocumentsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.aeromexico.commons.model.TraceLogDTO;
import com.am.checkin.web.util.TraceLogUtil;

/**
 *
 * @author Carlos Luna
 */
@Path("/boarding-passes")
public class BoardingPassController {

    private static final Logger LOG = LoggerFactory.getLogger(BoardingPassController.class);

    /**
     * Boarding pass service
     */
    @Inject
    private BoardingPassesService boardingPassesService;

    @Inject
    private BoardingDocumentsService boardingPassesDocumentService;

    /**
     * Service for log information on database
     */
    @Inject
    private LogOnDatabaseService logOnDatabaseService;

    /**
     * Service for send messages to slack
     */
    @Inject
    private SlackService slackService;

    @Inject
    @Named("CheckInSetUpConfigFactoryWrapper")
    private CheckInSetUpConfigFactoryWrapper setUpConfigFactory;

    @PostConstruct
    public void init() {
        LOG.info("BoardingPassController initialized.");
    }

    /**
     * Definition of service
     *
     * @param boardingPassesRQ
     * @return
     * @throws Exception
     */
    @GET
    @Path("/{recordLocator}")
    @Produces(MediaType.APPLICATION_JSON)
    public BoardingPassCollection getBoardingPasses(@BeanParam BoardingPassesRQ boardingPassesRQ) throws Exception {       
        boardingPassesRQ.setUriStr(CommonUtil.getLink(boardingPassesRQ.getUri()));
        LOG.info("BoardingPassController:getBoardingPasses: {}", boardingPassesRQ);
        TraceLogDTO traceLogDto = new TraceLogDTO();
        traceLogDto = TraceLogUtil.getTraceLog(boardingPassesRQ, "boardingPasses");
        traceLogDto.setComponent("BoardingPassController");

        LOG.info("BoardingPassController:getBoardingPasses: {}", boardingPassesRQ);

        try {
            BoardingPassCollection boardingPasses = boardingPassesDocumentService.getBoardingPasses(boardingPassesRQ);
            if (null != boardingPasses) {
                boardingPasses.setCartId(boardingPassesRQ.getCartId());
            }
            slack(boardingPassesRQ.processed(), boardingPassesRQ);
            logOnDatabaseService.logBoardingPassOnSuccess(boardingPassesRQ);
            traceLogDto.setStatusBoardinPass("SUCCESSFUL");
            LOG.info("BOARDINGPASSES {} ",new Gson().toJson(traceLogDto));
            return boardingPasses;
        } catch (Throwable ex) {
            slack(boardingPassesRQ.processed(), boardingPassesRQ, ex);
            logOnDatabaseService.logBoardingPassOnError(boardingPassesRQ, ex);
            LOG.error("BoardingPasses.getBoardingPasses Error: {}", ex.getMessage(), ex);
            traceLogDto.setCodeError("NA");
            traceLogDto.setEndPointName("boardingPasses");
            traceLogDto.setStatusBoardinPass("UNSUCCESSFUL");
            traceLogDto.setMsgError(ex.getMessage().replaceAll("\\s","_"));
            traceLogDto.setPos(boardingPassesRQ.getPos());
            LOG.error("BOARDINGPASS {} ",new Gson().toJson(traceLogDto));
            throw ex;
        }
    }

    private void slack(long processed, BoardingPassesRQ boardingPassesRQ) {
        slack(processed, boardingPassesRQ, null);
    }

    private void slack(long processed, BoardingPassesRQ boardingPassesRQ, Throwable exception) {
        try {
            if (exception != null) {
                Attachments attach = AttachmentUtil.getAttachmentException(boardingPassesRQ.getRecordLocator(), boardingPassesRQ.getUriStr(), exception.getMessage());
                slackService.sendMessageAttachment("Error alert API monitor: *BoardingPasses.getBoardingPasses*", attach);
            } else if (processed > (setUpConfigFactory.getInstance().getTransactionTimeout() * 1000)) {
                Attachments attach = AttachmentUtil.getAttachment(boardingPassesRQ.getRecordLocator(), boardingPassesRQ.getUriStr(), setUpConfigFactory.getInstance().getTransactionTimeout(), processed);
                slackService.sendMessageAttachment("Error alert API monitor: *BoardingPasses.getBoardingPasses*", attach);
            }
        } catch (Exception ex) {
            LOG.error("BoardingPassController:getBoardingPasses Slack message failed: " + ex.getMessage(), ex);
        }
    }

    public CheckInSetUpConfigFactoryWrapper getSetUpConfigFactory() {
        return setUpConfigFactory;
    }

    public void setSetUpConfigFactory(CheckInSetUpConfigFactoryWrapper setUpConfigFactory) {
        this.setUpConfigFactory = setUpConfigFactory;
    }

}
