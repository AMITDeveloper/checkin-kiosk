package com.am.checkin.web.controller;

import java.net.URI;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.BeanParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.aeromexico.cloudfiles.AMCloudFile;
import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.Ancillaries;
import com.aeromexico.commons.model.AuthorizationResult;
import com.aeromexico.commons.model.CheckinConfirmation;
import com.aeromexico.commons.model.CheckinConfirmationBase;
import com.aeromexico.commons.model.FormsOfPaymentWrapper;
import com.aeromexico.commons.model.SystemPropertiesFactory;
import com.aeromexico.commons.model.ThreeDsFormResponse;
import com.aeromexico.commons.model.TraceLogDTO;
import com.aeromexico.commons.model.rq.Payment3dsRQ;
import com.aeromexico.commons.model.rq.PurchaseOrderRQ;
import com.aeromexico.commons.web.types.PosType;
import com.aeromexico.dao.client.exception.MongoDisconnectException;
import com.aeromexico.sabre.api.session.common.ServiceCall;
import com.aeromexico.sabre.api.session.exception.SabreLayerUnavailableException;
import com.aeromexico.slack.models.Attachments;
import com.aeromexico.slack.service.SlackService;
import com.aeromexico.webe4.web.purchaseorder.model.ThreeDSecureTokenResponse;
import com.am.checkin.web.service.LogOnDatabaseService;
import com.am.checkin.web.service.PaymentAuthorizationService;
import com.am.checkin.web.service.PurchaseOrderService;
import com.am.checkin.web.util.AttachmentUtil;
import com.am.checkin.web.util.TraceLogUtil;
import com.am.checkin.web.v2.util.SystemVariablesUtil;
import com.google.gson.Gson;

import org.aeromexico.commons.exception.config.ReadProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author adrian
 */
@Path("/po")
public class PurchaseOrderController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PurchaseOrderController.class);

    @Inject
    private PurchaseOrderService purchaseOrderService;
    @Inject
    private PaymentAuthorizationService paymentAuthorizationService;

    @Inject
    private LogOnDatabaseService logOnDatabaseService;

    @Inject
    private SlackService slackService;

    @Inject
    private SystemPropertiesFactory systemPropertiesFactory;

    @Inject
    private ReadProperties prop;

    private static final String CHECKIN_PURCHASEORDER = "PURCHASEORDER";

    @PostConstruct
    public void init() {
        if (null == systemPropertiesFactory) {
            systemPropertiesFactory = new SystemPropertiesFactory();
        }
    }

    @POST
    @PermitAll
    @Path("/3ds-reroute-ckin/redirect")
    public Response threeDsRedirect(@Valid @BeanParam Payment3dsRQ request) {
        try {
            LOGGER.info("/3ds-reroute-ckin/redirect request: {}", request.toString());
            StringBuilder sb = new StringBuilder();
            sb.append(SystemVariablesUtil.getThreeDsRedirectPath());
            sb.append("?status=").append(request.getStatus());
            sb.append("&ResultCode=").append(request.getResultCode());
            sb.append("&PaymentRef=").append(request.getPaymentRef());
            sb.append("&ResponseCode=").append(request.getResponseCode());
            sb.append("&ApprovalCode=").append(request.getApprovalCode());
            sb.append("&SupplierID=").append(request.getSupplierID());
            sb.append("&SupplierTransID=").append(request.getSupplierTransID());
            sb.append("&SupplierResponseCode=").append(request.getSupplierResponseCode());
            sb.append("&MerchantID=").append(request.getMerchantID());
            String url = sb.toString();
            LOGGER.info("url: {}", url);
            URI location = new URI(url);
            LOGGER.info("PurchseOrderChk",location);
            return Response.seeOther(location).build();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }

        return Response.ok().build();
    }

    @POST
    @PermitAll
    @Path("/3ds-reroute-ckin/{result3ds}")
    @Produces(MediaType.TEXT_HTML)
    public Response threeDsPostMessage(
            @Valid @BeanParam Payment3dsRQ request,
            @PathParam("result3ds") String result3ds
    ) {

        LOGGER.info("/3ds-reroute-ckin/{result3ds} 3ds result: {}", result3ds);
        ThreeDSecureTokenResponse token = new ThreeDSecureTokenResponse(
                request.getResultCode(),
                request.getPaymentRef(),
                request.getResponseCode(),
                request.getSupplierID(),
                request.getMerchantID()
        );

        token.setApprovalCode(request.getApprovalCode());
        return Response
                .status(Response.Status.CREATED)
                .entity(token.returnPostMessageScript())
                .build();
    }

    @POST
    @Path("/auth")
    @Produces(MediaType.APPLICATION_JSON)
    public Response authorization(@Valid @BeanParam PurchaseOrderRQ purchaseOrderRQ) throws Exception {
        purchaseOrderService.loadProfileFormOfPayments(new FormsOfPaymentWrapper(
                purchaseOrderRQ.getPurchaseOrder().getPaymentInfo()), purchaseOrderRQ.getPos());

        try {
            List<ServiceCall> serviceCallList = new ArrayList<>();
            List<AuthorizationResult> authorizationResultList = paymentAuthorizationService.doPaymentAuthorization(
                    purchaseOrderRQ,
                    serviceCallList
            );

            //Stop the timer
            long processed = purchaseOrderRQ.processed();

            try {
                if (purchaseOrderRQ.getTotalTime() > (systemPropertiesFactory.getInstance().getTimeoutSabreTransaction() * 1000)) {
                    Attachments attach = AttachmentUtil.getAttachment(purchaseOrderRQ.getRecordLocator(), purchaseOrderRQ.getUriStr(), systemPropertiesFactory.getInstance().getTimeoutSabreTransaction(), processed);
                    slackService.sendMessageAttachment("Error alert API monitor: *PaymentAuthorizationService.doPaymentAuthorization*", attach);
                }
            } catch (Throwable ex1) {
                LOGGER.error("Error trying to send message to Slack channel in PO authotization { }", ex1);
            }

            try {
                checkTransactionTime(purchaseOrderRQ, serviceCallList, processed);
            } catch (Throwable ex1) {
                LOGGER.error("Error trying to send message to Slack channel in PO authotization { }", ex1);
            }

            AuthorizationResult authorizationResult = authorizationResultList.get(0);

            GenericEntity<AuthorizationResult> genericEntity = new GenericEntity<>(authorizationResult, AuthorizationResult.class);
            return Response.status(Response.Status.CREATED).entity(genericEntity).build();
        } catch (Throwable ex) {
            LOGGER.error(ex.getMessage());
            throw ex;
        }
    }

    @POST
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    public Response postPurchaseOrder(@Valid @BeanParam PurchaseOrderRQ purchaseOrderRQ) throws Exception {
    	TraceLogDTO traceLogDto = new TraceLogDTO();
    	traceLogDto.setComponent("PurchaseOrderController");
    	traceLogDto.setBookingClass("NA");
        traceLogDto.setDestination("NA");
        traceLogDto.setTotalPassenger("0");
        traceLogDto.setNumberFlight("NA");
        traceLogDto.setOrigin("NA");
        traceLogDto.setFormOfPayment("NA");
        traceLogDto.setEndPointName("po");
        traceLogDto.setStatusPurchaseOrder("UNSUCCESSFUL");
        traceLogDto.setAncillaries(new Ancillaries());
        traceLogDto.setCodeError("NA");
        traceLogDto.setFormOfPayment("NA");
        traceLogDto.setCurrency("NA");
        traceLogDto.setRoute("NA");
        Date date = new Date();
    	DateFormat hourdateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	traceLogDto.setDate("" + hourdateFormat.format(date).replaceAll("\\s","_"));
        try {
            List<ServiceCall> serviceCallList = new ArrayList<>();

            CheckinConfirmationBase checkinConfirmationBase;
            checkinConfirmationBase = purchaseOrderService.doPurchase(purchaseOrderRQ, serviceCallList);

            if (checkinConfirmationBase instanceof ThreeDsFormResponse) {
                ThreeDsFormResponse threeDsFormResponse = (ThreeDsFormResponse) checkinConfirmationBase;

                GenericEntity<ThreeDsFormResponse> genericEntity = new GenericEntity<>(threeDsFormResponse, ThreeDsFormResponse.class);
                return Response.status(Response.Status.CREATED).entity(genericEntity).build();
            } else {

                CheckinConfirmation checkinConfirmation = (CheckinConfirmation) checkinConfirmationBase;

                //Stop the timer
                long processed = purchaseOrderRQ.processed();

                try {
                    if (purchaseOrderRQ.getTotalTime() > (systemPropertiesFactory.getInstance().getTimeoutSabreTransaction() * 1000)) {
                        Attachments attach = AttachmentUtil.getAttachment(purchaseOrderRQ.getRecordLocator(), purchaseOrderRQ.getUriStr(), systemPropertiesFactory.getInstance().getTimeoutSabreTransaction(), processed);
                        slackService.sendMessageAttachment("Error alert API monitor: *PurchaseOrderService.doPurchase*", attach);
                    }
                } catch (Throwable ex1) {
                    LOGGER.error("Error trying to send message to Slack channel in PO {0}", ex1);
                }

                try {
                    checkTransactionTime(purchaseOrderRQ, serviceCallList, processed);
                } catch (Throwable ex1) {
                    LOGGER.error("Error trying to send message to Slack channel in PO {0}", ex1);
                }

                //For downloadReceipt
                if (checkinConfirmation.getPaymentSummary() != null
                        && checkinConfirmation.getPaymentSummary().getCollection() != null &&
                        checkinConfirmation.getPaymentSummary().getCollection().size() > 0) {
                    checkinConfirmation.setItineraryPdf(
                            createDownloadLinkRackspace(checkinConfirmation.getBoardingPassId()));
                }
                
                GenericEntity<CheckinConfirmation> genericEntity = new GenericEntity<>(checkinConfirmation, CheckinConfirmation.class);
                traceLogDto = TraceLogUtil.getTraceLog(purchaseOrderRQ, genericEntity, "po");
                LOGGER.info(CHECKIN_PURCHASEORDER + " {} ", new Gson().toJson(traceLogDto));
                return Response.status(Response.Status.CREATED).entity(genericEntity).build();
            }
        } catch (MongoDisconnectException me) {
        	traceLogDto.setPnr(purchaseOrderRQ.getRecordLocator());
            traceLogDto.setPos(purchaseOrderRQ.getPos());
        	traceLogDto.setCodeError(com.aeromexico.commons.web.util.Constants.CODE_22110101);
            traceLogDto.setMsgError(me.getMessage().replaceAll("\\s",""));
            LOGGER.error(CHECKIN_PURCHASEORDER," {} ", traceLogDto.toString());
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(purchaseOrderRQ.getPos())) {
                LOGGER.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
                throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getCode(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getActionable(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getMessage(),
                        Response.Status.INTERNAL_SERVER_ERROR);
            }else{
            LOGGER.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
            throw me;
            }
        } catch (SabreLayerUnavailableException ex) {
        	traceLogDto.setPnr(purchaseOrderRQ.getRecordLocator());
            traceLogDto.setPos(purchaseOrderRQ.getPos());
        	traceLogDto.setCodeError(com.aeromexico.commons.web.util.Constants.CODE_22110301);
            traceLogDto.setMsgError(ex.getMessage().replaceAll("\\s","_"));
            LOGGER.error(CHECKIN_PURCHASEORDER + " {} ", new Gson().toJson(traceLogDto));
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(purchaseOrderRQ.getPos())) {
                LOGGER.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_22110301,"PurchaseOrderController.java:273", ex.getMessage());
                throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getCode(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getActionable(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getMessage(),
                        Response.Status.INTERNAL_SERVER_ERROR);
            } else {
                LOGGER.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_12110301,"PurchaseOrderController.java:279", ex.getMessage());
                throw ex;
            }
        } catch (GenericException ex) {
        	traceLogDto.setPnr(purchaseOrderRQ.getRecordLocator());
            traceLogDto.setPos(purchaseOrderRQ.getPos());
        	traceLogDto.setCodeError(ex.getErrorCodeType().toString());
            traceLogDto.setMsgError(ex.getMessage().replaceAll("\\s","_"));
            LOGGER.error(CHECKIN_PURCHASEORDER + " {} ", new Gson().toJson(traceLogDto));
            if (!ex.wasLogged()) {
                logOnDatabaseService.logPreCheckinOnError(purchaseOrderRQ, purchaseOrderRQ.getCartId(), purchaseOrderRQ.getRecordLocator(), ex);
            }
            throw ex;
        } catch (Exception ex) {
        	traceLogDto.setPnr(purchaseOrderRQ.getRecordLocator());
            traceLogDto.setPos(purchaseOrderRQ.getPos());
            traceLogDto.setMsgError(ex.getMessage().replaceAll("\\s","_"));
            LOGGER.error(CHECKIN_PURCHASEORDER + " {} ", new Gson().toJson(traceLogDto));
            logOnDatabaseService.logPreCheckinOnError(purchaseOrderRQ, purchaseOrderRQ.getCartId(), purchaseOrderRQ.getRecordLocator(), ex);
            throw ex;
        } catch (Throwable ex) {
        	traceLogDto.setPnr(purchaseOrderRQ.getRecordLocator());
            traceLogDto.setPos(purchaseOrderRQ.getPos());
            traceLogDto.setMsgError(ex.getMessage().replaceAll("\\s","_"));
            LOGGER.error(CHECKIN_PURCHASEORDER + " {} ", new Gson().toJson(traceLogDto));
            logOnDatabaseService.logPreCheckinOnError(purchaseOrderRQ, purchaseOrderRQ.getCartId(), purchaseOrderRQ.getRecordLocator(), ex);
            throw ex;
        }
    }

    private void checkTransactionTime(PurchaseOrderRQ purchaseOrderRQ, List<ServiceCall> serviceCallList, long processed) {
        if (purchaseOrderRQ.getTotalTime() > (systemPropertiesFactory.getInstance().getTimeoutSabreTransaction() * 1000)) {

            Attachments attach = AttachmentUtil.getAttachment(purchaseOrderRQ.getRecordLocator(), purchaseOrderRQ.getUriStr(), systemPropertiesFactory.getInstance().getTimeoutSabreTransaction(), processed);
            long totalTime = 0;
            for (ServiceCall serviceCall : serviceCallList) {
                totalTime += serviceCall.getTime();
            }
            serviceCallList.add(new ServiceCall("Total", totalTime));
            slackService.sendMessageAttachment("Transaction took more than " + systemPropertiesFactory.getInstance().getTimeoutSabreTransaction() + " seconds. Time per service:\n" + serviceCallList.toString(), attach);

            LOGGER.info("Transaction took more than " + systemPropertiesFactory.getInstance().getTimeoutSabreTransaction() + " seconds. Time per service:\n" + serviceCallList.toString());
            LOGGER.info("PurchseOrderChk", systemPropertiesFactory.getInstance().getTimeoutSabreTransaction() + " seconds. Time per service:\n" + serviceCallList.toString());
        }
    }

    /**
     * Create download link to rackspace files
     *
     * @param recordLocator
     * @return
     */
    private String createDownloadLinkRackspace(String recordLocator) {
        AMCloudFile amCloudFile = new AMCloudFile();
        String downloadLink = amCloudFile.getPublishURL();
        amCloudFile.closeConnection();
        return downloadLink + "/" + recordLocator + "/" + "Aeromexico_Receipt_"+ recordLocator + ".pdf";
    }

}
