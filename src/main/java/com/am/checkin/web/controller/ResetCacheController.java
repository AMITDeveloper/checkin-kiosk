package com.am.checkin.web.controller;

import com.am.checkin.web.service.ResetCacheService;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adrian
 */
@Path("/resetcache")
public class ResetCacheController {

    private static final Logger LOG = LoggerFactory.getLogger(ResetCacheController.class);

    @Inject
    private ResetCacheService resetCacheService;

    @GET
    @Path("/{collection}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response resetCollection(@PathParam("collection") String collection) throws Exception {
        try {

            Map<String, List<String>> collectionsResetedMap  = resetCacheService.resetCollection(collection);

            return Response.status(Response.Status.OK).entity(collectionsResetedMap).build();
        } catch (Throwable ex) {
            LOG.error(ex.getMessage());
            throw ex;
        }
    }

    @GET
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response resetCollections() throws Exception {
        try {

            Map<String, List<String>> collectionsResetedMap  = resetCacheService.resetCollections();

            return Response.status(Response.Status.OK).entity(collectionsResetedMap).build();
        } catch (Throwable ex) {
            LOG.error(ex.getMessage());
            throw ex;
        }
    }

}
