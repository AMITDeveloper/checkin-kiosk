package com.am.checkin.web.controller;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeromexico.commons.model.rq.AirAvailRQ;
import com.am.checkin.web.service.AirAvailService;
import com.google.gson.Gson;
import com.sabre.webservices.sabrexml._2011._10.airavailable.OTAAirAvailRS;

@Path("air-avail")
public class AirAvailController {

	private static final Logger log = LoggerFactory.getLogger(AirAvailController.class);

	@Inject
	AirAvailService airAvailService;
	
	private Gson gson = new Gson();

	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFlightStatus(@Valid @BeanParam AirAvailRQ airAvailRQ) throws Exception {

		OTAAirAvailRS otaAirAvailRS = null;

		try {
			otaAirAvailRS = airAvailService.getAirAvail(airAvailRQ);
		} catch (Exception e) {
			log.error("AirAvail GET: " + e.getMessage());
			throw e;
		}
		return Response.status(Response.Status.OK).entity( gson.toJson(otaAirAvailRS) ).build();
	}

}
