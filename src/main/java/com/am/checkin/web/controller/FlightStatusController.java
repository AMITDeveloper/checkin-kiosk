package com.am.checkin.web.controller;

import com.aeromexico.commons.model.FlightStatusCollection;
import com.aeromexico.commons.model.rq.FlightStatusRQ;
import com.aeromexico.dao.util.CommonUtil;
import com.am.checkin.web.v2.services.FlightStatusService;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Armando Arroyo
 */
//@Path("flight-status")
public class FlightStatusController {

    private static final Logger log = LoggerFactory.getLogger(FlightStatusController.class);

    @Inject
    private FlightStatusService flightStatusService;

    @GET
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getFlightStatus(@Valid @BeanParam FlightStatusRQ flightStatusRQ, @Context UriInfo uri) {
        FlightStatusCollection flightStatusCollection = null;
        try {
            flightStatusRQ.setPath(CommonUtil.getLink(uri));
            flightStatusCollection = flightStatusService.callFlightStatus(flightStatusRQ);
        } catch (Exception e) {
            log.error("Flight Status GET: {}, {}", flightStatusRQ.getPath(), e.getMessage());
        }
        return Response.status(Response.Status.OK).entity(flightStatusCollection).build();
    }

}
