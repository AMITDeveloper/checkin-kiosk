package com.am.checkin.web.controller;

import com.aeromexico.commons.bigquery.services.BigQueryService;
import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.mailing.BigQueryTableEnum;
import com.aeromexico.commons.model.mailing.DocumentTypeEnum;
import static com.aeromexico.commons.model.mailing.MailingConstants.INCIDENT;
import static com.aeromexico.commons.model.mailing.MailingConstants.NULL_ADDRESSSES;
import static com.aeromexico.commons.model.mailing.MailingConstants.NULL_PNR;
import static com.aeromexico.commons.model.mailing.MailingConstants.NULL_TRANSACTION;
import com.aeromexico.commons.model.rq.EmailBoardingPassRQ;
import com.aeromexico.commons.model.rq.EmailReceiptRQ;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.types.PosType;
import com.am.checkin.web.service.EmailService;
import java.io.File;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("")
public class EMailController {

    private static final Logger logger = LoggerFactory.getLogger(EMailController.class);

    /**
     * Service for send mail
     */
    @Inject
    private EmailService emailService;

    /**
     * Service for email boarding pass
     *
     * @param emailBoardingPassRQ
     * @return
     */
    @POST
    @Path("/email-boarding-pass")
    @Produces(MediaType.APPLICATION_JSON)
    public Response eMailBoardingPass(@Valid @BeanParam EmailBoardingPassRQ emailBoardingPassRQ) {
        if (emailBoardingPassRQ.getBoardingPassId() != null) {
            logger.info("Sending the information of boarding pass");
            try {
                emailService.sendBoardingPassMailing(emailBoardingPassRQ);
                logger.info("EMAIL BOARDING PASS WAS SENT");
            } catch (Exception e) {
                logger.info("EMAIL INFORMATION NOT SENT (CHK BOARDING PASS): " + e.getMessage());
                try {
                    if(!e.getMessage().contains(INCIDENT)){  //Avoid to send the same notification 
                        BigQueryService.notReceivedNotification(emailBoardingPassRQ.getBoardingPassId() != null ?
                                emailBoardingPassRQ.getBoardingPassId()
                                :NULL_PNR,
                                emailBoardingPassRQ.getCartId() != null ? emailBoardingPassRQ.getCartId()
                                :NULL_TRANSACTION,
                                emailBoardingPassRQ.getEmailList() != null ? String.join(", ", emailBoardingPassRQ.getEmailList())
                                :NULL_ADDRESSSES,
                                System.currentTimeMillis(),
                                DocumentTypeEnum.getDocumentTypeEnum(
                                    emailBoardingPassRQ.getPos() != null ? emailBoardingPassRQ.getPos(): PosType.WEB.toString(), 
                                    DocumentTypeEnum.BOARDING_PASS).toString(),
                                BigQueryTableEnum.DOCUMENT_MAILING.toString(),
                                true,
                                e.getMessage(), 
                                emailService.getEnvironment());
                    }             
                } catch (Exception ex) {
                    logger.error("ERROR TO SEND NOTIFCATION BIG QUERY: {}", ex);
                }
                
                 throw new GenericException(Response.Status.NOT_FOUND, 
                            ErrorType.AM_DOCUMENTS_API_NOT_SENT_INFORMATION, 
                            e.getMessage());
            }
            
            return Response.status(Response.Status.CREATED).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    /**
     * Service for emailing receipt of purchase for ancillaries
     *
     * @param emailReceiptRQ
     * @param userAuth
     * @return
     */
    @POST
    @Path("/email-receipt")
    @Produces(MediaType.APPLICATION_JSON)
    public Response eMailAncillary(@Valid @BeanParam EmailReceiptRQ emailReceiptRQ, @HeaderParam("X-AM-User-Auth") String userAuth) {
        logger.info("Sending the receipt for a checkin purchase");
        try {
            emailService.sendBoardingPassReceiptMailing(emailReceiptRQ, userAuth);
            logger.info("EMAIL CHK PAYMENT RECEIPT WAS SENT");
        } catch (Exception e) {
            logger.info("EMAIL INFORMATION NOT SENT (CHK PAYMENT RECEIPT): " + e.getMessage());
            try {
                if(!e.getMessage().contains(INCIDENT)){  //Avoid to send the same notification 
                    BigQueryService.notReceivedNotification(emailReceiptRQ.getPnr() != null ? emailReceiptRQ.getPnr()
                            :NULL_PNR,
                            emailReceiptRQ.getCartId() != null ? emailReceiptRQ.getCartId()
                            :NULL_TRANSACTION,
                            emailReceiptRQ.getEmailList() != null ? String.join(", ", emailReceiptRQ.getEmailList())
                            :NULL_ADDRESSSES,
                            System.currentTimeMillis(),
                            DocumentTypeEnum.getDocumentTypeEnum(
                                emailReceiptRQ.getPos() != null ? emailReceiptRQ.getPos(): PosType.WEB.toString(), 
                                DocumentTypeEnum.PAYMENT_RECEIPT).toString(),
                            BigQueryTableEnum.DOCUMENT_MAILING.toString(),
                            true,
                            e.getMessage(),
                            emailService.getEnvironment());
                }           
            } catch (Exception ex) {
                logger.error("ERROR TO SEND NOTIFCATION BIG QUERY: {}", ex);
            }
            
            throw new GenericException(Response.Status.NOT_FOUND, 
                            ErrorType.AM_DOCUMENTS_API_NOT_SENT_INFORMATION, 
                            e.getMessage());
        }
        return Response.status(Response.Status.CREATED).build();
    }

    @GET
    @Path("/boarding-pass-download")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response eMailDownload(@QueryParam("pnr") String pnr,
            @QueryParam("legCode") String legCode,
            @QueryParam(value = "ticketNumber") String ticketNumber,
            @QueryParam("language") String language) {
        logger.info("Download already created boarding pass");
        try {
            File pdfBoardingPass = emailService.downloadBoardingPass(pnr, ticketNumber, language);
            if (pdfBoardingPass != null) {
                return Response.ok(pdfBoardingPass, MediaType.APPLICATION_OCTET_STREAM)
                        .status(Response.Status.CREATED)
                        .header("Content-Disposition", "attachment; filename=\"" + pdfBoardingPass.getName() + "\"")
                        .build();
            } else {
                return Response.serverError()
                        .build();
            }
        } catch (Exception ex) {
            return Response.serverError()
                    .build();
        }
    }

}
