package com.am.checkin.web.controller;

import com.aeromexico.commons.model.IssueBagRP;
import com.aeromexico.commons.model.SystemPropertiesFactory;
import com.aeromexico.commons.model.rq.IssueBagRQ;
import com.aeromexico.dao.util.CommonUtil;
import com.aeromexico.slack.models.Attachments;
import com.aeromexico.slack.service.SlackService;
import com.am.checkin.web.service.CheckInSetUpConfigFactoryWrapper;
import com.am.checkin.web.service.IssueBagTagService;
import com.am.checkin.web.service.LogOnDatabaseService;
import com.am.checkin.web.util.AttachmentUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/issueBagTag")
public class IssueBagTagController {

    private static final Logger LOG = LoggerFactory.getLogger(IssueBagTagController.class);

    /**
     *
     */
    @Inject
    private IssueBagTagService issueBagTagService;

    /**
     *
     */
    @Inject
    private LogOnDatabaseService logOnDatabaseService;

    /**
     *
     */
    @Inject
    private SlackService slackService;
    
    @Inject
    private SystemPropertiesFactory systemPropertiesFactory;

    @Inject
    @Named("CheckInSetUpConfigFactoryWrapper")
    private CheckInSetUpConfigFactoryWrapper setUpConfigFactory;

    @PostConstruct
    public void init() {
        if (null == setUpConfigFactory) {
            setUpConfigFactory = new CheckInSetUpConfigFactoryWrapper();
        }
    }

    @PUT
    @Path("/{cartId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response putIssueBag(@Valid @BeanParam IssueBagRQ issueBagRQ) throws Exception {

        LOG.info("issueBagTag RQ: " + issueBagRQ);

        try {

            IssueBagRP issueBagRP = issueBagTagService.getBagTag(issueBagRQ);
            GenericEntity<IssueBagRP> genericEntityRP = new GenericEntity<>(issueBagRP, IssueBagRP.class);
            issueBagRQ.setUriStr(CommonUtil.getLink(issueBagRQ.getUri()));

            //Stop the timer
            long processed = issueBagRQ.processed();
            try {
                if (processed > (setUpConfigFactory.getInstance().getTransactionTimeout() * 1000)) {
                    Attachments attach = AttachmentUtil.getAttachment(issueBagRQ.getRecordLocator(), issueBagRQ.getUriStr(), setUpConfigFactory.getInstance().getTransactionTimeout(), processed);
                    slackService.sendMessageAttachment("Error alert API monitor : *IssueBagTag.putIssueBag*", attach);
                }
            } catch (Exception ex1) {
                LOG.error("Error trying to send message to Slack channel in IssueBagTag {}", ex1);
            }

            logOnDatabaseService.logBagTagOnSuccess(issueBagRQ);

            return Response.status(Response.Status.OK).entity(genericEntityRP).build();

        } catch (Throwable ex) {
            try {
                Attachments attach = AttachmentUtil.getAttachmentException(issueBagRQ.getRecordLocator(), issueBagRQ.getUriStr(), ex.getMessage());
                slackService.sendMessageAttachment("Error alert API monitor: *IssueBagTag.putIssueBag*", attach);
            } catch (Exception ex1) {
                LOG.error(ex1.getMessage(), ex1);
            }
            logOnDatabaseService.logBagTagOnError(issueBagRQ, ex);
            if (ex != null && ex.getStackTrace() != null && ex.getStackTrace().length > 0) {
                LOG.error("issueBagTag " + ex.getMessage() + " " + ex.getStackTrace()[0].toString());
            } else {
                LOG.error("issueBagTag Exception: NO TRACE INFO");
            }
            throw ex;
        }
    }

}
