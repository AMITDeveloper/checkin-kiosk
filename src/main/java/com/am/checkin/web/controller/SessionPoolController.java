package com.am.checkin.web.controller;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeromexico.commons.model.SabreSessionPool;
import com.am.checkin.web.service.SessionPoolService;

@Path("/sessionpool")
public class SessionPoolController {

    @Inject
    SessionPoolService sessionPoolService;

    private static final Logger LOG = LoggerFactory.getLogger(SessionPoolController.class);

    @GET
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getSessionPool() throws Exception {
        try {

            SabreSessionPool sabreSessionPool = sessionPoolService.getSessionPoolInformation();

            return Response.status(Response.Status.OK).entity(sabreSessionPool).build();

        } catch (Throwable e) {
            LOG.error(e.getMessage());
            throw e;
        }
    }

}
