package com.am.checkin.web.controller;

import com.aeromexico.commons.model.AbstractSeatMap;
import com.aeromexico.commons.model.PreSelectSeatResponse;
import com.aeromexico.commons.model.SeatMap;
import com.aeromexico.commons.model.SeatmapCollection;
import com.aeromexico.commons.model.SystemPropertiesFactory;
import com.aeromexico.commons.model.rq.SeatMapsRQ;
import com.aeromexico.commons.model.rq.SeatmapCheckInRQ;
import com.aeromexico.commons.web.types.SeatmapStatusType;
import com.am.checkin.web.service.LogOnDatabaseService;
import com.am.checkin.web.service.SeatMapsServiceSoap;
import com.am.checkin.web.v2.util.SeatmapCollectionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@Path("")
public class SeatMapControllerSoap {

    private static final Logger LOG = LoggerFactory.getLogger(SeatMapControllerSoap.class);

    @Inject
    private SystemPropertiesFactory systemPropertiesFactory;

    @Inject
    private LogOnDatabaseService logOnDatabaseService;

    @Inject
    private SeatMapsServiceSoap seatMapsServiceSoap;

    @PostConstruct
    public void init() {
        if (null == systemPropertiesFactory) {
            systemPropertiesFactory = new SystemPropertiesFactory();
        }
    }

    @GET
    @Path("/seatmaps")
    @Produces(MediaType.APPLICATION_JSON)
    public SeatmapCollection getSeatMap(@Valid @BeanParam SeatmapCheckInRQ seatmapRQ) {
        SeatmapCollection seatmapCollection;

        try {
            seatmapCollection = seatMapsServiceSoap.getSeatMaps(seatmapRQ);

            LOG.info("SeatMap.getSeatMap {}", seatmapCollection.toString());
            logOnDatabaseService.logSeatMapOnSuccess(seatmapRQ);

            return seatmapCollection;

        } catch (Throwable ex) {

            logOnDatabaseService.logSeatMapOnError(seatmapRQ, ex);
            LOG.error("SeatMap.getSeatMap {}", ex.getMessage(), ex);

            seatmapCollection = SeatmapCollectionUtil.getSeatmapCollectionEmpty();

            return seatmapCollection;
        }

    }

    @GET
    @Path("seatmaps/preSelectSeat")
    @Produces(MediaType.APPLICATION_JSON)
    public PreSelectSeatResponse getPreSelectSeat(@Valid @BeanParam SeatMapsRQ seatMapsRQ) {
        PreSelectSeatResponse preSelectSeatResponse = new PreSelectSeatResponse();
        try {
            preSelectSeatResponse = seatMapsServiceSoap.preSelectSeat(seatMapsRQ);
        } catch (Exception ex) {
            LOG.error("Seat Maps Preselect, " + seatMapsRQ.getCartId() + ", " + ex.getMessage());
        }
        return preSelectSeatResponse;
    }
    
}
