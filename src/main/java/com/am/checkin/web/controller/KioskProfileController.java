/**
 *
 */
package com.am.checkin.web.controller;

import com.aeromexico.commons.model.KioskProfiles;
import com.aeromexico.commons.model.rq.KioskProfileRQ;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.exception.model.GenericException;
import com.am.checkin.web.service.KioskProfileService;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.List;

/**
 * @author Carlos Luna
 *
 */
@Path("/kiosk-profile")
public class KioskProfileController {

    private static final Logger logger = LoggerFactory.getLogger(KioskProfileController.class);

    /**
     * Boarding pass service
     */
    @Inject
    private KioskProfileService kioskProfileService;

    /**
     * Definition of service
     *
     * @param kioskProfileRq
     * @return
     * @throws Exception
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getKioskProfiles(@BeanParam KioskProfileRQ kioskProfileRq) throws Exception {
        logger.info("KioskProfileController.getKioskProfiles(), Calling profile service kiosk");
        if (StringUtils.isNotBlank(kioskProfileRq.getId())) {
            KioskProfiles kioskProfile = kioskProfileService.getProfiles(kioskProfileRq.getId());
            return Response.status(Response.Status.OK).entity(kioskProfile).build();
        } else {
            List<KioskProfiles> kioskProfiles = kioskProfileService.getListProfiles();
            return Response.status(Response.Status.OK).entity(kioskProfiles).build();
        }
    }

    /**
     * Put service for kiosk profile
     *
     * @param requestBody
     * @return
     * @throws Exception
     */
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public KioskProfiles saveKioskProfiles(@BeanParam KioskProfileRQ requestBody) throws Exception {
        logger.info("KioskProfileController.saveKioskProfiles(), Trying to save profile in the database");
        return kioskProfileService.saveProfile(requestBody.getId(), requestBody.getKioskProfile());
    }

    /**
     * Definition of service
     *
     * @param kioskProfileRq
     * @return
     * @throws Exception
     */
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteKioskProfiles(@BeanParam KioskProfileRQ kioskProfileRq) throws Exception {

    	logger.info("KioskProfileController.deleteKioskProfiles(), Calling profile service kiosk");

    	KioskProfiles kioskProfile = kioskProfileService.getProfiles(kioskProfileRq.getId());
    	if ( kioskProfile != null && kioskProfile.getId() != null ) {
    		kioskProfileService.deleteProfiles(kioskProfileRq.getId());	
    	} else {
    		throw new GenericException(Response.Status.NOT_FOUND, ErrorType.NOT_FOUND_PROFILE_FOR_KIOSK, "PROFILE NOT FOUND, ID: [" + kioskProfileRq.getId() + "]");
    	}
    	return Response.status(Response.Status.OK).entity(kioskProfile).build();
    }

}
