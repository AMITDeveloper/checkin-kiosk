package com.am.checkin.web.controller;

import com.aeromexico.commons.model.rq.PinpadAuthRQ;
import com.am.checkin.web.service.LogOnDatabaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/log")
public class DatabaseLogController {

    private static final Logger LOG = LoggerFactory.getLogger(DatabaseLogController.class);

    @Inject
    private LogOnDatabaseService logOnDatabaseService;

    @PUT
    @Path("/pinpadAuth/{cartId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response putShoppingCart(@Valid @BeanParam PinpadAuthRQ pinpadAuthRQ) throws Exception {
        try {

            logOnDatabaseService.logPinpadAuth(pinpadAuthRQ);

            return Response.status(Response.Status.OK).entity("{\"response\":\"OK\"}").build();

        } catch (Throwable ex) {

            LOG.error(ex.getMessage());
            throw ex;
        }
    }

}
