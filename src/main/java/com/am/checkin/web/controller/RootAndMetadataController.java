package com.am.checkin.web.controller;

import com.aeromexico.commons.model.Metadata;
import com.am.checkin.web.service.RootAndMetadataService;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author Rocio Mendoza
 */
@Path("/")
public class RootAndMetadataController {

    @Inject
    private RootAndMetadataService metadataService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRootAndMetadata() throws Exception {
        Metadata metadata = metadataService.getRootAndMetadata();
        return Response.status(Response.Status.OK).entity(metadata.toString()).build();
    }
}
