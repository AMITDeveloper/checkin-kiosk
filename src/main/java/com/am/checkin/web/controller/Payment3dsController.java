package com.am.checkin.web.controller;

import com.aeromexico.commons.model.rq.Payment3dsRQ;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.security.PermitAll;
import javax.validation.Valid;
import javax.ws.rs.BeanParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URLEncoder;

@Path("")
public class Payment3dsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(Payment3dsController.class);

    private static final String UI_REDIRECT_URL = "http://localhost:4444";

    @POST
    @PermitAll
    @Path("/Payment/RedirectPayment")
    public Response payment(@Valid @BeanParam Payment3dsRQ request) {
        try {

            LOGGER.info("request: {}", request.toString());

            StringBuilder sb = new StringBuilder();

            sb.append(UI_REDIRECT_URL);
            sb.append("?status=").append(request.getStatus());
            sb.append("&ResultCode=").append(request.getResultCode());
            sb.append("&PaymentRef=").append(request.getPaymentRef());
            sb.append("&ResponseCode=").append(request.getResponseCode());
            sb.append("&ApprovalCode=").append(request.getApprovalCode());
            sb.append("&SupplierID=").append(request.getSupplierID());
            sb.append("&SupplierTransID=").append(request.getSupplierTransID());
            sb.append("&SupplierResponseCode=").append(request.getSupplierResponseCode());
            //sb.append("&AuthRemarks1=").append(request.getAuthRemarks1());
            //sb.append("&AuthRemarks2=").append(request.getAuthRemarks2());
            //sb.append("&CSC_Remarks=").append(request.getCSC_Remarks());
            //sb.append("&AVS_Remarks=").append(request.getAVS_Remarks());
            sb.append("&MerchantID=").append(request.getMerchantID());

            //String url = URLEncoder.encode(sb.toString(), "UTF-8");

            String url = sb.toString();

            LOGGER.info("url: {}", url);

            URI location = new URI(url);

            return Response.seeOther(location).build();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }

        return Response.ok().build();
    }
}
