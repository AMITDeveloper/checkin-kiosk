package com.am.checkin.web.controller;

import com.aeromexico.commons.model.SystemPropertiesFactory;
import com.aeromexico.commons.model.rq.PingRQ;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/ping")
public class PingSabreWSController {
	

	@Inject
    private SystemPropertiesFactory systemPropertiesFactory;

	private String hostName = null;
	private String pingResponse = null; 

	@PostConstruct
	public void init() {
		hostName = systemPropertiesFactory.getInstance().getHostname();
		pingResponse = "{\"_meta\":{\"class\":\"Ping\"},\"status\":\"AVAILABLE\",\"hostName\":\"" + hostName + "\"}";
	}

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPing(@BeanParam PingRQ pingRQ) throws Exception {

    	return Response.status(Response.Status.OK).entity( pingResponse ).build();

    	/*
        try {
            Ping pingObj = pingService.getPingStatus();

            logOnDatabaseService.logPingOnSuccess(pingObj, pingRQ);

            return Response.status(Response.Status.OK).entity(pingObj).build();
        } catch (Throwable ex) {
            logOnDatabaseService.logPingOnError(ex);

            LOG.error(ex.getMessage());
            throw ex;
        }
        */

    }

}
