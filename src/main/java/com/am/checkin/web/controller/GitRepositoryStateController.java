
package com.am.checkin.web.controller;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.aeromexico.commons.model.GitRepositoryState;
import com.am.checkin.web.service.GitRepositoryStateService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Path("/git")
public class GitRepositoryStateController {

	@Inject
	GitRepositoryStateService gitRepositoryStateService; 

	@GET
	@Path("/status")
	@Produces(MediaType.APPLICATION_JSON)
	public Response checkGitRevision() {
		GitRepositoryState gitRepositoryState = gitRepositoryStateService.getGitRepositoryState();
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return Response.status(Response.Status.OK).entity( gson.toJson(gitRepositoryState) ).build();
	}

}
