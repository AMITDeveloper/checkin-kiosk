package com.am.checkin.web.controller;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.EnhancedPNRCollection;
import com.aeromexico.commons.model.PNRCollection;
import com.aeromexico.commons.model.rq.PnrRQ;
import com.aeromexico.commons.myb.model.ManagePNRCollection;
import com.aeromexico.commons.web.types.BookedLegCheckinStatusType;
import com.aeromexico.commons.web.types.PosType;
import com.aeromexico.dao.util.CommonUtil;
import com.aeromexico.sabre.api.session.common.ServiceCall;
import com.aeromexico.sabre.api.session.exception.SabreLayerUnavailableException;
import com.aeromexico.slack.models.Attachments;
import com.aeromexico.slack.service.SlackService;
import com.am.checkin.web.service.CheckInSetUpConfigFactoryWrapper;
import com.am.checkin.web.service.LogOnDatabaseService;
import com.am.checkin.web.service.MultiplePNRService;
import com.am.checkin.web.service.PNRLookupService;
import com.am.checkin.web.util.AttachmentUtil;
import org.aeromexico.commons.exception.config.ReadProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.google.gson.Gson;
import com.aeromexico.commons.model.TraceLogDTO;
import com.am.checkin.web.util.TraceLogUtil;

@Path("/pnr")
public class PNRLookupController {

    private static final Logger LOG = LoggerFactory.getLogger(PNRLookupController.class);

    @Inject
    private PNRLookupService pnrLookupService;


    @Inject
    private LogOnDatabaseService logOnDatabaseService;

    @Inject
    private SlackService slackService;

    @Inject
    @Named("CheckInSetUpConfigFactoryWrapper")
    private CheckInSetUpConfigFactoryWrapper setUpConfigFactory;

    @PostConstruct
    public void init() {
        LOG.info("PNRLookupController initialized.");
    }

    @Inject
    private ReadProperties prop;

    private static final String CHECKIN_TEXT = "CHECKIN";
    private static final String CHECKIN_UNSUCCESSFUL = "UNSUCCESSFUL";
    private static final String CHECKIN_FORMAT_DATE = "yyyy-MM-dd HH:mm:ss";

    @GET
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPNR(@Valid @BeanParam PnrRQ pnrRQ, @HeaderParam("X-AM-User-Auth") String userAuth) throws Exception {
        List<PnrRQ> pnrRQList = null;
        TraceLogDTO traceLogDto = new TraceLogDTO();
        traceLogDto.setComponent("PNRLookupController");
        traceLogDto.setPnr(pnrRQ.getRecordLocator());

        pnrRQList = getValidatePnrList(pnrRQ, userAuth);

        pnrRQ.setUriStr(CommonUtil.getLink(pnrRQ.getUri()));

        try {
            List<ServiceCall> serviceCallList = new ArrayList<>();
            PNRCollection pnrCollection;

            if (null != pnrRQList) {
                MultiplePNRService multiplePNRService = new MultiplePNRService(pnrRQList, pnrLookupService);
                //Process the request by calling the pnrLookup service by multiple pnrs.
                pnrCollection = multiplePNRService.multiplePNRs();
            } else {
                //Process the request by calling the pnrLookup service.
                pnrCollection = pnrLookupService.lookUpReservation(pnrRQ, serviceCallList);
            }
            
            LOG.info("PNRLookupController.getPNR RES: " + pnrCollection);

            //Build the response object
            GenericEntity<PNRCollection> genericEntity = new GenericEntity<>(pnrCollection, PNRCollection.class);
            //Stop the timer
            pnrRQ.processed();

            try {
                pnrRQ.setCartId(pnrCollection.getCartId());
            } catch (Exception ex) {
                //Ingore exception
            }
            //Log this transaction into the database
            if(PosType.KIOSK.toString().equalsIgnoreCase(pnrRQ.getPos())){
                logOnDatabaseService.logPnrOnSuccess(pnrRQ);
            }
            LOG.info("PNRLookupController.getPNR COMPLETED REQ: " + pnrRQ);
            //Send back the response
            traceLogDto = TraceLogUtil.getTraceLog(pnrRQ, genericEntity, "pnr");
            traceLogDto.setStatusPNRLookUp("SUCCESSFUL");
            String traceLogJson =  new Gson().toJson(traceLogDto);
            LOG.info(CHECKIN_TEXT+"{} ", traceLogJson);
            return Response.status(Response.Status.OK).entity(genericEntity).build();
        } catch (SabreLayerUnavailableException ex) {
        	Date date = new Date();
        	DateFormat hourdateFormat = new SimpleDateFormat(CHECKIN_FORMAT_DATE);
        	traceLogDto.setCodeError(com.aeromexico.commons.web.util.Constants.CODE_12110301);
            traceLogDto.setEndPointName("pnr");
            traceLogDto.setBookingClass("NA");
            traceLogDto.setNumberFlight("NA");
            traceLogDto.setDestination("NA");
            traceLogDto.setOrigin("NA");
            traceLogDto.setPos(pnrRQ.getPos());
            traceLogDto.setStore(pnrRQ.getStore());
            traceLogDto.setDate("" + hourdateFormat.format(date).replaceAll("\\s","_"));
            traceLogDto.setStatusPNRLookUp(CHECKIN_UNSUCCESSFUL);
            traceLogDto.setMsgError(ex.getMessage().replaceAll("\\s","_"));
            traceLogDto.setPnr(pnrRQ.getRecordLocator());
            
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(pnrRQ.getPos())) {
            	LOG.error(CHECKIN_TEXT+"{} ",new Gson().toJson(traceLogDto));
                LOG.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_22110301,"PNRLookupController.java:127", ex.getMessage());
                throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getCode(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getActionable(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getMessage(),
                        Response.Status.INTERNAL_SERVER_ERROR);
            } else {
            	LOG.error(CHECKIN_TEXT+"{} ",new Gson().toJson(traceLogDto));
                LOG.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}", com.aeromexico.commons.web.util.Constants.CODE_12110301,"PNRLookupController.java:133", ex.getMessage());
                throw ex;
            }
        } catch (GenericException ex) {
        	Date date = new Date();
        	DateFormat hourdateFormat = new SimpleDateFormat(CHECKIN_FORMAT_DATE);
        	traceLogDto.setEndPointName("pnr");
        	traceLogDto.setBookingClass("NA");
        	traceLogDto.setNumberFlight("NA");
        	traceLogDto.setDestination("NA");
            traceLogDto.setOrigin("NA");
            traceLogDto.setPnr(pnrRQ.getRecordLocator());
            traceLogDto.setPos(pnrRQ.getPos());
            traceLogDto.setStore(pnrRQ.getStore());
            traceLogDto.setDate("" + hourdateFormat.format(date).replaceAll("\\s","_"));
            traceLogDto.setStatusPNRLookUp(CHECKIN_UNSUCCESSFUL);
            traceLogDto.setMsgError(ex.getMessage().replaceAll("\\s","_"));
            traceLogDto.setCodeError(ex.getErrorCodeType().toString());
            processException(pnrRQ, ex);
            LOG.error(CHECKIN_TEXT+"{} ",new Gson().toJson(traceLogDto));
            throw ex;
        } catch (Exception ex) {
        	Date date = new Date();
        	DateFormat hourdateFormat = new SimpleDateFormat(CHECKIN_FORMAT_DATE);
        	traceLogDto.setEndPointName("pnr");
        	traceLogDto.setBookingClass("NA");
        	traceLogDto.setNumberFlight("NA");
        	traceLogDto.setDestination("NA");
            traceLogDto.setOrigin("NA");
        	traceLogDto.setPnr(pnrRQ.getRecordLocator());
            traceLogDto.setPos(pnrRQ.getPos());
            traceLogDto.setStore(pnrRQ.getStore());
            traceLogDto.setDate("" + hourdateFormat.format(date).replaceAll("\\s","_"));
            traceLogDto.setStatusPNRLookUp(CHECKIN_UNSUCCESSFUL);
            traceLogDto.setMsgError(ex.getMessage().replaceAll("\\s","_"));
            processException(pnrRQ, ex);
            LOG.error(CHECKIN_TEXT+"{} ",new Gson().toJson(traceLogDto));
            throw ex;
        }
    }

    public List<PnrRQ> getValidatePnrList(PnrRQ pnrRQ, String userAuth) throws Exception{
        List<PnrRQ> pnrRQList = null;

        if (!PosType.KIOSK.toString().equalsIgnoreCase(pnrRQ.getPos())) {
            try {
            	if ( userAuth == null ) {
            		throw new Exception("userAuth can't be null.");
            	}
                if (userAuth.contains("|")) {
                    pnrRQList = splitPNRs(userAuth, pnrRQ);
                } else {
                    pnrRQ.setUserAuth(userAuth);
                }
            } catch (Exception ex) {
                LOG.error("PNRLookupController.getPNR error setting UserAuth", ex.getMessage());
                throw ex;
            }
        } else {
            pnrRQ.setUserAuth(userAuth);
        }
        return pnrRQList;
    }

    private void slack(long processed, PnrRQ pnrRQ) {
        slack(processed, pnrRQ, null);
    }

    private void slack(long processed, PnrRQ pnrRQ, Throwable ex) {
        try {
            if (ex != null) {
                Attachments attach = AttachmentUtil.getAttachmentException(pnrRQ.getRecordLocator(), pnrRQ.getUriStr(), ex.getMessage());
                slackService.sendMessageAttachment("Error alert API monitor: *PNRLookupService.getPNR*", attach);
            } else if (processed > (setUpConfigFactory.getSystemPropertiesFactory().getInstance().getTimeoutSabreTransaction() * 1000)) {
                Attachments attach = AttachmentUtil.getAttachment(pnrRQ.getRecordLocator(), pnrRQ.getUriStr(), setUpConfigFactory.getSystemPropertiesFactory().getInstance().getTimeoutSabreTransaction(), processed);
                slackService.sendMessageAttachment("Error alert API monitor: *PNRLookupService.getPNR*", attach);
            }
        } catch (Throwable exception) {

            LOG.error("PNRLookupController.getPNR failed to send message to Slack: {}", exception.getMessage());
        }
    }

    private void callDetails(long processed, PnrRQ pnrRQ, List<ServiceCall> serviceCallList) {
        try {
            int sabreTimeoutTransaction = setUpConfigFactory.getSystemPropertiesFactory().getInstance().getTimeoutSabreTransaction();
            if (processed > (sabreTimeoutTransaction * 1000)) {
                Attachments attach = AttachmentUtil.getAttachment(pnrRQ.getRecordLocator(), pnrRQ.getUriStr(), sabreTimeoutTransaction, processed);
                long totalTime = 0;
                for (ServiceCall serviceCall : serviceCallList) {
                    totalTime += serviceCall.getTime();
                }
                serviceCallList.add(new ServiceCall("Total", totalTime));
                slackService.sendMessageAttachment("Transaction took more than " + sabreTimeoutTransaction + " seconds. Time per service:\n" + serviceCallList.toString(), attach);

                LOG.info("Transaction took more than " + sabreTimeoutTransaction + " seconds. Time per service:\n" + serviceCallList.toString());
            }

        } catch (Throwable ex) {
            LOG.error("PNRLookupController.getPNR failed to send message to Slack: {}", ex);
        }
    }

    @GET
    @Path("/enhanced")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEnhancedPNR(@Valid @BeanParam PnrRQ pnrRQ, @HeaderParam("X-AM-User-Auth") String userAuth) throws Exception {
        EnhancedPNRCollection enhancedPNRCollection = new EnhancedPNRCollection();

        List<PnrRQ> pnrRQList = new ArrayList<>();
        try {
            if (userAuth.contains("|")) {
                pnrRQList = splitPNRs(userAuth, pnrRQ);
            } else {
                pnrRQ.setUserAuth(userAuth);
                pnrRQList.add(pnrRQ);
            }
            for (PnrRQ request : pnrRQList) {
                try {
                    ManagePNRCollection managePNRCollection = new ManagePNRCollection();
                    if (managePNRCollection.get_collection().get(0).getLegs().getCollection().get(0).getCheckinStatus().equals(BookedLegCheckinStatusType.OPEN)) {
                        PNRCollection checkinCollection = pnrLookupService.lookUpReservation(pnrRQ, new ArrayList<>());
                        enhancedPNRCollection.getCollection().add(checkinCollection.getCollection().get(0));
                    } else {
                        enhancedPNRCollection.getCollection().add(managePNRCollection.get_collection().get(0));
                    }
                } catch (Exception ignored) {
                }
            }
            return Response.status(Response.Status.OK).entity(enhancedPNRCollection).build();
        } catch (Exception ex) {
            processException(pnrRQ, ex);
            throw ex;
        }
    }

    private void processException(@Valid @BeanParam PnrRQ pnrRQ, Throwable ex) {
        pnrRQ.processed();
        if (ex.getMessage() != null && ex.getMessage().contains("code: 046")) {
            pnrRQ.setMessage(ex.getMessage());
            if(PosType.KIOSK.toString().equalsIgnoreCase(pnrRQ.getPos())){
                logOnDatabaseService.logPnrOnSuccess(pnrRQ);
            }
            LOG.info("PNRLookupController.getPNR COMPLETED REQ: " + pnrRQ);
        } else {
            if(PosType.KIOSK.toString().equalsIgnoreCase(pnrRQ.getPos())){
                logOnDatabaseService.logPnrOnSuccess(pnrRQ);
            }
            LOG.error("PNRLookupController.getPNR Exception: " + ex.getMessage(), ex);
        }
    }

    public ArrayList<PnrRQ> splitPNRs(String userAuth, PnrRQ pnrRQ) throws Exception {
        ArrayList<PnrRQ> pnrRQList = new ArrayList<>();
        for (String auth : userAuth.split("\\|")) {
            PnrRQ pnrRQSearch = new PnrRQ();
            pnrRQSearch.setPremiereUpgrade(pnrRQ.getPremiereUpgrade());
            pnrRQSearch.setVolunteerOffer(pnrRQ.isVolunteerOffer());
            pnrRQSearch.setStore(pnrRQ.getStore());
            pnrRQSearch.setPos(pnrRQ.getPos());
            pnrRQSearch.setUserAuth(auth);
            pnrRQSearch.setUriStr(CommonUtil.getLink(pnrRQ.getUri()));
            pnrRQList.add(pnrRQSearch);
        }
        return pnrRQList;
    }
}
