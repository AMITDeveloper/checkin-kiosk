package com.am.checkin.web.controller;

import javax.inject.Inject;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.rq.SignatureRQ;
import com.am.checkin.web.service.SignatureService;
import javax.validation.Valid;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/carts")
public class SignatureController {

    private static final Logger LOG = LoggerFactory.getLogger(SignatureController.class);

    @Inject
    SignatureService signatureService;

    @PUT
    @Path("/signature/{recordLocator}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response putShoppingCart(@Valid @BeanParam SignatureRQ signatureRQ) throws Exception {
        try {
            signatureRQ.getCartPNRUpdate();
            CartPNR cartPNR = signatureService.putSignature(signatureRQ);
            return Response.status(Response.Status.OK).entity(cartPNR).build();
        } catch (Throwable ex) {
            LOG.error(ex.getMessage());
            throw ex;
        }
    }
}
