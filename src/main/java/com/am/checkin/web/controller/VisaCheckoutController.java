package com.am.checkin.web.controller;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.aeromexico.commons.model.ExtraInfo;
import com.aeromexico.commons.model.Incident;
import com.aeromexico.commons.model.VisaCheckOutFormOfPayment;
import com.aeromexico.commons.web.types.ActionableType;
import com.aeromexico.sharedservices.services.VisaCheckOutCheckinService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeromexico.commons.model.rq.VisaCheckOutRQ;
import com.aeromexico.visacheckout.integration.service.PaymentFacade;
import com.aeromexico.webe4.web.common.types.ErrorCodeType;

/**
 * @author Carlos Luna
 *
 */
@Path("/am-visa-api")
public class VisaCheckoutController {

    private static final Logger logger = LoggerFactory.getLogger(VisaCheckoutController.class);

    @Inject
    private VisaCheckOutCheckinService visaCheckOutService;

    @GET
    @Path("/parameters")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getVisaParameter() throws Exception {
        try {
            VisaCheckOutFormOfPayment response = visaCheckOutService.getParametersService();
            return Response.status(Response.Status.OK).entity(response).build();
        } catch (Exception e) {
            ExtraInfo extraInfo = new ExtraInfo(e.getMessage());
            Incident incident = new Incident(ErrorCodeType.VISA_CHECKOUT_PARAMETERS_FAILED.getErrorCode(),
                    ActionableType.FIX,
                    ErrorCodeType.VISA_CHECKOUT_PARAMETERS_FAILED.getValue(), extraInfo);
            return Response.status(Response.Status.OK).entity(incident).build();
        }
    }

    @GET
    @Path("/payments/{callId}/resume")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getVisaPayment(@Valid @BeanParam VisaCheckOutRQ visaCheckOutRq) throws Exception {
        logger.info("Visa CheckOut Payment, information for cart: [{}]", visaCheckOutRq.getCartId());
        String data = null;
        try {
            data = PaymentFacade.getData(visaCheckOutRq.getCartId(), visaCheckOutRq.getCallId(),
                    visaCheckOutRq.getPromoCode());
        } catch (Exception ex) {
            ExtraInfo extraInfo = new ExtraInfo(ex.getMessage());
            Incident incident = new Incident(ErrorCodeType.VISA_CHECKOUT_PAYMENT_FAILED.getErrorCode(),
                    ActionableType.FIX,
                    ErrorCodeType.VISA_CHECKOUT_PAYMENT_FAILED.getValue(), extraInfo);
            return Response.status(Response.Status.OK).entity(incident).build();
        }
        return Response.status(Response.Status.OK).entity(data).build();
    }
}
