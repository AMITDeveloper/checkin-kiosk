package com.am.checkin.web.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.aeromexico.dao.factories.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeromexico.commons.web.util.Constants;

/**
 *
 * @author adrian
 */
@Named
@ApplicationScoped
public class ResetCacheService {

    private static final Logger LOG = LoggerFactory.getLogger(ResetCacheService.class);

    @Inject
    private PaymentMethodsWrapperFactory paymentMethodsWrapperFactory;

    @Inject
    private AirportWrapperFactory airportWrapperFactory;

    @Inject
    private AirportWeatherWrapperFactory airportWeatherWrapperFactory;

    @Inject
    private BaggageAllowanceWrapperFactory baggageAllowanceWrapperFactory;

    @Inject
    private FreeBaggageAllowanceWrapperFactory freeBaggageAllowanceWrapperFactory;

    @Inject
    private FarebasisWrapperFactory farebasisWrapperFactory;

    @Inject
    private OverBookingConfigWrapperFactory overBookingConfigWrapperFactory;

    @Inject
    private GroundHandlingWrapperFactory groundHandlingWrapperFactory;

    @Inject
    private KioskProfileWrapperFactory kioskProfileWrapperFactory;

    @Inject
    private PriorityCodesWrapperFactory priorityCodesWrapperFactory;
    
    @Inject
    private TierBenefitCobrandWrapperFactory tierBenefitCobrandWrapperFactory;
    
    @Inject
    private TierBenefitWrapperFactory tierBenefitWrapperFactory;
    
    @Inject
    private AncillaryEntryWrapperFactory ancillaryEntryWrapperFactory;
    
    @Inject
    private PccFxWrapperFactory pccFxWrapperFactory;

    @Inject
    private RestrictedCheckinWrapperFactory restrictedCheckinWrapperFactory;

    private Map<String, List<String>> collectionsCachedMap = new HashMap<>();

    @PostConstruct
    public void init() {
        List<String> collectionsCached = new ArrayList<>(1);
        collectionsCached.add(Constants.FORMS_OF_PAYMENTS_COLL);
        collectionsCached.add(Constants.AIRPORT_COLL);
        collectionsCached.add(Constants.AIRPORTS_WEATHER_COLL);
        collectionsCached.add(Constants.COMP_BAG_ALLOW_COLL);
        collectionsCached.add(Constants.FREE_BAG_ALLOWANCE_COLL);
        collectionsCached.add(Constants.FARE_BASIS_COLL);
        collectionsCached.add(Constants.OVER_BOOKING_CONFIG_COLL);
        collectionsCached.add(Constants.GROUND_HANDLING);
        collectionsCached.add(Constants.KIOSK_PROFILE_COLLECTION);
        collectionsCached.add(Constants.PRIORITY_CODES_COLL);
        collectionsCached.add(Constants.TIER_BENEFIT_COBRAND_COLL);
        collectionsCached.add(Constants.TIER_BENEFIT_COLL);
        collectionsCached.add(Constants.PCC_FX_COLL);
        collectionsCached.add(Constants.RESTRICTED_CHECKIN);


        collectionsCachedMap.put("Collections cached", collectionsCached);
    }

    public Map<String, List<String>> resetCollection(String collection) {

        Map<String, List<String>> collectionsResetedMap = new HashMap<>();

        List<String> collectionsReseted = new ArrayList<>(1);

        switch (collection) {
            case Constants.FORMS_OF_PAYMENTS_COLL:
                LOG.info("Cleaning payment methods cache");
                paymentMethodsWrapperFactory.reset();
                collectionsReseted.add(Constants.FORMS_OF_PAYMENTS_COLL);
                break;
            case Constants.AIRPORT_COLL:
                LOG.info("Cleaning airports cache");
                airportWrapperFactory.reset();
                collectionsReseted.add(Constants.AIRPORT_COLL);
                break;
            case Constants.AIRPORTS_WEATHER_COLL:
                LOG.info("Cleaning airports weather cache");
                airportWeatherWrapperFactory.reset();
                collectionsReseted.add(Constants.AIRPORTS_WEATHER_COLL);
                break;

            case Constants.COMP_BAG_ALLOW_COLL:
                LOG.info("Cleaning airports weather cache");
                baggageAllowanceWrapperFactory.reset();
                collectionsReseted.add(Constants.COMP_BAG_ALLOW_COLL);
                break;
            case Constants.FREE_BAG_ALLOWANCE_COLL:
                LOG.info("Cleaning airports weather cache");
                freeBaggageAllowanceWrapperFactory.reset();
                collectionsReseted.add(Constants.FREE_BAG_ALLOWANCE_COLL);
                break;
            case Constants.FARE_BASIS_COLL:
                LOG.info("Cleaning farebasis cache");
                farebasisWrapperFactory.reset();
                collectionsReseted.add(Constants.FARE_BASIS_COLL);
                break;
            case Constants.OVER_BOOKING_CONFIG_COLL:
                LOG.info("Cleaning overbooking config cache");
                overBookingConfigWrapperFactory.reset();
                collectionsReseted.add(Constants.OVER_BOOKING_CONFIG_COLL);
                break;
            case Constants.GROUND_HANDLING:
                LOG.info("Cleaning ground handling cache");
                groundHandlingWrapperFactory.reset();
                collectionsReseted.add(Constants.GROUND_HANDLING);
                break;
            case Constants.KIOSK_PROFILE_COLLECTION:
                LOG.info("Cleaning kiosk profile cache");
                kioskProfileWrapperFactory.reset();
                collectionsReseted.add(Constants.KIOSK_PROFILE_COLLECTION);
                break;
            case Constants.PRIORITY_CODES_COLL:
                LOG.info("Cleaning priority codes");
                priorityCodesWrapperFactory.reset();
                collectionsReseted.add(Constants.PRIORITY_CODES_COLL);
                break;
            case Constants.TIER_BENEFIT_COBRAND_COLL:
                LOG.info("Cleaning tier benefit cobrand code");
                tierBenefitCobrandWrapperFactory.reset();
                collectionsReseted.add(Constants.TIER_BENEFIT_COBRAND_COLL);
                break;
            case Constants.TIER_BENEFIT_COLL:
                LOG.info("Cleaning tier benefit");
                tierBenefitWrapperFactory.reset();
                collectionsReseted.add(Constants.TIER_BENEFIT_COLL);
                break;
            case Constants.ANCILLARY_ENTRIES_COLL:
                LOG.info("Cleaning ancillary entries");
                ancillaryEntryWrapperFactory.reset();
                collectionsReseted.add(Constants.ANCILLARY_ENTRIES_COLL);
                break;
            case Constants.PCC_FX_COLL:
                LOG.info("Cleaning farelogix pccs");
                pccFxWrapperFactory.reset();
                collectionsReseted.add(Constants.PCC_FX_COLL);
                break;
            case Constants.RESTRICTED_CHECKIN:
                LOG.info("Cleaning Restricted Checkin Airports Collection");
                restrictedCheckinWrapperFactory.reset();
                collectionsReseted.add(Constants.RESTRICTED_CHECKIN);
                break;
            default:
                collectionsReseted.add("Collection " + collection + " is not already cached");
                break;

        }

        collectionsResetedMap.put("Collections reseted", collectionsReseted);

        return collectionsResetedMap;

    }

    public Map<String, List<String>> resetCollections() {
        Map<String, List<String>> collectionsResetedMap = new HashMap<>();
        List<String> collectionsReseted = new ArrayList<>(3);

        LOG.info("Cleaning colletions cache");

        paymentMethodsWrapperFactory.reset();
        collectionsReseted.add(Constants.FORMS_OF_PAYMENTS_COLL);

        airportWrapperFactory.reset();
        collectionsReseted.add(Constants.AIRPORT_COLL);

        airportWeatherWrapperFactory.reset();
        collectionsReseted.add(Constants.AIRPORTS_WEATHER_COLL);

        baggageAllowanceWrapperFactory.reset();
        collectionsReseted.add(Constants.COMP_BAG_ALLOW_COLL);

        freeBaggageAllowanceWrapperFactory.reset();
        collectionsReseted.add(Constants.FREE_BAG_ALLOWANCE_COLL);

        farebasisWrapperFactory.reset();
        collectionsReseted.add(Constants.FARE_BASIS_COLL);

        overBookingConfigWrapperFactory.reset();
        collectionsReseted.add(Constants.OVER_BOOKING_CONFIG_COLL);

        groundHandlingWrapperFactory.reset();
        collectionsReseted.add(Constants.GROUND_HANDLING);

        kioskProfileWrapperFactory.reset();
        collectionsReseted.add(Constants.KIOSK_PROFILE_COLLECTION);

        priorityCodesWrapperFactory.reset();
        collectionsReseted.add(Constants.PRIORITY_CODES_COLL);
        
        tierBenefitCobrandWrapperFactory.reset();
        collectionsReseted.add(Constants.TIER_BENEFIT_COBRAND_COLL);
        
        tierBenefitWrapperFactory.reset();
        collectionsReseted.add(Constants.TIER_BENEFIT_COLL);
        
        ancillaryEntryWrapperFactory.reset();
        collectionsReseted.add(Constants.ANCILLARY_ENTRIES_COLL);
        
        pccFxWrapperFactory.reset();
        collectionsReseted.add(Constants.PCC_FX_COLL);

        restrictedCheckinWrapperFactory.reset();
        collectionsReseted.add(Constants.RESTRICTED_CHECKIN);

        collectionsResetedMap.put("Collections reseted", collectionsReseted);

        return collectionsResetedMap;
    }

    /**
     * @return the collectionsCachedMap
     */
    public Map<String, List<String>> getCollectionsCachedMap() {
        return collectionsCachedMap;
    }

    /**
     * @param collectionsCachedMap the collectionsCachedMap to set
     */
    public void setCollectionsCachedMap(Map<String, List<String>> collectionsCachedMap) {
        this.collectionsCachedMap = collectionsCachedMap;
    }
}
