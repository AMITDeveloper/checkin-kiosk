package com.am.checkin.web.service;

import com.aeromexico.cloudfiles.AMCloudFile;
import com.aeromexico.codes.impl.CodesGenerator;
import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.AbstractSegmentChoice;
import com.aeromexico.commons.model.BoardingPass;
import com.aeromexico.commons.model.BoardingPassCollection;
import com.aeromexico.commons.model.BoardingPassKiosk;
import com.aeromexico.commons.model.BoardingPassResponse;
import com.aeromexico.commons.model.BoardingPassWeb;
import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedLegCollection;
import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.BookedTravelerBenefit;
import com.aeromexico.commons.model.BookingClass;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.FreeBaggageAllowanceDetailByCard;
import com.aeromexico.commons.model.Infant;
import com.aeromexico.commons.model.PNR;
import com.aeromexico.commons.model.PNRCollection;
import com.aeromexico.commons.model.PectabBoardingPass;
import com.aeromexico.commons.model.PectabBoardingPassList;
import com.aeromexico.commons.model.Seat;
import com.aeromexico.commons.model.SeatChoice;
import com.aeromexico.commons.model.SeatChoiceFareUpgrade;
import com.aeromexico.commons.model.SeatChoiceFareUpsell;
import com.aeromexico.commons.model.SeatChoiceUpgrade;
import com.aeromexico.commons.model.SeatChoiceUpsell;
import com.aeromexico.commons.model.SeatChoiceWaivedUpgrade;
import com.aeromexico.commons.model.Segment;
import com.aeromexico.commons.model.SegmentDocument;
import com.aeromexico.commons.model.SegmentDocumentList;
import com.aeromexico.commons.model.SegmentStatus;
import com.aeromexico.commons.model.WarningCollection;
import com.aeromexico.commons.model.rq.BoardingPassesRQ;
import com.aeromexico.commons.model.weather.AirportWeather;
import com.aeromexico.commons.model.weather.DailyForecasts;
import com.aeromexico.commons.web.types.AirlineCodeType;
import com.aeromexico.commons.web.types.DigitalSignatureStatusType;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.types.PassengerFareType;
import com.aeromexico.commons.web.types.PosType;
import com.aeromexico.commons.web.types.SeatSectionCodeType;
import com.aeromexico.commons.web.util.FilterPassengersUtil;
import com.aeromexico.commons.web.util.SegmentCodeUtil;
import com.aeromexico.dao.client.exception.MongoDisconnectException;
import com.aeromexico.dao.config.SetUpConfigFactory;
import com.aeromexico.dao.qualifiers.AirportsCache;
import com.aeromexico.dao.qualifiers.OverBookingConfigCache;
import com.aeromexico.dao.services.IAirportCodesDao;
import com.aeromexico.dao.services.IOverBookingConfigDao;
import com.aeromexico.dao.services.IPNRLookupDao;
import com.aeromexico.sabre.api.acs.AMReprintBPService;
import com.aeromexico.sabre.api.models.dcci.*;
import com.aeromexico.sabre.api.restclient.DigitalSignatureSabreClient;
import com.aeromexico.timezone.service.TimeZoneService;
import com.aeromexico.weather.service.WeatherService;
import com.am.checkin.web.event.dispatcher.BoardingPassesDispatcher;
import com.am.checkin.web.util.BoardingPassUtil;
import com.am.checkin.web.util.PnrCollectionUtil;
import com.am.checkin.web.util.PurchaseOrderUtil;
import com.am.checkin.web.v2.util.SystemVariablesUtil;
import com.google.gson.Gson;
import com.sabre.services.acs.bso.checkinpassenger.v3.ACSCheckInPassengerRSACS;
import com.sabre.services.acs.bso.reprintbp.v3.ACSReprintBPRSACS;
import com.sabre.services.acs.bso.reprintbp.v3.ItineraryACS;
import com.sabre.services.acs.bso.reprintbp.v3.PassengerInfoACS;
import com.sabre.services.stl.v3.PECTABDataListACS;
import com.sabre.services.stl.v3.PrintFormatACS;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;

import org.aeromexico.commons.exception.config.ReadProperties;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named
@ApplicationScoped
public class BoardingPassesService {

    private static final Logger LOG = LoggerFactory.getLogger(BoardingPassesService.class);
    public static final int EMPLOYEE_INDEX = 1;

    public static List<String> USA_AIRPORTS;

    /**
     * Rest Client para Sabre services
     */
    @Inject
    private DigitalSignatureSabreClient digitalSignatureSabreClient;

    /**
     * Dao for access to database
     */
    @Inject
    private IPNRLookupDao pnrLookupDao;

    /**
     * Dao for get city name for apple-wallet
     */
    @Inject
    @AirportsCache
    private IAirportCodesDao airportCodesDao;

    @Inject
    @OverBookingConfigCache
    private IOverBookingConfigDao overBookingConfigDao;

    /**
     * Service for do a sabreAPI call
     */
    @Inject
    private AMReprintBPService reprintService;

    @Inject
    private PassengersListService volunteeredListService;

    /**
     * Dispatcher to email service for boarding pass
     */
    @Inject
    private BoardingPassesDispatcher mailDispatcher;

    /**
     * Service for update collection on database
     */
    @Inject
    private PnrCollectionService updatePnrCollectionService;

    @Inject
    @Named("CheckInSetUpConfigFactoryWrapper")
    private SetUpConfigFactory setUpConfigFactory;

    @Inject
    private MobileBoardingPassService mobileBoardingPassService;

    @Inject
    private ReadProperties prop;

    @PostConstruct
    public void init() {
        LOG.info("BoardingPassesService initialized.");
    }

    public PNRCollection getPnrCollection(String recordLocator, String pos) {
        PNRCollection pnrCollection = findInMongoPnrCollection(recordLocator, pos);
        if (pnrCollection == null) {
            throw new GenericException(Response.Status.NOT_FOUND, ErrorType.EXPIRED_SESSION);
        }
        return pnrCollection;
    }

    /**
     * Main service for boarding pass
     *
     * @param boardingPassesRQ
     * @return
     * @throws Exception
     */
    public BoardingPassCollection getBoardingPasses(BoardingPassesRQ boardingPassesRQ) throws Exception {

        BoardingPassCollection boardingPassCollection = null;
        PNRCollection pnrCollection = getPnrCollection(boardingPassesRQ.getRecordLocator(), boardingPassesRQ.getPos());

        if (null != pnrCollection.getStore() && !pnrCollection.getStore().trim().isEmpty()) {
            boardingPassesRQ.setStore(pnrCollection.getStore());
        }

        if (PosType.KIOSK.toString().equalsIgnoreCase(boardingPassesRQ.getPos())) {
            boardingPassCollection = getKioskBoardingPasses(boardingPassesRQ, pnrCollection);
        } else {
            BoardingPassResponse boardingPassResponse = getWebBoardingPasses(boardingPassesRQ, pnrCollection);

            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(boardingPassesRQ.getPos())) {
                List<byte[]> imageFiles = mobileBoardingPassService.getMobileBoardingPass(boardingPassResponse.getPnrCollection());
                for (int i = 0; i < boardingPassResponse.getBoardingPassCollection().getCollection().size(); i++) {
                    if (null != imageFiles && imageFiles.size() > i) {
                        String imageBase64 = new CodesGenerator().convertImageToBase64(imageFiles.get(i));
                        BoardingPass boardingPass = boardingPassResponse.getBoardingPassCollection().getCollection().get(i);
                        if (null != boardingPass) {
                            boardingPass.setMobileBoardingPass(imageBase64);
                        }
                    }
                }
            }

            boardingPassCollection = boardingPassResponse.getBoardingPassCollection();
        }

        sanatizeData(boardingPassCollection);

        return boardingPassCollection;
    }

    private void sanatizeData(BoardingPassCollection boardingPassCollection) {

        if (boardingPassCollection == null) {
            return;
        }

        for (BoardingPass boardingPass : boardingPassCollection.getCollection()) {

            if (boardingPass != null && boardingPass.getPectabFormat() != null) {
                LOG.info("Re-print TravelDocData BEFORE: " + boardingPass.getPectabFormat());
                boardingPass.setPectabFormat(boardingPass.getPectabFormat().replaceAll("\u0026lt;", ">"));
                LOG.info("Re-print TravelDocData AFTER: " + boardingPass.getPectabFormat());
            }
        }

    }

    /**
     * @param boardingPassesRQ
     * @return
     * @throws Exception Sample URL service call from a Kiosk:
     * <p>
     *                   /checkIn/boarding-passes/RLMZJU?store=mx&pos=kiosk&language=
     * ES&legCode=MEX_AM_0301_2016-07-15&paxId=F35D69AD0001, F35D69AD0002
     */
    private BoardingPassCollection getKioskBoardingPasses(
            BoardingPassesRQ boardingPassesRQ,
            PNRCollection pnrCollection
    ) throws Exception {

        PNR pnr = pnrCollection.getPNRByRecordLocator(boardingPassesRQ.getRecordLocator());
        if (pnr == null) {
            // return, don't continue..
            LOG.error("Invalid PNR value in the request, please verify: {}", boardingPassesRQ);
            throw new GenericException(Response.Status.NOT_FOUND, ErrorType.PNR_NOT_FOUND);
        }

        if (pnr.isTsaRequired() && !boardingPassesRQ.isOverrideTsa()) {
            LOG.error("Tsa validation required");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.TSA_VALIDATION_REQUIRED);
        }

        if (pnr.getCarts() == null
                || pnr.getCarts().getCollection() == null
                || pnr.getCarts().getCollection().isEmpty()) {
            // return, don't continue..
            LOG.error("Invalid POS value in the request: {}", boardingPassesRQ);
            throw new GenericException(Response.Status.NOT_FOUND, ErrorType.CART_COLLECTION_OBJECT_EMPTY);
        }

        LOG.info("getKioskBoardingPasses PNR: {}", pnr);

        if (boardingPassesRQ.getLegCode() == null || boardingPassesRQ.getLegCode().trim().isEmpty()) {
            LOG.error("The legCode is required, request seupdate nt: {}", boardingPassesRQ);
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.LEGCODE_REQUIRED_FOR_KIOSK);
        }

        BookedLeg bookedLeg = pnr.getBookedLegByLegCode(boardingPassesRQ.getLegCode().trim());
        if (bookedLeg == null) {
            LOG.error("Cannot find the booked leg with the specified legcode, {}", boardingPassesRQ.getLegCode());
            throw new GenericException(Response.Status.NOT_FOUND, ErrorType.LEGCODE_NOT_FOUND_IN_THE_PNR);
        }

        CartPNR cartPNR = pnr.getCartPNRByLegCode(boardingPassesRQ.getLegCode().trim());
        if (boardingPassesRQ.getLegCode() != null && !boardingPassesRQ.getLegCode().trim().isEmpty()) {
            if (cartPNR == null) {
                LOG.error("Cannot find the cart with the specified legcode, {}", boardingPassesRQ);
                throw new GenericException(Response.Status.NOT_FOUND,
                        ErrorType.CART_NOT_FOUND_IN_COLLECTION_WITH_LEGCODE);
            }
        }

        // PAX ID is a required field for requests initiated from the Kiosk
        if (!validatePassengerIds(boardingPassesRQ, cartPNR)) {

            getReprintInformation(
                    pnr, boardingPassesRQ, cartPNR,
                    pnrCollection.getWarnings()
            );

            updatePnrCollectionService.createOrUpdatePnrCollectionAfterCheckin(
                    pnrCollection,
                    boardingPassesRQ.getRecordLocator(),
                    cartPNR.getMeta().getCartId(), true
            );
        }

        BoardingPassCollection boardingPassCollection = getKioskBoardingSelected(
                boardingPassesRQ, bookedLeg, cartPNR, pnr
        );

        boolean volunteerQualifier = false;

        try {
            LOG.info("Calling overbooking service");

            boolean atLeastOne = false;

            for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
                if (!bookedTraveler.isCheckinStatus()) {
                    bookedTraveler.setIsOverBookingEligible(false);
                }

                if (!atLeastOne && bookedTraveler.isIsOverBookingEligible()) {
                    atLeastOne = true;
                }

                BoardingPassKiosk boardingPass = boardingPassCollection.getBoardingPassByNameref(
                        bookedTraveler.getNameRefNumber()
                );

                if (null == boardingPass) {
                    boardingPass = new BoardingPassKiosk();
                    boardingPass.setTicketNumber(bookedTraveler.getTicketNumber());

                    if (null != bookedTraveler.getBookingClasses().getCollection()
                            && bookedTraveler.getBookingClasses().getCollection().size() > 0) {
                        boardingPass.setClassOfService(
                                bookedTraveler.getBookingClasses().getCollection().get(0).getBookingClass());
                    }

                    boardingPass.setFirstName(bookedTraveler.getFirstName());
                    boardingPass.setLastName(bookedTraveler.getLastName());
                    boardingPass.setNameRefNumber(bookedTraveler.getNameRefNumber());
                    boardingPass.setIsOverBookingEligible(bookedTraveler.isIsOverBookingEligible());
                    boardingPass.setIsSelectedToCheckin(false);

                    boardingPassCollection.getCollection().add(boardingPass);
                } else {
                    boardingPass.setIsOverBookingEligible(bookedTraveler.isIsOverBookingEligible());
                    boardingPass.setIsSelectedToCheckin(true);
                }
            }

            if (atLeastOne) {

                BookedSegment bookedSegment = bookedLeg.getSegments().getFirstSegmentOperatedBy(AirlineCodeType.AM);

                int volunteeres = volunteeredListService.getVolunteeredCount(
                        bookedSegment.getSegment().getOperatingCarrier(),
                        bookedSegment.getSegment().getOperatingFlightCode(),
                        bookedSegment.getSegment().getDepartureAirport(),
                        bookedSegment.getSegment().getDepartureDateTime()
                );

                int maxVolunteers = overBookingConfigDao.getMaxVolunteers(
                        bookedSegment.getSegment().getDepartureAirport(),
                        bookedSegment.getSegment().getOperatingFlightCode(),
                        bookedSegment.getSegment().getDepartureDateTime()
                );

                if (volunteeres < maxVolunteers) {
                    volunteerQualifier = true;
                } else {
                    if (PosType.KIOSK.toString().equalsIgnoreCase(boardingPassesRQ.getPos())) {
                        Iterator<BoardingPass> i = boardingPassCollection.getCollection().iterator();
                        int index = 0;
                        while (i.hasNext()) {
                            BoardingPass boardingPass = i.next();
                            if (boardingPass instanceof BoardingPassKiosk) {
                                BoardingPassKiosk boardingPassKiosk = (BoardingPassKiosk) boardingPass;
                                boardingPassKiosk.setIsOverBookingEligible(false);

                                boardingPassCollection.getCollection().set(index, boardingPassKiosk);
                            }
                            index++;
                        }

                        for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
                            bookedTraveler.setIsOverBookingEligible(false);
                        }
                    }
                }
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }

        boardingPassCollection.setMustOfferOverbooking(volunteerQualifier);
        return boardingPassCollection;
    }

    /**
     * This method allow to get information from selected boarding passes
     *
     * @param boardingPassesRQ
     * @param bookedLeg
     * @param cartPNR
     * @param pnr
     * @return
     */
    protected BoardingPassCollection getKioskBoardingSelected(
            BoardingPassesRQ boardingPassesRQ,
            BookedLeg bookedLeg,
            CartPNR cartPNR,
            PNR pnr
    ) {

        BoardingPassCollection boardingPassCollection = new BoardingPassCollection();

        LOG.info("Getting information for each passenger in the request: {}", boardingPassesRQ.getPaxId());

        for (String passengerId : boardingPassesRQ.getPaxId()) {

            BookedTraveler bookedTraveler;
            try {
                bookedTraveler = cartPNR.getBookedTravelerById(passengerId);
            } catch (Exception e) {
                LOG.error("Error to find the booked traveler does not match id: {}", passengerId);
                throw new GenericException(Response.Status.NOT_FOUND, ErrorType.TRAVELER_ID_NOT_MATCH);
            }

            try {
                List<BoardingPassKiosk> boardingPassFill = fillDataBoardingPassForKiosks(
                        bookedTraveler, boardingPassesRQ.getPos(), bookedLeg, pnr
                );

                if (null != boardingPassFill) {
                    //Reviewed  until the new mailing api is active
                    String legCode;
                    try {
                        legCode = getLegCodeFromDocumentList(bookedTraveler);
                    } catch (Exception e) {
                        legCode = bookedLeg.getSegments().getLegCodeRackspace();
                    }

                    String departureDate = bookedLeg.getSegments().getCollection().get(0).getSegment()
                            .getDepartureDateTime().substring(0, 10).replace("-", "_");

                    String link = createDownloadLinkRackspace(
                            boardingPassesRQ.getRecordLocator(),
                            departureDate,
                            legCode,
                            bookedTraveler.getFirstName().substring(0, 1) + "_" + bookedTraveler.getLastName() + "_" + bookedTraveler.getTicketNumber()
                    );
                    boardingPassCollection.addItineraryPdf(
                            bookedTraveler.getTicketNumber(),
                            link
                    );
                    boardingPassCollection.getCollection().addAll(boardingPassFill);
                }
            } catch (Exception ex) {
                LOG.error("Error with pnr collection information read from database: {}", cartPNR.toString());
                throw new GenericException(Response.Status.NOT_FOUND, ErrorType.ERROR_INFORMATION_ON_COLLECTION);
            }
        }

        LOG.info("Return {} boarding passes in the collection", boardingPassCollection.getCollection().size());

        if (boardingPassCollection.getCollection() == null || boardingPassCollection.getCollection().isEmpty()) {
            LOG.error("Error to generate boarding passes collection empty");
            if(PosType.WEB.equals(boardingPassesRQ.getPos())){
                throw new GenericException(Response.Status.NOT_FOUND, ErrorType.BOARDING_PASSES_NOT_GENERATED_WEB);
            }else{
                throw new GenericException(Response.Status.NOT_FOUND, ErrorType.BOARDING_PASSES_NOT_GENERATED);
            }
        }

        PectabBoardingPassList smallPectabList = new PectabBoardingPassList();

        if (null != cartPNR.getPectabBoardingPassList() && !cartPNR.getPectabBoardingPassList().isEmpty()) {
            PectabBoardingPass pectabBoardingPass = cartPNR.getPectabBoardingPassList().get(0);
            smallPectabList.add(pectabBoardingPass);
        }

        boardingPassCollection.setPectabBoardingPassList(smallPectabList);
        boardingPassCollection.setPnr(boardingPassesRQ.getRecordLocator());

        String iataCity = bookedLeg.getSegments().getCollection().get(0).getSegment().getArrivalAirport();
        DailyForecasts wheatherCityDest = getWeatherForService(
                iataCity, boardingPassesRQ.getPos(),
                boardingPassesRQ.getLanguage()
        );

        boardingPassCollection.setDailyForecasts(wheatherCityDest);
        boardingPassCollection.setPos(boardingPassesRQ.getPos());

        //Reviewed  until the new mailing api is active
        //If there is just one pax the link for all is the same
        if (cartPNR.getTravelerInfo().getCollection().size() == 1) {
            boardingPassCollection.addItineraryPdf(
                    "ALL",
                    boardingPassCollection.getItineraryPdf().get(
                            cartPNR.getTravelerInfo()
                                    .getCollection()
                                    .get(0)
                                    .getTicketNumber()
                    )
            );
        } else {
            String legCode;
            try {
                legCode = boardingPassCollection
                        .getItineraryPdf()
                        .get(
                                cartPNR.getTravelerInfo()
                                        .getCollection()
                                        .get(0)
                                        .getTicketNumber()
                        ).split("_")[5] + "_";
            } catch (Exception e) {
                legCode = bookedLeg.getSegments().getLegCodeRackspace();
            }

            String departureDate = bookedLeg.getSegments().getCollection().get(0).getSegment()
                    .getDepartureDateTime().substring(0, 10).replace("-", "_");

            String link = createDownloadLinkRackspace(
                    boardingPassesRQ.getRecordLocator(),
                    departureDate,
                    legCode,
                    "ALL"
            );
            boardingPassCollection.addItineraryPdf(
                    "ALL",
                    link
            );
        }

        return boardingPassCollection;
    }

    /**
     * This method allow to fill infant information from each booked traveler
     *
     * @param bookedTraveler
     * @param segmentCode
     * @return
     * @throws Exception
     */
    private BoardingPassWeb fillDataInfantForWeb(BookedTraveler bookedTraveler, String segmentCode) {
        LOG.info("Fill information of infant with booked traveler: {}", bookedTraveler);
        BoardingPassWeb boardingPassWeb = new BoardingPassWeb();

        boardingPassWeb.setBarcodeImage(bookedTraveler.getInfant().getSegmentDocumentsList().getBySegmentCode(segmentCode).getBarcodeImage());
        boardingPassWeb.setQrCodeImage(bookedTraveler.getInfant().getSegmentDocumentsList().getBySegmentCode(segmentCode).getQrcodeImage());
        boardingPassWeb.setTSAPre(bookedTraveler.getInfant().getSegmentDocumentsList().getBySegmentCode(segmentCode).isTsaPreCheck());
        boardingPassWeb.setSkyPriority(bookedTraveler.getInfant().getSegmentDocumentsList().getBySegmentCode(segmentCode).isSkyPriority());

        boardingPassWeb.setClassOfService(bookedTraveler.getBookingClasses().getBookingClass(segmentCode).getBookingClass());
        boardingPassWeb.setTicketNumber(bookedTraveler.getInfant().getTicketNumber());
        boardingPassWeb.setFirstName(bookedTraveler.getInfant().getFirstName());
        boardingPassWeb.setLastName(bookedTraveler.getInfant().getLastName());
        boardingPassWeb.setSelectee(bookedTraveler.isSelectee());
        boardingPassWeb.setFrequentFlyerAirline(bookedTraveler.getFrequentFlyerProgram());
        boardingPassWeb.setFrequentFlyerNumber(bookedTraveler.getFrequentFlyerNumber());
        boardingPassWeb.setControlCode(bookedTraveler.getSegmentDocumentsList().getBySegmentCode(segmentCode).getCheckInNumber());
        boardingPassWeb.setZone(bookedTraveler.getSegmentDocumentsList().getBySegmentCode(segmentCode).getBoardingZone());

        LOG.info("Get assigned seat and validating information for segment: {}", segmentCode);
        Seat seat = getSeatFromCart(bookedTraveler.getSegmentChoices().getCollection(), segmentCode);
        if (isFirstClassCabin(bookedTraveler.getBookingClasses().getCollection().get(0).getBookingCabin())) {
            seat.setSectionCode(SeatSectionCodeType.FIRST_CLASS);
        } else {
            seat.setSectionCode(SeatSectionCodeType.COACH);
        }
        seat.setSeatCode("INF");
        boardingPassWeb.setSeat(seat);
        return boardingPassWeb;
    }

    /**
     * This method allow to fill infant information from each booked traveler
     *
     * @param bookedTraveler
     * @param segmentCode
     * @return
     * @throws Exception
     */
    private BoardingPassKiosk fillDataInfantForKiosks(BookedTraveler bookedTraveler, String segmentCode) {
        LOG.info("Fill information of infant with booked traveler: {}", bookedTraveler);

        BoardingPassKiosk boardingPassKiosk;
        boardingPassKiosk = new BoardingPassKiosk();
        boardingPassKiosk.setBarcode(bookedTraveler.getInfant().getSegmentDocumentsList().getBySegmentCode(segmentCode).getBarcode());
        boardingPassKiosk.setPectabFormat(bookedTraveler.getInfant().getSegmentDocumentsList().getBySegmentCode(segmentCode).getTravelDoc());

        boardingPassKiosk.setClassOfService(bookedTraveler.getBookingClasses().getBookingClass(segmentCode).getBookingClass());
        boardingPassKiosk.setTicketNumber(bookedTraveler.getInfant().getTicketNumber());
        boardingPassKiosk.setFirstName(bookedTraveler.getInfant().getFirstName());
        boardingPassKiosk.setLastName(bookedTraveler.getInfant().getLastName());
        boardingPassKiosk.setSelectee(bookedTraveler.isSelectee());
        boardingPassKiosk.setControlCode(bookedTraveler.getSegmentDocumentsList().getBySegmentCode(segmentCode).getCheckInNumber());
        boardingPassKiosk.setZone(bookedTraveler.getSegmentDocumentsList().getBySegmentCode(segmentCode).getBoardingZone());

        LOG.info("Get assigned seat and validating information for segment: {}", segmentCode);
        List<AbstractSegmentChoice> collectionSegment = bookedTraveler.getSegmentChoices().getCollection();
        Seat seat = getSeatFromCart(collectionSegment, segmentCode);
        if (isFirstClassCabin(bookedTraveler.getBookingClasses().getCollection().get(0).getBookingCabin())) {
            seat.setSectionCode(SeatSectionCodeType.FIRST_CLASS);
        } else {
            seat.setSectionCode(SeatSectionCodeType.COACH);
        }
        seat.setSeatCode("INF");
        boardingPassKiosk.setSeat(seat);
        return boardingPassKiosk;
    }

    /**
     * This method allow fill the information in the boarding pass object
     *
     * @param bookedTraveler
     * @param pos
     * @param bookedLeg
     * @param pnr
     * @return
     * @throws Exception
     */
    protected List<BoardingPassKiosk> fillDataBoardingPassForKiosks(
            BookedTraveler bookedTraveler,
            String pos,
            BookedLeg bookedLeg,
            PNR pnr
    ) throws Exception {

        LOG.info("Fill information with booked traveler: {}", bookedTraveler.toString());

        List<BoardingPassKiosk> boardingList = new ArrayList<>();

        BoardingPassKiosk boardingPassKiosk;

        for (SegmentDocument segmentDocument : bookedTraveler.getSegmentDocumentsList()) {

            String segmentCode = segmentDocument.getSegmentCode();

            boardingPassKiosk = new BoardingPassKiosk();
            boardingPassKiosk.setBarcode(segmentDocument.getBarcode());
            boardingPassKiosk.setPectabFormat(segmentDocument.getTravelDoc());
            boardingPassKiosk.setNameRefNumber(bookedTraveler.getNameRefNumber());

            boardingPassKiosk.setControlCode(segmentDocument.getCheckInNumber());
            boardingPassKiosk.setZone(segmentDocument.getBoardingZone());

            if (null != bookedTraveler.getBookingClasses().getCollection()
                    && !bookedTraveler.getBookingClasses().getCollection().isEmpty()) {
                boardingPassKiosk.setClassOfService(
                        bookedTraveler.getBookingClasses().getBookingClass(segmentCode).getBookingClass()
                );
            }

            boardingPassKiosk.setTicketNumber(bookedTraveler.getTicketNumber());
            boardingPassKiosk.setPassengerId(bookedTraveler.getId());
            boardingPassKiosk.setFirstName(bookedTraveler.getFirstName());
            boardingPassKiosk.setLastName(bookedTraveler.getLastName());
            boardingPassKiosk.setSelectee(bookedTraveler.isSelectee());

            List<AbstractSegmentChoice> collectionSegment = bookedTraveler.getSegmentChoices().getCollection();

            Seat seat = getSeatFromCart(collectionSegment, segmentCode);

            if (isFirstClassCabin(bookedTraveler.getBookingClasses().getCollection().get(0).getBookingCabin())) {
                seat.setSectionCode(SeatSectionCodeType.FIRST_CLASS);
            } else {
                seat.setSectionCode(SeatSectionCodeType.COACH);
            }

            boardingPassKiosk.setSeat(seat);
            SegmentStatus segmentStatus = null;

            try {
                segmentStatus = getSegmetStatus(bookedLeg, segmentCode);
            } catch (Exception e) {
                LOG.error(e.getMessage(), e);
                LOG.error("Cannot get segment {} for the boarding passes", segmentCode, e);
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.CANNOT_GET_SEGMENT_STATUS_INFORMATION);
            }

            boardingPassKiosk.setSegmentStatus(segmentStatus);

            if (PosType.WEB.toString().equalsIgnoreCase(pos) || PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(pos)) {

                LOG.info("Adding all parameters for url");

                String appleWallet = calculateAppleWallet(
                        segmentStatus,
                        segmentDocument.getBarcode(),
                        segmentDocument.getBoardingZone(),
                        bookedTraveler.getFirstName(),
                        bookedTraveler.getLastName(),
                        seat.getSeatCode(),
                        seat.getSectionCode(),
                        segmentDocument.isSkyPriority(),
                        segmentDocument.isTsaPreCheck(),
                        pos,
                        pnr,
                        bookedTraveler.getTicketNumber(),
                        bookedTraveler.getFrequentFlyerProgram(),
                        bookedTraveler.getFrequentFlyerNumber()
                );

                boardingPassKiosk.setAppleWalletPass(appleWallet);
            }

            boardingList.add(boardingPassKiosk);

            if (null != bookedTraveler.getInfant()) {
                try {
                    BoardingPassKiosk boardingInfant = fillDataInfantForKiosks(bookedTraveler, segmentCode);
                    if (null != boardingInfant) {
                        boardingInfant.setSegmentStatus(segmentStatus);
                        boardingList.add(boardingInfant);
                    }
                } catch (Exception e) {
                    LOG.error("Cannot get the information for infant: {}", bookedTraveler.getInfant());
                    throw new GenericException(Response.Status.NOT_FOUND, ErrorType.CANNOT_GET_INFANT_INFORMATION);
                }
            }
        }

        return boardingList;
    }

    /**
     * This method allow fill the information in the boarding pass object
     *
     * @param bookedTraveler
     * @param pos
     * @param bookedLeg
     * @param pnr
     * @return
     * @throws Exception
     */
    protected List<BoardingPassWeb> fillDataBoardingPassForWeb(
            BookedTraveler bookedTraveler,
            String pos,
            BookedLeg bookedLeg,
            PNR pnr
    ) throws Exception {

        LOG.info("Fill information with booked traveler: {}", bookedTraveler.toString());

        List<BoardingPassWeb> boardingList = new ArrayList<>();

        BoardingPassWeb boardingPassWeb;

        for (SegmentDocument segmentDocument : bookedTraveler.getSegmentDocumentsList()) {

            String segmentCode = segmentDocument.getSegmentCode();

            boardingPassWeb = new BoardingPassWeb();
            if (!bookedTraveler.isSelectee()) {
                boardingPassWeb.setBarcodeImage(segmentDocument.getBarcodeImage());
                boardingPassWeb.setQrCodeImage(segmentDocument.getQrcodeImage());
            }

            boardingPassWeb.setTSAPre(segmentDocument.isTsaPreCheck());
            boardingPassWeb.setSkyPriority(segmentDocument.isSkyPriority());

            boardingPassWeb.setControlCode(segmentDocument.getCheckInNumber());
            boardingPassWeb.setZone(segmentDocument.getBoardingZone());

            if (null != bookedTraveler.getBookingClasses().getCollection()
                    && !bookedTraveler.getBookingClasses().getCollection().isEmpty()) {

                BookingClass bookingClass = bookedTraveler.getBookingClasses().getBookingClass(segmentCode);

                if (null != bookingClass) {
                    boardingPassWeb.setClassOfService(bookingClass.getBookingClass());
                } else {
                    boardingPassWeb.setClassOfService(
                            bookedTraveler.getBookingClasses().getCollection().get(0).getBookingClass()
                    );
                }
            }

            boardingPassWeb.setTicketNumber(bookedTraveler.getTicketNumber());
            boardingPassWeb.setNameRefNumber(bookedTraveler.getNameRefNumber());
            boardingPassWeb.setFirstName(bookedTraveler.getFirstName());
            boardingPassWeb.setLastName(bookedTraveler.getLastName());
            boardingPassWeb.setSelectee(bookedTraveler.isSelectee());
            boardingPassWeb.setFrequentFlyerNumber(bookedTraveler.getFrequentFlyerNumber());
            boardingPassWeb.setFrequentFlyerAirline(bookedTraveler.getFrequentFlyerProgram());

            List<AbstractSegmentChoice> collectionSegment = bookedTraveler.getSegmentChoices().getCollection();

            Seat seat = getSeatFromCart(collectionSegment, segmentCode);
            if (isFirstClassCabin(bookedTraveler.getBookingClasses().getCollection().get(0).getBookingCabin())) {
                seat.setSectionCode(SeatSectionCodeType.FIRST_CLASS);
            } else {
                seat.setSectionCode(SeatSectionCodeType.COACH);
            }

            boardingPassWeb.setSeat(seat);
            SegmentStatus segmentStatus;

            try {
                segmentStatus = getSegmetStatus(bookedLeg, segmentCode);
            } catch (Exception e) {
                LOG.error("Cannot get segment {} for the boarding passes", segmentCode, e);
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.CANNOT_GET_SEGMENT_STATUS_INFORMATION);
            }

            boardingPassWeb.setSegmentStatus(segmentStatus);

            LOG.info("Adding all parameters for url");

            // @fjosorio New change in calling method calculateApppleWallet adding pos param
            String appleWallet = calculateAppleWallet(
                    segmentStatus,
                    segmentDocument.getBarcode(),
                    segmentDocument.getBoardingZone(),
                    bookedTraveler.getFirstName(),
                    bookedTraveler.getLastName(),
                    seat.getSeatCode(),
                    seat.getSectionCode(),
                    segmentDocument.isSkyPriority(),
                    segmentDocument.isTsaPreCheck(),
                    pos,
                    pnr,
                    bookedTraveler.getTicketNumber(),
                    bookedTraveler.getFrequentFlyerProgram(),
                    bookedTraveler.getFrequentFlyerNumber()
            );

            boardingPassWeb.setAppleWalletPass(appleWallet);

            boardingList.add(boardingPassWeb);

            if (null != bookedTraveler.getInfant()) {
                try {
                    BoardingPassWeb boardingInfant = fillDataInfantForWeb(bookedTraveler, segmentCode);

                    if (null != boardingInfant) {

                        boardingInfant.setSegmentStatus(segmentStatus);

                        // @fjosorio New change in calling method calculateApppleWallet adding pos param
                        appleWallet = calculateAppleWallet(
                                segmentStatus,
                                bookedTraveler.getInfant().getSegmentDocumentsList().getBySegmentCode(segmentCode).getBarcode(),
                                bookedTraveler.getInfant().getSegmentDocumentsList().getBySegmentCode(segmentCode).getBoardingZone(),
                                bookedTraveler.getInfant().getFirstName(),
                                bookedTraveler.getInfant().getLastName(),
                                boardingInfant.getSeat().getSeatCode(),
                                boardingInfant.getSeat().getSectionCode(),
                                segmentDocument.isSkyPriority(),
                                segmentDocument.isTsaPreCheck(),
                                pos,
                                pnr,
                                null,
                                null,
                                null
                        );

                        boardingInfant.setAppleWalletPass(appleWallet);

                        boardingList.add(boardingInfant);
                    }
                } catch (Exception e) {
                    LOG.error(e.getMessage(), e);
                    LOG.error("Cannot get the information for infant: {}", bookedTraveler.getInfant());
                    throw new GenericException(Response.Status.NOT_FOUND, ErrorType.CANNOT_GET_INFANT_INFORMATION);
                }
            }
        }
        return boardingList;
    }

    /**
     * New method to calculate apple wallet for APP or WEB.
     *
     * @param segmentStatus {@link SegmentStatus}
     * @param barCode String.
     * @param zone String.
     * @param firstName String.
     * @param lastName String.
     * @param seatNumber String.
     * @param serviceClass {@link SeatSectionCodeType}
     * @param skyPriority boolean.
     * @param tsaPre boolean.
     * @return String. The apple wallet URL.
     * @throws UnsupportedEncodingException
     * @throws IOException
     */
    private String calculateAppleWallet(
            SegmentStatus segmentStatus,
            String barCode,
            String zone,
            String firstName,
            String lastName,
            String seatNumber,
            SeatSectionCodeType serviceClass,
            boolean skyPriority,
            boolean tsaPre,
            String pos,
            PNR pnr,
            String ticketNumber,
            String ffProgram,
            String ffNumber
    ) throws IOException {

        String contextRoot = "/api/v1/";
        if (pos.equalsIgnoreCase(PosType.CHECKIN_MOBILE.toString())) {
            contextRoot += "app/";
        }

        if (barCode != null) {
            barCode = barCode.replace("&lt;", ">");
        }

        StringBuilder sb = new StringBuilder(contextRoot + "checkin/apple-wallet/boarding-pass?");

        StringBuilder aux = new StringBuilder().append(firstName);
        aux.append(" ");
        aux.append(lastName);
        sb.append("passenger=");
        sb.append(URLEncoder.encode(aux.toString(), "UTF-8"));

        aux = new StringBuilder().append(segmentStatus.getSegment().getOperatingCarrier());
        aux.append(" ");
        aux.append(segmentStatus.getSegment().getOperatingFlightCode());
        sb.append("&flight=");
        sb.append(URLEncoder.encode(aux.toString(), "UTF-8"));

        sb.append("&departure-airport=");
        sb.append(segmentStatus.getSegment().getDepartureAirport());
        sb.append("&departure-city=");

        try {
            String cityName = airportCodesDao.getAirportWeatherCityNameByIata(segmentStatus.getSegment().getDepartureAirport());
            sb.append(URLEncoder.encode(cityName, "UTF-8"));
        } catch (Exception ex) {
            LOG.info("Cannot get information of city name from database", ex);
        }

        try {
            AirportWeather airportWeather = airportCodesDao.getAirportWeatherByIata(segmentStatus.getSegment().getDepartureAirport());

            sb.append("&latitude=");
            sb.append(airportWeather.getAirport().getLatitude());

            sb.append("&longitude=");
            sb.append(airportWeather.getAirport().getLongitude());

        } catch (Exception ex) {
            LOG.info("Cannot get city location", ex);
        }

        if (segmentStatus.getSegment().getDepartureDateTime().contains("T")) {
            try {
                LocalDateTime dateTime = LocalDateTime.parse(segmentStatus.getSegment().getDepartureDateTime(), DateTimeFormatter.ISO_LOCAL_DATE_TIME);

                sb.append("&departure-date=");
                sb.append(URLEncoder.encode(dateTime.format(DateTimeFormatter.ofPattern("MMM dd, yyyy")), "UTF-8"));

                sb.append("&departure-time=");
                sb.append(URLEncoder.encode(dateTime.format(DateTimeFormatter.ofPattern("hh:mma")), "UTF-8"));

            } catch (Exception e) {
                LOG.error("Error getting information from departure date and time: ", e);
                sb.append("&departure-date=");
                sb.append("&departure-time=");
            }
        }

        if (segmentStatus.getSegment().getArrivalDateTime().contains("T")) {
            try {
                LocalDateTime dateTime = LocalDateTime.parse(segmentStatus.getSegment().getArrivalDateTime(), DateTimeFormatter.ISO_LOCAL_DATE_TIME);

                sb.append("&arrival-date=");
                sb.append(URLEncoder.encode(dateTime.format(DateTimeFormatter.ofPattern("MMM dd, yyyy")), "UTF-8"));

                sb.append("&arrival-time=");
                sb.append(URLEncoder.encode(dateTime.format(DateTimeFormatter.ofPattern("hh:mma")), "UTF-8"));

            } catch (Exception e) {
                LOG.error("Error getting information from arrival date and time: ", e);
                sb.append("&arrival-date=");
                sb.append("&arrival-time=");
            }
        }

        sb.append("&boarding-time=");
        String boardingTime = formatBoardingTime(segmentStatus.getBoardingTime());
        if (boardingTime != null) {
            sb.append(URLEncoder.encode(boardingTime, "UTF-8"));
        }

        sb.append("&skyPriority=");
        sb.append(URLEncoder.encode(Boolean.toString(skyPriority), "UTF-8"));

        sb.append("&tsaPre=");
        sb.append(URLEncoder.encode(Boolean.toString(tsaPre), "UTF-8"));

        sb.append("&boarding-group=");
        sb.append(zone);

        sb.append("&departure-terminal=");
        sb.append(segmentStatus.getBoardingTerminal());
        sb.append("&departure-gate=");
        sb.append(segmentStatus.getBoardingGate());

        sb.append("&arrival-city=");
        try {
            String cityName = airportCodesDao.getAirportWeatherCityNameByIata(segmentStatus.getSegment().getArrivalAirport());
            sb.append(URLEncoder.encode(cityName, "UTF-8"));
        } catch (Exception ex) {
            LOG.info("Cannot get information of city name from database", ex);
        }
        sb.append("&arrival-airport=");
        sb.append(segmentStatus.getSegment().getArrivalAirport());

        sb.append("&service-class=");
        switch (serviceClass) {
            case FIRST_CLASS:
                sb.append("BUSSINESS");
                break;
            default:
                sb.append("COACH");
                break;
        }

        if (seatNumber != null) {
            sb.append("&seat-assignment=");
            sb.append(seatNumber.trim());
        }

        if (pnr != null) {
            sb.append("&pnr=");
            sb.append(pnr.getPnr());
        }

        sb.append("&ticketNumber=");
        sb.append(ticketNumber);

        if (ffProgram != null && ffNumber != null) {
            sb.append("&clubPremier=");
            sb.append(ffProgram + ffNumber);
        }

        //sb.append("&international=");
        //sb.append(international);
        sb.append("&operatingCarrier=");
        sb.append(URLEncoder.encode(segmentStatus.getSegment().getOperatingCarrier(), "UTF-8"));

        sb.append("&barcodeData=");
        sb.append(URLEncoder.encode(barCode, "UTF-8"));

        sb.append("&relevantDate=");
        try {
            sb.append(URLEncoder.encode(getRelevantDate(segmentStatus.getSegment().getDepartureAirport(), segmentStatus.getSegment().getDepartureDateTime(), pos), "UTF-8"));
        } catch (Exception e) {
            LOG.error("Error creating relevant date: " + e.getMessage());
        }

        return sb.toString();
    }

    /**
     * Service for boarding pass
     *
     * @param pnr
     * @param boardingPassesRQ
     * @param cartPNR
     * @param warningCollection
     * @throws Exception
     */
    protected void getReprintInformation(
            PNR pnr,
            BoardingPassesRQ boardingPassesRQ,
            CartPNR cartPNR,
            WarningCollection warningCollection
    ) throws Exception {

        LOG.info("Finding information of the itinerary and the passengers");

        List<ACSReprintBPRSACS> reprintBPFinal = new ArrayList<>();
        String boardingPrinterCommand = setUpConfigFactory.getInstance().getBoardingPassPrinterCommand();
        String boardingPrinter = setUpConfigFactory.getInstance().getBoardingLNIATA();

        List<ItineraryACS> itineraryList = getItineraryFromPNR(pnr, boardingPassesRQ.getLegCode());
        for (ItineraryACS itinerary : itineraryList) {

            List<PassengerInfoACS> passengerInfoList = getListPassengerInfo(
                    cartPNR, boardingPassesRQ,
                    itinerary.getOrigin()
            );

            if (passengerInfoList.size() > 0) {
                List<ACSReprintBPRSACS> reprintBP = reprintService.reprintBP(itinerary, passengerInfoList,
                        PrintFormatACS.PECTAB, boardingPrinter, boardingPrinterCommand);
                for (ACSReprintBPRSACS reprintOne : reprintBP) {
                    if (null != reprintOne.getPECTABDataList()) {
                        reprintBPFinal.add(reprintOne);
                    } else {
                        LOG.info("The segment {} is not valid for get boarding pass {}",
                                itinerary.getOrigin() + "_" + itinerary.getDestination(),
                                reprintOne.getResult().getStatus().toString());
                    }
                }
            }
        }

        if (reprintBPFinal.size() > 0) {
            // Add validation for errors on reprint service
            BookedLeg bookedLeg = pnr.getBookedLegByLegCode(cartPNR.getLegCode());
            updateCheckin(
                    boardingPassesRQ.getPos(),
                    boardingPassesRQ.getRecordLocator(),
                    cartPNR,
                    bookedLeg,
                    reprintBPFinal,
                    warningCollection,
                    pnr.isTsaRequired()
            );
        }
    }

    /**
     * Service for boarding pass
     *
     * @param pnr
     * @param pos
     * @param cartPNR
     * @param warningCollection
     * @throws Exception
     */
    protected void getReprintInformationWeb(
            PNR pnr,
            String pos,
            CartPNR cartPNR,
            WarningCollection warningCollection
    ) throws Exception {

        List<ACSReprintBPRSACS> reprintBPFinal = new ArrayList<>();
        String boardingPrinterCommand;
        boardingPrinterCommand = setUpConfigFactory.getInstance().getBoardingPassPrinterCommand();
        String boardingPrinter = setUpConfigFactory.getInstance().getBoardingLNIATA();

        List<ItineraryACS> itineraryList = getItineraryFromPNRWeb(pnr, cartPNR.getLegCode());
        for (ItineraryACS itinerary : itineraryList) {

            List<PassengerInfoACS> passengerInfoList = getListPassengerInfoWeb(cartPNR, itinerary.getOrigin());

            if (passengerInfoList.size() > 0) {
                List<ACSReprintBPRSACS> reprintBP;
                reprintBP = reprintService.reprintBP(
                        itinerary, passengerInfoList,
                        PrintFormatACS.PECTAB,
                        boardingPrinter,
                        boardingPrinterCommand
                );

                for (ACSReprintBPRSACS reprintOne : reprintBP) {
                    if (null != reprintOne.getPECTABDataList()) {
                        reprintBPFinal.add(reprintOne);
                    } else {
                        LOG.error("The segment {} is not valid for get boarding pass {}",
                                itinerary.getOrigin() + "_" + itinerary.getDestination(),
                                reprintOne.getResult().getStatus().toString());
                    }
                }
            }
        }

        if (reprintBPFinal.size() > 0) {
            // Add validation for errors on reprint service
            BookedLeg bookedLeg = pnr.getBookedLegByLegCode(cartPNR.getLegCode());
            updateCheckin(
                    pos, pnr.getPnr(),
                    cartPNR, bookedLeg,
                    reprintBPFinal,
                    warningCollection,
                    pnr.isTsaRequired()
            );
        }
    }

    /**
     * Get passenger info from cart pnr
     *
     * @param cartPNR
     * @param boardingPassesRQ
     * @param segOriginal
     * @return
     */
    protected List<PassengerInfoACS> getListPassengerInfo(
            CartPNR cartPNR, BoardingPassesRQ boardingPassesRQ,
            String segOriginal
    ) {

        List<PassengerInfoACS> passengerInfoList = new ArrayList<>();

        for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {

            if (boardingPassesRQ.getPaxId().contains(bookedTraveler.getId())) {

                if (needBoardingPassInfo(bookedTraveler)) {
                    PassengerInfoACS passenger = new PassengerInfoACS();
                    passenger.setLastName(bookedTraveler.getLastName());
                    for (BookingClass booked : bookedTraveler.getBookingClasses().getCollection()) {
                        if (booked.getSegmentCode().substring(0, 3).equalsIgnoreCase(segOriginal)) {
                            passenger.setPassengerID(booked.getPassengerId());
                        }
                    }

                    if (null == passenger.getPassengerID() || passenger.getPassengerID().trim().isEmpty()) {
                        passenger.setPassengerID(bookedTraveler.getId());
                    }

                    passengerInfoList.add(passenger);
                }
            }
        }
        return passengerInfoList;
    }

    /**
     * Get passenger info from cart pnr
     *
     * @param cartPNR
     * @param segOriginal
     * @return
     */
    protected List<PassengerInfoACS> getListPassengerInfoWeb(CartPNR cartPNR, String segOriginal) {

        List<PassengerInfoACS> passengerInfoList = new ArrayList<>();

        for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {

            if (needBoardingPassInfo(bookedTraveler)) {
                PassengerInfoACS passenger = new PassengerInfoACS();
                passenger.setLastName(bookedTraveler.getLastName());
                for (BookingClass booked : bookedTraveler.getBookingClasses().getCollection()) {
                    if (booked.getSegmentCode().substring(0, 3).equalsIgnoreCase(segOriginal)) {
                        passenger.setPassengerID(booked.getPassengerId());
                    }
                }

                if (null == passenger.getPassengerID() || passenger.getPassengerID().trim().isEmpty()) {
                    passenger.setPassengerID(bookedTraveler.getId());
                }

                passengerInfoList.add(passenger);
                // }
            }
        }
        LOG.info("getListPassengerInfo for segment: {} {}", segOriginal, passengerInfoList.size());
        return passengerInfoList;
    }

    /**
     * Get Itinerary from pnr collection
     *
     * @param pnr
     * @param legCode
     * @return
     * @throws Exception
     */
    protected List<ItineraryACS> getItineraryFromPNR(PNR pnr, String legCode) throws Exception {

        LOG.info("Getting the itinerary object from pnr: {}", pnr.getPnr());

        BookedLeg legByLegCode;
        List<BookedLeg> legsByLegCodes = new ArrayList<>();
        List<ItineraryACS> itineraryList = new ArrayList<>();

        if (null != pnr.getLegs().getCollection() && !pnr.getLegs().getCollection().isEmpty()) {

            if (StringUtils.isNotBlank(legCode)) {
                legByLegCode = pnr.getBookedLegByLegCode(legCode);
                legsByLegCodes.add(legByLegCode);
            } else {
                for (CartPNR cartLeg : pnr.getCarts().getCollection()) {
                    legCode = cartLeg.getLegCode();
                    legByLegCode = pnr.getBookedLegByLegCode(legCode);
                    if (null != legByLegCode) {
                        legsByLegCodes.add(legByLegCode);
                    }
                }
            }

            createItineraryFromBookedLeg(legsByLegCodes, itineraryList);
        }

        return itineraryList;
    }

    private void createItineraryFromBookedLeg(List<BookedLeg> legsByLegCodes, List<ItineraryACS> itineraryList) {
        for (BookedLeg legData : legsByLegCodes) {
            if (null != legData.getSegments().getCollection() && !legData.getSegments().getCollection().isEmpty()) {
                for (BookedSegment bookedSegment : legData.getSegments().getCollection()) {
                    ItineraryACS itinerary = new ItineraryACS();
                    itinerary.setAirline(bookedSegment.getSegment().getOperatingCarrier());
                    itinerary.setDepartureDate(bookedSegment.getSegment().getDepartureDateTime().substring(0, 10));
                    itinerary.setDestination(bookedSegment.getSegment().getArrivalAirport());
                    itinerary.setFlight(bookedSegment.getSegment().getOperatingFlightCode());
                    itinerary.setOrigin(bookedSegment.getSegment().getDepartureAirport());
                    itineraryList.add(itinerary);
                }
            }
        }
    }

    /**
     * Get Itinerary from pnr collection
     *
     * @param pnr
     * @param legCode
     * @return
     * @throws Exception
     */
    protected List<ItineraryACS> getItineraryFromPNRWeb(PNR pnr, String legCode) throws Exception {

        LOG.info("Getting the itinerary object from pnr: {}", pnr.getPnr());

        BookedLeg legByLegCode;
        List<BookedLeg> legsByLegCodes = new ArrayList<>();
        List<ItineraryACS> itineraryList = new ArrayList<>();

        if (null != pnr.getLegs().getCollection() && !pnr.getLegs().getCollection().isEmpty()) {

            legByLegCode = pnr.getBookedLegByLegCode(legCode);
            if (null != legByLegCode) {
                legsByLegCodes.add(legByLegCode);
            }

            createItineraryFromBookedLeg(legsByLegCodes, itineraryList);
        }

        return itineraryList;
    }

    /**
     * Get segment status from Leg Collection
     *
     * @param legBooked
     * @param segment
     * @return
     */
    protected SegmentStatus getSegmetStatus(BookedLeg legBooked, String segment) {
        LOG.info("Adding segment information in the segment: {}", segment);

        BookedSegment bookedSegment = legBooked.getSegments().getBookedSegment(segment);

        if (null == bookedSegment) {
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.CANNOT_GET_SEGMENT_STATUS_INFORMATION);
        }

        SegmentStatus segmentStatus = new SegmentStatus();
        String defaultBoardingGate = "TBD";

        if (null != bookedSegment.getSegment().getBoardingGate()
                && !StringUtils.isBlank(bookedSegment.getSegment().getBoardingGate())) {

            String boardingGate = bookedSegment.getSegment().getBoardingGate();

            if (!"Not asigned".equalsIgnoreCase(boardingGate)
                    && !"GATE".equalsIgnoreCase(boardingGate)) {
                segmentStatus.setBoardingGate(boardingGate);
            } else {
                segmentStatus.setBoardingGate(defaultBoardingGate);
            }
        } else {
            segmentStatus.setBoardingGate(defaultBoardingGate);
        }

        segmentStatus.setBoardingTerminal(bookedSegment.getSegment().getBoardingTerminal());
        String boardingTimeFinal = null;
        if (StringUtils.isNotBlank(legBooked.getBoardingTime())) {
            boardingTimeFinal = getTwentyFour(legBooked.getBoardingTime());
        }
        segmentStatus.setBoardingTime(boardingTimeFinal);
        segmentStatus.setEstimatedArrivalTime(bookedSegment.getSegment().getArrivalDateTime());
        segmentStatus.setEstimatedDepartureTime(legBooked.getEstimatedDepartureTime());
        segmentStatus.setSegment(bookedSegment.getSegment());
        segmentStatus.setStatus(legBooked.getFlightStatus().toString());
        segmentStatus.setArrivalGate("");
        segmentStatus.setArrivalTerminal("");
        return segmentStatus;
    }

    /**
     * Get Seat from an abstract object
     *
     * @param collectionSegment
     * @return Seat Object
     */
    protected Seat getSeatFromCart(List<AbstractSegmentChoice> collectionSegment, String segment) {
        LOG.info("Making the information of seat passenger for segment: {}", segment);
        Seat seat = new Seat();
        for (AbstractSegmentChoice abstractSegmentChoice : collectionSegment) {
            if (abstractSegmentChoice.getSegmentCode().equals(segment)) {
                seat.setSeatCode(abstractSegmentChoice.getSeat().getCode());
                if (abstractSegmentChoice.getSeat() instanceof SeatChoice) {
                    SeatChoice seatChoise = (SeatChoice) abstractSegmentChoice.getSeat();
                    seat.setSeatCharacteristics(seatChoise.getSeatCharacteristics());
                } else if (abstractSegmentChoice.getSeat() instanceof SeatChoiceFareUpgrade) {
                    SeatChoiceFareUpgrade seatChoise = (SeatChoiceFareUpgrade) abstractSegmentChoice.getSeat();
                    seat.setSeatCharacteristics(seatChoise.getSeatCharacteristics());
                } else if (abstractSegmentChoice.getSeat() instanceof SeatChoiceFareUpsell) {
                    SeatChoiceFareUpsell seatChoise = (SeatChoiceFareUpsell) abstractSegmentChoice.getSeat();
                    seat.setSeatCharacteristics(seatChoise.getSeatCharacteristics());
                } else if (abstractSegmentChoice.getSeat() instanceof SeatChoiceUpgrade) {
                    SeatChoiceUpgrade seatChoise = (SeatChoiceUpgrade) abstractSegmentChoice.getSeat();
                    seat.setSeatCharacteristics(seatChoise.getSeatCharacteristics());
                } else if (abstractSegmentChoice.getSeat() instanceof SeatChoiceUpsell) {
                    SeatChoiceUpsell seatChoise = (SeatChoiceUpsell) abstractSegmentChoice.getSeat();
                    seat.setSeatCharacteristics(seatChoise.getSeatCharacteristics());
                } else if (abstractSegmentChoice.getSeat() instanceof SeatChoiceWaivedUpgrade) {
                    SeatChoiceWaivedUpgrade seatChoise = (SeatChoiceWaivedUpgrade) abstractSegmentChoice.getSeat();
                    seat.setSeatCharacteristics(seatChoise.getSeatCharacteristics());
                }
            }
        }
        return seat;
    }

    /**
     * This method is user for get boardingpasses from sabre response and is
     * used in Junit test
     *
     * @param boardingPassesRQ
     * @param pnrCollection
     * @return
     * @throws Exception
     */
    protected BoardingPassResponse getWebBoardingPasses(
            BoardingPassesRQ boardingPassesRQ,
            PNRCollection pnrCollection
    ) throws Exception {

        PNR pnr = pnrCollection.getPNRByRecordLocator(boardingPassesRQ.getRecordLocator());
        if (pnr == null) {
            LOG.error("Invalid PNR value in the request, please verify: {}", boardingPassesRQ);
            throw new GenericException(Response.Status.NOT_FOUND, ErrorType.PNR_NOT_FOUND);
        }

        if (pnr.isTsaRequired() && !boardingPassesRQ.isOverrideTsa()) {
            LOG.error("Tsa validation required");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.TSA_VALIDATION_REQUIRED);
        }

        if (pnr.getCarts() == null
                || pnr.getCarts().getCollection() == null
                || pnr.getCarts().getCollection().isEmpty()) {
            LOG.error("The PNR doesn't have carts please validate PNR: {}", boardingPassesRQ);
            throw new GenericException(Response.Status.NOT_FOUND, ErrorType.CART_COLLECTION_OBJECT_EMPTY);
        }

        if (boardingPassesRQ.getLegCode() == null || boardingPassesRQ.getLegCode().trim().isEmpty()) {
            LOG.error("Cannot find the booked leg with the legcode specified: {}", boardingPassesRQ);
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.LEGCODE_REQUIRED_FOR_KIOSK);
        }

        BookedLeg bookedLeg = pnr.getBookedLegByLegCode(boardingPassesRQ.getLegCode().trim());
        if (bookedLeg == null) {
            LOG.error("Cannot find the booked leg with the specified legcode: {}", boardingPassesRQ);
            throw new GenericException(Response.Status.NOT_FOUND, ErrorType.LEGCODE_NOT_FOUND_IN_THE_PNR);
        }

        CartPNR cartPNR = pnr.getCartPNRByLegCode(boardingPassesRQ.getLegCode().trim());
        if (boardingPassesRQ.getLegCode() != null
                && !boardingPassesRQ.getLegCode().trim().isEmpty()) {
            if (cartPNR == null) {
                LOG.error("Cannot find the cart with the specified legcode, {}", boardingPassesRQ);
                throw new GenericException(Response.Status.NOT_FOUND,
                        ErrorType.CART_NOT_FOUND_IN_COLLECTION_WITH_LEGCODE);
            }
        }

        List<BookedTraveler> alreadyCheckedInPassengers = FilterPassengersUtil.getAlreadyCheckedInPassengers(
                cartPNR.getTravelerInfo().getCollection()
        );

        BoardingPassCollection boardingPassCollection;

        List<List<BookedTraveler>> groupedPassengersByPassengerType = FilterPassengersUtil.groupPassengersByPassengerType(
                alreadyCheckedInPassengers
        );

        if (!alreadyCheckedInPassengers.isEmpty()
                && !groupedPassengersByPassengerType.get(EMPLOYEE_INDEX).isEmpty()) {
            cartPNR.setTouched(true);
            pnr.setStandby(true);

            boardingPassCollection = new BoardingPassCollection();

            for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
                if (bookedTraveler.isCheckinStatus()
                        && PassengerFareType.E.getCode().equalsIgnoreCase(bookedTraveler.getPassengerType())) {

                    bookedTraveler.setTouched(true);
                    BoardingPassUtil.setStandByBoardingPasses(bookedTraveler, bookedLeg, pnr.getPnr());

                    for (AbstractSegmentChoice abstractSegmentChoice : bookedTraveler.getSegmentChoices().getCollection()) {
                        abstractSegmentChoice.getSeat().setCode("STB");
                    }

                    List<BoardingPassWeb> boardingFilled;
                    boardingFilled = fillDataBoardingPassForWeb(
                            bookedTraveler,
                            boardingPassesRQ.getPos(),
                            bookedLeg,
                            pnr
                    );

                    if (boardingFilled != null && !boardingFilled.isEmpty()) {
                        boardingPassCollection.getCollection().addAll(boardingFilled);
                    }

                    String legCode;
                    try {
                        legCode = getLegCodeFromDocumentList(bookedTraveler);
                    } catch (Exception e) {
                        legCode = bookedLeg.getSegments().getLegCodeRackspace();
                    }

                    String departureDate = bookedLeg.getSegments().getCollection().get(0).getSegment().getDepartureDateTime().substring(0, 10).replace("-", "_");

                    String link = createDownloadLinkRackspace(
                            boardingPassesRQ.getRecordLocator(),
                            departureDate,
                            legCode,
                            bookedTraveler.getFirstName().substring(0, 1) + "_" + bookedTraveler.getLastName() + "_" + bookedTraveler.getTicketNumber()
                    );
                    boardingPassCollection.addItineraryPdf(
                            bookedTraveler.getTicketNumber(),
                            link
                    );

                }
            }
        } else {

            boolean requireDigitalSignature = false;

            if (isUsaOrigin(pnr, boardingPassesRQ.getLegCode())) {

                requireDigitalSignature = true;

                boardingPassCollection = getBoardingPassCollectionWithDigitalSignature(
                        boardingPassesRQ,
                        pnrCollection
                );

                setPriorityBoarding(pnrCollection, boardingPassesRQ.getRecordLocator());

                if (null != boardingPassCollection) {
                    updateBoardingPassesWithDigitalSignature(pnrCollection, boardingPassCollection);
                    getMailDispatcher().publish(pnrCollection);
                    BoardingPassResponse response = new BoardingPassResponse();
                    response.setBoardingPassCollection(boardingPassCollection);
                    response.setPnrCollection(pnrCollection);
                    return response;
                }
            }

            boardingPassCollection = new BoardingPassCollection();

            for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {

                validateReprintInformation(
                        boardingPassesRQ.getPos(),
                        pnr,
                        cartPNR,
                        bookedTraveler,
                        pnrCollection.getWarnings()
                );

                List<BoardingPassWeb> boardingFilled = fillDataBoardingPassForWeb(
                        bookedTraveler, boardingPassesRQ.getPos(), bookedLeg, pnr
                );

                if (boardingFilled != null && !boardingFilled.isEmpty()) {

                    if (requireDigitalSignature) {
                        for (BoardingPassWeb boardingPassWeb : boardingFilled) {
                            boardingPassWeb.setDigitalSignatureStatus(DigitalSignatureStatusType.MISSING);
                        }
                    }

                    boardingPassCollection.getCollection().addAll(boardingFilled);
                }

                String legCode;
                try {
                    legCode = getLegCodeFromDocumentList(bookedTraveler);
                } catch (Exception e) {
                    legCode = bookedLeg.getSegments().getLegCodeRackspace();
                }

                String departureDate = bookedLeg.getSegments().getCollection().get(0).getSegment().getDepartureDateTime().substring(0, 10).replace("-", "_");

                String link = createDownloadLinkRackspace(
                        boardingPassesRQ.getRecordLocator(),
                        departureDate,
                        legCode,
                        bookedTraveler.getFirstName().substring(0, 1) + "_" + bookedTraveler.getLastName() + "_" + bookedTraveler.getTicketNumber()
                );
                boardingPassCollection.addItineraryPdf(bookedTraveler.getTicketNumber(),
                        link
                );
            }
        }

        try {
            LOG.info("Purchase: Trying to send email");
            updateBoardingPassesWithDigitalSignature(pnrCollection, boardingPassCollection);

            setPriorityBoarding(pnrCollection, boardingPassesRQ.getRecordLocator());

            getMailDispatcher().publish(pnrCollection);
        } catch (Exception ex) {
            LOG.error("Error dispatching pnr collection please verify", ex);
        }

        if (null == boardingPassCollection.getCollection() || boardingPassCollection.getCollection().isEmpty()) {
            LOG.error("Error to get boarding pass information, some objects are not valid");
            LOG.error("CartPNR: {}", cartPNR);
            if(PosType.WEB.equals(boardingPassesRQ.getPos())){
                throw new GenericException(Response.Status.NOT_FOUND, ErrorType.BOARDING_PASSES_NOT_GENERATED_WEB);
            }else{
                throw new GenericException(Response.Status.NOT_FOUND, ErrorType.BOARDING_PASSES_NOT_GENERATED);
            }
        }

        LOG.info("Setting final data in boarding collection");
        PectabBoardingPassList pectabBoardingPassList = new PectabBoardingPassList();
        PectabBoardingPass pectabBoarding = new PectabBoardingPass();
        pectabBoarding.setValue("");
        pectabBoarding.setVersion("");
        pectabBoardingPassList.add(pectabBoarding);
        boardingPassCollection.setPnr(boardingPassesRQ.getRecordLocator());
        boardingPassCollection.setPectabBoardingPassList(pectabBoardingPassList);

        String iataCity;
        iataCity = boardingPassCollection.getCollection().get(0).getSegmentStatus().getSegment().getArrivalAirport();
        DailyForecasts wheatherCityDest;
        wheatherCityDest = getWeatherForService(
                iataCity,
                boardingPassesRQ.getPos(),
                boardingPassesRQ.getLanguage()
        );

        boardingPassCollection.setDailyForecasts(wheatherCityDest);
        boardingPassCollection.setPos(boardingPassesRQ.getPos());

        //Reviewed  until the new mailing api is active
        //If there is just one pax the link for all is the same
        if (cartPNR.getTravelerInfo().getCollection().size() == 1) {
            boardingPassCollection.addItineraryPdf(
                    "ALL",
                    boardingPassCollection.getItineraryPdf().get(cartPNR.getTravelerInfo().getCollection().get(0).getTicketNumber())
            );
        } else {
            String legCode;
            try {
                legCode = boardingPassCollection.getItineraryPdf().get(cartPNR.getTravelerInfo().getCollection().get(0).getTicketNumber()).split("_")[5] + "_";
            } catch (Exception e) {
                legCode = bookedLeg.getSegments().getLegCodeRackspace();
            }

            String departureDate = bookedLeg.getSegments().getCollection().get(0).getSegment().getDepartureDateTime().substring(0, 10).replace("-", "_");

            String link = createDownloadLinkRackspace(boardingPassesRQ.getRecordLocator(), departureDate, legCode, "ALL");
            boardingPassCollection.addItineraryPdf(
                    "ALL",
                    link
            );
        }

        LOG.info("pnrCollection after bording passes: {}", pnrCollection.toString());

        updatePnrCollectionService.createOrUpdatePnrCollectionAfterCheckin(
                pnrCollection,
                boardingPassesRQ.getRecordLocator(),
                cartPNR.getMeta().getCartId(), true
        );

        BoardingPassResponse response = new BoardingPassResponse();
        response.setBoardingPassCollection(boardingPassCollection);
        response.setPnrCollection(pnrCollection);
        return response;

    }

    private void updateBoardingPassesWithDigitalSignature(
            PNRCollection pnrCollection,
            BoardingPassCollection boardingPassCollection
    ) {
        for (PNR pnr : pnrCollection.getCollection()) {
            for (CartPNR cartPNR : pnr.getCarts().getCollection()) {
                for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
                    for (SegmentDocument segmentDocument : bookedTraveler.getSegmentDocumentsList()) {

                        BoardingPassWeb boardingPassWeb = boardingPassCollection.getBoardingPassByNamerefAndSegmentcode(
                                bookedTraveler.getNameRefNumber(), segmentDocument.getSegmentCode()
                        );

                        if (null != boardingPassWeb) {
                            segmentDocument.setQrcodeImage(boardingPassWeb.getQrCodeImage());
                            segmentDocument.setBarcodeImage(boardingPassWeb.getBarcodeImage());
                        }
                    }
                }
            }
        }
    }

    public static String haveCobranded(CartPNR cartPNR) {
        if (null == cartPNR
                || null == cartPNR.getTravelerInfo()
                || null == cartPNR.getTravelerInfo().getCollection()) {
            return null;
        }

        String cardId = null;
        int priority = 0;

        for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
            if (null != bookedTraveler.getBookedTravellerBenefitCollection()
                    && null != bookedTraveler.getBookedTravellerBenefitCollection().getCollection()
                    && !bookedTraveler.getBookedTravellerBenefitCollection().getCollection().isEmpty()) {

                for (BookedTravelerBenefit bookedTravelerBenefit : bookedTraveler.getBookedTravellerBenefitCollection().getCollection()) {

                    if (null != bookedTravelerBenefit.getBagBenefitCobrand()
                            && null != bookedTravelerBenefit.getBagBenefitCobrand().getFreeBagAllowanceByCardList()
                            && !bookedTravelerBenefit.getBagBenefitCobrand().getFreeBagAllowanceByCardList().isEmpty()) {

                        for (FreeBaggageAllowanceDetailByCard freeBaggageAllowanceDetailByCard : bookedTravelerBenefit.getBagBenefitCobrand().getFreeBagAllowanceByCardList()) {

                            String card = freeBaggageAllowanceDetailByCard.getCobrandCode();

                            if (null != card) {

                                switch (card) {
                                    case "AMXPLT":
                                        if (priority < 4) {
                                            //priority = 4;
                                            cardId = card;

                                            //is the highter priority, if one is found stop searching
                                            return cardId;
                                        }
                                        break;
                                    case "AMXGLD":
                                        if (priority < 3) {
                                            priority = 3;
                                            cardId = card;
                                        }
                                        break;
                                    case "SEIFNT":
                                        if (priority < 2) {
                                            priority = 2;
                                            cardId = card;
                                        }
                                        break;
                                    case "SEPLT":
                                        if (priority < 1) {
                                            priority = 1;
                                            cardId = card;
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
            }
        }

        return cardId;
    }

    public static void setPriorityBoarding(PNRCollection pnrCollection, String recordLocator) {
        try {
            PNR pnr = pnrCollection.getPNRByRecordLocator(recordLocator);

            for (CartPNR cartPNR : pnr.getCarts().getCollection()) {

                String cardId = haveCobranded(cartPNR);

                if (null == cardId) {
                    return;
                }

                for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
                    if (null != bookedTraveler.getSegmentDocumentsList()
                            && !bookedTraveler.getSegmentDocumentsList().isEmpty()) {

                        for (SegmentDocument segmentDocument : bookedTraveler.getSegmentDocumentsList()) {
                            if (null != segmentDocument) {
                                segmentDocument.setCardID(cardId);
                                segmentDocument.setPriorityBoarding(true);
                            }
                        }
                    }

                    if (null != bookedTraveler.getInfant() && null != bookedTraveler.getInfant().getSegmentDocumentsList()) {
                        for (SegmentDocument segmentDocument : bookedTraveler.getInfant().getSegmentDocumentsList()) {
                            if (null != segmentDocument) {
                                segmentDocument.setCardID(cardId);
                                segmentDocument.setPriorityBoarding(true);
                            }
                        }

                    }
                }
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
    }

    /**
     * Create download link to rackspace files
     *
     * @param recordLocator
     * @param departureDate
     * @param legCode
     * @param ticketNumber
     * @return
     */
    private String createDownloadLinkRackspace(
            String recordLocator, String departureDate,
            String legCode, String ticketNumber
    ) {

        if (SystemVariablesUtil.isCreateDownloadBoardingPassesLinkEnabled()) {
            try {
                AMCloudFile amCloudFile = new AMCloudFile();

                String downloadLink = amCloudFile.getPublishURL();
                //"http://a10093a1dcff20c33b04-0e7cdc201efa111f6c12c7e84417b41a.r62.cf1.rackcdn.com";
                amCloudFile.closeConnection();

                return downloadLink + "/" + recordLocator + "/" + "Aeromexico_" + recordLocator + "_"
                        + departureDate + "_"
                        + legCode
                        + ticketNumber + ".pdf";
            } catch (Exception ex) {
                LOG.error(ex.getMessage(), ex);
                return "";
            }
        } else {
            return "";
        }
    }

    /**
     * Build legCode from segment documnets list
     *
     * @param bookedTraveler
     * @return
     */
    private String getLegCodeFromDocumentList(BookedTraveler bookedTraveler)
            throws Exception {
        try {
            if (bookedTraveler.getSegmentDocumentsList() != null
                    && bookedTraveler.getSegmentDocumentsList().size() > 0) {
                SegmentDocumentList segmentDocumentList = bookedTraveler.getSegmentDocumentsList();

                if (segmentDocumentList.size() == 1) {
                    return segmentDocumentList.get(0).getSegmentCode().substring(0, 7)
                            .replace("_", "-") + "_";
                } else {
                    String departureCity = segmentDocumentList.get(0).getSegmentCode().substring(0, 3);
                    String arrivalCity = segmentDocumentList.get(segmentDocumentList.size() - 1).getSegmentCode()
                            .split("_")[1];

                    return departureCity + "-" + arrivalCity + "_";
                }
            }
        } catch (Exception e) {
            throw e;
        }

        return "";
    }

    private BoardingPassCollection getBoardingPassCollectionWithDigitalSignature(
            BoardingPassesRQ boardingPassesRQ,
            PNRCollection pnrCollection
    ) throws Exception {
        String sabreResponse;

        try {
            sabreResponse = digitalSignatureSabreClient.doGet(
                    boardingPassesRQ.getRecordLocator(),
                    boardingPassesRQ.getStore()
            );

            LOG.info("Sabre response: {}", sabreResponse);

            PassengerDetailsResponse lookupResponse;
            lookupResponse = new Gson().fromJson(sabreResponse, PassengerDetailsResponse.class);

            BoardingPassCollection boardingPassesWeb = createWebBoardingPasses(
                    lookupResponse,
                    boardingPassesRQ.getLanguage(),
                    boardingPassesRQ.getPos(),
                    pnrCollection,
                    boardingPassesRQ.getLegCode(),
                    boardingPassesRQ.getRecordLocator()
            );

            return boardingPassesWeb;

        } catch (Exception ex) {
            LOG.error("Error getting digital signature from internal call");
            LOG.error(ex.getMessage(), ex);

            return null;
        }
    }

    private void validateReprintInformation(
            String pos, PNR pnr,
            CartPNR cartPnr,
            BookedTraveler bookedTraveler,
            WarningCollection warningCollection
    ) throws Exception {
        if (bookedTraveler == null) {
            LOG.error("Invalid booked Traveler in pnr {} the object is null", pnr);
            throw new GenericException(Response.Status.NOT_FOUND, ErrorType.TRAVELER_ID_NOT_MATCH);
        } else if (bookedTraveler.getSegmentDocumentsList() != null
                && !bookedTraveler.getSegmentDocumentsList().isEmpty()) {

            for (SegmentDocument segmentDocument : bookedTraveler.getSegmentDocumentsList()) {
                if (segmentDocument.getTravelDoc() == null || segmentDocument.getTravelDoc().trim().isEmpty()) {

                    getReprintInformationWeb(
                            pnr, pos, cartPnr, warningCollection
                    );
                }
            }

        } else {
            getReprintInformationWeb(
                    pnr, pos, cartPnr, warningCollection
            );
        }
    }

    /**
     * This method is user for get boarding pass collection from sabre response
     * and is used in Junit test
     *
     * @param lookupResponse
     * @param language
     * @param pos
     * @param pnrCollection
     * @param legCode
     * @param recordLocator
     * @return
     * @throws java.lang.Exception
     */
    protected BoardingPassCollection createWebBoardingPasses(
            PassengerDetailsResponse lookupResponse,
            String language,
            String pos,
            PNRCollection pnrCollection,
            String legCode,
            String recordLocator
    ) throws Exception {

        PNR pnr = pnrCollection.getPNRByRecordLocator(recordLocator);
        BookedLegCollection bookedLegCollection = pnr.getLegs();

        LOG.info("DIGITAL: " + lookupResponse);
        BoardingPassCollection boardingCollection = new BoardingPassCollection();
        List<BoardingPass> boardingPassList = new ArrayList<>();
        CartPNR cartPnr = null;

        try {
            cartPnr = pnr.getCartPNRByLegCode(legCode);
        } catch (Exception ex) {
            //TODO: throw an exception with a valid error and error code
            throw new Exception("Could nog find cart for legcode");
        }

        String iataCity = "";
        Reservation reservation = lookupResponse.getReservation();
        for (Passenger passengerElement : reservation.getPassengers().getPassenger()) {
            if(!"INFANT".equalsIgnoreCase(passengerElement.getType().getValue())) {
                if (isNotSelectee(passengerElement)) {

                    String nameRef = passengerElement.getId().substring(1);

                    for (PassengerSegment passSegment : passengerElement.getPassengerSegments().getPassengerSegment()) {

                        for (PassengerFlight passFlight : passSegment.getPassengerFlight()) {

                            String segmentCode = containsSegmentCode(passFlight, cartPnr);

                            if (passFlight.getCheckedIn()
                                    && null != segmentCode
                                    && null != passFlight.getBoardingPass()) {

                                BoardingPassWeb boardingPass = new BoardingPassWeb();
                                boardingPass.setSegmentCode(segmentCode);

                                LOG.info("Fill information of passengers for pnr: {}", lookupResponse.getReservation().getRecordLocator());
                                CodesGenerator generator = new CodesGenerator();

                                boardingPass.setNameRefNumber(nameRef);
                                boardingPass.setFirstName(passFlight.getBoardingPass().getPersonName().getFirst());
                                boardingPass.setLastName(passFlight.getBoardingPass().getPersonName().getLast());

                                if (null != passFlight.getBoardingPass().getLoyaltyAccount()) {
                                    boardingPass.setFrequentFlyerAirline(passFlight.getBoardingPass().getLoyaltyAccount().getMemberAirline());
                                    boardingPass.setFrequentFlyerNumber(passFlight.getBoardingPass().getLoyaltyAccount().getMemberId());
                                }

                                boolean hasDigitalSignature = false;

                                if (StringUtils.isNotBlank(passFlight.getBoardingPass().getBarCode())) {

                                    boardingPass.setBarcode(passFlight.getBoardingPass().getBarCode());

                                    //sample
                                    //M1LOPEZ/EMILY         EBRHGOH MEXPVRAM 0142 079Y009A0026 162>5322RR9079BAM                                        2A139211019840500                          N^160MEQCIDxazxMO4rAt7uNhciy1WHZh12QhMgPS9g0NgWwtdc+lAiAkiNd3b9uLEHUEncjBeiDl0JTozKXyyOx97OAL+0y+dw==
                                    String[] parts = passFlight.getBoardingPass().getBarCode().split("\\^");

                                    if (null != parts && parts.length > 1) {
                                        hasDigitalSignature = true;
                                    }

                                    boardingPass.setBarcodeImage(
                                            generator.codesGenerator2D(
                                                    passFlight.getBoardingPass().getBarCode()
                                            )
                                    );
                                    boardingPass.setQrCodeImage(
                                            generator.codesGeneratorQR(
                                                    passFlight.getBoardingPass().getBarCode()
                                            )
                                    );
                                } else {
                                    LOG.error(
                                            "Error to get barcode from sabre service, response sabre {}",
                                            lookupResponse.getResults().get(0).getStatus().get(0).getMessage()
                                    );
                                    throw new GenericException(
                                            Response.Status.NOT_FOUND,
                                            ErrorType.UNABLE_TO_PROVIDE_BOARDING_PASS_BAR_CODE
                                    );
                                }

                                boardingPass.setControlCode(passFlight.getBoardingPass().getCheckInSequenceNumber());
                                boardingPass.setClassOfService(passFlight.getBoardingPass().getFareInfo().getBookingClass());

                                boolean tsaValue = false;
                                if (null != passFlight.getBoardingPass().getDisplayData()) {
                                    tsaValue = StringUtils.isNotBlank(passFlight.getBoardingPass().getDisplayData().getTsaPreCheckText());
                                }
                                boardingPass.setTSAPre(tsaValue);

                                boolean skyPriority = false;
                                if (null != passFlight.getBoardingPass().getSupplementaryData()) {
                                    skyPriority = passFlight.getBoardingPass().getSupplementaryData().getSkyPriority();
                                }
                                boardingPass.setSkyPriority(skyPriority);

                                if (StringUtils.isNotBlank(passFlight.getBoardingPass().getZone())) {
                                    boardingPass.setZone(passFlight.getBoardingPass().getZone());
                                } else {
                                    boardingPass.setZone("-");
                                }

                                boardingPass.setTicketNumber(passFlight.getBoardingPass().getTicketNumber().getNumber());
                                SegmentStatus segmentStatus = extractSegmentStatusSabre(passFlight, bookedLegCollection);
                                boardingPass.setSegmentStatus(segmentStatus);

                                Seat seat = extractSeatSabre(passFlight);
                                boardingPass.setSeat(seat);

                                if (hasDigitalSignature) {
                                    boardingPass.setDigitalSignatureStatus(DigitalSignatureStatusType.OK);
                                } else {
                                    LOG.error("MISSING DIGITAL SIGNATURE: {} {}", recordLocator, passFlight.getBoardingPass().getBarCode());
                                    boardingPass.setDigitalSignatureStatus(DigitalSignatureStatusType.MISSING);
                                }

                                for (BookedTraveler bookedTraveler : cartPnr.getTravelerInfo().getCollection()) {
                                    if (null != bookedTraveler.getNameRefNumber()
                                            && passengerElement.getId().contains(bookedTraveler.getNameRefNumber())) {
                                        if (bookedTraveler.isCheckinStatus()) {

                                            if (hasDigitalSignature) {
                                                bookedTraveler.setDigitalSignatureStatus(DigitalSignatureStatusType.OK);
                                            } else {
                                                bookedTraveler.setDigitalSignatureStatus(DigitalSignatureStatusType.MISSING);
                                            }

                                            //Reviewed  until the new mailing api is active
                                            String legCodeString;
                                            try {
                                                legCodeString = getLegCodeFromDocumentList(bookedTraveler);
                                            } catch (Exception e) {
                                                legCodeString = pnr.getBookedLegByLegCode(legCode).getSegments().getLegCodeRackspace();
                                            }

                                            String departureDate = pnr
                                                    .getBookedLegByLegCode(legCode)
                                                    .getSegments()
                                                    .getCollection()
                                                    .get(0)
                                                    .getSegment()
                                                    .getDepartureDateTime().substring(0, 10).replace("-", "_");

                                            String link = createDownloadLinkRackspace(
                                                    lookupResponse.getReservation().getRecordLocator(),
                                                    departureDate,
                                                    legCodeString,
                                                    bookedTraveler.getFirstName().substring(0, 1) + "_" + bookedTraveler.getLastName() + "_" + bookedTraveler.getTicketNumber()
                                            );

                                            boardingCollection.addItineraryPdf(
                                                    bookedTraveler.getTicketNumber(),
                                                    link
                                            );

                                            boardingPass.setSelectee(bookedTraveler.isSelectee());

                                            if (bookedTraveler.getBookingClasses() != null && !bookedTraveler.getBookingClasses().getCollection().isEmpty()) {
                                                for (BookingClass bookingClass : bookedTraveler.getBookingClasses().getCollection()) {
                                                    if (SegmentCodeUtil.compareSegmentCodeWithoutTime(bookingClass.getSegmentCode(), boardingPass.getSegmentStatus().getSegment().getSegmentCode())) {
                                                        bookedTraveler.getSegmentDocumentsList().addOrReplace(createSegmentDocumentFromBoardingPass(segmentCode, passFlight, boardingPass));
                                                    }
                                                }
                                            } else {
                                                bookedTraveler.getSegmentDocumentsList().addOrReplace(createSegmentDocumentFromBoardingPass(segmentCode, passFlight, boardingPass));
                                            }
                                        } else {
                                            bookedTraveler.setDigitalSignatureStatus(DigitalSignatureStatusType.NOT_CHECKED_IN);
                                        }
                                        break;
                                    }
                                }

                                if (null != passFlight.getBoardingPass().getFlightDetail()) {
                                    iataCity = passFlight.getBoardingPass().getFlightDetail().getArrivalAirport();
                                }

                                //Reviewed  until the new mailing api is active
                                //Check if there is just one pax
                                if (cartPnr.getTravelerInfo().getCollection().size() == 1) {
                                    boardingCollection.addItineraryPdf("ALL",
                                            boardingCollection.getItineraryPdf().get(cartPnr.getTravelerInfo()
                                                    .getCollection().get(0).getTicketNumber()));
                                } else {
                                    String legCodeString;
                                    try {
                                        legCodeString = boardingCollection.getItineraryPdf().get(cartPnr.getTravelerInfo()
                                                .getCollection().get(0).getTicketNumber()).split("_")[5] + "_";
                                    } catch (Exception e) {
                                        legCodeString = pnr.getBookedLegByLegCode(legCode).getSegments().getLegCodeRackspace();
                                    }

                                    String departureDate = pnr.getBookedLegByLegCode(legCode).getSegments().getCollection().get(0).getSegment()
                                            .getDepartureDateTime().substring(0, 10).replace("-", "_");

                                    String link = createDownloadLinkRackspace(
                                            lookupResponse.getReservation().getRecordLocator(),
                                            departureDate,
                                            legCodeString,
                                            "ALL"
                                    );

                                    boardingCollection.addItineraryPdf("ALL",
                                            link);
                                }
                                try {
                                    String appleWallet = calculateAppleWallet(
                                            segmentStatus,
                                            passFlight.getBoardingPass().getBarCode(),
                                            boardingPass.getZone(),
                                            boardingPass.getFirstName(),
                                            boardingPass.getLastName(),
                                            boardingPass.getSeat().getSeatCode(),
                                            boardingPass.getSeat().getSectionCode(),
                                            boardingPass.isSkyPriority(),
                                            boardingPass.isTSAPre(),
                                            pos,
                                            pnr,
                                            boardingPass.getTicketNumber(),
                                            boardingPass.getFrequentFlyerAirline(),
                                            boardingPass.getFrequentFlyerNumber()
                                    );

                                    boardingPass.setAppleWalletPass(appleWallet);
                                } catch (Exception ex) {
                                    LOG.error("Error trying to get applewallet string", ex);
                                }
                                boardingPassList.add(boardingPass);
                            }
                        }
                    }
                }
            }else{
                List<BoardingPass> boardingInfant = getInfantSabre(
                        passengerElement,
                        bookedLegCollection,
                        pos, pnr
                );
                boardingPassList.addAll(boardingInfant);
            }
        }

        if (boardingPassList.isEmpty()) {
            return null;
        } else {
            LOG.info("Setting final data for boarding collection");
            PectabBoardingPassList pectabBoardingPassList = new PectabBoardingPassList();
            PectabBoardingPass pectabBoarding = new PectabBoardingPass();
            pectabBoarding.setValue("");
            pectabBoarding.setVersion("");
            pectabBoardingPassList.add(pectabBoarding);
            boardingCollection.setPnr(lookupResponse.getReservation().getRecordLocator());
            boardingCollection.setPectabBoardingPassList(pectabBoardingPassList);
            boardingCollection.setDailyForecasts(getWeatherForService(iataCity, pos, language));
            boardingCollection.setCollection(boardingPassList);
            boardingCollection.setPos(pos);
            return boardingCollection;
        }
    }

    private SegmentDocument createSegmentDocumentFromBoardingPass(
            String segmentCode,
            PassengerFlight passFlight,
            BoardingPassWeb boardingPass
    ) {
        SegmentDocument segmentDocument = new SegmentDocument();
        segmentDocument.setBarcode(passFlight.getBoardingPass().getBarCode());
        segmentDocument.setBarcodeImage(boardingPass.getBarcodeImage());
        segmentDocument.setQrcodeImage(boardingPass.getQrCodeImage());
        segmentDocument.setTsaPreCheck(boardingPass.isTSAPre());
        segmentDocument.setSkyPriority(boardingPass.isSkyPriority());
        segmentDocument.setBoardingZone(boardingPass.getZone());
        segmentDocument.setSegmentCode(segmentCode);
        return segmentDocument;
    }

    private boolean isNotSelectee(Passenger passengerElement) {
        if (passengerElement.getEligibilities() != null && !passengerElement.getEligibilities().getEligibility().isEmpty()) {
            for (Eligibility elegibilities : passengerElement.getEligibilities().getEligibility()) {
                for(Reason reason : elegibilities.getReason()){
                    if (reason.getMessage().equalsIgnoreCase("BOARDING_PASS_INHIBITED_SELECTEE")) {
                        return false;
                    }
                }

            }
        }
        return true;
    }

    private String containsSegmentCode(PassengerFlight passFlight, CartPNR cartPnr) {

        if (null != passFlight.getBoardingPass().getFlightDetail()
                && null != passFlight.getBoardingPass()) {
            try {

                String segmentCode = passFlight.getBoardingPass().getFlightDetail().getDepartureAirport()
                        + "_" + passFlight.getBoardingPass().getFlightDetail().getArrivalAirport()
                        + "_" + passFlight.getBoardingPass().getFlightDetail().getOperatingAirline()
                        + "_" + passFlight.getBoardingPass().getFlightDetail().getDepartureTime().substring(0, 10)
                        + "_" + passFlight.getBoardingPass().getFlightDetail().getDepartureTime().substring(11, 16).replace(":", "");

                for (BookedTraveler bookedTraveler : cartPnr.getTravelerInfo().getCollection()) {
                    for (BookingClass bookinClasses : bookedTraveler.getBookingClasses().getCollection()) {
                        if (SegmentCodeUtil.compareSegmentCodeWithoutTime(segmentCode, bookinClasses.getSegmentCode())) {
                            return segmentCode;
                        }
                    }
                }
            } catch (Exception ex) {
                LOG.error(ex.getMessage(), ex);
            }
        }

        return null;
    }

    /**
     * Method to get Infant information from Sabre. Additional changed adding
     * pos for apple wallet.
     *
     * @param infant {@link Infant}
     * @param bookedLegCollection {@link BookedLegCollection}
     * @param pos String.
     * @return List<BoardingPass>
     */
    private List<BoardingPass> getInfantSabre(Passenger infant, BookedLegCollection bookedLegCollection, String pos, PNR pnr) {
        List<BoardingPass> boardingPassesInfant = new ArrayList<>();
        for (PassengerSegment passSegment : infant.getPassengerSegments().getPassengerSegment()) {
            for (PassengerFlight passFlight : passSegment.getPassengerFlight()) {
                if (passFlight.getCheckedIn()) {
                    BoardingPassWeb boardingPass = new BoardingPassWeb();
                    if (passFlight.getBoardingPass() != null) {
                        LOG.info("Fill information of infant passengers");
                        CodesGenerator generator = new CodesGenerator();
                        boardingPass.setFirstName(passFlight.getBoardingPass().getPersonName().getFirst());
                        boardingPass.setLastName(passFlight.getBoardingPass().getPersonName().getLast());
                        boardingPass.setBarcodeImage(generator.codesGenerator2D(passFlight.getBoardingPass().getBarCode()));
                        boardingPass.setQrCodeImage(generator.codesGeneratorQR(passFlight.getBoardingPass().getBarCode()));
                        boardingPass.setControlCode(passFlight.getBoardingPass().getCheckInSequenceNumber());
                        boardingPass.setClassOfService(passFlight.getBoardingPass().getFareInfo().getBookingClass());
                        if (passFlight.getBoardingPass().getLoyaltyAccount().getMemberId() != null) {
                            boardingPass.setFrequentFlyerAirline(passFlight.getBoardingPass().getLoyaltyAccount().getMemberAirline());
                            boardingPass.setFrequentFlyerNumber(passFlight.getBoardingPass().getLoyaltyAccount().getMemberId());
                        }
                        boolean tsaValue = StringUtils.isNotBlank(passFlight.getBoardingPass().getDisplayData().getTsaPreCheckText());
                        boardingPass.setTSAPre(tsaValue);
                        boardingPass.setSkyPriority(passFlight.getBoardingPass().getSupplementaryData().getSkyPriority());
                        if (StringUtils.isNotBlank(passFlight.getBoardingPass().getZone())) {
                            boardingPass.setZone(passFlight.getBoardingPass().getZone());
                        } else {
                            boardingPass.setZone("-");
                        }
                        boardingPass.setTicketNumber(passFlight.getBoardingPass().getTicketNumber().getNumber());

                        SegmentStatus segmentStatus = extractSegmentStatusSabre(passFlight, bookedLegCollection);
                        boardingPass.setSegmentStatus(segmentStatus);
                        Seat seat = extractSeatSabre(passFlight);
                        boardingPass.setSeat(seat);

                        try {
                            String appleWallet = calculateAppleWallet(
                                    segmentStatus,
                                    passFlight.getBoardingPass().getBarCode(),
                                    boardingPass.getZone(),
                                    boardingPass.getFirstName(),
                                    boardingPass.getLastName(),
                                    boardingPass.getSeat().getSeatCode(),
                                    boardingPass.getSeat().getSectionCode(),
                                    boardingPass.isSkyPriority(),
                                    boardingPass.isTSAPre(),
                                    pos,
                                    pnr,
                                    boardingPass.getTicketNumber(),
                                    boardingPass.getFrequentFlyerAirline(),
                                    boardingPass.getFrequentFlyerNumber()
                            );

                            boardingPass.setAppleWalletPass(appleWallet);
                        } catch (Exception ex) {
                            LOG.error("Error trying to get applewallet string: " + ex.getMessage(), ex);
                        }
                    }
                    boardingPassesInfant.add(boardingPass);
                }
            }
        }
        return boardingPassesInfant;
    }

    /**
     * This method is user for get seat from sabre response and is used in Junit
     * test
     *
     * @param passFlight
     * @return
     */
    protected Seat extractSeatSabre(PassengerFlight passFlight) {
        if (null == passFlight.getSeat()) {
            return null;
        }

        Seat seat = new Seat();
        seat.setSeatCode(passFlight.getSeat().getValue());

        if (null != passFlight.getBoardingPass()) {

            if (isFirstClassCabin(passFlight.getBoardingPass().getFareInfo().getBookingClass())) {
                seat.setSectionCode(SeatSectionCodeType.FIRST_CLASS);
            } else {
                seat.setSectionCode(SeatSectionCodeType.COACH);
            }

        } else {
            seat.setSectionCode(SeatSectionCodeType.COACH);
        }

        seat.setSeatCharacteristics(new ArrayList<>());

        return seat;
    }

    /**
     * This method is user for get segment from sabre response and is used in
     * Junit test
     *
     * @param passFlight
     * @param bookedLegCollection
     * @return
     */
    protected SegmentStatus extractSegmentStatusSabre(
            PassengerFlight passFlight,
            BookedLegCollection bookedLegCollection
    ) {
        SegmentStatus segmentStatus = new SegmentStatus();

        String defaultDepartureGate = "TBD";

        if (null != passFlight
                && null != passFlight.getBoardingPass().getFlightDetail()
                && null != passFlight.getBoardingPass().getFlightDetail().getDepartureGate()
                && StringUtils.isNotBlank( passFlight.getBoardingPass().getFlightDetail().getDepartureGate())) {

            String departureGate = passFlight.getBoardingPass().getFlightDetail().getDepartureGate();

            if (!"Not asigned".equalsIgnoreCase(departureGate)
                    && !"GATE".equalsIgnoreCase(departureGate)) {
                segmentStatus.setBoardingGate(departureGate);
            } else {
                segmentStatus.setBoardingGate(defaultDepartureGate);
            }

        } else {
            segmentStatus.setBoardingGate(defaultDepartureGate);
        }

        String boardingTimeFinal = "--:--";

        if (null != passFlight) {
            if (StringUtils.isNotBlank(
                    passFlight.getBoardingPass().getDisplayData().getBoardingTime()
            )) {
                boardingTimeFinal = passFlight.getBoardingPass().getDisplayData().getBoardingTime().substring(0, 4);
                boardingTimeFinal = boardingTimeFinal.substring(0, 2) + ":" + boardingTimeFinal.substring(2, 4);
            }
        }

        segmentStatus.setBoardingTime(boardingTimeFinal);

        segmentStatus.setArrivalGate("");
        segmentStatus.setArrivalTerminal("");
        segmentStatus.setBoardingTerminal("");

        if (null != passFlight) {
            segmentStatus.setEstimatedArrivalTime(passFlight.getBoardingPass().getFlightDetail().getArrivalTime());
            segmentStatus.setEstimatedDepartureTime(passFlight.getBoardingPass().getFlightDetail().getDepartureTime());
            segmentStatus.setStatus(passFlight.getBoardingPass().getFlightDetail().getDepartureFlightScheduleStatus());

            Segment segment = extractSegmentSabre(passFlight, bookedLegCollection);
            segmentStatus.setSegment(segment);
        }

        return segmentStatus;
    }

    /**
     * This method is user for get information in the response from Sabre
     * Digital Signature is used in JUnit test
     *
     * @param passFlight
     * @param bookedLegCollection
     * @return
     */
    protected Segment extractSegmentSabre(PassengerFlight passFlight, BookedLegCollection bookedLegCollection) {
        Segment segment = new Segment();

        segment.setLayoverToNextSegmentsInMinutes(0);

        segment.setArrivalAirport(passFlight.getBoardingPass().getFlightDetail().getArrivalAirport());
        segment.setArrivalDateTime(passFlight.getBoardingPass().getFlightDetail().getArrivalTime());
        segment.setDepartureAirport(passFlight.getBoardingPass().getFlightDetail().getDepartureAirport());
        segment.setDepartureDateTime(passFlight.getBoardingPass().getFlightDetail().getDepartureTime());
        segment.setOperatingCarrier(passFlight.getBoardingPass().getFlightDetail().getOperatingAirline());
        segment.setOperatingFlightCode(String.valueOf(passFlight.getBoardingPass().getFlightDetail().getOperatingFlightNumber()));
        segment.setMarketingCarrier(passFlight.getBoardingPass().getFlightDetail().getAirline());
        segment.setMarketingFlightCode(String.valueOf(passFlight.getBoardingPass().getFlightDetail().getFlightNumber()));
        segment.setIsPremierAvailable(false);
        segment.setFlightDurationInMinutes(0);

        String replaceTime = passFlight.getBoardingPass().getFlightDetail().getDepartureTime().replace("T", "_").substring(0, 16);
        replaceTime = replaceTime.replace(":", "");
        String sb = passFlight.getBoardingPass().getFlightDetail().getDepartureAirport() + "_"
                + passFlight.getBoardingPass().getFlightDetail().getArrivalAirport() + "_"
                + passFlight.getBoardingPass().getFlightDetail().getAirline() + "_"
                + replaceTime;
        segment.setSegmentCode(sb);

        for (BookedLeg bookedLeg : bookedLegCollection.getCollection()) {
            for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {
                Segment segmentPnr = bookedSegment.getSegment();
                if (segmentPnr.getDepartureAirport().equals(passFlight.getBoardingPass().getFlightDetail().getDepartureAirport())
                        && segmentPnr.getArrivalAirport().equals(passFlight.getBoardingPass().getFlightDetail().getArrivalAirport())) {
                    segment.setFlightDurationInMinutes(segmentPnr.getFlightDurationInMinutes());
                    segment.setSegmentNumber(segmentPnr.getSegmentNumber());
                    segment.setSegmentStatus(segmentPnr.getSegmentStatus());
                    segment.setAircraftType(segmentPnr.getAircraftType());
                }
            }
        }
        return segment;
    }

    private boolean isUsaOrigin(String origin) {
        try {

            if (null == USA_AIRPORTS || USA_AIRPORTS.isEmpty()) {
                String listResult = setUpConfigFactory.getInstance().getUsaAirports();
                String[] airports = listResult.replace(" ", "").split(",");
                for (int i = 0; i < airports.length; i++) {
                    airports[i] = airports[i].toUpperCase();
                }
                USA_AIRPORTS = new ArrayList<>(Arrays.asList(airports));
            }

            if (null != USA_AIRPORTS && !USA_AIRPORTS.isEmpty()) {

                if (USA_AIRPORTS.contains(origin)) {
                    return true;
                }

            } else {
                LOG.info("Usa airports is empty");
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            return false;
        }
        return false;
    }

    private boolean isUsaOrigin(PNR pnr, String legCode) {
        try {

            if (null == USA_AIRPORTS || USA_AIRPORTS.isEmpty()) {
                String listResult = setUpConfigFactory.getInstance().getUsaAirports();
                String[] airports = listResult.replace(" ", "").split(",");
                for (int i = 0; i < airports.length; i++) {
                    airports[i] = airports[i].toUpperCase();
                }
                USA_AIRPORTS = new ArrayList<>(Arrays.asList(airports));
            }

            if (null != USA_AIRPORTS && !USA_AIRPORTS.isEmpty()) {

                CartPNR cart = pnr.getCartPNRByLegCode(legCode);

                BookedLeg bookedLeg = pnr.getBookedLegByLegCode(legCode);

                for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {
                    if (USA_AIRPORTS.contains(bookedSegment.getSegment().getDepartureAirport())) {
                        return true;
                    }
                }

            } else {
                LOG.info("Usa airports is empty");
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            return false;
        }
        return false;
    }

    private boolean validatePassengerIds(BoardingPassesRQ boardingPassesRQ, CartPNR cartPNR) throws Exception {

        if (boardingPassesRQ.getPaxId() == null || boardingPassesRQ.getPaxId().isEmpty()) {
            LOG.error("Passenger list is empty, please verify that is a valid list");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PAX_LIST_REQUIRED_FOR_KIOSK);
        }

        for (String passengerId : boardingPassesRQ.getPaxId()) {

            BookedTraveler bookedTraveler = cartPNR.getBookedTravelerById(passengerId);

            if (bookedTraveler == null) {
                LOG.error("Invalid pax ID: {}", passengerId);
                throw new GenericException(Response.Status.NOT_FOUND, ErrorType.TRAVELER_ID_NOT_MATCH);
            } else if (bookedTraveler.getSegmentDocumentsList() != null
                    && !bookedTraveler.getSegmentDocumentsList().isEmpty()) {

                for (SegmentDocument segmentDocument : bookedTraveler.getSegmentDocumentsList()) {
                    if (segmentDocument.getTravelDoc() == null || StringUtils.isBlank(segmentDocument.getTravelDoc())) {
                        LOG.info("validatePassengerIds returning FALSE for PAX {}", passengerId);
                        return false;
                    }
                }

            } else {
                return false;
            }
        }

        return true;
    }

    private DailyForecasts getWeatherForService(String iataCity, String pos, String language) {

        DailyForecasts wheatherCityDest = null;
        String formatArrival;

        if (PosType.KIOSK.toString().equalsIgnoreCase(pos)) {
            LOG.info("Trying to get the weather of destination");
            boolean isLanguageMX = false;
            Pattern pattern = Pattern.compile("ES_?.?");
            if (null != language) {
                Matcher matxh = pattern.matcher(language.toUpperCase());
                isLanguageMX = matxh.matches();
            }
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            formatArrival = sdf.format(new Date());

            try {

                wheatherCityDest = WeatherService.getWeather(iataCity, formatArrival, isLanguageMX);
            } catch (Exception e) {
                LOG.error("Cannot complete the weather service call, {}", e);
            }
        }
        return wheatherCityDest;
    }

    /**
     * @param recordLocator
     * @return
     */
    protected PNRCollection findInMongoPnrCollection(String recordLocator, String pos) {

        try {
            return pnrLookupDao.readPNRCollectionByRecordLocator(recordLocator.toUpperCase());
        }catch (MongoDisconnectException me){
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(pos)) {
                throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getCode(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getActionable(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getMessage(),
                        Response.Status.INTERNAL_SERVER_ERROR);
            } else {
                throw me;
            }
        } catch (Exception ex) {
            LOG.error("Failed to getPNR {}: {}", recordLocator, ex.getMessage());
            throw new GenericException(Response.Status.NOT_FOUND, ErrorType.PNR_COLLECTION_OBJECT_EMPTY);
        }
    }

    /**
     *
     * @param origin
     * @param departureDateTime
     * @return
     * @throws Exception
     */
    private String getRelevantDate(String origin, String departureDateTime, String pos) throws Exception {

        LOG.info("+getRelevantDate({}, {})", origin, departureDateTime);

        //Get timeZone for the departure city
        AirportWeather airportWeather;
        try {
            airportWeather = airportCodesDao.getAirportWeatherByIata(origin);
        }catch (MongoDisconnectException me){
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(pos)) {
                throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getCode(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getActionable(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getMessage(),
                        Response.Status.INTERNAL_SERVER_ERROR);
            } else {
                throw me;
            }
        }
        com.aeromexico.timezone.service.TimeZone timeZone = TimeZoneService.getTimeZone(airportWeather.getAirport().getCode(), airportWeather.getAirport().getLatitude(), airportWeather.getAirport().getLongitude());

        LOG.info("TimeZone: " + new Gson().toJson(timeZone));

        //Departure date/time based on local time
        //"2017-10-27T15:25:00"
        SimpleDateFormat localTime = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        localTime.setTimeZone(TimeZone.getTimeZone(timeZone.getTimeZoneId()));

        Calendar localCalendar = Calendar.getInstance();
        localCalendar.setTimeZone(TimeZone.getTimeZone(timeZone.getTimeZoneId()));
        localCalendar.setTime(localTime.parse(departureDateTime));

        //Convert departure date/time from local time zone to UTC/GMT
        //"2017-10-27T15:25-05:00"
        SimpleDateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmX");
        utcFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

        String result = utcFormat.format(localCalendar.getTime());

        LOG.info("-getRelevantDate({})", result);

        return result;
    }

    /**
     *
     * @param pos
     * @param recordLocator
     * @param cartPNR
     * @param bookedLeg
     * @param reprintBP
     * @param warningCollection
     * @param tsaRequired
     * @throws Exception
     */
    private void updateCheckin(
            String pos,
            String recordLocator,
            CartPNR cartPNR,
            BookedLeg bookedLeg,
            List<ACSReprintBPRSACS> reprintBP,
            WarningCollection warningCollection,
            boolean tsaRequired
    ) throws Exception {

        LOG.info("Updating the boarding pass information in cart collection");

        List<ACSCheckInPassengerRSACS> checkInPassengerRSACSList = new ArrayList();

        for (ACSReprintBPRSACS reprintTemp : reprintBP) {

            ACSCheckInPassengerRSACS ascTempData = new ACSCheckInPassengerRSACS();
            ascTempData.setFreeTextInfoList(reprintTemp.getFreeTextInfoList());
            ascTempData.setItineraryPassengerList(reprintTemp.getItineraryPassengerList());
            ascTempData.setPECTABDataList(reprintTemp.getPECTABDataList());

            checkInPassengerRSACSList.add(ascTempData);
        }

        if (PurchaseOrderUtil.searchInBoardingPasses("NOT VALID WITHOUT FLIGHT COUPON", checkInPassengerRSACSList)) {
            if (PurchaseOrderUtil.searchInAllBoardingPasses("NOT VALID WITHOUT FLIGHT COUPON", checkInPassengerRSACSList, warningCollection)) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.NOT_VALID_WITHOUT_FLIGHT_COUPON, PosType.getType(pos));
            }
        }

        List<ACSCheckInPassengerRSACS> acsCheckInPassengerRSACSList = new ArrayList<>();
        List<PectabBoardingPass> pectabDataList = new ArrayList<>();

        for (ACSReprintBPRSACS reprintTemp : reprintBP) {

            ACSCheckInPassengerRSACS ascTempData = new ACSCheckInPassengerRSACS();
            ascTempData.setFreeTextInfoList(reprintTemp.getFreeTextInfoList());
            ascTempData.setItineraryPassengerList(reprintTemp.getItineraryPassengerList());
            ascTempData.setPECTABDataList(reprintTemp.getPECTABDataList());

            if (null != reprintTemp.getPECTABDataList()) {
                if (null != reprintTemp.getPECTABDataList().getPECTABData()
                        && !reprintTemp.getPECTABDataList().getPECTABData().isEmpty()) {
                    for (PECTABDataListACS.PECTABData pectabData : reprintTemp.getPECTABDataList().getPECTABData()) {
                        PectabBoardingPass pectabDataLocal = new PectabBoardingPass();
                        pectabDataLocal.setValue(pectabData.getValue());
                        pectabDataLocal.setVersion(pectabData.getVersion());
                        pectabDataList.add(pectabDataLocal);
                    }
                    ascTempData.setResult(reprintTemp.getResult());
                    acsCheckInPassengerRSACSList.add(ascTempData);
                }
            }
        }
        cartPNR.getPectabBoardingPassList().addAll(pectabDataList);

        List<BookedTraveler> bookedTravelersEligibleForCheckin = cartPNR.getTravelerInfo().getCollection();
        Set<BookedTraveler> bookedTravelElegibles = new HashSet<>(bookedTravelersEligibleForCheckin);

        PnrCollectionUtil.updateAfterSuccessCheckin(
                bookedTravelElegibles,
                acsCheckInPassengerRSACSList,
                cartPNR.getTravelerInfo().getCollection(),
                bookedLeg,
                pos,
                recordLocator,
                tsaRequired,
                warningCollection
        );

    }

    private boolean needBoardingPassInfo(BookedTraveler bookedTraveler) {
        if (bookedTraveler.getSegmentDocumentsList() != null && bookedTraveler.getSegmentDocumentsList().size() > 0) {
            for (SegmentDocument segmentDocument : bookedTraveler.getSegmentDocumentsList()) {
                if (segmentDocument.getTravelDoc() == null) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Parse date format for the schema pattern
     *
     * @param twelve
     * @return
     */
    private String getTwentyFour(String twelve) {
        LOG.info("Parsing the boarding time field");
        String twenty = null;
        if (twelve != null && !twelve.isEmpty()) {
            if (twelve.contains("AM") || twelve.contains("PM")) {
                SimpleDateFormat twelveHours = new SimpleDateFormat("hh:mmaa");
                SimpleDateFormat twentyFourHours = new SimpleDateFormat("HH:mm");
                try {
                    twenty = twentyFourHours.format(twelveHours.parse(twelve));
                } catch (ParseException e) {
                    LOG.error("Error to parsing time: " + e);
                }
            } else if (twelve.length() == 5) {
                return twelve;
            }
        }
        return twenty;
    }

    /**
     * Parse date format for the schema pattern
     *
     * @return
     */
    private String formatBoardingTime(String boardingTime) {
        LOG.info("Parsing the boarding time field: " + boardingTime);
        if (boardingTime != null && !boardingTime.isEmpty()) {
            boardingTime = boardingTime.toUpperCase();
            if (boardingTime.contains("AM") || boardingTime.contains("PM")) {
                return boardingTime;
            } else {
                //Add AM or PM
                if (boardingTime.length() == 5) {
                    SimpleDateFormat twelveHours = new SimpleDateFormat("hh:mmaa");
                    SimpleDateFormat twentyFourHours = new SimpleDateFormat("HH:mm");
                    try {
                        boardingTime = twelveHours.format(twentyFourHours.parse(boardingTime));
                    } catch (ParseException e) {
                        LOG.error("Error to parsing time: " + e);
                    }
                }
            }
        }
        return boardingTime;
    }

    /**
     * Look for the section code
     *
     * @param cabinClass
     * @return boolean
     */
    public boolean isFirstClassCabin(String cabinClass) {
        // ECONOMICA = W,V,R,N, R
        // CLASICA = T,Q, L, H, K, U
        // FLEXIBLE = M, B, Y
        // CONFORT = I, D
        // PREMIER = C, J
        return cabinClass.equalsIgnoreCase("I")
                || cabinClass.equalsIgnoreCase("D")
                || cabinClass.equalsIgnoreCase("C")
                || cabinClass.equalsIgnoreCase("J");
    }

    /**
     * @return the pnrLookupDao
     */
    public IPNRLookupDao getPnrLookupDao() {
        return pnrLookupDao;
    }

    /**
     * @param pnrLookupDao the pnrLookupDao to set
     */
    public void setPnrLookupDao(IPNRLookupDao pnrLookupDao) {
        this.pnrLookupDao = pnrLookupDao;
    }

    /**
     * @return the reprintService
     */
    public AMReprintBPService getReprintService() {
        return reprintService;
    }

    /**
     * @param reprintService the reprintService to set
     */
    public void setReprintService(AMReprintBPService reprintService) {
        this.reprintService = reprintService;
    }

    public void getDigitalSignatureService(
            PNRCollection pnrCollection,
            String recordLocator,
            String legCode,
            String language,
            String store,
            String pos,
            CartPNR cartPNR,
            PNR pnr
    ) {
        try {
            PNR pnrSelected = pnrCollection.getPNRByRecordLocator(recordLocator);

            if (null != pnrSelected) {
                if (isUsaOrigin(pnrSelected, legCode)) {
                    LOG.info("Consulting digital signature for pnr: {}", recordLocator);

                    try {
                        String sabreResponse = digitalSignatureSabreClient.doGet(recordLocator, store);

                        LOG.info("Sabre response: {}", sabreResponse);

                        PassengerDetailsResponse lookupResponse;
                        lookupResponse = new Gson().fromJson(sabreResponse, PassengerDetailsResponse.class);

                        BoardingPassCollection response = createWebBoardingPasses(
                                lookupResponse,
                                language,
                                pos,
                                pnrCollection,
                                legCode,
                                recordLocator
                        );

                        if (response == null) {
                            throw new Exception();
                        }
                    } catch (Exception ex) {

                        LOG.error("Error getting digital signature from external call");
                        LOG.error(ex.getMessage(), ex);

                        BoardingPassCollection boardingCollection = new BoardingPassCollection();
                        for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {

                            if (bookedTraveler.isCheckinStatus()) {
                                bookedTraveler.setDigitalSignatureStatus(DigitalSignatureStatusType.MISSING);
                            } else {
                                bookedTraveler.setDigitalSignatureStatus(DigitalSignatureStatusType.NOT_CHECKED_IN);
                            }

                            validateReprintInformation(
                                    pos,
                                    pnrCollection.getPNRByRecordLocator(recordLocator),
                                    cartPNR,
                                    bookedTraveler,
                                    pnrCollection.getWarnings()
                            );

                            List<BoardingPassWeb> boardingFilled = fillDataBoardingPassForWeb(
                                    bookedTraveler,
                                    pos,
                                    pnrCollection.getBookedLegByLegCode(legCode),
                                    pnr
                            );

                            if (boardingFilled != null && !boardingFilled.isEmpty()) {

                                for (BoardingPassWeb boardingPassWeb : boardingFilled) {
                                    boardingPassWeb.setDigitalSignatureStatus(DigitalSignatureStatusType.MISSING);
                                }

                                boardingCollection.getCollection().addAll(boardingFilled);
                            }

                            //Reviewed  until the new mailing api is active
                            String legCodeString = null;
                            try {
                                legCodeString = getLegCodeFromDocumentList(bookedTraveler);
                            } catch (Exception e) {
                                legCodeString = pnrSelected.getBookedLegByLegCode(legCode).getSegments().getLegCodeRackspace();
                            }

                            String departureDate = pnrSelected.getBookedLegByLegCode(legCode).getSegments().getCollection().get(0).getSegment()
                                    .getDepartureDateTime().substring(0, 10).replace("-", "_");

                            String link = createDownloadLinkRackspace(
                                    recordLocator,
                                    departureDate,
                                    legCodeString,
                                    bookedTraveler.getFirstName().substring(0, 1) + "_" + bookedTraveler.getLastName() + "_" + bookedTraveler.getTicketNumber()
                            );

                            boardingCollection.addItineraryPdf(
                                    bookedTraveler.getTicketNumber(),
                                    link
                            );
                        }
                    }
                } else {
                    LOG.info("Origin is not USA");
                }
            } else {
                LOG.error("Error to get pnr object from recordLocator: {}", recordLocator);
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
    }

    /**
     * @return the updatePnrCollectionService
     */
    public PnrCollectionService getUpdatePnrCollectionService() {
        return updatePnrCollectionService;
    }

    /**
     * @param updatePnrCollectionService the updatePnrCollectionService to set
     */
    public void setUpdatePnrCollectionService(PnrCollectionService updatePnrCollectionService) {
        this.updatePnrCollectionService = updatePnrCollectionService;
    }

    /**
     * @return the mailDispatcher
     */
    public BoardingPassesDispatcher getMailDispatcher() {
        return mailDispatcher;
    }

    /**
     * @param mailDispatcher the mailDispatcher to set
     */
    public void setMailDispatcher(BoardingPassesDispatcher mailDispatcher) {
        this.mailDispatcher = mailDispatcher;
    }

    public DigitalSignatureSabreClient getDigitalSignatureSabreClient() {
        return digitalSignatureSabreClient;
    }

    public void setDigitalSignatureSabreClient(DigitalSignatureSabreClient digitalSignatureSabreClient) {
        this.digitalSignatureSabreClient = digitalSignatureSabreClient;
    }

    /**
     * @return the setUpConfigFactory
     */
    public SetUpConfigFactory getSetUpConfigFactory() {
        if (null == setUpConfigFactory) {
            setUpConfigFactory = new CheckInSetUpConfigFactoryWrapper();
        }
        return setUpConfigFactory;
    }

    /**
     * @param setUpConfigFactory the setUpConfigFactory to set
     */
    public void setSetUpConfigFactory(SetUpConfigFactory setUpConfigFactory) {
        this.setUpConfigFactory = setUpConfigFactory;
    }
}
