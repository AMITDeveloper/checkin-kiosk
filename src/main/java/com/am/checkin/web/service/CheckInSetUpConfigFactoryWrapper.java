package com.am.checkin.web.service;

import com.aeromexico.commons.model.AuthorizationPaymentConfig;
import com.aeromexico.commons.web.util.Constants;
import com.aeromexico.dao.client.exception.MongoDisconnectException;
import com.aeromexico.dao.config.GlobalConfiguration;
import com.aeromexico.dao.config.SetUpConfigFactory;
import com.aeromexico.dao.services.AuthorizationPaymentConfigDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Named("CheckInSetUpConfigFactoryWrapper")
@ApplicationScoped
public class CheckInSetUpConfigFactoryWrapper extends SetUpConfigFactory {

    private static final Logger LOG = LoggerFactory.getLogger(CheckInSetUpConfigFactoryWrapper.class);

    @Inject
    private AuthorizationPaymentConfigDao authorizationPaymentConfigDao;

    @Override
    public void loadSpecificProperties() {
        try {
            GlobalConfiguration.CybersourceConfig cybersourceConfig = new GlobalConfiguration.CybersourceConfig();
            cybersourceConfig.setUrl(systemPropertiesFactory.getInstance().getCybersourceServerUrl());
            cybersourceConfig.setTransactionKey(systemPropertiesFactory.getInstance().getCybersourceTransactionKey());
            cybersourceConfig.setOrgId(systemPropertiesFactory.getInstance().getCybersourceOrgId());
            cybersourceConfig.setMerchantId(systemPropertiesFactory.getInstance().getCybersourceMerchantId());
            globalConfig.setCybersourceConfig(cybersourceConfig);
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }

        try {
            List<AuthorizationPaymentConfig> authorizationPaymentConfigList
                    = authorizationPaymentConfigDao.getAuthorizationPaymentConfigByProject(Constants.PROJECT_CHECKIN);

            authorizationPaymentConfigList = addAuthorizationPaymentConfig(authorizationPaymentConfigList,
                    authorizationPaymentConfigDao.getAuthorizationPaymentConfigByProject(Constants.PROJECT_KIOSK));

            authorizationPaymentConfigList = addAuthorizationPaymentConfig(authorizationPaymentConfigList,
                    authorizationPaymentConfigDao.getAuthorizationPaymentConfigByProject(Constants.PROJECT_MOBILE));

            globalConfig.setAuthorizationPaymentList(authorizationPaymentConfigList);
        } catch (MongoDisconnectException me) {
            LOG.error(me.getMessage(), me);
            throw me;
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
    }

    @Override
    public AuthorizationPaymentConfig getAuthorizationPaymentConfigByStore(String store, String pos) {
        AuthorizationPaymentConfig authorizationPaymentConfig = null;
        try {
            authorizationPaymentConfig = authorizationPaymentConfigDao.getAuthorizationPaymentConfigByStore(Constants.PROJECT_CHECKIN, store);
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
        return authorizationPaymentConfig;
    }

}
