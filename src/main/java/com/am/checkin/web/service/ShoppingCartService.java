package com.am.checkin.web.service;

import com.aeromexico.commons.clubpremier.response.rs.TierBenefitsRS;
import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.AbstractAncillary;
import com.aeromexico.commons.model.AncillaryOfferCollection;
import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.BookedTravelerAncillaryCollection;
import com.aeromexico.commons.model.BookedTravellerBenefitCollection;
import com.aeromexico.commons.model.BookingClass;
import com.aeromexico.commons.model.CarRateDetailsResponse;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.CartPNRUpdate;
import com.aeromexico.commons.model.CurrencyValue;
import com.aeromexico.commons.model.ExtraInfo;
import com.aeromexico.commons.model.FrequentFlyer;
import com.aeromexico.commons.model.PNR;
import com.aeromexico.commons.model.PNRCollection;
import com.aeromexico.commons.model.PaidTravelerAncillary;
import com.aeromexico.commons.model.Segment;
import com.aeromexico.commons.model.TierBenefit;
import com.aeromexico.commons.model.TierBenefitCobrand;
import com.aeromexico.commons.model.TierBenefitCobrandList;
import com.aeromexico.commons.model.TimaticInfo;
import com.aeromexico.commons.model.Total;
import com.aeromexico.commons.model.TravelerAncillary;
import com.aeromexico.commons.model.VisaInfo;
import com.aeromexico.commons.model.Warning;
import com.aeromexico.commons.model.WarningCollection;
import com.aeromexico.commons.model.rq.ShoppingCartGETRQ;
import com.aeromexico.commons.model.rq.ShoppingCartRQ;
import com.aeromexico.commons.model.utils.AncillariesUtil;
import com.aeromexico.commons.model.utils.CurrencyUtil;
import com.aeromexico.commons.model.weather.AirportWeather;
import com.aeromexico.commons.web.types.ActionableType;
import com.aeromexico.commons.web.types.CheckinIneligibleReasons;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.types.FarebasisFamilyType;
import com.aeromexico.commons.web.types.MarketType;
import com.aeromexico.commons.web.types.PosType;
import com.aeromexico.commons.web.types.PriorityCodeType;
import com.aeromexico.commons.web.types.RegionType;
import com.aeromexico.commons.web.types.TierLevelType;
import com.aeromexico.commons.web.util.Constants;
import com.aeromexico.commons.web.util.ErrorCodeDescriptions;
import com.aeromexico.commons.web.util.FilterPassengersUtil;
import com.aeromexico.commons.web.util.TimeUtil;
import com.aeromexico.dao.client.GenericRestClient;
import com.aeromexico.dao.client.exception.MongoDisconnectException;
import com.aeromexico.dao.qualifiers.AirportsCache;
import com.aeromexico.dao.qualifiers.TierBenefitsCache;
import com.aeromexico.dao.services.IAirportCodesDao;
import com.aeromexico.dao.services.IPNRLookupDao;
import com.aeromexico.dao.services.ITierBenefitDao;
import com.aeromexico.dao.services.ShoppingCartDao;
import com.aeromexico.dao.util.CommonUtil;
import com.aeromexico.profile.util.BenefitUtil;
import com.aeromexico.sabre.api.acs.AMEditPassCharService;
import com.aeromexico.sabre.api.acs.AMFlightRemarksService;
import com.aeromexico.sabre.api.acs.AMPassengerDataService;
import com.aeromexico.sabre.api.acs.AMPassengerSecurityService;
import com.aeromexico.sabre.api.model.SabrePassengerDTO;
import com.aeromexico.sabre.api.passenger.AMPassengerDetailsService;
import com.aeromexico.sabre.api.session.common.ServiceCall;
import com.aeromexico.sabre.api.updatereservation.AMUpdateReservationService;
import com.aeromexico.sharedservices.util.AncillaryUtil;
import com.aeromexico.sharedservices.util.RevenuePriorityCodeUtil;
import com.aeromexico.sharedservices.util.UpdatePassengerUtil;
import com.aeromexico.webe4.web.common.types.ErrorCodeType;
import com.aeromexico.webe4.web.shoppingcart.model.RateReferenceMongo;
import com.am.checkin.web.util.PNRLookUpServiceUtil;
import com.am.checkin.web.util.PnrCollectionUtil;
import com.am.checkin.web.util.ShoppingCartErrorUtil;
import com.am.checkin.web.util.ShoppingCartUtil;
import com.am.checkin.web.v2.seamless.service.SeamlessUpdatePassengerInfoService;
import com.am.checkin.web.v2.services.CovidRestrictionStorageService;
import com.am.checkin.web.v2.util.PriorityUtil;
import com.am.checkin.web.v2.util.SystemVariablesUtil;
import com.google.gson.Gson;
import com.mongodb.MongoException;
import com.sabre.services.checkin.getpassengerdata.v4.GetPassengerDataRSACS;
import com.sabre.services.checkin.getpassengerdata.v4.PassengerDataResponseACS;
import com.sabre.services.checkin.getpassengerdata.v4.PassengerDataResponseListACS;
import com.sabre.services.checkin.getpassengerdata.v4.PassengerItineraryListACS;
import com.sabre.services.checkin.updatesecurityinfo.v4.ActionType;
import com.sabre.services.checkin.updatesecurityinfo.v4.AddressInfoType;
import com.sabre.services.checkin.updatesecurityinfo.v4.AddressInfoType.Address;
import com.sabre.services.checkin.updatesecurityinfo.v4.ItineraryType;
import com.sabre.services.checkin.updatesecurityinfo.v4.OtherDocumentType;
import com.sabre.services.checkin.updatesecurityinfo.v4.OtherDocumentsInfoType;
import com.sabre.services.checkin.updatesecurityinfo.v4.PassengerContactInfoType.Phone;
import com.sabre.services.checkin.updatesecurityinfo.v4.PassengerDocumentsType;
import com.sabre.services.checkin.updatesecurityinfo.v4.PassengerDocumentsType.PassengerContactInfo;
import com.sabre.services.checkin.updatesecurityinfo.v4.PassengerInfoType;
import com.sabre.services.checkin.updatesecurityinfo.v4.PassengerSecurityInfoType;
import com.sabre.services.checkin.updatesecurityinfo.v4.SecurityChecksType;
import com.sabre.services.checkin.updatesecurityinfo.v4.SecurityChecksType.RequestTimatic;
import com.sabre.services.checkin.updatesecurityinfo.v4.TimaticComplexType;
import com.sabre.services.checkin.updatesecurityinfo.v4.TimaticDocType;
import com.sabre.services.checkin.updatesecurityinfo.v4.TimaticDocsType;
import com.sabre.services.checkin.updatesecurityinfo.v4.TimaticStayType;
import com.sabre.services.checkin.updatesecurityinfo.v4.TravelDocumentListType;
import com.sabre.services.checkin.updatesecurityinfo.v4.TravelDocumentType;
import com.sabre.services.checkin.updatesecurityinfo.v4.UpdateSecurityInfoRQType;
import com.sabre.services.checkin.updatesecurityinfo.v4.UpdateSecurityInfoRSType;
import com.sabre.services.checkin.updatesecurityinfo.v4.VisaType;
import com.sabre.services.sp.pd.v3_3.PassengerDetailsRS;
import com.sabre.services.stl.v3.ErrorOrSuccessCode;
import com.sabre.services.stl.v3.FreeTextInfoACS;
import com.sabre.services.stl.v3.PersonNamePlain;
import com.sabre.services.stl.v3.YNIndicator;
import com.sabre.webservices.pnrbuilder.updatereservation.ReservationUpdateListType;
import com.sabre.webservices.pnrbuilder.updatereservation.UpdateReservationRS;
import org.aeromexico.commons.exception.config.ReadProperties;
import org.apache.commons.lang.StringUtils;
import org.drools.javaparser.utils.Log;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Named
@ApplicationScoped
public class ShoppingCartService {

    private static final Logger LOG = LoggerFactory.getLogger(ShoppingCartService.class);

    private static final boolean INFANT = true;
    private static final boolean ADULT = !INFANT;

    @Inject
    private AMEditPassCharService editPassCharService;

    @Inject
    private AMPassengerSecurityService passengerSecurityService;

    @Inject
    private AMUpdateReservationService updateReservationService;

    @Inject
    private AMPassengerDetailsService passengerDetailsService;

    @Inject
    private CheckInService checkInService;

    @Inject
    private IPNRLookupDao pnrLookupDao;

    @Inject
    private SeatsService seatsService;

    @Inject
    private ShoppingCartAncillaries shoppingCartAncillaries;

    @Inject
    private PnrCollectionService updatePnrCollectionService;

    @Inject
    private AMFlightRemarksService amFlightRemarksService;

    @Inject
    private AMPassengerDataService passengerDataService;

    @Inject
    @TierBenefitsCache
    private ITierBenefitDao tierBenefitDao;

    @Inject
    @AirportsCache
    private IAirportCodesDao airportsCodesDao;

    @Inject
    private CovidRestrictionStorageService covidRestrictionStorageService;

    @Inject
    private com.aeromexico.profile.services.TierBenefitService tierBenefitService;

    @Inject
    private ReadProperties prop;

    private static final String[] TIMATIC_STAY_TYPES = {"Vacation", "Duty", "Business"};
    private static final String[] VISA_TYPES = {"Visiting Visa", "Residency Document"};

    @Inject
    private ShoppingCartDao shoppingCartDao;

    @Inject
    private SeamlessUpdatePassengerInfoService seamlessUpdatePassengerInfoService;

    @Inject
    @Named("CheckInSetUpConfigFactoryWrapper")
    private CheckInSetUpConfigFactoryWrapper setUpConfigFactory;

    public static List<String> USA_AIRPORTS;

    @PostConstruct
    public void init() {
        if (null == setUpConfigFactory) {
            setUpConfigFactory = new CheckInSetUpConfigFactoryWrapper();
        }
    }

    /**
     * @param bookedTraveler Remove all new ancillaries added to DB
     */
    private void removeAncillariesInDB(BookedTraveler bookedTraveler) {

        if (null == bookedTraveler.getAncillaries() || null == bookedTraveler.getAncillaries().getCollection() || bookedTraveler.getAncillaries().getCollection().isEmpty()) {
            return;
        }

        Iterator<AbstractAncillary> i = bookedTraveler.getAncillaries().getCollection().iterator();
        while (i.hasNext()) {
            // must be called before you can call i.remove()
            AbstractAncillary abstractAncillaryDB = i.next();

            if (abstractAncillaryDB instanceof TravelerAncillary) {
                TravelerAncillary travelerAncillary = (TravelerAncillary) abstractAncillaryDB;

                if (!travelerAncillary.isPartOfReservation()) {
                    i.remove();
                }
            } // if ancillary is not part of the reservation that must be an
            // unpaid ancillary (TravelerAncillary)
        }

    }

    /**
     * @param shoppingCartRQ
     * @param serviceCallList
     * @return
     * @throws Exception
     */
    public CartPNR cleanShoppingCart(ShoppingCartGETRQ shoppingCartRQ, List<ServiceCall> serviceCallList) throws Exception {

        Set<Integer> ancillaryIDs = new HashSet<>();

        String cartId = shoppingCartRQ.getCartId();

        // First read the pnrCollection from the database
        PNRCollection pnrCollection = pnrLookupDao.readPNRCollectionByCartId(cartId);

        // If the pnrCollection is null, throw an expired session exception
        if (null == pnrCollection) {
            LOG.error(CommonUtil.getMessageByCode(Constants.CODE_004) + shoppingCartRQ.getCartId());
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION,
                    CommonUtil.getMessageByCode(Constants.CODE_004));
        }

        if (null != pnrCollection.getStore() && !pnrCollection.getStore().trim().isEmpty()) {
            shoppingCartRQ.setStore(pnrCollection.getStore());
        }

        if (null != pnrCollection.getPos() && !pnrCollection.getPos().trim().isEmpty()) {
            shoppingCartRQ.setPos(pnrCollection.getPos());
        }

//		LOG.info("cleanShoppingCart pnrCollection( " + shoppingCartRQ.getCartId() + ") - " + pnrCollection);
        PNR pnr = pnrCollection.getPNRByCartId(cartId);

        CartPNR cartPNR = pnr.getCartPNRByCartId(shoppingCartRQ.getCartId());

        BookedLeg bookedLeg = pnr.getBookedLegByLegCode(cartPNR.getLegCode());

        List<BookedTraveler> bookedTravelerList = cartPNR.getTravelerInfo().getCollection();

        for (BookedTraveler bookedTraveler : bookedTravelerList) {
            try {
                seatsService.removeUnpaidSeatAncillaries(
                        bookedLeg, bookedTraveler,
                        pnr.getPnr(), cartId,
                        ancillaryIDs, serviceCallList, shoppingCartRQ.getPos()
                );
                removeAncillariesInDB(bookedTraveler);
            } catch (Exception ex) {
                // just log
                LOG.error(ex.getMessage());
            }
        }

        String currencyCode = cartPNR.getMeta().getTotal().getCurrency().getCurrencyCode();

        List<BookedTraveler> bookedTravelersSelectForCheckin = FilterPassengersUtil.getMarkedForCheckin(cartPNR.getTravelerInfo().getCollection());

        PnrCollectionUtil.updateTotal(cartPNR, bookedTravelersSelectForCheckin, currencyCode);

        updatePnrCollectionService.updatePnrCollection(pnrCollection, true);

        return cartPNR;
    }

    /**
     * @param currencyCode
     * @param bookedTravelersList
     * @return
     */
    private Total getTotalCartPNRForUpgrades(String currencyCode, List<BookedTraveler> bookedTravelersList) {

        Total total = new Total();
        total.setPoints(0);
        total.setCurrency(CurrencyUtil.getCurrencyValueUpgrade(currencyCode, bookedTravelersList));

        return total;
    }

    /**
     * @param currencyCode
     * @param bookedTravelersList
     * @return
     * @throws java.lang.Exception
     */
    private Total getTotalCartPNR(String currencyCode, List<BookedTraveler> bookedTravelersList) throws Exception {

        Total total = new Total();
        total.setPoints(0);
        total.setCurrency(CurrencyUtil.getCurrencyValue(currencyCode, bookedTravelersList));

        return total;
    }


    /**
     * @param shoppingCartRQ
     * @param serviceCallList
     * @return
     * @throws Exception
     */
    public CartPNR putShoppingCart(ShoppingCartRQ shoppingCartRQ, List<ServiceCall> serviceCallList) throws Exception {

        LOG.info("**putShoppingCart: {}", shoppingCartRQ);

        // First read the pnrCollection from the database
        PNRCollection pnrCollection = null;
        try {
            pnrCollection = pnrLookupDao.readPNRCollectionByCartId(shoppingCartRQ.getCartId());
        } catch (MongoDisconnectException me) {
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(shoppingCartRQ.getPos())) {
                LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
                throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getCode(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getActionable(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getMessage(),
                        Response.Status.INTERNAL_SERVER_ERROR);
            } else {
                LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
                throw me;
            }
        }

        // If the pnrCollection is null, throw an expired session exception
        if (null == pnrCollection || null == pnrCollection.getCollection() || pnrCollection.getCollection().isEmpty()) {
            LOG.error(CommonUtil.getMessageByCode(Constants.CODE_004) + "pnrCollection is null "
                    + shoppingCartRQ.getCartId());
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION,
                    CommonUtil.getMessageByCode(Constants.CODE_004));
        }

        if (null != pnrCollection.getStore() && !pnrCollection.getStore().trim().isEmpty()) {
            shoppingCartRQ.setStore(pnrCollection.getStore());
        }

        if (null != pnrCollection.getPos() && !pnrCollection.getPos().trim().isEmpty()) {
            shoppingCartRQ.setPos(pnrCollection.getPos());
        }

        String cartId = shoppingCartRQ.getCartId();
        String store = shoppingCartRQ.getStore();
        PNR pnr = pnrCollection.getPNRByCartId(cartId);

        if (null == pnr) {
            LOG.error(CommonUtil
                    .getMessageByCode(ErrorType.PNR_OBJECT_EMPTY.getFullDescription() + shoppingCartRQ.getCartId()));
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PNR_OBJECT_EMPTY,
                    ErrorType.PNR_OBJECT_EMPTY.getFullDescription());
        }

        shoppingCartRQ.setRecordLocator(pnr.getPnr());
        CartPNR cartPNR = pnrCollection.getCartPNRByCartId(cartId);

        if (null == cartPNR) {
            LOG.error(CommonUtil.getMessageByCode(
                    ErrorType.CART_COLLECTION_OBJECT_EMPTY.getFullDescription() + shoppingCartRQ.getCartId()));
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.CART_COLLECTION_OBJECT_EMPTY,
                    ErrorType.CART_COLLECTION_OBJECT_EMPTY.getErrorCode());
        } else if ( cartPNR.getWarnings() != null && cartPNR.getWarnings().getCollection() != null && cartPNR.getWarnings().getCollection().size() > 0 ) {
        	// Need to clear any old warnings from previous shopping carts put requests.
        	cartPNR.getWarnings().getCollection().clear();
        }


        CartPNRUpdate cartPNRUpdate = shoppingCartRQ.getCartPNRUpdate();

        cartPNR.getMeta().setNumberOfUpdates(cartPNR.getMeta().getNumberOfUpdates() + 1);

        //Start Save rateReference Hertz
        setCartRentalInfo(shoppingCartRQ, cartPNR);

        BookedLeg bookedLeg = pnr.getLegs().getBookedLegByLegCode(cartPNR.getLegCode());

        if (cartPNR.isSeamlessCheckin()) {//seamless checkin

            //Call seamless checkin flow
            seamlessUpdatePassengerInfoService.updatePassengerInfoInfo(shoppingCartRQ, cartPNR, cartPNRUpdate, bookedLeg);

        } else {//normal am flow

            List<BookedTraveler> bookedTravelerUpdateList = cartPNRUpdate.getTravelerInfo().getCollection();

            if ( PosType.KIOSK.toString().equalsIgnoreCase(shoppingCartRQ.getPos() ) ) {

            	// Clear previously selected passengers
                clearSelectedPassengers(cartPNR, bookedTravelerUpdateList);

            	markPassengersAsSelected(cartPNR, bookedTravelerUpdateList, pnr.getPnr());
            } else {
	            if (1 == cartPNR.getMeta().getNumberOfUpdates()) {
	                markPassengersAsSelected(cartPNR, bookedTravelerUpdateList, pnr.getPnr());
	            }
            }

            List<BookedTraveler> passengersNotSelectedForCheckin = bookedTravelerUpdateList.stream()
                    .filter(p -> !p.isSelectedToCheckin())
                    .collect(Collectors.toList());

            for (BookedTraveler bookedTraveler : passengersNotSelectedForCheckin) {
                shoppingCartAncillaries.removeUpgrade(bookedTraveler);
            }

            AncillaryOfferCollection ancillaryOfferCollectionOnDB = null;
            boolean hasAPhoneReservation = false;
            boolean hasAEmailReservation = false;
            boolean ffChanged = false;

            List<BookedTraveler> passengersSelectedForCheckin = bookedTravelerUpdateList.stream()
                    .filter(p -> p.isSelectedToCheckin())
                    .collect(Collectors.toList());

            for (BookedTraveler bookedTravelerUpdate : passengersSelectedForCheckin) {

                // Find bookedTraveler in DB
                BookedTraveler bookedTraveler = cartPNR.getBookedTravelerById(bookedTravelerUpdate.getId());

                LOG.info("**bookedTraveler: {}", bookedTraveler);

                if (bookedTraveler == null) {
                    LOG.error(ErrorType.TRAVELER_ID_NOT_MATCH.getFullDescription() + " PASSENGER ID : "
                            + bookedTravelerUpdate.getId() + " - PNR: " + pnrCollection.getCollection().get(0).getPnr());
                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.TRAVELER_ID_NOT_MATCH,
                            ErrorType.TRAVELER_ID_NOT_MATCH.getFullDescription() + bookedTravelerUpdate.getId() + " - PNR: "
                                    + pnrCollection.getCollection().get(0).getPnr());
                }

                if (!bookedTraveler.isCheckinStatus()) {
                    if ("WEB".equalsIgnoreCase(shoppingCartRQ.getPos())) {
                        bookedTraveler.setCovidRestrictionConfirmed(bookedTravelerUpdate.isCovidRestrictionConfirmed());
                    } else if (!bookedTraveler.isCovidRestrictionConfirmed()) {
                        bookedTraveler.setCovidRestrictionConfirmed(bookedTravelerUpdate.isCovidRestrictionConfirmed());
                    }
                }

                // If this is true we won't be able to sameData-in the passenger anyway
                if (bookedTraveler.getBookingClasses() == null || bookedTraveler.getBookingClasses().getCollection() == null
                        || bookedTraveler.getBookingClasses().getCollection().isEmpty()) {
                    LOG.error(ErrorCodeDescriptions.MSG_CODE_UNABLE_TO_FIND_BOOKINGCLASS_FIELD
                            + bookedTravelerUpdate.getId() + " - PNR: " + pnrCollection.getCollection().get(0).getPnr());
                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.NOT_EXIST_BOOKING_CLASS_IN_PASSENGER,
                            ErrorType.NOT_EXIST_BOOKING_CLASS_IN_PASSENGER.getFullDescription()
                                    + bookedTravelerUpdate.getId() + " - PNR :"
                                    + pnrCollection.getCollection().get(0).getPnr());
                }

                // Was the passenger FF info updated or added?
                boolean changed = updatedFrequentFlyerNumber(
                        bookedTraveler,
                        bookedTravelerUpdate,
                        pnrCollection.getPNRByCartId(shoppingCartRQ.getCartId()),
                        shoppingCartRQ.getCartId(),
                        cartPNR
                );
                ffChanged = ffChanged || changed;

                // Was the passenger CorporateFFN info updated or added?
                updatedCorporateFrequentFlyerNumber(bookedTraveler, bookedTravelerUpdate, pnrCollection.getPNRByCartId(shoppingCartRQ.getCartId()));

                boolean adultTravelerDocumentUpdated = false;
                boolean infantTravelerDocumentUpdated = false;

                boolean updatePassportWasIntended = false;
                boolean updateInfantPassportWasIntended = false;

                /**
                 * *
                 * The following update/changes apply only if the passenger is not
                 * checked in
                 */
                if (!bookedTraveler.isCheckinStatus()) {

                    // Was the passenger gender updated or added?
                    updatedGender(bookedTraveler, bookedTravelerUpdate);

                    // Was the DOB updated or added?
                    updatedDateOfBirth(bookedTraveler, bookedTravelerUpdate);

                    // Was the passenger country of Residence updated or added?
                    updatedCountryOfResidence(bookedTraveler, bookedTravelerUpdate, cartPNR.getLegCode(), pnrCollection.getPNRByCartId(shoppingCartRQ.getCartId()));

                    // Was the passenger destination address updated or added?
                    updatedDestinationAddress(bookedTraveler, bookedTravelerUpdate, cartPNR.getLegCode(), pnrCollection.getPNRByCartId(shoppingCartRQ.getCartId()));

                    // Is the traveler destination and residence country the same? in this case we remove MDA and MDAI
                    validateSameDestinationAndCountryResidence(bookedTraveler, bookedTravelerUpdate, cartPNR.getLegCode(), pnrCollection.getPNRByCartId(shoppingCartRQ.getCartId()));

                    // Was the passenger emergency contact info updated or added?
                    updatedEmergencyContact(bookedTraveler, bookedTravelerUpdate, cartPNR.getLegCode(), pnrCollection.getPNRByCartId(shoppingCartRQ.getCartId()));

                    // Was the traveler document updated or added?
                    if (null != bookedTravelerUpdate.getTravelDocument()
                            && (null == bookedTravelerUpdate.isUpdateTravelDocument()
                            || Boolean.TRUE.equals(bookedTravelerUpdate.isUpdateTravelDocument()))) {

                        if ((null == bookedTraveler.wasTravelDocumentUpdated()
                                || Boolean.FALSE.equals(bookedTraveler.wasTravelDocumentUpdated()))
                                && Boolean.TRUE.equals(bookedTravelerUpdate.isUpdateTravelDocument())) {
                            updatePassportWasIntended = true;
                        }

                        bookedTraveler.setTravelDocumentUpdated(true);

                        adultTravelerDocumentUpdated = updatedTravelerDocument(
                                bookedTraveler,
                                bookedTravelerUpdate,
                                pnrCollection.getPNRByCartId(shoppingCartRQ.getCartId()),
                                cartPNR.getLegCode()
                        );

                        if (SystemVariablesUtil.isTimaticEnabled() && adultTravelerDocumentUpdated) {
                            // If the passenger traveling to Havana (HAV), add the visa verified flag first, before requesting Timatic status
                            if (cartPNR.isToHava()) {
                                updateTimaticInfo(
                                        bookedTraveler,
                                        bookedTravelerUpdate,
                                        false,
                                        false,
                                        cartPNR,
                                        pnrCollection.getPNRByCartId(shoppingCartRQ.getCartId())
                                );
                            }

                            //Query the TIMATIC status after a passport info was added
                            setTimaticStatus(
                                    bookedTraveler,
                                    bookedTravelerUpdate,
                                    cartPNR.getLegCode(),
                                    pnrCollection.getPNRByCartId(shoppingCartRQ.getCartId()),
                                    cartPNR,
                                    ADULT
                            );
                        }
                    }

                    if (null != bookedTravelerUpdate.getInfant() && null != bookedTraveler.getInfant()) {

                        addGeneralInfantInfo(bookedTraveler, bookedTravelerUpdate);

                        if (null != bookedTravelerUpdate.getInfant().getTravelDocument()
                                && (null == bookedTravelerUpdate.getInfant().isUpdateTravelDocument()
                                || Boolean.TRUE.equals(bookedTravelerUpdate.getInfant().isUpdateTravelDocument()))) {

                            if ((null == bookedTraveler.getInfant().wasTravelDocumentUpdated()
                                    || Boolean.FALSE.equals(bookedTraveler.getInfant().wasTravelDocumentUpdated()))
                                    && Boolean.TRUE.equals(bookedTravelerUpdate.getInfant().isUpdateTravelDocument())) {
                                updateInfantPassportWasIntended = true;
                            }

                            bookedTraveler.getInfant().setTravelDocumentUpdated(true);

                            // Was the infant info updated or added?
                            infantTravelerDocumentUpdated = updatedInfantInfo(
                                    bookedTraveler,
                                    bookedTravelerUpdate,
                                    pnrCollection.getPNRByCartId(shoppingCartRQ.getCartId()),
                                    cartPNR.getLegCode()
                            );

                            if (SystemVariablesUtil.isTimaticEnabled() && infantTravelerDocumentUpdated) {
                                //Query the TIMATIC status for infant after passport info was added
                                setTimaticStatus(
                                        bookedTraveler,
                                        bookedTravelerUpdate,
                                        cartPNR.getLegCode(),
                                        pnrCollection.getPNRByCartId(shoppingCartRQ.getCartId()),
                                        cartPNR,
                                        INFANT
                                );
                            }
                        }
                    }

                    LOG.info("PnrPassId: {},  hasAPhoneReservation: {}", bookedTravelerUpdate.getNameRefNumber(), hasAPhoneReservation);
                    if (!hasAPhoneReservation) {
                        hasAPhoneReservation = savePhoneMissingInformation(pnr.getPnr(), bookedTravelerUpdate, bookedTraveler, cartPNR.getWarnings());
                    } else {
                        if (null != bookedTraveler.getMissingCheckinRequiredFields() && !bookedTraveler.getMissingCheckinRequiredFields().isEmpty()) {
                            if (bookedTraveler.getMissingCheckinRequiredFields().contains(CheckinIneligibleReasons.PHONE.toString())) {
                                PnrCollectionUtil.removeMissingInformation(bookedTraveler, CheckinIneligibleReasons.PHONE.toString());
                            }
                        }
                    }

                    LOG.info("PnrPassId: {},  hasAEmailReservation: {}", bookedTravelerUpdate.getNameRefNumber(), hasAEmailReservation);
                    if (!hasAEmailReservation) {
                        hasAEmailReservation = saveEmailMissingInformation(pnr.getPnr(), bookedTravelerUpdate, bookedTraveler, cartPNR.getWarnings());
                    } else {
                        if (null != bookedTraveler.getMissingCheckinRequiredFields() && !bookedTraveler.getMissingCheckinRequiredFields().isEmpty()) {
                            if (bookedTraveler.getMissingCheckinRequiredFields().contains(CheckinIneligibleReasons.EMAIL.toString())) {
                                PnrCollectionUtil.removeMissingInformation(bookedTraveler, CheckinIneligibleReasons.EMAIL.toString()
                                );
                            }
                        }
                    }
                }

                // Was there a seat selection or seat change?
                if (bookedTraveler.isCheckinStatus()) {

                    if (bookedTraveler.isChangeSeatAllowed()) {
                        // Was there a seat selection or seat change although the
                        // passenger has been checked?
                        seatsService.updateSeatChecked(
                                bookedTraveler,
                                bookedTravelerUpdate,
                                cartPNR.getLegCode(),
                                pnrCollection,
                                cartId,
                                shoppingCartRQ.getPos(),
                                serviceCallList
                        );
                    } else {
                        LOG.error("Seat change is not allowed for this passenger");
                        LOG.error("CHECKIN_WARNING: CODE: {} DESCRIPTION: {} PNR: {}", ErrorType.PAX_CANNOT_CHANGE_SEAT.getErrorCode(),ErrorType.PAX_CANNOT_CHANGE_SEAT.getFullDescription(),bookedTraveler.getPnrPassId());
                        Warning warning = new Warning(ErrorType.PAX_CANNOT_CHANGE_SEAT.getErrorCode(), ErrorType.PAX_CANNOT_CHANGE_SEAT.getFullDescription() + bookedTraveler.getPnrPassId());
                        cartPNR.getWarnings().addToList(warning);
                    }

                    // Was there a upgrade although the passenger has been checked?
                    shoppingCartAncillaries.addUpgradeChecked(bookedTraveler, bookedTravelerUpdate, pnrCollection, cartId);

                } else {
                    if (bookedTraveler.isChangeSeatAllowed()) {
                        seatsService.updateSeat(
                                bookedTraveler, bookedTravelerUpdate,
                                cartPNR.getLegCode(), pnrCollection, cartId, store,
                                cartPNR.getWarnings(), serviceCallList, shoppingCartRQ.getPos()
                        );
                    } else {
                        LOG.error("Seat change is not allowed for this passenger");
                        LOG.error("CHECKIN_WARNING: CODE: {} DESCRIPTION: {} PNR: {}", ErrorType.PAX_CANNOT_CHANGE_SEAT.getErrorCode(),ErrorType.PAX_CANNOT_CHANGE_SEAT.getFullDescription(),bookedTraveler.getPnrPassId());
                        Warning warning = new Warning(ErrorType.PAX_CANNOT_CHANGE_SEAT.getErrorCode(), ErrorType.PAX_CANNOT_CHANGE_SEAT.getFullDescription() + bookedTraveler.getPnrPassId());
                        cartPNR.getWarnings().addToList(warning);
                    }
                }

                if (bookedTraveler.isCheckinStatus()) {
                    // Was this an over-booking offer request?
                    if (null != bookedTravelerUpdate.getOverBookingInfo()
                            && null != bookedTravelerUpdate.getOverBookingInfo().getAmount()) {
                        updatedOverBookingInfo(
                                bookedTraveler,
                                bookedTravelerUpdate,
                                cartPNR.getLegCode(),
                                pnrCollection.getPNRByCartId(shoppingCartRQ.getCartId()),
                                cartPNR.getWarnings(), shoppingCartRQ
                        );
                    }
                }

                // Was there an ancillary selection or bag added?
                shoppingCartAncillaries.updateAncillary(
                        bookedTraveler,
                        bookedTravelerUpdate,
                        pnrCollection,
                        cartId,
                        shoppingCartRQ,
                        ancillaryOfferCollectionOnDB,
                        cartPNR.getWarnings()
                );

                // Was there a KTN added or updated?
                updatedKTN(
                        bookedTraveler,
                        bookedTravelerUpdate,
                        pnrCollection.getPNRByCartId(shoppingCartRQ.getCartId()),
                        cartPNR.getWarnings()
                );

                // Was there a redress number added or updated?
                updatedRedress(bookedTraveler, bookedTravelerUpdate, cartPNR.getLegCode(), pnrCollection.getPNRByCartId(shoppingCartRQ.getCartId()));

                updateVisaInfo(bookedTraveler, bookedTravelerUpdate, cartPNR.getLegCode(), pnrCollection.getPNRByCartId(shoppingCartRQ.getCartId()), cartPNR);

                // Is there TIMATIC info in the shopping cart PUT request to add or update?
                updateTimaticInfo(
                        bookedTraveler,
                        bookedTravelerUpdate,
                        updatePassportWasIntended,
                        updateInfantPassportWasIntended,
                        cartPNR,
                        pnrCollection.getPNRByCartId(shoppingCartRQ.getCartId())
                );


                // Double check a case where we have passport info added through other channels and no TIMATIC info were requested or captured.
                if (SystemVariablesUtil.isTimaticEnabled() && !bookedTraveler.isCheckinStatus()) {
                    if (null != bookedTraveler.getTravelDocument()
                            && (null == bookedTraveler.getTimaticInfo()
                            || null == bookedTraveler.getTimaticInfo().getStatus())) {
                        setTimaticStatus(bookedTraveler, bookedTravelerUpdate, cartPNR.getLegCode(), pnrCollection.getPNRByCartId(shoppingCartRQ.getCartId()), cartPNR, ADULT);
                    }

                    // Infant
                    if (null != bookedTraveler.getInfant()
                            && null != bookedTraveler.getInfant().getTravelDocument()
                            && (null == bookedTraveler.getInfant().getTimaticInfo()
                            || null == bookedTraveler.getInfant().getTimaticInfo().getStatus())
                    ) {
                        setTimaticStatus(bookedTraveler, bookedTravelerUpdate, cartPNR.getLegCode(), pnrCollection.getPNRByCartId(shoppingCartRQ.getCartId()), cartPNR, INFANT);
                    }
                }

            } // done updating each selected passenger

            if (ffChanged) {
                PriorityUtil.setRevenuePriorityCode(cartPNR.getTravelerInfo().getCollection());
            }
        }

        String currencyCode = cartPNR.getMeta().getTotal().getCurrency().getCurrencyCode();
        List<BookedTraveler> selectedTravelers = FilterPassengersUtil.getMarkedForCheckin(cartPNR.getTravelerInfo().getCollection());
        List<BookedTraveler> bookedTravelersWithUpgrade = FilterPassengersUtil.getPassengersWithUpgrade(selectedTravelers);
        boolean isThereUpgrades = !bookedTravelersWithUpgrade.isEmpty();

        covidRestrictionStorageService.saveCovidRestrictionForm(pnr.getPnr(), cartPNR, bookedLeg);

        if (isThereUpgrades) {
            cartPNR.getMeta().setTotal(getTotalCartPNRForUpgrades(currencyCode, bookedTravelersWithUpgrade));
            updatePnrCollectionService.updatePnrCollection(pnrCollection, false);

            // if exist Unpaid Ancillaries and Unpaid Seat the total will show
            // it.
            CurrencyValue totalAux = CurrencyUtil.getCurrencyValue(currencyCode, bookedTravelersWithUpgrade);
            if (0 != totalAux.getTotal().compareTo(BigDecimal.ZERO)) {
                ShoppingCartUtil.removeUnpaidAncillariesAndUnpaidSeat(bookedTravelersWithUpgrade);
            }
        } else {
            cartPNR.getMeta().setTotal(getTotalCartPNR(currencyCode, selectedTravelers));
            updatePnrCollectionService.updatePnrCollection(pnrCollection, false);
        }

        cartPNR.getMeta().getTotal().getCurrency().setTaxDetails(
                CurrencyUtil.fixTaxDescriptions(
                        cartPNR.getMeta().getTotal().getCurrency().getTaxDetails(),
                        shoppingCartRQ.getLanguage(),
                        store
                )
        );

        return cartPNR;
    }

    /**
     * When a shopping cart request is made to update/change the selected passenger(s) for check-in, the previously selected
     * passenger(s) need to be de-selected or cleared, otherwise the shopping cart total will be incorrectly calculated.
     *
     * @param cartPNR
     * @param bookedTravelerUpdateList
     *
     * @throws Exception
     */
    private void clearSelectedPassengers(CartPNR cartPNR, List<BookedTraveler> bookedTravelerUpdateList) throws Exception {

    	for ( BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection() ) {
    		bookedTraveler.setIsSelectedToCheckin(false);
    	}
    }

    public void markPassengersAsSelected(CartPNR cartPNR, List<BookedTraveler> bookedTravelerUpdateList, String pnr) throws Exception {

        cartPNR.getMeta().setPassengersSelected(true);

        //mark passengers as selected
        for (BookedTraveler bookedTravelerUpdate : bookedTravelerUpdateList) {
            // Find bookedTraveler in DB
            BookedTraveler bookedTraveler = cartPNR.getBookedTravelerById(bookedTravelerUpdate.getId());

            LOG.info("**bookedTraveler: {}", bookedTraveler);

            if (bookedTraveler == null) {
                LOG.error(ErrorType.TRAVELER_ID_NOT_MATCH.getFullDescription() + " PASSENGER ID : "
                        + bookedTravelerUpdate.getId() + " - PNR: " + pnr);
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.TRAVELER_ID_NOT_MATCH,
                        ErrorType.TRAVELER_ID_NOT_MATCH.getFullDescription() + bookedTravelerUpdate.getId() + " - PNR: "
                                + pnr);
            }

            // Was the passenger selected for sameData-in or a change after sameData-in?
            bookedTraveler.setIsSelectedToCheckin(bookedTravelerUpdate.isSelectedToCheckin());
        }
    }

    public boolean getTypeAdmin(List<AbstractAncillary> bookedTraveler){
        boolean flag = false;
            if(bookedTraveler instanceof PaidTravelerAncillary){
                flag = true;
            }
        return flag;
    }

    public void setCartRentalInfo(ShoppingCartRQ shoppingCartRQ, CartPNR cartPNR) throws ClassNotFoundException, IOException {
        if (null == shoppingCartRQ.getCartPNRUpdate().getCar()
                || null == shoppingCartRQ.getCartPNRUpdate().getCar().getRateReference()
                || shoppingCartRQ.getCartPNRUpdate().getCar().getRateReference().isEmpty()) {

            cartPNR.setCar(null);
            cartPNR.setCarReference(null);

            try {
                shoppingCartDao.deleteRateReferenceMongoDoc(shoppingCartRQ.getCartId());
            } catch (MongoDisconnectException me) {
                if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(shoppingCartRQ.getPos())) {
                    LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
                } else {
                    LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
                    throw me;
                }
            } catch (Exception ex) {
                LOG.error(ex.getMessage(), ex);
            }

        } else if (null == cartPNR.getCar()
                || null == cartPNR.getCar().getPickUpBranch()
                || null == cartPNR.getCar().getPickUpBranch().getCar()
                || !shoppingCartRQ.getCartPNRUpdate().getCar().getRateReference().equals(cartPNR.getCarReference())) {

            boolean hertzReserved = false;

            try {

                String response = callServiceHertzRates(shoppingCartRQ.getCartPNRUpdate().getCar().getRateReference(), shoppingCartRQ.getLanguage());

                if (response.contains("warning")) {
                    LOG.error("CHECKIN_WARNING: CODE: {} DESCRIPTION: {} ",ErrorCodeType.CAR_RATE_DETAIL_FAILED.getErrorCode(),ErrorCodeType.CAR_RATE_DETAIL_FAILED.getValue());
                    Warning warningHertz = new Warning(
                            ErrorCodeType.CAR_RATE_DETAIL_FAILED.getErrorCode(),
                            ErrorCodeType.CAR_RATE_DETAIL_FAILED.getValue(),
                            new ExtraInfo(response)
                    );

                    cartPNR.getWarnings().getCollection().add(warningHertz);

                    cartPNR.setCar(null);
                    cartPNR.setCarReference(null);
                } else {
                    CarRateDetailsResponse carRateDetailsResponse = new Gson().fromJson(response, CarRateDetailsResponse.class);

                    if (carRateDetailsResponse != null) {
                        if (shoppingCartRQ.getCartPNRUpdate().getCar().getDiscount() != null
                                && BigDecimal.ZERO.compareTo(shoppingCartRQ.getCartPNRUpdate().getCar().getDiscount()) == -1) {

                            carRateDetailsResponse.getPickUpBranch().getCar().setDiscount(
                                    shoppingCartRQ.getCartPNRUpdate().getCar().getDiscount()
                            );
                        }

                        cartPNR.setCar(carRateDetailsResponse);
                        cartPNR.setCarReference(shoppingCartRQ.getCartPNRUpdate().getCar().getRateReference());

                        hertzReserved = true;
                    } else {
                        LOG.error("CHECKIN_WARNING: CODE: {} DESCRIPTION: {} ",ErrorCodeType.CAR_RATE_DETAIL_FAILED.getErrorCode(),ErrorCodeType.CAR_RATE_DETAIL_FAILED.getValue());
                        Warning warningHertz = new Warning(ErrorCodeType.CAR_RATE_DETAIL_FAILED.getErrorCode(), ErrorCodeType.CAR_RATE_DETAIL_FAILED.getValue());

                        cartPNR.getWarnings().getCollection().add(warningHertz);

                        cartPNR.setCar(null);
                        cartPNR.setCarReference(null);
                    }

                }

            } catch (Exception e) {
                LOG.error(e.getMessage(), e);
                LOG.error("CHECKIN_WARNING: CODE: {} DESCRIPTION: {} ",ErrorCodeType.CAR_RATE_DETAIL_FAILED.getErrorCode(),ErrorCodeType.CAR_RATE_DETAIL_FAILED.getValue());
                Warning warningHertz = new Warning(
                        ErrorCodeType.CAR_RATE_DETAIL_FAILED.getErrorCode(),
                        ErrorCodeType.CAR_RATE_DETAIL_FAILED.getValue(),
                        new ExtraInfo(e.getMessage())
                );

                cartPNR.getWarnings().getCollection().add(warningHertz);

                cartPNR.setCar(null);
                cartPNR.setCarReference(null);
            }

            if (hertzReserved) {
                RateReferenceMongo rateReferenceMongo;
                if (shoppingCartRQ.getCartPNRUpdate().getCar().getDiscount() != null
                        && BigDecimal.ZERO.compareTo(shoppingCartRQ.getCartPNRUpdate().getCar().getDiscount()) == -1) {

                    rateReferenceMongo = new RateReferenceMongo(
                            shoppingCartRQ.getCartId(),
                            shoppingCartRQ.getCartPNRUpdate().getCar().getRateReference(),
                            shoppingCartRQ.getCartPNRUpdate().getCar().getDiscount()
                    );

                } else {

                    rateReferenceMongo = new RateReferenceMongo(
                            shoppingCartRQ.getCartId(),
                            shoppingCartRQ.getCartPNRUpdate().getCar().getRateReference()
                    );
                }

                try {

                    boolean saved = shoppingCartDao.saveRateReferenceMongoDoc(
                            rateReferenceMongo
                    );

//                    if (!saved) {
//                        Warning warningHertz = new Warning(ErrorCodeType.CAR_RATE_DETAIL_FAILED.getErrorCode(), ErrorCodeType.CAR_RATE_DETAIL_FAILED.getValue());
//
//                        cartPNR.getWarnings().getCollection().add(warningHertz);
//
//                        cartPNR.setCar(null);
//                        cartPNR.setCarReference(null);
//                    }
                } catch (MongoDisconnectException me) {
                    if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(shoppingCartRQ.getPos())) {
                        LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
                    } else {
                        LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
                        throw me;
                    }
                } catch (Exception ex) {
                    Warning warningHertz = new Warning(ErrorCodeType.CAR_RATE_DETAIL_FAILED.getErrorCode(), ErrorCodeType.CAR_RATE_DETAIL_FAILED.getValue());

                    cartPNR.getWarnings().getCollection().add(warningHertz);

                    cartPNR.setCar(null);
                    cartPNR.setCarReference(null);
                }

            }

        } // Ending Insert Hertz Reservation into ShoppingCart
    }

    private void updateVisaInfo(BookedTraveler bookedTraveler, BookedTraveler bookedTravelerUpdate, String bookedLegCode, PNR pnr, CartPNR cartPNR) throws Exception {

        // This condition should never happen
        if (bookedTraveler == null || bookedTravelerUpdate == null) {
            return;
        }

        // Is there Visa info in the shopping cart PUT request for the adult passenger?
        if (bookedTravelerUpdate.getVisaInfo() != null) {

            if (bookedTravelerUpdate.getVisaInfo().getType() != null) {
                if (bookedTravelerUpdate.getVisaInfo().getType().equalsIgnoreCase(VISA_TYPES[1])) {
                    // If the VISA type is a Residence Document, send it to Timatic
                    if (bookedTravelerUpdate.getTimaticInfo() == null) {
                        bookedTravelerUpdate.setTimaticInfo(new TimaticInfo());
                    }

                    bookedTravelerUpdate.getTimaticInfo().setResidencyCountry(bookedTravelerUpdate.getVisaInfo().getApplicableCountryCode());
                    // 4 = PERMANENT RESIDENT ALIEN CARD FORM I 551
                    bookedTravelerUpdate.getTimaticInfo().setResidencyDocumentType("4");
                }
            }

            if (bookedTraveler.getVisaInfo() == null) {
                //The shopping cart PUT request has Visa info and the there are not existing visa info in the passenger record to compare with. So, add it...
                updateVisaInfoForPassenger(bookedTraveler, bookedTravelerUpdate, bookedLegCode, pnr, cartPNR, ADULT);
            } else if (!bookedTraveler.getVisaInfo().equals(bookedTravelerUpdate.getVisaInfo())) {//Compare visa data from the shopping cart PUT request with the current visa data. If the info is not different, return; else add it.
                //The visa info was changed/updated. So, update it.
                updateVisaInfoForPassenger(bookedTraveler, bookedTravelerUpdate, bookedLegCode, pnr, cartPNR, ADULT);

            }
        }

        // This condition should never happen
        if (bookedTravelerUpdate.getInfant() == null || bookedTravelerUpdate.getInfant().getVisaInfo() == null) {
            // No infant info in the shopping cart request. Return.
            return;
        }

        //This should not happen:
        // Trying to adding infant visa where there is no infant traveling with this booked traveler!!
        if (bookedTraveler.getInfant() == null) {
            // No infant in the PNR. Invalid request.
            return;
        }

        // Is there Visa info in the shopping cart PUT request for the infant passenger?
        if (bookedTraveler.getInfant().getVisaInfo() == null) {
            //The shopping cart PUT request has Visa info and the there are not existing visa info in the passenger record to compare with. So, add it...
            updateVisaInfoForPassenger(bookedTraveler, bookedTravelerUpdate, bookedLegCode, pnr, cartPNR, INFANT);
        } else {
            //Compare visa data from the shopping cart PUT request with the current visa data. If the info is not different, return; else add it.
            if (!bookedTraveler.getInfant().getVisaInfo().equals(bookedTravelerUpdate.getInfant().getVisaInfo())) {
                //The visa info was changed/updated. So, update it.
                updateVisaInfoForPassenger(bookedTraveler, bookedTravelerUpdate, bookedLegCode, pnr, cartPNR, INFANT);
            }
        }
    }

    private void setTimaticStatus(
            BookedTraveler bookedTraveler,
            BookedTraveler bookedTravelerUpdate,
            String bookedLegCode,
            PNR pnr,
            CartPNR cartPNR,
            boolean isInfant
    ) throws Exception {

        // This conditions should never happen
        if (bookedTraveler == null || bookedTravelerUpdate == null) {
            return;
        }

        //If this request is for an infant, make sure that we have infant info
        if (isInfant) {
            // This conditions should never happen
            if (bookedTraveler.getInfant() == null) {
                return;
            }
        }

        getTimaticStatus(bookedTraveler, bookedTravelerUpdate, bookedLegCode, pnr, cartPNR, isInfant);

    }

    private void updateTimaticInfo(
            BookedTraveler bookedTraveler,
            BookedTraveler bookedTravelerUpdate,
            boolean updatePassportWasIntended,
            boolean updateInfantPassportWasIntended,
            CartPNR cartPNR,
            PNR pnr
    ) throws Exception {

        // This conditions should never happen
        if (bookedTraveler == null || bookedTravelerUpdate == null) {
            return;
        }

        if (bookedTraveler.isCheckinStatus()) {
            LOG.info("NOT updateTimaticInfo for {}", bookedTraveler.getId());
            return;
        }

        if ((!updatePassportWasIntended && Boolean.TRUE.equals(bookedTraveler.wasTravelDocumentUpdated()))) {

            //
            // Adult
            //
            // Is there TIMATIC info in the shopping cart PUT request for the adult passenger?
            if (bookedTravelerUpdate.getTimaticInfo() == null && cartPNR.isToHava()) {

                LOG.info("updateTimaticInfo for an Adult to Havana {}", bookedTraveler.getId());

                bookedTravelerUpdate.setTimaticInfo(new TimaticInfo());
                bookedTravelerUpdate.getTimaticInfo().setVisaVerified(true);

                if (bookedTraveler.getTimaticInfo() == null) {
                    bookedTraveler.setTimaticInfo(new TimaticInfo());
                }

                bookedTraveler.getTimaticInfo().setVisaVerified(true);

            } else if (bookedTravelerUpdate.getTimaticInfo() != null) {

                LOG.info("updateTimaticInfo for an Adult {}", bookedTraveler.getId());

                if (cartPNR.isToHava()) {
                    if (bookedTraveler.getTimaticInfo() == null) {
                        bookedTraveler.setTimaticInfo(new TimaticInfo());
                    }
                    bookedTraveler.getTimaticInfo().setVisaVerified(true);
                    bookedTravelerUpdate.getTimaticInfo().setVisaVerified(true);
                }

                boolean etaSent = false;

                //Does the passenger have an eTA (Electronic Travel Authorization)?
                if (bookedTravelerUpdate.getTimaticInfo().hasETA()) {

                    if (bookedTraveler.getTimaticInfo() == null) {
                        bookedTraveler.setTimaticInfo(new TimaticInfo());
                    }

                    bookedTraveler.getTimaticInfo().setVisaVerified(true);
                    bookedTravelerUpdate.getTimaticInfo().setVisaVerified(true);

                    etaSent = true;

                    bookedTraveler.setEtaAlreadySent(true);
                    bookedTravelerUpdate.setEtaAlreadySent(true);
                }


                if (bookedTraveler.getTimaticInfo() == null) {
                    //The shopping cart PUT request has TIMATIC info. So, add it...
                    LOG.debug("Adding new Timatic info");
                    updateTimaticInfoForPassenger(bookedTraveler, bookedTravelerUpdate, cartPNR.getLegCode(), pnr, cartPNR, ADULT);
                } else if (!bookedTraveler.getTimaticInfo().equals(bookedTravelerUpdate.getTimaticInfo())) { //Compare TIMATIC data from the shopping cart PUT request with the current Timatic data. If the info is not different, return. Else, add it.
                    //The TIMATIC info was changed/updated. So, update it.
                    LOG.debug("Updating existing Timatic info");
                    updateTimaticInfoForPassenger(bookedTraveler, bookedTravelerUpdate, cartPNR.getLegCode(), pnr, cartPNR, ADULT);
                }

                if (bookedTraveler.getTimaticInfo() == null) {
                    bookedTraveler.setTimaticInfo(new TimaticInfo());
                }
                bookedTraveler.setTimaticProvided(true);

                //ADULT recidencyCountry
                /*if (null != bookedTraveler.getTimaticInfo()) {
                    if(null != bookedTraveler.getTimaticInfo().getResidencyCountry()){
                        String residencyInfant = bookedTraveler.getTimaticInfo().getResidencyCountry();
                        bookedTravelerUpdate.getTimaticInfo().setResidencyCountry(residencyInfant);
                    }
                }*/

                if (bookedTraveler.getTimaticInfo() != null
                        && etaSent
                        && !"OK TO BOARD".equalsIgnoreCase(bookedTraveler.getTimaticInfo().getStatus())) {
                    if (!bookedTraveler.isReturningDateAlreadySent()) {
                        bookedTraveler.getTimaticInfo().setReturningDateRequired(true);
                    }
                }
            }
        }

        if (bookedTraveler.getInfant() != null
                && bookedTravelerUpdate.getInfant() != null
                && !updateInfantPassportWasIntended
                && null != bookedTraveler.getInfant()
                && Boolean.TRUE.equals(bookedTraveler.getInfant().wasTravelDocumentUpdated())) {
            //
            // Infant
            //
            // Is there an infant traveler with the adult passenger?
            if (bookedTravelerUpdate.getInfant().getTimaticInfo() == null && cartPNR.isToHava()) {

                LOG.info("updateTimaticInfo for an Infant to Havana {}", bookedTraveler.getId());

                bookedTravelerUpdate.getInfant().setTimaticInfo(new TimaticInfo());

                bookedTravelerUpdate.getInfant().getTimaticInfo().setVisaVerified(true);

                if (bookedTraveler.getInfant().getTimaticInfo() == null) {
                    bookedTraveler.getInfant().setTimaticInfo(new TimaticInfo());
                    bookedTraveler.getInfant().getTimaticInfo().setVisaVerified(true);
                }

            } else if (bookedTravelerUpdate.getInfant().getTimaticInfo() != null) {

                LOG.info("updateTimaticInfo for an Infant {}", bookedTraveler.getId());

                if (cartPNR.isToHava()) {
                    if (bookedTraveler.getInfant().getTimaticInfo() == null) {
                        bookedTraveler.getInfant().setTimaticInfo(new TimaticInfo());
                    }
                    bookedTraveler.getInfant().getTimaticInfo().setVisaVerified(true);
                    bookedTravelerUpdate.getInfant().getTimaticInfo().setVisaVerified(true);
                }

                boolean etaSent = false;

                if (bookedTravelerUpdate.getInfant().getTimaticInfo().hasETA()) {
                    if (bookedTraveler.getInfant().getTimaticInfo() == null) {
                        bookedTraveler.getInfant().setTimaticInfo(new TimaticInfo());
                    }
                    bookedTraveler.getInfant().getTimaticInfo().setVisaVerified(true);
                    bookedTravelerUpdate.getInfant().getTimaticInfo().setVisaVerified(true);

                    bookedTraveler.getInfant().setEtaAlreadySent(true);
                    bookedTravelerUpdate.getInfant().setEtaAlreadySent(true);

                    etaSent = true;
                }

                if (bookedTraveler.getInfant().getTimaticInfo() == null) {
                    LOG.debug("Adding new Timatic info for infant");
                    //The shopping cart PUT request has TIMATIC info. So, add it...
                    updateTimaticInfoForPassenger(bookedTraveler, bookedTravelerUpdate, cartPNR.getLegCode(), pnr, cartPNR, INFANT);
                } else {
                    //Compare TIMATIC data from the shopping cart PUT request with the current Timatic data. If the info is not different, return. Else, add it.
                    if (!bookedTraveler.getTimaticInfo().equals(bookedTravelerUpdate.getTimaticInfo())) {
                        LOG.debug("Updating existing Timatic info for infant");
                        //The TIMATIC info was changed/updated. So, update it.
                        updateTimaticInfoForPassenger(bookedTraveler, bookedTravelerUpdate, cartPNR.getLegCode(), pnr, cartPNR, INFANT);
                    }
                }

                if (bookedTraveler.getInfant().getTimaticInfo() == null) {
                    bookedTraveler.getInfant().setTimaticInfo(new TimaticInfo());
                }
                bookedTraveler.getInfant().setTimaticProvided(true);
		//infant recidencyCountry
                if (null != bookedTraveler.getInfant().getTimaticInfo()) {
                    if(null != bookedTraveler.getInfant().getTimaticInfo().getResidencyCountry()){
                        String residencyInfant = bookedTraveler.getInfant().getTimaticInfo().getResidencyCountry();
                        bookedTravelerUpdate.getInfant().getTimaticInfo().setResidencyCountry(residencyInfant);
                    }
                }

                if (bookedTraveler.getInfant() != null
                        && bookedTraveler.getInfant().getTimaticInfo() != null
                        && etaSent
                        && !"OK TO BOARD".equalsIgnoreCase(bookedTraveler.getInfant().getTimaticInfo().getStatus())) {
                    if (!bookedTraveler.getInfant().isReturningDateAlreadySent()) {
                        bookedTraveler.getInfant().getTimaticInfo().setReturningDateRequired(true);
                    }
                }
            }
        }
    }

    /**
     * @param bookedTraveler
     * @param bookedTravelerUpdate
     * @param bookedLegCode
     * @param pnr
     * @throws Exception
     */
    private void getTimaticStatus(
            BookedTraveler bookedTraveler,
            BookedTraveler bookedTravelerUpdate,
            String bookedLegCode,
            PNR pnr,
            CartPNR cartPNR,
            boolean isInfant
    ) throws Exception {

        LOG.info("getTimaticStatus(pax id={}, pnr={}, isInfant={})", bookedTraveler.getId(), pnr, isInfant);

        BookedSegment bookedSegment = pnr.getFirstBookedSegmentOnTime(bookedLegCode);

        if (null == bookedSegment || null == bookedSegment.getSegment()) {
            LOG.error("CHECKIN_ERROR: DESCRIPTION: {} LEGCODE: {}  PNR: {}",ErrorCodeDescriptions.MSG_CODE_BOOKEDSEGMENT_NOT_FOUND,bookedLegCode,pnr.getPnr());
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.BOOKEDSEGMENT_NOT_FOUND, ErrorType.BOOKEDSEGMENT_NOT_FOUND.getFullDescription() + " LegCode: " + bookedLegCode + " - PNR: " + pnr.getPnr());
        }

        BookingClass bookingClass = ShoppingCartUtil.getBookingClassMap(bookedTraveler, bookedSegment.getSegment().getSegmentCode());

        if (null == bookingClass) {
            LOG.error("CHECKIN_ERROR: DESCRIPTION: {} PASSENGER ID: {} PNR: {}",ErrorCodeDescriptions.MSG_CODE_ERROR_NOT_FOUND_BOOKINGCLASS_FOR_SEGMENTCODE_IN_SEGMENT+bookedSegment.getSegment().getSegmentCode(), bookedTraveler.getId(), pnr.getPnr());
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.NOT_EXIST_BOOKING_CLASS_IN_PASSENGER, ErrorType.NOT_EXIST_BOOKING_CLASS_IN_PASSENGER.getFullDescription() + " FOR SEGMENTCODE: " + bookedSegment.getSegment().getSegmentCode() + " PASSENGER ID: " + bookedTraveler.getId() + " - PNR :" + pnr.getPnr());
        }

        if (!getTimaticStatus(bookedSegment.getSegment(), bookedTraveler, bookedTravelerUpdate, bookingClass, cartPNR, isInfant)) {
            //Failed to get TIMATIC status
            LOG.error("CHECKIN_ERROR: Failed to get passenger TIMATIC status ");
        }
    }

    private boolean getTimaticStatus(
            Segment segment,
            BookedTraveler bookedTraveler,
            BookedTraveler bookedTravelerUpdate,
            BookingClass bookingClass,
            CartPNR cartPNR,
            boolean isInfant
    ) throws Exception {

        LOG.info("getTimaticStatus(segment={}, bookedTraveler={}, bookedTravelerUpdateg={}, isInfant={} );", segment.getSegmentCode(), bookedTraveler.getId(), bookedTravelerUpdate.getId(), isInfant);

        UpdateSecurityInfoRQType updateSecurityInfoRQ = getPassengerSecurityRQTimaticStatus(segment, bookingClass, bookedTraveler, isInfant);
        UpdateSecurityInfoRSType updateSecurityInfoRS = passengerSecurityService.passengerSecurity(updateSecurityInfoRQ);

        if (ErrorOrSuccessCode.SUCCESS.equals(updateSecurityInfoRS.getResult().getStatus())) {

            //Process TIMATIC status response for infant
            if (isInfant) {
                LOG.info("getTimaticStatus: success TIMATIC Infant update");
                processTimaticResponseForInfant(segment, updateSecurityInfoRS, bookedTraveler, bookedTravelerUpdate);
            } else {
                LOG.info("getTimaticStatus: success TIMATIC Adult update");
                processTimaticResponseForAdult(segment, updateSecurityInfoRS, bookedTraveler, bookedTravelerUpdate);
            }

            return true;
        } else {

            Log.error("CHECKIN_ERROR: getTimaticStatus updateSecurityInfoRS ERROR: {}", updateSecurityInfoRS);

            if (null != updateSecurityInfoRS.getResult() && null != updateSecurityInfoRS.getResult().getSystemSpecificResults() &&
                    !updateSecurityInfoRS.getResult().getSystemSpecificResults().isEmpty()) {

                // Was there a TIMATIC communication issue?
                /*
                 * <ns2:ErrorMessage code="5729">UNABLE TO CONTACT TIMATIC</ns2:ErrorMessage>
                 */
                if (updateSecurityInfoRS.getResult().getSystemSpecificResults().get(0).getErrorMessage().getCode().intValue() == 5729) {

                    Log.error("CHECKIN_ERROR: getTimaticStatus updateSecurityInfoRS ERROR: {}", updateSecurityInfoRS.getResult().getSystemSpecificResults().get(0).getErrorMessage().getValue());

                    Warning warning = new Warning();
                    warning.setCode(ErrorType.UNABLE_TO_CONTACT_TIMATIC.getErrorCode());
                    warning.setMsg(ErrorType.UNABLE_TO_CONTACT_TIMATIC.getFullDescription());
                    cartPNR.getWarnings().addToList(warning);

                } else {

                    Log.error("CHECKIN_ERROR: getTimaticStatus updateSecurityInfoRS ERROR: {}", updateSecurityInfoRS.getResult().getSystemSpecificResults().get(0).getErrorMessage().getValue());

                    for (com.sabre.services.stl.v3.SystemSpecificResults systemSpecificResults : updateSecurityInfoRS.getResult().getSystemSpecificResults()) {
                        if ("TIM-CONDITIONAL".equalsIgnoreCase(systemSpecificResults.getErrorMessage().getValue())
                                || "CONDITIONAL".equalsIgnoreCase(systemSpecificResults.getErrorMessage().getValue())
                                || "INF CONDITIONAL".equalsIgnoreCase(systemSpecificResults.getErrorMessage().getValue())
                                || "TIM-INF CONDITIONAL".equalsIgnoreCase(systemSpecificResults.getErrorMessage().getValue())
                                || "NOT OK TO BOARD".equalsIgnoreCase(systemSpecificResults.getErrorMessage().getValue())
                                || "TIM-NOT OK TO BOARD".equalsIgnoreCase(systemSpecificResults.getErrorMessage().getValue())) {

                            processConditionalTimaticResponse(segment, bookedTraveler, isInfant);

                            return true;
                        }
                    }
                }
            }
        }

        //When will enter here?
        // PnrCollectionUtil.removeTimaticIneligible(bookedTraveler);

        return false;

    }

    private void processTimaticResponseForInfant(Segment segment, UpdateSecurityInfoRSType updateSecurityInfoRS, BookedTraveler bookedTraveler, BookedTraveler bookedTravelerUpdate) throws Exception {

        //This should never happen
        if (bookedTraveler.getInfant() == null || null == updateSecurityInfoRS || null == updateSecurityInfoRS.getFreeTextInfoList() || null == updateSecurityInfoRS.getFreeTextInfoList().getFreeTextInfo() || updateSecurityInfoRS.getFreeTextInfoList().getFreeTextInfo().isEmpty()) {
            LOG.error("CHECKIN_ERROR: processTimaticResponseForInfant: invalid request, no infant object.");
            return;
        }

        LOG.info("processTimaticResponseForInfant()");

        PnrCollectionUtil.removeMissingInformation(bookedTraveler, CheckinIneligibleReasons.TIMI.toString());

        // Add TIMATIC Status to passenger
        for (FreeTextInfoACS freeTextInfoACS : updateSecurityInfoRS.getFreeTextInfoList().getFreeTextInfo()) {
            for (String status : freeTextInfoACS.getTextLine().getText()) {

                LOG.info("processTimaticResponseForInfant: TIMATIC STATUS {} ", status);

                if ("TIM-INF OK TO BOARD".equalsIgnoreCase(status) || "INF OK TO BOARD".equalsIgnoreCase(status)) {

                    // remove missing flag from ineligibleReasons
                    PnrCollectionUtil.removeIneligibleReasonVerification(bookedTraveler, CheckinIneligibleReasons.TIMI_NOT_OK.toString());
                    PnrCollectionUtil.removeIneligibleReasonVerification(bookedTraveler, CheckinIneligibleReasons.TIM_INF_CONDITIONAL.toString());

                    TimaticInfo timaticInfo = null;

                    if (bookedTraveler.getInfant().getTimaticInfo() == null) {
                        if (bookedTravelerUpdate.getInfant().getTimaticInfo() == null) {
                            timaticInfo = new TimaticInfo();
                        } else {
                            timaticInfo = bookedTravelerUpdate.getInfant().getTimaticInfo();
                        }

                        timaticInfo.setStatus("OK TO BOARD");
                        bookedTraveler.getInfant().setTimaticInfo(timaticInfo);
                        return;
                    } else {
                        bookedTraveler.getInfant().getTimaticInfo().setStatus("OK TO BOARD");
                        return;
                    }

                } else if ("TIM-INF CONDITIONAL".equalsIgnoreCase(status) || "INF CONDITIONAL".equalsIgnoreCase(status)) {
                    // remove missing flag from ineligibleReasons
                    bookedTraveler.getIneligibleReasons().add(CheckinIneligibleReasons.TIM_INF_CONDITIONAL.toString());

                    processConditionalTimaticResponse(segment, bookedTraveler, INFANT);
                } else {
                    // remove missing flag from ineligibleReasons
                    bookedTraveler.getIneligibleReasons().add(CheckinIneligibleReasons.TIMI_NOT_OK.toString());

                    processConditionalTimaticResponse(segment, bookedTraveler, INFANT);

                }
            }
        }
    }

    private void processTimaticResponseForAdult(Segment segment, UpdateSecurityInfoRSType updateSecurityInfoRS, BookedTraveler bookedTraveler, BookedTraveler bookedTravelerUpdate) throws Exception {

        PnrCollectionUtil.removeMissingInformation(bookedTraveler, CheckinIneligibleReasons.TIM.toString());

        // Add TIMATIC Status to passenger
        for (FreeTextInfoACS freeTextInfoACS : updateSecurityInfoRS.getFreeTextInfoList().getFreeTextInfo()) {
            for (String status : freeTextInfoACS.getTextLine().getText()) {
                if ("TIM-OK TO BOARD".equalsIgnoreCase(status) || "OK TO BOARD".equalsIgnoreCase(status)) {

                    // remove missing flag from ineligibleReasons
                    PnrCollectionUtil.removeIneligibleReasonVerification(bookedTraveler, CheckinIneligibleReasons.TIM_NOT_OK.toString());
                    PnrCollectionUtil.removeIneligibleReasonVerification(bookedTraveler, CheckinIneligibleReasons.TIM_CONDITIONAL.toString());

                    TimaticInfo timaticInfo = null;

                    if (bookedTraveler.getTimaticInfo() == null) {

                        if (bookedTravelerUpdate.getTimaticInfo() == null) {
                            timaticInfo = new TimaticInfo();
                        } else {
                            timaticInfo = bookedTravelerUpdate.getTimaticInfo();
                        }

                        timaticInfo.setStatus("OK TO BOARD");
                        bookedTraveler.setTimaticInfo(timaticInfo);
                        return;
                    } else {
                        bookedTraveler.getTimaticInfo().setStatus("OK TO BOARD");
                        return;
                    }
                } else if ("TIM-CONDITIONAL".equalsIgnoreCase(status) || "CONDITIONAL".equalsIgnoreCase(status)) {
                    // remove missing flag from ineligibleReasons
                    bookedTraveler.getIneligibleReasons().add(CheckinIneligibleReasons.TIM_CONDITIONAL.toString());

                    processConditionalTimaticResponse(segment, bookedTraveler, ADULT);
                } else {
                    // remove missing flag from ineligibleReasons
                    bookedTraveler.getIneligibleReasons().add(CheckinIneligibleReasons.TIM_NOT_OK.toString());

                    processConditionalTimaticResponse(segment, bookedTraveler, ADULT);
                }
            }
        }
    }

    private void processConditionalTimaticResponse(
            Segment segment,
            BookedTraveler bookedTraveler,
            boolean isInfant
    ) {

        LOG.info("processConditionalTimaticResponse( segment={}, bookedTraveler={}, isInfant={} );", segment.getSegmentCode(), bookedTraveler.getId(), isInfant);

        // call passenger data service to get TIMATIC conditions
        GetPassengerDataRSACS passengerDataResponse = null;

        try {
            passengerDataResponse = passengerDataService.passengerDataByItineraryAndPassengerID(
                    segment.getOperatingCarrier(),
                    segment.getOperatingFlightCode(),
                    ShoppingCartUtil.getDepartureDate(segment.getDepartureDateTime()),
                    segment.getDepartureAirport(),
                    bookedTraveler.getLastName(),
                    bookedTraveler.getId(),
                    true
            );
        } catch (Exception e) {
            LOG.error("CHECKIN_ERROR: Failed to get TIMATIC passenger {} data status: {}", bookedTraveler.getId(), e.getMessage());
            return;
        }

        // Passenger data lookup by ID, when the lookup is successful, it should return only one record
        if (passengerDataResponse != null
                && passengerDataResponse.getPassengerDataResponseList() != null
                && passengerDataResponse.getPassengerDataResponseList().getPassengerDataResponse() != null
                && passengerDataResponse.getPassengerDataResponseList().getPassengerDataResponse().size() > 0) {

            CommonServiceUtil.setTimaticInfo(
                    bookedTraveler,
                    passengerDataResponse.getPassengerDataResponseList().getPassengerDataResponse().get(0),
                    isInfant
            );

            CommonServiceUtil.parseTimaticFreeText(
                    bookedTraveler,
                    passengerDataResponse.getPassengerDataResponseList().getPassengerDataResponse().get(0),
                    isInfant
            );

        } else {
            LOG.error("CHECKIN_ERROR: Invalid TIMATIC passenger data response.");
        }

        if (isInfant) {

            //For infant passenger
            if (bookedTraveler.getInfant().getTimaticInfo() == null) {

                LOG.info("Adding default TIMATIC CONDITIONAL status to infant passenger...");

                TimaticInfo timaticInfo = new TimaticInfo();
                timaticInfo.setStatus("CONDITIONAL");

                bookedTraveler.getInfant().setTimaticInfo(timaticInfo);
            }

        } else {

            //For adult passenger
            if (bookedTraveler.getTimaticInfo() == null) {

                LOG.info("Adding default TIMATIC CONDITIONAL status to adult passenger...");

                TimaticInfo timaticInfo = new TimaticInfo();
                timaticInfo.setStatus("CONDITIONAL");

                bookedTraveler.setTimaticInfo(timaticInfo);
            }
        }
    }

    /**
     * @param bookedTraveler
     * @param bookedTravelerUpdate
     * @param bookedLegCode
     * @param pnr
     * @throws Exception
     */
    private void updateTimaticInfoForPassenger(
            BookedTraveler bookedTraveler,
            BookedTraveler bookedTravelerUpdate,
            String bookedLegCode,
            PNR pnr,
            CartPNR cartPNR,
            boolean isInfant
    ) throws Exception {

        BookedSegment bookedSegment = pnr.getFirstBookedSegmentOnTime(bookedLegCode);

        if (null == bookedSegment || null == bookedSegment.getSegment()) {
            LOG.error(ErrorCodeDescriptions.MSG_CODE_BOOKEDSEGMENT_NOT_FOUND + " LegCode: " + bookedLegCode + " - PNR: " + pnr.getPnr());
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.BOOKEDSEGMENT_NOT_FOUND, ErrorType.BOOKEDSEGMENT_NOT_FOUND.getFullDescription() + " LegCode: " + bookedLegCode + " - PNR: " + pnr.getPnr());
        }

        BookingClass bookingClass = ShoppingCartUtil.getBookingClassMap(bookedTraveler, bookedSegment.getSegment().getSegmentCode());

        if (null == bookingClass) {
            LOG.error(ErrorCodeDescriptions.MSG_CODE_ERROR_NOT_FOUND_BOOKINGCLASS_FOR_SEGMENTCODE_IN_SEGMENT + bookedSegment.getSegment().getSegmentCode() + " PASSENGER ID: " + bookedTraveler.getId() + " - PNR: " + pnr.getPnr());
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.NOT_EXIST_BOOKING_CLASS_IN_PASSENGER, ErrorType.NOT_EXIST_BOOKING_CLASS_IN_PASSENGER.getFullDescription() + " FOR SEGMENTCODE: " + bookedSegment.getSegment().getSegmentCode() + " PASSENGER ID: " + bookedTraveler.getId() + " - PNR :" + pnr.getPnr());
        }

        //Get arrival airport from the last segment on the booked leg
        BookedLeg bookedLeg = pnr.getBookedLegByLegCode(bookedLegCode);
        if (null == bookedLeg) {
            //TODO: throw an error. Can't find booked leg using provided leg code.
        }

        UpdateSecurityInfoRQType updateSecurityInfoRQ;
        UpdateSecurityInfoRSType updateSecurityInfoRS;

        // adult
        if (!isInfant) {
            LOG.info("Adding Timatic info for an adult.");
            updateSecurityInfoRQ = getPassengerSecurityRQTimaticInfo(
                    bookedSegment.getSegment(),
                    bookingClass,
                    bookedTravelerUpdate,
                    bookedTraveler,
                    ADULT
            );

            updateSecurityInfoRS = passengerSecurityService.passengerSecurity(updateSecurityInfoRQ);

            if (updateSecurityInfoRS.getResult().getStatus().equals(ErrorOrSuccessCode.SUCCESS)) {
                // remove missing flag from ineligibleReasons
                PnrCollectionUtil.removeMissingInformation(bookedTraveler, CheckinIneligibleReasons.TIM.toString());

                //Get TIMATIC status
                getTimaticStatus(bookedSegment.getSegment(), bookedTraveler, bookedTravelerUpdate, bookingClass, cartPNR, ADULT);
            } else {
                // Failed to add TIMATIC info for adult passenger
                ShoppingCartErrorUtil.sabreErrorPassengerSecurity(updateSecurityInfoRS);
            }
        } else {// Infant
            LOG.info("Adding Timatic info for an infant.");
            updateSecurityInfoRQ = getPassengerSecurityRQTimaticInfo(bookedSegment.getSegment(), bookingClass, bookedTravelerUpdate, bookedTraveler, INFANT);
            updateSecurityInfoRS = passengerSecurityService.passengerSecurity(updateSecurityInfoRQ);

            if (updateSecurityInfoRS.getResult().getStatus().equals(ErrorOrSuccessCode.SUCCESS)) {
                // remove missing flag from ineligibleReasons
                PnrCollectionUtil.removeMissingInformation(bookedTraveler, CheckinIneligibleReasons.TIMI.toString());

                //Get TIMATIC status
                getTimaticStatus(bookedSegment.getSegment(), bookedTraveler, bookedTravelerUpdate, bookingClass, cartPNR, INFANT);
            } else {
                // Failed to add TIMATIC info for infant passenger
                ShoppingCartErrorUtil.sabreErrorPassengerSecurity(updateSecurityInfoRS);
            }
        }

    }

    /**
     * This method look to see if the response is a:
     * 		!UNABLE TO CONTACT TIMATIC error
     * @param updateSecurityInfoRS
     * @return
     */
    private boolean isTimaticCommunicationError( UpdateSecurityInfoRSType updateSecurityInfoRS ) {

    	// Was there a TIMATIC communication issue?
        /*
		 * <ns2:Status>BusinessLogicError</ns2:Status>
		 * <ns2:CompletionStatus>Incomplete</ns2:CompletionStatus>
		 * <ns2:System>CKI-WS</ns2:System>
		 * <ns2:SystemSpecificResults>
		 *	<ns2:ErrorMessage code="5729">!UNABLE TO CONTACT TIMATIC</ns2:ErrorMessage>
		 *	<ns2:Element>PassengerSecurityInfo/ValidateSecurityInfo/RequestTimatic</ns2:Element>
		 * </ns2:SystemSpecificResults>
         */
        if ( updateSecurityInfoRS.getResult().getStatus().equals(ErrorOrSuccessCode.BUSINESS_LOGIC_ERROR) &&
        		updateSecurityInfoRS.getResult().getSystemSpecificResults().get(0).getErrorMessage().getValue().equalsIgnoreCase( "!UNABLE TO CONTACT TIMATIC" ) &&
        		updateSecurityInfoRS.getResult().getSystemSpecificResults().get(0).getErrorMessage().getCode().intValue() == 5729 ) {

            Log.error("getTimaticStatus isTimaticCommunicationError ERROR: {}", updateSecurityInfoRS.getResult().getSystemSpecificResults().get(0).getErrorMessage().getValue());

            return true;
        }

        return false;
    }

    private void updateVisaInfoForPassenger(BookedTraveler bookedTraveler, BookedTraveler bookedTravelerUpdate, String bookedLegCode, PNR pnr, CartPNR cartPNR, boolean isInfant) throws Exception {

        BookedSegment bookedSegment = pnr.getFirstBookedSegmentOnTime(bookedLegCode);

        if (null == bookedSegment || null == bookedSegment.getSegment()) {
            LOG.error("CHECKIN_ERROR: DESCRIPTION: {} LEGCODE: {} PNR: {}",ErrorCodeDescriptions.MSG_CODE_BOOKEDSEGMENT_NOT_FOUND,bookedLegCode,pnr.getPnr());
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.BOOKEDSEGMENT_NOT_FOUND, ErrorType.BOOKEDSEGMENT_NOT_FOUND.getFullDescription() + " LegCode: " + bookedLegCode + " - PNR: " + pnr.getPnr());
        }

        BookingClass bookingClass = ShoppingCartUtil.getBookingClassMap(bookedTraveler, bookedSegment.getSegment().getSegmentCode());

        if (null == bookingClass) {
            LOG.error("CHECKIN_ERROR: DESCRIPTION: {} PASSENGER ID: {} PNR: {}",ErrorCodeDescriptions.MSG_CODE_ERROR_NOT_FOUND_BOOKINGCLASS_FOR_SEGMENTCODE_IN_SEGMENT + bookedSegment.getSegment().getSegmentCode(),bookedTraveler.getId(),pnr.getPnr());
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.NOT_EXIST_BOOKING_CLASS_IN_PASSENGER, ErrorType.NOT_EXIST_BOOKING_CLASS_IN_PASSENGER.getFullDescription() + " FOR SEGMENTCODE: " + bookedSegment.getSegment().getSegmentCode() + " PASSENGER ID: " + bookedTraveler.getId() + " - PNR :" + pnr.getPnr());
        }

        //Add visa information for this adult or infant passenger
        UpdateSecurityInfoRQType updateSecurityInfoRQ = getPassengerSecurityRQVisa(bookedSegment.getSegment(), bookingClass, bookedTravelerUpdate, bookedTraveler, isInfant);
        UpdateSecurityInfoRSType updateSecurityInfoRS = passengerSecurityService.passengerSecurity(updateSecurityInfoRQ);

        if (updateSecurityInfoRS.getResult().getStatus().equals(ErrorOrSuccessCode.SUCCESS)) {
            // remove missing flag from ineligibleReasons
            PnrCollectionUtil.removeMissingInformation(bookedTraveler, CheckinIneligibleReasons.TIM.toString());

            if (isInfant) {
                PnrCollectionUtil.removeMissingInformation(bookedTraveler, CheckinIneligibleReasons.INFANT_VISA.toString());

                bookedTraveler.getInfant().setVisaInfo(bookedTravelerUpdate.getVisaInfo());
                bookedTraveler.getInfant().setVisaInfoAlreadySent(true);
                bookedTraveler.getInfant().setTimaticProvided(true);
            } else {
                PnrCollectionUtil.removeMissingInformation(bookedTraveler, CheckinIneligibleReasons.VISA.toString());

                bookedTraveler.setVisaInfo(bookedTravelerUpdate.getVisaInfo());
                bookedTraveler.setVisaInfoAlreadySent(true);
                bookedTraveler.setTimaticProvided(true);
            }

            //Get TIMATIC status
            getTimaticStatus(bookedSegment.getSegment(), bookedTraveler, bookedTravelerUpdate, bookingClass, cartPNR, isInfant);
        } else {
            // Failed to add TIMATIC info to adult passenger
            ShoppingCartErrorUtil.sabreErrorPassengerSecurity(updateSecurityInfoRS);
        }

    }

    /*
     * valida si esta completo kit experto
     */
    private void IsKitExperto(BookedTraveler bookedTravelerUpdate, AncillaryOfferCollection ancillaryOfferCollectionOnDB) {

        if (null != bookedTravelerUpdate.getAncillaries()
                && null != bookedTravelerUpdate.getAncillaries().getCollection()
                && !bookedTravelerUpdate.getAncillaries().getCollection().isEmpty()) {
            if (validaKitIscompleted(bookedTravelerUpdate) == 4) {
                updateKit(bookedTravelerUpdate, false, null);
            } else if (isPaidancillaryInRQ(bookedTravelerUpdate)) {
                updateKit(bookedTravelerUpdate, true, ancillaryOfferCollectionOnDB);
            } else if (validaKitCero(bookedTravelerUpdate) == 3) {
                kitChange(bookedTravelerUpdate);
            }
        }
    }

    private void kitChange(BookedTraveler bookedTravelerUpdate) {
        for (AbstractAncillary abstractAncillaryUpdate : bookedTravelerUpdate.getAncillaries().getCollection()) {
            if (abstractAncillaryUpdate instanceof TravelerAncillary) {
                TravelerAncillary travelerAncillaryUpdate = (TravelerAncillary) abstractAncillaryUpdate;
                if (AncillariesUtil.ancillariesExpertKit.contains(travelerAncillaryUpdate.getAncillary().getType())) {
                    travelerAncillaryUpdate.setQuantity(1);
                }
                if (travelerAncillaryUpdate.getQuantity() > 0 && AncillariesUtil.ancillariesIncludedInExpertKit.contains(travelerAncillaryUpdate.getAncillary().getType())) {
                    travelerAncillaryUpdate.setQuantity(0);
                }
            }
        }
    }

    private int validaKitCero(BookedTraveler bookedTravelerUpdate) {
        int contkitOptions = 0;
        for (AbstractAncillary abstractAncillaryUpdate : bookedTravelerUpdate.getAncillaries().getCollection()) {
            if (abstractAncillaryUpdate instanceof TravelerAncillary) {
                TravelerAncillary travelerAncillaryUpdate = (TravelerAncillary) abstractAncillaryUpdate;
                if (AncillariesUtil.ancillariesExpertKit.contains(travelerAncillaryUpdate.getAncillary().getType())) {
                    if (null != travelerAncillaryUpdate.getOptions() && !travelerAncillaryUpdate.getOptions().isEmpty()) {
                        for (AbstractAncillary AbstractAncillaryOptions : travelerAncillaryUpdate.getOptions()) {
                            if (AbstractAncillaryOptions instanceof TravelerAncillary) {
                                TravelerAncillary TravelerAncillaryOptions = (TravelerAncillary) AbstractAncillaryOptions;
                                if (TravelerAncillaryOptions.getQuantity() > 0 && AncillariesUtil.ancillariesIncludedInExpertKit.contains(TravelerAncillaryOptions.getAncillary().getType())) {
                                    contkitOptions++;
                                }
                            }
                        }
                    }
                }
            }
        }

        return contkitOptions;
    }

    private boolean isPaidancillaryInRQ(BookedTraveler bookedTravelerUpdate) {

        for (AbstractAncillary abstractAncillaryUpdate : bookedTravelerUpdate.getAncillaries().getCollection()) {
            if (abstractAncillaryUpdate instanceof PaidTravelerAncillary) {
                PaidTravelerAncillary PaidTravelerAncillaryUpdate = (PaidTravelerAncillary) abstractAncillaryUpdate;
                if (PaidTravelerAncillaryUpdate.getQuantity() > 0 && com.aeromexico.commons.model.utils.AncillariesUtil.ancillariesIncludedInExpertKit
                        .contains(PaidTravelerAncillaryUpdate.getType())) {
                    return true;
                }
            }
        }
        return false;
    }

    private void updateKit(BookedTraveler bookedTravelerUpdate, boolean ispaid, AncillaryOfferCollection ancillaryOfferCollectionOnDB) {

        for (AbstractAncillary abstractAncillaryUpdate : bookedTravelerUpdate.getAncillaries().getCollection()) {
            if (abstractAncillaryUpdate instanceof TravelerAncillary) {
                TravelerAncillary travelerAncillaryUpdate = (TravelerAncillary) abstractAncillaryUpdate;
                if (travelerAncillaryUpdate.getQuantity() > 0 && AncillariesUtil.ancillariesIncludedInExpertKit.contains(travelerAncillaryUpdate.getAncillary().getType()) && !ispaid) {
                    travelerAncillaryUpdate.setQuantity(0);
                } else if (ispaid && AncillariesUtil.ancillariesExpertKit.contains(travelerAncillaryUpdate.getAncillary().getType())) {
                    travelerAncillaryUpdate.setQuantity(0);
                }
            }
        }

        if (ispaid) {

            //travelerAncillaryUpdate.setQuantity(0);
            List<String> paidList = getPaidTravelerList(bookedTravelerUpdate.getAncillaries());
            List<String> includedInExpertKit = AncillariesUtil.ancillariesIncludedInExpertKit;

            TravelerAncillary travelerAncillaryUpdate = new TravelerAncillary();

            List<AbstractAncillary> AbstractAncillarylist = bookedTravelerUpdate.getAncillaries().getCollection();
            TravelerAncillary travelerAncillaryUpdatej = null;

            for (AbstractAncillary abstractAncillary : AbstractAncillarylist) {

                if (abstractAncillary instanceof TravelerAncillary) {
                    travelerAncillaryUpdate = (TravelerAncillary) abstractAncillary;
                    for (String ancillary : includedInExpertKit) {
                        if (!paidList.contains(ancillary)) {
                            travelerAncillaryUpdatej = travelerAncillaryUpdate.builTravelerAncillary(
                                    AncillaryUtil.getAncillaryFromAncillaryOfferCollection(ancillary, ancillaryOfferCollectionOnDB)
                            );
                        }
                    }
                }
            }

            bookedTravelerUpdate.getAncillaries().getCollection().add(travelerAncillaryUpdatej);
        }
    }

    private int validaKitIscompleted(BookedTraveler bookedTravelerUpdate) {

        int contkit = 0;

        for (AbstractAncillary abstractAncillaryUpdate : bookedTravelerUpdate.getAncillaries().getCollection()) {
            if (abstractAncillaryUpdate instanceof TravelerAncillary) {
                TravelerAncillary travelerAncillaryUpdate = (TravelerAncillary) abstractAncillaryUpdate;
                if ((travelerAncillaryUpdate.getQuantity() > 0 && AncillariesUtil.ancillariesExpertKit.contains(travelerAncillaryUpdate.getAncillary().getType()))
                        || (travelerAncillaryUpdate.getQuantity() > 0 && AncillariesUtil.ancillariesIncludedInExpertKit
                        .contains(travelerAncillaryUpdate.getAncillary().getType()))) {
                    contkit++;

                }
            }
        }
        return contkit;
    }

    private void addorUpdateKitResponse(List<BookedTraveler> bookedTravelerUpdateList, CartPNR cartPNR, String cartId) throws Exception {

        AncillaryOfferCollection ancillaryOfferCollectionOnDB;
        try {
            ancillaryOfferCollectionOnDB = shoppingCartAncillaries.getAncillariesCollection(cartId);
        } catch (MongoException ex) {
            ancillaryOfferCollectionOnDB = null;
        }

        for (BookedTraveler bookedTravelerUpdate : bookedTravelerUpdateList) {

            BookedTraveler bookedTraveler = cartPNR.getBookedTravelerById(bookedTravelerUpdate.getId());

            if (null != bookedTravelerUpdate.getAncillaries()
                    && null != bookedTravelerUpdate.getAncillaries().getCollection()
                    && !bookedTravelerUpdate.getAncillaries().getCollection().isEmpty()) {

                for (AbstractAncillary abstractAncillaryUpdate : bookedTravelerUpdate.getAncillaries()
                        .getCollection()) {

                    if (abstractAncillaryUpdate instanceof TravelerAncillary) {

                        TravelerAncillary travelerAncillaryUpdate = (TravelerAncillary) abstractAncillaryUpdate;

                        if (AncillariesUtil.ancillariesExpertKit.contains(travelerAncillaryUpdate.getAncillary().getType())
                                && travelerAncillaryUpdate.getQuantity() > 0 && travelerAncillaryUpdate.getOptions().size() > 0) {
                            changeOptionKitCase1(bookedTraveler, ancillaryOfferCollectionOnDB);
                        } else if (AncillariesUtil.ancillariesExpertKit.contains(travelerAncillaryUpdate.getAncillary().getType())
                                && travelerAncillaryUpdate.getQuantity() <= 0 && !travelerAncillaryUpdate.getOptions().isEmpty()
                                && isOptionsVal(travelerAncillaryUpdate.getOptions())) {
                            travelerAncillaryUpdate.setQuantity(1);
                            bookedTraveler.getAncillaries().getCollection().add(travelerAncillaryUpdate);
                        } else if (AncillariesUtil.ancillariesExpertKit.contains(travelerAncillaryUpdate.getAncillary().getType())
                                && travelerAncillaryUpdate.getQuantity() >= 1 && travelerAncillaryUpdate.getOptions().isEmpty()) {
                            optionAncillaryKit(travelerAncillaryUpdate, ancillaryOfferCollectionOnDB, null, bookedTraveler);
                        } else if (AncillariesUtil.ancillariesExpertKit.contains(travelerAncillaryUpdate.getAncillary().getType())
                                && travelerAncillaryUpdate.getQuantity() <= 0 && travelerAncillaryUpdate.getOptions().isEmpty()) {
                            travelerAncillaryUpdate.setQuantity(1);
                            optionAncillaryKit(travelerAncillaryUpdate, ancillaryOfferCollectionOnDB, bookedTravelerUpdate.getAncillaries(), bookedTraveler);
                        } else if (AncillariesUtil.ancillariesExpertKit.contains(travelerAncillaryUpdate.getAncillary().getType())
                                && travelerAncillaryUpdate.getQuantity() <= 0 && !travelerAncillaryUpdate.getOptions().isEmpty()
                                && !isOptionsVal(travelerAncillaryUpdate.getOptions())) {
                            bookedTraveler.getAncillaries().getCollection().add(travelerAncillaryUpdate);
                        }
                    }
                }
            }
        }
    }

    private boolean isOptionsVal(List<AbstractAncillary> optionsAncilary) {

        for (AbstractAncillary travelerAncillaryOpt : optionsAncilary) {
            if (travelerAncillaryOpt instanceof TravelerAncillary) {
                TravelerAncillary travelerAncillaryAbsOpt = (TravelerAncillary) travelerAncillaryOpt;
                if (travelerAncillaryAbsOpt.getQuantity() > 0 && AncillariesUtil.ancillariesIncludedInExpertKit.contains(travelerAncillaryAbsOpt.getAncillary().getType())) {
                    return true;
                }
            }
        }
        return false;
    }

    private void optionAncillaryKit(TravelerAncillary travelerAncillaryUpdate, AncillaryOfferCollection ancillaryOfferCollectionOnDB, BookedTravelerAncillaryCollection ancillaries, BookedTraveler bookedTraveler) throws GenericException, Exception {

        if (ancillaryOfferCollectionOnDB != null) {

            if (ancillaries != null) {
                List<String> paidList = getPaidTravelerList(ancillaries);
                List<String> includedInExpertKit = AncillariesUtil.ancillariesIncludedInExpertKit;

                for (String ancillary : includedInExpertKit) {
                    if (!paidList.contains(ancillary)) {
                        if (AncillariesUtil.ancillariesExpertKit.contains(travelerAncillaryUpdate.getAncillary().getType())) {
                            travelerAncillaryUpdate.getOptions().add(
                                    travelerAncillaryUpdate.builTravelerAncillary(
                                            AncillaryUtil.getAncillaryFromAncillaryOfferCollection(
                                                    ancillary,
                                                    ancillaryOfferCollectionOnDB
                                            )
                                    )
                            );
                        }

                    }
                }
                bookedTraveler.getAncillaries().getCollection().add(travelerAncillaryUpdate);

            } else {

                List<TravelerAncillary> options = new ArrayList<>();

                for (String ancillariesKit : AncillariesUtil.ancillariesIncludedInExpertKit) {

                    TravelerAncillary travelerAncillary = travelerAncillaryUpdate.builTravelerAncillary(
                            AncillaryUtil.getAncillaryFromAncillaryOfferCollection(
                                    ancillariesKit,
                                    ancillaryOfferCollectionOnDB
                            )
                    );

                    options.add(travelerAncillary);
                }

                for (AbstractAncillary abstractAncillary : bookedTraveler.getAncillaries().getCollection()) {
                    if (abstractAncillary instanceof TravelerAncillary) {
                        TravelerAncillary travelerAncillary = (TravelerAncillary) abstractAncillary;
                        if (AncillariesUtil.ancillariesExpertKit.contains(travelerAncillary.getAncillary().getType())) {
                            travelerAncillary.getOptions().addAll(options);
                        }
                    }
                }
            }

        }

    }

    private List<String> getPaidTravelerList(BookedTravelerAncillaryCollection ancillaries) {
        List<String> paidList = new ArrayList<>();

        for (AbstractAncillary abstractAncillaryUpdate : ancillaries.getCollection()) {
            if (abstractAncillaryUpdate instanceof PaidTravelerAncillary) {
                PaidTravelerAncillary PaidTravelerAncillaryUpdate = (PaidTravelerAncillary) abstractAncillaryUpdate;
                if (PaidTravelerAncillaryUpdate.getQuantity() > 0
                        && com.aeromexico.commons.model.utils.AncillariesUtil.ancillariesIncludedInExpertKit
                        .contains(PaidTravelerAncillaryUpdate.getType())) {
                    paidList.add(PaidTravelerAncillaryUpdate.getType());
                }
            }
        }
        return paidList;
    }

    private void changeOptionKitCase1(BookedTraveler bookedTraveler, AncillaryOfferCollection ancillaryOfferCollectionOnDB) {

        for (AbstractAncillary abstractAncillary : bookedTraveler.getAncillaries().getCollection()) {
            if (abstractAncillary instanceof TravelerAncillary) {
                TravelerAncillary travelerAncillary = (TravelerAncillary) abstractAncillary;

                if (AncillariesUtil.ancillariesExpertKit.contains(travelerAncillary.getAncillary().getType()) && travelerAncillary.getQuantity() > 0) {

                    if (!travelerAncillary.getOptions().isEmpty()) {
                        for (AbstractAncillary travelerAncillaryOpt : travelerAncillary.getOptions()) {
                            if (travelerAncillaryOpt instanceof TravelerAncillary) {
                                TravelerAncillary travelerAncillaryAbsOpt = (TravelerAncillary) travelerAncillaryOpt;
                                if (travelerAncillaryAbsOpt.getQuantity() == 0 && AncillariesUtil.ancillariesIncludedInExpertKit.contains(travelerAncillaryAbsOpt.getAncillary().getType())) {
                                    travelerAncillaryAbsOpt.setQuantity(1);
                                }
                            }

                        }
                    } else {
                        for (String ancillariesKit : AncillariesUtil.ancillariesIncludedInExpertKit) {

                            TravelerAncillary travelerAncillaryOps = travelerAncillary.builTravelerAncillary(
                                    AncillaryUtil.getAncillaryFromAncillaryOfferCollection(ancillariesKit, ancillaryOfferCollectionOnDB)
                            );

                            travelerAncillary.getOptions().add(travelerAncillaryOps);

                        }
                    }
                }
            }
        }
    }

    /**
     * @param bookedTraveler
     * @param bookedTravelerUpdate
     * @throws Exception
     */
    private void updatedGender(BookedTraveler bookedTraveler, BookedTraveler bookedTravelerUpdate) throws Exception {

        // Gender
        if (null != bookedTravelerUpdate.getGender()) {

            if (null == bookedTraveler.getGender() || !bookedTravelerUpdate.getGender().getCode().equalsIgnoreCase(bookedTraveler.getGender().getCode())) {
                bookedTraveler.setGender(bookedTravelerUpdate.getGender());
                PnrCollectionUtil.removeMissingInformation(bookedTraveler, CheckinIneligibleReasons.MG.toString());
            }
        }

    }

    /**
     * @param bookedTraveler
     * @param bookedTravelerUpdate
     * @throws Exception
     */
    private void updatedDateOfBirth(BookedTraveler bookedTraveler, BookedTraveler bookedTravelerUpdate) throws Exception {

        // DateOfBirth
        if (null != bookedTravelerUpdate.getDateOfBirth()) {

            // Update date of birth in cartPNR
            bookedTraveler.setDateOfBirth(bookedTravelerUpdate.getDateOfBirth());
            PnrCollectionUtil.removeMissingInformation(bookedTraveler, CheckinIneligibleReasons.DOB.toString());
        }

    }

    /**
     * Update mail to passenger and remove value of missing information schema
     *
     * @param bookedTraveler
     * @param bookedTravelerUpdate
     * @throws Exception
     */
    private void updatedMail(BookedTraveler bookedTraveler, BookedTraveler bookedTravelerUpdate) throws Exception {
        // Email
        if (null != bookedTravelerUpdate.getEmail()) {
            if (null == bookedTraveler.getEmail() || !bookedTravelerUpdate.getEmail().equalsIgnoreCase(bookedTraveler.getEmail())) {
                // Update email in cartPNR
                bookedTraveler.setEmail(bookedTravelerUpdate.getEmail());
                PnrCollectionUtil.removeMissingInformation(bookedTraveler, CheckinIneligibleReasons.EMAIL.toString());
            }
        }
    }

    /**
     * Update phone to passenger and remove value of missing information schema
     *
     * @param bookedTraveler
     * @param bookedTravelerUpdate
     * @throws Exception
     */
    private void updatedPhone(BookedTraveler bookedTraveler, BookedTraveler bookedTravelerUpdate) throws Exception {
        // Phone
        if (null != bookedTravelerUpdate.getPhones() && !bookedTravelerUpdate.getPhones().getCollection().isEmpty()) {
            if (null == bookedTraveler.getPhones() || bookedTraveler.getPhones().getCollection().isEmpty()) {
                // Update email in cartPNR
                bookedTraveler.setPhones(bookedTravelerUpdate.getPhones());
                PnrCollectionUtil.removeMissingInformation(bookedTraveler, CheckinIneligibleReasons.PHONE.toString());
            }
        }
    }

    /**
     * *
     * This methods compares the "Country Of Residence" in the shopping cart PUT
     * request with the local copy of the passenger info. If the info are
     * different, returns true. Otherwise, return false.
     *
     * @return boolean
     */
    private boolean wasCountryOfResidenceUpdatedOfAdded(BookedTraveler bookedTraveler, BookedTraveler bookedTravelerUpdate) {

        // No residency update after check-in.
        if (bookedTraveler.isCheckinStatus()) {
            return false;
        }

        //In this case, the shopping cart PUT request did not have any passenger info in the request. Which is most likely an invalid request.
        if (bookedTravelerUpdate == null) {
            return false;
        }

        //In this case, the shopping cart PUT request did not have any "Country Of Resident" information
        if (bookedTravelerUpdate.getCountryOfResidence() == null) {
            return false;
        }

        //This case is not expected to happen. If the bookedTraveler is null, something is wrong. Better to return false in this case.
        if (bookedTraveler == null) {
            return false;
        }

        //In this case, we have "Country Of Residence" in the shopping cart PUT request, but we don't have any previous data to compare with.
        //And since this is a new data, we need to add. So, return true;
        if (bookedTraveler.getCountryOfResidence() == null) {
            return true;
        }

        //In this case, there are "Country Of Residence" info in the shopping cart PUT request and there are no
        if (bookedTravelerUpdate.getCountryOfResidence().equalsIgnoreCase(bookedTraveler.getCountryOfResidence())) {
            return false;
        }

        return true;
    }

    /**
     * @param bookedTraveler
     * @param bookedTravelerUpdate
     * @param bookedLegCode
     * @param pnr
     * @throws Exception
     */
    private void updatedCountryOfResidence(BookedTraveler bookedTraveler, BookedTraveler bookedTravelerUpdate, String bookedLegCode, PNR pnr) throws Exception {

        // Do we have to add "Country of Residence"?
        if (!wasCountryOfResidenceUpdatedOfAdded(bookedTraveler, bookedTravelerUpdate)) {
            //There was no "Country Of Residence" added or updated
            return;
        }

        BookedSegment bookedSegment = pnr.getFirstBookedSegmentOnTime(bookedLegCode);

        if (null == bookedSegment || null == bookedSegment.getSegment()) {
            LOG.error("CHECKIN_ERROR: DESCRIPTION: {} LEGCODE: {} PNR: {}",ErrorCodeDescriptions.MSG_CODE_BOOKEDSEGMENT_NOT_FOUND,bookedLegCode,pnr.getPnr());
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.BOOKEDSEGMENT_NOT_FOUND, ErrorType.BOOKEDSEGMENT_NOT_FOUND.getFullDescription() + " LegCode: " + bookedLegCode + " - PNR: " + pnr.getPnr());
        }

        BookingClass bookingClass = ShoppingCartUtil.getBookingClassMap(bookedTraveler, bookedSegment.getSegment().getSegmentCode());

        if (null == bookingClass) {
            LOG.error("CHECKIN_ERROR: DESCRIPTION: {} PASSENGER ID: {} PNR: {}",ErrorCodeDescriptions.MSG_CODE_ERROR_NOT_FOUND_BOOKINGCLASS_FOR_SEGMENTCODE_IN_SEGMENT + bookedSegment.getSegment().getSegmentCode(),bookedTraveler.getId(),pnr.getPnr());
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.NOT_EXIST_BOOKING_CLASS_IN_PASSENGER, ErrorType.NOT_EXIST_BOOKING_CLASS_IN_PASSENGER.getFullDescription() + " FOR SEGMENTCODE: " + bookedSegment.getSegment().getSegmentCode() + " PASSENGER ID: " + bookedTraveler.getId() + " - PNR :" + pnr.getPnr());
        }

        if (bookedTravelerUpdate.getCountryOfResidence().trim().isEmpty()) {
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EMPTY_FIELD_COUNTRY_OF_RESIDENCE, ErrorType.EMPTY_FIELD_COUNTRY_OF_RESIDENCE.getFullDescription() + " PASSENGER ID: " + bookedTraveler.getId() + " - PNR: " + pnr.getPnr());
        }

        // adult
        UpdateSecurityInfoRQType updateSecurityInfoRQ = getPassengerSecurityRQResidenceAddress(bookedSegment.getSegment(), bookingClass, bookedTravelerUpdate, bookedTraveler, ADULT);
        UpdateSecurityInfoRSType updateSecurityInfoRS = passengerSecurityService.passengerSecurity(updateSecurityInfoRQ);

        if (!updateSecurityInfoRS.getResult().getStatus().equals(ErrorOrSuccessCode.SUCCESS)) {

            //Failed to add country of residence to the adult passenger. Log request & response
            ShoppingCartErrorUtil.sabreErrorPassengerSecurity(updateSecurityInfoRS);
        } else {
            // Update country of residence in cartPNR
            bookedTraveler.setCountryOfResidence(bookedTravelerUpdate.getCountryOfResidence());

            // remove missing flag from ineligibleReasons
            PnrCollectionUtil.removeMissingInformation(bookedTraveler, CheckinIneligibleReasons.MRA.toString());
        }

        // Infant - start
        //if (PnrCollectionUtil.findMissingInformation(bookedTraveler, CheckinIneligibleReasons.MRAI.toString())) {
        if (bookedTraveler.getInfant() != null) {

            updateSecurityInfoRQ = getPassengerSecurityRQResidenceAddress(bookedSegment.getSegment(), bookingClass, bookedTravelerUpdate, bookedTraveler, INFANT);
            updateSecurityInfoRS = passengerSecurityService.passengerSecurity(updateSecurityInfoRQ);

            if (!updateSecurityInfoRS.getResult().getStatus().equals(ErrorOrSuccessCode.SUCCESS)) {
                //Failed to add country of residence to the infant passenger. Log request & response
                ShoppingCartErrorUtil.sabreErrorPassengerSecurity(updateSecurityInfoRS);
            } else if (PnrCollectionUtil.findMissingInformation(bookedTraveler, CheckinIneligibleReasons.MRAI.toString())) {
                // remove missing flag from ineligibleReasons for infant
                PnrCollectionUtil.removeMissingInformation(bookedTraveler, CheckinIneligibleReasons.MRAI.toString());
            }
        }
        // Infant - end
    }

    /**
     * @param bookedTraveler
     * @param bookedTravelerUpdate
     * @param bookedLegCode
     * @param pnr
     * @throws Exception
     */
    private void validateSameDestinationAndCountryResidence(BookedTraveler bookedTraveler, BookedTraveler bookedTravelerUpdate, String bookedLegCode, PNR pnr) throws Exception {

        BookedSegment bookedSegment = pnr.getFirstBookedSegmentOnTime(bookedLegCode);
        //We validate for Adult missing destination address
        if (PnrCollectionUtil.findMissingInformation(bookedTraveler, CheckinIneligibleReasons.MDA.toString())) {

            AirportWeather arrivalAirport = airportsCodesDao.getAirportWeatherByIata(bookedSegment.getSegment().getArrivalAirport());
            if (arrivalAirport.getAirport().getCountry().equalsIgnoreCase(bookedTraveler.getCountryOfResidence())) {
                PnrCollectionUtil.removeMissingInformation(bookedTraveler, CheckinIneligibleReasons.MDA.toString());
            }
        }
        //Then we validate for infant missing destination address
        if (PnrCollectionUtil.findMissingInformation(bookedTraveler, CheckinIneligibleReasons.MDAI.toString())) {
            AirportWeather arrivalAirport = airportsCodesDao.getAirportWeatherByIata(bookedSegment.getSegment().getArrivalAirport());
            if (arrivalAirport.getAirport().getCountry().equalsIgnoreCase(bookedTraveler.getCountryOfResidence())) {
                PnrCollectionUtil.removeMissingInformation(bookedTraveler, CheckinIneligibleReasons.MDAI.toString());
            }
        }
    }

    /**
     * @param bookedTraveler
     * @param bookedTravelerUpdate
     * @param bookedLegCode
     * @param pnr
     * @throws Exception
     */
    private void updatedDestinationAddress(
            BookedTraveler bookedTraveler,
            BookedTraveler bookedTravelerUpdate,
            String bookedLegCode,
            PNR pnr
    ) throws Exception {

        // Destination Address
        if (null != bookedTravelerUpdate.getDestinationAddress()) {

            // Check if the information is different
            if (null == bookedTraveler.getDestinationAddress()
                    || (!bookedTraveler.getDestinationAddress().equals(bookedTravelerUpdate.getDestinationAddress()))) {

                BookedSegment bookedSegment = pnr.getFirstBookedSegmentOnTime(bookedLegCode);

                if (bookedSegment == null || bookedSegment.getSegment() == null) {
                    LOG.error("CHECKIN_ERROR: DESCRIPTION: {} LEGCODE: {} PNR: {}",ErrorCodeDescriptions.MSG_CODE_BOOKEDSEGMENT_NOT_FOUND,bookedLegCode,pnr.getPnr());
                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.BOOKEDSEGMENT_NOT_FOUND,
                            ErrorType.BOOKEDSEGMENT_NOT_FOUND.getFullDescription() + " LegCode : " + bookedLegCode
                                    + " - PNR : " + pnr.getPnr());
                }

                BookingClass bookingClass = ShoppingCartUtil.getBookingClassMap(bookedTraveler, bookedSegment.getSegment().getSegmentCode());

                if (null == bookingClass) {
                    LOG.error("CHECKIN_ERROR: DESCRIPTION: {} PASSENGER ID: {} PNR: {}",ErrorCodeDescriptions.MSG_CODE_ERROR_NOT_FOUND_BOOKINGCLASS_FOR_SEGMENTCODE_IN_SEGMENT+
                    bookedSegment.getSegment().getSegmentCode(),bookedTraveler.getId(), pnr.getPnr());
                    throw new GenericException(Response.Status.BAD_REQUEST,
                            ErrorType.NOT_EXIST_BOOKING_CLASS_IN_PASSENGER,
                            ErrorType.NOT_EXIST_BOOKING_CLASS_IN_PASSENGER.getFullDescription() + " FOR SEGMENTCODE : "
                                    + bookedSegment.getSegment().getSegmentCode() + " PASSENGER ID : "
                                    + bookedTraveler.getId() + " - PNR :" + pnr.getPnr());
                }

                ShoppingCartUtil.isEmptyDestinationAddress(bookedTravelerUpdate.getDestinationAddress());

                // adult
                UpdateSecurityInfoRQType updateSecurityInfoRQ = getPassengerSecurityRQDestinationAddress(
                        bookedSegment.getSegment(),
                        bookingClass,
                        bookedTravelerUpdate,
                        bookedTraveler,
                        ADULT
                );
                UpdateSecurityInfoRSType updateSecurityInfoRS = passengerSecurityService.passengerSecurity(
                        updateSecurityInfoRQ
                );

                if (!updateSecurityInfoRS.getResult().getStatus().equals(ErrorOrSuccessCode.SUCCESS)) {
                    LOG.error("CEHCKIN_ERROR: PassengerSecurity - Destination_Address: " + new Gson().toJson(updateSecurityInfoRS));
                    ShoppingCartErrorUtil.sabreErrorPassengerSecurity(updateSecurityInfoRS);
                } else {
                    // Update destination address in cartPNR
                    bookedTraveler.setDestinationAddress(bookedTravelerUpdate.getDestinationAddress());
                    // remove missing flag from ineligibleReasons
                    PnrCollectionUtil.removeMissingInformation(bookedTraveler, CheckinIneligibleReasons.MDA.toString());
                }

                // Infant
                if (PnrCollectionUtil.findMissingInformation(bookedTraveler, CheckinIneligibleReasons.MDAI.toString())) {

                    updateSecurityInfoRQ = getPassengerSecurityRQDestinationAddress(
                            bookedSegment.getSegment(),
                            bookingClass,
                            bookedTravelerUpdate,
                            bookedTraveler,
                            INFANT
                    );
                    updateSecurityInfoRS = passengerSecurityService.passengerSecurity(updateSecurityInfoRQ);

                    if (!updateSecurityInfoRS.getResult().getStatus().equals(ErrorOrSuccessCode.SUCCESS)) {
                        ShoppingCartErrorUtil.sabreErrorPassengerSecurity(updateSecurityInfoRS);
                    } else {
                        // remove missing flag from ineligibleReasons for infant
                        PnrCollectionUtil.removeMissingInformation(bookedTraveler, CheckinIneligibleReasons.MDAI.toString());
                    }
                }
            }
        }
    }

    /**
     * @param bookedTravelers
     * @param bookedSegment
     * @param recordLocator
     * @throws Exception
     */
    public void declineEmergencyContact(
            List<BookedTraveler> bookedTravelers,
            BookedSegment bookedSegment,
            String recordLocator
    ) throws Exception {

        if (null == bookedSegment || null == bookedSegment.getSegment()) {
            LOG.error("CHECKIN_ERROR: DESCRIPTION: {} PNR: {}",
                    ErrorCodeDescriptions.MSG_CODE_BOOKEDSEGMENT_NOT_FOUND,
                    recordLocator
            );
            throw new GenericException(
                    Response.Status.BAD_REQUEST,
                    ErrorType.BOOKEDSEGMENT_NOT_FOUND,
                    ErrorType.BOOKEDSEGMENT_NOT_FOUND.getFullDescription() + " - PNR : " + recordLocator
            );
        }

        for (BookedTraveler bookedTraveler : bookedTravelers) {

            if (bookedTraveler.getDeclinedEmergencyContact()) {

                BookingClass bookingClass = ShoppingCartUtil.getBookingClassMap(
                        bookedTraveler,
                        bookedSegment.getSegment().getSegmentCode()
                );

                if (null == bookingClass) {
                    LOG.error("CHECKIN_ERROR: DESCRIPTION: {} PASSENGER ID: {} PNR: {}",ErrorCodeDescriptions.MSG_CODE_ERROR_NOT_FOUND_BOOKINGCLASS_FOR_SEGMENTCODE_IN_SEGMENT
                            + bookedSegment.getSegment().getSegmentCode(),bookedTraveler.getId()
                            ,recordLocator);
                    throw new GenericException(Response.Status.BAD_REQUEST,
                            ErrorType.NOT_EXIST_BOOKING_CLASS_IN_PASSENGER,
                            ErrorType.NOT_EXIST_BOOKING_CLASS_IN_PASSENGER.getFullDescription() + "FOR SEGMENTCODE: "
                                    + bookedSegment.getSegment().getSegmentCode() + "PASSENGER ID: "
                                    + bookedTraveler.getId() + " - PNR :" + recordLocator);
                }

                UpdateSecurityInfoRQType updateSecurityInfoRQ = getPassengerSecurityRQEmergencyContact(
                        bookedSegment.getSegment(), bookingClass, bookedTraveler
                );

                UpdateSecurityInfoRSType updateSecurityInfoRS = passengerSecurityService.passengerSecurity(
                        updateSecurityInfoRQ
                );

                if (!ErrorOrSuccessCode.SUCCESS.equals(updateSecurityInfoRS.getResult().getStatus())) {
                    ShoppingCartErrorUtil.sabreErrorPassengerSecurity(updateSecurityInfoRS);
                } else {
                    // remove missing flag from ineligibleReasons}
                    PnrCollectionUtil.removeMissingInformation(bookedTraveler, CheckinIneligibleReasons.MEC.toString());
                }
            }

        }
    }

    /**
     * @param bookedTraveler
     * @param bookedTravelerUpdate
     * @param bookedLegCode
     * @param pnr
     * @throws Exception
     */
    private void updatedEmergencyContact(
            BookedTraveler bookedTraveler,
            BookedTraveler bookedTravelerUpdate,
            String bookedLegCode,
            PNR pnr
    ) throws Exception {

        // Emergency contact info
        if (bookedTravelerUpdate.getDeclinedEmergencyContact()) {

            // Check if the information is different
            if (!bookedTraveler.getDeclinedEmergencyContact()) {

                BookedSegment bookedSegment = pnr.getFirstBookedSegmentOnTime(bookedLegCode);

                if (null == bookedSegment || null == bookedSegment.getSegment()) {
                    LOG.error("CHECKIN_ERROR: DESCRIPTION: {} LEGCODE: {} PNR: {}",ErrorCodeDescriptions.MSG_CODE_BOOKEDSEGMENT_NOT_FOUND,bookedLegCode
                            ,pnr.getPnr());
                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.BOOKEDSEGMENT_NOT_FOUND,
                            ErrorType.BOOKEDSEGMENT_NOT_FOUND.getFullDescription() + " LegCode : " + bookedLegCode
                                    + " - PNR : " + pnr.getPnr());
                }

                BookingClass bookingClass = ShoppingCartUtil.getBookingClassMap(bookedTraveler,
                        bookedSegment.getSegment().getSegmentCode());

                if (null == bookingClass) {
                    LOG.error("CHECKIN_ERROR: DESCRIPTION: {} PASSENGER ID: {} PNR: {}",ErrorCodeDescriptions.MSG_CODE_ERROR_NOT_FOUND_BOOKINGCLASS_FOR_SEGMENTCODE_IN_SEGMENT
                            + bookedSegment.getSegment().getSegmentCode(),bookedTraveler.getId()
                            ,pnr.getPnr());
                    throw new GenericException(Response.Status.BAD_REQUEST,
                            ErrorType.NOT_EXIST_BOOKING_CLASS_IN_PASSENGER,
                            ErrorType.NOT_EXIST_BOOKING_CLASS_IN_PASSENGER.getFullDescription() + "FOR SEGMENTCODE: "
                                    + bookedSegment.getSegment().getSegmentCode() + "PASSENGER ID: "
                                    + bookedTraveler.getId() + " - PNR :" + pnr.getPnr());
                }

                UpdateSecurityInfoRQType updateSecurityInfoRQ = getPassengerSecurityRQEmergencyContact(
                        bookedSegment.getSegment(), bookingClass, bookedTravelerUpdate, bookedTraveler
                );

                UpdateSecurityInfoRSType updateSecurityInfoRS = passengerSecurityService.passengerSecurity(
                        updateSecurityInfoRQ
                );

                if (!ErrorOrSuccessCode.SUCCESS.equals(updateSecurityInfoRS.getResult().getStatus())) {
                    ShoppingCartErrorUtil.sabreErrorPassengerSecurity(updateSecurityInfoRS);
                } else {
                    // Update emergency contact info in cartPNR
                    bookedTraveler.setDeclinedEmergencyContact(true);

                    // remove missing flag from ineligibleReasons}
                    PnrCollectionUtil.removeMissingInformation(bookedTraveler, CheckinIneligibleReasons.MEC.toString());
                }
            }

        } else if (null != bookedTravelerUpdate.getEmergencyContact()) {

            // Check if the information is different
            if (!bookedTravelerUpdate.getEmergencyContact().equals(bookedTraveler.getEmergencyContact())) {

                BookedSegment bookedSegment = pnr.getFirstBookedSegmentOnTime(bookedLegCode);

                if (null == bookedSegment || null == bookedSegment.getSegment()) {
                    LOG.error("CHECKIN_ERROR: DESCRIPTION: {} LEGCODE: {} PNR: {}",ErrorCodeDescriptions.MSG_CODE_BOOKEDSEGMENT_NOT_FOUND,bookedLegCode
                            ,pnr.getPnr());
                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.BOOKEDSEGMENT_NOT_FOUND,
                            ErrorType.BOOKEDSEGMENT_NOT_FOUND.getFullDescription() + " LegCode : " + bookedLegCode
                                    + " - PNR : " + pnr.getPnr());
                }

                BookingClass bookingClass = ShoppingCartUtil.getBookingClassMap(bookedTraveler,
                        bookedSegment.getSegment().getSegmentCode());

                if (null == bookingClass) {
                    LOG.error("CHECKIN_ERROR: DESCRIPTION: {} PASSENGER ID: {} PNR: {}",ErrorCodeDescriptions.MSG_CODE_ERROR_NOT_FOUND_BOOKINGCLASS_FOR_SEGMENTCODE_IN_SEGMENT
                            + bookedSegment.getSegment().getSegmentCode(),bookedTraveler.getId()
                            ,pnr.getPnr());
                    throw new GenericException(Response.Status.BAD_REQUEST,
                            ErrorType.NOT_EXIST_BOOKING_CLASS_IN_PASSENGER,
                            ErrorType.NOT_EXIST_BOOKING_CLASS_IN_PASSENGER.getFullDescription() + " FOR SEGMENTCODE : "
                                    + bookedSegment.getSegment().getSegmentCode() + "PASSENGER ID: "
                                    + bookedTraveler.getId() + " - PNR :" + pnr.getPnr());
                }

                ShoppingCartUtil.isEmptyEmergencyContact(bookedTravelerUpdate.getEmergencyContact());

                UpdateSecurityInfoRQType updateSecurityInfoRQ = getPassengerSecurityRQEmergencyContact(
                        bookedSegment.getSegment(),
                        bookingClass,
                        bookedTravelerUpdate,
                        bookedTraveler
                );

                UpdateSecurityInfoRSType updateSecurityInfoRS = passengerSecurityService.passengerSecurity(updateSecurityInfoRQ);

                if (!updateSecurityInfoRS.getResult().getStatus().equals(ErrorOrSuccessCode.SUCCESS)) {
                    ShoppingCartErrorUtil.sabreErrorPassengerSecurity(updateSecurityInfoRS);
                } else {
                    // Update emergency contact info in cartPNR
                    bookedTraveler.setEmergencyContact(bookedTravelerUpdate.getEmergencyContact());
                    bookedTraveler.setDeclinedEmergencyContact(false);

                    // remove missing flag from ineligibleReasons}
                    PnrCollectionUtil.removeMissingInformation(bookedTraveler, CheckinIneligibleReasons.MEC.toString());
                }
            }
        }
    }

    /**
     * @param bookedTraveler
     * @param bookedTravelerUpdate
     * @param pnr
     * @param cartId
     * @param cartPNR
     * @return
     * @throws Exception
     */
    private boolean updatedFrequentFlyerNumber(
            BookedTraveler bookedTraveler,
            BookedTraveler bookedTravelerUpdate,
            PNR pnr,
            String cartId,
            CartPNR cartPNR
    ) throws Exception {

        // FrequentFlyerNumber
        if (null != bookedTravelerUpdate.getFrequentFlyerNumber()) {

            if (null == bookedTravelerUpdate.getFrequentFlyerProgram()) {
                LOG.error("CHECKIN_ERROR_WARNING: CODE: {} DESCRIPTION: {} ",ErrorType.INVALID_REQUEST_FREQUENT_FLYER.getErrorCode(),ErrorType.INVALID_REQUEST_FREQUENT_FLYER.getFullDescription());
                Warning warning = new Warning();
                warning.setCode(ErrorType.INVALID_REQUEST_FREQUENT_FLYER.getErrorCode());
                warning.setMsg(ErrorType.INVALID_REQUEST_FREQUENT_FLYER.getFullDescription());
                cartPNR.getWarnings().addToList(warning);
                return false;
            }

            if (StringUtils.isBlank(bookedTravelerUpdate.getFrequentFlyerProgram())
                    || StringUtils.isBlank(bookedTravelerUpdate.getFrequentFlyerNumber())) {
                        LOG.error("CHECKIN_ERROR_WARNING: CODE: {} DESCRIPTION: {} FFN: {} FFP: {}",
                        ErrorType.EMPTY_FIELDS.getErrorCode(),ErrorCodeDescriptions.MSG_CODE_EXIST_EMPTY_FIELDS,bookedTravelerUpdate.getFrequentFlyerNumber(),bookedTravelerUpdate.getFrequentFlyerProgram());
                Warning warning = new Warning();
                warning.setCode(ErrorType.EMPTY_FIELDS.getErrorCode());
                warning.setMsg(ErrorCodeDescriptions.MSG_CODE_EXIST_EMPTY_FIELDS + " | " + "frequentFlyerNumber: "
                        + bookedTravelerUpdate.getFrequentFlyerNumber() + " , " + "frequentFlyerProgram: "
                        + bookedTravelerUpdate.getFrequentFlyerProgram());
                cartPNR.getWarnings().addToList(warning);

                return false;
            }

            if (null == bookedTraveler.getFrequentFlyerNumber()
                    || null == bookedTraveler.getFrequentFlyerProgram()
                    || !bookedTraveler.getFrequentFlyerNumber().equals(bookedTravelerUpdate.getFrequentFlyerNumber())
                    || !bookedTraveler.getFrequentFlyerProgram().equals(bookedTravelerUpdate.getFrequentFlyerProgram())) {

                SabrePassengerDTO sabrePassengerDTO = getSabrePassengerDTOFF(
                        bookedTraveler, bookedTravelerUpdate, pnr.getPnr()
                );

                PassengerDetailsRS passengerDetailsRS = passengerDetailsService.updatePassengerLoyalty(sabrePassengerDTO);

                if (!passengerDetailsRS.getApplicationResults().getWarning().isEmpty()
                        && 1 < passengerDetailsRS.getApplicationResults().getWarning().size()) {

                    ShoppingCartErrorUtil.frequentFlyerWarning(passengerDetailsRS, cartPNR.getWarnings());

                    return false;

                } else {

                    bookedTraveler.setFrequentFlyerNumber(bookedTravelerUpdate.getFrequentFlyerNumber());
                    bookedTraveler.setFrequentFlyerProgram(bookedTravelerUpdate.getFrequentFlyerProgram());

                    try {

                        GetPassengerDataRSACS passengerDataRS = passengerDataService.passengerDataByPNRLocator(pnr.getPnr(), true, false, null);
                        LOG.info("passengerDataRS after set ff:" + new Gson().toJson(passengerDataRS));

                        if (null != passengerDataRS) {
                            //Get the tier Level
                            TierLevelType tierLevelType = TierLevelType.getType(
                                    getTierLevelType(passengerDataRS, bookedTraveler)
                            );

                            if (null == tierLevelType) {
                                LOG.error("CHECKIN_ERROR: TierLevelType is: null");
                            } else {
                                FrequentFlyer frequentFlyer = new FrequentFlyer();
                                frequentFlyer.setNumber(bookedTravelerUpdate.getFrequentFlyerNumber());
                                frequentFlyer.setSupplierCode(bookedTravelerUpdate.getFrequentFlyerProgram());
                                frequentFlyer.setTierLevelNumber(null);
                                frequentFlyer.setTierTag(tierLevelType.getCode());
                                frequentFlyer.setTierPriority(
                                        RevenuePriorityCodeUtil.getTierPriority(frequentFlyer.getTierTag())
                                );
                                bookedTraveler.getLoyaltyNumbers().add(frequentFlyer);

                                bookedTraveler.setTierLevel(tierLevelType.getCode());

                                try {
                                    TierBenefit tierBenefitOffer = tierBenefitDao.getTierBenefitByCode(tierLevelType.getCode());

                                    if (null != tierBenefitOffer) {
                                        BookedLeg bookedLeg = pnr.getBookedLegByCartId(cartId);
                                        AirportWeather origin = airportsCodesDao.getAirportWeatherByIata(bookedLeg.getFirstSegmentInLeg().getSegment().getDepartureAirport());
                                        AirportWeather arrival = airportsCodesDao.getAirportWeatherByIata(bookedLeg.getLatestSegmentInLeg().getSegment().getArrivalAirport());

                                        // Benefits Cobrand
                                        TierBenefitCobrandList tierBenefitCobrandList = new TierBenefitCobrandList();
                                        if (null != bookedTraveler.getFrequentFlyerNumber() && null != bookedTraveler.getDateOfBirth()) {
                                            TierBenefitsRS tierBenefitsRS = getCobrandsCard(bookedTraveler);
                                            if (!tierBenefitsRS.getMember().get(0).getIsSuccess()) {
                                                LOG.error(tierBenefitsRS.getMember().get(0).getErrorDescription());
                                            } else {
                                                tierBenefitCobrandList = getBenefitsInformationByCard(bookedTraveler, tierBenefitsRS);
                                            }
                                        }

                                        BookedTravellerBenefitCollection BookedTravellerBenefitCollection = new BookedTravellerBenefitCollection();
                                        BookedTravellerBenefitCollection.getCollection().add(
                                                BenefitUtil.getTierBenefitCheckIn(
                                                        tierBenefitOffer,
                                                        tierBenefitCobrandList,
                                                        origin,
                                                        arrival,
                                                        cartPNR.getLegCode(),
                                                        bookedTraveler.getBookingClasses()
                                                )
                                        );

                                        bookedTraveler.setBookedTravellerBenefitCollection(BookedTravellerBenefitCollection);
                                    } else {
                                        LOG.error("CHECKIN_ERROR - tierBenefitOffer is the infomation about cp and is null");

                                        Warning warning = new Warning();
                                        warning.setCode(ErrorType.TIER_BENEFIT_NOT_FOUND.getErrorCode());
                                        warning.setMsg(ErrorType.TIER_BENEFIT_NOT_FOUND.getFullDescription());
                                        cartPNR.getWarnings().addToList(warning);

                                    }
                                } catch (Exception ex) {
                                    LOG.error(ex.getMessage(), ex);

                                    Warning warning = new Warning();
                                    warning.setCode(ErrorType.ERROR_RETRIEVING_BENEFIT.getErrorCode());
                                    warning.setMsg(ErrorType.ERROR_RETRIEVING_BENEFIT.getFullDescription());
                                    cartPNR.getWarnings().addToList(warning);
                                }
                            }
                        }
                    } catch (Exception e) {
                        LOG.error("CHECKIN_ERROR: Getting PassengerData | PNR:" + pnr.getPnr() + "| SABRE_ERR:" + e.getMessage(), e);

                        Warning warning = new Warning();
                        warning.setCode(ErrorType.ERROR_RETRIEVING_BENEFIT.getErrorCode());
                        warning.setMsg(ErrorType.ERROR_RETRIEVING_BENEFIT.getFullDescription());
                        cartPNR.getWarnings().addToList(warning);
                    }

                    return true;
                }
            }
        }

        return false;
    }

    private String getTierLevelType(GetPassengerDataRSACS passengerDataRS, BookedTraveler bookedTraveler) {
        String tierLevelType = null;
        try {
            PassengerDataResponseListACS passengerDataResponseListACS = passengerDataRS.getPassengerDataResponseList();
            for (PassengerDataResponseACS passengerDataResponseACS : passengerDataResponseListACS.getPassengerDataResponse()) {
                if (bookedTraveler.getId().equalsIgnoreCase(passengerDataResponseACS.getPassengerID())) {
                    if (null != passengerDataResponseACS.getPassengerItineraryList()) {
                        PassengerItineraryListACS passengerItineraryListACS = passengerDataResponseACS.getPassengerItineraryList();
                        if (null != passengerItineraryListACS && !passengerItineraryListACS.getPassengerItinerary().isEmpty()) {
                            tierLevelType = passengerItineraryListACS.getPassengerItinerary().get(0).getTierLevel();
                        }
                    }
                }
            }
        } catch (Exception ex) {
            tierLevelType = null;
        }
        return tierLevelType;
    }

    private TierBenefitsRS getCobrandsCard(BookedTraveler bookedTraveler) {
        TierBenefitsRS tierBenefitsRS = new TierBenefitsRS();
        try {
            String dateOfBirthFormat = PNRLookUpServiceUtil.getDateOfBirthFormatToCP(bookedTraveler.getDateOfBirth());
            tierBenefitsRS = tierBenefitService.getTierBenefits(bookedTraveler.getFrequentFlyerNumber(), dateOfBirthFormat);
            LOG.info("tierBenefitsRS: {}", new Gson().toJson(tierBenefitsRS));
            return tierBenefitsRS;
        } catch (Exception e) {
            LOG.info(e.getMessage());
        }

        return tierBenefitsRS;
    }

    private TierBenefitCobrandList getBenefitsInformationByCard(BookedTraveler bookedTraveler, TierBenefitsRS tierBenefitsRS) throws Exception {

        if (null == tierBenefitsRS
                || null == tierBenefitsRS.getMember()
                || tierBenefitsRS.getMember().isEmpty()
                || null == tierBenefitsRS.getMember().get(0).getCobrandCard()
                || tierBenefitsRS.getMember().get(0).getCobrandCard().trim().isEmpty()) {
            return new TierBenefitCobrandList();
        }

        List<String> lstCobrandCards;
        lstCobrandCards = Arrays.asList(tierBenefitsRS.getMember().get(0).getCobrandCard().split("\\|"));

        if (null == lstCobrandCards || lstCobrandCards.isEmpty()) {
            return new TierBenefitCobrandList();
        }

        TierBenefitCobrandList tierBenefitCobrandList = new TierBenefitCobrandList();
        int numCard = lstCobrandCards.size();

        if (numCard > 1) {
            List<TierBenefitCobrand> tierBenefitClubPremierList;
            tierBenefitClubPremierList = tierBenefitDao.getTierBenefitCobrandByCodeList(lstCobrandCards);
            if (null != tierBenefitClubPremierList) {
                tierBenefitCobrandList.addAll(tierBenefitClubPremierList);
            }

        } else if (numCard == 1) {
            TierBenefitCobrand tierBenefitClubPremier = tierBenefitDao.getTierBenefitCobrandByCode(lstCobrandCards.get(0));
            if (null != tierBenefitClubPremier) {
                tierBenefitCobrandList.add(tierBenefitClubPremier);
            }

        }

        return tierBenefitCobrandList;
    }

    /**
     * @param bookedTraveler
     * @param bookedTravelerUpdate
     * @param pnr
     * @throws Exception
     */
    private void updatedCorporateFrequentFlyerNumber(BookedTraveler bookedTraveler, BookedTraveler bookedTravelerUpdate,
                                                     PNR pnr) throws Exception {

        // CorporateFrequentFlyerNumber
        if (null != bookedTravelerUpdate.getCorporateFrequentFlyerNumber()) {

//			if (null == bookedTravelerUpdate.getCorporateFrequentFlyerProgram()) {
//				throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.CORPORATE_FFPROGRAM_NO_VALID,
//						ErrorType.CORPORATE_FFPROGRAM_NO_VALID.getFullDescription());
//			}
//			if ((null == bookedTraveler.getCorporateFrequentFlyerNumber() && null == bookedTraveler.getCorporateFrequentFlyerProgram())
//					|| (!bookedTraveler.getCorporateFrequentFlyerNumber().equals(bookedTravelerUpdate.getCorporateFrequentFlyerNumber())
//							|| !bookedTraveler.getCorporateFrequentFlyerProgram().equals(bookedTravelerUpdate.getCorporateFrequentFlyerProgram()))) {
            if (null == bookedTraveler.getCorporateFrequentFlyerNumber()
                    || !bookedTraveler.getCorporateFrequentFlyerNumber().equals(bookedTravelerUpdate.getCorporateFrequentFlyerNumber())) {

                if (bookedTravelerUpdate.getCorporateFrequentFlyerNumber().trim().isEmpty()) {
                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EMPTY_FIELDS,
                            ErrorCodeDescriptions.MSG_CODE_EXIST_EMPTY_FIELDS);
                }

                String cffProgram = null != bookedTravelerUpdate.getCorporateFrequentFlyerProgram() ? bookedTravelerUpdate.getCorporateFrequentFlyerProgram() : "AM";
                UpdateReservationRS updateReservationRS = updateReservationService.createOSI(
                        pnr.getPnr(),
                        cffProgram,
                        bookedTravelerUpdate.getCorporateFrequentFlyerNumber(),
                        bookedTraveler.getNameRefNumber()
                );

                if (null != updateReservationRS.getErrors()) {
                    throw new Exception(updateReservationRS.getErrors().getError().get(0).getMessage());
                }
                // Update CorporateFrequentFlyerNumber info in cartPNR
                bookedTraveler.setCorporateFrequentFlyerNumber(bookedTravelerUpdate.getCorporateFrequentFlyerNumber());
                bookedTraveler.setCorporateFrequentFlyerProgram(cffProgram);
            }
        }

    }

    /**
     * @param bookedTraveler
     * @param bookedTravelerUpdate
     * @param pnr
     * @param bookedLegCode
     * @return
     * @throws Exception
     */
    private boolean updatedTravelerDocument(
            BookedTraveler bookedTraveler,
            BookedTraveler bookedTravelerUpdate,
            PNR pnr,
            String bookedLegCode
    ) throws Exception {

        // Traveler Document/passport
        if (null == bookedTravelerUpdate.getTravelDocument()) {
            return false;
        }

        // Check if the information is different
        if (null == bookedTraveler.getTravelDocument()
                || !bookedTraveler.getTravelDocument().equals(bookedTravelerUpdate.getTravelDocument())) {

            BookedLeg bookedLeg = pnr.getBookedLegByLegCode(bookedLegCode);

            if (null != bookedTravelerUpdate.getTravelDocument().getNationality()
                    && "US".equalsIgnoreCase(bookedTravelerUpdate.getTravelDocument().getNationality())
                    && null != bookedLeg
                    && RegionType.USA.equals(bookedLeg.getRegionKey())
                    && MarketType.TRANSBORDER.equals(bookedLeg.getMarketFlight())) {

                BookedSegment bookedSegment = pnr.getFirstBookedSegmentOnTime(bookedLegCode);

                if (null == bookedSegment || null == bookedSegment.getSegment()) {
                    LOG.error("CHECKIN_ERROR: DESCRIPTION: {} LEGCODE: {} PNR: {}",ErrorCodeDescriptions.MSG_CODE_BOOKEDSEGMENT_NOT_FOUND,bookedLegCode
                            ,pnr.getPnr());
                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.BOOKEDSEGMENT_NOT_FOUND,
                            ErrorType.BOOKEDSEGMENT_NOT_FOUND.getFullDescription() + " LegCode : " + bookedLegCode
                                    + "- PNR : " + pnr.getPnr());
                }

                BookingClass bookingClass = ShoppingCartUtil.getBookingClassMap(
                        bookedTraveler,
                        bookedSegment.getSegment().getSegmentCode()
                );

                if (null == bookingClass) {
                    LOG.error("CHECKIN_ERROR: DESCRIPTION: {} PASSENGER ID: {} PNR: {}",ErrorCodeDescriptions.MSG_CODE_ERROR_NOT_FOUND_BOOKINGCLASS_FOR_SEGMENTCODE_IN_SEGMENT
                            ,bookedSegment.getSegment().getSegmentCode()+bookedTraveler.getId()
                            ,pnr.getPnr());
                    throw new GenericException(Response.Status.BAD_REQUEST,
                            ErrorType.NOT_EXIST_BOOKING_CLASS_IN_PASSENGER,
                            ErrorType.NOT_EXIST_BOOKING_CLASS_IN_PASSENGER.getFullDescription()
                                    + " FOR SEGMENTCODE : " + bookedSegment.getSegment().getSegmentCode()
                                    + " PASSENGER ID : " + bookedTraveler.getId() + " - PNR :" + pnr.getPnr());
                }

                if (bookedTravelerUpdate.getCountryOfResidence() == null) {
                    bookedTravelerUpdate.setCountryOfResidence("US");
                }

                // RESINDENCE ADDRESS
                if (null == bookedTraveler.getCountryOfResidence()
                        || bookedTraveler.getCountryOfResidence().trim().isEmpty()) {
                    updatedCountryOfResidence(bookedTraveler, bookedTravelerUpdate, bookedLegCode, pnr);
                }

                // DESTINATION ADDRESS
                if (PnrCollectionUtil.findMissingInformation(bookedTraveler, CheckinIneligibleReasons.MDA.toString())) {

                    // adult
                    UpdateSecurityInfoRQType updateSecurityInfoRQ = getDefaultPassengerSecurityRQDestinationAddress(
                            bookedSegment.getSegment(),
                            bookingClass,
                            bookedTraveler,
                            "US",
                            ADULT
                    );

                    UpdateSecurityInfoRSType updateSecurityInfoRS = passengerSecurityService.passengerSecurity(
                            updateSecurityInfoRQ
                    );

                    if (!updateSecurityInfoRS.getResult().getStatus().equals(ErrorOrSuccessCode.SUCCESS)) {
                        ShoppingCartErrorUtil.sabreErrorPassengerSecurity(updateSecurityInfoRS);
                    } else {
                        // remove missing flag from ineligibleReasons
                        PnrCollectionUtil.removeMissingInformation(bookedTraveler, CheckinIneligibleReasons.MDA.toString());

                        // Update destination address in cartPNR
                        bookedTraveler.setDestinationAddress(bookedTravelerUpdate.getDestinationAddress());
                    }
                }

                // if exist a missingCheckinRequiredFields for infant
                if (PnrCollectionUtil.findMissingInformation(bookedTraveler, CheckinIneligibleReasons.MDAI.toString())) {

                    //infant
                    UpdateSecurityInfoRQType updateSecurityInfoRQ = getDefaultPassengerSecurityRQDestinationAddress(
                            bookedSegment.getSegment(),
                            bookingClass,
                            bookedTraveler,
                            "US",
                            INFANT
                    );

                    UpdateSecurityInfoRSType updateSecurityInfoRS = passengerSecurityService.passengerSecurity(updateSecurityInfoRQ);

                    if (!updateSecurityInfoRS.getResult().getStatus().equals(ErrorOrSuccessCode.SUCCESS)) {
                        ShoppingCartErrorUtil.sabreErrorPassengerSecurity(updateSecurityInfoRS);
                    } else {
                        // remove missing flag from ineligibleReasons
                        PnrCollectionUtil.removeMissingInformation(bookedTraveler, CheckinIneligibleReasons.MDAI.toString());
                    }
                }
            }

            // add travel document
            BookedSegment bookedSegment = pnr.getFirstBookedSegmentOnTime(bookedLegCode);

            if (null == bookedSegment || null == bookedSegment.getSegment()) {
                LOG.error("CHECKIN_ERROR: DESCRIPTION: {} LEGCODE: {} PNR: {}",ErrorCodeDescriptions.MSG_CODE_BOOKEDSEGMENT_NOT_FOUND,bookedLegCode
                        ,pnr.getPnr());
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.BOOKEDSEGMENT_NOT_FOUND,
                        ErrorType.BOOKEDSEGMENT_NOT_FOUND.getFullDescription() + " LegCode : " + bookedLegCode
                                + " - PNR : " + pnr.getPnr());
            }

            if (null == bookedTraveler.getGender()) {
                LOG.error("CHECKIN_ERROR: DESCRIPTION: {} PASSENGER ID: {} ",ErrorType.NOT_EXIST_GENDER_IN_PASSENGER.getFullDescription(),
                        bookedTraveler.getId());
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.NOT_EXIST_GENDER_IN_PASSENGER,
                        ErrorType.NOT_EXIST_GENDER_IN_PASSENGER.getFullDescription() + " PASSENGER ID : "
                                + bookedTraveler.getId());
            }

            if (null == bookedTraveler.getDateOfBirth()) {
                LOG.error("CHECKIN_ERROR: DESCRIPTION: {} PASSENGER ID: {} ",ErrorType.DATEOFBIRTH_IS_REQUIRED.getFullDescription()
                        ,bookedTraveler.getId());
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.DATEOFBIRTH_IS_REQUIRED,
                        ErrorType.DATEOFBIRTH_IS_REQUIRED.getFullDescription() + " PASSENGER ID : "
                                + bookedTraveler.getId());

            }

            BookingClass bookingClass = ShoppingCartUtil.getBookingClassMap(bookedTraveler, bookedSegment.getSegment().getSegmentCode());

            if (null == bookingClass) {
                LOG.error("CHECKIN_ERROR: DESCRIPTION: {} PASSENGER ID: {} PNR: {}",ErrorCodeDescriptions.MSG_CODE_ERROR_NOT_FOUND_BOOKINGCLASS_FOR_SEGMENTCODE_IN_SEGMENT
                        + bookedSegment.getSegment().getSegmentCode(),bookedTraveler.getId()
                        ,pnr.getPnr());
                throw new GenericException(Response.Status.BAD_REQUEST,
                        ErrorType.NOT_EXIST_BOOKING_CLASS_IN_PASSENGER,
                        ErrorType.NOT_EXIST_BOOKING_CLASS_IN_PASSENGER.getFullDescription() + "FOR SEGMENTCODE: "
                                + bookedSegment.getSegment().getSegmentCode() + "PASSENGER ID: "
                                + bookedTraveler.getId() + " - PNR :" + pnr.getPnr());
            }

            ShoppingCartUtil.isEmptyTravelDocument(bookedTravelerUpdate.getTravelDocument());

            UpdateSecurityInfoRQType updateSecurityInfoRQ = getPassengerSecurityRQTravelDocument(
                    bookedSegment.getSegment(),
                    bookingClass,
                    bookedTravelerUpdate,
                    bookedTraveler,
                    bookedTravelerUpdate.getTravelDocument().isPassportScanned(),
                    false
            );
            UpdateSecurityInfoRSType updateSecurityInfoRS = passengerSecurityService.passengerSecurity(updateSecurityInfoRQ);

            if (!updateSecurityInfoRS.getResult().getStatus().equals(ErrorOrSuccessCode.SUCCESS)) {
                ShoppingCartErrorUtil.sabreErrorPassengerSecurity(updateSecurityInfoRS);
            } else {
                // Update travel document in cartPNR
                String numberDocumentValid = bookedTravelerUpdate.getTravelDocument().getDocumentNumber().replaceAll("[^A-Za-z0-9]", "");
                bookedTravelerUpdate.getTravelDocument().setDocumentNumber(numberDocumentValid);
                bookedTraveler.setTravelDocument(bookedTravelerUpdate.getTravelDocument());

                // remove missing flag from ineligibleReasons
                PnrCollectionUtil.removeMissingInformation(bookedTraveler, CheckinIneligibleReasons.MRT.toString());
            }

            return true;
        } else { // information is the same
            // remove missing flag from ineligibleReasons
            PnrCollectionUtil.removeMissingInformation(bookedTraveler, CheckinIneligibleReasons.MRT.toString());
        }

        return false;
    }

    private void addGeneralInfantInfo(
            BookedTraveler bookedTraveler,
            BookedTraveler bookedTravelerUpdate
    ) {

        // Update infant gender if it has a value other than null.
        if (bookedTravelerUpdate.getInfant().getGender() != null) {
            // Update infant gender in cartPNR
            bookedTraveler.getInfant().setGender(bookedTravelerUpdate.getInfant().getGender());
        }

        // Update infant DateOfBirth, if has value other than null.
        if (bookedTravelerUpdate.getInfant().getDateOfBirth() != null) {
            // Update infant date of birth in cartPNR
            bookedTraveler.getInfant().setDateOfBirth(bookedTravelerUpdate.getInfant().getDateOfBirth());
        }

        if(bookedTravelerUpdate.getInfant().getCountryOfResidence() != null){
            bookedTraveler.getInfant().setCountryOfResidence(bookedTravelerUpdate.getInfant().getCountryOfResidence());
        }

    }

    /**
     * @param bookedTraveler
     * @param bookedTravelerUpdate
     * @param pnr
     * @param bookedLegCode
     * @return
     * @throws Exception
     */
    private boolean updatedInfantInfo(
            BookedTraveler bookedTraveler,
            BookedTraveler bookedTravelerUpdate,
            PNR pnr,
            String bookedLegCode
    ) throws Exception {

        // Update infant travel document/passport

        // Check if the information is different
        if (null == bookedTraveler.getInfant().getTravelDocument()
                || !bookedTraveler.getInfant().getTravelDocument().equals(bookedTravelerUpdate.getInfant().getTravelDocument())) {

            BookedSegment bookedSegment = pnr.getFirstBookedSegmentOnTime(bookedLegCode);

            if (null == bookedSegment || null == bookedSegment.getSegment()) {
                LOG.error("CHECKIN_ERROR: DESCRIPTION: {} LEGCODE: {} PNR: {}",ErrorCodeDescriptions.MSG_CODE_BOOKEDSEGMENT_NOT_FOUND
                        ,bookedLegCode
                        ,pnr.getPnr()
                );
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.BOOKEDSEGMENT_NOT_FOUND,
                        ErrorType.BOOKEDSEGMENT_NOT_FOUND.getFullDescription()
                                + " LegCode : "
                                + bookedLegCode
                                + " - PNR : "
                                + pnr.getPnr()
                );
            }

            BookingClass bookingClass = ShoppingCartUtil.getBookingClassMap(
                    bookedTraveler,
                    bookedSegment.getSegment().getSegmentCode()
            );

            if (null == bookingClass) {
                LOG.error("CHECKIN_ERROR: DESCRIPTION: {} PASSENGER ID: {} PNR: {}",ErrorCodeDescriptions.MSG_CODE_ERROR_NOT_FOUND_BOOKINGCLASS_FOR_SEGMENTCODE_IN_SEGMENT
                        + bookedSegment.getSegment().getSegmentCode(),
                        bookedTraveler.getId(),pnr.getPnr());
                throw new GenericException(Response.Status.BAD_REQUEST,
                        ErrorType.NOT_EXIST_BOOKING_CLASS_IN_PASSENGER,
                        ErrorType.NOT_EXIST_BOOKING_CLASS_IN_PASSENGER.getFullDescription()
                                + " FOR SEGMENTCODE : " + bookedSegment.getSegment().getSegmentCode()
                                + " PASSENGER ID : " + bookedTraveler.getId() + " - PNR :" + pnr.getPnr());
            }

            if (null == bookedTraveler.getInfant().getGender()) {
                LOG.error("CHECKIN_ERROR: " + ErrorType.NOT_EXIST_GENDER_IN_PASSENGER.getFullDescription() + " INFANT - "
                        + " PASSENGER ID : " + bookedTraveler.getId());
                throw new GenericException(Response.Status.BAD_REQUEST,
                        ErrorType.NOT_EXIST_GENDER_IN_PASSENGER,
                        ErrorType.NOT_EXIST_GENDER_IN_PASSENGER.getFullDescription() + " INFANT - "
                                + " PASSENGER ID : " + bookedTraveler.getId());
            }

            if (null == bookedTraveler.getInfant().getDateOfBirth()) {
                LOG.error("CHECKIN_ERROR: " + ErrorType.DATEOFBIRTH_IS_REQUIRED.getFullDescription()
                        + " INFANT - "
                        + " PASSENGER ID : "
                        + bookedTraveler.getId()
                );

                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.DATEOFBIRTH_IS_REQUIRED,
                        ErrorType.DATEOFBIRTH_IS_REQUIRED.getFullDescription()
                                + " INFANT - "
                                + " PASSENGER ID : "
                                + bookedTraveler.getId()
                );
            }

            if (null == bookedTravelerUpdate.getInfant().getTravelDocument().getNationality()) {
                bookedTravelerUpdate.getInfant().getTravelDocument().setNationality(bookedTravelerUpdate.getCountryOfResidence());
            }

            // Check if the information is different
            if (null == bookedTraveler.getInfant().getTravelDocument()
                    || !bookedTraveler.getInfant().getTravelDocument().equals(bookedTravelerUpdate.getInfant().getTravelDocument())) {

                ShoppingCartUtil.isEmptyTravelDocumentInfant(bookedTravelerUpdate.getInfant().getTravelDocument());

                UpdateSecurityInfoRQType updateSecurityInfoRQ = getPassengerSecurityRQTravelDocument(
                        bookedSegment.getSegment(),
                        bookingClass,
                        bookedTravelerUpdate,
                        bookedTraveler,
                        bookedTravelerUpdate.getInfant().getTravelDocument().isPassportScanned(),
                        INFANT
                );
                UpdateSecurityInfoRSType updateSecurityInfoRS = passengerSecurityService.passengerSecurity(updateSecurityInfoRQ);

                if (!updateSecurityInfoRS.getResult().getStatus().equals(ErrorOrSuccessCode.SUCCESS)) {
                    ShoppingCartErrorUtil.sabreErrorPassengerSecurity(updateSecurityInfoRS);
                } else {
                    // update infant travel document in cartPNR
                    String numberDocumentValid = bookedTravelerUpdate.getInfant().getTravelDocument().getDocumentNumber().replaceAll("[^A-Za-z0-9]", "");
                    bookedTravelerUpdate.getInfant().getTravelDocument().setDocumentNumber(numberDocumentValid);
                    bookedTraveler.getInfant().setTravelDocument(bookedTravelerUpdate.getInfant().getTravelDocument());

                    // remove missing flag from ineligibleReasons
                    PnrCollectionUtil.removeMissingInformation(bookedTraveler, CheckinIneligibleReasons.MRTI.toString());

                    return true;
                }
            }
        } else {// information is the same
            // remove missing flag from ineligibleReasons
            PnrCollectionUtil.removeMissingInformation(bookedTraveler, CheckinIneligibleReasons.MRTI.toString());
        }

        return false;
    }

    private void updatedOverBookingInfo(
            BookedTraveler bookedTraveler, BookedTraveler bookedTravelerUpdate,
            String bookedLegCode, PNR pnr,
            WarningCollection warningCollection, ShoppingCartRQ shoppingCartRQ
    ) {

        try {
            if (bookedTravelerUpdate.getOverBookingInfo().getAccepted()) {
                // An offer was submitted. Capture the offer details in the
                // flight remarks.
                addRemark(bookedLegCode, pnr, bookedTraveler, bookedTravelerUpdate);

                // Add the passenger to the priority list with OVS priority
                // code.
                String overBookingPriorityCode = PriorityCodeType.OVS.getCode();
                checkInService.addPassengersToPriorityList(
                        bookedTraveler,
                        pnr.getBookedLegByLegCode(bookedLegCode),
                        pnr.getPnr(),
                        shoppingCartRQ,
                        overBookingPriorityCode
                );

                // Add VOL edit code so that we don't ask the same passenger
                // for an offer again.
                addEditCode(bookedTraveler, bookedTravelerUpdate, bookedLegCode, pnr, warningCollection, "VOL");
            } else {
                // The passenger rejected to submit an offer. Add VOLX so
                // that we don't ask the same passenger again.
                addEditCode(bookedTraveler, bookedTravelerUpdate, bookedLegCode, pnr, warningCollection, "VOLX");
            }
        } catch (Exception e) {
            // If we fail for any reason, don't return an error.
            LOG.error("CHECKIN_ERROR: Failed to capture overbooking offer: " + e.getMessage());
        }
    }

    private boolean addRemark(String bookedLegCode, PNR pnr, BookedTraveler bookedTraveler,
                              BookedTraveler bookedTravelerUpdate) throws Exception {

        String remark = "VOLUNTEER " + pnr.getPnr() + " "
                + bookedTravelerUpdate.getOverBookingInfo().getAmount().getAmount() + " "
                + bookedTravelerUpdate.getOverBookingInfo().getAmount().getCurrency() + " "
                + bookedTraveler.getLastName() + "/" + bookedTraveler.getFirstName();
        BookedSegment bookedSegment = pnr.getFirstBookedSegmentOnTime(bookedLegCode);
        Segment segment = bookedSegment.getSegment();

        return amFlightRemarksService.setFlightRemark(segment.getOperatingFlightCode(), segment.getDepartureAirport(),
                TimeUtil.parseDateOfRemark(segment.getDepartureDateTime()), "R", remark, null);
        // return updateReservationService.createRemark(pnr.getPnr(),
        // litsRemarks, "CHECKIN_API", null);
    }

    /**
     * @param bookedTraveler
     * @param bookedTravelerUpdate
     * @param pnr
     * @param warningCollection
     * @throws Exception
     */
    private void updatedKTN(
            BookedTraveler bookedTraveler,
            BookedTraveler bookedTravelerUpdate,
            PNR pnr,
            WarningCollection warningCollection
    ) throws Exception {

        // reset
        if (null == warningCollection) {
            warningCollection = new WarningCollection();
        }
        // KTN
        if (null != bookedTravelerUpdate.getKtn()) {

            if (null == bookedTraveler.getKtn()
                    || !bookedTraveler.getKtn().equalsIgnoreCase(bookedTravelerUpdate.getKtn())) {

                if (StringUtils.isBlank(bookedTravelerUpdate.getKtn())) {
                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EMPTY_FIELDS,
                            ErrorCodeDescriptions.MSG_CODE_EXIST_EMPTY_FIELDS + ": KTN");
                }
                SabrePassengerDTO sabrePassengerDTO = getSabrePassengerDTOKtn(bookedTraveler, bookedTravelerUpdate,
                        pnr.getPnr());
                PassengerDetailsRS passengerDetailsRS;
                passengerDetailsRS = passengerDetailsService.updatePassengerPersonalInfo(sabrePassengerDTO);
//				LOG.info("PassengerDetailsRS - PassengerDetailsRS-KTN: " + new Gson().toJson(passengerDetailsRS));
                if (!passengerDetailsRS.getApplicationResults().getWarning().isEmpty()
                        && 1 < passengerDetailsRS.getApplicationResults().getWarning().size()) {
                    ShoppingCartErrorUtil.frequentFlyerWarning(passengerDetailsRS, warningCollection);

                    return;
                }
                // Update Known Traveler Number info in cartPNR
                bookedTraveler.setKtn(bookedTravelerUpdate.getKtn());
            }
        }
    }

    /**
     * @param bookedTraveler
     * @param bookedTravelerUpdate
     * @param bookedLegCode
     * @param pnr
     * @throws Exception
     */
    private void updatedRedress(BookedTraveler bookedTraveler, BookedTraveler bookedTravelerUpdate, String bookedLegCode, PNR pnr) throws Exception {

        // reddressNumber
        if (null != bookedTravelerUpdate.getRedressNumber()) {

            // Check if the information is different
            if (null == bookedTraveler.getRedressNumber()
                    || (!bookedTravelerUpdate.getRedressNumber().equalsIgnoreCase(bookedTraveler.getRedressNumber()))) {

                BookedSegment bookedSegment = pnr.getFirstBookedSegmentOnTime(bookedLegCode);

                if (null == bookedSegment || null == bookedSegment.getSegment()) {
                    LOG.error("CHECKIN_ERROR: DESCRIPTION: {} LEGCODE: {} PNR: {}",ErrorCodeDescriptions.MSG_CODE_BOOKEDSEGMENT_NOT_FOUND,bookedLegCode
                            ,pnr.getPnr());
                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.BOOKEDSEGMENT_NOT_FOUND,
                            ErrorType.BOOKEDSEGMENT_NOT_FOUND.getFullDescription() + " LegCode : " + bookedLegCode
                                    + " - PNR : " + pnr.getPnr());
                }

                BookingClass bookingClass = ShoppingCartUtil.getBookingClassMap(bookedTraveler,
                        bookedSegment.getSegment().getSegmentCode());

                if (null == bookingClass) {
                    LOG.error("CHECKIN_ERROR: DESCRIPTION: {} PASSENGER ID: {} PNR: {}",ErrorCodeDescriptions.MSG_CODE_ERROR_NOT_FOUND_BOOKINGCLASS_FOR_SEGMENTCODE_IN_SEGMENT
                            + bookedSegment.getSegment().getSegmentCode(),bookedTraveler.getId()
                            ,pnr.getPnr());
                    throw new GenericException(Response.Status.BAD_REQUEST,
                            ErrorType.NOT_EXIST_BOOKING_CLASS_IN_PASSENGER,
                            ErrorType.NOT_EXIST_BOOKING_CLASS_IN_PASSENGER.getFullDescription() + " FOR SEGMENTCODE : "
                                    + bookedSegment.getSegment().getSegmentCode() + " PASSENGER ID : "
                                    + bookedTraveler.getId() + " - PNR : " + pnr.getPnr());
                }

                if (bookedTravelerUpdate.getRedressNumber().trim().isEmpty()) {
                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EMPTY_FIELDS,
                            ErrorCodeDescriptions.MSG_CODE_INVALID_COUNTRY_OF_RESIDENCE);
                }

                // adult
                boolean infant = false;

                UpdateSecurityInfoRQType updateSecurityInfoRQ = getPassengerSecurityRQRedressNumber(bookedSegment.getSegment(), bookingClass, bookedTravelerUpdate, bookedTraveler, infant);
                UpdateSecurityInfoRSType updateSecurityInfoRS = passengerSecurityService.passengerSecurity(updateSecurityInfoRQ);

                //LOG.debug("PassengerSecurity - RedressNumber: " + new Gson().toJson(acsPassengerSecurityRSACS));
                if (!updateSecurityInfoRS.getResult().getStatus().equals(ErrorOrSuccessCode.SUCCESS)) {
                    ShoppingCartErrorUtil.sabreErrorPassengerSecurity(updateSecurityInfoRS);
                }

                // Update country of residence in cartPNR
                bookedTraveler.setRedressNumber(bookedTravelerUpdate.getRedressNumber());
            }
        }
    }

    /**
     * @param bookedTraveler
     * @param bookedTravelerUpdate
     * @param bookedLegCode
     * @param pnr
     * @param warningCollection
     * @throws Exception
     */
    private boolean addEditCode(BookedTraveler bookedTraveler, BookedTraveler bookedTravelerUpdate,
                                String bookedLegCode, PNR pnr, WarningCollection warningCollection, String editCode) throws Exception {

//		LOG.info("addEditCode({}, {})", bookedTraveler.getId(), editCode);
        BookedSegment bookedSegment = pnr.getFirstBookedSegmentOnTime(bookedLegCode);

        if (null == bookedSegment || null == bookedSegment.getSegment()) {
            LOG.error("CHECKIN_ERROR: "+ErrorCodeDescriptions.MSG_CODE_BOOKEDSEGMENT_NOT_FOUND + " LegCode:{} PNR:{} PASSENGER ID:{}",
                    bookedLegCode, pnr.getPnr(), bookedTraveler.getId());
            return false;
        }

        BookingClass bookingClass = ShoppingCartUtil.getBookingClassMap(bookedTraveler,
                bookedSegment.getSegment().getSegmentCode());

        if (null == bookingClass) {
            LOG.error("CHECKIN_ERROR: "+
                    ErrorCodeDescriptions.MSG_CODE_ERROR_NOT_FOUND_BOOKINGCLASS_FOR_SEGMENTCODE_IN_SEGMENT
                            + "SEGMENT:{} PNR:{} PASSENGER ID:{}",
                    bookedSegment.getSegment().getSegmentCode(), pnr.getPnr(), bookedTraveler.getId());
            return false;
        }

        editPassCharService.editPassengerChararcteristics(
                ShoppingCartUtil.getEditPassengerCharacteristicsRQCode(
                        bookedSegment.getSegment(), bookingClass, bookedTraveler, editCode)
        );
//		LOG.info("addEditCode({}, {}) was added.", bookedTraveler.getId(), editCode);
        return true;
    }

    /**
     * @param segment
     * @param bookingClass
     * @param bookedTravelerUpdate
     * @param bookedTraveler
     * @param infant
     * @return
     * @throws Exception
     */
    private UpdateSecurityInfoRQType getPassengerSecurityRQResidenceAddress(Segment segment, BookingClass bookingClass, BookedTraveler bookedTravelerUpdate, BookedTraveler bookedTraveler, boolean infant) throws Exception {

        UpdateSecurityInfoRQType passengerSecurityRQ = new UpdateSecurityInfoRQType();

        passengerSecurityRQ.setItinerary(getSecurityItinerayACS(segment, bookingClass));
        passengerSecurityRQ.setPassengerInfo(getPassengerInfoType(bookedTraveler));
        passengerSecurityRQ.setPassengerSecurityInfo(getResidenceAddress(bookedTravelerUpdate.getCountryOfResidence(), infant));

        return passengerSecurityRQ;
    }

    private UpdateSecurityInfoRQType getPassengerSecurityRQVisa(Segment segment, BookingClass bookingClass, BookedTraveler bookedTravelerUpdate, BookedTraveler bookedTraveler, boolean isInfant) throws Exception {

        UpdateSecurityInfoRQType passengerSecurityRQ = new UpdateSecurityInfoRQType();

        passengerSecurityRQ.setItinerary(getSecurityItinerayACS(segment, bookingClass));
        passengerSecurityRQ.setPassengerInfo(getPassengerInfoType(bookedTraveler));

        if (isInfant && bookedTravelerUpdate.getInfant() != null && bookedTravelerUpdate.getInfant().getVisaInfo() != null) {
            passengerSecurityRQ.setPassengerSecurityInfo(getVisaSecurityInfoType(bookedTravelerUpdate.getInfant().getVisaInfo(), bookingClass, INFANT));
        } else if (!isInfant && bookedTravelerUpdate.getVisaInfo() != null) {
            passengerSecurityRQ.setPassengerSecurityInfo(getVisaSecurityInfoType(bookedTravelerUpdate.getVisaInfo(), bookingClass, ADULT));
        } else {
            //TODO: log an error. Invalid request.
        }

        return passengerSecurityRQ;
    }

    /**
     * @param segment
     * @param bookingClass
     * @param bookedTravelerUpdate
     * @param bookedTraveler
     * @param isInfant
     * @return
     * @throws Exception
     */
    private UpdateSecurityInfoRQType getPassengerSecurityRQTimaticInfo(
            Segment segment,
            BookingClass bookingClass,
            BookedTraveler bookedTravelerUpdate,
            BookedTraveler bookedTraveler,
            boolean isInfant
    ) throws Exception {

        LOG.info("getPassengerSecurityRQTimaticInfo( infant: {})", isInfant);

        UpdateSecurityInfoRQType passengerSecurityRQ = new UpdateSecurityInfoRQType();

        passengerSecurityRQ.setItinerary(getSecurityItinerayACS(segment, bookingClass));
        passengerSecurityRQ.setPassengerInfo(getPassengerInfoType(bookedTraveler));

        if (isInfant && bookedTravelerUpdate.getInfant() != null) {
            passengerSecurityRQ.setPassengerSecurityInfo(
                    getTimaticInfoInfan(bookedTravelerUpdate.getInfant().getTimaticInfo(), INFANT, bookedTraveler)
                    /*getTimaticInfo(
                            bookedTravelerUpdate.getInfant().getTimaticInfo(), INFANT
                    )*/
            );

            if (null != bookedTravelerUpdate.getInfant().getTimaticInfo()
                    && null != bookedTravelerUpdate.getInfant().getTimaticInfo().getReturnDate()
                    && !bookedTravelerUpdate.getInfant().getTimaticInfo().getReturnDate().trim().isEmpty()) {

                if (null != bookedTraveler.getInfant() && null != bookedTraveler.getInfant().getTimaticInfo()) {
                    bookedTraveler.getInfant().setTimaticProvided(true);
                    bookedTraveler.getInfant().setReturningDateAlreadySent(true);
                    bookedTraveler.getInfant().getTimaticInfo().setReturningDateRequired(false);
                }
            }
        } else if (!isInfant) {
            passengerSecurityRQ.setPassengerSecurityInfo(
                    getTimaticInfo(
                            bookedTravelerUpdate.getTimaticInfo(), ADULT
                    )
            );

            if (null != bookedTravelerUpdate.getTimaticInfo()
                    && null != bookedTravelerUpdate.getTimaticInfo().getReturnDate()
                    && !bookedTravelerUpdate.getTimaticInfo().getReturnDate().trim().isEmpty()) {

                if (null != bookedTraveler.getTimaticInfo()) {
                    bookedTraveler.setTimaticProvided(true);
                    bookedTraveler.getTimaticInfo().setReturningDateRequired(false);
                    bookedTraveler.setReturningDateAlreadySent(true);
                }
            }

        } else {
            //TODO: log an error, invalid request.
            LOG.error("CHECKIN_ERROR: Invalid TIMATIC request");
        }

        return passengerSecurityRQ;
    }

    /**
     * @param segment
     * @param bookingClass
     * @param bookedTraveler
     * @param isInfant
     * @return
     * @throws Exception
     */
    private UpdateSecurityInfoRQType getPassengerSecurityRQTimaticStatus(
            Segment segment, BookingClass bookingClass,
            BookedTraveler bookedTraveler, boolean isInfant
    ) throws Exception {

        UpdateSecurityInfoRQType passengerSecurityRQ = new UpdateSecurityInfoRQType();

        passengerSecurityRQ.setItinerary(getSecurityItinerayACS(segment, bookingClass));
        passengerSecurityRQ.setPassengerInfo(getPassengerInfoType(bookedTraveler));
        passengerSecurityRQ.setPassengerSecurityInfo(getTimaticStatusSecurityInfo(isInfant));

        return passengerSecurityRQ;
    }

    private PassengerInfoType getPassengerInfoType(BookedTraveler bookedTraveler) {
        PassengerInfoType passengerInfoType = new PassengerInfoType();
        passengerInfoType.setLastName(bookedTraveler.getLastName());
        passengerInfoType.setFirstName(bookedTraveler.getFirstName());
        passengerInfoType.setPassengerID(bookedTraveler.getId());
        return passengerInfoType;
    }

    /**
     * @param segment
     * @param bookingClass
     * @param bookedTravelerUpdate
     * @param bookedTraveler
     * @param infant
     * @return
     * @throws Exception
     */
    private UpdateSecurityInfoRQType getPassengerSecurityRQDestinationAddress(
            Segment segment,
            BookingClass bookingClass,
            BookedTraveler bookedTravelerUpdate,
            BookedTraveler bookedTraveler,
            boolean infant
    ) throws Exception {

        UpdateSecurityInfoRQType passengerSecurityRQ = new UpdateSecurityInfoRQType();

        passengerSecurityRQ.setItinerary(getSecurityItinerayACS(segment, bookingClass));
        passengerSecurityRQ.setPassengerInfo(getPassengerInfoType(bookedTraveler));
        passengerSecurityRQ.setPassengerSecurityInfo(
                getDestinationAddress(
                        bookedTravelerUpdate,
                        infant
                )
        );

        return passengerSecurityRQ;
    }

    /**
     * @param segment
     * @param bookingClass
     * @param bookedTraveler
     * @param countryCode
     * @param infant
     * @return
     * @throws Exception
     */
    private UpdateSecurityInfoRQType getDefaultPassengerSecurityRQDestinationAddress(
            Segment segment,
            BookingClass bookingClass,
            BookedTraveler bookedTraveler,
            String countryCode,
            boolean infant
    ) throws Exception {

        UpdateSecurityInfoRQType passengerSecurityRQ = new UpdateSecurityInfoRQType();

        passengerSecurityRQ.setItinerary(getSecurityItinerayACS(segment, bookingClass));
        passengerSecurityRQ.setPassengerInfo(getPassengerInfoType(bookedTraveler));
        passengerSecurityRQ.setPassengerSecurityInfo(
                getDefaultDestinationAddress(
                        countryCode,
                        infant
                )
        );

        return passengerSecurityRQ;
    }

    /**
     * @param segment
     * @param bookingClass
     * @param bookedTraveler
     * @return
     * @throws Exception
     */
    private UpdateSecurityInfoRQType getPassengerSecurityRQEmergencyContact(
            Segment segment,
            BookingClass bookingClass,
            BookedTraveler bookedTraveler
    ) throws Exception {

        UpdateSecurityInfoRQType passengerSecurityRQ = new UpdateSecurityInfoRQType();

        passengerSecurityRQ.setItinerary(
                getSecurityItinerayACS(segment, bookingClass)
        );
        passengerSecurityRQ.setPassengerInfo(getPassengerInfoType(bookedTraveler));
        passengerSecurityRQ.setPassengerSecurityInfo(
                transformShoppingCartPassengerEmergencyContact()
        );

        return passengerSecurityRQ;
    }

    /**
     * @param segment
     * @param bookingClass
     * @param bookedTravelerUpdate
     * @param bookedTraveler
     * @return
     * @throws Exception
     */
    private UpdateSecurityInfoRQType getPassengerSecurityRQEmergencyContact(
            Segment segment,
            BookingClass bookingClass,
            BookedTraveler bookedTravelerUpdate,
            BookedTraveler bookedTraveler
    ) throws Exception {

        UpdateSecurityInfoRQType passengerSecurityRQ = new UpdateSecurityInfoRQType();

        passengerSecurityRQ.setItinerary(getSecurityItinerayACS(segment, bookingClass));
        passengerSecurityRQ.setPassengerInfo(getPassengerInfoType(bookedTraveler));
        passengerSecurityRQ.setPassengerSecurityInfo(
                transformShoppingCartPassengerEmergencyContact(bookedTravelerUpdate)
        );

        return passengerSecurityRQ;
    }

    private SabrePassengerDTO getSabrePassengerDTOFF(
            BookedTraveler bookedTraveler,
            BookedTraveler bookedTravelerUpdate,
            String pnr
    ) {
        SabrePassengerDTO sabrePassengerDTO = new SabrePassengerDTO(bookedTraveler.getNameRefNumber());
        sabrePassengerDTO.setFrequentFlyerNumber(bookedTravelerUpdate.getFrequentFlyerNumber());
        sabrePassengerDTO.setFrequentFlyerProgram(bookedTravelerUpdate.getFrequentFlyerProgram());
        sabrePassengerDTO.setFirstName(bookedTraveler.getFirstName());
        sabrePassengerDTO.setLastName(bookedTraveler.getLastName());
        sabrePassengerDTO.setPnr(pnr);
        return sabrePassengerDTO;
    }

    /**
     * @param segment
     * @param bookingClass
     * @param bookedTravelerUpdate
     * @param bookedTraveler
     * @return
     * @throws Exception
     */
    private UpdateSecurityInfoRQType getPassengerSecurityRQTravelDocument(Segment segment, BookingClass bookingClass, BookedTraveler bookedTravelerUpdate, BookedTraveler bookedTraveler, boolean swipeIndicator, boolean infant) throws Exception {

        UpdateSecurityInfoRQType passengerSecurityRQ = new UpdateSecurityInfoRQType();

        passengerSecurityRQ.setItinerary(getSecurityItinerayACS(segment, bookingClass));
        passengerSecurityRQ.setPassengerInfo(getPassengerInfoType(bookedTraveler));
        passengerSecurityRQ.setPassengerSecurityInfo(getTravelDocument(bookedTravelerUpdate, bookedTraveler, swipeIndicator, infant));

        return passengerSecurityRQ;
    }

    private SabrePassengerDTO getSabrePassengerDTOKtn(BookedTraveler bookedTraveler, BookedTraveler bookedTravelerUpdate, String pnr) {
        SabrePassengerDTO sabrePassengerDTO = new SabrePassengerDTO(bookedTraveler.getNameRefNumber());
        // sabrePassengerDTO.setNameRef(bookedTraveler.getNameRefNumber());
        sabrePassengerDTO.setKtn(bookedTravelerUpdate.getKtn());
        sabrePassengerDTO.setFirstName(bookedTraveler.getFirstName());
        sabrePassengerDTO.setLastName(bookedTraveler.getLastName());
        sabrePassengerDTO.setDateOfBirth(bookedTraveler.getDateOfBirth());
        sabrePassengerDTO.setPnr(pnr);
        return sabrePassengerDTO;
    }

    /**
     * @param segment
     * @param bookingClass
     * @param bookedTravelerUpdate
     * @param bookedTraveler
     * @param infant
     * @return
     * @throws Exception
     */
    private UpdateSecurityInfoRQType getPassengerSecurityRQRedressNumber(Segment segment, BookingClass bookingClass, BookedTraveler bookedTravelerUpdate, BookedTraveler bookedTraveler, boolean infant) throws Exception {

        UpdateSecurityInfoRQType passengerSecurityRQ = new UpdateSecurityInfoRQType();

        passengerSecurityRQ.setItinerary(getSecurityItinerayACS(segment, bookingClass));
        passengerSecurityRQ.setPassengerInfo(getPassengerInfoType(bookedTraveler));
        passengerSecurityRQ.setPassengerSecurityInfo(getRedressNumber(bookedTravelerUpdate, bookedTraveler, bookingClass, infant));

        return passengerSecurityRQ;
    }

    /**
     * @return
     */
    private PassengerSecurityInfoType transformShoppingCartPassengerEmergencyContact() {

        PassengerSecurityInfoType passengerSecurityInfo = new PassengerSecurityInfoType();
        PassengerDocumentsType passengerDocuments = new PassengerDocumentsType();
        PassengerContactInfo passengerContactInfo = new PassengerContactInfo();

        passengerContactInfo.setAction(ActionType.EDIT);
        passengerDocuments.setPassengerContactInfo(passengerContactInfo);
        passengerSecurityInfo.setManageSecurityInfo(passengerDocuments);

        return passengerSecurityInfo;
    }

    /**
     * @param bookedTravelerUpdate
     * @return
     */
    private PassengerSecurityInfoType transformShoppingCartPassengerEmergencyContact(BookedTraveler bookedTravelerUpdate) {

        PassengerSecurityInfoType passengerSecurityInfo = new PassengerSecurityInfoType();
        PassengerDocumentsType passengerDocuments = new PassengerDocumentsType();
        PassengerContactInfo passengerContactInfo = new PassengerContactInfo();

        passengerContactInfo.setAction(ActionType.EDIT);
        passengerDocuments.setPassengerContactInfo(passengerContactInfo);
        passengerSecurityInfo.setManageSecurityInfo(passengerDocuments);

        if (null != bookedTravelerUpdate
                && !bookedTravelerUpdate.getDeclinedEmergencyContact()
                && null != bookedTravelerUpdate.getEmergencyContact()) {

            Phone phone = new Phone();
            phone.setCountryCode(bookedTravelerUpdate.getEmergencyContact().getPhone().getCountry());
            phone.setValue(bookedTravelerUpdate.getEmergencyContact().getPhone().getNumber());
            passengerContactInfo.setPhone(phone);
            passengerContactInfo.setName(bookedTravelerUpdate.getEmergencyContact().getFirstName().toUpperCase());
            //passengerContactInfo.setRelatedData("WORK");
        }

        return passengerSecurityInfo;
    }

    /**
     * @param segment
     * @param bookingClass
     * @return
     * @throws Exception
     */
    private ItineraryType getSecurityItinerayACS(Segment segment, BookingClass bookingClass) throws Exception {

        ItineraryType itineraryType = new ItineraryType();

        itineraryType.setAirline(segment.getOperatingCarrier());
        itineraryType.setFlight(segment.getOperatingFlightCode());
        itineraryType.setDepartureDate(ShoppingCartUtil.getDepartureDate(segment.getDepartureDateTime()));
        itineraryType.setOrigin(segment.getDepartureAirport());
        itineraryType.setBookingClass(bookingClass.getBookingClass());

        return itineraryType;
    }

    /**
     * @param countryCode
     * @param infant
     * @return
     * @throws Exception
     */
    private PassengerSecurityInfoType getResidenceAddress(String countryCode, boolean infant) throws Exception {

        PassengerSecurityInfoType passengerSecurityInfo = new PassengerSecurityInfoType();

        PassengerDocumentsType passengerDocuments = new PassengerDocumentsType();
        passengerSecurityInfo.setManageSecurityInfo(passengerDocuments);

        AddressInfoType addressInfo = new AddressInfoType();
        addressInfo.setAction(ActionType.EDIT);
        addressInfo.setInfant(infant);
        Address address = new Address();
        addressInfo.getAddress().add(address);
        address.setType("RESIDENT");
        address.setCountryCode(ShoppingCartUtil.formatStringUpperCaseSize(countryCode, 3));
        passengerDocuments.setAddressInfoList(addressInfo);

        return passengerSecurityInfo;
    }

    /**
     * @param isInfant
     * @return
     * @throws Exception
     */
    private PassengerSecurityInfoType getTimaticStatusSecurityInfo(boolean isInfant) throws Exception {

        LOG.info("getTimaticStatusSecurityInfo(infant:{})", isInfant);

        PassengerSecurityInfoType passengerSecurityInfo = new PassengerSecurityInfoType();

        SecurityChecksType securityChecksType = new SecurityChecksType();
        RequestTimatic requestTimatic = new RequestTimatic();
        requestTimatic.setInfant(isInfant);
        securityChecksType.setRequestTimatic(requestTimatic);
        passengerSecurityInfo.setValidateSecurityInfo(securityChecksType);

        return passengerSecurityInfo;
    }

    /**
     * @param timaticInfo
     * @param infant
     * @return
     * @throws Exception
     */
    private PassengerSecurityInfoType getTimaticInfo(
            TimaticInfo timaticInfo,
            boolean infant
    ) throws Exception {

        PassengerSecurityInfoType passengerSecurityInfo = new PassengerSecurityInfoType();

        PassengerDocumentsType passengerDocuments = new PassengerDocumentsType();
        passengerSecurityInfo.setManageSecurityInfo(passengerDocuments);

        TimaticDocsType timaticDocs = new TimaticDocsType();
        timaticDocs.setAction(ActionType.EDIT);
        timaticDocs.setInfant(infant);

        if (timaticInfo.getStayType() != null && !timaticInfo.getStayType().trim().isEmpty()) {
            TimaticStayType timaticStayType = new TimaticStayType();

            String validatedValue = getValidTimaticStayType(timaticInfo.getStayType());

            if (validatedValue != null) {
                timaticStayType.setStayType(validatedValue);
            } else {
                //TODO: throw an exception: invalid type type value
            }

            timaticDocs.setTimaticStay(timaticStayType);
        }

        // If document type is not set, default to 1
        if (timaticInfo.getDocumentType() == null) {
            // If null, default to 1= Normal Passport
            timaticInfo.setDocumentType("1");
        } else {
            timaticInfo.setDocumentType(timaticInfo.getDocumentType().trim());
            if (timaticInfo.getDocumentType().isEmpty()) {
                // If empty string, default to 1= Normal Passport
                timaticInfo.setDocumentType("1");
            }
        }

        // TODO: validate Document Type is a integer value
        TimaticDocType timaticDocType = new TimaticDocType();
        timaticDocType.setDocumentType(timaticInfo.getDocumentType());

        if (timaticInfo.getResidencyDocumentType() != null && !timaticInfo.getResidencyDocumentType().trim().isEmpty()) {
            TimaticDocType.ResidencyDocumentType residencyDocumentType = new TimaticDocType.ResidencyDocumentType();
            residencyDocumentType.setCountryCode(timaticInfo.getResidencyCountry());
            residencyDocumentType.setValue(timaticInfo.getResidencyDocumentType());
            timaticDocType.setResidencyDocumentType(residencyDocumentType);
        }

        timaticDocs.setTimaticDoc(timaticDocType);

        if (timaticInfo.getReturnDate() != null && !timaticInfo.getReturnDate().trim().isEmpty()) {
            timaticDocs.setReturnDate(timaticInfo.getReturnDate());
        }

        if (timaticInfo.isVisaVerified()) {
            timaticDocs.setVisaVerified("TRUE");
        }

        TimaticComplexType timatic = new TimaticComplexType();
        timatic.setTimaticDocs(timaticDocs);

        passengerDocuments.setTimatic(timatic);

        return passengerSecurityInfo;
    }

    private String getValidTimaticStayType(String inputStayType) {

        for (String stayType : TIMATIC_STAY_TYPES) {
            if (stayType.equalsIgnoreCase(inputStayType)) {
                return stayType;
            }
        }

        return null;
    }

    /**
     * @param bookedTravelerUpdate
     * @param infant
     * @return
     * @throws Exception
     */
    private PassengerSecurityInfoType getDestinationAddress(
            BookedTraveler bookedTravelerUpdate,
            boolean infant
    ) throws Exception {

        Address address = new Address();
        address.setType("DESTINATION");

        address.setStreetAddress(ShoppingCartUtil.formatStringUpperCaseSize(bookedTravelerUpdate.getDestinationAddress().getAddressOne(), 35));
        address.setCity(ShoppingCartUtil.formatStringUpperCaseSize(bookedTravelerUpdate.getDestinationAddress().getCity(), 35));
        address.setCountryCode(ShoppingCartUtil.formatStringUpperCaseSize(bookedTravelerUpdate.getDestinationAddress().getCountry(), 3));
        address.setStateProvince(ShoppingCartUtil.formatStringUpperCaseSize(bookedTravelerUpdate.getDestinationAddress().getState(), 35));

        if (null != bookedTravelerUpdate.getDestinationAddress().getZipCode()) {
            address.setZipCode(ShoppingCartUtil.formatStringUpperCaseSize(bookedTravelerUpdate.getDestinationAddress().getZipCode(), 20));
        }

        PassengerSecurityInfoType passengerSecurityInfo = new PassengerSecurityInfoType();

        AddressInfoType addressInfo = new AddressInfoType();
        addressInfo.setAction(ActionType.EDIT);
        addressInfo.setInfant(infant);

        PassengerDocumentsType passengerDocuments = new PassengerDocumentsType();
        passengerSecurityInfo.setManageSecurityInfo(passengerDocuments);
        passengerDocuments.setAddressInfoList(addressInfo);

        addressInfo.getAddress().add(address);

        return passengerSecurityInfo;
    }

    /**
     * @param countryCode
     * @param infant
     * @return
     * @throws Exception
     */
    private PassengerSecurityInfoType getDefaultDestinationAddress(
            String countryCode,
            boolean infant
    ) throws Exception {

        Address address = new Address();
        address.setType("DESTINATION");

        address.setStreetAddress("NA");
        address.setCity("NA");
        address.setCountryCode(countryCode);
        address.setStateProvince("NA");

        PassengerSecurityInfoType passengerSecurityInfo = new PassengerSecurityInfoType();

        AddressInfoType addressInfo = new AddressInfoType();
        addressInfo.setAction(ActionType.EDIT);
        addressInfo.setInfant(infant);

        PassengerDocumentsType passengerDocuments = new PassengerDocumentsType();
        passengerSecurityInfo.setManageSecurityInfo(passengerDocuments);
        passengerDocuments.setAddressInfoList(addressInfo);

        addressInfo.getAddress().add(address);

        return passengerSecurityInfo;
    }

    /**
     * @param bookedTravelerUpdate
     * @param bookedTraveler
     * @param bookingClass
     * @param infant
     * @return
     * @throws Exception
     */
    private PassengerSecurityInfoType getRedressNumber(BookedTraveler bookedTravelerUpdate, BookedTraveler bookedTraveler, BookingClass bookingClass, boolean infant) throws Exception {

        PassengerSecurityInfoType passengerSecurityInfo = new PassengerSecurityInfoType();
        PassengerDocumentsType passengerDocumentsType = new PassengerDocumentsType();
        OtherDocumentsInfoType otherDocumentsInfo = new OtherDocumentsInfoType();
        OtherDocumentType otherDocument = new OtherDocumentType();

        otherDocument.setNumber(ShoppingCartUtil.formatStringUpperCaseSize(bookedTravelerUpdate.getRedressNumber(), 30));
        otherDocumentsInfo.setRedress(otherDocument);
        otherDocumentsInfo.setAction(ActionType.EDIT);
        otherDocumentsInfo.setInfant(infant);
        passengerDocumentsType.setOtherDocumentsInfo(otherDocumentsInfo);
        passengerSecurityInfo.setManageSecurityInfo(passengerDocumentsType);

        return passengerSecurityInfo;
    }

    private PassengerSecurityInfoType getVisaSecurityInfoType(VisaInfo visaInfo, BookingClass bookingClass, boolean infant) throws Exception {

        PassengerSecurityInfoType passengerSecurityInfo = new PassengerSecurityInfoType();
        PassengerDocumentsType passengerDocumentsType = new PassengerDocumentsType();
        OtherDocumentsInfoType otherDocumentsInfo = new OtherDocumentsInfoType();

        VisaType visaType = new VisaType();
        visaType.setApplicableCountryCode(visaInfo.getApplicableCountryCode());

        if (visaInfo.getIssueCity() != null) {
            visaType.setIssueCity(visaInfo.getIssueCity().toUpperCase());
        } else {
            //TODO: verify if this field is required.
        }

        if (visaInfo.getIssueDate() != null) {
            visaType.setIssueDate(visaInfo.getIssueDate());
        } else {
            //TODO: verify if this field is required. If required, throw an invalid VISA request exception.
        }

        // VISA number is alpha-numeric: make sure all is upper case.
        if (visaInfo.getNumber() != null) {
            visaType.setNumber(visaInfo.getNumber().toUpperCase());
        } else {
            //TODO: throw exception. Invalid VISA request. VISA number is required. Can't be null or blank.
        }

        if (visaInfo.getBirthPlace() != null) {
            visaType.setBirthPlace(visaInfo.getBirthPlace().toUpperCase());
        } else {
            //TODO: verify if this field is required. If required, throw an invalid VISA exception.
        }

        otherDocumentsInfo.setAction(ActionType.EDIT);
        otherDocumentsInfo.setInfant(infant);

        otherDocumentsInfo.getVisa().add(visaType);

        passengerDocumentsType.setOtherDocumentsInfo(otherDocumentsInfo);
        passengerSecurityInfo.setManageSecurityInfo(passengerDocumentsType);

        return passengerSecurityInfo;
    }

    /**
     * @param bookedTravelerUpdate
     * @param bookedTraveler
     * @return
     * @throws Exception
     */
    private PassengerSecurityInfoType getTravelDocument(BookedTraveler bookedTravelerUpdate, BookedTraveler bookedTraveler, boolean swipeIndicator, boolean infant) throws Exception {

        PassengerSecurityInfoType passengerSecurityInfo = new PassengerSecurityInfoType();

        PassengerDocumentsType passengerDocuments = new PassengerDocumentsType();
        passengerSecurityInfo.setManageSecurityInfo(passengerDocuments);

        TravelDocumentListType travelDocumentList = new TravelDocumentListType();
        travelDocumentList.setInfant(infant);
        travelDocumentList.setAction(ActionType.EDIT);
        passengerDocuments.setTravelDocumentList(travelDocumentList);

        TravelDocumentType travelDocument = new TravelDocumentType();
        travelDocument.setType("PASSPORT");

        if (infant) {
            PersonNamePlain personNamePlain = new PersonNamePlain();
            personNamePlain.setFirstName(ShoppingCartUtil.formatStringUpperCaseSize(bookedTraveler.getInfant().getFirstName(), 64));
            personNamePlain.setLastName(ShoppingCartUtil.formatStringUpperCaseSize(bookedTraveler.getInfant().getLastName(), 30));
            travelDocument.setPersonName(personNamePlain);

            String numberDocumentValid = bookedTravelerUpdate.getInfant().getTravelDocument().getDocumentNumber().replaceAll("[^A-Za-z0-9]", "");
            travelDocument.setNumber(ShoppingCartUtil.formatStringUpperCaseSize(numberDocumentValid, 20));
            travelDocument.setIssueCountry(ShoppingCartUtil.formatStringUpperCaseSize(bookedTravelerUpdate.getInfant().getTravelDocument().getIssuingCountry(), 3));
            travelDocument.setExpiryDate(bookedTravelerUpdate.getInfant().getTravelDocument().getExpirationDate());
            travelDocument.setNationality(ShoppingCartUtil.formatStringUpperCaseSize(bookedTravelerUpdate.getInfant().getTravelDocument().getNationality(), 3));
            travelDocument.setDateOfBirth(bookedTraveler.getInfant().getDateOfBirth());
            travelDocument.setGender(ShoppingCartUtil.formatStringUpperCaseSize(bookedTraveler.getInfant().getGender().getCode(), 2));

            //TODO: Need to investigate
            //travelDocument.setMultinamePassportHolder(YNIndicator.Y);
        } else {
            PersonNamePlain personNamePlain = new PersonNamePlain();
            personNamePlain.setFirstName(ShoppingCartUtil.formatStringUpperCaseSize(bookedTraveler.getFirstName(), 30));
            personNamePlain.setLastName(ShoppingCartUtil.formatStringUpperCaseSize(bookedTraveler.getLastName(), 30));
            travelDocument.setPersonName(personNamePlain);

            String numberDocumentValid = bookedTravelerUpdate.getTravelDocument().getDocumentNumber().replaceAll("[^A-Za-z0-9]", "");
            travelDocument.setNumber(ShoppingCartUtil.formatStringUpperCaseSize(numberDocumentValid, 20));
            travelDocument.setIssueCountry(ShoppingCartUtil.formatStringUpperCaseSize(bookedTravelerUpdate.getTravelDocument().getIssuingCountry(), 3));
            travelDocument.setExpiryDate(bookedTravelerUpdate.getTravelDocument().getExpirationDate());
            travelDocument.setNationality(ShoppingCartUtil.formatStringUpperCaseSize(bookedTravelerUpdate.getTravelDocument().getNationality(), 3));
            travelDocument.setDateOfBirth(bookedTraveler.getDateOfBirth());
            travelDocument.setGender(ShoppingCartUtil.formatStringUpperCaseSize(bookedTraveler.getGender().getCode(), 2));
        }

        if (swipeIndicator) {
            travelDocument.setSwipeIndicator(YNIndicator.Y);
        }

        travelDocumentList.getTravelDocument().add(travelDocument);

        return passengerSecurityInfo;
    }

    /**
     * @param CartId
     * @param shoppingCartRQ
     * @return
     * @throws Exception
     */
    public CartPNR getShoppingCart(String CartId, ShoppingCartRQ shoppingCartRQ) throws Exception {

        // First read the pnrCollection from the database
        PNRCollection pnrCollection = pnrLookupDao.readPNRCollectionByCartId(CartId);

        // If the pnrCollection is null, throw an expired session exception
        if (pnrCollection == null) {
            LOG.error(CommonUtil.getMessageByCode(Constants.CODE_027) + shoppingCartRQ.getCartId());
            throw new GenericException(Response.Status.NOT_FOUND, ErrorType.CART_NOT_FOUND_IN_COLLECTION,
                    CommonUtil.getMessageByCode(Constants.CODE_027) + shoppingCartRQ.getCartId());

        }

        if (null != pnrCollection.getStore() && !pnrCollection.getStore().trim().isEmpty()) {
            shoppingCartRQ.setStore(pnrCollection.getStore());
        }

        if (null != pnrCollection.getPos() && !pnrCollection.getPos().trim().isEmpty()) {
            shoppingCartRQ.setPos(pnrCollection.getPos());
        }

        CartPNR cartPNR = pnrCollection.getCartPNRByCartId(shoppingCartRQ.getCartId());

        return cartPNR;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public String callServiceHertzRates(String rateReference, String language) {
        LOG.info("POST Hertz - Request", rateReference);
        MultivaluedMap parameters = new MultivaluedMapImpl();
        parameters.add("lang", language.substring(0, 2));
        GenericRestClient client = new GenericRestClient(setUpConfigFactory.getSystemPropertiesFactory().getInstance().getAmTitaniumApiUrl());
        String response = "{}";
        response = client.doGet("/rates/" + rateReference, parameters);

        LOG.info("POST Hertz - Response", CommonUtil.removeJsonFormat(response));
        return response;
    }

    private boolean savePhoneMissingInformation(
            String pnr,
            BookedTraveler bookedTravelerUpdate,
            BookedTraveler bookedTraveler,
            WarningCollection warningCollection
    ) {
        boolean savedPhone = false;
        try {// Missing information is removed from phone and mail when you already have a passenger
            // update phone reservation
            if (null != bookedTravelerUpdate.getPhones()
                    && !bookedTravelerUpdate.getPhones().getCollection().isEmpty()) {
                if (null == bookedTraveler.getPhones()
                        || bookedTraveler.getPhones().getCollection().isEmpty()) {
                    if (updatePhoneAndEmailInReservation(
                            pnr,
                            bookedTravelerUpdate,
                            bookedTraveler.getNameRefNumber(),
                            false,
                            true)) {
                        // Update phone in cartPNR
                        savedPhone = true;
                        bookedTraveler.setPhones(bookedTravelerUpdate.getPhones());
                        PnrCollectionUtil.removeMissingInformation(
                                bookedTraveler,
                                CheckinIneligibleReasons.PHONE.toString()
                        );
                    } else {
                        LOG.error("CHECKIN_ERROR: Error to save Phone Missing Information CODE: {} DESCRIPTION: {} PNR: {} ",
                        ErrorType.NOT_SAVE_PHONE_MISSING_INFORMATION.getErrorCode(),
                        ErrorType.NOT_SAVE_PHONE_MISSING_INFORMATION.getFullDescription(),
                        bookedTraveler.getPnrPassId());
                        Warning warning = new Warning(
                                ErrorType.NOT_SAVE_PHONE_MISSING_INFORMATION.getErrorCode(),
                                ErrorType.NOT_SAVE_PHONE_MISSING_INFORMATION.getFullDescription()
                                        + ", "
                                        + bookedTraveler.getPnrPassId()
                        );
                        warning.setActionable(ActionableType.RETRY.toString());
                        warningCollection.addToList(warning);
                    }
                }
            }
        } catch (Exception e) {
            LOG.error("CHECKIN_ERROR: Error to save Phone Missing Information CODE: {} DESCRIPTION: {} PNR: {} ",
            ErrorType.NOT_SAVE_PHONE_MISSING_INFORMATION.getErrorCode(),
            ErrorType.NOT_SAVE_PHONE_MISSING_INFORMATION.getFullDescription(),
            bookedTraveler.getPnrPassId());
            Warning warning = new Warning(
                    ErrorType.NOT_SAVE_PHONE_MISSING_INFORMATION.getErrorCode(),
                    ErrorType.NOT_SAVE_PHONE_MISSING_INFORMATION.getFullDescription()
                            + ", "
                            + bookedTraveler.getPnrPassId()
            );
            warning.setActionable(ActionableType.RETRY.toString());
            warningCollection.addToList(warning);
        }
        return savedPhone;
    }

    private boolean saveEmailMissingInformation(String pnr,
                                                BookedTraveler bookedTravelerUpdate,
                                                BookedTraveler bookedTraveler,
                                                WarningCollection warningCollection) {
        boolean savedEmail = false;
        try {// Missing information is removed from phone and mail when you already have a passenger
            // update email reservation
            if (null != bookedTravelerUpdate.getEmail()
                    && !bookedTravelerUpdate.getEmail().isEmpty()) {
                if (null == bookedTraveler.getEmail()
                        || !bookedTravelerUpdate.getEmail().equalsIgnoreCase(bookedTraveler.getEmail())) {
                    if (updatePhoneAndEmailInReservation(
                            pnr,
                            bookedTravelerUpdate,
                            bookedTraveler.getNameRefNumber(),
                            true,
                            false)) {
                        // Update email in cartPNR
                        savedEmail = true;
                        bookedTraveler.setEmail(bookedTravelerUpdate.getEmail());
                        PnrCollectionUtil.removeMissingInformation(
                                bookedTraveler,
                                CheckinIneligibleReasons.EMAIL.toString()
                        );

                        List<String> savedEmailsList = new ArrayList<>();
                        savedEmailsList.add(bookedTravelerUpdate.getEmail());
                        bookedTraveler.setSavedEmails(savedEmailsList);
                    } else {
                        LOG.error("CHECKIN_ERROR: Error to save Email Missing Information CODE: {} DESCRIPTION: {} PNR: {} ",
                        ErrorType.NOT_SAVE_EMAIL_MISSING_INFORMATION.getErrorCode(),
                        ErrorType.NOT_SAVE_EMAIL_MISSING_INFORMATION.getFullDescription(),
                        bookedTraveler.getPnrPassId());
                        Warning warning = new Warning(
                                ErrorType.NOT_SAVE_EMAIL_MISSING_INFORMATION.getErrorCode(),
                                ErrorType.NOT_SAVE_EMAIL_MISSING_INFORMATION.getFullDescription()
                                        + ", "
                                        + bookedTraveler.getPnrPassId()
                        );
                        warning.setActionable(ActionableType.RETRY.toString());
                        warningCollection.addToList(warning);
                    }
                }
            }

        } catch (Exception e) {
            LOG.error("CHECKIN_ERROR: Error to save Email Missing Information CODE: {} DESCRIPTION: {} PNR: {}  ",
            ErrorType.NOT_SAVE_EMAIL_MISSING_INFORMATION.getErrorCode(),
            ErrorType.NOT_SAVE_EMAIL_MISSING_INFORMATION.getFullDescription(),
            bookedTraveler.getPnrPassId());
            Warning warning = new Warning(
                    ErrorType.NOT_SAVE_EMAIL_MISSING_INFORMATION.getErrorCode(),
                    ErrorType.NOT_SAVE_EMAIL_MISSING_INFORMATION.getFullDescription()
                            + ", "
                            + bookedTraveler.getPnrPassId()
            );
            warning.setActionable(ActionableType.RETRY.toString());
            warningCollection.addToList(warning);
        }
        return savedEmail;
    }

    /**
     * Add Mail and Phone Number Information to the passenger
     *
     * @param pnr
     * @param bookedTravelerUpdate
     * @param updateMail
     * @param updatePhone
     * @return boolean, true was success false in other case
     */
    private boolean updatePhoneAndEmailInReservation(String pnr,
                                                     BookedTraveler bookedTravelerUpdate,
                                                     String nameRefNumber,
                                                     boolean updateMail,
                                                     boolean updatePhone) {

        ReservationUpdateListType reservationUpdateListType
                = UpdatePassengerUtil.castPassengersToSabreRequest(
                bookedTravelerUpdate,
                nameRefNumber,
                pnr,
                updateMail,
                updatePhone
        );
        return updateReservationService.createPhoneNumberAndEmailAddress(reservationUpdateListType);

    }

    private static boolean isClassic( String bookingClass) {
        boolean isclassic = false ;
        for (String cl : FarebasisFamilyType.DOMESTIC_CLASSIC_FAMILY.getFamilyClasses()) {
            if(bookingClass.equals(cl)) {
                isclassic = true;
                break;
            }else {
                isclassic =  false;
            
            }
        }
        return isclassic;
    }

    /**
     * @param timaticInfo
     * @param infant
     * @return
     * @throws Exception
     */
    private PassengerSecurityInfoType getTimaticInfoInfan(
            TimaticInfo timaticInfo,
            boolean infant,
            BookedTraveler bookedTraveler
    ) throws Exception {

        PassengerSecurityInfoType passengerSecurityInfo = new PassengerSecurityInfoType();

        PassengerDocumentsType passengerDocuments = new PassengerDocumentsType();
        passengerSecurityInfo.setManageSecurityInfo(passengerDocuments);

        TimaticDocsType timaticDocs = new TimaticDocsType();
        timaticDocs.setAction(ActionType.EDIT);
        timaticDocs.setInfant(infant);

        if (timaticInfo.getStayType() != null && !timaticInfo.getStayType().trim().isEmpty()) {
            TimaticStayType timaticStayType = new TimaticStayType();

            String validatedValue = getValidTimaticStayType(timaticInfo.getStayType());

            if (validatedValue != null) {
                timaticStayType.setStayType(validatedValue);
            } else {
                //TODO: throw an exception: invalid type type value
            }

            timaticDocs.setTimaticStay(timaticStayType);
        }

        // If document type is not set, default to 1
        if (timaticInfo.getDocumentType() == null) {
            // If null, default to 1= Normal Passport
            timaticInfo.setDocumentType("1");
        } else {
            timaticInfo.setDocumentType(timaticInfo.getDocumentType().trim());
            if (timaticInfo.getDocumentType().isEmpty()) {
                // If empty string, default to 1= Normal Passport
                timaticInfo.setDocumentType("1");
            }
        }

        // TODO: validate Document Type is a integer value
        TimaticDocType timaticDocType = new TimaticDocType();
        timaticDocType.setDocumentType(timaticInfo.getDocumentType());

        if (timaticInfo.getResidencyDocumentType() != null && !timaticInfo.getResidencyDocumentType().trim().isEmpty()) {
            TimaticDocType.ResidencyDocumentType residencyDocumentType = new TimaticDocType.ResidencyDocumentType();
            if(!infant){
                residencyDocumentType.setCountryCode(timaticInfo.getResidencyCountry());
            } else {
                residencyDocumentType.setCountryCode(bookedTraveler.getCountryOfResidence());
            }
            residencyDocumentType.setValue(timaticInfo.getResidencyDocumentType());
            timaticDocType.setResidencyDocumentType(residencyDocumentType);
        }

        timaticDocs.setTimaticDoc(timaticDocType);

        if (timaticInfo.getReturnDate() != null && !timaticInfo.getReturnDate().trim().isEmpty()) {
            timaticDocs.setReturnDate(timaticInfo.getReturnDate());
        }

        if (timaticInfo.isVisaVerified()) {
            timaticDocs.setVisaVerified("TRUE");
        }

        TimaticComplexType timatic = new TimaticComplexType();
        timatic.setTimaticDocs(timaticDocs);

        passengerDocuments.setTimatic(timatic);

        return passengerSecurityInfo;
    }


    /**
     * @return
     */
    public AMEditPassCharService getEditPassCharService() {
        return editPassCharService;
    }

    /**
     * @param editPassCharService
     */
    public void setEditPassCharService(AMEditPassCharService editPassCharService) {
        this.editPassCharService = editPassCharService;
    }

    /**
     * @return
     */
    public IPNRLookupDao getPnrLookupDao() {
        return pnrLookupDao;
    }

    /**
     * @param pnrLookupDao
     */
    public void setPnrLookupDao(IPNRLookupDao pnrLookupDao) {
        this.pnrLookupDao = pnrLookupDao;
    }

    /**
     * @return
     */
    public AMPassengerSecurityService getAMPassengerSecurityService() {
        return passengerSecurityService;
    }

    /**
     * @param passengerSecurityService
     */
    public void setAMPassengerSecurityService(AMPassengerSecurityService passengerSecurityService) {
        this.passengerSecurityService = passengerSecurityService;
    }

    /**
     * @return the updatePnrCollectionService
     */
    public PnrCollectionService getUpdatePnrCollectionService() {
        return updatePnrCollectionService;
    }

    /**
     * @param updatePnrCollectionService the updatePnrCollectionService to set
     */
    public void setUpdatePnrCollectionService(PnrCollectionService updatePnrCollectionService) {
        this.updatePnrCollectionService = updatePnrCollectionService;
    }

    public AMPassengerDetailsService getPassengerDetailsService() {
        return passengerDetailsService;
    }

    public void setPassengerDetailsService(AMPassengerDetailsService passengerDetailsService) {
        this.passengerDetailsService = passengerDetailsService;
    }

    /**
     * @return the seatsService
     */
    public SeatsService getSeatsService() {
        return seatsService;
    }

    /**
     * @param seatsService the seatsService to set
     */
    public void setSeatsService(SeatsService seatsService) {
        this.seatsService = seatsService;
    }

    /**
     * @return the updateReservationService
     */
    public AMUpdateReservationService getUpdateReservationService() {
        return updateReservationService;
    }

    /**
     * @param updateReservationService the updateReservationService to set
     */
    public void setUpdateReservationService(AMUpdateReservationService updateReservationService) {
        this.updateReservationService = updateReservationService;
    }

    public ShoppingCartAncillaries getShoppingCartAncillaries() {
        return shoppingCartAncillaries;
    }

    public void setShoppingCartAncillaries(ShoppingCartAncillaries shoppingCartAncillaries) {
        this.shoppingCartAncillaries = shoppingCartAncillaries;
    }

}
