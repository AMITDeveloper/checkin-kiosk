package com.am.checkin.web.service;

import com.aeromexico.commons.model.AuthorizationResult;
import com.aeromexico.commons.model.PNR;
import com.aeromexico.commons.model.PNRCollection;
import com.aeromexico.commons.model.rq.PinPadInitializationRQ;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.dao.services.IPNRLookupDao;
import com.aeromexico.sabre.api.session.common.ServiceCall;
import com.aeromexico.commons.exception.model.GenericException;
import com.am.checkin.web.util.AuthorizationResultUtil;
import com.google.gson.Gson;

import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;

import org.opentravel.ota.payments.AuthorizationResultType;
import org.opentravel.ota.payments.PaymentRQ;
import org.opentravel.ota.payments.PaymentRS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author adrian
 */
@Named
@ApplicationScoped
public class PinpadInitializationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PurchaseOrderService.class);

    private static final Gson GSON = new Gson();

    @Inject
    private PaymentService paymentService;

    @Inject
    private IPNRLookupDao pnrLookupDao;

    /**
     * @param cartId
     * @return
     * @throws Exception
     */
    private PNRCollection getPNRCollection(String cartId) throws Exception {
        PNRCollection pnrCollection = pnrLookupDao.readPNRCollectionByCartId(cartId);
        if (null == pnrCollection) {
            logError(null, cartId, "Pnr collection not found to do purchase");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION, "Pnr collection not found");
        }

        return pnrCollection;
    }

    /**
     * @param pinPadInitializationRQ
     * @param serviceCallList
     * @return
     * @throws Exception
     */
    public AuthorizationResult initializePinpad(
            PinPadInitializationRQ pinPadInitializationRQ,
            List<ServiceCall> serviceCallList
    ) throws Exception {
        String cartId;
        String recordLocator;
        if (null != pinPadInitializationRQ.getPurchaseOrder().getCartId()) {
            try {
                cartId = pinPadInitializationRQ.getPurchaseOrder().getCartId();
                logDebug(null, cartId, "Encrypt Key: getting initialization tokens");

                PNRCollection pnrCollection = getPNRCollection(cartId);

                logDebug(null, cartId, "Encrypt Key: Finding PNR by cartid " + cartId);
                PNR pnr = pnrCollection.getPNRByCartId(cartId);
                if (null == pnr) {
                    logError(null, cartId, "Encrypt Key: Pnr not found");
                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION, "Encrypt Key: Pnr not found");
                }
                recordLocator = pnr.getPnr();

                if (null == recordLocator || recordLocator.trim().isEmpty()) {
                    logError(null, cartId, "Encrypt Key: Pnr not found");
                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION, "Encrypt Key: Pnr not found");
                }
            } catch (Exception ex) {
                cartId = null;
                recordLocator = null;
            }
        } else {
            cartId = null;
            recordLocator = null;
        }

        List<AuthorizationResult> authorizationResultList = doInitialization(
                pinPadInitializationRQ,
                recordLocator,
                cartId,
                serviceCallList
        );

        return authorizationResultList.get(0);

    }

    /**
     * @param pinPadInitializationRQ
     * @param recordLocator
     * @param cartId
     * @param serviceCallList
     * @return
     * @throws Exception
     */
    private List<AuthorizationResult> doInitialization(
            PinPadInitializationRQ pinPadInitializationRQ,
            String recordLocator, String cartId,
            List<ServiceCall> serviceCallList
    ) throws Exception {
        try {

            PaymentRQ paymentRQ = paymentService.getEncryptKeyRQ(pinPadInitializationRQ);
            logDebug(recordLocator, cartId, "Encrypt Key: Payment RQ");
            logDebug(recordLocator, cartId, GSON.toJson(paymentRQ));

            PaymentRS paymentRS = paymentService.getEncryptKeyRS(paymentRQ, recordLocator, cartId, "Getting Encrypt Key", serviceCallList);

            logDebug(recordLocator, cartId, "Encrypt Key: Payment RS");
            logDebug(recordLocator, cartId, GSON.toJson(paymentRS));

            List<AuthorizationResultType> authorizationResultTypeList = paymentRS.getAuthorizationResult();

            for (AuthorizationResultType authorizationResultType : authorizationResultTypeList) {
                if (!"APPROVED".equalsIgnoreCase(authorizationResultType.getResponseCode())) {
                    //process failure
                }
            }

            List<AuthorizationResult> authorizationResultList = AuthorizationResultUtil.getAuthorizationResult(authorizationResultTypeList);

            return authorizationResultList;

        } catch (GenericException ex) {
            logError(recordLocator, cartId, ex.getMessage());
            throw ex;
        } catch (Exception ex) {
            logError(recordLocator, cartId, ex.getMessage());
            throw ex;
        }
    }

    /**
     * @param recordLocator
     * @param cartId
     * @param msg
     */
    public void log(String recordLocator, String cartId, String msg) {
        try {

            StringBuilder sb = new StringBuilder();
            sb.append("\n").append("Purchase Order Service: ").append("\n");
            sb.append("recordLocator: ").append(recordLocator).append("\n");
            sb.append("cartId: ").append(cartId).append("\n");
            sb.append("msg: ").append(msg).append("\n");
            LOGGER.info(sb.toString());
        } catch (Exception ex) {
            //ignore
        }
    }

    /**
     * @param recordLocator
     * @param cartId
     * @param msg
     */
    public void logDebug(String recordLocator, String cartId, String msg) {
        try {

            StringBuilder sb = new StringBuilder();
            sb.append("\n").append("Purchase Order Service: ").append("\n");
            sb.append("recordLocator: ").append(recordLocator).append("\n");
            sb.append("cartId: ").append(cartId).append("\n");
            sb.append("msg: ").append(msg).append("\n");
            LOGGER.debug(sb.toString());
        } catch (Exception ex) {
            //ignore
        }
    }

    /**
     * @param recordLocator
     * @param cartId
     * @param msg
     */
    public void logError(String recordLocator, String cartId, String msg) {
        try {

            StringBuilder sb = new StringBuilder();
            sb.append("\n").append("Purchase Order Service: ").append("\n");
            sb.append("recordLocator: ").append(recordLocator).append("\n");
            sb.append("cartId: ").append(cartId).append("\n");
            sb.append("msg: ").append(msg).append("\n");
            LOGGER.error(sb.toString());
        } catch (Exception ex) {
            //ignore
        }
    }
}
