package com.am.checkin.web.service;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.CreditCardFormOfPayment;
import com.aeromexico.commons.model.FormOfPayment;
import com.aeromexico.commons.model.FormOfPaymentCollection;
import com.aeromexico.commons.model.GiftCardFormOfPayment;
import com.aeromexico.commons.model.Links;
import com.aeromexico.commons.model.VisaCheckOutFormOfPayment;
import com.aeromexico.commons.model.Self;
import com.aeromexico.commons.model.rq.PaymentMethodRQ;
import com.aeromexico.commons.web.types.CreditCardType;
import com.aeromexico.commons.web.types.GiftCardType;
import com.aeromexico.commons.web.types.PosType;
import com.aeromexico.commons.web.util.Constants;
import com.aeromexico.dao.client.exception.MongoDisconnectException;
import com.aeromexico.dao.qualifiers.PaymentMethodsCache;
import com.aeromexico.dao.services.IPaymentMethodDao;
import com.aeromexico.webe4.web.payment.model.PaymentMethod;
import org.aeromexico.commons.exception.config.ReadProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;

@Named
@ApplicationScoped
public class PaymentMethodsService {
    private static final Logger LOG = LoggerFactory.getLogger(PaymentMethodsService.class);

    @Inject
    @PaymentMethodsCache
    private IPaymentMethodDao paymentMethodDao;

    @Inject
    @Named("CheckInSetUpConfigFactoryWrapper")
    private CheckInSetUpConfigFactoryWrapper setUpConfigFactory;

    @Inject
    private ReadProperties prop;

    @PostConstruct
    public void init() {
        if (null == setUpConfigFactory) {
            setUpConfigFactory = new CheckInSetUpConfigFactoryWrapper();
        }
    }

    /**
     * @param paymentMethodRQ
     * @return
     * @throws Exception
     */
    public FormOfPaymentCollection getPaymentMethods(PaymentMethodRQ paymentMethodRQ) throws Exception {
        FormOfPaymentCollection formOfPaymentCollection = new FormOfPaymentCollection();
        try{
        CreditCardFormOfPayment creditCardFormOfPaymentAMEX = new CreditCardFormOfPayment();
        CreditCardFormOfPayment creditCardFormOfPaymentVISA = new CreditCardFormOfPayment();
        CreditCardFormOfPayment creditCardFormOfPaymentMASTER = new CreditCardFormOfPayment();

        //creditCardFormOfPaymentAMEX.setBankName("Otro");
        creditCardFormOfPaymentAMEX.setCardCode(CreditCardType.AMEX.getCardCode());
        creditCardFormOfPaymentAMEX.setCardType(CreditCardType.AMEX);
        creditCardFormOfPaymentAMEX.setInstallments(null);

        //creditCardFormOfPaymentVISA.setBankName("Otro");
        creditCardFormOfPaymentVISA.setCardCode(CreditCardType.VISA.getCardCode());
        creditCardFormOfPaymentVISA.setCardType(CreditCardType.VISA);
        creditCardFormOfPaymentVISA.setInstallments(null);

        //creditCardFormOfPaymentMASTER.setBankName("Otro");
        creditCardFormOfPaymentMASTER.setCardCode(CreditCardType.MASTER.getCardCode());
        creditCardFormOfPaymentMASTER.setCardType(CreditCardType.MASTER);
        creditCardFormOfPaymentMASTER.setInstallments(null);

        Self self = new Self("/payment-methods?store=" + paymentMethodRQ.getStore() + "&pos=" + paymentMethodRQ.getPos());
        Links links = new Links(self);

        ArrayList<FormOfPayment> collection = new ArrayList<>();
        collection.add(creditCardFormOfPaymentAMEX);
        collection.add(creditCardFormOfPaymentVISA);
        collection.add(creditCardFormOfPaymentMASTER);
        
        addFormOfPaymentVisaCheckOut(collection);
        addFormOfPaymentUATP(collection, paymentMethodRQ.getPos());
       
        formOfPaymentCollection.setLinks(links);
        formOfPaymentCollection.setCollection(collection);

        String cybersourceOrgId = setUpConfigFactory.getInstance().getCybersourceConfig().getOrgId();

        formOfPaymentCollection.setCybersourceOrgId(cybersourceOrgId);
        }catch (MongoDisconnectException ex){
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(paymentMethodRQ.getPos())) {
                LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), ex.fillInStackTrace());
                throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getCode(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getActionable(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getMessage(),
                        Response.Status.INTERNAL_SERVER_ERROR);
            } else {
                LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), ex.fillInStackTrace());
                throw ex;
            }
        }catch (Exception ex){
            LOG.error(ex.getMessage(), ex);
        }
        return formOfPaymentCollection;
    }

    private void addFormOfPaymentUATP(ArrayList<FormOfPayment> formOfPayments, String pos) throws Exception {
        try {
            List<PaymentMethod> paymentMethods = paymentMethodDao.getPaymentMethods();
            if (null != paymentMethods) {
                final String PAYMENT_TYPE_UATP = "UATP";
                for (PaymentMethod paymentMethod : paymentMethods) {
                    if ((PAYMENT_TYPE_UATP.equalsIgnoreCase(paymentMethod.getCardType()))
                            && (PosType.WEB.toString().equalsIgnoreCase(pos))) {
                        GiftCardFormOfPayment giftCardFormOfPayment = new GiftCardFormOfPayment();
                        giftCardFormOfPayment.setCardType(GiftCardType.fromValue(paymentMethod.getCardType()));
                        formOfPayments.add(giftCardFormOfPayment);
                        break;
                    }
                }
            }
        } catch (Exception e) {
            LOG.error("Transformation error. paymentMethods => addFormOfPaymentUATP: " + e.getMessage());
            throw e;
        }
    }

    private void addFormOfPaymentVisaCheckOut(ArrayList<FormOfPayment> formOfPayments) throws Exception {
        try {
            List<PaymentMethod> paymentMethods = paymentMethodDao.getPaymentMethods();
            if (null != paymentMethods) {
                final String PAYMENT_TYPE_VISA_CHECKOUT = "VISA_CHECKOUT";
                for (PaymentMethod paymentMethod : paymentMethods) {
                    if (PAYMENT_TYPE_VISA_CHECKOUT.equalsIgnoreCase(paymentMethod.getPaymentType())) {
                        VisaCheckOutFormOfPayment visaCheckOutFormOfPayment = new VisaCheckOutFormOfPayment();
                        visaCheckOutFormOfPayment.setApiKey(paymentMethod.getApiKey());
                        visaCheckOutFormOfPayment.setUrl(paymentMethod.getUrl());
                        formOfPayments.add(visaCheckOutFormOfPayment);
                        break;
                    }
                }
            }
        } catch (Exception e) {
            LOG.error("Transformation error. paymentMethods => addFormOfPaymentVisaCheckOut: " + e.getMessage());
            throw e;
        }
    }
    

    /**
     * @return the setUpConfigFactory
     */
    public CheckInSetUpConfigFactoryWrapper getSetUpConfigFactory() {
        return setUpConfigFactory;
    }

    /**
     * @param setUpConfigFactory the setUpConfigFactory to set
     */
    public void setSetUpConfigFactory(CheckInSetUpConfigFactoryWrapper setUpConfigFactory) {
        this.setUpConfigFactory = setUpConfigFactory;
    }

}
