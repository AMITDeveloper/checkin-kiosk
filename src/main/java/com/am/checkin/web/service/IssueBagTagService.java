package com.am.checkin.web.service;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.AbstractAncillary;
import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedLegCollection;
import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.BookedTravelerCollection;
import com.aeromexico.commons.model.BookingClass;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.IssueBagRP;
import com.aeromexico.commons.model.PNR;
import com.aeromexico.commons.model.PNRCollection;
import com.aeromexico.commons.model.PaidTravelerAncillary;
import com.aeromexico.commons.model.Segment;
import com.aeromexico.commons.model.rq.IssueBagRQ;
import com.aeromexico.commons.model.weather.AirportWeather;
import com.aeromexico.commons.web.types.*;
import com.aeromexico.commons.web.util.Constants;
import com.aeromexico.commons.web.util.SegmentCodeUtil;
import com.aeromexico.dao.qualifiers.AirportsCache;
import com.aeromexico.dao.services.IAirportCodesDao;
import com.aeromexico.dao.util.CommonUtil;
import com.aeromexico.sabre.sabre.api.issuebagtag.AMIssueBagTagService;
import com.aeromexico.sabre.sabre.api.issuebagtag.model.BagRouteInfo;
import com.aeromexico.sabre.sabre.api.issuebagtag.model.IssueBagReq;
import com.am.checkin.web.event.dispatcher.UpdatePNRCollectionDispatcher;
import com.am.checkin.web.event.model.UpdatePNRCollectionEvent;
import com.am.checkin.web.util.IssueBagTagErrorUtil;
import com.sabre.services.checkin.issuebagtag.v4.*;
import com.sabre.services.stl.v3.ErrorOrSuccessCode;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;

import org.drools.javaparser.utils.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
PAX WITH 1 SEG: (TIJ 173 0708 to MEX)

BTP 
01 0301 *
01 AM *
       02 6139801324*
       03 ONE/LEGPAX    *
       05 MEXICO CITY   *
       06 6139801324*
       07 AEROMEXICO*

10 AM *
       11  173*
       12 MEX*
       13 004*
       14 0700A*
       70 VVCNKT*
       71 19OCT16/0324A*                 current date and time
       72 TIJ/SPK*
       75 MX*
       88 TALON 1 DE 1    -*
       89 10  *
       9D                 *



PAX WITH 2 SEG:

BTP
02 0301*
01 AM *
       02 9139114765*
       03 TWO/LEGPAX    *
       05 GUADALAJARA   *
       06 9139114765*
       07 AEROMEXICO*

10 AM *
       11 173*
       12 MEX*
       13 003*
       14 0700A*

20 AM *
       21  214*
       22 GDL*
       23 003*
       24 0200P*
       70 VVFXPP*
       71 19OCT16/0322A*
       72 TIJ/SPK*
       75 MX*
       88 TALON 1 DE 1    -*
       89 10  *
       9D                 *



PAX WITH 3 SEG:  MEX(214 1405)-GDL(414 0610)-MTY
BTP
03 0801*

01 AM 80 13 22*
       02 6139801322*
       03 ONE/TEST          *
       04 MONTERREY MX MTYMX*
       05  *
       07 001*
       08 001*
       09 001*

10 AM *
       11 MEX*
       12  173*
       15 /19*

20 AM *
       21 GDL*
       22  214*
       25 /19*

30 AM *
       31 MTY*
       32  414*
       35 /20  *

       47 6139801322*
       60 MEXICO CITY     MX*
       61 SKD 0700*
       70 GUADALAJARA     MX*
       71 SKD 1400*
       80 MONTERREY MX MTYMX*
       81 SKD 0610*
       9D                *
       B3 TIJ4SPK 0306 19OCT*



PAX with 4 SEG: (TIJ-MEX-GDL-MTY)

BTP
04 0301*

01 AM *
       02 6139801323*
       03 FOUR/LEGPAX   *
       05 MEXICO CITY   *
       06 6139801323*
       07 AEROMEXICO*

10 AM *
       11 173*
       12 MEX*
       13 002*
       14 0700A*

20 AM *
       21 214*
       22 GDL*
       23 002*
       24 0200P*

30 AM *
       31 414*
       32 MTY*
       33 002*
       34 0610A*

40 AM *
       41 919*
       42 MEX*
       43   *
       44 0847A*

70 VYVCOS*
71 19OCT16/0318A*
72 TIJ/SPK*
75 MX*
88 TALON 1 DE 1    -*
89 10  *
9D                *
 */
@Named
@ApplicationScoped
public class IssueBagTagService {

	private static final Logger logger = LoggerFactory.getLogger(IssueBagTagService.class);

	@Inject
	private PnrCollectionService pnrCollectionService;

	@Inject
	private AMIssueBagTagService issueBagTagService;

	@Inject
	@AirportsCache
	private IAirportCodesDao airportsCodes;

	@Inject
	@Named("CheckInSetUpConfigFactoryWrapper")
	private CheckInSetUpConfigFactoryWrapper setUpConfigFactory;

	@Inject
	private UpdatePNRCollectionDispatcher updatePNRCollectionDispatcher;

	@PostConstruct
	public void init() {
		if (null == setUpConfigFactory) {
			setUpConfigFactory = new CheckInSetUpConfigFactoryWrapper();
		}
	}

	/**
	 *
	 * @param issueBagRQ
	 * @return
	 * @throws Exception
	 */
	public IssueBagRP getBagTag(IssueBagRQ issueBagRQ) throws Exception {
		// Getting Valid passenger from Request. Only one passenger per request.
		BookedTraveler bookedTravelerRQ = getValidBookedTraveler(issueBagRQ);
		String cartId = issueBagRQ.getCartId();
		PNRCollection pnrCollection = pnrCollectionService.getPNRCollection(cartId, issueBagRQ.getPos());

		IssueBagRP issueBagRP = getBagTag(bookedTravelerRQ, issueBagRQ, pnrCollection);

		// Save pnr collection with updated checked bags
		updatePnrCollection(pnrCollection);

		return issueBagRP;

	}

	/**
	 * This method return the bagtag for the requested baggage, it also update the
	 * checked baggages for this passenger
	 *
	 *
	 * @param bookedTravelerRQ
	 * @param issueBagRQ
	 * @param pnrCollection
	 * @return
	 * @throws Exception
	 */
	public IssueBagRP getBagTag(BookedTraveler bookedTravelerRQ, IssueBagRQ issueBagRQ, PNRCollection pnrCollection) throws Exception {

		String cartId = issueBagRQ.getCartId();

		// Setting recorLocator to be logged.
		PNR pnr = pnrCollection.getPNRByCartId(cartId);
		issueBagRQ.setRecordLocator(pnr.getPnr());

		// Getting List of passenger from Mongo by CartId
		CartPNR cartPNR = pnr.getCartPNRByCartId(cartId);
		BookedTraveler bookedTravelerMongo = getBookedTraveler(bookedTravelerRQ.getId(), cartPNR, cartId);

		// Getting BokkingClass by Passenger Id (Applies when there is more than one segment)
		BookingClass bookingClassMongo = getBookingClassByPaxId(bookedTravelerRQ.getId(), bookedTravelerMongo);

		BookedLegCollection bookedLegCollection = pnr.getLegsByCartId(cartId);

		// Getting segments from Mongo
		Segment segmentMongo = getSegmentBySegmentCode(bookedLegCollection, bookingClassMongo.getSegmentCode());

		BagRouteInfo bagRouteInfo = new BagRouteInfo();

		bagRouteInfo.setBookingClass(bookingClassMongo.getBookingClass());
		bagRouteInfo.setDepartureDate(segmentMongo.getDepartureDateTime().substring(0, 10));
		bagRouteInfo.setOpCarrier(segmentMongo.getOperatingCarrier());
		bagRouteInfo.setOpflightNumber(segmentMongo.getOperatingFlightCode());
		bagRouteInfo.setOrigin(segmentMongo.getDepartureAirport());

		boolean flagGap = false;
		if (isChangeAirport(bookedLegCollection, bookingClassMongo.getSegmentCode(), segmentMongo.getArrivalAirport())) {
			bagRouteInfo.setDestination(segmentMongo.getArrivalAirport());
			flagGap = true;
		}

		// Agregar validacion para determinar si el aeropuerto cambia mandar destination
		// Agregar la validacion para encender nueva bandera

		int totalFreeBagAllow = bookedTravelerMongo.getFreeBaggageAllowance().getChecked();
		int totalPrePaidBags = getTotalPrePaidBags(bookedTravelerMongo);
		int tierBenefitFreeBags = getTierBeneftFreeBags(bookedTravelerMongo);

		if ( (totalFreeBagAllow + totalPrePaidBags + tierBenefitFreeBags) <= bookedTravelerMongo.getCheckedBags() ) {
			logger.info( "IssueBagTagService:getBagTag({} - {}) totalFreeBagAllow = {} totalPrePaidBags = {} tierBenefitFreeBags = {} checkedBags = {}", cartId, pnr.getPnr(), totalFreeBagAllow, totalPrePaidBags, tierBenefitFreeBags, bookedTravelerMongo.getCheckedBags());
			logger.error("{}: {}", issueBagRQ.getCartId(), ErrorType.NO_FREEBAGS_AVAILABLES.getFullDescription());
			throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.NO_FREEBAGS_AVAILABLES, ErrorType.NO_FREEBAGS_AVAILABLES.getFullDescription());
		}

		validateDepartAirport(issueBagRQ, pnr.getBookedLegByCartId(cartId), pnr.getPnr());

		// Validation to allow only ONTIME flights to print bag tag.
		if ( ! (BookedLegFlightStatusType.ON_TIME.toString().equalsIgnoreCase(segmentMongo.getFlightStatus().toString()) || BookedLegFlightStatusType.DELAYED.toString().equalsIgnoreCase(segmentMongo.getFlightStatus().toString()))) {
			logger.error("{}: {}: {}", issueBagRQ.getCartId(), ErrorType.NO_ELEGIBLE_TO_PRINT_BAG_TAG, 	segmentMongo.getFlightStatus().toString());
			throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.NO_ELEGIBLE_TO_PRINT_BAG_TAG);
		}

		int weight = 0;
		try {
			weight = bookedTravelerRQ.getBaggageInfo().getWeight().intValue();
		} catch (Exception e) {
			weight = 0;
		}

		if (weight <= 0) {
			logger.error("{}: {}", issueBagRQ.getCartId(), CommonUtil.getMessageByCode(Constants.CODE_047));
			throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.BAD_REQUEST, CommonUtil.getMessageByCode(Constants.CODE_047));
		}

		IssueBagReq issueBagReq = new IssueBagReq();
		issueBagReq.setPassengerId(bookedTravelerMongo.getId());
		issueBagReq.setLastName(bookedTravelerMongo.getLastName());
		issueBagReq.setBagRouteInfo(bagRouteInfo);
		issueBagReq.setUnit(bookedTravelerRQ.getBaggageInfo().getUnit());
		issueBagReq.setWeight(weight);

		String bagPrinterCommand = setUpConfigFactory.getInstance().formBagPrinterCommand();
		String cityCodeCommand = setUpConfigFactory.getInstance().getCityCodeCommand();
		IssueBagTagRSACS issueBagTagRS;
		IssueBagRP issueBagRP = null;
		int retry = 2;
		do {
			issueBagTagRS = issueBagTagService.getIssueBagTag(issueBagReq, bagPrinterCommand, cityCodeCommand);
			if (null != issueBagTagRS && ErrorOrSuccessCode.SUCCESS.equals(issueBagTagRS.getResult().getStatus())) {

				issueBagRP = getBagTag(issueBagTagRS, pnr.getPnr(), bookedLegCollection, issueBagReq, bookedTravelerMongo, cartPNR.getLegCode(), flagGap, segmentMongo.getDepartureDateTime());
				// Update number of checked bags and set as used prepaid Bags
				updateCheckedBaggages(bookedTravelerMongo);

				retry = 0;

			} else {
				if (null != issueBagTagRS && null != issueBagTagRS.getResult().getSystemSpecificResults() && !issueBagTagRS.getResult().getSystemSpecificResults().isEmpty()) {
					IssueBagTagErrorUtil.IssueBagTagSabreError(issueBagTagRS);

				} else {
					if (retry <= 0) {
						StringBuilder sb = new StringBuilder();

						issueBagTagRS.getResult().getSystemSpecificResults().forEach(i -> sb.append(i.getErrorMessage().getValue()));

						throw new GenericException(Response.Status.INTERNAL_SERVER_ERROR, ErrorType.INTERNAL_ERROR, sb.toString());
					}
				}

			}
			retry--;
		} while (retry >= 0);

		return issueBagRP;
	}

	private boolean isChangeAirport(BookedLegCollection bookedLegCollection, String segmentCode,
			String arrivalAirport) {
		for (BookedLeg bookedLeg : bookedLegCollection.getCollection()) {
			if (bookedLeg.getSegments().getCollection().size() > 1) {
				for (int i = 0; i < bookedLeg.getSegments().getCollection().size(); i++) {
					BookedSegment bookedSegment = bookedLeg.getSegments().getCollection().get(i);
					if (SegmentCodeUtil.compareSegmentCodeWithoutTime(bookedSegment.getSegment().getSegmentCode(),
							segmentCode)) {
						if (i + 1 < bookedLeg.getSegments().getCollection().size()) {
							bookedSegment = bookedLeg.getSegments().getCollection().get(i + 1);
							return !arrivalAirport.equalsIgnoreCase(bookedSegment.getSegment().getDepartureAirport());
						}
					}
				}
			}
		}
		return false;
	}

	private void validateDepartAirport(IssueBagRQ issueBagRQ, BookedLeg bookedLeg, String recLoc) {
		// Validating Departure Airport.
		// Only Kiosk
		if (PosType.KIOSK.toString().equalsIgnoreCase(issueBagRQ.getPos())) {
			boolean existsDepartureAirport = false;

			for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {
				if (null == issueBagRQ.getLocation() || issueBagRQ.getLocation().equalsIgnoreCase(bookedSegment.getSegment().getDepartureAirport())) {
					existsDepartureAirport = true;
				}
			}

			if ( ! existsDepartureAirport ) {
				throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.LOCATION_NOT_ALLOWED_BT, ErrorType.LOCATION_NOT_ALLOWED_BT.getFullDescription() + "|KIOSK_LOC:" 	+ issueBagRQ.getLocation() + "|PNR:" + recLoc);
			}
		}
	}

	/**
	 *
	 * @param bookedTraveler
	 * @throws Exception
	 */
	private void updateCheckedBaggages(BookedTraveler bookedTraveler) throws Exception {

		// Adding new checked Bag for that passenger
		bookedTraveler.setCheckedBags(bookedTraveler.getCheckedBags() + 1);

		// Total of free bags
		int totalFreeBagAllow = bookedTraveler.getFreeBaggageAllowancePerLegsArray().get(0).getFreeBaggageAllowance().getChecked();
		// int totalPrePaidBags = getTotalFreePrePaidBags(bookedTraveler);

		if ((totalFreeBagAllow - bookedTraveler.getCheckedBags()) < 0) {
			updateUsedPrePaidBags(Math.abs(totalFreeBagAllow - bookedTraveler.getCheckedBags()), bookedTraveler);
		}
	}

	/**
	 *
	 * @param checkedBags
	 * @param bookedTraveler
	 */
	private void updateUsedPrePaidBags(int checkedBags, BookedTraveler bookedTraveler) {
		for (AbstractAncillary abstractAncillary : bookedTraveler.getAncillaries().getCollection()) {
			if (abstractAncillary instanceof PaidTravelerAncillary) {
				PaidTravelerAncillary paidTravelerAncillary = (PaidTravelerAncillary) abstractAncillary;
				if ("BG".equalsIgnoreCase(paidTravelerAncillary.getGroupCode())) {
					if ( ! paidTravelerAncillary.getCommercialName().contains( "CARRY ON" ) ) {
						if (checkedBags > 0) {
							((PaidTravelerAncillary) abstractAncillary).setUsed(true);
							--checkedBags;
						}
					}
				}
			}
		}
	}

	/**
	 *
	 * @param bookedTraveler
	 * @return
	 */
	private int getTotalPrePaidBags(BookedTraveler bookedTraveler) {

		Log.info( "+getTotalPrePaidBags - bookedTraveler: {}", bookedTraveler);

		int total = 0;

		for (AbstractAncillary abstractAncillary : bookedTraveler.getAncillaries().getCollection()) {
			if (abstractAncillary instanceof PaidTravelerAncillary) {
				PaidTravelerAncillary paidTravelerAncillary = (PaidTravelerAncillary) abstractAncillary;
				if ("BG".equalsIgnoreCase(paidTravelerAncillary.getGroupCode())) {
					total++;
				}
			}
		}

		Log.info( "-getTotalPrePaidBags - total: {}", total);

		return total;
	}

	private int getTierBeneftFreeBags(BookedTraveler bookedTraveler) {

		int total = 0;

		try {
			total = bookedTraveler.getBookedTravellerBenefitCollection().getCollection().get(0).getBag().getFreeBagAllowance().getChecked();
		} catch (Exception e) {
			total = 0;
			// Ignore exception
		}

		return total;

	}

	/**
	 *
	 * @param issueBagTagRS
	 * @param recLoc
	 * @param bookedLegCollection
	 * @param issueBagReq
	 * @return
	 */
	private IssueBagRP getBagTag(IssueBagTagRSACS issueBagTagRS, String recLoc, BookedLegCollection bookedLegCollection,
			IssueBagReq issueBagReq, BookedTraveler bookedTraveler, String legCode, boolean flagGap, String departureDateTime) {

		IssueBagRP issueBagRP = new IssueBagRP();
		issueBagRP.setBagTagInfo(transformResponseBagTag(issueBagTagRS, recLoc, bookedLegCollection, issueBagReq.getWeight(), legCode, departureDateTime));
		issueBagRP.setBagTagNumber(getBagTagNumber(issueBagTagRS));
		issueBagRP.setPriorityCode(getPriorityCode(issueBagReq, bookedTraveler, bookedLegCollection, legCode));
		issueBagRP.setArnk(flagGap);

		return issueBagRP;
	}

	/**
	 * * This method used to update the bag tag number returned by Sabre. If the bag
	 * tag number is less than 10. Will return the number as is if it is 10 digits
	 * long.
	 *
	 * @return
	 */
	private void updateBagTagNumber(IssueBagTagRSACS issueBagTagRS, String latestArrivalAirport, BookedLegCollection bookedLegCollection, String legCode) {

		// We assume that we are going to recieve one pax per call.
		PassengerInfoACS passengerInfoACS = issueBagTagRS.getPassengerInfoList().getPassengerInfo().get(0);

		if (passengerInfoACS.getBagTagInfoList().getBagTagInfo().get(0).getBagTagNumber().length() < 10) {
			String newBagTagNumber = getBagIndexNumber(latestArrivalAirport, bookedLegCollection, legCode) + passengerInfoACS.getBagTagInfoList().getBagTagInfo().get(0).getBagTagNumber();
			passengerInfoACS.getBagTagInfoList().getBagTagInfo().get(0).setBagTagNumber(newBagTagNumber);
		}
	}

	/**
	 *
	 * @param issueBagTagRS
	 * @param recLoc
	 * @param bookedLegCollection
	 * @return
	 */
	private String transformResponseBagTag(IssueBagTagRSACS issueBagTagRS, String recLoc, BookedLegCollection bookedLegCollection, Integer weight, String legCode, String departureDateTime) {

		String latestArrivalAirport = getLatestArrivalAirport(issueBagTagRS.getItineraryInfoList());
		updateBagTagNumber(issueBagTagRS, latestArrivalAirport, bookedLegCollection, legCode);
		AirportWeather airportWeather = getAirportFromIata(latestArrivalAirport);
		String headerBagTag = getHeaderBagTag(issueBagTagRS, bookedLegCollection, legCode);
		String itineraryBagTag = getItinerayBagTag(issueBagTagRS, bookedLegCollection);
		String footerBgaTag = getFooterBagTag(issueBagTagRS, recLoc, weight, airportWeather, departureDateTime);

		return headerBagTag + itineraryBagTag + footerBgaTag;

	}

	/**
	 *
	 * @param issueBagTagRS
	 * @return
	 */
	private String getHeaderBagTag(IssueBagTagRSACS issueBagTagRS, BookedLegCollection bookedLegCollection, String legCode) {

		// We assume that we are going to recieve one pax per call.
		PassengerInfoACS passengerInfoACS = issueBagTagRS.getPassengerInfoList().getPassengerInfo().get(0);

		int numberOfSegments = 1;
		if (null != issueBagTagRS.getItineraryInfoList().getFlightConnectionInfo()) {
			numberOfSegments = issueBagTagRS.getItineraryInfoList().getFlightConnectionInfo().size() + 1;
		}

		String latestArrivalAirport = getLatestArrivalAirport(issueBagTagRS.getItineraryInfoList());
		String bagTagHeader = "BTP"; // STATIC
		bagTagHeader += "0" + numberOfSegments; // Number of Segments
		bagTagHeader += "0301*"; // STATIC
		bagTagHeader += "01AM *"; // STATIC

		bagTagHeader += "02" + passengerInfoACS.getBagTagInfoList().getBagTagInfo().get(0).getBagTagNumber() + "*"; // BAGTAG
																													// NUMBER

		// TAG
		// NUMBER
		bagTagHeader += padRight("03" + passengerInfoACS.getLastName() + "/" + passengerInfoACS.getFirstName(), 17)
				+ "*"; // PAX NAME
		bagTagHeader += padRight("05" + getCityNameByIATACode(latestArrivalAirport), 17) + "*"; // Arrival

		// City
		bagTagHeader += "06" + passengerInfoACS.getBagTagInfoList().getBagTagInfo().get(0).getBagTagNumber() + "*"; // BAGTAG
																													// NUMBER

		// TAG
		// NUMBER
		bagTagHeader += "07AEROMEXICO*"; // STATIC

		return bagTagHeader;
	}

	/**
	 *
	 * @param iataCode
	 * @param bookedLegCollection
	 * @return
	 */
	private String getBagIndexNumber(String iataCode, BookedLegCollection bookedLegCollection, String legCode) {
		/*
		 * GI#=The baggage index number is based on the first letter of the city code as
		 * follows: Interline 0 A-C 3 D-H 4 I-L 5 M-O 6 P-R 7 S 8 T-Z 9
		 */
		String result = "0139";
		if (!isIterlineFlight(bookedLegCollection, legCode)) {
			result = "139";
			switch (iataCode.substring(0, 1)) {
			case "A":
			case "B":
			case "C":
				result = "3" + result;
				break;
			case "D":
			case "E":
			case "F":
			case "G":
			case "H":
				result = "4" + result;
				break;
			case "I":
			case "J":
			case "K":
			case "L":
				result = "5" + result;
				break;
			case "M":
			case "N":
			case "O":
				result = "6" + result;
				break;
			case "P":
			case "Q":
			case "R":
				result = "7" + result;
				break;
			case "S":
				result = "8" + result;
				break;
			case "T":
			case "U":
			case "V":
			case "W":
			case "X":
			case "Y":
			case "Z":
				result = "9" + result;
				break;

			}

		}
		return result;
	}

	private boolean isIterlineFlight(BookedLegCollection bookedLegCollection, String legCode) {
		for (BookedLeg bookedLeg : bookedLegCollection.getCollection()) {
			if (bookedLeg.getSegments().getLegCode().equalsIgnoreCase(legCode)) {
				for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {
					if (!bookedSegment.getSegment().getOperatingCarrier().equalsIgnoreCase("AM")) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 *
	 * @param issueBagTagRS
	 * @param bookedLegCollection
	 * @return
	 */
	private String getItinerayBagTag(IssueBagTagRSACS issueBagTagRS, BookedLegCollection bookedLegCollection) {

		String bagTagIti = "";

		// We assume that we are going to recieve one pax per call.
		PassengerInfoACS passengerInfoACS = issueBagTagRS.getPassengerInfoList().getPassengerInfo().get(0);

		FlightInfoACS flightInfoACS = issueBagTagRS.getItineraryInfoList().getFlightInfo();

		Segment segment = getSegmentByOriginDestination(bookedLegCollection, flightInfoACS.getOrigin(),
				flightInfoACS.getDestination());

		bagTagIti += "10" + segment.getOperatingCarrier() + " *"; // OP CARRIER
		bagTagIti += "11" + padLeft(flightInfoACS.getFlight(), 4) + "*"; // FLIGHT
		// NUMBER
		bagTagIti += "12" + flightInfoACS.getDestination() + "*"; // Arrival
		// City

		if (passengerInfoACS.getCheckInNumber() < 10) {
			bagTagIti += "13" + "00" + passengerInfoACS.getCheckInNumber() + "*";
		} else if (passengerInfoACS.getCheckInNumber() < 100) {
			bagTagIti += "13" + "0" + passengerInfoACS.getCheckInNumber() + "*";
		} else {
			bagTagIti += "13" + passengerInfoACS.getCheckInNumber() + "*";
		}

		bagTagIti += "14" + getTimeFromSegmentCode(segment.getSegmentCode()) + "*";

		int numberOfSegment = 2;

		if (null != issueBagTagRS.getItineraryInfoList().getFlightConnectionInfo()) {

			for (FlightConnectionInfoACS flightConnectionInfoACS : issueBagTagRS.getItineraryInfoList()
					.getFlightConnectionInfo()) {
				segment = getSegmentByOriginDestination(bookedLegCollection, flightConnectionInfoACS.getOrigin(),
						flightConnectionInfoACS.getDestination());

				bagTagIti += numberOfSegment + "0" + segment.getOperatingCarrier() + " *"; // OP
				// CARRIER
				bagTagIti += numberOfSegment + "1" + padLeft(flightConnectionInfoACS.getFlight(), 4) + "*"; // FLIGHT
				// NUMBER
				bagTagIti += numberOfSegment + "2" + flightConnectionInfoACS.getDestination() + "*"; // Arrival
				// City

				// Check-in control or sequence number
				if (passengerInfoACS.getCheckInNumber() < 10) {
					bagTagIti += numberOfSegment + "3" + "00" + passengerInfoACS.getCheckInNumber() + "*";
				} else if (passengerInfoACS.getCheckInNumber() < 100) {
					bagTagIti += numberOfSegment + "3" + "0" + passengerInfoACS.getCheckInNumber() + "*";
				} else {
					bagTagIti += numberOfSegment + "3" + passengerInfoACS.getCheckInNumber() + "*";
				}

				segment = getSegmentByOriginDestination(bookedLegCollection, flightConnectionInfoACS.getOrigin(),
						flightConnectionInfoACS.getDestination());

				bagTagIti += numberOfSegment + "4" + getTimeFromSegmentCode(segment.getSegmentCode()) + "*"; // Departure
				// time
				// in
				// the
				// following
				// format:
				// 0835A
				// or
				// 0530P
				numberOfSegment++;
			}

		}

		return bagTagIti;
	}

	/**
	 *
	 * @param bookedLegCollection
	 * @param origin
	 * @param destination
	 * @return
	 */
	private Segment getSegmentByOriginDestination(BookedLegCollection bookedLegCollection, String origin,
			String destination) {

		for (BookedLeg bookedLeg : bookedLegCollection.getCollection()) {
			for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {
				String segmentCode = bookedSegment.getSegment().getSegmentCode().substring(0, 7);
				if ((origin + "_" + destination).equalsIgnoreCase(segmentCode)) {
					return bookedSegment.getSegment();
				}
			}
		}

		return null;

	}

	/*
	 * <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
	 * <ns4:ACS_IssueBagTagRS xmlns:ns2="http://services.sabre.com/STL/v3"
	 * xmlns:ns4="http://services.sabre.com/ACS/BSO/issueBagTag/v10"
	 * xmlns:ns3="http://services.sabre.com/checkin/issueBagTag/v4">
	 * <ItineraryInfoList> <FlightInfo> <Airline>AM</Airline> <Flight>706</Flight>
	 * <Origin>MEX</Origin> <Destination>HMO</Destination> </FlightInfo>
	 * </ItineraryInfoList>
	 * 
	 * <PassengerInfoList> <PassengerInfo> <LastName>TEPACH XOL</LastName>
	 * <FirstName>SANTI</FirstName> <PassengerID>C88BC0510001</PassengerID>
	 * <NumberInParty>1</NumberInParty> <Destination>HMO</Destination>
	 * <CheckInNumber>85</CheckInNumber> <BagCount>1</BagCount> <BagTagInfoList>
	 * <BagAdditionalInfo>060522</BagAdditionalInfo> <BagTagInfo>
	 * <BagTagNumber>060522</BagTagNumber> </BagTagInfo> <TotalWeightAndUnit
	 * unit="KG">5</TotalWeightAndUnit> </BagTagInfoList> <EditCodeList>
	 * <EditCode>M</EditCode> <EditCode>ET</EditCode> <EditCode>PRCH</EditCode>
	 * <EditCode>ETI</EditCode> <EditCode>AE</EditCode> <EditCode>KS</EditCode>
	 * </EditCodeList> </PassengerInfo> </PassengerInfoList> <Result
	 * messageId="ID-ckivlp129-sabre-com-33438-1479437779586-0-495909"
	 * timeStamp="2016-11-24T17:59:31.655Z"> <ns2:Status>Success</ns2:Status>
	 * <ns2:CompletionStatus>Complete</ns2:CompletionStatus>
	 * <ns2:System>CKI-WS</ns2:System> </Result> </ns4:ACS_IssueBagTagRS>
	 */
	/**
	 *
	 * @param issueBagTagRS
	 * @param recLoc
	 * @return
	 */
	private String getFooterBagTag(IssueBagTagRSACS issueBagTagRS, String recLoc, Integer weight,
			AirportWeather airportWeather, String departureDateTime) {

		String bagTagFooter = "";
		bagTagFooter += "70" + recLoc + "*";
		bagTagFooter += "71" + getDepartureTimeFomated(departureDateTime) + "*";
		bagTagFooter += "72" + issueBagTagRS.getItineraryInfoList().getFlightInfo().getOrigin() + "/KSK*";
		if (airportWeather != null && airportWeather.getAirport().getCountryCode() != null) {
			if (airportWeather.getAirport().getCountryCode().contains("US")) {
				String countryCode[] = airportWeather.getAirport().getCountryCode().split("-");
				if (countryCode.length > 0) {
					bagTagFooter += "75" + countryCode[1] + "*";
				} else {
					bagTagFooter += "75" + countryCode[0] + "*";
				}
			} else {
				bagTagFooter += "75" + airportWeather.getAirport().getCountryCode() + "*";
			}
		} else {
			throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.CART_NOT_FOUND_IN_COLLECTION,
					CommonUtil.getMessageByCode(Constants.CODE_001));
			// bagTagFooter += "75" + "MX*";
		}

		bagTagFooter += "88" + "TALON 1 DE 1    -*";

		bagTagFooter += "89" + weight + " *";

		bagTagFooter += "9D                 *";

		return bagTagFooter;

	}

	/**
	 *
	 * @param itineraryInfoList
	 * @return
	 */
	private String getLatestArrivalAirport(ItineraryInfoListACS itineraryInfoList) {

		if (null != itineraryInfoList.getFlightConnectionInfo()
				&& itineraryInfoList.getFlightConnectionInfo().size() > 0) {
			return itineraryInfoList.getFlightConnectionInfo()
					.get(itineraryInfoList.getFlightConnectionInfo().size() - 1).getDestination();
		} else {
			return itineraryInfoList.getFlightInfo().getDestination();
		}

	}

	/**
	 * @param String date/time format: 2019-03-27T21:45:00
	 * @return
	 */
	public String getDepartureTimeFomated(String departureDateTime) {

		String formatedDate = "";

		try {
			SimpleDateFormat parseFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			SimpleDateFormat returnFormat = new SimpleDateFormat("ddMMMyy'/'HHmma");

			Date date = parseFormat.parse(departureDateTime);

			formatedDate = returnFormat.format(date);
			formatedDate = formatedDate.replace("AM", "A").replace("PM", "P").toUpperCase();

		} catch (ParseException e) {
			e.printStackTrace();
		}

		return formatedDate;
	}

	/**
	 *
	 * @param segmentCode
	 * @return
	 */
	private String getTimeFromSegmentCode(String segmentCode) {
		String result = "";
		String segmentCodeSplit[] = segmentCode.split("_");
		if (segmentCodeSplit != null && segmentCodeSplit.length >= 5) {
			SimpleDateFormat formatter = new SimpleDateFormat("HHmm");
			try {

				Date date = formatter.parse(segmentCodeSplit[4]);
				SimpleDateFormat parseFormat = new SimpleDateFormat("hhmma");
				result = parseFormat.format(date).replace("AM", "A").replace("PM", "P");
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

		return result;
	}

	/**
	 *
	 * @param issueBagRQ
	 * @return
	 * @throws Exception
	 */
	private BookedTraveler getValidBookedTraveler(IssueBagRQ issueBagRQ) throws Exception {

		BookedTravelerCollection bookedTravelerCollection = issueBagRQ.getCartPNRUpdate().getTravelerInfo();

		if (bookedTravelerCollection == null || bookedTravelerCollection.getCollection() == null
				|| bookedTravelerCollection.getCollection().isEmpty()) {

			logger.error(CommonUtil.getMessageByCode(Constants.CODE_048) + " to the cartID : " + issueBagRQ.getCartId()
					+ ". " + issueBagRQ);

			throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.BAD_REQUEST_NOT_HAVE_BOOKEDTRAVELERS,
					ErrorType.BAD_REQUEST_NOT_HAVE_BOOKEDTRAVELERS.getFullDescription());
		}

		// Get the list of passengers
		List<BookedTraveler> bookedTravelerUpdateList = issueBagRQ.getCartPNRUpdate().getTravelerInfo().getCollection();

		// Getting only one passenger per request.
		if (bookedTravelerUpdateList.size() != 1) {
			throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.BAD_REQUEST_NOT_HAVE_BOOKEDTRAVELERS,
					ErrorType.BAD_REQUEST_NOT_HAVE_BOOKEDTRAVELERS.getFullDescription());
		}

		return bookedTravelerUpdateList.get(0);
	}

	/**
	 *
	 * @param passengerId
	 * @return
	 * @throws Exception
	 */
	private BookedTraveler getBookedTraveler(String passengerId, CartPNR cartPNR, String cartId) throws Exception {

		// Getting List of passenger from Mongo by CartId
		List<BookedTraveler> bookedTravelerList = cartPNR.getTravelerInfo().getCollection();

		for (BookedTraveler bookedTraveler : bookedTravelerList) {
			for (BookingClass bookingClass : bookedTraveler.getBookingClasses().getCollection()) {
				if (bookingClass.getPassengerId().equalsIgnoreCase(passengerId)) {
					return bookedTraveler;
				}
			}
		}

		logger.error(
				CommonUtil.getMessageByCode(Constants.CODE_048) + " to the cartID : " + cartId + " " + passengerId);

		throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.BAD_REQUEST_NOT_HAVE_BOOKEDTRAVELERS,
				ErrorType.BAD_REQUEST_NOT_HAVE_BOOKEDTRAVELERS.getFullDescription());
	}

	/**
	 *
	 * @param passengerId
	 * @param bookedTraveler
	 * @return
	 * @throws Exception
	 */
	private BookingClass getBookingClassByPaxId(String passengerId, BookedTraveler bookedTraveler) throws Exception {

		for (BookingClass bookingClass : bookedTraveler.getBookingClasses().getCollection()) {
			if (bookingClass.getPassengerId().equalsIgnoreCase(passengerId)) {
				return bookingClass;
			}
		}

		throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.BAD_REQUEST_NOT_HAVE_BOOKEDTRAVELERS,
				ErrorType.BAD_REQUEST_NOT_HAVE_BOOKEDTRAVELERS.getFullDescription());
	}

	/**
	 *
	 * @param bookedLegCollection
	 * @param segmentCode
	 * @return
	 */
	private Segment getSegmentBySegmentCode(BookedLegCollection bookedLegCollection, String segmentCode) {

		for (BookedLeg bookedLeg : bookedLegCollection.getCollection()) {
			for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {
				if (SegmentCodeUtil.compareSegmentCodeWithoutTime(bookedSegment.getSegment().getSegmentCode(),
						segmentCode)) {
					return bookedSegment.getSegment();
				}
			}
		}

		throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.BOOKEDSEGMENT_NOT_FOUND,
				ErrorType.BOOKEDSEGMENT_NOT_FOUND.getFullDescription());
	}

	private String getBagTagNumber(IssueBagTagRSACS issueBagTagRS) {
		try {
			PassengerInfoACS passengerInfoACS = issueBagTagRS.getPassengerInfoList().getPassengerInfo().get(0);
			return passengerInfoACS.getBagTagInfoList().getBagTagInfo().get(0).getBagTagNumber();
		} catch (Exception ex) {
			logger.error("IssueBagTag | Getting BagTagNumber | " + ex.getMessage());
		}
		return "";
	}

	/**
	 *
	 * @param s
	 * @param n
	 * @return
	 */
	public static String padRight(String s, int n) {
		if (s.length() > n) {
			return s.substring(0, n);
		} else {
			return String.format("%1$-" + n + "s", s);
		}

	}

	/**
	 *
	 * @param s
	 * @param n
	 * @return
	 */
	public static String padLeft(String s, int n) {
		if (s.length() > n) {
			return s.substring(0, n);
		} else {
			return String.format("%1$" + n + "s", s);
		}
	}

	/**
	 *
	 * @param iataAirport
	 * @return
	 */
	public String getCityNameByIATACode(String iataAirport) {
		String airportInfo = iataAirport;
		try {
			airportInfo = airportsCodes.getAirportWeatherCityNameByIata(iataAirport);
			if (null == airportInfo) {
				airportInfo = iataAirport;
			}
		} catch (Exception e) {
			logger.info("getCityNameByIATACode - " + iataAirport + "( " + e.getMessage().toString() + " )");
		}
		return airportInfo.toUpperCase();
	}

	private AirportWeather getAirportFromIata(String iata) {
		AirportWeather airportWeather = new AirportWeather();
		try {
			airportWeather = airportsCodes.getAirportWeatherByIata(iata);
		} catch (Exception e) {
			logger.info("getCityNameByIATACode - " + iata + "( " + e.getMessage().toString() + " )");
		}
		return airportWeather;

	}

	private String getPriorityCode(IssueBagReq issueBagReq, BookedTraveler bookedTraveler,
			BookedLegCollection bookedLegCollection, String legCode) {

		// GetLegType
		String legType = getLegType(legCode, bookedLegCollection);

		BagTagPriorityType bagTagPriorityType = null;
		if ("CONNECTING".equalsIgnoreCase(legType)) {
			bagTagPriorityType = BagTagPriorityType.NORMALC;
		} else {
			bagTagPriorityType = BagTagPriorityType.NORMAL;
		}

		// If Premier Class.
		if ("C".equalsIgnoreCase(issueBagReq.getBagRouteInfo().getBookingClass())
				|| "J".equalsIgnoreCase(issueBagReq.getBagRouteInfo().getBookingClass())) {
			if ("CONNECTING".equalsIgnoreCase(legType)) {
				bagTagPriorityType = BagTagPriorityType.PRIORITYC;
			} else {
				bagTagPriorityType = BagTagPriorityType.PRIORITY;
			}

		}

		// If has Paid Seat.
		if (null != bookedTraveler.getSeatAncillaries()) {
			for (AbstractAncillary abstractAncillary : bookedTraveler.getSeatAncillaries().getCollection()) {
				if (abstractAncillary instanceof PaidTravelerAncillary) {
					PaidTravelerAncillary paidTravelerAncillary = (PaidTravelerAncillary) abstractAncillary;
					if (paidTravelerAncillary.getCommercialName().contains("PAIDSEAT")
							&& paidTravelerAncillary.getActionCode().equalsIgnoreCase("HK")) {
						if ("CONNECTING".equalsIgnoreCase(legType)) {
							bagTagPriorityType = BagTagPriorityType.NORMALC;
						} else {
							bagTagPriorityType = BagTagPriorityType.NORMAL;
						}
					}

					if (paidTravelerAncillary.getCommercialName().contains("PAIDSEAT")
							&& paidTravelerAncillary.getActionCode().equalsIgnoreCase("HI")
							&& paidTravelerAncillary.getTotalPrice().getTotal().compareTo(BigDecimal.ZERO) > 0) {
						if ("CONNECTING".equalsIgnoreCase(legType)) {
							bagTagPriorityType = BagTagPriorityType.PRIORITYC;
						} else {
							bagTagPriorityType = BagTagPriorityType.PRIORITY;
						}
					}
				}
			}
		}

		return bagTagPriorityType.toString();
	}

	private String getLegType(String legCode, BookedLegCollection bookedLegCollection) {
		for (BookedLeg bookedLeg : bookedLegCollection.getCollection()) {
			if (legCode.equalsIgnoreCase(bookedLeg.getSegments().getLegCode())) {
				if (LegType.CONNECTING.toString().equalsIgnoreCase(bookedLeg.getSegments().getLegType())) {
					return "CONNECTING";
				}
			}
		}
		return "NON-STOP";
	}

	/**
	 * @return the setUpConfigFactory
	 */
	public CheckInSetUpConfigFactoryWrapper getSetUpConfigFactory() {
		if (null == setUpConfigFactory) {
			setUpConfigFactory = new CheckInSetUpConfigFactoryWrapper();
		}
		return setUpConfigFactory;
	}

	/**
	 * @param setUpConfigFactory the setUpConfigFactory to set
	 */
	public void setSetUpConfigFactory(CheckInSetUpConfigFactoryWrapper setUpConfigFactory) {
		this.setUpConfigFactory = setUpConfigFactory;
	}

	/**
	 * @param pnrCollection This method is used to update the PNR collection MongoDB
	 *                      in an asynchronous mode
	 */
	private void updatePnrCollection(PNRCollection pnrCollection) {
		try {
			updatePNRCollectionDispatcher.publish(new UpdatePNRCollectionEvent(pnrCollection));
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}

	}

	public PnrCollectionService getPnrCollectionService() {
		return pnrCollectionService;
	}

	public void setPnrCollectionService(PnrCollectionService pnrCollectionService) {
		this.pnrCollectionService = pnrCollectionService;
	}

	public AMIssueBagTagService getIssueBagTagService() {
		return issueBagTagService;
	}

	public void setIssueBagTagService(AMIssueBagTagService issueBagTagService) {
		this.issueBagTagService = issueBagTagService;
	}

	public IAirportCodesDao getAirportsCodes() {
		return airportsCodes;
	}

	public void setAirportsCodes(IAirportCodesDao airportsCodes) {
		this.airportsCodes = airportsCodes;
	}

	public UpdatePNRCollectionDispatcher getUpdatePNRCollectionDispatcher() {
		return updatePNRCollectionDispatcher;
	}

	public void setUpdatePNRCollectionDispatcher(UpdatePNRCollectionDispatcher updatePNRCollectionDispatcher) {
		this.updatePNRCollectionDispatcher = updatePNRCollectionDispatcher;
	}
}
