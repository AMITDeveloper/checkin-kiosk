
package com.am.checkin.web.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import com.aeromexico.commons.model.GitRepositoryState;

@Named
@ApplicationScoped
public class GitRepositoryStateService {

	GitRepositoryState gitRepositoryState;

	@PostConstruct
	public void setup() {

		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		InputStream inputStream = cl.getResourceAsStream("git.properties");
		Properties properties = new Properties();

		try {

			properties.load(inputStream);
			gitRepositoryState = new GitRepositoryState();

			gitRepositoryState.setClosestTagCommitCount( properties.getProperty("git.closest.tag.commit.count") );
			gitRepositoryState.setClosestTagName( properties.getProperty("git.closest.tag.name") );
			//gitRepositoryState.setCommitId( properties.getProperty("") );
			gitRepositoryState.setCommitIdAbbrev( properties.getProperty("git.commit.id.abbrev") );
			gitRepositoryState.setCommitMessageFull( properties.getProperty("git.commit.message.full") );
			gitRepositoryState.setCommitMessageShort( properties.getProperty("git.commit.message.short") );
			gitRepositoryState.setCommitTime( properties.getProperty("git.commit.time") );
			gitRepositoryState.setCommitUserEmail( properties.getProperty("git.commit.user.email") );
			gitRepositoryState.setCommitUserName( properties.getProperty("git.commit.user.name") );
			gitRepositoryState.setDescribe( properties.getProperty("git.commit.id.describe") );
			gitRepositoryState.setDescribeShort( properties.getProperty("git.commit.id.describe-short") );
			gitRepositoryState.setDirty( properties.getProperty("git.dirty") );
			gitRepositoryState.setRemoteOriginUrl( properties.getProperty("git.remote.origin.url") );
			gitRepositoryState.setTags( properties.getProperty("git.tags") );
			gitRepositoryState.setJenkins( properties.getProperty( "jenkins.build.number" ) );

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public GitRepositoryState getGitRepositoryState() {
		return gitRepositoryState;
	}

}
