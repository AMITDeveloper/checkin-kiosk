package com.am.checkin.web.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;

import com.aeromexico.commons.model.AbstractAncillary;
import com.aeromexico.commons.model.AncillariesCollectionWrapper;
import com.aeromexico.commons.model.Ancillary;
import com.aeromexico.commons.model.AncillaryOffer;
import com.aeromexico.commons.model.AncillaryOfferCollection;
import com.aeromexico.commons.model.BenefitCorporateRecognition;
import com.aeromexico.commons.model.BenefitRedeemed;
import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.CabinUpgradeAncillary;
import com.aeromexico.commons.model.CheckedWeight;
import com.aeromexico.commons.model.CurrencyValue;
import com.aeromexico.commons.model.ExtraWeightAncillary;
import com.aeromexico.commons.model.FreeBaggageAllowanceDetailByCard;
import com.aeromexico.commons.model.FreeBaggageAllowancePerLeg;
import com.aeromexico.commons.model.PNR;
import com.aeromexico.commons.model.PNRCollection;
import com.aeromexico.commons.model.PaidTravelerAncillary;
import com.aeromexico.commons.model.TravelerAncillary;
import com.aeromexico.commons.model.Warning;
import com.aeromexico.commons.model.WarningCollection;
import com.aeromexico.dao.qualifiers.TierBenefitsCache;
import com.aeromexico.dao.services.ITierBenefitDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.rq.PurchaseOrderRQ;
import com.aeromexico.commons.model.rq.ShoppingCartRQ;
import com.aeromexico.commons.model.utils.CurrencyUtil;
import com.aeromexico.commons.web.types.AncillaryGroupType;
import com.aeromexico.commons.web.types.AncillaryPostBookingType;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.types.PosType;
import com.aeromexico.commons.web.types.StoreFrontType;
import com.aeromexico.commons.web.types.WeightUnitType;
import com.aeromexico.commons.web.util.ErrorCodeDescriptions;
import com.aeromexico.dao.services.IPNRLookupDao;
import com.aeromexico.sabre.api.sabrecommand.AMSabreCommandService;
import com.aeromexico.sabre.api.session.common.ServiceCall;
import com.aeromexico.sabre.api.session.exception.SabreLayerUnavailableException;
import com.aeromexico.sabre.api.sessionpool.factory.SabreSession;
import com.aeromexico.sabre.api.sessionpool.manager.SabreSessionPoolManager;
import com.aeromexico.sabre.api.updatereservation.AMUpdateReservationService;
import com.aeromexico.sharedservices.services.GetReservationService;
import com.aeromexico.sharedservices.util.AncillaryUtil;
import com.am.checkin.web.util.PNRLookUpServiceUtil;
import com.am.checkin.web.util.PurchaseOrderUtil;
import com.am.checkin.web.util.ShoppingCartErrorUtil;
import com.am.checkin.web.util.ShoppingCartUtil;
import com.google.gson.Gson;
import com.mongodb.MongoException;
import com.sabre.services.micellaneous.SegmentIndicator;
import com.sabre.webservices.sabrexml._2003._07.SabreCommandLLSRS;

@Named
@ApplicationScoped
public class ShoppingCartAncillaries {

    private static final Logger LOG = LoggerFactory.getLogger(ShoppingCartAncillaries.class);

    @Inject
    private IPNRLookupDao pnrLookupDao;

    @Inject
    private AMUpdateReservationService amUpdateReservationService;

    @Inject
    private AEService aeService;

    @Inject
    @TierBenefitsCache
    private ITierBenefitDao tierBenefitDao;

    @Inject
    private AMSabreCommandService sabreCommandService;

    @Inject
    private SabreSessionPoolManager sabreSessionPoolManager;
    
    @Inject
    private GetReservationService getReservationService;


    public void updateAncillary(
            BookedTraveler bookedTraveler,
            BookedTraveler bookedTravelerUpdate,
            PNRCollection pnrCollection,
            String cartId,
            ShoppingCartRQ shoppingCartRQ,
            AncillaryOfferCollection ancillaryOfferCollectionOnDB,
            WarningCollection warningCollection
    ) throws Exception {

    	LOG.info( "+putShoppingCart.updateAncillary" );

    	// For Ancillaries
        if (null != bookedTravelerUpdate.getAncillaries()
                && null != bookedTravelerUpdate.getAncillaries().getCollection()
                && !bookedTravelerUpdate.getAncillaries().getCollection().isEmpty()) {

            if (null == bookedTraveler.getAncillaries()
                    || bookedTraveler.getAncillaries().getCollection().isEmpty()
                    || (!ShoppingCartUtil.compareAncillaryCollections(bookedTraveler.getAncillaries().getCollection(), bookedTravelerUpdate.getAncillaries().getCollection()))) {

                TravelerAncillary travelerAncillaryToRedeem = AncillaryUtil.findBenefitBgAncillary(bookedTravelerUpdate);

                if (null != travelerAncillaryToRedeem) {
                    //remove all ancillaries
                    bookedTraveler.getAncillaries().removeOnlyUnpaid();
                }

                for (AbstractAncillary abstractAncillaryUpdate : bookedTravelerUpdate.getAncillaries().getCollection()) {

                    if (abstractAncillaryUpdate instanceof TravelerAncillary) {

                        addOrRemoveTravelerAncillary(
                                abstractAncillaryUpdate,
                                bookedTraveler,
                                ancillaryOfferCollectionOnDB,
                                pnrCollection,
                                cartId,
                                bookedTravelerUpdate,
                                shoppingCartRQ,
                                warningCollection
                        );

                    } else if (abstractAncillaryUpdate instanceof ExtraWeightAncillary) {

                        addOrRemoveExtraWeight(abstractAncillaryUpdate, bookedTraveler, ancillaryOfferCollectionOnDB, cartId, bookedTravelerUpdate, shoppingCartRQ.getStore(), pnrCollection);

                    } else if (abstractAncillaryUpdate instanceof CabinUpgradeAncillary) {

                        addOrRemoveUpgrade(bookedTraveler, pnrCollection, cartId, abstractAncillaryUpdate);

                    }
                }
            }
        }
    }

    private void addOrRemoveTravelerAncillary(
            AbstractAncillary abstractAncillaryUpdate,
            BookedTraveler bookedTraveler,
            AncillaryOfferCollection ancillaryOfferCollectionOnDB,
            PNRCollection pnrCollection,
            String cartId,
            BookedTraveler bookedTravelerUpdate,
            ShoppingCartRQ shoppingCartRQ,
            WarningCollection warningCollection
    ) throws GenericException, Exception {

        TravelerAncillary travelerAncillaryUpdate = (TravelerAncillary) abstractAncillaryUpdate;

        if (null == ancillaryOfferCollectionOnDB) {
            try {
                ancillaryOfferCollectionOnDB = getAncillariesCollection(cartId);
            } catch (MongoException ex) {
                ancillaryOfferCollectionOnDB = null;
            }
        }

        LOG.info("ancillaryOfferCollectionOnDB {} ", ancillaryOfferCollectionOnDB.toString() );
        if (null != travelerAncillaryUpdate.getBenefitCodeId()) {// have a
            // benefit
            // adding free bag by benefit!
            String typeBag = getTypeBag(bookedTraveler, travelerAncillaryUpdate, shoppingCartRQ.getPos());
            LOG.info("typeBag " + typeBag);
            
            if ("0CC".equalsIgnoreCase(typeBag)) {
                if (null != ancillaryOfferCollectionOnDB) {
                    //Realiza cambio de codigo
                    // read from DB
                    Ancillary ancillaryDB = AncillaryUtil.getAncillaryFromAncillaryOfferCollection(
                        typeBag, ancillaryOfferCollectionOnDB
                    );
                    
                    if (null == ancillaryDB) {
                        ancillaryDB = AncillaryUtil.getAncillaryFromAncillaryOfferCollection(
                            "0IA", ancillaryOfferCollectionOnDB
                        );
                        if (null != ancillaryDB) {
                            typeBag = "0IA";
                        }
                    }
                }
            }
            LOG.info("typeBag_dos " + typeBag);
            if (null != typeBag) {
                travelerAncillaryUpdate.getAncillary().setType(typeBag);
            } else {
                LOG.info("THE TYPE CODE ANCILLARY NOT EXIST");
                Warning warning = new Warning();
                warning.setCode(ErrorType.ERROR_TO_ADD_CODE.getErrorCode());
                warning.setMsg(ErrorType.ERROR_TO_ADD_CODE.getFullDescription());
                warningCollection.addToList(warning);
                return;
            }
        }

        AncillaryPostBookingType bgAncillaryType = AncillaryPostBookingType.getType(travelerAncillaryUpdate.getAncillary().getType());

         // carryOn
        if(bgAncillaryType.getRficSubCode().equalsIgnoreCase("0MJ")){
            bgAncillaryType = AncillaryPostBookingType.getType("0CZ");
        }

        if (null != bgAncillaryType) {

            boolean isPartOfReservation = ShoppingCartUtil.isPartOfReservation(
                    travelerAncillaryUpdate.getAncillary(),
                    bookedTraveler.getAncillaries().getCollection()
            );

            AbstractAncillary abstractAncillaryDB = ShoppingCartUtil.getSameAncillary(
                    travelerAncillaryUpdate.getAncillary(),
                    bookedTraveler.getAncillaries().getCollection()
            );

            if (isPartOfReservation && null != abstractAncillaryDB) {

                // if it is an unpaid ancillary
                if (abstractAncillaryDB instanceof TravelerAncillary) {

                    TravelerAncillary ancillarydb = (TravelerAncillary) abstractAncillaryDB;

                    // It will be removed on purchase time, we
                    // just set the quantity to zero
                    if (travelerAncillaryUpdate.getQuantity() <= 0) {
                        ancillarydb.setQuantity(0);
                    } else {
                        // if the ancillary already exist we
                        // just ignore the request, we set the
                        // quantity to one
                        ancillarydb.setQuantity(1);
                    }

                } else if (abstractAncillaryDB instanceof PaidTravelerAncillary) { // for
                    // already
                    // paid
                    // ancillaries
                    LOG.error(
                            ErrorType.CHANGE_PAID_ANCILLARY_NOT_ALLOWED.getFullDescription() + " PASSENGER ID : "
                            + bookedTraveler.getId()
                    );

                    throw new GenericException(
                            Response.Status.UNAUTHORIZED,
                            ErrorType.CHANGE_PAID_ANCILLARY_NOT_ALLOWED,
                            ErrorType.CHANGE_PAID_ANCILLARY_NOT_ALLOWED.getFullDescription() + " PASSENGER ID : "
                            + bookedTraveler.getId()
                    );
                }

            } // for new ancillaries
            else if (null == abstractAncillaryDB && travelerAncillaryUpdate.getQuantity() >= 1) {

                addNewAncillary(
                        ancillaryOfferCollectionOnDB,
                        pnrCollection,
                        cartId,
                        travelerAncillaryUpdate,
                        bgAncillaryType,
                        bookedTraveler,
                        bookedTravelerUpdate,
                        shoppingCartRQ,
                        warningCollection
                );

            } else if (null != abstractAncillaryDB && travelerAncillaryUpdate.getQuantity() >= 1) {

                if (abstractAncillaryDB instanceof TravelerAncillary) {
                    TravelerAncillary ancillarydb = (TravelerAncillary) abstractAncillaryDB;
                    ancillarydb.setQuantity(1);
                }

            } else {
                
                travelerAncillaryUpdate.getAncillary().setGroupCode(bgAncillaryType.getGroup());
                travelerAncillaryUpdate.getAncillary().setVendor(bgAncillaryType.getVendor());
                travelerAncillaryUpdate.getAncillary().setSsrCode(bgAncillaryType.getSsr());
                travelerAncillaryUpdate.getAncillary().setRficSubcode(bgAncillaryType.getRficSubCode());
                travelerAncillaryUpdate.getAncillary().setRficCode(bgAncillaryType.getRficCode());
                travelerAncillaryUpdate.getAncillary().setCommercialName(bgAncillaryType.getCommercialName());
                travelerAncillaryUpdate.getAncillary().setSegmentIndicator(SegmentIndicator.P.toString());
                travelerAncillaryUpdate.getAncillary().setEmdType(bgAncillaryType.getEmdType());
                travelerAncillaryUpdate.setNameNumber(bookedTravelerUpdate.getNameRefNumber());

                ShoppingCartUtil.removeAncillaryFromDB(
                        travelerAncillaryUpdate.getAncillary(),
                        bookedTraveler.getAncillaries().getCollection()
                );
            }
        } // ignore
    }

    private String getTypeBag(BookedTraveler bookedTraveler, TravelerAncillary travelerAncillaryUpdate, String pos) {
        
        // get code bag
        String sigCodeType = null;

        List<AbstractAncillary> listPaidTravelerAncillary = filterAncillariesTypeBG(bookedTraveler.getAncillaries().getCollection());

        if (null != listPaidTravelerAncillary && !listPaidTravelerAncillary.isEmpty()) {
            sigCodeType = getType(listPaidTravelerAncillary);

        } else {
            int freeTierBag = 0;
            try {
                // Is there a free tier bag benefit?
                if( null != travelerAncillaryUpdate.getBenefitCodeId() && !travelerAncillaryUpdate.getBenefitCodeId().isEmpty()) {
                    String benefitCodeIdRQ = travelerAncillaryUpdate.getBenefitCodeId();

                    if (benefitCodeIdRQ.equalsIgnoreCase(bookedTraveler.getBookedTravellerBenefitCollection().getCollection().get(0).getBag().getReason())) {
                        freeTierBag = bookedTraveler.getBookedTravellerBenefitCollection().getCollection().get(0).getBag().getFreeBagAllowance().getChecked();
                    } else {
                        if (null != bookedTraveler.getBookedTravellerBenefitCollection().getCollection().get(0).getBagBenefitCobrand()
                            && !bookedTraveler.getBookedTravellerBenefitCollection().getCollection().get(0).getBagBenefitCobrand().getFreeBagAllowanceByCardList().isEmpty()) {

                            for (FreeBaggageAllowanceDetailByCard FreeBaggageAllowanceDetailByCard : bookedTraveler.getBookedTravellerBenefitCollection().getCollection().get(0).getBagBenefitCobrand().getFreeBagAllowanceByCardList()) {
                                if (benefitCodeIdRQ.equalsIgnoreCase(FreeBaggageAllowanceDetailByCard.getCobrandCode())) {
                                    freeTierBag = FreeBaggageAllowanceDetailByCard.getChecked();
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                // ignore
            }

            int bagAllownace = 0;
            
            if (freeTierBag == 1) {
                if (bookedTraveler.getFreeBaggageAllowancePerLegsArray() != null && bookedTraveler.getFreeBaggageAllowancePerLegsArray().size() > 0) {
                    bagAllownace = bookedTraveler.getFreeBaggageAllowancePerLegsArray().get(0).getFreeBaggageAllowance().getChecked();
                }
                switch (bagAllownace) { // if free bag allowance is 0, then 0CC is going to be free
                    case 0:
                        sigCodeType = "0CC";
                        break;
                    case 1:
                        sigCodeType = "0CD";
                        break;
                    case 2:
                        sigCodeType = "0CE";
                        break;
                    case 3:
                        sigCodeType = "0CF";
                        break;
                    case 4:
                        sigCodeType = "0CG";
                        break;
                    case 5:
                        sigCodeType = "0CH";
                        break;
                    case 6:
                        sigCodeType = "0CI";
                        break;
                    case 7:
                        sigCodeType = "0CJ";
                        break;
                    case 8:
                        sigCodeType = "0CK";
                        break;
                    case 9:
                        sigCodeType = "0EN";
                        break;
                    default:
                        sigCodeType = null;
                        break;
                }
            }
            travelerAncillaryUpdate.setQuantity(1);
        }
        return sigCodeType;
    }

    private String getType(List<AbstractAncillary> listPaidTravelerAncillary) {
        String type = null;
        int code = -1;

        for (AbstractAncillary abstractAncillary : listPaidTravelerAncillary) {
            if (abstractAncillary instanceof PaidTravelerAncillary) {
                PaidTravelerAncillary paidTravelerAncillary = (PaidTravelerAncillary) abstractAncillary;
                switch (paidTravelerAncillary.getType()) {
                    case "0CC":
                    case "0IA":
                        if (code < 0) {
                            code = 0;
                            type = "0CD";
                        }
                        break;
                    case "0IZ":
                        if (code < 0) {
                            code = 0;
                            type = "0CC";
                        }
                        break;
                    case "0CD":
                        if (code < 1) {
                            code = 1;
                            type = "0CE";
                        }
                        break;
                    case "0CE":
                        if (code < 2) {
                            code = 2;
                            type = "0CF";
                        }
                        break;
                    case "0CF":
                        if (code < 3) {
                            code = 3;
                            type = "0CG";
                        }
                        break;
                    case "0CG":
                        if (code < 4) {
                            code = 4;
                            type = "0CH";
                        }
                        break;
                    case "0CH":
                        if (code < 5) {
                            code = 5;
                            type = "0CI";
                        }
                        break;
                    case "0CI":
                        if (code < 6) {
                            code = 6;
                            type = "0CJ";
                        }
                        break;
                    case "0CJ":
                        if (code < 7) {
                            code = 7;
                            type = "0CK";
                        }
                        break;
                    case "0CK":
                        if (code < 8) {
                            code = 8;
                            type = "0EN";
                        }
                        break;
                    case "0EN":
                        if (code < 9) {
                            code = 9;
                            type = null;
                        }
                        break;
                }
            }

        }

        return type;
    }

    private List<AbstractAncillary> filterAncillariesTypeBG(List<AbstractAncillary> ancillariesList) {
        return ancillariesList.stream()
                .filter(anc -> anc instanceof PaidTravelerAncillary
                && ((PaidTravelerAncillary) anc).getGroupCode().equalsIgnoreCase("BG"))
                .collect(Collectors.toList());
    }

    private void addNewAncillary(
            AncillaryOfferCollection ancillaryOfferCollectionOnDB,
            PNRCollection pnrCollection,
            String cartId,
            TravelerAncillary travelerAncillaryUpdate,
            AncillaryPostBookingType bgAncillaryType,
            BookedTraveler bookedTraveler,
            BookedTraveler bookedTravelerUpdate,
            ShoppingCartRQ shoppingCartRQ,
            WarningCollection warningCollection
    ) throws GenericException, Exception {

        if (null == ancillaryOfferCollectionOnDB) {
            try {
                ancillaryOfferCollectionOnDB = getAncillariesCollection(cartId);
            } catch (MongoException ex) {
                ancillaryOfferCollectionOnDB = null;
            }
        }

        // read from DB
        Ancillary ancillaryDB = AncillaryUtil.getAncillaryFromAncillaryOfferCollection(
                travelerAncillaryUpdate.getAncillary().getType(), ancillaryOfferCollectionOnDB
        );

        if (null == ancillaryDB) {
            // Throw exception
            LOG.error(ErrorType.UNABLE_TO_RETRIEVE_PRICE_FOR_ANCILLARY.getFullDescription() + " | ancillary: "
                    + travelerAncillaryUpdate.getAncillary().getType());

            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNABLE_TO_RETRIEVE_PRICE_FOR_ANCILLARY,
                    ErrorType.UNABLE_TO_RETRIEVE_PRICE_FOR_ANCILLARY.getFullDescription() + " | ancillary: "
                    + travelerAncillaryUpdate.getAncillary().getType());
        }

        PNR pnr = pnrCollection.getPNRByCartId(cartId);
        
        travelerAncillaryUpdate.getAncillary().setGroupCode(bgAncillaryType.getGroup());
        travelerAncillaryUpdate.getAncillary().setVendor(bgAncillaryType.getVendor());
        travelerAncillaryUpdate.getAncillary().setSsrCode(bgAncillaryType.getSsr());
        travelerAncillaryUpdate.getAncillary().setRficSubcode(bgAncillaryType.getRficSubCode());
        travelerAncillaryUpdate.getAncillary().setRealRficSubcode(ancillaryDB.getRealRficSubcode());
        travelerAncillaryUpdate.getAncillary().setListSegmentCode(ancillaryDB.getListSegmentCode());
        travelerAncillaryUpdate.getAncillary().setRficCode(bgAncillaryType.getRficCode());
        travelerAncillaryUpdate.getAncillary().setCommercialName(bgAncillaryType.getCommercialName());
        travelerAncillaryUpdate.getAncillary().setSegmentIndicator(SegmentIndicator.P.toString());
        travelerAncillaryUpdate.getAncillary().setEmdType(bgAncillaryType.getEmdType());
        travelerAncillaryUpdate.getAncillary().setOwningCarrierCode(bgAncillaryType.getOwningCarrierCode());
        travelerAncillaryUpdate.setNameNumber(bookedTravelerUpdate.getNameRefNumber());

        // maybe we have to validate if currency in the request has not been modified

        /*
		boolean benefits = false;

        if (PosType.KIOSK.toString().equalsIgnoreCase(shoppingCartRQ.getPos())) {

        	// Is there a free bag tier benefit?
            benefits = getBenefits(bookedTraveler, travelerAncillaryUpdate);
            if (benefits) {
                travelerAncillaryUpdate.setApplyBenefit(true);
                travelerAncillaryUpdate.getAncillary().setCurrency(CurrencyUtil.getCurrencyValue(ancillaryDB.getCurrency().getCurrencyCode()));
            } else {

                CurrencyValue currencyValue = CurrencyUtil.getTestCurrencyValue(ancillaryDB.getCurrency().getCurrencyCode());

                if (null == currencyValue) {
                    currencyValue = ancillaryDB.getCurrency();
                }

                travelerAncillaryUpdate.getAncillary().setCurrency(currencyValue);
            }
            bookedTraveler.getAncillaries().getCollection().add(travelerAncillaryUpdate);
        } else 
        */

        if (null != travelerAncillaryUpdate.getBenefitCodeId()) {// WEB

        } else 
        */
        if (null != travelerAncillaryUpdate.getBenefitCodeId()) {

        travelerAncillaryUpdate.setApplyBenefit(true);
        travelerAncillaryUpdate.getAncillary().setCurrency( CurrencyUtil.getCurrencyValue( ancillaryDB.getCurrency().getCurrencyCode() ) );
        bookedTraveler.getAncillaries().getCollection().add(travelerAncillaryUpdate);

        PurchaseOrderRQ purchaseOrderRQ = new PurchaseOrderRQ();
        purchaseOrderRQ.setStore(shoppingCartRQ.getStore());
        List<BookedTraveler> bookedTravelersRequested = new ArrayList<>();
        bookedTravelersRequested.add(bookedTraveler);
        BookedLeg bookedLeg = pnr.getBookedLegByCartId(cartId);
        String recordLocator = pnr.getPnr();

        List<ServiceCall> serviceCallList = new ArrayList<>();

        boolean addBag = false;

        try {
        	aeService.addNewAncillariesToReservation(
        			purchaseOrderRQ,
        			bookedTravelersRequested,
        			bookedLeg,
        			pnr.getGlobalMarketFlight(),
        			recordLocator,
        			cartId,
        			shoppingCartRQ.getStore(),
        			serviceCallList
        			);
                LOG.info("ADDING FREE BAG TO RESERVATION IN SABRE");
                addBag = true;
                travelerAncillaryUpdate.getAncillary().setRedeemed(true);
                List<TravelerAncillary> travelerAncillariesAdded = new ArrayList<>();
                getTravelerAncillaries(bookedTravelersRequested, travelerAncillariesAdded);
                List<String> litsRemarks = reviewAndbuildRemark(bookedTravelersRequested);
                if (!litsRemarks.isEmpty()) {
                    boolean addRemark = amUpdateReservationService.createRemark(recordLocator, litsRemarks, "CHECKIN_API_", null);
                    if (addRemark) {
                        // setBenefitRedeemed
                        String codeBenefitRedeemed = travelerAncillaryUpdate.getBenefitCodeId();
                        if (null != codeBenefitRedeemed) {
                            // review if exit a free bag
                            BenefitRedeemed benefitRedeemed = PNRLookUpServiceUtil.setBenefitInfo(bookedTraveler,
                                    codeBenefitRedeemed, travelerAncillaryUpdate.getLegCode());
                            if (null != benefitRedeemed) {
                                bookedTraveler.getBenefitRedeemedCollection().getCollection().add(benefitRedeemed);

                            }

                        }
                    }
                    if (addBag && addRemark) {
                        PaidTravelerAncillary paidTravelerAncillary = transformTravelerAncillary(bookedTraveler);
                        if (null != paidTravelerAncillary) {
                            int index = bookedTraveler.getAncillaries().getIndexByStoredId(paidTravelerAncillary.getStoredId());
                            if (index >= 0) {
                                bookedTraveler.getAncillaries().getCollection().set(index, paidTravelerAncillary);
                            }
                        }
                    }
                }

            } catch (GenericException msg) {
                ShoppingCartUtil.removeAncillaryFromDB(travelerAncillaryUpdate.getAncillary(),
                        bookedTraveler.getAncillaries().getCollection());
                addBag = false;
                LOG.info("A ERROR OCCURS WHEN CREATING AE");
                ShoppingCartErrorUtil.checkErrorToCreateAE(msg, warningCollection);
            } catch (Exception msg) {
                ShoppingCartUtil.removeAncillaryFromDB(travelerAncillaryUpdate.getAncillary(),
                        bookedTraveler.getAncillaries().getCollection());
                addBag = false;
                LOG.info("A ERROR OCCURS WHEN CREATING AE");
                Warning warning = new Warning();
                warning.setCode(ErrorType.UNKNOWN_CODE.getErrorCode());
                warning.setMsg(ErrorCodeDescriptions.MSG_CODE_UNKNOWN_ERROR + " | " + msg.getMessage());
                warningCollection.addToList(warning);
            }

        } else {

            CurrencyValue currencyValue = CurrencyUtil.getTestCurrencyValue(ancillaryDB.getCurrency().getCurrencyCode());

            if (null == currencyValue) {
                currencyValue = ancillaryDB.getCurrency();
            }

            boolean reservationToApplyPreferredBags = false;

            // Benefit by corporate recognition
            if (null != currencyValue && BigDecimal.ZERO.compareTo(currencyValue.getTotal()) == 0
            && bgAncillaryType.getType().equalsIgnoreCase("0IA") ) {
                reservationToApplyPreferredBags  = true;
                LOG.info("Se va aplicar beneficio corporativo de bags");
            }

            // save remark to benefit preferred bags
            if (null != bookedTraveler.getBenefitCorporateRecognition()
                && bookedTraveler.getBenefitCorporateRecognition().isPreferredBagsBenefit() 
                && reservationToApplyPreferredBags) {

                this.createRemarkByPreferredBags(
                        bookedTraveler.getBenefitCorporateRecognition(),
                        pnr.getPnr(),
                        cartId, 
                        pnr.getLegs().getCollection().get(0).getSegments().getCollection().get(0).getSegment().getSegmentCode()
                        );
            }

            travelerAncillaryUpdate.getAncillary().setCurrency(currencyValue);
            bookedTraveler.getAncillaries().getCollection().add(travelerAncillaryUpdate);
        }

    }

    private PaidTravelerAncillary transformTravelerAncillary(BookedTraveler bookedTraveler) {
        if (null == bookedTraveler.getAncillaries() && bookedTraveler.getAncillaries().getCollection().isEmpty()) {
            return null;
        }

        for (AbstractAncillary abstractAncillary : bookedTraveler.getAncillaries().getCollection()) {
            if (abstractAncillary instanceof TravelerAncillary) {
                TravelerAncillary travelerAncillary = (TravelerAncillary) abstractAncillary;
                PaidTravelerAncillary paidTravelerAncillary = travelerAncillary.transformToPaidAncillary();
                return paidTravelerAncillary;

            }
        }
        return null;
    }

    private List<TravelerAncillary> getTravelerAncillaries(List<BookedTraveler> bookedTravelerList,
            List<TravelerAncillary> travelerAncillariesAdded) {

        for (BookedTraveler bookedTraveler : bookedTravelerList) {
            List<AbstractAncillary> allAncillaries = PurchaseOrderUtil.getAllAncillaries(bookedTraveler);
            int itemUpdateId = 1;
            for (TravelerAncillary travelerAncillary : PurchaseOrderUtil.getNewAncillaries(allAncillaries)) {
                travelerAncillary.setNameNumber(bookedTraveler.getNameRefNumber());
                travelerAncillary.setAncillaryId(String.valueOf(itemUpdateId));
                travelerAncillariesAdded.add(travelerAncillary);
            }
        }

        return travelerAncillariesAdded;

    }

    private List<String> reviewAndbuildRemark(List<BookedTraveler> bookedTravelersRequested) throws Exception {
        List<String> litsRemarks = new ArrayList<>();
        for (BookedTraveler bookedTraveler : bookedTravelersRequested) {
            if (null != bookedTraveler.getAncillaries()) {
                for (AbstractAncillary abstractAncillary : bookedTraveler.getAncillaries().getCollection()) {
                    if (abstractAncillary instanceof TravelerAncillary) {
                        TravelerAncillary travelerAncillary = (TravelerAncillary) abstractAncillary;
                        if (travelerAncillary.isApplyBenefit()) {
                            if (null != travelerAncillary.getBenefitCodeId()) {
                                if ("GOLD".equalsIgnoreCase(travelerAncillary.getBenefitCodeId())
                                        || "PLATINUM".equalsIgnoreCase(travelerAncillary.getBenefitCodeId())
                                        || "TITANIUM".equalsIgnoreCase(travelerAncillary.getBenefitCodeId())
                                        || "ONE".equalsIgnoreCase(travelerAncillary.getBenefitCodeId())) {

                                    final String CODE_CLUB_PREMIER = "TBCPLB";
                                    String remark = buildRemark(CODE_CLUB_PREMIER,
                                            bookedTraveler.getFrequentFlyerNumber(),
                                            travelerAncillary.getBenefitCodeId(), travelerAncillary.getLegCode());
                                    litsRemarks.add(remark);

                                } else {
                                    final String CODE_COBRAND_CARD = "TBCCB";
                                    String remark = buildRemark(CODE_COBRAND_CARD,
                                            bookedTraveler.getFrequentFlyerNumber(),
                                            travelerAncillary.getBenefitCodeId(), travelerAncillary.getLegCode());
                                    litsRemarks.add(remark);

                                }
                            }
                        }
                    }

                }
            }

        }

        return litsRemarks;

    }

    private String buildRemark(String code, String ff, String benefit, String legCode) {
        final String SEPARATOR = "/";
        String remark = code + SEPARATOR + ff + SEPARATOR + benefit + SEPARATOR + legCode.replace("_", "-");
        return remark;

    }

    private boolean getBenefits(BookedTraveler bookedTraveler, TravelerAncillary travelerAncillaryUpdate) {
        int freeTierBag = 0;
        try {
            // Is there a free tier bag benefit?
            freeTierBag = bookedTraveler.getBookedTravellerBenefitCollection().getCollection().get(0).getBag()
                    .getFreeBagAllowance().getChecked();
        } catch (Exception e) {
            // ignore
        }

        boolean freeBag = false;
        int bagAllownace = 0;

        if (freeTierBag == 1) {

            if (bookedTraveler.getFreeBaggageAllowancePerLegsArray() != null
                    && bookedTraveler.getFreeBaggageAllowancePerLegsArray().size() > 0) {
                bagAllownace = bookedTraveler.getFreeBaggageAllowancePerLegsArray().get(0).getFreeBaggageAllowance()
                        .getChecked();
            }

            // if free bag allowance is 0, then 0CC is going to be free
            if (bagAllownace == 0) {
                if ("0CC".equalsIgnoreCase(travelerAncillaryUpdate.getAncillary().getType())) {
                    freeBag = true;
                }
            } else if (bagAllownace == 1) {
                // If the free bag allowance is 1, then 0CD is going to
                // be free
                if ("0CD".equalsIgnoreCase(travelerAncillaryUpdate.getAncillary().getType())) {
                    freeBag = true;
                }
            }
        }

        travelerAncillaryUpdate.setQuantity(1);
        LOG.info("Traveller: {} ", bookedTraveler);
        LOG.info("TIER BAG: freeBag = {}, freeTierBag = {}, bagAllownace = {}", freeBag, freeTierBag, bagAllownace);
        return freeBag;
    }

    public AncillaryOfferCollection getAncillariesCollection(String cartId) throws Exception, GenericException {

        // read the ancillariesCollectionWrapper from the database
        AncillariesCollectionWrapper ancillariesCollectionWrapper = pnrLookupDao.readAncillariesCollectionByCartId(cartId);
        if (null == ancillariesCollectionWrapper) {
            LOG.error(ErrorType.CART_COLLECTION_OBJECT_EMPTY.getFullDescription() + ": ancillariesCollectionWrapper!");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNABLE_TO_RETRIEVE_PRICE_FOR_ANCILLARY,
                    ErrorType.UNABLE_TO_RETRIEVE_PRICE_FOR_ANCILLARY.getFullDescription() + "CartId - " + cartId);
        }

        AncillaryOfferCollection ancillaryOfferCollection = ancillariesCollectionWrapper.getAncillaryOfferCollection();
        if (null == ancillaryOfferCollection) {
            LOG.error(ErrorType.CART_COLLECTION_OBJECT_EMPTY.getFullDescription() + ": ancillaryOfferCollection!");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNABLE_TO_RETRIEVE_PRICE_FOR_ANCILLARY,
                    ErrorType.UNABLE_TO_RETRIEVE_PRICE_FOR_ANCILLARY.getFullDescription() + "CartId - " + cartId);
        }
        return ancillaryOfferCollection;
    }

    /**
     *
     * @param bookedTraveler
     * @param pnrCollection
     * @param cartId
     * @param abstractAncillaryUpdate
     * @throws Exception
     */
    private void addOrRemoveUpgrade(BookedTraveler bookedTraveler, PNRCollection pnrCollection, String cartId,
            AbstractAncillary abstractAncillaryUpdate) throws Exception {
        // Upgrade
        CabinUpgradeAncillary cabinUpgradeAncillary = (CabinUpgradeAncillary) abstractAncillaryUpdate;
        AbstractAncillary abstractAncillary = ShoppingCartUtil.getSameUpgrade(cabinUpgradeAncillary,
                bookedTraveler.getAncillaries().getCollection());

        if (null == abstractAncillary && cabinUpgradeAncillary.getQuantity() > 0) {
            addUpgrade(bookedTraveler, cabinUpgradeAncillary, pnrCollection, cartId);
        } else if (cabinUpgradeAncillary.getQuantity() == 0) {
            ShoppingCartUtil.removeCabinUpgradeAncillaryInDB(bookedTraveler, cabinUpgradeAncillary.getSegmentCode());
        }
    }

    /**
     * @param abstractAncillaryUpdate
     * @param ancillaryOfferCollectionOnDB
     * @param bookedTraveler
     * @param bookedTravelerUpdate
     * @param cartId
     * @param store
     * @throws Exception
     * @throws GenericException
     *
     */
    private void addOrRemoveExtraWeight(AbstractAncillary abstractAncillaryUpdate, BookedTraveler bookedTraveler, AncillaryOfferCollection ancillaryOfferCollectionOnDB, String cartId, BookedTraveler bookedTravelerUpdate, String store, PNRCollection pnrCollection) throws GenericException, Exception {

        ExtraWeightAncillary extraWeightAncillary = (ExtraWeightAncillary) abstractAncillaryUpdate;
        AncillaryPostBookingType bgAncillaryType = AncillaryPostBookingType.getBgType(extraWeightAncillary.getAncillary().getType());

        if (null != bgAncillaryType && AncillaryGroupType.BG.toString().equalsIgnoreCase(bgAncillaryType.getGroup())) {

            boolean isPartOfReservation = ShoppingCartUtil.isPartOfReservation(extraWeightAncillary.getAncillary(), bookedTraveler.getAncillaries().getCollection());
            AbstractAncillary abstractAncillaryDB = ShoppingCartUtil.getSameExtraWeightAncillary( extraWeightAncillary.getAncillary(), bookedTraveler.getAncillaries().getCollection() );

            if (isPartOfReservation && null != abstractAncillaryDB) {

                // if it is an unpaid ancillary
                if (abstractAncillaryDB instanceof ExtraWeightAncillary) {

                    ExtraWeightAncillary extraWeightAncillarydb = (ExtraWeightAncillary) abstractAncillaryDB;

                    if (extraWeightAncillary.getQuantity() != extraWeightAncillarydb.getQuantity()) {

                        ShoppingCartUtil.removeExtraWeightAncillaryFromDB(extraWeightAncillary.getAncillary(), bookedTraveler.getAncillaries().getCollection());

                        if (null == ancillaryOfferCollectionOnDB) {
                            try {
                                ancillaryOfferCollectionOnDB = getAncillariesCollection(cartId);
                            } catch (MongoException ex) {
                                ancillaryOfferCollectionOnDB = null;
                            }
                        }

                        // read from DB
                        Ancillary ancillaryDB = AncillaryUtil.getAncillaryFromAncillaryOfferCollection( extraWeightAncillary.getAncillary().getType(), ancillaryOfferCollectionOnDB );

                        if (null == ancillaryDB) {
                            // Throw exception
                            LOG.error(ErrorType.UNABLE_TO_RETRIEVE_PRICE_FOR_ANCILLARY.getFullDescription()
                                    + " | ancillary: " + extraWeightAncillary.getAncillary().getType());

                            throw new GenericException(Response.Status.BAD_REQUEST,
                                    ErrorType.UNABLE_TO_RETRIEVE_PRICE_FOR_ANCILLARY,
                                    ErrorType.UNABLE_TO_RETRIEVE_PRICE_FOR_ANCILLARY.getFullDescription()
                                    + " | ancillary: " + extraWeightAncillary.getAncillary().getType());
                        }

                        boolean isDomestic = isDomestic(pnrCollection, cartId);
                        getExtraWeightAncillary(
                                bookedTraveler,
                                bookedTravelerUpdate,
                                store,
                                extraWeightAncillary,
                                bgAncillaryType,
                                ancillaryDB,
                                isDomestic
                        );

                        bookedTraveler.getAncillaries().getCollection().add(extraWeightAncillary);
                    }

                }

            } else if (null == abstractAncillaryDB && extraWeightAncillary.getQuantity() >= 1) { // new
                // ancillary){
                if (null == ancillaryOfferCollectionOnDB) {
                    try {
                        ancillaryOfferCollectionOnDB = getAncillariesCollection(cartId);
                    } catch (MongoException ex) {
                        ancillaryOfferCollectionOnDB = null;
                    }
                }

                // read from DB
                Ancillary ancillaryDB = AncillaryUtil.getAncillaryFromAncillaryOfferCollection(
                        extraWeightAncillary.getAncillary().getType(),
                        ancillaryOfferCollectionOnDB
                );

                if (null == ancillaryDB) {
                    // Throw exception
                    LOG.error(
                            ErrorType.UNABLE_TO_RETRIEVE_PRICE_FOR_ANCILLARY.getFullDescription()
                            + " | ancillary: "
                            + extraWeightAncillary.getAncillary().getType()
                    );

                    throw new GenericException(
                            Response.Status.BAD_REQUEST,
                            ErrorType.UNABLE_TO_RETRIEVE_PRICE_FOR_ANCILLARY,
                            ErrorType.UNABLE_TO_RETRIEVE_PRICE_FOR_ANCILLARY.getFullDescription()
                            + " | ancillary: "
                            + extraWeightAncillary.getAncillary().getType()
                    );
                }

                boolean isDomestic = isDomestic(pnrCollection, cartId);
                getExtraWeightAncillary(
                        bookedTraveler,
                        bookedTravelerUpdate,
                        store,
                        extraWeightAncillary,
                        bgAncillaryType,
                        ancillaryDB,
                        isDomestic
                );

                bookedTraveler.getAncillaries().getCollection().add(extraWeightAncillary);

            } else if (0 == extraWeightAncillary.getQuantity()) {
                if (null != abstractAncillaryDB) {
                    ShoppingCartUtil.removeExtraWeightAncillaryFromDB(
                            extraWeightAncillary.getAncillary(),
                            bookedTraveler.getAncillaries().getCollection()
                    );
                }
            }
        }

    }

    /***
     * This Function return true
     * @return
     */
    private boolean isDomestic(PNRCollection pnrCollection, String cartId) {

    	if ( pnrCollection == null ) {
    		return false;
    	}

    	if ( cartId == null ) {
    		return false;
    	}

    	PNR pnr = null;
    	try {
    		pnr = pnrCollection.getPNRByCartId(cartId);
    	} catch(Exception ex) {
    		return false;
    	}

    	if ( pnr == null) {
    		return false;
    	}

    	BookedLeg bookedLeg = pnr.getBookedLegByCartId(cartId);

    	for ( BookedSegment segment: bookedLeg.getSegments().getCollection() ) {

    		// If any of the segment not a domestic flight
    		if ( ! segment.getSegment().isDomestic() ) {
    			return false;
    		}
    	}

    	return true;
    }

    /**
     *
     * @param bookedTraveler
     * @param bookedTravelerUpdate
     * @param store
     * @param extraWeightAncillary
     * @param bgAncillaryType
     * @param ancillaryDB
     * @throws Exception
     */
    private void getExtraWeightAncillary(
            BookedTraveler bookedTraveler,
            BookedTraveler bookedTravelerUpdate,
            String store,
            ExtraWeightAncillary extraWeightAncillary,
            AncillaryPostBookingType bgAncillaryType,
            Ancillary ancillaryDB,
            boolean isDomestic
    ) throws Exception {

        extraWeightAncillary.getAncillary().setGroupCode(bgAncillaryType.getGroup());
        extraWeightAncillary.getAncillary().setVendor(bgAncillaryType.getVendor());
        extraWeightAncillary.getAncillary().setSsrCode(bgAncillaryType.getSsr());
        extraWeightAncillary.getAncillary().setRficSubcode(bgAncillaryType.getRficSubCode());
        extraWeightAncillary.getAncillary().setRficCode(bgAncillaryType.getRficCode());
        extraWeightAncillary.getAncillary().setCommercialName(bgAncillaryType.getCommercialName());
        extraWeightAncillary.getAncillary().setSegmentIndicator(SegmentIndicator.P.toString());
        extraWeightAncillary.getAncillary().setEmdType(bgAncillaryType.getEmdType());
        extraWeightAncillary.getAncillary().setOwningCarrierCode(bgAncillaryType.getOwningCarrierCode());
        extraWeightAncillary.setNameNumber(bookedTravelerUpdate.getNameRefNumber());

        FreeBaggageAllowancePerLeg freeBaggageAllowancePerLeg = ShoppingCartUtil.getFreeBaggageAllowancePerLeg(
                bookedTraveler, extraWeightAncillary.getLegCode()
        );

        if (null == freeBaggageAllowancePerLeg) {
            // Throw exception
            LOG.error(ErrorType.CART_COLLECTION_OBJECT_EMPTY.getFullDescription() + ": freeBaggageAllowancePerLeg!");
            throw new Exception();
        }

        CheckedWeight checkedWeight;
        if (StoreFrontType.MX.toString().equalsIgnoreCase(store)) {
            checkedWeight = freeBaggageAllowancePerLeg.getFreeBaggageAllowance().getByUnit(WeightUnitType.KG.toString());
        } else {
            checkedWeight = freeBaggageAllowancePerLeg.getFreeBaggageAllowance().getByUnit(WeightUnitType.LB.toString());
        }

        extraWeightAncillary.setWeightUnit(checkedWeight.getCheckedWeightUnit());
        extraWeightAncillary.setExtraWeight(extraWeightAncillary.getTotalWeight() - checkedWeight.getCheckedFreeWeight());
        extraWeightAncillary.getAncillary().setPerUnit(ancillaryDB.isPerUnit());

        if ( ! ancillaryDB.isPerUnit() ) {
        	extraWeightAncillary.setQuantity(1);
        }

        // maybe we have to validate if currency in the
        // request has not been modified
        CurrencyValue currencyValue = CurrencyUtil.getTestCurrencyValue(ancillaryDB.getCurrency().getCurrencyCode());

        if (null == currencyValue) {
        	currencyValue = ancillaryDB.getCurrency();
        }

        
        // Override values starts:
		currencyValue.setTotalTaxRefundable( BigDecimal.valueOf( 0 ) );
		currencyValue.setTotalTax( BigDecimal.valueOf( 0 ) );
		
		currencyValue.getTaxDetails().clear();

		if (isDomestic) {

        	if ( "0C6".equalsIgnoreCase( extraWeightAncillary.getAncillary().getType() ) ) {
        		currencyValue.setBase( domesticOverrideValue_0C6 );
        		currencyValue.setTotal( domesticOverrideValue_0C6 );

        	} else {
        		currencyValue.setBase( domesticOverrideValue );
        		currencyValue.setTotal( domesticOverrideValue );
        	}

        } else {
        	if ( "0C6".equalsIgnoreCase( extraWeightAncillary.getAncillary().getType() ) ) {
        		currencyValue.setBase( internationalOverrideValue_0C6 );
        		currencyValue.setTotal( internationalOverrideValue_0C6  );
        	} else {
        		currencyValue.setBase( internationalOverrideValue );
        		currencyValue.setTotal( internationalOverrideValue );
        	}
        }
        // Override value ends
        
        extraWeightAncillary.getAncillary().setCurrency(currencyValue);
    }

    /**
     *
     * @param bookedTraveler
     * @param cabinUpgradeAncillary
     * @param pnrCollection
     * @param cartId
     * @throws Exception
     */
    private void addUpgrade(BookedTraveler bookedTraveler, CabinUpgradeAncillary cabinUpgradeAncillary,
            PNRCollection pnrCollection, String cartId) throws Exception {

        AncillaryOffer ancillaryOfferDB;
        ancillaryOfferDB = ShoppingCartUtil.readAncillaryOfferOfPNRCollection(pnrCollection.getPNRByCartId(cartId),
                cartId, cabinUpgradeAncillary);

        TravelerAncillary travelerAncillary = null;

        if (null != ancillaryOfferDB) {
            travelerAncillary = PurchaseOrderUtil.createUpgradeAncillary(bookedTraveler, cabinUpgradeAncillary,
                    ancillaryOfferDB);
        }
        // update info
        if (null != travelerAncillary) {
            cabinUpgradeAncillary.setCurrency(travelerAncillary.getAncillary().getCurrency());
            cabinUpgradeAncillary.setActionCode(travelerAncillary.getActionCode());
            cabinUpgradeAncillary.setAncillaryId(travelerAncillary.getAncillaryId());
            cabinUpgradeAncillary.setLinkedID(travelerAncillary.getLinkedID());
            cabinUpgradeAncillary.setIsPartOfReservation(travelerAncillary.isPartOfReservation());
            cabinUpgradeAncillary.setNameNumber(travelerAncillary.getNameNumber());
            cabinUpgradeAncillary.setSegmentCodeAux(cabinUpgradeAncillary.getSegmentCode());
            cabinUpgradeAncillary.setName(travelerAncillary.getAncillary().getCommercialName());
            cabinUpgradeAncillary.setType(travelerAncillary.getAncillary().getType());

            cabinUpgradeAncillary.setAncillary(travelerAncillary.getAncillary());

            bookedTraveler.getAncillaries().getCollection().add(cabinUpgradeAncillary);
        }
    }

    public void addUpgradeChecked(BookedTraveler bookedTraveler, BookedTraveler bookedTravelerUpdate,
            PNRCollection pnrCollection, String cartId) throws Exception {

        // For Ancillaries
        if (null != bookedTravelerUpdate.getAncillaries()
                && null != bookedTravelerUpdate.getAncillaries().getCollection()) {

            if (null == bookedTraveler.getAncillaries() || bookedTraveler.getAncillaries().getCollection().isEmpty()
                    || (!ShoppingCartUtil.compareAncillaryCollections(bookedTraveler.getAncillaries().getCollection(),
                            bookedTravelerUpdate.getAncillaries().getCollection()))) {

                for (AbstractAncillary abstractAncillaryUpdate : bookedTravelerUpdate.getAncillaries()
                        .getCollection()) {

                    if (abstractAncillaryUpdate instanceof CabinUpgradeAncillary) {

                        addOrRemoveUpgrade(bookedTraveler, pnrCollection, cartId, abstractAncillaryUpdate);
                    }
                }
            }
        }
    }

    void removeUpgrade(BookedTraveler bookedTraveler) {

        // For Ancillaries
        if (bookedTraveler.getAncillaries() == null || bookedTraveler.getAncillaries().getCollection() == null
                || bookedTraveler.getAncillaries().getCollection().isEmpty()) {
            return;
        }

        /*
		 * This is supposed to be the list of un-paid ancillaries
         */
        List<AbstractAncillary> toRemove = new ArrayList<AbstractAncillary>();
        for (AbstractAncillary abstractAncillaryUpdate : bookedTraveler.getAncillaries().getCollection()) {
            if (abstractAncillaryUpdate instanceof CabinUpgradeAncillary) {
                toRemove.add(abstractAncillaryUpdate);
            }
        }

        if (toRemove.size() > 0) {
            bookedTraveler.getAncillaries().getCollection().removeAll(toRemove);
        }

    }

    /**
     * Add remark in reservation by preferred Bags benefit.
     * p.e. 1STBAG 5000000000967 10B 24062020
     */
    private void createRemarkByPreferredBags(
            BenefitCorporateRecognition benefitCorporateRecognition,
            String pnr,
            String cartId,
            String segmentCode)
    {
        try {
            if (null != benefitCorporateRecognition) {
                String remarkText = "3OSI YY 1STBAG BY CORPORATE PRIORITY"
                        .concat(" ")
                        .concat(null != benefitCorporateRecognition.getClid() ? benefitCorporateRecognition.getClid() : "")
                        .concat(" ")
                        .concat(null != segmentCode ? segmentCode.substring(0,7).replace("_", "") : "") //MEX_CUN_
                        .concat(" ");
                        //.concat(null != benefitCorporateRecognition.getSeatCode() ? benefitCorporateRecognition.getSeatCode() : "")
                        //.concat(" ");

                LocalDate today = LocalDate.now();
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("ddMMyyyy");
                remarkText = remarkText.concat(today.format(formatter));
                try {
                    CeateRemarkOSI(remarkText,pnr,cartId);
                } catch (SabreLayerUnavailableException ex) {
                    LOG.error(ex.getMessage(), ex);
                } catch (Exception ex) {
                    LOG.error("Update Reservation: Error adding remark preferred Bags {}_cardId_{}_message{} ", pnr , cartId,
                            ex.getMessage()
                    );
                }
            }
        } catch (Exception e) {
            LOG.error("Update Reservation: Error adding remark preferred Bags {}_cardId_{}_message{} ", pnr , cartId,
                            e.getMessage()
                    );
        }
    }
    
    /**
     * Add login CP information
     * @param actualBookedTraveler
     * @param updateBookedTraveler 
     */
    
    private void CeateRemarkOSI(String command, String pnr, String cartId) throws Exception {
        try {
            
        	SabreSession sabreSession = sabreSessionPoolManager.acquireSession();
            String token = sabreSession.getSessionId();
            
            getReservationService.getReservationStateful(
                  sabreSession.getSessionId(), pnr, cartId);

            SabreCommandLLSRS sabreCommandLLSRS = sabreCommandService.sabreCommand(token, command, "SCREEN");
            LOG.info(pnr, cartId, "Command Response: " + command, new Gson().toJson(sabreCommandLLSRS), false);
            if (sabreCommandLLSRS.getResponse().contains("*")) {
                command = "6MANAGE_API";
                sabreCommandLLSRS = sabreCommandService.sabreCommand(token, command, "SCREEN");
                LOG.info(pnr, cartId, "Command Response: " + command, new Gson().toJson(sabreCommandLLSRS), false);

                command = "E";
                sabreCommandLLSRS = sabreCommandService.sabreCommand(token, command, "SCREEN");
                LOG.info(pnr, cartId, "Command Response: " + command, new Gson().toJson(sabreCommandLLSRS), false);

                if (!sabreCommandLLSRS.getResponse().trim().startsWith("OK")) {
                    command = "I";
                    sabreCommandService.sabreCommand(token, command, "SCREEN");
                    LOG.info(pnr, cartId, "Command Response: " + command, new Gson().toJson(sabreCommandLLSRS), true);
                }
            } else {
                command = "I";
                sabreCommandService.sabreCommand(token, command, "SCREEN");
                LOG.info(pnr, cartId, "Command Response: " + command, new Gson().toJson(sabreCommandLLSRS), true);
            }

            sabreSessionPoolManager.closeSession(sabreSession);
        } catch (Exception ex) {
            LOG.error("Remove Voucher, Error executing command to remove remark emd_pnr{}cartId{}ex{}", pnr, cartId, ex);
            sabreCommandService.sabreCommand("I", "SCREEN");
        }
    }

    public void addFreeBagByBenefitAncillary(BookedTraveler bookedTraveler, BookedTraveler bookedTravelerUpdate,
            PNRCollection pnrCollection, String cartId, String store, String pos,
            AncillaryOfferCollection ancillaryOfferCollectionOnDB) {

    }

    public IPNRLookupDao getPnrLookupDao() {
        return pnrLookupDao;
    }

    public AMUpdateReservationService getAmUpdateReservationService() {
        return amUpdateReservationService;
    }

    public void setPnrLookupDao(IPNRLookupDao pnrLookupDao) {
        this.pnrLookupDao = pnrLookupDao;
    }

    public void setAmUpdateReservationService(AMUpdateReservationService amUpdateReservationService) {
        this.amUpdateReservationService = amUpdateReservationService;
    }

}
