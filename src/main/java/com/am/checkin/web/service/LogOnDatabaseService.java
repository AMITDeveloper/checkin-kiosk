package com.am.checkin.web.service;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.exception.model.MybException;
import com.aeromexico.commons.loggingutils.LogUtil;
import com.aeromexico.commons.loggingutils.PingLog;
import com.aeromexico.commons.model.Ping;
import com.aeromexico.commons.model.SystemPropertiesFactory;
import com.aeromexico.commons.model.rq.AncillaryRQ;
import com.aeromexico.commons.model.rq.BoardingPassesRQ;
import com.aeromexico.commons.model.rq.CheckInRQ;
import com.aeromexico.commons.model.rq.IssueBagRQ;
import com.aeromexico.commons.model.rq.PassengersListRQ;
import com.aeromexico.commons.model.rq.PingRQ;
import com.aeromexico.commons.model.rq.PinpadAuthRQ;
import com.aeromexico.commons.model.rq.PnrRQ;
import com.aeromexico.commons.model.rq.PurchaseOrderRQ;
import com.aeromexico.commons.model.rq.SeatmapCheckInRQ;
import com.aeromexico.commons.model.rq.ShoppingCartRQ;
import com.aeromexico.commons.web.types.PosType;
import com.aeromexico.commons.web.util.Constants;
import com.aeromexico.commons.web.util.MaskConfidentialDataUtil;
import com.aeromexico.dao.async.SimpleAsync;
import com.aeromexico.dao.client.exception.MongoDisconnectException;
import com.aeromexico.dao.event.DocumentEvent;
import com.aeromexico.dao.services.CommonAsyncLogOnDatabaseService;
import com.aeromexico.dao.services.IPNRLookupDao;
import com.am.checkin.web.config.Version;
import com.am.checkin.web.v2.util.SystemVariablesUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.sabre.services.acs.bso.checkinpassenger.v3.ACSCheckInPassengerRQACS;
import com.sabre.services.acs.bso.checkinpassenger.v3.ACSCheckInPassengerRSACS;
import com.sabre.services.micellaneous.CollectMiscFeeRQ;
import com.sabre.services.micellaneous.CollectMiscFeeRS;
import com.sabre.webservices.pnrbuilder.updatereservation.UpdateReservationRQ;
import com.sabre.webservices.pnrbuilder.updatereservation.UpdateReservationRS;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;

import org.aeromexico.commons.exception.config.ReadProperties;
import org.opentravel.ota.payments.PaymentRQ;
import org.opentravel.ota.payments.PaymentRS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adrian
 */
@Named
@ApplicationScoped
public class LogOnDatabaseService {

    private static final Logger LOG = LoggerFactory.getLogger(LogOnDatabaseService.class);
    private static final Gson GSON = new GsonBuilder()
            //.serializeNulls()
            //.excludeFieldsWithoutExposeAnnotation()
            .create();
    private static final String ERROR_CONVERTING_TO_JSON = "ERROR CONVERTING OBJECT TO JSON";

    @Inject
    private IPNRLookupDao pnrLookupDao;

    @Inject
    private CommonAsyncLogOnDatabaseService commonAsyncLogOnDatabaseService;

    @Inject
    private SystemPropertiesFactory systemPropertiesFactory;

    @Inject
    private ReadProperties prop;

    private String hostname = "";

    private String apiVersion = "";

    @PostConstruct
    public void init() {
        if (null == systemPropertiesFactory) {
            systemPropertiesFactory = new SystemPropertiesFactory();
        }

        try {
            hostname = systemPropertiesFactory.getInstance().getHostname();
        } catch (Exception ex) {
            //
        }

        try {
            apiVersion = Version.getVersion();
        } catch (Exception ex) {
            //
        }
    }

    /**
     *
     * @param pinpadAuthRQ
     */
    public void logPinpadAuth(PinpadAuthRQ pinpadAuthRQ) {
        try {
            JsonElement jsonElementRS;
            try {
//                other way
//                JsonParser parser = new JsonParser();
//                JsonObject o = parser.parse(pinpadAuthRQ.getRequest()).getAsJsonObject();
//                jsonElementRS = GSON.toJsonTree(o, JsonObject.class);

                jsonElementRS = GSON.fromJson(pinpadAuthRQ.getRequest(), JsonElement.class);
            } catch (Exception ex) {
                jsonElementRS = getError(ex);
            }

            commonAsyncLogOnDatabaseService.logSuccessOnDBAsync(
                    pinpadAuthRQ, pinpadAuthRQ.getCartId(), pinpadAuthRQ.getRecordLocator(),
                    null, jsonElementRS, Constants.PINPAD_AUTH_LOG_COLLECTION, apiVersion
            );
        }catch (MongoDisconnectException me){
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(pinpadAuthRQ.getPos())) {
                LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
            } else {
                LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
        }
    }

    /**
     * @param pnrRQ
     */
    public void logPnrOnSuccess(PnrRQ pnrRQ) {
        if (SystemVariablesUtil.isLoggingEnabled()) {
            try {
                if (PosType.KIOSK.toString().equalsIgnoreCase(pnrRQ.getPos())) {
                    commonAsyncLogOnDatabaseService.logSuccessOnDBAsync(
                            pnrRQ, pnrRQ.getCartId(), pnrRQ.getRecordLocator(),
                            null, null, Constants.PNR_LOOKUP_LOG_COLLECTION, apiVersion
                    );
                }
            } catch (MongoDisconnectException me) {
                if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(pnrRQ.getPos())) {
                    LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
                } else {
                    LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
                }
            } catch (Exception ex) {
                LOG.error(ex.getMessage());
            }
        }
    }

    /**
     *
     * @param pnrRQ
     * @param thr
     */
    public void logPnrOnError(PnrRQ pnrRQ, Throwable thr) {
        if (SystemVariablesUtil.isLoggingEnabled()) {
            try {
                if (PosType.KIOSK.toString().equalsIgnoreCase(pnrRQ.getPos())) {
                    String errorCode = null;
                    String errorMsg = null;

                    if (thr instanceof GenericException) {
                        GenericException ex = (GenericException) thr;
                        errorCode = ex.getErrorCodeType().getErrorCode();
                        errorMsg = ex.getErrorCodeType().getFullDescription();
                    } else if (thr instanceof MybException) {
                        MybException ex = (MybException) thr;
                        errorCode = ex.getErrorCode();
                        errorMsg = ex.getMsg();
                    } else if (thr instanceof Exception) {
                        Exception ex = (Exception) thr;
                        errorMsg = ex.getMessage();
                    }

                    commonAsyncLogOnDatabaseService.logErrorDBAsync(
                            pnrRQ, pnrRQ.getCartId(), pnrRQ.getRecordLocator(),
                            null, null, Constants.PNR_LOOKUP_LOG_COLLECTION, thr, errorCode, errorMsg, apiVersion
                    );
                }
            } catch (MongoDisconnectException me) {
                if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(pnrRQ.getPos())) {
                    LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
                } else {
                    LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
                }
            } catch (Exception ex) {
                LOG.error(ex.getMessage());
            }
        }
    }

    /**
     *
     * @param shoppingCartRQ
     */
    public void logShoppingCartOnSuccess(ShoppingCartRQ shoppingCartRQ) {
        if (SystemVariablesUtil.isLoggingEnabled()) {
            try {
                if (PosType.KIOSK.toString().equalsIgnoreCase(shoppingCartRQ.getPos())) {
                    commonAsyncLogOnDatabaseService.logSuccessOnDBAsync(
                            shoppingCartRQ, shoppingCartRQ.getCartId(), shoppingCartRQ.getRecordLocator(),
                            null, null, Constants.SHOPPING_CART_LOG_COLLECTION, apiVersion
                    );
                }
            } catch (MongoDisconnectException me) {
                if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(shoppingCartRQ.getPos())) {
                    LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
                } else {
                    LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
                }
                throw me;
            } catch (Exception ex) {
                LOG.error(ex.getMessage());
            }
        }
    }

    /**
     *
     * @param shoppingCartRQ
     * @param thr
     */
    public void logShoppingCartOnError(ShoppingCartRQ shoppingCartRQ, Throwable thr) {
        if (SystemVariablesUtil.isLoggingEnabled()) {
            try {
                if (PosType.KIOSK.toString().equalsIgnoreCase(shoppingCartRQ.getPos())) {
                    String errorCode = null;
                    String errorMsg = null;

                    if (thr instanceof GenericException) {
                        GenericException ex = (GenericException) thr;
                        errorCode = ex.getErrorCodeType().getErrorCode();
                        errorMsg = ex.getErrorCodeType().getFullDescription();
                    } else if (thr instanceof MybException) {
                        MybException ex = (MybException) thr;
                        errorCode = ex.getErrorCode();
                        errorMsg = ex.getMsg();
                    } else if (thr instanceof Exception) {
                        Exception ex = (Exception) thr;
                        errorMsg = ex.getMessage();
                    }


                    commonAsyncLogOnDatabaseService.logErrorDBAsync(
                            shoppingCartRQ, shoppingCartRQ.getCartId(), shoppingCartRQ.getRecordLocator(),
                            null, null, Constants.SHOPPING_CART_LOG_COLLECTION, thr, errorCode, errorMsg, apiVersion
                    );
                }
            } catch (MongoDisconnectException me) {
                if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(shoppingCartRQ.getPos())) {
                    LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
                } else {
                    LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
                }
            } catch (Exception ex) {
                LOG.error(ex.getMessage());
            }
        }
    }

    /**
     *
     * @param boardingPassesRQ
     */
    public void logBoardingPassOnSuccess(BoardingPassesRQ boardingPassesRQ) {
        if (SystemVariablesUtil.isLoggingEnabled()) {
            try {
                if (PosType.KIOSK.toString().equalsIgnoreCase(boardingPassesRQ.getPos())) {
                    commonAsyncLogOnDatabaseService.logSuccessOnDBAsync(
                            boardingPassesRQ, boardingPassesRQ.getCartId(), boardingPassesRQ.getRecordLocator(),
                            null, null, Constants.BOARDING_PASS_LOG_COLLECTION, apiVersion
                    );
                }
            } catch (MongoDisconnectException me) {
                if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(boardingPassesRQ.getPos())) {
                    LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
                } else {
                    LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
                }
            } catch (Exception ex) {
                LOG.error(ex.getMessage());
            }
        }
    }

    /**
     *
     * @param boardingPassesRQ
     * @param thr
     */
    public void logBoardingPassOnError(BoardingPassesRQ boardingPassesRQ, Throwable thr) {
        if (SystemVariablesUtil.isLoggingEnabled()) {
            try {
                if (PosType.KIOSK.toString().equalsIgnoreCase(boardingPassesRQ.getPos())) {
                    String errorCode = null;
                    String errorMsg = null;

                    if (thr instanceof GenericException) {
                        GenericException ex = (GenericException) thr;
                        errorCode = ex.getErrorCodeType().getErrorCode();
                        errorMsg = ex.getErrorCodeType().getFullDescription();
                    } else if (thr instanceof MybException) {
                        MybException ex = (MybException) thr;
                        errorCode = ex.getErrorCode();
                        errorMsg = ex.getMsg();
                    } else if (thr instanceof Exception) {
                        Exception ex = (Exception) thr;
                        errorMsg = ex.getMessage();
                    }

                    commonAsyncLogOnDatabaseService.logErrorDBAsync(
                            boardingPassesRQ, boardingPassesRQ.getCartId(), boardingPassesRQ.getRecordLocator(),
                            null, null, Constants.BOARDING_PASS_LOG_COLLECTION, thr, errorCode, errorMsg, apiVersion
                    );
                }
            } catch (MongoDisconnectException me) {
                if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(boardingPassesRQ.getPos())) {
                    LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
                } else {
                    LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
                }
            } catch (Exception ex) {
                LOG.error(ex.getMessage());
            }
        }
    }

    /**
     *
     * @param checkInRQ
     * @param cartId
     * @param recordLocator
     */
    public void logCheckinOnSuccess(
            CheckInRQ checkInRQ,
            String cartId,
            String recordLocator
    ) {
        logCheckinOnSuccess(checkInRQ, cartId, recordLocator, null, null);
    }

    /**
     *
     * @param checkInRQ
     * @param cartId
     * @param recordLocator
     * @param checkInPassengerRQ
     * @param checkInPassengerRS
     */
    public void logCheckinOnSuccess(
            CheckInRQ checkInRQ,
            String cartId,
            String recordLocator,
            ACSCheckInPassengerRQACS checkInPassengerRQ,
            ACSCheckInPassengerRSACS checkInPassengerRS
    ) {
        if (SystemVariablesUtil.isLoggingEnabled()) {
            try {
                if (PosType.KIOSK.toString().equalsIgnoreCase(checkInRQ.getPos())) {
                    JsonElement jsonElementRQ;
                    JsonElement jsonElementRS;
                    try {
                        jsonElementRQ = GSON.toJsonTree(checkInPassengerRQ, ACSCheckInPassengerRQACS.class);
                    } catch (Exception ex) {
                        jsonElementRQ = getError(ex);
                    }

                    try {
                        jsonElementRS = GSON.toJsonTree(checkInPassengerRS, ACSCheckInPassengerRSACS.class);
                    } catch (Exception ex) {
                        jsonElementRS = getError(ex);
                    }

                    commonAsyncLogOnDatabaseService.logSuccessOnDBAsync(
                            checkInRQ, cartId, recordLocator,
                            jsonElementRQ, jsonElementRS, Constants.CHECKIN_LOG_COLLECTION, apiVersion
                    );
                }
            } catch (MongoDisconnectException me) {
                if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(checkInRQ.getPos())) {
                    LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
                } else {
                    LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
                }
            } catch (Exception ex) {
                LOG.error(ex.getMessage());
            }
        }
    }

    /**
     *
     * @param checkInRQ
     * @param cartId
     * @param recordLocator
     * @param thr
     */
    public void logPreCheckinOnError(
            CheckInRQ checkInRQ,
            String cartId,
            String recordLocator,
            Throwable thr
    ) {
        if (SystemVariablesUtil.isLoggingEnabled()) {
            try {
                if (PosType.KIOSK.toString().equalsIgnoreCase(checkInRQ.getPos())) {
                    String errorCode = null;
                    String errorMsg = null;

                    if (thr instanceof GenericException) {
                        GenericException ex = (GenericException) thr;
                        errorCode = ex.getErrorCodeType().getErrorCode();
                        errorMsg = ex.getErrorCodeType().getFullDescription();
                    } else if (thr instanceof MybException) {
                        MybException ex = (MybException) thr;
                        errorCode = ex.getErrorCode();
                        errorMsg = ex.getMsg();
                    } else if (thr instanceof Exception) {
                        Exception ex = (Exception) thr;
                        errorMsg = ex.getMessage();
                    }

                    commonAsyncLogOnDatabaseService.logErrorDBAsync(
                            checkInRQ, cartId, recordLocator,
                            null, null, Constants.PRE_CHECKIN_LOG_COLLECTION, thr, errorCode, errorMsg, apiVersion
                    );
                }
            } catch (MongoDisconnectException me) {
                if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(checkInRQ.getPos())) {
                    LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
                } else {
                    LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
                }
            } catch (Exception ex) {
                LOG.error(ex.getMessage());
            }
        }
    }

    /**
     *
     * @param checkInRQ
     * @param cartId
     * @param recordLocator
     * @param thr
     */
    public void logCheckinOnError(
            CheckInRQ checkInRQ,
            String cartId,
            String recordLocator,
            Throwable thr
    ) {
        logCheckinOnError(checkInRQ, cartId, recordLocator, null, null, thr);
    }

    /**
     *
     * @param checkInRQ
     * @param cartId
     * @param recordLocator
     * @param checkInPassengerRQ
     * @param checkInPassengerRS
     * @param thr
     */
    public void logCheckinOnError(
            CheckInRQ checkInRQ,
            String cartId,
            String recordLocator,
            ACSCheckInPassengerRQACS checkInPassengerRQ,
            ACSCheckInPassengerRSACS checkInPassengerRS,
            Throwable thr
    ) {
        if (SystemVariablesUtil.isLoggingEnabled()) {
            try {
                if (PosType.KIOSK.toString().equalsIgnoreCase(checkInRQ.getPos())) {
                    JsonElement jsonElementRQ;
                    JsonElement jsonElementRS;
                    try {
                        jsonElementRQ = GSON.toJsonTree(checkInPassengerRQ, ACSCheckInPassengerRQACS.class);
                    } catch (Exception ex) {
                        jsonElementRQ = getError(ex);
                    }

                    try {
                        jsonElementRS = GSON.toJsonTree(checkInPassengerRS, ACSCheckInPassengerRSACS.class);
                    } catch (Exception ex) {
                        jsonElementRS = getError(ex);
                    }

                    String errorCode = null;
                    String errorMsg = null;

                    if (thr instanceof GenericException) {
                        GenericException ex = (GenericException) thr;
                        errorCode = ex.getErrorCodeType().getErrorCode();
                        errorMsg = ex.getErrorCodeType().getFullDescription();
                    } else if (thr instanceof MybException) {
                        MybException ex = (MybException) thr;
                        errorCode = ex.getErrorCode();
                        errorMsg = ex.getMsg();
                    } else if (thr instanceof Exception) {
                        Exception ex = (Exception) thr;
                        errorMsg = ex.getMessage();
                    }

                    commonAsyncLogOnDatabaseService.logErrorDBAsync(checkInRQ, cartId, recordLocator,
                            jsonElementRQ, jsonElementRS, Constants.CHECKIN_LOG_COLLECTION, thr, errorCode, errorMsg, apiVersion
                    );
                }
            } catch (MongoDisconnectException me) {
                if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(checkInRQ.getPos())) {
                    LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
                } else {
                    LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
                }
            } catch (Exception ex) {
                LOG.error(ex.getMessage());
            }
        }
    }

    /**
     *
     * @param purchaseOrderRQ
     * @param paymentRQ
     * @param paymentRS
     */
    public void logPaymentAuthOnSuccess(
            PurchaseOrderRQ purchaseOrderRQ, PaymentRQ paymentRQ, PaymentRS paymentRS
    ) {
        if (SystemVariablesUtil.isLoggingEnabled()) {
            try {
                if (PosType.KIOSK.toString().equalsIgnoreCase(purchaseOrderRQ.getPos())) {
                    JsonElement jsonElementRQ;
                    JsonElement jsonElementRS;
                    try {
                        jsonElementRQ = GSON.toJsonTree(paymentRQ, PaymentRQ.class);
                        MaskConfidentialDataUtil.maskConfidentialDataOnPaymentRQ(jsonElementRQ.getAsJsonObject());
                    } catch (Exception ex) {
                        jsonElementRQ = getError(ex);
                    }

                    try {
                        jsonElementRS = GSON.toJsonTree(paymentRS, PaymentRS.class);
                        MaskConfidentialDataUtil.maskConfidentialDataOnPaymentRS(jsonElementRS.getAsJsonObject());
                    } catch (Exception ex) {
                        jsonElementRS = getError(ex);
                    }

                    String cartId;
                    try {
                        cartId = purchaseOrderRQ.getPurchaseOrder().getCartId();
                    } catch (Exception ex) {
                        cartId = null;
                    }

                    commonAsyncLogOnDatabaseService.logSuccessOnDBAsync(
                            purchaseOrderRQ, cartId, purchaseOrderRQ.getRecordLocator(),
                            jsonElementRQ, jsonElementRS, Constants.PAYMENT_AUTH_LOG_COLLECTION, apiVersion
                    );
                }
            } catch (MongoDisconnectException me) {
                if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(purchaseOrderRQ.getPos())) {
                    LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
                } else {
                    LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
                }
            } catch (Exception ex) {
                LOG.error(ex.getMessage());
            }
        }
    }

    /**
     *
     * @param purchaseOrderRQ
     * @param paymentRQ
     * @param paymentRS
     * @param thr
     */
    public void logPaymentAuthOnError(
            PurchaseOrderRQ purchaseOrderRQ, PaymentRQ paymentRQ, PaymentRS paymentRS, Throwable thr) {
        if (SystemVariablesUtil.isLoggingEnabled()) {
        try {
            if (PosType.KIOSK.toString().equalsIgnoreCase(purchaseOrderRQ.getPos())) {
                    JsonElement jsonElementRQ;
                    JsonElement jsonElementRS;
                    try {
                        jsonElementRQ = GSON.toJsonTree(paymentRQ, PaymentRQ.class);
                        MaskConfidentialDataUtil.maskConfidentialDataOnPaymentRQ(jsonElementRQ.getAsJsonObject());
                    } catch (Exception ex) {
                        jsonElementRQ = getError(ex);
                    }

                    try {
                        jsonElementRS = GSON.toJsonTree(paymentRS, PaymentRS.class);
                        MaskConfidentialDataUtil.maskConfidentialDataOnPaymentRS(jsonElementRS.getAsJsonObject());
                    } catch (Exception ex) {
                        jsonElementRS = getError(ex);
                    }

                    String cartId;
                    try {
                        cartId = purchaseOrderRQ.getPurchaseOrder().getCartId();
                    } catch (Exception ex) {
                        cartId = null;
                    }

                    String errorCode = null;
                    String errorMsg = null;

                    if (thr instanceof GenericException) {
                        GenericException ex = (GenericException) thr;
                        errorCode = ex.getErrorCodeType().getErrorCode();
                        errorMsg = ex.getErrorCodeType().getFullDescription();
                    } else if (thr instanceof MybException) {
                        MybException ex = (MybException) thr;
                        errorCode = ex.getErrorCode();
                        errorMsg = ex.getMsg();
                    } else if (thr instanceof Exception) {
                        Exception ex = (Exception) thr;
                        errorMsg = ex.getMessage();
                    }

                    commonAsyncLogOnDatabaseService.logErrorDBAsync(
                            purchaseOrderRQ, cartId, purchaseOrderRQ.getRecordLocator(),
                            jsonElementRQ, jsonElementRS, Constants.PAYMENT_AUTH_LOG_COLLECTION, thr, errorCode, errorMsg, apiVersion
                    );
                }
            }catch(MongoDisconnectException me){
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(purchaseOrderRQ.getPos())) {
                LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
            } else {
                LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
            }
            }  catch(Exception ex){
                LOG.error(ex.getMessage());
            }
        }
    }

    /**
     *
     * @param checkInRQ
     * @param updateReservationRQ
     * @param updateReservationRS
     * @param recordLocator
     * @param cartId
     */
    public void logAEOnSuccess(
            CheckInRQ checkInRQ,
            UpdateReservationRQ updateReservationRQ,
            UpdateReservationRS updateReservationRS,
            String recordLocator,
            String cartId
    ) {
        try {

            JsonElement jsonElementRQ;
            JsonElement jsonElementRS;
            try {
                jsonElementRQ = GSON.toJsonTree(updateReservationRQ, UpdateReservationRQ.class);
            } catch (Exception ex) {
                jsonElementRQ = getError(ex);
            }

            try {
                jsonElementRS = GSON.toJsonTree(updateReservationRQ, UpdateReservationRS.class);
            } catch (Exception ex) {
                jsonElementRS = getError(ex);
            }

            commonAsyncLogOnDatabaseService.logSuccessOnDBAsync(
                    checkInRQ, cartId, recordLocator,
                    jsonElementRQ, jsonElementRS, Constants.AE_LOG_COLLECTION, apiVersion
            );
        }catch (MongoDisconnectException me){
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(checkInRQ.getPos())) {
                LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
            } else {
                LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
            }
        }  catch (Exception ex) {
            LOG.error(ex.getMessage());
        }
    }

    /**
     *
     * @param checkInRQ
     * @param updateReservationRQ
     * @param updateReservationRS
     * @param recordLocator
     * @param cartId
     * @param thr
     */
    public void logAEOnError(
            CheckInRQ checkInRQ,
            UpdateReservationRQ updateReservationRQ,
            UpdateReservationRS updateReservationRS,
            String recordLocator,
            String cartId,
            Throwable thr
    ) {
        try {

            JsonElement jsonElementRQ;
            JsonElement jsonElementRS;
            try {
                jsonElementRQ = GSON.toJsonTree(updateReservationRQ, UpdateReservationRQ.class);
            } catch (Exception ex) {
                jsonElementRQ = getError(ex);
            }

            try {
                jsonElementRS = GSON.toJsonTree(updateReservationRQ, UpdateReservationRS.class);
            } catch (Exception ex) {
                jsonElementRS = getError(ex);
            }

            String errorCode = null;
            String errorMsg = null;

            if (thr instanceof GenericException) {
                GenericException ex = (GenericException) thr;
                errorCode = ex.getErrorCodeType().getErrorCode();
                errorMsg = ex.getErrorCodeType().getFullDescription();
            } else if (thr instanceof MybException) {
                MybException ex = (MybException) thr;
                errorCode = ex.getErrorCode();
                errorMsg = ex.getMsg();
            } else if (thr instanceof Exception) {
                Exception ex = (Exception) thr;
                errorMsg = ex.getMessage();
            }

            commonAsyncLogOnDatabaseService.logErrorDBAsync(
                    checkInRQ, cartId, recordLocator,
                    jsonElementRQ, jsonElementRS, Constants.AE_LOG_COLLECTION, thr, errorCode, errorMsg, apiVersion
            );
        }catch (MongoDisconnectException me){
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(checkInRQ.getPos())) {
                LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
            } else {
                LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
            }
        }  catch (Exception ex) {
            LOG.error(ex.getMessage());
        }
    }

    /**
     *
     * @param purchaseOrderRQ
     * @param collectMiscFeeRQ
     * @param collectMiscFeeRS
     */
    public void logEmdOnSuccess(
            PurchaseOrderRQ purchaseOrderRQ,
            CollectMiscFeeRQ collectMiscFeeRQ, CollectMiscFeeRS collectMiscFeeRS
    ) {
        try {

            JsonElement jsonElementRQ;
            JsonElement jsonElementRS;
            try {
                jsonElementRQ = GSON.toJsonTree(collectMiscFeeRQ, CollectMiscFeeRQ.class);
                MaskConfidentialDataUtil.maskConfidentialDataOnCollectMiscFeeRQ(jsonElementRQ.getAsJsonObject());
            } catch (Exception ex) {
                jsonElementRQ = getError(ex);
            }

            try {
                jsonElementRS = GSON.toJsonTree(collectMiscFeeRS, CollectMiscFeeRS.class);
                MaskConfidentialDataUtil.maskConfidentialDataOnCollectMiscFeeRS(jsonElementRS.getAsJsonObject());
            } catch (Exception ex) {
                jsonElementRS = getError(ex);
            }

            String cartId;
            try {
                cartId = purchaseOrderRQ.getPurchaseOrder().getCartId();
            } catch (Exception ex) {
                cartId = null;
            }

            commonAsyncLogOnDatabaseService.logSuccessOnDBAsync(
                    purchaseOrderRQ, cartId, purchaseOrderRQ.getRecordLocator(),
                    jsonElementRQ, jsonElementRS, Constants.EMD_LOG_COLLECTION, apiVersion
            );
        }catch (MongoDisconnectException me){
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(purchaseOrderRQ.getPos())) {
                LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
            } else {
                LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
            }
        }  catch (Exception ex) {
            LOG.error(ex.getMessage());
        }
    }

    /**
     *
     * @param purchaseOrderRQ
     * @param collectMiscFeeRQ
     * @param collectMiscFeeRS
     * @param thr
     */
    public void logEmdOnError(
            PurchaseOrderRQ purchaseOrderRQ, CollectMiscFeeRQ collectMiscFeeRQ,
            CollectMiscFeeRS collectMiscFeeRS, Throwable thr
    ) {
        try {

            JsonElement jsonElementRQ;
            JsonElement jsonElementRS;
            try {
                jsonElementRQ = GSON.toJsonTree(collectMiscFeeRQ, CollectMiscFeeRQ.class);
                MaskConfidentialDataUtil.maskConfidentialDataOnCollectMiscFeeRQ(jsonElementRQ.getAsJsonObject());
            } catch (Exception ex) {
                jsonElementRQ = getError(ex);
            }

            try {
                jsonElementRS = GSON.toJsonTree(collectMiscFeeRS, CollectMiscFeeRS.class);
                MaskConfidentialDataUtil.maskConfidentialDataOnCollectMiscFeeRS(jsonElementRS.getAsJsonObject());
            } catch (Exception ex) {
                jsonElementRS = getError(ex);
            }

            String cartId;
            try {
                cartId = purchaseOrderRQ.getPurchaseOrder().getCartId();
            } catch (Exception ex) {
                cartId = null;
            }

            String errorCode = null;
            String errorMsg = null;

            if (thr instanceof GenericException) {
                GenericException ex = (GenericException) thr;
                errorCode = ex.getErrorCodeType().getErrorCode();
                errorMsg = ex.getErrorCodeType().getFullDescription();
            } else if (thr instanceof MybException) {
                MybException ex = (MybException) thr;
                errorCode = ex.getErrorCode();
                errorMsg = ex.getMsg();
            } else if (thr instanceof Exception) {
                Exception ex = (Exception) thr;
                errorMsg = ex.getMessage();
            }

            commonAsyncLogOnDatabaseService.logErrorDBAsync(
                    purchaseOrderRQ, cartId, purchaseOrderRQ.getRecordLocator(),
                    jsonElementRQ, jsonElementRS, Constants.EMD_LOG_COLLECTION, thr, errorCode, errorMsg, apiVersion
            );
        }catch (MongoDisconnectException me){
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(purchaseOrderRQ.getPos())) {
                LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
            } else {
                LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
            }
        }  catch (Exception ex) {
            LOG.error(ex.getMessage());
        }
    }

    /**
     *
     * @param ancillariesRQ
     */
    public void logAncillariesOnSuccess(AncillaryRQ ancillariesRQ) {
        if (SystemVariablesUtil.isLoggingEnabled()) {
            try {
                if (PosType.KIOSK.toString().equalsIgnoreCase(ancillariesRQ.getPos())) {
                    commonAsyncLogOnDatabaseService.logSuccessOnDBAsync(
                            ancillariesRQ, ancillariesRQ.getCartId(), ancillariesRQ.getRecordLocator(),
                            null, null, Constants.ANCILLARIES_LOG_COLLECTION, apiVersion
                    );
                }
            } catch (MongoDisconnectException me) {
                if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(ancillariesRQ.getPos())) {
                    LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
                } else {
                    LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
                }
            } catch (Exception ex) {
                LOG.error(ex.getMessage());
            }
        }
    }

    /**
     *
     * @param ancillariesRQ
     * @param thr
     */
    public void logAncillariesOnError(AncillaryRQ ancillariesRQ, Throwable thr) {
        if (SystemVariablesUtil.isLoggingEnabled()) {
            try {
                if (PosType.KIOSK.toString().equalsIgnoreCase(ancillariesRQ.getPos())) {

                    String errorCode = null;
                    String errorMsg = null;

                    if (thr instanceof GenericException) {
                        GenericException ex = (GenericException) thr;
                        errorCode = ex.getErrorCodeType().getErrorCode();
                        errorMsg = ex.getErrorCodeType().getFullDescription();
                    } else if (thr instanceof MybException) {
                        MybException ex = (MybException) thr;
                        errorCode = ex.getErrorCode();
                        errorMsg = ex.getMsg();
                    } else if (thr instanceof Exception) {
                        Exception ex = (Exception) thr;
                        errorMsg = ex.getMessage();
                    }

                    commonAsyncLogOnDatabaseService.logErrorDBAsync(
                            ancillariesRQ, ancillariesRQ.getCartId(), ancillariesRQ.getRecordLocator(),
                            null, null, Constants.ANCILLARIES_LOG_COLLECTION, thr, errorCode, errorMsg, apiVersion
                    );
                }
            } catch (MongoDisconnectException me) {
                if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(ancillariesRQ.getPos())) {
                    LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
                } else {
                    LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
                }
            } catch (Exception ex) {
                LOG.error(ex.getMessage());
            }
        }
    }

    /**
     *
     * @param passengersListRQ
     */
    public void logPriorityListOnSuccess(PassengersListRQ passengersListRQ) {
        if (SystemVariablesUtil.isLoggingEnabled()) {
            try {
                if (PosType.KIOSK.toString().equalsIgnoreCase(passengersListRQ.getPos())) {
                    commonAsyncLogOnDatabaseService.logSuccessOnDBAsync(
                            passengersListRQ, passengersListRQ.getCartId(), passengersListRQ.getRecordLocator(),
                            null, null, Constants.PASSENGERS_LIST_LOG_COLLECTION, apiVersion
                    );
                }
            } catch (MongoDisconnectException me) {
                if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(passengersListRQ.getPos())) {
                    LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
                } else {
                    LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
                }
            } catch (Exception ex) {
                LOG.error(ex.getMessage());
            }
        }
    }

    /**
     *
     * @param passengersListRQ
     * @param thr
     */
    public void logPriorityListOnError(PassengersListRQ passengersListRQ, Throwable thr) {
        if (SystemVariablesUtil.isLoggingEnabled()) {
            try {
                if (PosType.KIOSK.toString().equalsIgnoreCase(passengersListRQ.getPos())) {

                    String errorCode = null;
                    String errorMsg = null;

                    if (thr instanceof GenericException) {
                        GenericException ex = (GenericException) thr;
                        errorCode = ex.getErrorCodeType().getErrorCode();
                        errorMsg = ex.getErrorCodeType().getFullDescription();
                    } else if (thr instanceof MybException) {
                        MybException ex = (MybException) thr;
                        errorCode = ex.getErrorCode();
                        errorMsg = ex.getMsg();
                    } else if (thr instanceof Exception) {
                        Exception ex = (Exception) thr;
                        errorMsg = ex.getMessage();
                    }

                    commonAsyncLogOnDatabaseService.logErrorDBAsync(
                            passengersListRQ, passengersListRQ.getCartId(), passengersListRQ.getRecordLocator(),
                            null, null, Constants.PASSENGERS_LIST_LOG_COLLECTION, thr, errorCode, errorMsg, apiVersion
                    );
                }
            } catch (MongoDisconnectException me) {
                if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(passengersListRQ.getPos())) {
                    LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
                    throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getCode(),
                            prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getActionable(),
                            prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getMessage(),
                            Response.Status.INTERNAL_SERVER_ERROR);
                } else {
                    LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
                    throw me;
                }
            } catch (Exception ex) {
                LOG.error(ex.getMessage());
            }
        }
    }

    /**
     *
     * @param seatmapCheckInRQ
     */
    public void logSeatMapOnSuccess(SeatmapCheckInRQ seatmapCheckInRQ) {
        try {
            commonAsyncLogOnDatabaseService.logSuccessOnDBAsync(
                    seatmapCheckInRQ, seatmapCheckInRQ.getCartId(), seatmapCheckInRQ.getRecordLocator(),
                    null, null, Constants.SEATMAP_LOG_COLLECTION, apiVersion
            );
        }catch (MongoDisconnectException me){
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(seatmapCheckInRQ.getPos())) {
                LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
            } else {
                LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
            }
        }  catch (Exception ex) {
            LOG.error(ex.getMessage());
        }
    }

    /**
     *
     * @param seatmapCheckInRQ
     * @param thr
     */
    public void logSeatMapOnError(SeatmapCheckInRQ seatmapCheckInRQ, Throwable thr) {
        try {

            String errorCode = null;
            String errorMsg = null;

            if (thr instanceof GenericException) {
                GenericException ex = (GenericException) thr;
                errorCode = ex.getErrorCodeType().getErrorCode();
                errorMsg = ex.getErrorCodeType().getFullDescription();
            } else if (thr instanceof MybException) {
                MybException ex = (MybException) thr;
                errorCode = ex.getErrorCode();
                errorMsg = ex.getMsg();
            } else if (thr instanceof Exception) {
                Exception ex = (Exception) thr;
                errorMsg = ex.getMessage();
            }

            commonAsyncLogOnDatabaseService.logErrorDBAsync(
                    seatmapCheckInRQ, seatmapCheckInRQ.getCartId(), seatmapCheckInRQ.getRecordLocator(),
                    null, null, Constants.SEATMAP_LOG_COLLECTION, thr, errorCode, errorMsg, apiVersion
            );
        }catch (MongoDisconnectException me){
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(seatmapCheckInRQ.getPos())) {
                LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
            } else {
                LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
            }
        }  catch (Exception ex) {
            LOG.error(ex.getMessage());
        }
    }

    /**
     *
     * @param issueBagRQ
     */
    public void logBagTagOnSuccess(IssueBagRQ issueBagRQ) {
        try {
            commonAsyncLogOnDatabaseService.logSuccessOnDBAsync(
                    issueBagRQ, issueBagRQ.getCartId(), issueBagRQ.getRecordLocator(),
                    null, null, Constants.BAGTAG_LOG_COLLECTION, apiVersion
            );
        }catch (MongoDisconnectException me){
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(issueBagRQ.getPos())) {
                LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
            } else {
                LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
            }
        }  catch (Exception ex) {
            LOG.error(ex.getMessage());
        }
    }

    /**
     *
     * @param issueBagRQ
     * @param thr
     */
    public void logBagTagOnError(IssueBagRQ issueBagRQ, Throwable thr) {
        try {

            String errorCode = null;
            String errorMsg = null;

            if (thr instanceof GenericException) {
                GenericException ex = (GenericException) thr;
                errorCode = ex.getErrorCodeType().getErrorCode();
                errorMsg = ex.getErrorCodeType().getFullDescription();
            } else if (thr instanceof MybException) {
                MybException ex = (MybException) thr;
                errorCode = ex.getErrorCode();
                errorMsg = ex.getMsg();
            } else if (thr instanceof Exception) {
                Exception ex = (Exception) thr;
                errorMsg = ex.getMessage();
            }

            commonAsyncLogOnDatabaseService.logErrorDBAsync(
                    issueBagRQ, issueBagRQ.getCartId(), issueBagRQ.getRecordLocator(),
                    null, null, Constants.BAGTAG_LOG_COLLECTION, thr, errorCode, errorMsg, apiVersion
            );
        }catch (MongoDisconnectException me){
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(issueBagRQ.getPos())) {
                LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
            } else {
                LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
            }
        }  catch (Exception ex) {
            LOG.error(ex.getMessage());
        }
    }

    /**
     * @param documentEvents
     * @param collection
     */
    private void updateDocuments(String[] documentEvents, String collection) {
        try {
            boolean code = pnrLookupDao.saveMultipleDocuments(documentEvents, collection);
        }catch (MongoDisconnectException me){
            LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
        }  catch (Exception ex) {
            LOG.error(ex.getMessage());
        }
    }

    /**
     *
     * @param ping
     * @param pingRQ
     */
    @SimpleAsync
    public void logPingOnSuccess(Ping ping, PingRQ pingRQ) {
        try {

            PingLog pingLog = new PingLog(ping, apiVersion);
            pingLog.setKioskId(pingRQ.getKioskId());

            PingLog pingLogPrev = null;
            try {
                String pingDocument = pnrLookupDao.getLastDocument("kioskId", pingRQ.getKioskId(), Constants.PING_LOG_COLLECTION);

                pingLogPrev = GSON.fromJson(pingDocument, PingLog.class);
            } catch (Exception ex) {
                //
            }

            try {
                pnrLookupDao.cleanCollecion("kioskId", pingRQ.getKioskId(), Constants.PING_LOG_COLLECTION);
            } catch (Exception ex) {
                //
            }

            List<String> documentsList = new ArrayList<>();

            if (null != pingLogPrev) {
                DocumentEvent documentEventPrev = new DocumentEvent(
                        pingLogPrev.toString(), null, null, Constants.PING_LOG_COLLECTION
                );
                documentsList.add(documentEventPrev.getDocument());
            }

            DocumentEvent documentEvent = new DocumentEvent(
                    pingLog.toString(), null, null, Constants.PING_LOG_COLLECTION
            );
            documentsList.add(documentEvent.getDocument());

            updateDocuments(documentsList.toArray(new String[0]), Constants.PING_LOG_COLLECTION);
        }catch (MongoDisconnectException me){
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(pingRQ.getPos())) {
                LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
            } else {
                LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
            }
        }  catch (Exception ex) {
            LOG.error(ex.getMessage());
        }
    }

    /**
     *
     * @param thr
     */
    @SimpleAsync
    public void logPingOnError(Throwable thr) {
        try {
            PingLog pingLog = new PingLog(hostname, apiVersion);

            if (null != thr) {
                String message = LogUtil.getMessage(thr);
                String debugOn = LogUtil.getTrace(thr);

                pingLog.setDebugHere(debugOn);
                pingLog.setErrorcodeDescription(message);
            }

            PingLog pingLogPrev = null;
            try {
                String pingDocument = pnrLookupDao.getLastDocument(Constants.PING_LOG_COLLECTION);
                pingLogPrev = GSON.fromJson(pingDocument, PingLog.class);
            } catch (Exception ex1) {
                //
            }

            try {
                pnrLookupDao.clearCollection(Constants.PING_LOG_COLLECTION);
            } catch (Exception ex1) {
                //
            }

            List<String> documentsList = new ArrayList<>();

            if (null != pingLogPrev) {
                DocumentEvent documentEventPrev = new DocumentEvent(
                        pingLogPrev.toString(), null, null, Constants.PING_LOG_COLLECTION
                );
                documentsList.add(documentEventPrev.getDocument());
            }

            DocumentEvent documentEvent = new DocumentEvent(
                    pingLog.toString(), null, null, Constants.PING_LOG_COLLECTION
            );
            documentsList.add(documentEvent.getDocument());

            updateDocuments(documentsList.toArray(new String[0]), Constants.PING_LOG_COLLECTION);
        }catch (MongoDisconnectException me){
            LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
        }  catch (Exception ex) {
            LOG.error(ex.getMessage());
        }
    }

    /**
     *
     * @param ex
     * @return
     */
    private JsonElement getError(Exception ex) {
        try {
            return GSON.toJsonTree(ERROR_CONVERTING_TO_JSON + ": " + ex.getMessage());
        } catch (Exception ex1) {
            return null;
        }
    }

    /**
     * @return the systemPropertiesFactory
     */
    public SystemPropertiesFactory getSystemPropertiesFactory() {
        if (null == systemPropertiesFactory) {
            systemPropertiesFactory = new SystemPropertiesFactory();
        }
        return systemPropertiesFactory;
    }

    /**
     * @param systemPropertiesFactory the systemPropertiesFactory to set
     */
    public void setSystemPropertiesFactory(SystemPropertiesFactory systemPropertiesFactory) {
        this.systemPropertiesFactory = systemPropertiesFactory;
    }
}
