package com.am.checkin.web.service;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.AuthorizationResult;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.IssuedEmds;
import com.aeromexico.commons.model.PNR;
import com.aeromexico.commons.model.PNRCollection;
import com.aeromexico.commons.model.PaymentLinked;
import com.aeromexico.commons.model.PectabBoardingPass;
import com.aeromexico.commons.model.PectabReceipt;
import com.aeromexico.commons.model.PectabReceiptData;
import com.aeromexico.commons.model.rq.PurchaseOrderRQ;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.types.LanguageType;
import com.aeromexico.dao.services.IPNRCollectionDao;
import com.am.checkin.web.event.dispatcher.MergePNRCollectionAfterCheckinDispatcher;
import com.am.checkin.web.event.model.*;
import com.google.gson.Gson;
import com.mongodb.MongoException;

import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adrian
 */
@Named
@ApplicationScoped
public class UpdateCollectionAfterCheckinOnDB implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(UpdateCollectionAfterCheckinOnDB.class);

    @Inject
    private MergePNRCollectionAfterCheckinDispatcher mergePNRCollectionAfterCheckinDispatcher;

    @Inject
    private IPNRCollectionDao pnrCollectionDao;

    @Inject
    private PectabReceiptService pectabReceiptService;

    /**
     *
     * @param pnrCollectionAfterCheckin
     * @param legCode
     * @param recordLocator
     * @param cartId
     * @return
     * @throws Exception
     */
    public int synchronousCartUpdate(
            PNRCollection pnrCollectionAfterCheckin, String legCode, String recordLocator, String cartId
    ) throws Exception {

        if (null == pnrCollectionAfterCheckin) {
            LOG.error(recordLocator, cartId, "Pnr collection was null triying to update cart");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION,
                    "Pnr collection was null triying to update cart");
        }

        LOG.debug(recordLocator, cartId, "Finding PNR by recordLocator to update cart: " + recordLocator);
        PNR pnr = pnrCollectionAfterCheckin.getPNRByRecordLocator(recordLocator);
        if (null == pnr) {
            LOG.error(recordLocator, cartId, "Pnr not found while triying to update cart");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION,
                    "Pnr not found while triying to update cart");
        }

        int pnrIndex = pnrCollectionAfterCheckin.getIndexPNRByRecordLocator(recordLocator);
        if (pnrIndex < 0) {
            LOG.error(recordLocator, cartId, "Pnr not found while triying to update cart");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION,
                    "Pnr not found while triying to update cart");
        }

        LOG.debug(recordLocator, cartId, "Finding cartpnr by legCode to update cart: " + legCode);
        CartPNR cartPNR = pnr.getCartPNRByLegCode(legCode);
        if (null == cartPNR) {
            LOG.error(recordLocator, cartId, "Cart not found while triying to update cart");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION,
                    "Cart not found while triying to update cart");
        }

        int cartIndex = pnrCollectionAfterCheckin.getIndexCartPNRByRecordLocatorAndLegCode(recordLocator, legCode);
        if (cartIndex < 0) {
            LOG.error(recordLocator, cartId, "Cart not found while triying to update cart");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION,
                    "Cart not found while triying to update payment list");
        }

        UpdateCartPNREvent updateCartPNREvent = new UpdateCartPNREvent(
                cartPNR, recordLocator, legCode, pnrIndex, cartIndex
        );

        // Retry three times before throw an exception
        int totalRetries = 3;
        int retries = 0;
        boolean retry = true;
        int result = -1;

        while (retry && retries < totalRetries) {
            retries++;
            retry = false;

            try {
                result = pnrCollectionDao.updateCartPNROnPNRCollectionAfterCheckin(updateCartPNREvent.getRecordLocator(), updateCartPNREvent.getLegCode(), updateCartPNREvent.getPnrIndex(), updateCartPNREvent.getCartIndex(), updateCartPNREvent.getCartPNR().toString());
            } catch (MongoException ex) {
                // retry only on DB error

                if (retries >= totalRetries) {
                    throw ex;
                }

                retry = true;
            }
        }

        return result;
    }

    /**
     *
     * @param pnrCollectionAfterCheckin
     * @param authorizationResult
     * @param purchaseOrderRQ
     * @param cartPNR
     * @param recordLocator
     * @param cartId
     * @param currencyCode
     * @param issuedEmds
     */
    public void formAndUpdatePectabReceipt(
            PNRCollection pnrCollectionAfterCheckin,
            AuthorizationResult authorizationResult,
            PurchaseOrderRQ purchaseOrderRQ,
            CartPNR cartPNR,
            String recordLocator,
            String cartId,
            String currencyCode,
            IssuedEmds issuedEmds
    ) {
        if (null != authorizationResult) {
            try {

                LanguageType languageType;
                if (LanguageType.EN.toString().equalsIgnoreCase(purchaseOrderRQ.getLanguage())) {
                    languageType = LanguageType.EN;
                } else {
                    languageType = LanguageType.ES;
                }

                PectabReceiptData pectabReceiptData;
                pectabReceiptData = PectabReceiptService.getPectabReceiptData(
                        purchaseOrderRQ.getPurchaseOrder(),
                        authorizationResult,
                        issuedEmds,
                        languageType,
                        recordLocator,
                        cartId,
                        purchaseOrderRQ.getPos()
                );

                PectabReceipt pectabReceipt;
                pectabReceipt = pectabReceiptService.getPectabReceipt(pectabReceiptData, languageType, currencyCode);
                pectabReceipt.setPaymentId(authorizationResult.getPaymentId());

                cartPNR.getPectabReceiptList().addIfNotExist(pectabReceipt);

                LOG.debug("Pectab Receipt : {} {} {}", recordLocator, cartPNR.getMeta().getCartId(), new Gson().toJson(pectabReceipt));

                // merge receipt pectab
                FireUpdatePectabReceiptListEvent(
                        pnrCollectionAfterCheckin,
                        pectabReceipt,
                        recordLocator,
                        cartId,
                        cartPNR.getLegCode()
                );

            } catch (Exception ex) {
                // ignore
                LOG.error(recordLocator, cartPNR.getMeta().getCartId(), "Error Updating Pectab Receipt");
                LOG.error(recordLocator, cartPNR.getMeta().getCartId(), ex.getMessage());
            }
        }
    }

    /**
     *
     * @param pnrCollectionAfterCheckin
     * @param recordLocator
     * @param cartId
     * @param legCode
     * @throws Exception
     */
    public void FireUpdateCartPNREvent(PNRCollection pnrCollectionAfterCheckin, String recordLocator, String cartId,
            String legCode) throws Exception {
        if (null == pnrCollectionAfterCheckin) {
            LOG.error(recordLocator, cartId, "Pnr collection was null triying to update cart");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION,
                    "Pnr collection was null triying to update cart");
        }

        LOG.debug(recordLocator, cartId, "Finding PNR by recordLocator to update cart: " + recordLocator);
        PNR pnr = pnrCollectionAfterCheckin.getPNRByRecordLocator(recordLocator);
        if (null == pnr) {
            LOG.error(recordLocator, cartId, "Pnr not found while triying to update cart");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION,
                    "Pnr not found while triying to update cart");
        }

        int pnrIndex = pnrCollectionAfterCheckin.getIndexPNRByRecordLocator(recordLocator);
        if (pnrIndex < 0) {
            LOG.error(recordLocator, cartId, "Pnr not found while triying to update cart");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION,
                    "Pnr not found while triying to update cart");
        }

        LOG.debug(recordLocator, cartId, "Finding cartpnr by legCode to update cart: " + legCode);
        CartPNR cartPNR = pnr.getCartPNRByLegCode(legCode);
        if (null == cartPNR) {
            LOG.error(recordLocator, cartId, "Cart not found while triying to update cart");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION,
                    "Cart not found while triying to update cart");
        }

        int cartIndex = pnrCollectionAfterCheckin.getIndexCartPNRByRecordLocatorAndLegCode(recordLocator, legCode);
        if (cartIndex < 0) {
            LOG.error(recordLocator, cartId, "Cart not found while triying to update cart");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION,
                    "Cart not found while triying to update cart");
        }

        UpdateCartPNREvent updateCartPNREvent = new UpdateCartPNREvent(cartPNR, recordLocator, legCode, pnrIndex,
                cartIndex);

        mergePNRCollectionAfterCheckinDispatcher.publishUpdateCartPNREvent(updateCartPNREvent);
    }

    /**
     *
     * @param pnrCollectionAfterCheckin
     * @param paymentLinked
     * @param recordLocator
     * @param cartId
     * @param legCode
     * @throws Exception
     */
    public void FireUpdatePaymentLinkedListEvent(PNRCollection pnrCollectionAfterCheckin, PaymentLinked paymentLinked,
            String recordLocator, String cartId, String legCode) throws Exception {
        if (null == pnrCollectionAfterCheckin) {
            LOG.error(recordLocator, cartId, "Pnr collection was null triying to update payment list");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION,
                    "Pnr collection was null triying to update  payment list");
        }

        LOG.debug(recordLocator, cartId, "Finding PNR by recordLocator to update payment list: " + recordLocator);
        PNR pnr = pnrCollectionAfterCheckin.getPNRByRecordLocator(recordLocator);
        if (null == pnr) {
            LOG.error(recordLocator, cartId, "Pnr not found while triying to update payment list");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION,
                    "Pnr not found while triying to update payment list");
        }

        int pnrIndex = pnrCollectionAfterCheckin.getIndexPNRByRecordLocator(recordLocator);
        if (pnrIndex < 0) {
            LOG.error(recordLocator, cartId, "Pnr not found while triying to update payment list");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION,
                    "Pnr not found while triying to update payment list");
        }

        LOG.debug(recordLocator, cartId, "Finding cartpnr by legCode to update payment list: " + legCode);
        CartPNR cartPNR = pnr.getCartPNRByLegCode(legCode);
        if (null == cartPNR) {
            LOG.error(recordLocator, cartId, "Cart not found while triying to update payment list");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION,
                    "Cart not found while triying to update payment list");
        }

        int cartIndex = pnrCollectionAfterCheckin.getIndexCartPNRByRecordLocatorAndLegCode(recordLocator, legCode);
        if (cartIndex < 0) {
            LOG.error(recordLocator, cartId, "Cart not found while triying to update payment list");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION,
                    "Cart not found while triying to update payment list");
        }

        if (null != paymentLinked) {
            cartPNR.getPaymentLinkedList().addIfNotExist(paymentLinked);
        }

        UpdatePaymentLinkedListEvent paymentLinkedListEvent = new UpdatePaymentLinkedListEvent(
                cartPNR.getPaymentLinkedList(), recordLocator, legCode, pnrIndex, cartIndex);

        mergePNRCollectionAfterCheckinDispatcher.publishPaymentLinkedListEvent(paymentLinkedListEvent);
    }

    /**
     *
     * @param pnrCollectionAfterCheckin
     * @param pectabDataList
     * @param recordLocator
     * @param cartId
     * @param legCode
     * @throws Exception
     */
    public void FireUpdatePectabBoardingPassListEvent(PNRCollection pnrCollectionAfterCheckin,
            List<PectabBoardingPass> pectabDataList, String recordLocator, String cartId, String legCode)
            throws Exception {
        if (null == pnrCollectionAfterCheckin) {
            LOG.error(recordLocator, cartId, "Pnr collection was null triying to update pectab boarding pass list");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION,
                    "Pnr collection was null triying to update pectab boarding pass list");
        }

        LOG.info(recordLocator, cartId,
                "Finding PNR by recordLocator to update pectab boarding pass list: " + recordLocator);
        PNR pnr = pnrCollectionAfterCheckin.getPNRByRecordLocator(recordLocator);
        if (null == pnr) {
            LOG.error(recordLocator, cartId, "Pnr not found while triying to update pectab  boarding pass list");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION,
                    "Pnr not found while triying to update pectab  boarding pass list");
        }

        int pnrIndex = pnrCollectionAfterCheckin.getIndexPNRByRecordLocator(recordLocator);
        if (pnrIndex < 0) {
            LOG.error(recordLocator, cartId, "Pnr not found while triying to update pectab  boarding pass list");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION,
                    "Pnr not found while triying to update pectab  boarding pass list");
        }

        LOG.info(recordLocator, cartId,
                "Finding cartpnr by legCode to update pectab boarding pass list: " + legCode);
        CartPNR cartPNR = pnr.getCartPNRByLegCode(legCode);
        if (null == cartPNR) {
            LOG.error(recordLocator, cartId, "Cart not found while triying to update pectab  boarding pass list");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION,
                    "Cart not found while triying to update pectab  boarding pass list");
        }

        int cartIndex = pnrCollectionAfterCheckin.getIndexCartPNRByRecordLocatorAndLegCode(recordLocator, legCode);
        if (cartIndex < 0) {
            LOG.error(recordLocator, cartId, "Cart not found while triying to update pectab  boarding pass list");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION,
                    "Cart not found while triying to update pectab  boarding pass list");
        }

        if (null != pectabDataList && !pectabDataList.isEmpty()) {
            cartPNR.getPectabBoardingPassList().addAllIfNotExist(pectabDataList);
        }

        UpdatePectabBoardingPassListEvent updatePectabDataListEvent = new UpdatePectabBoardingPassListEvent(
                cartPNR.getPectabBoardingPassList(), recordLocator, legCode, pnrIndex, cartIndex);
        mergePNRCollectionAfterCheckinDispatcher.publishUpdatePectabBoardingPassListEvent(updatePectabDataListEvent);
    }

    /**
     *
     * @param pnrCollectionAfterCheckin
     * @param pectabReceipt
     * @param recordLocator
     * @param cartId
     * @param legCode
     * @throws Exception
     */
    public void FireUpdatePectabReceiptListEvent(PNRCollection pnrCollectionAfterCheckin, PectabReceipt pectabReceipt,
            String recordLocator, String cartId, String legCode) throws Exception {

        if (null == pnrCollectionAfterCheckin) {
            LOG.error(recordLocator, cartId, "Pnr collection was null triying to update pectab receipt list");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION,
                    "Pnr collection was null triying to update pectab receipt list");
        }

        LOG.info(recordLocator, cartId,
                "Finding PNR by recordLocator to update pectab receipt list: " + recordLocator);
        PNR pnr = pnrCollectionAfterCheckin.getPNRByRecordLocator(recordLocator);
        if (null == pnr) {
            LOG.error(recordLocator, cartId, "Pnr not found  while triying to update pectab receipt list");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION,
                    "Pnr not found  while triying to update pectab receipt list");
        }

        int pnrIndex = pnrCollectionAfterCheckin.getIndexPNRByRecordLocator(recordLocator);
        if (pnrIndex < 0) {
            LOG.error(recordLocator, cartId, "Pnr not found  while triying to update pectab receipt list");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION,
                    "Pnr not found  while triying to update pectab receipt list");
        }

        LOG.info(recordLocator, cartId, "Finding cartpnr by legCode to update pectab receipt list: " + legCode);
        CartPNR cartPNR = pnr.getCartPNRByLegCode(legCode);
        if (null == cartPNR) {
            LOG.error(recordLocator, cartId, "Cart not found while triying to update pectab receipt list");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION,
                    "Cart not found while triying to update pectab receipt list");
        }

        int cartIndex = pnrCollectionAfterCheckin.getIndexCartPNRByRecordLocatorAndLegCode(recordLocator, legCode);
        if (cartIndex < 0) {
            LOG.error(recordLocator, cartId, "Cart not found while triying to update pectab receipt list");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION,
                    "Cart not found while triying to update pectab receipt list");
        }

        if (null != pectabReceipt) {
            cartPNR.getPectabReceiptList().addIfNotExist(pectabReceipt);
        }

        UpdatePectabReceiptListEvent updatePectabReceiptListEvent = new UpdatePectabReceiptListEvent(
                cartPNR.getPectabReceiptList(), recordLocator, legCode, pnrIndex, cartIndex);

        mergePNRCollectionAfterCheckinDispatcher.publishUpdatePectabReceiptListEvent(updatePectabReceiptListEvent);
    }

    /**
     *
     * @param pnrCollectionAfterCheckin
     * @param bookedTravelersEligibleForCheckin
     * @param recordLocator
     * @param cartId
     * @param legCode
     * @throws Exception
     */
    public void FireUpdateBookedTravelerEvents(PNRCollection pnrCollectionAfterCheckin,
            List<BookedTraveler> bookedTravelersEligibleForCheckin, String recordLocator, String cartId, String legCode)
            throws Exception {
        if (null == pnrCollectionAfterCheckin) {
            LOG.error(recordLocator, cartId, "Pnr collection was null triying to update booked travelers");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION,
                    "Pnr collection was null triying to update booked travelers");
        }

        int pnrIndex = pnrCollectionAfterCheckin.getIndexPNRByRecordLocator(recordLocator);

        if (pnrIndex >= 0) {

            CartPNR cartPNR = pnrCollectionAfterCheckin.getCartPNRByRecordLocatorAndLegCode(recordLocator, legCode);
            int cartIndex = pnrCollectionAfterCheckin.getIndexCartPNRByRecordLocatorAndLegCode(recordLocator, legCode);

            if (null != cartPNR && cartIndex >= 0) {
                for (BookedTraveler bookedTravelerCheckedIn : bookedTravelersEligibleForCheckin) {

                    int passengerIndex = 0;
                    boolean found = false;
                    for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
                        if (bookedTravelerCheckedIn.getId().equalsIgnoreCase(bookedTraveler.getId())) {
                            found = true;
                            break;
                        }
                        passengerIndex++;
                    }

                    if (found) {

                        UpdateBookedTravelerEvent updateBookedTravelerEvent = new UpdateBookedTravelerEvent(
                                bookedTravelerCheckedIn, pnrIndex, cartIndex, passengerIndex);

                        mergePNRCollectionAfterCheckinDispatcher
                                .publishUpdateBookedTravelerEvent(updateBookedTravelerEvent);
                    }
                }
            } else {
                //
                LOG.error(recordLocator, cartId, "Cart not found  while triying to update booked travelers");
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION,
                        "Cart not found  while triying to update booked travelers");
            }

        } else {
            //
            LOG.error(recordLocator, cartId, "Pnr not found  while triying to update booked travelers");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION,
                    "Pnr not found  while triying to update booked travelers");
        }
    }

}
