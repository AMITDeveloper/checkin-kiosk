package com.am.checkin.web.service;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookingClassMap;
import com.aeromexico.commons.model.PNR;
import com.aeromexico.commons.model.PNRCollection;
import com.aeromexico.commons.model.SeatmapCollection;
import com.aeromexico.commons.model.SeatmapCollectionWrapper;
import com.aeromexico.commons.model.rq.SeatmapCheckInRQ;
import com.aeromexico.commons.seatmaps.SeatMapCommonService;
import com.aeromexico.commons.web.types.PassengerFareType;
import com.aeromexico.commons.web.types.PosType;
import com.aeromexico.dao.dispatcher.SaveSeatmapCollectionDispatcher;
import com.aeromexico.dao.event.SaveSeatmapCollectionEvent;
import com.aeromexico.dao.services.IPNRLookupDao;
import com.aeromexico.dao.util.CommonUtil;
import com.am.checkin.web.event.dispatcher.UpdatePNRCollectionDispatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@ApplicationScoped
public class SeatMapsService {

    private static final Logger log = LoggerFactory.getLogger(SeatMapsService.class);

    @Inject
    private SaveSeatmapCollectionDispatcher saveSeatmapCollectionDispatcher;

    @Inject
    private UpdatePNRCollectionDispatcher pnrCollectionDispatcher;

    @Inject
    private SeatMapCommonService seatMapCommonService;

    @Inject
    private IPNRLookupDao pnrLookupDao;

    @Inject
    @Named("CheckInSetUpConfigFactoryWrapper")
    private CheckInSetUpConfigFactoryWrapper setUpConfigFactory;
    
    @PostConstruct
    public void init() {
        if (null == setUpConfigFactory) {
            setUpConfigFactory = new CheckInSetUpConfigFactoryWrapper();
        }
    }

    /**
     *
     * @param seatMapRQ
     * @return SeatmapCollection
     * @throws Exception
     */
    public SeatmapCollection getSeatMaps(SeatmapCheckInRQ seatMapRQ) throws Exception {
        // This should return only ONE PNR.
        PNRCollection pnrCollection = pnrLookupDao.readPNRCollectionByCartId(seatMapRQ.getCartId());
        if (null == pnrCollection) {
            log.info("Can't find PNR: " + seatMapRQ.getCartId());
            return null;
        } else if (pnrCollection.getCollection().size() != 1) {
            log.info("Can't find PNR: " + seatMapRQ.getCartId());
            return null;
        }

        BookedLeg bookedLeg = pnrCollection.getBookedLegByCartId(seatMapRQ.getCartId());
        String link = CommonUtil.getLink(seatMapRQ.getUri());
        //Leer tipo de pasajero
        if (pnrCollection.getCollection().get(0).isStandby() 
                || PassengerFareType.E.getCode().equalsIgnoreCase(pnrCollection.getCollection().get(0).getCarts().getCollection().get(0).getTravelerInfo().getCollection().get(0).getPassengerType())) {
            return seatMapCommonService.getSeatMapCheckInSOAPUnavailable(bookedLeg, link);
        }

        if (null != pnrCollection.getStore() && !pnrCollection.getStore().trim().isEmpty()) {
            seatMapRQ.setStore(pnrCollection.getStore());
        }

        if (null != pnrCollection.getPos() && !pnrCollection.getPos().trim().isEmpty()) {
            seatMapRQ.setPos(pnrCollection.getPos());
        }else{
            seatMapRQ.setPos(PosType.WEB.toString());
        }

        PNR pnr = pnrCollection.getPNRByCartId(seatMapRQ.getCartId());
        if (null != pnr) {
            seatMapRQ.setRecordLocator(pnr.getPnr());
        }

        SeatmapCollection seatmapCollection;
        try {
            // We are going to use the BookingClasses of the first passenger/traveler in the PNR cart.
            BookingClassMap bookingClassMap = pnrCollection.getBookingClassMapByCardId(seatMapRQ.getCartId());
            String paymentEnabled = seatMapRQ.getPaymentEnabled() != null ? seatMapRQ.getPaymentEnabled() : "false";
            seatMapRQ.setNeededSeats(pnrCollection.getNeededSeats(seatMapRQ.getCartId()));

            seatmapCollection = seatMapCommonService.getSeatMapCheckInSOAP(bookedLeg, bookingClassMap, link, seatMapRQ.getStore(),
                    seatMapRQ.getPos(), paymentEnabled, seatMapRQ.getNeededSeats(), setUpConfigFactory);
        } catch (Exception e) {
            log.error("seatMapCommonService Cart Id: " + seatMapRQ.getCartId() + ", " + e.getMessage());
            throw e;
        }
        try {
            SeatmapCollectionWrapper seatmapCollectionWrapper = new SeatmapCollectionWrapper();
            seatmapCollectionWrapper.setCartId(seatMapRQ.getCartId());
            seatmapCollectionWrapper.setSeatmapCollection(seatmapCollection);
            saveSeatmapCollection(seatmapCollectionWrapper);
        } catch (Exception ex) {
            log.error("seatmapCollectionWrapper: " + ex.getMessage());
        }
        return seatmapCollection;
    }

    /**
     *
     * @param seatmapCollectionWrapper
     */
    private void saveSeatmapCollection(SeatmapCollectionWrapper seatmapCollectionWrapper) {
        saveSeatmapCollection(seatmapCollectionWrapper, false);
    }

    /**
     *
     * This method is used to update the PNR collection MongoDB in an
     * asynchronous mode
     */
    private void saveSeatmapCollection(SeatmapCollectionWrapper seatmapCollectionWrapper, boolean mustBeAsynchronous) {
        if (mustBeAsynchronous) {
            try {
                SaveSeatmapCollectionEvent saveSeatmapCollectionEvent = new SaveSeatmapCollectionEvent(seatmapCollectionWrapper);
                saveSeatmapCollectionDispatcher.publish(saveSeatmapCollectionEvent);
            } catch (Exception ex) {
                log.error(ex.getMessage());
                //ignore
            }
        } else {
            try {
                SaveSeatmapCollectionEvent saveSeatmapCollectionEvent = new SaveSeatmapCollectionEvent(seatmapCollectionWrapper);
                int code = pnrLookupDao.saveSeatMapCollection(saveSeatmapCollectionEvent.getSeatmapCollectionWrapper());

                log.info("Saving seat map collection to DB, code:" + code);
            } catch (Exception ex) {
                log.info("Error saving seat map collection to data base: " + ex.getMessage());
            }
        }
    }

    public SaveSeatmapCollectionDispatcher getSaveSeatmapCollectionDispatcher() {
        return saveSeatmapCollectionDispatcher;
    }

    public void setSaveSeatmapCollectionDispatcher(SaveSeatmapCollectionDispatcher saveSeatmapCollectionDispatcher) {
        this.saveSeatmapCollectionDispatcher = saveSeatmapCollectionDispatcher;
    }

    public UpdatePNRCollectionDispatcher getPnrCollectionDispatcher() {
        return pnrCollectionDispatcher;
    }

    public void setPnrCollectionDispatcher(UpdatePNRCollectionDispatcher pnrCollectionDispatcher) {
        this.pnrCollectionDispatcher = pnrCollectionDispatcher;
    }

    public SeatMapCommonService getSeatMapCommonService() {
        return seatMapCommonService;
    }

    public void setSeatMapCommonService(SeatMapCommonService seatMapCommonService) {
        this.seatMapCommonService = seatMapCommonService;
    }

    public IPNRLookupDao getPnrLookupDao() {
        return pnrLookupDao;
    }

    public void setPnrLookupDao(IPNRLookupDao pnrLookupDao) {
        this.pnrLookupDao = pnrLookupDao;
    }
}
