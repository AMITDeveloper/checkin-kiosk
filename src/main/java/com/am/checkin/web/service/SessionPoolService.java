package com.am.checkin.web.service;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeromexico.commons.model.SabreSessionPool;
import com.aeromexico.dao.client.DBAPI;
import com.aeromexico.sabre.api.sessionpool.manager.SabreSessionPoolManager;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

@Named
@ApplicationScoped
public class SessionPoolService {

    private static final Logger LOG = LoggerFactory.getLogger(SessionPoolService.class);

    @Inject
    private SabreSessionPoolManager sabreSessionPoolManager;

    @Inject
    private DBAPI dbAPI;

    public SabreSessionPool getSessionPoolInformation() throws Exception {

        com.aeromexico.sabre.api.sessionpool.factory.SabreSessionPool sabreSessionPool = sabreSessionPoolManager.getSabreSessionPool();
        SabreSessionPool sabreSessionPoolInfo = transformSabreSessionPool(sabreSessionPool);

        LOG.info("SESSION POOL INFORMATION - " + sabreSessionPool);
        return sabreSessionPoolInfo;
    }

    private SabreSessionPool transformSabreSessionPool(com.aeromexico.sabre.api.sessionpool.factory.SabreSessionPool sabreSessionPool) {

        SabreSessionPool sabreSessionPoolInfo = new SabreSessionPool();

        sabreSessionPoolInfo.setBlockWhenExhausted(sabreSessionPool.getBlockWhenExhausted());
        sabreSessionPoolInfo.setBorrowedCount(sabreSessionPool.getBorrowedCount());
        sabreSessionPoolInfo.setCreatedCount(sabreSessionPool.getCreatedCount());
        sabreSessionPoolInfo.setCreationStackTrace(sabreSessionPool.getCreationStackTrace());
        sabreSessionPoolInfo.setDestroyedByBorrowValidationCount(sabreSessionPool.getDestroyedByBorrowValidationCount());
        sabreSessionPoolInfo.setDestroyedByEvictorCount(sabreSessionPool.getDestroyedByEvictorCount());
        sabreSessionPoolInfo.setDestroyedCount(sabreSessionPool.getDestroyedCount());
        sabreSessionPoolInfo.setEvictionPolicyClassName(sabreSessionPool.getEvictionPolicyClassName());
        sabreSessionPoolInfo.setFactoryType(sabreSessionPool.getFactoryType());
        sabreSessionPoolInfo.setFairness(sabreSessionPool.getFairness());
        sabreSessionPoolInfo.setLifo(sabreSessionPool.getLifo());
        sabreSessionPoolInfo.setLogAbandoned(sabreSessionPool.getLogAbandoned());
        sabreSessionPoolInfo.setMaxBorrowWaitTimeMillis(sabreSessionPool.getMaxBorrowWaitTimeMillis());
        sabreSessionPoolInfo.setMaxIdle(sabreSessionPool.getMaxIdle());
        sabreSessionPoolInfo.setMaxTotal(sabreSessionPool.getMaxTotal());
        sabreSessionPoolInfo.setMaxWaitMillis(sabreSessionPool.getMaxWaitMillis());
        sabreSessionPoolInfo.setMeanActiveTimeMillis(sabreSessionPool.getMeanActiveTimeMillis());
        sabreSessionPoolInfo.setMeanIdleTimeMillis(sabreSessionPool.getMeanIdleTimeMillis());
        sabreSessionPoolInfo.setMeanBorrowWaitTimeMillis(sabreSessionPool.getMeanBorrowWaitTimeMillis());
        sabreSessionPoolInfo.setMinEvictableIdleTimeMillis(sabreSessionPool.getMinEvictableIdleTimeMillis());
        sabreSessionPoolInfo.setMinIdle(sabreSessionPool.getMinIdle());
        sabreSessionPoolInfo.setNumActive(sabreSessionPool.getNumActive());
        sabreSessionPoolInfo.setNumIdle(sabreSessionPool.getNumIdle());
        sabreSessionPoolInfo.setNumTestsPerEvictionRun(sabreSessionPool.getNumTestsPerEvictionRun());
        sabreSessionPoolInfo.setNumWaiters(sabreSessionPool.getNumWaiters());
        sabreSessionPoolInfo.setRemoveAbandonedOnBorrow(sabreSessionPool.getRemoveAbandonedOnBorrow());
        sabreSessionPoolInfo.setRemoveAbandonedOnMaintenance(sabreSessionPool.getRemoveAbandonedOnMaintenance());
        sabreSessionPoolInfo.setRemoveAbandonedTimeout(sabreSessionPool.getRemoveAbandonedTimeout());
        sabreSessionPoolInfo.setReturnedCount(sabreSessionPool.getReturnedCount());
        sabreSessionPoolInfo.setSoftMinEvictableIdleTimeMillis(sabreSessionPool.getSoftMinEvictableIdleTimeMillis());
        sabreSessionPoolInfo.setSoftMinEvictableIdleTimeMillis(sabreSessionPool.getSoftMinEvictableIdleTimeMillis());
        sabreSessionPoolInfo.setTestOnBorrow(sabreSessionPool.getTestOnBorrow());
        sabreSessionPoolInfo.setTestOnCreate(sabreSessionPool.getTestOnCreate());
        sabreSessionPoolInfo.setTestOnReturn(sabreSessionPool.getTestOnReturn());
        sabreSessionPoolInfo.setTestWhileIdle(sabreSessionPool.getTestWhileIdle());
        sabreSessionPoolInfo.setTimeBetweenEvictionRunsMillis(sabreSessionPool.getTimeBetweenEvictionRunsMillis());

        try {

            LOG.info("Mongo options: " + dbAPI.getMongoClientOptions().toString());

            Gson gson = new Gson();
            JsonElement json = gson.fromJson(dbAPI.getMongoClientOptions().toString(), JsonElement.class);
            sabreSessionPoolInfo.setMongoClientOptions(json);
        } catch (Exception ex) {
            LOG.error("Error reading mongo options: " + ex.getMessage(), ex);
        }

        return sabreSessionPoolInfo;
    }

}
