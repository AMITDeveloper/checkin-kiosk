package com.am.checkin.web.service;

import com.aeromexico.commons.web.util.Constants;
import com.aeromexico.commons.web.util.ErrorCodeDescriptions;
import com.aeromexico.cybersource.model.CybersourceResponse;
import com.aeromexico.cybersource.service.AMCybersourceService;
import com.aeromexico.cybersource.util.CybersourceConstants;
import com.aeromexico.dao.qualifiers.BaggageAllowanceCache;
import com.aeromexico.commons.exception.model.CybersourceException;
import com.aeromexico.commons.exception.model.CybersourceInternalErrorException;
import com.aeromexico.commons.model.Address;
import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.CreditCardPaymentRequest;
import com.aeromexico.commons.model.rq.PurchaseOrderRQ;
import com.aeromexico.commons.web.types.ItemsToBePaidType;
import com.aeromexico.commons.web.util.MaskCreditCardUtil;
import com.aeromexico.dao.config.GlobalConfiguration;
import com.aeromexico.webe4.web.common.types.CybersourceReasonCodeType;
import com.aeromexico.webe4.web.common.types.ErrorCodeType;
import com.cybersource.schemas.transaction_data_1.CaseManagementActionService;
import com.cybersource.schemas.transaction_data_1.ReplyMessage;
import com.cybersource.schemas.transaction_data_1.RequestMessage;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import com.aeromexico.dao.services.IBaggageAllowanceDao;
import com.am.checkin.web.util.CheckinCyberSourceUtil;
import javax.enterprise.context.ApplicationScoped;

/**
 *
 * @author adrian
 */
@Named("cybersourceService")
@ApplicationScoped
public class CheckinCybersourceService {

    private static final MaskCreditCardUtil MASK_CREDIT_CARD_UTIL = new MaskCreditCardUtil();

    private static final Logger logger = LoggerFactory.getLogger(CheckinCybersourceService.class);
    private final Gson gson;

    @Inject
    private AMCybersourceService cybersourceService;

    @Inject
    @BaggageAllowanceCache
    private IBaggageAllowanceDao compBagAllowDao;

    @Inject
    @Named("CheckInSetUpConfigFactoryWrapper")
    private CheckInSetUpConfigFactoryWrapper setUpConfigFactory;

    private CheckinCyberSourceUtil cyberSourceUtil;

    /**
     *
     */
    @PostConstruct
    public void init() {
        if (null == setUpConfigFactory) {
            setUpConfigFactory = new CheckInSetUpConfigFactoryWrapper();
        }

        this.cyberSourceUtil = new CheckinCyberSourceUtil(getCompBagAllowAPI());
    }

    /**
     *
     */
    public CheckinCybersourceService() {
        this.gson = new Gson();
    }

    /**
     *
     * @param requestMessage
     * @param cartId
     * @return
     * @throws Exception
     */
    private CybersourceResponse callCybersource(RequestMessage requestMessage, String cartId) throws Exception {
        logger.info("************CYBERSOURCE************");
        if (null != requestMessage.getCard() && null != requestMessage.getCard().getAccountNumber()) {
            String accountNumber = requestMessage.getCard().getAccountNumber();
            //hide confidential data before log
            requestMessage.getCard().setAccountNumber(MASK_CREDIT_CARD_UTIL.maskCardNumber(accountNumber));
            logger.info("Cybersource request: " + cybersourceService.toString(requestMessage));
            requestMessage.getCard().setAccountNumber(accountNumber);
        } else {
            logger.info("Cybersource request: " + cybersourceService.toString(requestMessage));
        }

        ReplyMessage replyMessage = getCybersourceService().callCybersourceSOAPWS(requestMessage);

        logger.info("Cybersource response: " + cybersourceService.toString(replyMessage));
        logger.info("Call Cyber Source", cartId, gson.toJson(requestMessage), gson.toJson(replyMessage));

        // Poputating the AMCybersourceService response fields that we care
        CybersourceResponse cybersourceResponse = new CybersourceResponse();

        if (null != replyMessage) {

            cybersourceResponse.setDecision(replyMessage.getDecision());
            cybersourceResponse.setReasonCode(replyMessage.getReasonCode());
            cybersourceResponse.setRequestId(replyMessage.getRequestID());
            cybersourceResponse.setRequestToken(replyMessage.getRequestToken());
            cybersourceResponse.setIssuerMessage(replyMessage.getIssuerMessage());

            if (replyMessage.getInvalidField() != null) {

                // Originally the code above used new String[list.size()].
                // However, this
                // http://shipilev.net/blog/2016/arrays-wisdom-ancients/
                // reveals that due to JVM optimizations, using new String[0] is
                // better now.
                String[] invalidField = replyMessage.getInvalidField().toArray(new String[0]);
                cybersourceResponse.setInvalidField(invalidField);
            }

            if (replyMessage.getMissingField() != null) {
                // Originally the code above used new String[list.size()].
                // However, this
                // http://shipilev.net/blog/2016/arrays-wisdom-ancients/
                // reveals that due to JVM optimizations, using new String[0] is
                // better now.

                String[] missingField = replyMessage.getMissingField().toArray(new String[0]);
                cybersourceResponse.setMissingField(missingField);
            }
        }

        return cybersourceResponse;
    }

    /**
     *
     * @param requestMessageList
     * @param cartId
     * @return
     * @throws CybersourceException
     * @throws CybersourceInternalErrorException
     */
    private List<CybersourceResponse> callCybersourceList(
            List<RequestMessage> requestMessageList,
            String cartId
    ) throws CybersourceException, CybersourceInternalErrorException {

        List<CybersourceResponse> cybersourceResponseList = new ArrayList<>();
        if (null == requestMessageList || requestMessageList.isEmpty()) {
            return cybersourceResponseList;
        }

        for (RequestMessage requestMessage : requestMessageList) {
            try {
                boolean retry;
                boolean checkForRetry = true;
                do {
                    retry = false;

                    CybersourceResponse cybersourceResponse = callCybersource(requestMessage, cartId);

                    CybersourceReasonCodeType cybersourceReasonCodeType = CybersourceReasonCodeType
                            .getType(cybersourceResponse.getReasonCode().intValue());
                    String msg = cybersourceReasonCodeType != null ? " | " + cybersourceReasonCodeType
                            .getReasoncodeMessage().get(cybersourceResponse.getReasonCode().intValue())
                            : Constants.UNKNOWN_CODE;

                    // decline if the reasonCode is unknown
                    CybersourceReasonCodeType.ActionType action = cybersourceReasonCodeType != null
                            ? cybersourceReasonCodeType.getAction() : CybersourceReasonCodeType.ActionType.RETRY;

                    if (null == action) {
                        if (checkForRetry) {
                            checkForRetry = false;
                            retry = true;
                        } else {
                            throw new CybersourceException(Response.Status.BAD_REQUEST.getStatusCode(),
                                    ErrorCodeDescriptions.MSG_CODE_CYBERSOURCE_REJECTION + " retry more than two times",
                                    ErrorCodeType.TRANSACTION_REJECTED.getErrorCode());
                        }
                    } else {
                        switch (action) {
                            case ACCEPT:
                            case SOFT_DECLINE:
                                cybersourceResponseList.add(cybersourceResponse);
                                break;
                            case DECLINE:
                            case ERROR:
                                int reasonCode = cybersourceResponse.getReasonCode().intValue();
                                msg = msg + " | reasonCode: " + reasonCode;
                                msg = msg + " | decision: " + cybersourceResponse.getDecision();
                                String reasonCodeStr = cybersourceReasonCodeType != null
                                        ? cybersourceReasonCodeType.getReasoncodeStr() : Constants.UNKNOWN_CODE;

                                // just for customLog
                                if (cybersourceReasonCodeType != null
                                        && (cybersourceReasonCodeType == CybersourceReasonCodeType.DINVALIDDATA
                                        || cybersourceReasonCodeType == CybersourceReasonCodeType.DMISSINGFIELD)) {
                                    String[] invalidFields = cybersourceResponse.getInvalidField();
                                    String[] missingFields = cybersourceResponse.getMissingField();

                                    logger.info("cybersource rejected the transaction");
                                    logger.info("reasonCode: ");
                                    logger.info(String.valueOf(reasonCode));
                                    logger.info("reasonCodeStr: " + reasonCodeStr);

                                    if (null != invalidFields && invalidFields.length > 0) {
                                        msg = msg + " | invalidFields: [";
                                        logger.info("invalidFields: ");
                                        for (String invalidFiled : invalidFields) {
                                            logger.info(invalidFiled);
                                            int index = invalidFiled.lastIndexOf(":");
                                            msg = msg + invalidFiled.substring(index >= 0 ? index + 1 : 0) + ", ";
                                        }
                                        msg = msg.substring(0, msg.length() - 2);
                                        msg = msg + "]";
                                    }

                                    if (null != missingFields && missingFields.length > 0) {
                                        logger.info("missingFields: ");
                                        msg = msg + " | missingFields: [";
                                        for (String missingFiled : missingFields) {
                                            logger.info(missingFiled);
                                            int index = missingFiled.lastIndexOf(":");
                                            msg = msg + missingFiled.substring(index >= 0 ? index + 1 : 0) + ", ";
                                        }
                                        msg = msg.substring(0, msg.length() - 2);
                                        msg = msg + "]";
                                    }
                                }

                                // just for customLog
                                String issuerMessage = cybersourceResponse.getIssuerMessage();
                                if (null != issuerMessage && !issuerMessage.isEmpty()) {
                                    logger.info("issuerMessage: ");
                                    logger.info(issuerMessage);
                                    msg = msg + " | issuerMessage: ";
                                    msg += issuerMessage;
                                }

                                throw new CybersourceException(Response.Status.BAD_REQUEST.getStatusCode(),
                                        ErrorCodeDescriptions.MSG_CODE_CYBERSOURCE_REJECTION + msg,
                                        ErrorCodeType.TRANSACTION_REJECTED.getErrorCode());

                            case RETRY:
                            default:
                                if (checkForRetry) {
                                    checkForRetry = false;
                                    retry = true;
                                } else {
                                    throw new CybersourceException(Response.Status.BAD_REQUEST.getStatusCode(),
                                            ErrorCodeDescriptions.MSG_CODE_CYBERSOURCE_REJECTION + " retry more than two times",
                                            ErrorCodeType.TRANSACTION_REJECTED.getErrorCode());
                                }
                                break;
                        }
                    }

                } while (retry);
            } catch (Exception ex) {
                logger.error("Error to invoque cybersource service pre, the remote service is unavailable");
                logger.error("Error: {}", ex);

                throw new CybersourceInternalErrorException(Response.Status.BAD_REQUEST.getStatusCode(),
                        ErrorCodeDescriptions.MSG_CODE_CYBERSOURCE_INTERNAL_ERROR + " | " + ex.getMessage(),
                        ErrorCodeType.INTERNAL_PROVIDER_ERROR.getErrorCode());
            }
        }

        return cybersourceResponseList;
    }

    /**
     *
     * @param creditCardPaymentRequestList
     * @param address
     * @param purchaseOrderRQ
     * @param bookedTravelersRequested
     * @param bookedLegList
     * @param toPaidItemsType
     * @param fingerprintSessionId
     * @param ipAddr
     * @param recordLocator
     * @param creationDate
     * @param cartId
     * @param store
     * @param pos
     * @param transactionTime
     * @return
     */
    public List<RequestMessage> getCybersourceRQ(
            List<CreditCardPaymentRequest> creditCardPaymentRequestList,
            Address address,
            PurchaseOrderRQ purchaseOrderRQ,
            List<BookedTraveler> bookedTravelersRequested,
            List<BookedLeg> bookedLegList,
            ItemsToBePaidType toPaidItemsType,
            String fingerprintSessionId,
            String ipAddr,
            String recordLocator,
            String creationDate,
            String cartId,
            String store,
            String pos,
            String transactionTime,
            String kioskId
    ) {
        List<RequestMessage> requestMessageList = new ArrayList<>();
        if (null == creditCardPaymentRequestList || creditCardPaymentRequestList.isEmpty()) {
            return requestMessageList;
        }

        try {
            requestMessageList = cyberSourceUtil.getTransformedRequestForCybersource(
                    creditCardPaymentRequestList,
                    address,
                    bookedTravelersRequested,
                    bookedLegList,
                    toPaidItemsType,
                    recordLocator,
                    creationDate,
                    cartId,
                    fingerprintSessionId,
                    ipAddr,
                    transactionTime,
                    store,
                    kioskId
            );
        } catch (Exception e) {
            logger.error("Cannot transform request for cybersource");
            logger.error("Error: {}", e);

            throw new CybersourceInternalErrorException(Response.Status.BAD_REQUEST.getStatusCode(),
                    ErrorCodeDescriptions.MSG_CODE_CYBERSOURCE_INTERNAL_ERROR + " | " + e.getMessage(),
                    ErrorCodeType.INTERNAL_PROVIDER_ERROR.getErrorCode());
        }

        return requestMessageList;
    }

    /**
     *
     * @param requestMessageList
     * @param cartId
     * @return
     * @throws CybersourceException
     * @throws CybersourceInternalErrorException
     */
    public List<CybersourceResponse> preTicketCall(
            List<RequestMessage> requestMessageList,
            String cartId
    ) throws CybersourceException, CybersourceInternalErrorException {

        return callCybersourceList(
                requestMessageList,
                cartId
        );
    }

    /**
     *
     * @param cyberSourceResponse
     * @param message
     * @param cartId
     * @return
     * @throws CybersourceException
     * @throws CybersourceInternalErrorException
     */
    public CybersourceResponse postTicketCall(
            CybersourceResponse cyberSourceResponse, String message, String cartId
    ) throws CybersourceException, CybersourceInternalErrorException {
        try {
            GlobalConfiguration.CybersourceConfig cybersourceConfig = setUpConfigFactory.getInstance().getCybersourceConfig();

            RequestMessage requestMessage = new RequestMessage();
            requestMessage.setMerchantID(cybersourceConfig.getMerchantId());
            requestMessage.setMerchantReferenceCode(cartId);
            requestMessage.setClientLibrary("Java Axis WSS4J");
            requestMessage.setClientLibraryVersion(CybersourceConstants.LIB_VERSION);
            requestMessage.setClientEnvironment(System.getProperty("os.name") + "/" + System.getProperty("os.version")
                    + "/" + System.getProperty("java.vendor") + "/" + System.getProperty("java.version"));
            // System.setProperty("axis.ClientConfigFile", "SampleDeploy.wsdd");
            requestMessage.setCaseManagementActionService(new CaseManagementActionService());
            requestMessage.getCaseManagementActionService().setRun("true");
            requestMessage.getCaseManagementActionService().setRequestID(cyberSourceResponse.getRequestId());
            requestMessage.getCaseManagementActionService().setComments(message);
            requestMessage.getCaseManagementActionService().setActionCode("ADD_NOTE");

            return callCybersource(requestMessage, cartId);
        } catch (Exception ex) {
            logger.error("Error to invoque cybersource service post, the remote service is unavailable", ex);

            throw new CybersourceInternalErrorException(Response.Status.BAD_REQUEST.getStatusCode(),
                    ErrorCodeDescriptions.MSG_CODE_CYBERSOURCE_INTERNAL_ERROR + " | " + ex.getMessage(),
                    ErrorCodeType.INTERNAL_PROVIDER_ERROR.getErrorCode());
        }
    }

    /**
     *
     * @return
     */
    public CheckinCyberSourceUtil getCyberSourceUtil() {
        return cyberSourceUtil;
    }

    /**
     * @return the cybersourceService
     */
    public AMCybersourceService getCybersourceService() {
        return cybersourceService;
    }

    /**
     * @param cybersourceService the cybersourceService to set
     */
    public void setCybersourceService(AMCybersourceService cybersourceService) {
        this.cybersourceService = cybersourceService;
    }

    /**
     * @return the compBagAllowDao
     */
    public IBaggageAllowanceDao getCompBagAllowAPI() {
        return compBagAllowDao;
    }

    /**
     * @param compBagAllowAPI the compBagAllowDao to set
     */
    public void setCompBagAllowAPI(IBaggageAllowanceDao compBagAllowAPI) {
        this.compBagAllowDao = compBagAllowAPI;
    }

    /**
     * @return the setUpConfigFactory
     */
    public CheckInSetUpConfigFactoryWrapper getSetUpConfigFactory() {
        return setUpConfigFactory;
    }

    /**
     * @param setUpConfigFactory the setUpConfigFactory to set
     */
    public void setSetUpConfigFactory(CheckInSetUpConfigFactoryWrapper setUpConfigFactory) {
        this.setUpConfigFactory = setUpConfigFactory;
    }
}
