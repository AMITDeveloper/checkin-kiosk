package com.am.checkin.web.service;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.AncillariesCollectionWrapper;
import com.aeromexico.commons.model.AncillaryOfferCollection;
import com.aeromexico.commons.model.AuthorizationPaymentConfig;
import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedLegCollection;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.BookedTravelerCollection;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.CurrencyValue;
import com.aeromexico.commons.model.PNR;
import com.aeromexico.commons.model.PNRCollection;
import com.aeromexico.commons.model.chubb.ChubbCollectionWrapper;
import com.aeromexico.commons.model.chubb.PriceQuoteResponse;
import com.aeromexico.commons.model.rq.AncillaryRQ;
import com.aeromexico.commons.web.types.AncillaryPostBookingType;
import com.aeromexico.commons.web.types.CurrencyType;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.types.PosType;
import com.aeromexico.commons.web.util.FilterPassengersUtil;
import com.aeromexico.dao.client.exception.ChubbServiceUnavailableException;
import com.aeromexico.dao.client.exception.MongoDisconnectException;
import com.aeromexico.dao.config.SetUpConfigFactory;
import com.aeromexico.dao.model.AncillaryEntry;
import com.aeromexico.dao.services.AncillaryEntriesDaoCache;
import com.aeromexico.dao.services.ChubbDao;
import com.aeromexico.dao.services.IPNRLookupDao;
import com.aeromexico.sabre.api.session.exception.SabreEmptyResponseException;
import com.aeromexico.sabre.api.session.exception.SabreLayerUnavailableException;
import com.aeromexico.services.ChubbService;
import com.aeromexico.sharedservices.services.GetAncillaryOffersService;
import com.aeromexico.sharedservices.util.AncillaryUtil;
import com.aeromexico.sharedservices.util.GetAncillaryOffersUtil;
import com.aeromexico.util.ChubbUtil;
import com.am.checkin.web.event.dispatcher.SaveAncillariesCollectionDispatcher;
import com.am.checkin.web.event.model.SaveAncillariesCollectionEvent;
import com.am.checkin.web.v2.util.AncillaryCollectionUtil;
import com.am.checkin.web.v2.util.SystemVariablesUtil;
import com.google.gson.Gson;
import com.sabre.services.merch.ancillary.offer.v02.GetAncillaryOffersRQ;
import com.sabre.services.merch.ancillary.offer.v02.GetAncillaryOffersRS;
import com.sabre.services.stl_messagecommon.v02_01.CompletionCodes;
import com.sabre.webservices.pnrbuilder.AncillaryTaxPNRB;
import com.sabre.webservices.pnrbuilder.OptionalAncillaryServicesInformationDataPNRB;
import com.sabre.webservices.pnrbuilder.PricedAncillaryServicePNRB;
import org.aeromexico.commons.exception.config.ReadProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Named
@ApplicationScoped
public class AncillariesService {

    private static final Logger log = LoggerFactory.getLogger(AncillariesService.class);

    @Inject
    private IPNRLookupDao pnrLookupDao;

    @Inject
    private SaveAncillariesCollectionDispatcher saveAncillariesCollectionDispatcher;

    @Inject
    @Named("CheckInSetUpConfigFactoryWrapper")
    private SetUpConfigFactory setUpConfigFactory;

    @Inject
    private AncillaryEntriesDaoCache ancillaryEntriesDaoCache;

    @Inject
    private GetAncillaryOffersService getAncillaryOffersService;

    @Inject
    private ChubbService chubbService;

    @Inject
    private ChubbDao chubbDao;

    @Inject
    private ReadProperties prop;
    
    @PostConstruct
    public void init() {
        if (null == setUpConfigFactory) {
            setUpConfigFactory = new CheckInSetUpConfigFactoryWrapper();
        }
    }

    /**
     * @param ancillariesRQ
     * @return
     */
    public AncillaryOfferCollection getAncillaries_2_0(AncillaryRQ ancillariesRQ) {

        AncillaryOfferCollection ancillaryOfferCollection;

        try {

            String store = ancillariesRQ.getStore();
            String pos = ancillariesRQ.getPos();

            //Read the pnrCollection
            PNRCollection pnrCollection;

            try {
                pnrCollection = pnrLookupDao.readPNRCollectionByCartId(ancillariesRQ.getCartId());
                if (null != pnrCollection.getStore() && !pnrCollection.getStore().trim().isEmpty()) {
                    ancillariesRQ.setStore(pnrCollection.getStore());
                }

                if (null != pnrCollection.getPos() && !pnrCollection.getPos().trim().isEmpty()) {
                    ancillariesRQ.setPos(pnrCollection.getPos());
                }

            } catch (MongoDisconnectException me) {
                if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(ancillariesRQ.getPos())) {
                    log.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
                    throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getCode(),
                            prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getActionable(),
                            prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getMessage(),
                            Response.Status.INTERNAL_SERVER_ERROR);
                } else {
                    log.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
                    throw me;
                }
            } catch (GenericException ex) {
                log.error(ex.getMessage(), ex);
                throw ex;
            }

            PNR pnr = pnrCollection.getPNRByCartId(ancillariesRQ.getCartId());
            if (null == pnr) {
                GenericException ex = new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION, "Pnr not found");
                throw ex;
            }

            String recordLocator = pnr.getPnr();
            if (null == recordLocator || recordLocator.trim().isEmpty()) {
                GenericException ex = new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION, "Pnr not found");
                throw ex;
            }

            //just copy the record locator to be used in log
            ancillariesRQ.setRecordLocator(recordLocator);

            CartPNR cartPNR = pnr.getCartPNRByCartId(ancillariesRQ.getCartId());
            if (null == cartPNR) {
                GenericException ex = new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION, "Cart not found");
                throw ex;
            }

            if (cartPNR.isSeamlessCheckin()){
                ancillaryOfferCollection = AncillaryCollectionUtil.getAncillaryOfferCollectionEmpty();
                return ancillaryOfferCollection;
            }


            String legCode = cartPNR.getLegCode();
            BookedLeg bookedLeg = pnr.getBookedLegByLegCode(legCode);
            if (null == bookedLeg) {
                GenericException ex = new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION, "BookedLeg not found");
                throw ex;
            }

            List<BookedTraveler> bookedTravelerRequestedForCheckinList = FilterPassengersUtil.getMarkedForCheckin(cartPNR.getTravelerInfo().getCollection());

            List<AncillaryEntry> ancillariesEntryList;
            try {
                ancillariesEntryList = AncillaryUtil.filterAncillariesForRegion( ancillaryEntriesDaoCache.getCheckinAncillaryEntries(), pnr.getGlobalMarketFlight() );

                if (pnr.getLegs().isMultiCity() && pnr.getLegs().hasNonUsLeg()) {
                    Iterator<AncillaryEntry> it = ancillariesEntryList.iterator();

                    while (it.hasNext()) {
                        AncillaryEntry ancillaryEntry = (AncillaryEntry) it.next();

                        if (AncillaryPostBookingType._0C3.getRficSubCode().equalsIgnoreCase(ancillaryEntry.getRfiSubCode())
                                || AncillaryPostBookingType._0JT.getRficSubCode().equalsIgnoreCase(ancillaryEntry.getRfiSubCode())
                                || AncillaryPostBookingType._0J0.getRficSubCode().equalsIgnoreCase(ancillaryEntry.getRfiSubCode())) {
                            it.remove();
                        }
                    }
                }
            } catch (MongoDisconnectException me) {
                if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(ancillariesRQ.getPos())) {
                    log.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
                } else {
                    log.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
                }
                ancillariesEntryList = new ArrayList<>();
            } catch (Exception ex) {
                ancillariesEntryList = new ArrayList<>();
                log.error("ManageAncillaryService.manageAncillary() => Error getAncillariesOfBooking(): " + ex.getMessage());
            }

            String cityCodeCommand;
            String cityCode;
            AuthorizationPaymentConfig authorizationPaymentConfig;
            authorizationPaymentConfig = setUpConfigFactory.getInstance().getAuthorizationPaymentConfig(ancillariesRQ.getStore(), ancillariesRQ.getPos());
            cityCode = authorizationPaymentConfig.getCityCode();
            cityCodeCommand = setUpConfigFactory.getInstance().getCityCodeCommand();

            log.info("City code: {} command: {}" + cityCode, cityCodeCommand);
            
            if (ancillariesRQ.getPos().equalsIgnoreCase(PosType.KIOSK.getCode()) || ancillariesRQ.getPos().equalsIgnoreCase(PosType.CHECKIN_MOBILE.toString())) {
                GetAncillaryOffersUtil.excludeIncludedBags( bookedTravelerRequestedForCheckinList, ancillariesEntryList, bookedLeg );
            }

            if (PosType.KIOSK.toString().equalsIgnoreCase(ancillariesRQ.getPos())) {
                GetAncillaryOffersUtil.excludeIncludedBags( bookedTravelerRequestedForCheckinList, ancillariesEntryList, bookedLeg );
            }

            GetAncillaryOffersRQ getAncillaryOffersRQ = GetAncillaryOffersUtil.getAncillaryOffersRQ(
                    pnr.getLegs(),
                    bookedTravelerRequestedForCheckinList,
                    ancillariesEntryList,
                    authorizationPaymentConfig,
                    com.sabre.services.merch.request.v02.RequestType.PAYLOAD,
                    "CHECKIN",
                    "AM",
                    true
            );

            log.info(new Gson().toJson(getAncillaryOffersRQ, GetAncillaryOffersRQ.class));

            GetAncillaryOffersRS getAncillaryOffersRS = getAncillaryOffersService.getAncillaryOffersRS(getAncillaryOffersRQ, cityCode, cityCodeCommand);

            if (getAncillaryOffersRS != null && getAncillaryOffersRS.getApplicationResults() != null && CompletionCodes.COMPLETE.equals(getAncillaryOffersRS.getApplicationResults().getStatus())) {

                log.info(new Gson().toJson(getAncillaryOffersRS, GetAncillaryOffersRS.class));

                ancillaryOfferCollection = AncillaryUtil.transformAncillaryRS_2_0(
                        getAncillaryOffersRS,
                        pnrCollection,
                        getAncillaryOffersRQ,
                        ancillariesRQ.getCartId(),
                        ancillariesRQ.getPos(),
                        ancillariesRQ.getStore()
                );

                if (ancillaryOfferCollection != null) {
                    //Call Chubb Service
                    if (ChubbUtil.hasChubbAncillaries(ancillaryOfferCollection)
                            && ChubbUtil.isCandidateForChubb(store, pos, pnr.getFlightItineraryType())) {
                        try {
                            PriceQuoteResponse priceQuoteResponse = getPriceQuoteResponse(
                                    cartPNR.getTravelerInfo(),
                                    pnr.getLegs(),
                                    store,
                                    authorizationPaymentConfig.getCurrency(),
                                    pnr.getPnr(),
                                    authorizationPaymentConfig.getStationNumber(),
                                    ancillariesRQ.getCartId(),
                                    false
                            );

                            ChubbUtil.replaceAncillariesWithChubbCheckin(ancillaryOfferCollection, priceQuoteResponse, pos);

                        } catch (Exception ex) {
                            log.error(ex.getMessage(), ex);
                        }
                    }
                    
                    AncillariesCollectionWrapper ancillariesCollectionWrapper = new AncillariesCollectionWrapper();
                    ancillariesCollectionWrapper.setCartId(ancillariesRQ.getCartId());
                    ancillariesCollectionWrapper.setAncillaryOfferCollection(ancillaryOfferCollection);

                    saveAncillariesCollection(ancillariesCollectionWrapper);

                    return ancillaryOfferCollection;
                }
            }
        } catch (SabreEmptyResponseException ser) {
            log.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110302).toString(), ser.fillInStackTrace());
        } catch (SabreLayerUnavailableException ex) {
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(ancillariesRQ.getPos())) {
                log.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_22110301,"AncillariesService.java:271", ex.getMessage());
            } else {
                log.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_12110301,"AncillariesService.java:271", ex.getMessage());
            }
        } catch (MongoDisconnectException me) {
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(ancillariesRQ.getPos())) {
                log.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
            } else {
                log.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
            }
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }

        ancillaryOfferCollection = AncillaryCollectionUtil.getAncillaryOfferCollectionEmpty();


        return ancillaryOfferCollection;
    }

    public PriceQuoteResponse getPriceQuoteResponse(
            BookedTravelerCollection bookedTravelerCollection,
            BookedLegCollection bookedLegCollection,
            String store,
            String currency,
            String bookingNumber,
            String pcc,
            String cartId,
            boolean searchInDB
    ) throws Exception {

        try {
            if (searchInDB) {
                ChubbCollectionWrapper chubbCollectionWrapper = chubbDao.getPoliciesQuoteChubbCollectionByCartId(cartId);
                if (chubbCollectionWrapper != null && chubbCollectionWrapper.getPriceQuoteResponse() != null) {
                    return chubbCollectionWrapper.getPriceQuoteResponse();
                }
            }

            PriceQuoteResponse priceQuoteResponse = chubbService.callQuotePolicy(
                    bookedTravelerCollection, bookedLegCollection, store, currency, bookingNumber, pcc,
                    SystemVariablesUtil.getChubbAgentName(),
                    SystemVariablesUtil.getChubbAgentCode(),
                    SystemVariablesUtil.getChubbAgentInternalCode()
            );

            ChubbCollectionWrapper chubbCollectionWrapper = new ChubbCollectionWrapper();
            chubbCollectionWrapper.setCartId(cartId);
            chubbCollectionWrapper.setPriceQuoteResponse(priceQuoteResponse);
            chubbDao.savePoliciesQuoteChubbCollection(chubbCollectionWrapper);

            return priceQuoteResponse;
        } catch (MongoDisconnectException mde) {
            log.error(mde.getMessage(), mde);
            throw mde;
        } catch (ChubbServiceUnavailableException csu) {
            log.error(ChubbUtil.buildLogChubb(csu, this.getClass().getName()));
            throw csu;
        } catch (Exception ex) {
            log.error("Error get price quote CHUBB: " + ex.getMessage());
            throw ex;
        }
    }

    /**
     * @param ancillariesCollectionWrapper
     */
    private void saveAncillariesCollection(AncillariesCollectionWrapper ancillariesCollectionWrapper) {
        saveAncillariesCollection(ancillariesCollectionWrapper, false);
    }

    /**
     * This method is used to update the PNR collection MongoDB in an
     * asynchronous mode
     *
     * @param ancillariesCollectionWrapper
     * @param mustBeAsynchronous
     */
    private void saveAncillariesCollection(AncillariesCollectionWrapper ancillariesCollectionWrapper, boolean mustBeAsynchronous) {
        if (mustBeAsynchronous) {
            try {
                SaveAncillariesCollectionEvent saveAncillariesCollectionEvent = new SaveAncillariesCollectionEvent(ancillariesCollectionWrapper);
                saveAncillariesCollectionDispatcher.publish(saveAncillariesCollectionEvent);
            } catch (Exception ex) {
                log.error(ex.getMessage());
                //ignore
            }
        } else {
            try {
                SaveAncillariesCollectionEvent saveAncillariesCollectionEvent = new SaveAncillariesCollectionEvent(ancillariesCollectionWrapper);
                int code = pnrLookupDao.saveAncillariesCollection(saveAncillariesCollectionEvent.getAncillariesCollectionWrapper());

                log.info("Saving ancillaries to DB, code:" + code);
            } catch (Exception ex) {
                log.info("Error saving ancillaries to data base: " + ex.getMessage());
            }
        }
    }

    protected CurrencyValue getCurrencyValue(
            PricedAncillaryServicePNRB pricedAncillaryServicePNRB
    ) {

        CurrencyValue currencyValue = new CurrencyValue();

        try {
            OptionalAncillaryServicesInformationDataPNRB ancillaryData = pricedAncillaryServicePNRB.getOptionalAncillaryServiceInformation().get(0);

            String currencyCode = ancillaryData.getTTLPrice().getCurrency();
            currencyValue.setCurrencyCode(currencyCode);

            currencyValue.setTotal(ancillaryData.getTTLPrice().getPrice());

            if (null != ancillaryData.getOriginalBasePrice()
                    && currencyCode.equalsIgnoreCase(ancillaryData.getOriginalBasePrice().getCurrency())) {
                currencyValue.setBase(ancillaryData.getOriginalBasePrice().getPrice());

                if (null != ancillaryData.getEquivalentPrice()) {
                    currencyValue.setEquivalent(ancillaryData.getEquivalentPrice().getPrice());
                    currencyValue.setEquivalentCurrencyCode(ancillaryData.getEquivalentPrice().getCurrency());
                }

            } else if (null != ancillaryData.getEquivalentPrice()
                    && currencyCode.equalsIgnoreCase(ancillaryData.getEquivalentPrice().getCurrency())) {

                currencyValue.setBase(ancillaryData.getEquivalentPrice().getPrice());

                if (null != ancillaryData.getOriginalBasePrice()) {
                    currencyValue.setEquivalent(ancillaryData.getOriginalBasePrice().getPrice());
                    currencyValue.setEquivalentCurrencyCode(ancillaryData.getOriginalBasePrice().getCurrency());
                }
            }

            try {
                currencyValue.setTaxDetails(getTaxDetails(ancillaryData.getTaxes()));
            } catch (Exception e) {
                currencyValue.setTaxDetails(new HashMap<>());
            }

            try {
                currencyValue.setTotalTax(getTotalTax(ancillaryData.getTaxes()));
            } catch (Exception e) {
                currencyValue.setTotalTax(BigDecimal.ZERO);
            }

        } catch (Exception e) {
            currencyValue = new CurrencyValue();
        }

        /**
         * * THIS IS TEMPORARY **
         */
        if (CurrencyType.MXN.toString().equalsIgnoreCase(currencyValue.getCurrencyCode()) && "CXI".equalsIgnoreCase(pricedAncillaryServicePNRB.getRficSubcode())) {
            currencyValue.setTotal(BigDecimal.valueOf(350));
            currencyValue.setBase(BigDecimal.valueOf(300));
            currencyValue.setTotalTax(BigDecimal.valueOf(50));
        }

        return currencyValue;
    }

    private Map<String, BigDecimal> getTaxDetails(com.sabre.webservices.pnrbuilder.AncillaryServicesUpdatePNRB.Taxes taxes) {
        Map<String, BigDecimal> mapTaxDetails = new LinkedHashMap<>();
        if (taxes != null) {
            for (AncillaryTaxPNRB ancillaryTaxPNRB : taxes.getTax()) {
                try {
                    mapTaxDetails.put(ancillaryTaxPNRB.getTaxCode(), ancillaryTaxPNRB.getTaxAmount());
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }
        }

        return mapTaxDetails;
    }

    private BigDecimal getTotalTax(com.sabre.webservices.pnrbuilder.AncillaryServicesUpdatePNRB.Taxes taxes) {
        BigDecimal totalTax = BigDecimal.ZERO;
        if (taxes != null) {
            for (AncillaryTaxPNRB ancillaryTaxPNRB : taxes.getTax()) {
                try {
                    totalTax = totalTax.add(ancillaryTaxPNRB.getTaxAmount());
                } catch (Exception e) {
                    //ignore
                }
            }
        }

        return totalTax;
    }

    public IPNRLookupDao getPnrLookupDao() {
        return pnrLookupDao;
    }

    public void setPnrLookupDao(IPNRLookupDao pnrLookupDao) {
        this.pnrLookupDao = pnrLookupDao;
    }

    /**
     * @return the saveAncillariesCollectionDispatcher
     */
    public SaveAncillariesCollectionDispatcher getSaveAncillariesCollectionDispatcher() {
        return saveAncillariesCollectionDispatcher;
    }

    /**
     * @param saveAncillariesCollectionDispatcher the
     *                                            saveAncillariesCollectionDispatcher to set
     */
    public void setSaveAncillariesCollectionDispatcher(SaveAncillariesCollectionDispatcher saveAncillariesCollectionDispatcher) {
        this.saveAncillariesCollectionDispatcher = saveAncillariesCollectionDispatcher;
    }

    /**
     * @return the setUpConfigFactory
     */
    public SetUpConfigFactory getSetUpConfigFactory() {
        if (null == setUpConfigFactory) {
            setUpConfigFactory = new CheckInSetUpConfigFactoryWrapper();
        }
        return setUpConfigFactory;
    }

    /**
     * @param setUpConfigFactory the setUpConfigFactory to set
     */
    public void setSetUpConfigFactory(SetUpConfigFactory setUpConfigFactory) {
        this.setUpConfigFactory = setUpConfigFactory;
    }

    /**
     * @return the ancillaryEntriesDaoCache
     */
    public AncillaryEntriesDaoCache getAncillaryEntriesDaoCache() {
        return ancillaryEntriesDaoCache;
    }

    /**
     * @param ancillaryEntriesDaoCache the ancillaryEntriesDaoCache to set
     */
    public void setAncillaryEntriesDaoCache(AncillaryEntriesDaoCache ancillaryEntriesDaoCache) {
        this.ancillaryEntriesDaoCache = ancillaryEntriesDaoCache;
    }
}
