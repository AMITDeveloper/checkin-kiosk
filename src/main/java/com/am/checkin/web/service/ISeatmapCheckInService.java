package com.am.checkin.web.service;


import com.aeromexico.commons.model.SeatmapCollection;
import com.aeromexico.commons.model.rq.SeatmapCheckInRQ;

public interface ISeatmapCheckInService {

    /**
     * 
     * @param seatmapRQ
     * @return
     * @throws Exception
     */
    public SeatmapCollection getSeatMaps(SeatmapCheckInRQ seatmapRQ) throws Exception;
}
