
package com.am.checkin.web.service;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.aeromexico.sabre.api.session.exception.SabreEmptyResponseException;
import com.aeromexico.sabre.api.session.exception.SabreLayerUnavailableException;
import org.aeromexico.commons.exception.config.ReadProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.aeromexico.commons.model.Flight;
import com.aeromexico.commons.model.FlightCollection;
import com.aeromexico.commons.model.rq.FlightListRQ;
import com.aeromexico.sabre.api.sabrecommand.AMSabreCommandService;
import com.sabre.webservices.sabrexml._2003._07.SabreCommandLLSRS;

@Named
@ApplicationScoped
public class FlightListService {

	private static final Logger LOG = LoggerFactory.getLogger(FlightListService.class);

	@Inject
	AMSabreCommandService sabreCommandService;

	@Inject
	private ReadProperties prop;

	public FlightCollection getFlightList( FlightListRQ flightListRQ) {

		if ( flightListRQ.gerRequestType() == FlightListRQ.RequestType.ARRIVAL) {
			return getArrivalFlightList(flightListRQ);
		} else {
			return getDepartureFlightList(flightListRQ);
		}
	}

	private FlightCollection getArrivalFlightList( FlightListRQ flightListRQ) {

		SabreCommandLLSRS sabreCommandLLSRS = null;

		LOG.info( "Host Command: " + flightListRQ.gerHostCommand() );

		try {
			sabreCommandLLSRS = sabreCommandService.sabreCommand( flightListRQ.gerHostCommand(), "SDSXML", null);
		}catch (SabreLayerUnavailableException se){
			throw se;
		}catch (SabreEmptyResponseException ser) {
			LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110302).toString(), ser.fillInStackTrace());
			throw ser;
		} catch (Exception e) {
			LOG.error( e.getMessage() );
		}

		FlightCollection flightCollection = new FlightCollection();

		if ( sabreCommandLLSRS != null ) {
			if ( sabreCommandLLSRS.getXMLContent() != null ) {
				//We got a good response 
				parseArrivalData( sabreCommandLLSRS.getXMLContent().getAny(), flightCollection.getCollection());
			} else {
				//We got a bad response
				LOG.error( "Host Response: " + sabreCommandLLSRS.getResponse() );
			}
		}

		return flightCollection;
	}

	private FlightCollection getDepartureFlightList( FlightListRQ flightListRQ ) {

		SabreCommandLLSRS sabreCommandLLSRS = null;

		LOG.info( "Host Command: " + flightListRQ.gerHostCommand() );

		try {
			sabreCommandLLSRS = sabreCommandService.sabreCommand( flightListRQ.gerHostCommand(), "SDSXML", null);
		}catch (SabreLayerUnavailableException se){
			throw se;
		}catch (SabreEmptyResponseException ser) {
			LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110302).toString(), ser.fillInStackTrace());
			throw ser;
		} catch (Exception e) {
			LOG.error( e.getMessage() );
		}

		FlightCollection flightCollection = new FlightCollection();

		if ( sabreCommandLLSRS != null ) {
			if ( sabreCommandLLSRS.getXMLContent() != null ) {
				//We got a good response 
				parseDepartureData( sabreCommandLLSRS.getXMLContent().getAny(), flightCollection.getCollection());
			} else {
				//We got a bad response
				LOG.error( "Host Response: " + sabreCommandLLSRS.getResponse() );
			}
		}

		return flightCollection;
	}

	private String getNodeValue( Element e, String name) {

    	NodeList nodeList = e.getElementsByTagName(name);
		if ( nodeList != null && nodeList.getLength() > 0 ) {
			Node node = nodeList.item(0);
			if ( node != null && node.hasChildNodes() ) {
				return node.getFirstChild().getNodeValue();	
			}
		}

		return null;
    }

	private void parseDepartureData(List<Element> elements, List<Flight> flightList) {

    	Flight flight = null;

    	try {

			// loop through each item
			NodeList items = elements.get(0).getChildNodes();

			for (int i = 0; i < items.getLength(); i++) {

				Node n = items.item(i);

				if (n.getNodeType() != Node.ELEMENT_NODE)
					continue;

				Element e = (Element) n;

				if ( "SDE0X4".equalsIgnoreCase( e.getTagName() ) ) {
					continue;
				}

				if ( "SDE0X2".equalsIgnoreCase( e.getTagName() ) ) {
					continue;
				}

				flight = new Flight();
				flightList.add( flight );

				//<SDE0X5>
				if ( "SDE0X5".equalsIgnoreCase( e.getTagName() ) ) {

					//airline code
					flight.setCode( getNodeValue(e, "SDE0X500Q9") );

					//Flight number
					flight.setNumber( getNodeValue(e, "outboundFlightNumber") );

					//departure date
					flight.setDate( getNodeValue(e, "SDE0X500EA") );

					//terminal
					flight.setTerminal( getNodeValue(e, "airportTerminalOfDeparture") );

					//gate
					flight.setGate( getNodeValue(e, "gateIdentifier") );

					//status
					flight.setStatus( getNodeValue(e, "SDE0X500C1") );

					//departureTime
					flight.setDepartureTime( getNodeValue(e, "departureTime") );

					//Destination
					flight.setDestination( getNodeValue(e, "SDE0X501AE") );

					//Final Destination
					flight.setFinalDestination( getNodeValue(e, "SDE0X500V9") );

					//indicator
					flight.setIndicator( getNodeValue(e, "indicator") );

					//nameFieldRemarks
					flight.setRemarks( getNodeValue(e, "nameFieldRemarks") );
					
					//aircraftTypeCode
					flight.setAircraftType( getNodeValue(e, "aircraftTypeCode") );
				}

				i++;
				n = items.item(i);
				e = (Element)n;

				//<SDE0X6>
				if ( "SDE0X6".equalsIgnoreCase( e.getTagName() ) ) {
					//freeText - load
					flight.setLoad( getNodeValue(e, "freeText") );

					//departureTime
					flight.setScheduledDepartureTime( getNodeValue(e, "departureTime") );
				}
			}

		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
    }

	private void parseArrivalData(List<Element> elements, List<Flight> flightList) {

    	Flight flight = null;

    	try {

			// loop through each item
			NodeList items = elements.get(0).getChildNodes();

			for (int i = 0; i < items.getLength(); i++) {

				Node n = items.item(i);

				if (n.getNodeType() != Node.ELEMENT_NODE)
					continue;

				Element e = (Element) n;

				if ( "SDE0Y2".equalsIgnoreCase( e.getTagName() ) ) {
					continue;
				}

				if ( "SDE0Y4".equalsIgnoreCase( e.getTagName() ) ) {
					continue;
				}

				flight = new Flight();
				flightList.add( flight );

				//<SDE0Y5>
				if ( "SDE0Y5".equalsIgnoreCase( e.getTagName() ) ) {

					//airline code
					flight.setCode( getNodeValue(e, "SDE0Y500Q9") );

					//Flight number
					flight.setNumber( getNodeValue(e, "outboundFlightNumber") );

					//departure date
					flight.setDate( getNodeValue(e, "SDE0Y500EA") );

					//terminal
					flight.setTerminal( getNodeValue(e, "terminalofarrival") );

					//gate
					flight.setGate( getNodeValue(e, "actualarrivalGateIdentifier") );
					
					//freeText - load
					flight.setLoad( getNodeValue(e, "freeText") );

					//SDE0Y500L1
					flight.setOrigin( getNodeValue(e, "SDE0Y500L1") );

					//scheduledarrivalTime
					flight.setScheduledArrivalTime( getNodeValue(e, "scheduledarrivalTime") );

					//estimatedarrivalTime
					flight.setEstimatedArrivalTime( getNodeValue(e, "estimatedarrivalTime") );
				}
			}

		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
    }

}
