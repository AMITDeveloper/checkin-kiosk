package com.am.checkin.web.service;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.util.Constants;
import com.aeromexico.sabre.api.sessionpool.factory.SabreSession;
import com.aeromexico.sabre.sabreapi.contextchange.AMContextChangeService;
import com.sabre.services.stl.v01.ProblemInformation;
import com.sabre.services.stl.v01.SystemSpecificResults;
import com.sabre.services.stl_header.v120.CompletionCodes;
import com.sabre.services.stl_header.v120.MessageCondition;
import com.sabre.webservices.sabrexml._2011._10.ContextChangeRQ;
import com.sabre.webservices.sabrexml._2011._10.ContextChangeRS;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adrian
 */
@Named
@ApplicationScoped
public class ChangeContexService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ChangeContexService.class);

    private static final String VERSION = "2.0.3";

    @Inject
    private AMContextChangeService contextChangeService;

    public ContextChangeRS changeContext(SabreSession sabreSession, ContextChangeRQ contextChangeRQ) throws Exception {

        ContextChangeRS contextChangeRS = contextChangeService.contextChange(sabreSession, contextChangeRQ);

        return contextChangeRS;
    }

    public static ContextChangeRQ createContextChangeAaaRQ(String aaa, String pcc, String accountingCode, String officeStationCode) {
        ContextChangeRQ contextChangeRQ = new ContextChangeRQ();

        contextChangeRQ.setVersion(VERSION);

        ContextChangeRQ.ChangeAAA changeAAA = new ContextChangeRQ.ChangeAAA();

        changeAAA.setAccountingCity(aaa);
        changeAAA.setAccountingCode(accountingCode);
        changeAAA.setOfficeStationCode(officeStationCode);
        changeAAA.setPseudoCityCode(aaa);

        contextChangeRQ.setChangeAAA(changeAAA);

        return contextChangeRQ;
    }

    public static ContextChangeRQ createContextChangeOverSignFirstStepRQ(String username) {
        ContextChangeRQ contextChangeRQ = new ContextChangeRQ();

        contextChangeRQ.setVersion(VERSION);

        ContextChangeRQ.OverSign overSign = new ContextChangeRQ.OverSign();

        overSign.setOrganization(Constants.PARTITION);
        overSign.setUsername(username);

        contextChangeRQ.setOverSign(overSign);

        return contextChangeRQ;
    }

    public static ContextChangeRQ createContextChangeOverSignSecondStepRQ(String dutyCode, String username, String password) {
        ContextChangeRQ contextChangeRQ = new ContextChangeRQ();

        contextChangeRQ.setVersion(VERSION);

        ContextChangeRQ.OverSign overSign = new ContextChangeRQ.OverSign();

        overSign.setArea(Constants.AREA);
        overSign.setDutyCode(dutyCode);
        overSign.setOrganization(Constants.PARTITION);
        overSign.setPassword(password);
        overSign.setUsername(username);

        contextChangeRQ.setOverSign(overSign);

        return contextChangeRQ;
    }

    public static ContextChangeRQ createContextChangeDutyRQ(String dutyCode) {
        ContextChangeRQ contextChangeRQ = new ContextChangeRQ();

        contextChangeRQ.setVersion(VERSION);

        ContextChangeRQ.ChangeDuty changeDuty = new ContextChangeRQ.ChangeDuty();
        changeDuty.setCode(dutyCode);

        contextChangeRQ.setChangeDuty(changeDuty);

        return contextChangeRQ;
    }

    public static ContextChangeRQ createContextChangePasswordRQ(String partitionCode, String pin, String currentPassword, String newPassword) {
        ContextChangeRQ contextChangeRQ = new ContextChangeRQ();

        contextChangeRQ.setVersion(VERSION);

        ContextChangeRQ.ChangePartition changePartition = new ContextChangeRQ.ChangePartition();

        changePartition.setCode(partitionCode);
        changePartition.setNewPassword(newPassword);
        changePartition.setPassword(currentPassword);
        changePartition.setPIN(pin);

        contextChangeRQ.setChangePartition(changePartition);

        return contextChangeRQ;
    }

    public static void validateContextChangeResponse(ContextChangeRS contextChangeRS) {
        if (!CompletionCodes.COMPLETE.equals(contextChangeRS.getApplicationResults().getStatus())) {
            StringBuilder sb = new StringBuilder();

            if (null != contextChangeRS.getApplicationResults().getError()) {
                for (ProblemInformation problemInformation : contextChangeRS.getApplicationResults().getError()) {
                    if (null != problemInformation.getSystemSpecificResults()) {
                        for (SystemSpecificResults systemSpecificResult : problemInformation.getSystemSpecificResults()) {
                            for (MessageCondition messageCondition : systemSpecificResult.getMessage()) {
                                sb.append(messageCondition.getValue()).append("|");
                            }

                            sb.append(systemSpecificResult.getShortText());
                        }
                    }
                }
            }

            String msg = sb.toString();

            if (StringUtils.containsIgnoreCase(msg, "YOUR PASSCODE MUST BE CHANGED")) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.YOUR_PASSCODE_MUST_BE_CHANGED, msg);
            } else if (StringUtils.containsIgnoreCase(msg, "FORMAT")) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.WRONG_FORMAT_WITH_CONTEXT_CHANGE, msg);
            } else {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.SOMETHING_WRONG_WITH_CONTEXT_CHANGE, msg);
            }
        } else {
            if (null == contextChangeRS.getApplicationResults().getSuccess() || contextChangeRS.getApplicationResults().getSuccess().isEmpty()) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.SOMETHING_WRONG_WITH_CONTEXT_CHANGE);
            }
        }
    }
}
