package com.am.checkin.web.service;

import com.aeromexico.commons.model.AbstractSegmentChoice;
import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.PNR;
import com.aeromexico.commons.model.PNRCollection;
import com.aeromexico.commons.model.SegmentDocument;
import com.aeromexico.commons.model.utils.DateTool;
import com.am.checkin.web.v2.util.SystemVariablesUtil;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import org.apache.commons.io.FileUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.runtime.RuntimeConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xhtmlrenderer.swing.Java2DRenderer;
import org.xhtmlrenderer.util.FSImageWriter;

@Named
@ApplicationScoped
public class MobileBoardingPassService {

    private static final Logger logger = LoggerFactory.getLogger(MobileBoardingPassService.class);

    private static Template template;

    public MobileBoardingPassService() {
    }

    @PostConstruct
    public void init() {
        Properties props = new Properties();
        props.put(RuntimeConstants.RESOURCE_LOADER, "classpath");
        props.put("classpath.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        props.put("velocimacro.library.autoreload", "true");
        props.put("velocimacro.permissions.allow.inline.to.replace.global", "true");
        props.put("classpath.resource.loader.cache", "false");
        props.put(RuntimeConstants.RUNTIME_LOG_LOGSYSTEM_CLASS, "org.apache.velocity.runtime.log.SystemLogChute");
        Velocity.init(props);

        template = Velocity.getTemplate("mobileTemplates/PNGBoardingPass.vm");
    }

    public ArrayList<byte[]> getMobileBoardingPass(PNRCollection pnrCollection) {
        ArrayList<byte[]> mobileBoardingPasses = new ArrayList<>();
        try {
            for (PNR pnr : pnrCollection.getCollection()) {
                for (CartPNR cartPnr : pnr.getCarts().getCollection()) {
                    BookedLeg bookedLeg = pnr.getLegs().getBookedLegByLegCode(cartPnr.getLegCode());
                    if (bookedLeg != null) {
                        int bookedTravelerIndex = 0;
                        for (BookedTraveler bookedTraveler : cartPnr.getTravelerInfo().getCollection()) {
                            if (bookedTraveler.isCheckinStatus()) {

                                int segmentDocumentIndex = 0;
                                for (SegmentDocument segmentDocument : bookedTraveler.getSegmentDocumentsList()) {
                                    if (segmentDocument.getQrcodeImage() != null) {
                                        saveImageInFile(SystemVariablesUtil.getMobileBoardingPassTmpPath(), segmentDocument.getQrcodeImage(), "codigoQR" + pnr.getPnr() + segmentDocumentIndex + bookedTravelerIndex + ".png");
                                        if (bookedTraveler.getInfant() != null) {
                                            for (int j = 0; j < bookedTraveler.getInfant().getSegmentDocumentsList().size(); j++) {
                                                saveImageInFile(SystemVariablesUtil.getMobileBoardingPassTmpPath(), bookedTraveler.getInfant().getSegmentDocumentsList()
                                                        .get(j).getQrcodeImage(), "codigoQR" + pnr.getPnr() + segmentDocumentIndex + bookedTravelerIndex + "infant.png");
                                            }
                                        }
                                    }
                                    segmentDocumentIndex++;
                                }

                                if (bookedLeg.getSegments().getLegCode().equals(cartPnr.getLegCode())) {
                                    int bookedSegmentIndex = 0;
                                    for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {
                                        try {
                                            byte[] mbp = createMobilePass(cartPnr, template, bookedTraveler,
                                                    bookedTravelerIndex, pnr, bookedLeg, bookedSegment,
                                                    bookedSegmentIndex, false);
                                            mobileBoardingPasses.add(mbp);
                                            if (bookedTraveler.getInfant() != null) {
                                                byte[] mbpi = createMobilePass(cartPnr, template, bookedTraveler,
                                                        bookedTravelerIndex, pnr, bookedLeg, bookedSegment,
                                                        bookedSegmentIndex, true);
                                                mobileBoardingPasses.add(mbpi);
                                            }
                                        } catch (Exception ex) {
                                            logger.error(ex.getMessage(), ex);
                                        }
                                        bookedSegmentIndex++;
                                    }
                                }
                            }
                            bookedTravelerIndex++;
                        }
                    }
                }
            }
        } catch (Exception ex) {
            logger.error("Failed MBP", ex);
        }
        return mobileBoardingPasses;
    }

    private byte[] createMobilePass(CartPNR cartPNR, Template template, BookedTraveler bookedTraveler, int bookedTravelerIndex,
            PNR pnr, BookedLeg documentedLeg, BookedSegment bookedSegment, int bookedSegmentIndex, boolean isInfant) throws IOException, FontFormatException {
        
        String pathMobile = "";
    	pathMobile = getTextProperties();
                
        VelocityContext context = new VelocityContext();
        context.put("pnr", pnr.getPnr());
        context.put("lnPNGTitle", "Confirmaci&oacute;n de Vuelo PNG");
        context.put("lnRunBy", "Operado por Aerom&eacute;xico");
        context.put("lnNoDisponible", "-");
        context.put("lnTbScheTit1", "SALIDA");
        context.put("lnTbDetGateO", "SALA");
        context.put("lnTbDetZoneO", "ZONA");
        context.put("lnTbDetSeatO", "ASIENTO");
        context.put("lnNoDisponible", "-");
        context.put("lnAsientoInf", "Inf");
        context.put("lnSelecteePax1", "Este no es un pase de abordar");
        context.put("lnSelecteePax2", "Acude al mostrador al llegar al aeropuerto para solicitar tu pase de abordar");
        context.put("lnClassName", "Clase");
        context.put("lnControlName", "Control");
        context.put("lnPassenger", "PASAJERO");
        context.put("dateTool", new DateTool("ES"));
        context.put("isSelectee", bookedTraveler.isSelectee());
        context.put("departureTerminal", documentedLeg.getBoardingTerminal());
        context.put("whichSegment", bookedSegmentIndex);
        context.put("booTraveler", bookedTraveler);
        context.put("segment", bookedSegment);
        context.put("pathCode", "/opt/am/mail/");
        context.put("barCodePath", "/opt/am/tmp/");
        context.put("path", pathMobile);
        context.put("number", bookedTravelerIndex);
        context.put("infant", isInfant);
        context.put("bookedTravelerSegmentList", bookedTraveler.getSegmentDocumentsList());
        context.put("booTravelerBaggageList", bookedTraveler.getBaggageRouteList());

        ArrayList<AbstractSegmentChoice> seats = new ArrayList<>();
        seats.addAll(cartPNR.getTravelerInfo().getCollection().get(bookedTravelerIndex).getSegmentChoices().getCollection());
        context.put("seats", seats);

        StringWriter writer = new StringWriter();
        template.merge(context, writer);
        String resultHtml = writer.toString();
        File tmpFile = new File(SystemVariablesUtil.getMobileBoardingPassTmpPath() + pnr.getPnr());
        FileUtils.writeStringToFile(tmpFile, resultHtml, Charset.defaultCharset());

        Java2DRenderer renderer = new Java2DRenderer(tmpFile, 750, 1334);

        Font font = Font.createFont(Font.TRUETYPE_FONT, MobileBoardingPassService.class.getResourceAsStream("/mobileTemplates/Helvetica.otf"));
        renderer.getSharedContext().setFontMapping("Helvetica", font);
        renderer.getSharedContext().setFontMapping("Arial", font);
        renderer.getSharedContext().setFontMapping("Calibri", font);

        BufferedImage image = renderer.getImage();
        FSImageWriter imageWriter = new FSImageWriter();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        imageWriter.write(image, baos);
        return baos.toByteArray();
    }

    public void saveImageInFile(String path, String imagenBase64, String fileName) {
        File imageFile = new File(path + fileName);
        byte[] data = Base64.getDecoder().decode(imagenBase64);

        try (FileOutputStream fos = new FileOutputStream(imageFile)) {
            fos.write(data);
            fos.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public String getTextProperties() {
    	String text = "";
    	try (InputStream input = MobileBoardingPassService.class.getClassLoader().getResourceAsStream("mobilegeneral.properties")) {

            Properties prop = new Properties();

            if (input == null) {
            	logger.error("Sorry, unable to find mobilegeneral.properties");
                return "";
            }
            
            prop.load(input);
            
            text = prop.getProperty("mobile.mobilepath");
            
            logger.info("properties_text: " + text);
            
        } catch (IOException ex) {
        	logger.error("Error MobileBoardingPassService:getTextProperties ", ex.getMessage());
        }
    	return text;
    }

}
