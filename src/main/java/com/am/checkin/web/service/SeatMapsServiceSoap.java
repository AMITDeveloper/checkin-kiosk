package com.am.checkin.web.service;

import com.aeromexico.commons.exception.model.GenericException;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.aeromexico.commons.model.*;
import com.aeromexico.commons.model.rq.SeatMapsRQ;
import com.aeromexico.commons.web.types.*;
import com.aeromexico.sabre.api.session.exception.SabreLayerUnavailableException;
import com.am.checkin.web.v2.util.SeatmapCollectionUtil;
import com.aeromexico.sabre.api.session.exception.SabreEmptyResponseException;
import com.am.checkin.web.v2.util.SystemVariablesUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeromexico.commons.model.rq.SeatmapCheckInRQ;
import com.aeromexico.commons.model.utils.CurrencyUtil;
import com.aeromexico.commons.seatMapSOAP.service.SeatMapCheckInSoapService;
import com.aeromexico.commons.seatMapSOAP.service.commons.SeatMapCommon;
import com.aeromexico.commons.seatmaps.util.SeatMapUtil;
import com.aeromexico.dao.client.exception.MongoDisconnectException;
import com.aeromexico.dao.dispatcher.SaveSeatmapCollectionDispatcher;
import com.aeromexico.dao.event.SaveSeatmapCollectionEvent;
import com.aeromexico.dao.services.IPNRLookupDao;
import com.aeromexico.dao.util.CommonUtil;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.core.Response;

import org.aeromexico.commons.exception.config.ReadProperties;

@Named
@ApplicationScoped
public class SeatMapsServiceSoap {

    private static final Logger log = LoggerFactory.getLogger(SeatMapsServiceSoap.class);

    @Inject
    private SaveSeatmapCollectionDispatcher saveSeatmapCollectionDispatcher;

    @Inject
    private SeatMapCheckInSoapService seatMapCheckInSoapService;

    @Inject
    private IPNRLookupDao pnrLookupDao;

    @Inject
    private FlxSeatMapPriceService flxSeatmapPricesService;

    @Inject
    @Named("CheckInSetUpConfigFactoryWrapper")
    private CheckInSetUpConfigFactoryWrapper setUpConfigFactory;

    @Inject
    private SeatMapCommon seatMapCommon;

    @Inject
    private ReadProperties prop;

    @PostConstruct
    public void init() {
        if (null == setUpConfigFactory) {
            setUpConfigFactory = new CheckInSetUpConfigFactoryWrapper();
        }
    }

    /**
     * @param seatmapCheckInRQ
     * @return SeatmapCollection
     * @throws Exception
     */
    public SeatmapCollection getSeatMaps(SeatmapCheckInRQ seatmapCheckInRQ) throws Exception {
        // This should return only ONE PNR.
        PNRCollection pnrCollection = null;
        try {
            pnrCollection = pnrLookupDao.readPNRCollectionByCartId(seatmapCheckInRQ.getCartId());
        } catch (MongoDisconnectException me) {
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(seatmapCheckInRQ.getPos())) {
                log.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
            } else {
                log.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
            }
        } catch (SabreLayerUnavailableException se) {
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(seatmapCheckInRQ.getPos())) {
                log.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_22110301,"SeatMapsServiceSoap.java:94", se.getMessage());
            } else {
                log.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_12110301,"SeatMapsServiceSoap.java:96", se.getMessage());
            }
        }catch (SabreEmptyResponseException ser) {
            log.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110302).toString(), ser.fillInStackTrace());
        }

        if (null == pnrCollection || null == pnrCollection.getCollection() || pnrCollection.getCollection().isEmpty()) {
            log.info("Can't find PNR: " + seatmapCheckInRQ.getCartId());
            return null;
        }

        BookedLeg bookedLeg = pnrCollection.getBookedLegByCartId(seatmapCheckInRQ.getCartId());
        String link = CommonUtil.getLink(seatmapCheckInRQ.getUri());

        if (SeatSelectionType.HIDE_SEATMAP.equals(bookedLeg.getSeatSelectionType())
                || pnrCollection.getCollection().get(0).isStandby()
                //Leer tipo de pasajero
                || PassengerFareType.E.getCode().equalsIgnoreCase(pnrCollection.getCollection().get(0).getCarts().getCollection().get(0).getTravelerInfo().getCollection().get(0).getPassengerType())
                || pnrCollection.getCollection().get(0).isStandby()) {
            return seatMapCheckInSoapService.getSeatMapCheckInSoapUnavailable(bookedLeg, link);
        }

        if (null != pnrCollection.getStore() && !pnrCollection.getStore().trim().isEmpty()) {
            seatmapCheckInRQ.setStore(pnrCollection.getStore());
        }

        if (null != pnrCollection.getPos() && !pnrCollection.getPos().trim().isEmpty()) {
            seatmapCheckInRQ.setPos(pnrCollection.getPos());
        }

        PNR pnr = pnrCollection.getPNRByCartId(seatmapCheckInRQ.getCartId());
        CartPNR cartPNR = null;
        if (null != pnr) {
            seatmapCheckInRQ.setRecordLocator(pnr.getPnr());
            cartPNR = pnr.getCartPNRByCartId(seatmapCheckInRQ.getCartId());
        }

        SeatmapCollection seatmapCollection;

        if (cartPNR.isSeamlessCheckin()) {
            seatmapCollection = SeatmapCollectionUtil.getSeatmapCollectionEmpty();

            return seatmapCollection;
        }

        try {
            // We are going to use the BookingClasses of the first passenger/traveler in the PNR cart.
            BookingClassMap bookingClassMap = pnrCollection.getBookingClassMapByCardId(seatmapCheckInRQ.getCartId());
            seatmapCheckInRQ.setNeededSeats(pnrCollection.getNeededSeats(seatmapCheckInRQ.getCartId()));

            seatmapCollection = seatMapCheckInSoapService.getSeatMapCheckInSoap(
                    bookedLeg, bookingClassMap, seatmapCheckInRQ, setUpConfigFactory
            );
        } catch (SabreLayerUnavailableException ex) {
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(seatmapCheckInRQ.getPos())) {
                log.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_22110301,"SeatMapsServiceSoap.java:151", ex.getMessage());
            } else {
                log.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_12110301,"SeatMapsServiceSoap.java:153", ex.getMessage());
            }
            seatmapCollection = null;
        }catch (SabreEmptyResponseException ser) {
            log.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110302).toString(), ser.fillInStackTrace());
            seatmapCollection = null;
        } catch (Exception e) {
            log.error("seatMapCommonService Cart Id: " + seatmapCheckInRQ.getCartId() + ", " + e.getMessage());
            throw e;
        }
        
        if (null != pnr && null != pnr.getLegs()) {
            for (BookedLeg bookLeg : pnr.getLegs().getCollection()) {
                for (BookedSegment bookedSegment : bookLeg.getSegments().getCollection()) {

                    if (null != bookedSegment.getSegment().getFareFamily()
                            && (FarebasisFamilyType.DOMESTIC_BASIC_FAMILY.getFamilyName().equalsIgnoreCase(bookedSegment.getSegment().getFareFamily())
                            || FarebasisFamilyType.TRANSBORDER_BASIC_FAMILY.getFamilyName().equalsIgnoreCase(bookedSegment.getSegment().getFareFamily())
                            || FarebasisFamilyType.CANADA_BASIC_FAMILY.getFamilyName().equalsIgnoreCase(bookedSegment.getSegment().getFareFamily())
                            || FarebasisFamilyType.CENTROAMERICA_CARIBE_BASIC_FAMILY.getFamilyName().equalsIgnoreCase(bookedSegment.getSegment().getFareFamily())
                            || FarebasisFamilyType.EUROPA_BASIC_FAMILY.getFamilyName().equalsIgnoreCase(bookedSegment.getSegment().getFareFamily())
                            || FarebasisFamilyType.SUDAMERICA_ASIA_BASIC_FAMILY.getFamilyName().equalsIgnoreCase(bookedSegment.getSegment().getFareFamily()))) {

                        List<SeatSectionCodeType> seatSectionCodeType = new ArrayList<>();

                        if (seatmapCheckInRQ.isInternalAssignment()) {
                            if (PosType.KIOSK.toString().equalsIgnoreCase(seatmapCheckInRQ.getPos())) {
                                seatSectionCodeType.add(SeatSectionCodeType.AM_PLUS);
                            } else {
                                seatSectionCodeType.add(SeatSectionCodeType.AM_PLUS);
                                seatSectionCodeType.add(SeatSectionCodeType.EXIT_ROW);
                                seatSectionCodeType.add(SeatSectionCodeType.PREFERRED);
                            }
                        } else {
                            seatSectionCodeType.add(SeatSectionCodeType.AM_PLUS);
                            seatSectionCodeType.add(SeatSectionCodeType.EXIT_ROW);
                            /**
                             * some seats are PREFERRED
                             */
                            //seatSectionCodeType.add(SeatSectionCodeType.PREFERRED);
                        }

                        //SeatMapUtil.lockSeatMap(bookedSegment.getSegment().getSegmentCode(), seatmapCollection, seatSectionCodeType);
                        SeatMapUtil.unLockSeatMap(bookedSegment.getSegment().getSegmentCode(), seatmapCollection, seatSectionCodeType);
                    }
                }
            }
        }

        if (seatmapCollection != null && seatmapCollection.getCollection() != null) {
            //Disable emergency exit seats
            if (disableExitRow(cartPNR)) {
                List<SeatSectionCodeType> seatSectionCodeType = new ArrayList<>();
                seatSectionCodeType.add(SeatSectionCodeType.EXIT_ROW);
                for (AbstractSeatMap abstractSeatMap : seatmapCollection.getCollection()) {
                    if (abstractSeatMap instanceof SeatMap) {
                        SeatMap seatMap = (SeatMap) abstractSeatMap;
                        SeatMapUtil.lockSection(seatMap, seatSectionCodeType);
                    }
                }
            }

            //Rules AMPlus
//            if (cartPNR != null) {
//                seatMapCommon.rulesFareAMPlus(cartPNR.getTravelerInfo(), seatmapCollection);
//            }
        }

        if (SystemVariablesUtil.isFareLogixSeatmapEnabled()) {
            log.info("EXECUTE_FARELOGIX {}",pnr.getPnr());
            try {
                flxSeatmapPricesService.setPricesFromFareLogix(pnr, seatmapCollection, seatmapCheckInRQ);
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        log.info("PNRSEATMAP {}",pnr.getPnr());
        
        // change preferred seat to cost zero by corporate benefit
        if (null != pnrCollection.getCollection().get(0).getClid() && pnrCollection.getCollection().get(0).getClid().getApplyCorporateBenefit()) {
        	this.overwriteZeroPriceToPreferredSection(seatmapCollection);
        }

        if (null == seatmapCollection) {
            seatmapCollection = new SeatmapCollection();
            List<AbstractSeatMap> abstractSeatMapList = new ArrayList<>();
            SeatMap seatMap = new SeatMap();
            seatMap.setStatus(SeatmapStatusType.UNAVAILABLE);
            abstractSeatMapList.add(seatMap);
            seatmapCollection.setCollection(abstractSeatMapList);
        }

        try {
            SeatmapCollectionWrapper seatmapCollectionWrapper = new SeatmapCollectionWrapper();
            seatmapCollectionWrapper.setCartId(seatmapCheckInRQ.getCartId());
            seatmapCollectionWrapper.setSeatmapCollection(seatmapCollection);
            saveSeatmapCollection(seatmapCollectionWrapper);
        } catch (Exception ex) {
            log.error("seatmapCollectionWrapper: " + ex.getMessage());
        }

        return seatmapCollection;
    }
    
    private void overwriteZeroPriceToPreferredSection(SeatmapCollection seatmapCollection) {
        if (null != seatmapCollection && !seatmapCollection.getCollection().isEmpty()) {
        	for (AbstractSeatMap abstractSeatMap : seatmapCollection.getCollection()) {
                if (abstractSeatMap instanceof SeatMap) {
                    SeatMap seatMap = (SeatMap) abstractSeatMap;
                    for (SeatmapSection listSeatMap : seatMap.getSections().getCollection()) {
                    	if (SeatSectionCodeType.PREFERRED == listSeatMap.getCode()) {
                    		for (SeatmapRow row : listSeatMap.getRows().getCollection()) {
                    			for (SeatmapSeat seat : row.getSeats().getCollection()) {
                    				if (seat.getChoice() instanceof SeatChoiceUpsell) {
                    					SeatChoiceUpsell seatChoiceUpsell = (SeatChoiceUpsell) seat.getChoice();
                                    	if (SeatUpgradeType.PREFERRED_UPGRADE == seatChoiceUpsell.getType()) {
	                                    	String currencyCode = seatChoiceUpsell.getCurrency().getCurrencyCode();
	        		                        seatChoiceUpsell.setCurrency(this.getZeroCurrencyValue(currencyCode));
                                    	}
                    				}
                    			}
                    		}
                    	}
                    }
                }
        	}
        }
    }
    
    /**
     * @param currencyCode
     * @return
     */
    private CurrencyValue getZeroCurrencyValue(String currencyCode) {
        try {
            CurrencyValue currencyValue = new CurrencyValue();
            currencyValue.setCurrencyCode(currencyCode);
            currencyValue.setTotalTax(CurrencyUtil.getAmount(BigDecimal.ZERO, currencyCode));
            currencyValue.setBase(CurrencyUtil.getAmount(BigDecimal.ZERO, currencyCode));
            currencyValue.setTotal(CurrencyUtil.getAmount(BigDecimal.ZERO, currencyCode));

            return currencyValue;

        } catch (Exception ex) {
            return null;
        }
    }

    private boolean disableExitRow(CartPNR cartPNR) {
        if (cartPNR != null && cartPNR.getTravelerInfo() != null && cartPNR.getTravelerInfo().getCollection() != null) {
            try {
                for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
                    if (null == bookedTraveler.getDateOfBirth() || bookedTraveler.getDateOfBirth().isEmpty()) {
                        return true;
                    } else {
                        LocalDate now = LocalDate.now();
                        LocalDate dateOfBirth = LocalDate.parse(bookedTraveler.getDateOfBirth());
                        int yearsPassenger = Period.between(dateOfBirth, now).getYears();
                        if (yearsPassenger < 15) {
                            return true;
                        }
                    }
                }
            } catch (Exception e) {
                log.info("Error DisableExitRow: ", e);
            }
        }
        return false;
    }

    /**
     * @param seatmapCollectionWrapper
     */
    private void saveSeatmapCollection(SeatmapCollectionWrapper seatmapCollectionWrapper) {
        saveSeatmapCollection(seatmapCollectionWrapper, false);
    }

    /**
     * This method is used to update the PNR collection MongoDB in an
     * asynchronous mode
     */
    private void saveSeatmapCollection(SeatmapCollectionWrapper seatmapCollectionWrapper, boolean mustBeAsynchronous) {
        if (mustBeAsynchronous) {
            try {
                SaveSeatmapCollectionEvent saveSeatmapCollectionEvent = new SaveSeatmapCollectionEvent(seatmapCollectionWrapper);
                saveSeatmapCollectionDispatcher.publish(saveSeatmapCollectionEvent);
            } catch (Exception ex) {
                log.error(ex.getMessage());
                //ignore
            }
        } else {
            try {
                SaveSeatmapCollectionEvent saveSeatmapCollectionEvent = new SaveSeatmapCollectionEvent(seatmapCollectionWrapper);
                int code = pnrLookupDao.saveSeatMapCollection(saveSeatmapCollectionEvent.getSeatmapCollectionWrapper());

                log.info("Saving seat map collection to DB, code:" + code);
            } catch (MongoDisconnectException me) {
                log.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
            } catch (SabreLayerUnavailableException se) {
                log.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_12110301,"SeatMapsServiceSoap.java:311", se.fillInStackTrace());
            }catch (SabreEmptyResponseException ser) {
                log.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110302).toString(), ser.fillInStackTrace());
            } catch (Exception ex) {
                log.info("Error saving seat map collection to data base: " + ex.getMessage());
            }
        }
    }


    /**
     * PreSelectSeat
     *
     * @param seatMapsRQ
     * @return
     */
    public PreSelectSeatResponse preSelectSeat(SeatMapsRQ seatMapsRQ) {
        PreSelectSeatResponse preSelectSeatResponse = new PreSelectSeatResponse();
        Map<Integer, List<SegmentChoice>> preselectSeatmap = new HashMap<>();
        try {
            PNRCollection pnrCollection = pnrLookupDao.readPNRCollectionByCartId(seatMapsRQ.getCartId());
            if (null == pnrCollection || null == pnrCollection.getCollection() || pnrCollection.getCollection().isEmpty()) {
                log.info("Can't find PNR: " + seatMapsRQ.getCartId());
                return preSelectSeatResponse;
            }

            PNR pnr = pnrCollection.getPNRByCartId(seatMapsRQ.getCartId());
            CartPNR cartPNR = null;
            if (null != pnr) {
                cartPNR = pnr.getCartPNRByCartId(seatMapsRQ.getCartId());
            }
            SeatmapCollection seatmapCollection = getSeatMapCollection(seatMapsRQ.getCartId());
            if (seatmapCollection != null && cartPNR != null) {
                for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
                    if (bookedTraveler.getSegmentChoices() == null || bookedTraveler.getSegmentChoices().getCollection().isEmpty()) {
                        List<SegmentChoice> lstSegmentChoice = seatMapCommon.getLstSegmentChoice(seatmapCollection.getCollection());
                        if (!lstSegmentChoice.isEmpty()) {
                            preselectSeatmap.put(Integer.parseInt(bookedTraveler.getNameRefNumber().split("\\.")[0]), lstSegmentChoice);
                        }
                    }
                }
            }
        } catch (Exception ex) {
            log.info("Error: ", ex);
        }

        if (!preselectSeatmap.isEmpty()) {
            preSelectSeatResponse.setPreselectSeatmap(preselectSeatmap);
        }

        return preSelectSeatResponse;
    }

    public SeatmapCollection getSeatMapCollection(String cartId) throws Exception, GenericException {

        // read the seatmapCollectionWrapper from the database
        SeatmapCollectionWrapper seatmapCollectionWrapper = pnrLookupDao.readSeatMapCollectionByCartId(cartId);

        if (null == seatmapCollectionWrapper) {
            log.error(ErrorType.CART_COLLECTION_OBJECT_EMPTY.getFullDescription() + ": seatmapCollectionWrapper!");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNABLE_TO_RETRIEVE_PRICE_FOR_SEAT,
                    ErrorType.UNABLE_TO_RETRIEVE_PRICE_FOR_SEAT.getFullDescription() + "CartId - " + cartId);
        }
        SeatmapCollection seatmapCollection = seatmapCollectionWrapper.getSeatmapCollection();
        if (null == seatmapCollection) {
            log.error(ErrorType.CART_COLLECTION_OBJECT_EMPTY.getFullDescription() + ": seatmapCollection!");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNABLE_TO_RETRIEVE_PRICE_FOR_SEAT,
                    ErrorType.UNABLE_TO_RETRIEVE_PRICE_FOR_SEAT.getFullDescription() + "CartId - " + cartId);
        }
        return seatmapCollection;
    }

    public SaveSeatmapCollectionDispatcher getSaveSeatmapCollectionDispatcher() {
        return saveSeatmapCollectionDispatcher;
    }

    public void setSaveSeatmapCollectionDispatcher(SaveSeatmapCollectionDispatcher saveSeatmapCollectionDispatcher) {
        this.saveSeatmapCollectionDispatcher = saveSeatmapCollectionDispatcher;
    }

    public SeatMapCheckInSoapService getSeatMapCheckInSoapService() {
        return seatMapCheckInSoapService;
    }

    public void setSeatMapCheckInSoapService(SeatMapCheckInSoapService seatMapCheckInSoapService) {
        this.seatMapCheckInSoapService = seatMapCheckInSoapService;
    }

    public IPNRLookupDao getPnrLookupDao() {
        return pnrLookupDao;
    }

    public void setPnrLookupDao(IPNRLookupDao pnrLookupDao) {
        this.pnrLookupDao = pnrLookupDao;
    }
}
