package com.am.checkin.web.service;

import com.aeromexico.commons.model.weather.DailyForecasts;
import com.aeromexico.weather.service.WeatherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

@Named
@ApplicationScoped
public class WeatherCityService {

    private static final Logger log = LoggerFactory.getLogger(WeatherCityService.class);

    public DailyForecasts getWheatherInfoCity(String iataCity, String arrivalDate, boolean isLanguageMX) throws Exception {

        DailyForecasts dailyForecasts = null;
        try {

            dailyForecasts = WeatherService.getWeather(iataCity, arrivalDate, isLanguageMX);

        } catch (Exception e) {
            log.error("WeatherCityService: " + e.getMessage());
        }

        return dailyForecasts;
    }
}
