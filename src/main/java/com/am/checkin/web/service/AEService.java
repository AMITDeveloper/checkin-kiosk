package com.am.checkin.web.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;

import com.aeromexico.commons.web.types.*;
import com.aeromexico.sabre.api.session.exception.SabreEmptyResponseException;
import com.aeromexico.sabre.api.session.exception.SabreLayerUnavailableException;
import org.aeromexico.commons.exception.config.ReadProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.loggingutils.LogUtil;
import com.aeromexico.commons.model.AbstractAncillary;
import com.aeromexico.commons.model.AbstractSegmentChoice;
import com.aeromexico.commons.model.Ancillary;
import com.aeromexico.commons.model.AuthorizationResult;
import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.BookingClass;
import com.aeromexico.commons.model.CabinUpgradeAncillary;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.ExtraWeightAncillary;
import com.aeromexico.commons.model.PNR;
import com.aeromexico.commons.model.PNRCollection;
import com.aeromexico.commons.model.PaidSegmentChoice;
import com.aeromexico.commons.model.PaidTravelerAncillary;
import com.aeromexico.commons.model.SegmentChoice;
import com.aeromexico.commons.model.TravelerAncillary;
import com.aeromexico.commons.model.UpgradeAncillary;
import com.aeromexico.commons.model.rq.PurchaseOrderRQ;
import com.aeromexico.commons.web.util.FilterPassengersUtil;
import com.aeromexico.commons.web.util.SegmentCodeUtil;
import com.aeromexico.dao.async.SimpleAsync;
import com.aeromexico.dao.qualifiers.AirportsCache;
import com.aeromexico.dao.services.IAirportCodesDao;
import com.aeromexico.dao.services.IPNRLookupDao;
import com.aeromexico.sabre.api.acs.AMEditPassCharService;
import com.aeromexico.sabre.api.session.common.ServiceCall;
import com.aeromexico.sabre.api.sessionpool.factory.SabreSession;
import com.aeromexico.sabre.api.updatereservation.AMUpdateReservationService;
import com.am.checkin.web.util.AEUtil;
import com.am.checkin.web.util.AncillaryWaivedUtil;
import com.am.checkin.web.util.PnrCollectionUtil;
import com.am.checkin.web.util.PurchaseOrderUtil;
import com.am.checkin.web.util.ShoppingCartUtil;
import com.google.gson.Gson;
import com.sabre.services.acs.bso.editpassengercharacteristics.v3.ACSEditPassengerCharacteristicsRQACS;
import com.sabre.services.acs.bso.editpassengercharacteristics.v3.ACSEditPassengerCharacteristicsRSACS;
import com.sabre.services.model.ReservationItem;
import com.sabre.services.stl.v3.ErrorOrSuccessCode;
import com.sabre.webservices.pnrbuilder.updatereservation.ReservationUpdateItemType;
import com.sabre.webservices.pnrbuilder.updatereservation.UpdateReservationRQ;
import com.sabre.webservices.pnrbuilder.updatereservation.UpdateReservationRS;
import java.math.BigDecimal;

/**
 *
 * @author adrian
 */
@Named
@ApplicationScoped
public class AEService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AEService.class);

    private static final Gson GSON = new Gson();
    private static final String SERVICE = "AE Service";

    @Inject
    private AMUpdateReservationService updateReservationService;

    @Inject
    private IPNRLookupDao pnrLookupDao;

    @Inject
    private AMEditPassCharService editPassCharService;

    @Inject
    @AirportsCache
    private IAirportCodesDao airportsCodesDao;

    @Inject
    private LogOnDatabaseService logOnDatabaseService;

    @Inject
    private ReadProperties prop;

    /**
     *
     * @return @throws Exception
     */
    public SabreSession getSession() throws Exception {
        return updateReservationService.getSession();
    }

    /**
     *
     * @param sabreSession
     */
    public void closeSession(SabreSession sabreSession) {
        updateReservationService.closeSession(sabreSession);
    }

    /**
     *
     * @param bookedTraveler
     * @param travelerAncillary
     * @param bookedLeg
     * @param recordLocator
     * @param cartId
     * @param store
     * @param serviceCallList
     * @throws Exception
     */
    public void addNewAncillaryToReservation(
            BookedTraveler bookedTraveler,
            TravelerAncillary travelerAncillary,
            BookedLeg bookedLeg,
            String recordLocator,
            String cartId,
            String store,
            List<ServiceCall> serviceCallList,
            String pos
    ) throws Exception {

        List<TravelerAncillary> travelerAncillariesAdded = new ArrayList<>();

        UpdateReservationRQ updateReservationRQ;
        updateReservationRQ = AEUtil.getCreateReservationRQ(
                bookedTraveler,
                travelerAncillary,
                bookedLeg,
                recordLocator,
                store,
                travelerAncillariesAdded
        );

        if (!updateReservationRQ.getReservationUpdateList().getReservationUpdateItem().isEmpty()) {

            List<BookedTraveler> bookedTravelerList = new ArrayList<>();
            bookedTravelerList.add(bookedTraveler);
            try {
                UpdateReservationRS updateReservationRS = callUpdateReservation(
                        recordLocator, cartId,
                        "adding ancillaries to reservation",
                        updateReservationRQ,
                        serviceCallList, pos
                );

                PnrCollectionUtil.setAncillaryId(travelerAncillariesAdded, updateReservationRS);//, seatsAdded
                PnrCollectionUtil.updateAncillaryInfo(
                        bookedTravelerList,
                        updateReservationRS.getReservation().getPassengerReservation().getPassengers().getPassenger()
                );

                updateActionCodesAfterAdding(travelerAncillariesAdded, recordLocator, cartId, serviceCallList, pos);

            }catch (SabreEmptyResponseException ser) {
                PnrCollectionUtil.removeDefaultAncillaryId(travelerAncillariesAdded);//, seatsAdded
                LOGGER.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110302).toString(), ser.fillInStackTrace());
                throw ser;
            }catch (SabreLayerUnavailableException se){
                PnrCollectionUtil.removeDefaultAncillaryId(travelerAncillariesAdded);//, seatsAdded
                if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(pos)) {
                    LOGGER.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_22110301,"AEService.java:176", se.getMessage());
                    throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getCode(),
                            prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getActionable(),
                            prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getMessage(),
                            Response.Status.INTERNAL_SERVER_ERROR);
                } else {
                    LOGGER.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_12110301,"AEService.java:182", se.getMessage());
                    throw se;
                }
            } catch (Exception ex) {

                PnrCollectionUtil.removeDefaultAncillaryId(travelerAncillariesAdded);//, seatsAdded

                throw ex;
            }
        }
    }

    /**
     *
     * @param sabreSession
     * @param bookedTravelerList
     * @param bookedLeg
     * @param recordLocator
     * @param cartId
     * @param store
     * @param upgradeAncillaryList
     * @param closeSession
     * @param serviceCallList
     * @throws Exception
     */
    public void addNewUpgradeAncillariesToReservation(
            SabreSession sabreSession,
            List<BookedTraveler> bookedTravelerList,
            BookedLeg bookedLeg,
            String recordLocator,
            String cartId,
            String store,
            List<UpgradeAncillary> upgradeAncillaryList,
            boolean closeSession,
            List<ServiceCall> serviceCallList,
            String pos
    ) throws Exception {

        List<CabinUpgradeAncillary> cabinUpgradeAncillariesAdded = new ArrayList<>();

        UpdateReservationRQ updateReservationRQ;
        updateReservationRQ = AEUtil.getCreateReservationRQForUpgrades(
                bookedTravelerList,
                cabinUpgradeAncillariesAdded,
                upgradeAncillaryList,
                bookedLeg,
                recordLocator,
                store
        );

        if (!updateReservationRQ.getReservationUpdateList().getReservationUpdateItem().isEmpty()) {

            try {
                UpdateReservationRS updateReservationRS;
                updateReservationRS = callUpdateReservation(
                        sabreSession, recordLocator, cartId,
                        "adding ancillaries to reservation",
                        updateReservationRQ, closeSession,
                        serviceCallList, pos
                );

                PnrCollectionUtil.setUpgradeAncillaryId(
                        cabinUpgradeAncillariesAdded, updateReservationRS
                );
                PnrCollectionUtil.updateAncillaryInfo(
                        bookedTravelerList,
                        updateReservationRS.getReservation().getPassengerReservation().getPassengers().getPassenger()
                );

                try {
                    updateUpgradeActionCode(
                            cabinUpgradeAncillariesAdded,
                            recordLocator,
                            cartId,
                            ActionCodeType.HD,
                            serviceCallList,
                            pos
                    );

                    for (CabinUpgradeAncillary cabinUpgradeAncillary : cabinUpgradeAncillariesAdded) {
                        cabinUpgradeAncillary.setActionCode(ActionCodeType.HD.toString());
                    }
                } catch (Exception ex) {
                    logError(recordLocator, cartId, "Error updating action code: " + ex.getMessage());
                }

            }catch (SabreEmptyResponseException ser) {
                LOGGER.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110302).toString(), ser.fillInStackTrace());
                PnrCollectionUtil.removeDefaultUpgradeAncillaryId(cabinUpgradeAncillariesAdded);//, seatsAdded
                throw ser;
            }catch (SabreLayerUnavailableException se){
                PnrCollectionUtil.removeDefaultUpgradeAncillaryId(cabinUpgradeAncillariesAdded);//, seatsAdded
                if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(pos)) {
                    LOGGER.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_22110301,"AEService.java:275", se.getMessage());
                    throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getCode(),
                            prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getActionable(),
                            prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getMessage(),
                            Response.Status.INTERNAL_SERVER_ERROR);
                } else {
                    LOGGER.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_12110301,"AEService.java:281", se.getMessage());
                    throw se;
                }
            } catch (Exception ex) {

                PnrCollectionUtil.removeDefaultUpgradeAncillaryId(cabinUpgradeAncillariesAdded);//, seatsAdded

                throw ex;
            }
        }

    }

    /**
     *
     * @param bookedTravelerList
     * @param bookedLeg
     * @param recordLocator
     * @param cartId
     * @param serviceCallList
     * @throws Exception
     */
    public void addNewFirstBagToReservation(
            List<BookedTraveler> bookedTravelerList, BookedLeg bookedLeg,
            String recordLocator, String cartId,
            List<ServiceCall> serviceCallList, String pos
    ) throws Exception {

        Ancillary ancillary = new Ancillary();
        ancillary.setCommercialName("1A PIEZA EXTRA 1ST XTRA PZ");
        ancillary.setRficCode("C");
        ancillary.setRficSubcode("0CC");
        ancillary.setOwningCarrierCode("AM");
        ancillary.setVendor("ATP");
        ancillary.setEmdType("2");
        ancillary.setFeeApplicationIndicator("4");
        ancillary.setSegmentIndicator("S");
        ancillary.setGroupCode("BG");
        addNewFirstBagToReservation(
                null, bookedTravelerList, bookedLeg,
                recordLocator, cartId, ancillary,
                serviceCallList, pos
        );
    }

    /**
     *
     * @param sabreSession
     * @param bookedTravelerList
     * @param bookedLeg
     * @param recordLocator
     * @param cartId
     * @param ancillary
     * @param serviceCallList
     * @throws Exception
     */
    public void addNewFirstBagToReservation(
            SabreSession sabreSession,
            List<BookedTraveler> bookedTravelerList,
            BookedLeg bookedLeg,
            String recordLocator,
            String cartId,
            Ancillary ancillary,
            List<ServiceCall> serviceCallList,
            String pos
    ) throws Exception {

        UpdateReservationRQ updateReservationRQ;

        for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {
            for (BookedTraveler bookedTraveler : bookedTravelerList) {
                if (AncillaryWaivedUtil.isElegibleForWaiveFirstBag(bookedSegment, bookedTraveler, airportsCodesDao)) {
                    updateReservationRQ = AEUtil.getCreateReservationRQForFirstBag(bookedTraveler, bookedLeg, recordLocator, ancillary, bookedSegment);
                    if (!updateReservationRQ.getReservationUpdateList().getReservationUpdateItem().isEmpty()
                            && null != updateReservationRQ.getReservationUpdateList().getReservationUpdateItem().get(0).getAncillaryServicesUpdate().getNameAssociationList()) {

                        try {
                            UpdateReservationRS updateReservationRS = callUpdateReservation(
                                    sabreSession, recordLocator, cartId,
                                    "adding ancillaries to reservation",
                                    updateReservationRQ, true,
                                    serviceCallList, pos
                            );
                            PaidTravelerAncillary paidTravelerAncillary = new PaidTravelerAncillary();
                            paidTravelerAncillary.setType(ancillary.getRficSubcode());
                            paidTravelerAncillary.setGroupCode(ancillary.getGroupCode());
                            paidTravelerAncillary.setUsed(false);
                            paidTravelerAncillary.setQuantity(1);
                            paidTravelerAncillary.setLegCode(bookedLeg.getSegments().getLegCode());

                            bookedTraveler.getAncillaries().getCollection().add(paidTravelerAncillary);
                            if (!bookedTraveler.getSsrRemarks().getSsrCodes().contains("AE")) {
                                bookedTraveler.getSsrRemarks().getSsrCodes().add("AE");
                            }

                            PnrCollectionUtil.updateAncillaryInfo(
                                    bookedTravelerList,
                                    updateReservationRS.getReservation().getPassengerReservation().getPassengers().getPassenger()
                            );
                        } catch (Exception ex) {
                            throw ex;
                        }
                    }
                }
            }
        }
    }

    /**
     *
     * @param bookedTravelerList
     * @param bookedLeg
     * @param recordLocator
     * @param cartId
     * @param store
     * @param serviceCallList
     * @throws Exception
     */
    public void addNewExtraWeightAncillariesToReservation(
            List<BookedTraveler> bookedTravelerList,
            BookedLeg bookedLeg,
            String recordLocator,
            String cartId,
            String store,
            List<ServiceCall> serviceCallList,
            String pos
    ) throws Exception {

        List<ExtraWeightAncillary> extraWeightAncillariesAdded = new ArrayList<>();

        UpdateReservationRQ updateReservationRQ;
        updateReservationRQ = AEUtil.getCreateReservationRQForExtraWeight(
                bookedTravelerList, bookedLeg,
                recordLocator, store, extraWeightAncillariesAdded
        );

        if (!updateReservationRQ.getReservationUpdateList().getReservationUpdateItem().isEmpty()) {

            try {
                UpdateReservationRS updateReservationRS;
                updateReservationRS = callUpdateReservation(
                        recordLocator, cartId,
                        "adding ancillaries to reservation",
                        updateReservationRQ,
                        serviceCallList,
                        pos
                );

                PnrCollectionUtil.setExtraWeightAncillaryId(extraWeightAncillariesAdded, updateReservationRS);//, seatsAdded
                PnrCollectionUtil.updateAncillaryInfo(
                        bookedTravelerList,
                        updateReservationRS.getReservation().getPassengerReservation().getPassengers().getPassenger()
                );

                try {
                    updateExtraWeightActionCode(
                            extraWeightAncillariesAdded, recordLocator,
                            cartId, ActionCodeType.HD,
                            serviceCallList, pos
                    );

                    for (ExtraWeightAncillary extraWeightAncillary : extraWeightAncillariesAdded) {
                        extraWeightAncillary.setActionCode(ActionCodeType.HD.toString());
                    }
                } catch (Exception ex) {
                    logError(recordLocator, cartId, "Error updating action code: " + ex.getMessage());
                }

            }catch (SabreEmptyResponseException ser) {
                PnrCollectionUtil.removeDefaultExtraWeightAncillaryId(extraWeightAncillariesAdded);
                LOGGER.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110302).toString(), ser.fillInStackTrace());
                throw ser;
            }catch (SabreLayerUnavailableException se){
                PnrCollectionUtil.removeDefaultExtraWeightAncillaryId(extraWeightAncillariesAdded);
                if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(pos)) {
                    LOGGER.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_22110301,"AEService.java:456", se.getMessage());
                    throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getCode(),
                            prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getActionable(),
                            prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getMessage(),
                            Response.Status.INTERNAL_SERVER_ERROR);
                } else {
                    LOGGER.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_12110301,"AEService.java:462", se.getMessage());
                    throw se;
                }
            } catch (Exception ex) {

                PnrCollectionUtil.removeDefaultExtraWeightAncillaryId(extraWeightAncillariesAdded);

                throw ex;
            }
        }
    }

    /**
     *
     * @param purchaseOrderRQ
     * @param bookedTravelerList
     * @param bookedLeg
     * @param marketType
     * @param recordLocator
     * @param cartId
     * @param store
     * @param serviceCallList
     * @throws GenericException
     * @throws Exception
     */
    public void addNewAncillariesToReservation(
            PurchaseOrderRQ purchaseOrderRQ,
            List<BookedTraveler> bookedTravelerList,
            BookedLeg bookedLeg,
            MarketType marketType,
            String recordLocator,
            String cartId,
            String store,
            List<ServiceCall> serviceCallList
    ) throws GenericException, Exception {
        addNewAncillariesToReservation(
                null,
                purchaseOrderRQ,
                bookedTravelerList,
                bookedLeg,
                marketType,
                recordLocator,
                cartId,
                store,
                true,
                serviceCallList
        );
    }

    /**
     *
     * @param sabreSession
     * @param purchaseOrderRQ
     * @param bookedTravelerList
     * @param bookedLeg
     * @param marketType
     * @param recordLocator
     * @param cartId
     * @param store
     * @param closeSession
     * @param serviceCallList
     * @throws GenericException
     * @throws Exception
     */
    public void addNewAncillariesToReservation(
            SabreSession sabreSession,
            PurchaseOrderRQ purchaseOrderRQ,
            List<BookedTraveler> bookedTravelerList,
            BookedLeg bookedLeg,
            MarketType marketType,
            String recordLocator,
            String cartId,
            String store,
            boolean closeSession,
            List<ServiceCall> serviceCallList
    ) throws GenericException, Exception {

        List<TravelerAncillary> travelerAncillariesAdded = new ArrayList<>();

        UpdateReservationRQ updateReservationRQ;
        updateReservationRQ = AEUtil.getCreateReservationRQ(
                bookedTravelerList,
                bookedLeg,
                marketType,
                recordLocator,
                store,
                travelerAncillariesAdded
        );
        UpdateReservationRS updateReservationRS = null;

        if (!updateReservationRQ.getReservationUpdateList().getReservationUpdateItem().isEmpty()) {

            try {
                updateReservationRS = callUpdateReservation(
                        sabreSession,
                        recordLocator, cartId,
                        "adding ancillaries to reservation",
                        updateReservationRQ,
                        closeSession,
                        serviceCallList,
                        purchaseOrderRQ.getPos()
                );

                logOnDatabaseService.logAEOnSuccess(
                        purchaseOrderRQ,
                        updateReservationRQ,
                        updateReservationRS,
                        recordLocator,
                        cartId
                );

                //add remark
                //addRemarkToBenefit(recordLocator,bookedTravelerList,travelerAncillariesAdded);
                PnrCollectionUtil.setAncillaryId(travelerAncillariesAdded, updateReservationRS);//, seatsAdded
                PnrCollectionUtil.updateAncillaryInfo(
                        bookedTravelerList,
                        updateReservationRS.getReservation().getPassengerReservation().getPassengers().getPassenger()
                );

                updateActionCodesAfterAdding(travelerAncillariesAdded, recordLocator, cartId, serviceCallList, purchaseOrderRQ.getPos());

            }catch (SabreEmptyResponseException ser) {
                LOGGER.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110302).toString(), ser.fillInStackTrace());
                PnrCollectionUtil.removeDefaultAncillaryId(travelerAncillariesAdded);

                logOnDatabaseService.logAEOnError(
                        purchaseOrderRQ,
                        updateReservationRQ,
                        updateReservationRS,
                        recordLocator,
                        cartId,
                        ser
                );
                throw ser;
            } catch (SabreLayerUnavailableException ex) {

                PnrCollectionUtil.removeDefaultAncillaryId(travelerAncillariesAdded);

                logOnDatabaseService.logAEOnError(
                        purchaseOrderRQ,
                        updateReservationRQ,
                        updateReservationRS,
                        recordLocator,
                        cartId,
                        ex
                );

                if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(purchaseOrderRQ.getPos())) {
                    LOGGER.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_22110301,"AEService.java:610", ex.getMessage());
                    throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getCode(),
                            prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getActionable(),
                            prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getMessage(),
                            Response.Status.INTERNAL_SERVER_ERROR);
                } else {
                    LOGGER.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_12110301,"AEService.java:616", ex.getMessage());
                    throw ex;
                }
            } catch (GenericException ex) {
                ex.setWasLogged(true);

                PnrCollectionUtil.removeDefaultAncillaryId(travelerAncillariesAdded);

                logOnDatabaseService.logAEOnError(
                        purchaseOrderRQ,
                        updateReservationRQ,
                        updateReservationRS,
                        recordLocator,
                        cartId,
                        ex
                );

                throw ex;
            } catch (Exception ex) {

                PnrCollectionUtil.removeDefaultAncillaryId(travelerAncillariesAdded);

                logOnDatabaseService.logAEOnError(
                        purchaseOrderRQ,
                        updateReservationRQ,
                        updateReservationRS,
                        recordLocator,
                        cartId,
                        ex
                );

                throw ex;
            }
        }
    }

    private void updateActionCodesAfterAdding(List<TravelerAncillary> travelerAncillariesAdded, String recordLocator, String cartId, List<ServiceCall> serviceCallList, String pos) {
        try {
            updateActionCode(
                    PurchaseOrderUtil.getNonZeroCostAncillaries(
                            PurchaseOrderUtil.getNotBenefitAncillaries(travelerAncillariesAdded)
                    ),
                    recordLocator,
                    cartId,
                    ActionCodeType.HD,
                    serviceCallList,
                    pos
            );

            for (TravelerAncillary travelerAncillaryLocal : travelerAncillariesAdded) {
                if (!travelerAncillaryLocal.isApplyBenefit()
                        && 1 == travelerAncillaryLocal.getAncillary().getCurrency().getTotal().compareTo(BigDecimal.ZERO)) {
                    travelerAncillaryLocal.setActionCode(ActionCodeType.HD.toString());
                }
            }
        } catch (Exception ex) {
            logError(recordLocator, cartId, "Error updating action code: " + ex.getMessage());
        }

        try {
            updateActionCode(
                    PurchaseOrderUtil.getZeroCostAncillaries(
                            PurchaseOrderUtil.getNotBenefitAncillaries(travelerAncillariesAdded)
                    ),
                    recordLocator,
                    cartId,
                    ActionCodeType.HI,
                    serviceCallList,
                    pos
            );

            for (TravelerAncillary travelerAncillaryLocal : travelerAncillariesAdded) {
                if (!travelerAncillaryLocal.isApplyBenefit()
                        && 0 == travelerAncillaryLocal.getAncillary().getCurrency().getTotal().compareTo(BigDecimal.ZERO)) {
                    travelerAncillaryLocal.setActionCode(ActionCodeType.HI.toString());
                }
            }
        } catch (Exception ex) {
            logError(recordLocator, cartId, "Error updating action code: " + ex.getMessage());
        }

        try {
            updateActionCode(
                    PurchaseOrderUtil.getBenefitAncillaries(travelerAncillariesAdded),
                    recordLocator,
                    cartId,
                    ActionCodeType.HK,
                    serviceCallList,
                    pos
            );

            for (TravelerAncillary travelerAncillaryLocal : travelerAncillariesAdded) { 
                if (travelerAncillaryLocal.isApplyBenefit()) {
                    travelerAncillaryLocal.setActionCode(ActionCodeType.HK.toString());
                }
            }
        } catch (Exception ex) {
            logError(recordLocator, cartId, "Error updating action code: " + ex.getMessage());
        }
    }

    /**
     *
     * @param travelerAncillariesAdded
     * @param recordLocator
     * @param cartId
     */
    private void updateActionCode(
            List<TravelerAncillary> travelerAncillariesAdded,
            String recordLocator,
            String cartId,
            ActionCodeType actionCode,
            List<ServiceCall> serviceCallList,
            String pos
    ) throws Exception {

        if (null == travelerAncillariesAdded || travelerAncillariesAdded.isEmpty() || null == actionCode) {
            return;
        }

        List<ReservationUpdateItemType> reservationUpdateItemTypeList;
        reservationUpdateItemTypeList = AEUtil.getUpdateReservationActionCodeItemList(travelerAncillariesAdded, actionCode);

        if (reservationUpdateItemTypeList.isEmpty()) {
            return;
        }

        //Update Reservation RQ
        UpdateReservationRQ updateReservationRQ;
        updateReservationRQ = AEUtil.getUpdateReservationRQ(recordLocator, reservationUpdateItemTypeList);

        callUpdateReservation(
                recordLocator, cartId,
                "Update action code for added ancillaries",
                updateReservationRQ,
                serviceCallList,
                pos
        );
    }

    /**
     *
     * @param extraWeightAncillariesAdded
     * @param recordLocator
     * @param cartId
     */
    private void updateExtraWeightActionCode(
            List<ExtraWeightAncillary> extraWeightAncillariesAdded,
            String recordLocator, String cartId, ActionCodeType actionCode,
            List<ServiceCall> serviceCallList, String pos
    ) throws Exception {

        if (null == extraWeightAncillariesAdded || extraWeightAncillariesAdded.isEmpty() || null == actionCode) {
            return;
        }

        List<ReservationUpdateItemType> reservationUpdateItemTypeList;
        reservationUpdateItemTypeList = AEUtil.getUpdateReservationActionCodeItemList(extraWeightAncillariesAdded, actionCode);

        if (reservationUpdateItemTypeList.isEmpty()) {
            return;
        }

        //Update Reservation RQ
        UpdateReservationRQ updateReservationRQ;
        updateReservationRQ = AEUtil.getUpdateReservationRQ(recordLocator, reservationUpdateItemTypeList);

        callUpdateReservation(
                recordLocator, cartId,
                "Update action code for added ancillaries",
                updateReservationRQ,
                serviceCallList,
                pos
        );
    }

    /**
     *
     * @param cabinUpgradeAncillariesAdded
     * @param recordLocator
     * @param cartId
     * @param actionCode
     * @param serviceCallList
     * @throws Exception
     */
    private void updateUpgradeActionCode(
            List<CabinUpgradeAncillary> cabinUpgradeAncillariesAdded,
            String recordLocator, String cartId, ActionCodeType actionCode,
            List<ServiceCall> serviceCallList, String pos
    ) throws Exception {

        if (null == cabinUpgradeAncillariesAdded || cabinUpgradeAncillariesAdded.isEmpty() || null == actionCode) {
            return;
        }

        List<ReservationUpdateItemType> reservationUpdateItemTypeList;
        reservationUpdateItemTypeList = AEUtil.getUpdateReservationActionCodeItemList(cabinUpgradeAncillariesAdded, actionCode);

        if (reservationUpdateItemTypeList.isEmpty()) {
            return;
        }

        //Update Reservation RQ
        UpdateReservationRQ updateReservationRQ;
        updateReservationRQ = AEUtil.getUpdateReservationRQ(recordLocator, reservationUpdateItemTypeList);

        callUpdateReservation(
                recordLocator, cartId,
                "Update action code for added upgrades",
                updateReservationRQ,
                serviceCallList,
                pos
        );

    }

    /**
     *
     * @param recordLocator
     * @param cartId
     * @param title
     * @param updateReservationRQ
     * @param serviceCallList
     * @return
     * @throws GenericException
     * @throws Exception
     */
    private UpdateReservationRS callUpdateReservation(
            String recordLocator, String cartId,
            String title, UpdateReservationRQ updateReservationRQ,
            List<ServiceCall> serviceCallList, String pos
    ) throws GenericException, Exception {
        return callUpdateReservation(
                null, recordLocator, cartId, title,
                updateReservationRQ, true, serviceCallList, pos
        );
    }

    /**
     *
     * @param sabreSession
     * @param recordLocator
     * @param cartId
     * @param title
     * @param updateReservationRQ
     * @param closeSession
     * @param serviceCallList
     * @return
     * @throws GenericException
     * @throws Exception
     */
    private UpdateReservationRS callUpdateReservation(
            SabreSession sabreSession, String recordLocator, String cartId,
            String title, UpdateReservationRQ updateReservationRQ,
            boolean closeSession, List<ServiceCall> serviceCallList, String pos
    ) throws GenericException, Exception {

        try {
            UpdateReservationRS updateReservationRS;
            updateReservationRS = updateReservationService.updateReservation(
                    sabreSession, updateReservationRQ, closeSession, serviceCallList
            );

            log(recordLocator, cartId, title, updateReservationRQ, updateReservationRS);

            //this call will throw an exception if something was wrong
            AEUtil.checkUpdateReservationErrors(updateReservationRS);

            return updateReservationRS;

        }catch (SabreEmptyResponseException ser) {
            LOGGER.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110302).toString(), ser.fillInStackTrace());
            throw ser;
        }catch (SabreLayerUnavailableException se){
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(pos)) {
                LOGGER.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_22110301,"AEService.java:891", se.getMessage());
                throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getCode(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getActionable(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getMessage(),
                        Response.Status.INTERNAL_SERVER_ERROR);
            } else {
                LOGGER.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_12110301,"AEService.java:897", se.getMessage());
                throw se;
            }
        } catch (GenericException ex) {
            this.logError(recordLocator, cartId, title, updateReservationRQ, ex.getMessage());
            throw ex;
        } catch (Exception ex) {
            this.logError(recordLocator, cartId, title, updateReservationRQ, ex.getMessage());
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNKNOWN_CODE, ex.getMessage());
        }
    }

    /**
     *
     * @param bookedTravelerList
     * @param bookedLeg
     * @param currencyCode
     * @param recordLocator
     * @param cartId
     * @param store
     * @param serviceCallList
     * @throws Exception
     */
    public void addAncillariesToReservationForUnreservedSeat(
            List<BookedTraveler> bookedTravelerList,
            BookedLeg bookedLeg,
            String currencyCode,
            String recordLocator,
            String cartId,
            String store,
            List<ServiceCall> serviceCallList,
            String pos
    ) throws Exception {

        List<BookedTraveler> bookedTravelerListWithUnreservedSeats = new ArrayList<>();

        for (BookedTraveler bookedTraveler : bookedTravelerList) {
            /**
             * P, E for employees travel. F for full fare
             */
            if (!bookedTraveler.isCheckinStatus()
                    && (PassengerFareType.F.toString().equalsIgnoreCase(bookedTraveler.getPassengerType()) || PassengerFareType.P.toString().equalsIgnoreCase(bookedTraveler.getPassengerType()))) {

                boolean hasUnreservedSeat = false;
                for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {

                    if (null != bookedSegment.getSegment()) {
                        String equipmentType = bookedSegment.getSegment().getAircraftType();
                        String segmentCode = bookedSegment.getSegment().getSegmentCode();
                        String seatCode = AEUtil.getSeatCode(segmentCode, bookedTraveler.getSegmentChoices().getCollection());

                        if (null == seatCode || seatCode.isEmpty()) {
                            seatCode = "00";
                        }

                        BookingClass bookingClass;
                        bookingClass = FilterPassengersUtil.getBookingClassBySegmentCode(
                                bookedTraveler.getBookingClasses().getCollection(), segmentCode
                        );

                        if (AirlineCodeType.AM.name().equalsIgnoreCase(bookedSegment.getSegment().getOperatingCarrier())
                                && null != bookingClass
                                && CabinType.COACH_CLASS.getCode().equalsIgnoreCase(bookingClass.getBookingCabin())) {

                            if (!PurchaseOrderUtil.existAncillaryEntryBySegmentCode(
                                    segmentCode, bookedTraveler.getSeatAncillaries().getCollection()
                            )) {
                                hasUnreservedSeat = true;

                                TravelerAncillary travelerAncillary;
                                travelerAncillary = PurchaseOrderUtil.createSeatAncillaryEntryForUnreservedSeats(
                                        bookedTraveler.getNameRefNumber(), segmentCode,
                                        bookedLeg.getSegments().getLegCode(), currencyCode,
                                        seatCode, equipmentType
                                );

                                bookedTraveler.getSeatAncillaries().getCollection().add(travelerAncillary);
                                bookedTraveler.setTouched(true);
                            } else {
                                log(recordLocator, cartId, "Unable to create AE for default seat, there is already an ancillary for that segment");
                            }
                        } else {
                            if (null == bookingClass) {
                                log(recordLocator, cartId, "Unable to create AE for default seat, bookingClass: null, operatingCarrier: " + bookedSegment.getSegment().getOperatingCarrier());
                            } else {
                                log(recordLocator, cartId, "Unable to create AE for default seat, bookingCabin: " + bookingClass.getBookingCabin() + " , operatingCarrier: " + bookedSegment.getSegment().getOperatingCarrier());
                            }
                        }
                    }
                }

                if (hasUnreservedSeat) {
                    bookedTravelerListWithUnreservedSeats.add(bookedTraveler);
                }
            }
        }

        if (!bookedTravelerListWithUnreservedSeats.isEmpty()) {

            List<TravelerAncillary> travelerAncillariesAdded = new ArrayList<>();
            //List<SegmentChoice> seatsAdded = new ArrayList<>();
            UpdateReservationRQ updateReservationRQ;
            updateReservationRQ = AEUtil.getCreateReservationRQForUnreservedSeats(
                    bookedTravelerListWithUnreservedSeats,
                    bookedLeg,
                    recordLocator,
                    store,
                    travelerAncillariesAdded
            );

            if (!updateReservationRQ.getReservationUpdateList().getReservationUpdateItem().isEmpty()) {
                try {
                    UpdateReservationRS updateReservationRS;
                    updateReservationRS = callUpdateReservation(
                            recordLocator, cartId,
                            "adding ancillaries for unreserved seats",
                            updateReservationRQ,
                            serviceCallList,
                            pos
                    );

                    PnrCollectionUtil.setAncillaryId(travelerAncillariesAdded, updateReservationRS);//, seatsAdded
                    PnrCollectionUtil.updateAncillaryInfo(
                            bookedTravelerListWithUnreservedSeats,
                            updateReservationRS.getReservation().getPassengerReservation().getPassengers().getPassenger()
                    );

                    try {
                        updateActionCode(
                                travelerAncillariesAdded,
                                recordLocator,
                                cartId,
                                ActionCodeType.HI,
                                serviceCallList,
                                pos
                        );

                        for (TravelerAncillary travelerAncillaryLocal : travelerAncillariesAdded) {
                            travelerAncillaryLocal.setActionCode(ActionCodeType.HI.toString());
                        }
                    } catch (Exception ex) {
                        logError(recordLocator, cartId, "Error updating action code for unreserved seats: " + ex.getMessage());
                    }
                } catch (Exception ex) {

                    PnrCollectionUtil.removeDefaultAncillaryId(travelerAncillariesAdded);

                    PnrCollectionUtil.removeSeatAncillariesAdded(
                            bookedTravelerListWithUnreservedSeats, travelerAncillariesAdded
                    );
                    throw ex;
                }
            } else {
                log(recordLocator, cartId, "There is not items for update");
            }
        } else {
            log(recordLocator, cartId, "Not users without seats");
        }

    }

    /**
     *
     * @param pnrCollection
     * @param serviceCallList
     * @return
     * @throws Exception
     */
    public boolean removeUnpaidAncillariesForUnassignedSeats(
            PNRCollection pnrCollection, List<ServiceCall> serviceCallList
    ) throws Exception {
        if (null == pnrCollection) {
            return false;
        }

        PNR pnr;
        try {
            pnr = pnrCollection.getCollection().get(0);
        } catch (Exception ex) {
            return false;
        }

        String recordLocator = pnr.getPnr();

        String cartId;
        try {
            cartId = pnr.getCarts().getCollection().get(0).getMeta().getCartId();
        } catch (Exception ex) {
            cartId = null;
        }

        Set<Integer> ancillaryIDs = new HashSet<>();

        List<ReservationUpdateItemType> reservationUpdateItemTypeList;
        reservationUpdateItemTypeList = AEUtil.getUnpaidSeatItemsToBeRemoved(pnr, ancillaryIDs);

        if (reservationUpdateItemTypeList.isEmpty()) {
            LOGGER.info("There is no unpaid unassigned seats ancillaries to be removed");
            return false;
        }

        //Update Reservation RQ
        UpdateReservationRQ updateReservationRQ;
        updateReservationRQ = AEUtil.getUpdateReservationRQ(recordLocator, reservationUpdateItemTypeList);

        log(recordLocator, cartId, "Update Reservation: Delete AE for unpaid unassigned seats RQ\nbody: " + GSON.toJson(updateReservationRQ));

        UpdateReservationRS updateReservationRS;
        updateReservationRS = updateReservationService.deleteAE(updateReservationRQ, serviceCallList);

        log(recordLocator, cartId, "Update Reservation: Delete AE for unpaid unassigned seats RS\nbody: " + GSON.toJson(updateReservationRS));

        //this call will throw an exception if something was wrong
        AEUtil.checkUpdateReservationErrors(updateReservationRS);

        if (ancillaryIDs.isEmpty()) {
            return true;
        }

        //Unassign seats
        unassignRemovedSeats(pnr);

        //remove deleted ancillaries from collection
        AEUtil.removeDeletedItemsFromCollection(pnr, ancillaryIDs);

        return true;

    }

    /**
     *
     * @param pnr
     * @param serviceCallList
     * @return
     * @throws Exception
     */
    public boolean removeUnpaidAncillariesForExtraWeight(
            PNR pnr,
            List<ServiceCall> serviceCallList
    ) throws Exception {
        if (null == pnr) {
            return false;
        }

        String recordLocator = pnr.getPnr();

        String cartId;
        try {
            cartId = pnr.getCarts().getCollection().get(0).getMeta().getCartId();
        } catch (Exception ex) {
            cartId = null;
        }

        Set<Integer> ancillaryIDs = new HashSet<>();

        List<ReservationUpdateItemType> reservationUpdateItemTypeList;
        reservationUpdateItemTypeList = AEUtil.getUnpaidExtraWeightItemsToBeRemoved(pnr, ancillaryIDs);

        if (reservationUpdateItemTypeList.isEmpty()) {
            LOGGER.info("There is no unpaid extra weight ancillaries to be removed");
            return false;
        }

        //Update Reservation RQ
        UpdateReservationRQ updateReservationRQ;
        updateReservationRQ = AEUtil.getUpdateReservationRQ(recordLocator, reservationUpdateItemTypeList);

        log(recordLocator, cartId, "Update Reservation: Delete AE for unpaid extra weight RQ\nbody: " + GSON.toJson(updateReservationRQ));

        UpdateReservationRS updateReservationRS;
        updateReservationRS = updateReservationService.deleteAE(updateReservationRQ, serviceCallList);

        log(recordLocator, cartId, "Update Reservation: Delete AE for unpaid extra weight RS\nbody: " + GSON.toJson(updateReservationRS));

        //this call will throw an exception if something was wrong
        AEUtil.checkUpdateReservationErrors(updateReservationRS);

        if (ancillaryIDs.isEmpty()) {
            return true;
        }

        //remove deleted ancillaries from collection
        AEUtil.removeDeletedItemsFromCollection(pnr, ancillaryIDs);

        return true;

    }

    /**
     *
     * @param pnr
     */
    private void unassignRemovedSeats(PNR pnr) {
        LOGGER.info("Update Reservation, unassigning removed seats.");
        for (CartPNR cartPNR : pnr.getCarts().getCollection()) {
            BookedLeg bookedLeg;
            try {
                bookedLeg = pnr.getBookedLegByLegCode(cartPNR.getLegCode());
            } catch (Exception ex) {
                bookedLeg = null;
            }

            if (null != bookedLeg) {
                for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
                    for (AbstractAncillary abstractAncillary : bookedTraveler.getSeatAncillaries().getCollection()) {
                        if (abstractAncillary instanceof TravelerAncillary) {
                            TravelerAncillary travelerAncillary = (TravelerAncillary) abstractAncillary;

                            if (travelerAncillary.isManuallyDeleted()) {
                                try {
                                    BookedSegment bookedSegment;
                                    bookedSegment = bookedLeg.getBookedSegment(
                                            travelerAncillary.getSegmentCodeAux()
                                    );

                                    AbstractSegmentChoice abstractSegmentChoice;
                                    abstractSegmentChoice = bookedTraveler.getSegmentChoices().getBySegmentCode(
                                            travelerAncillary.getSegmentCodeAux()
                                    );

                                    String seatCode = abstractSegmentChoice.getSeat().getCode();

                                    BookingClass bookingClass;
                                    bookingClass = bookedTraveler.getBookingClasses().getBookingClass(
                                            travelerAncillary.getSegmentCodeAux()
                                    );

                                    boolean delete = true;
                                    // delete the seat code on Sabre
                                    ACSEditPassengerCharacteristicsRQACS editPassengerCharacteristicsRQ;
                                    editPassengerCharacteristicsRQ = ShoppingCartUtil.getEditPassengerCharacteristicsRQSegmentChoices(
                                            bookedSegment.getSegment(), bookingClass, bookedTraveler, seatCode, delete
                                    );

                                    //LOGGER.info("Update Reservation, unassign removed seat RQ:  " + editPassCharService.toString(editPassengerCharacteristicsRQ));
                                    ACSEditPassengerCharacteristicsRSACS editPassengerCharacteristicsRS;
                                    editPassengerCharacteristicsRS = editPassCharService.editPassengerChararcteristics(
                                            editPassengerCharacteristicsRQ
                                    );

                                    //LOGGER.info("Update Reservation, unassign removed seat RS: " + editPassCharService.toString(editPassengerCharacteristicsRS));
                                    // sameData if the change was success
                                    if (!ErrorOrSuccessCode.SUCCESS.equals(editPassengerCharacteristicsRS.getResult().getStatus())) {
                                        //ignore
                                    }

                                } catch (Exception ex) {
                                    LOGGER.error("Update Reservation, Error unassigning removed seat. " + ex.getMessage());
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     *
     * @param bookedTravelerList
     * @param recordLocator
     * @param cartId
     * @param serviceCallList
     * @throws Exception
     */
    public void removeZeroQuantityAncillariesFromReservation(
            List<BookedTraveler> bookedTravelerList,
            String recordLocator, String cartId,
            List<ServiceCall> serviceCallList
    ) throws Exception {

        boolean thereAreQtyZeroAncillaries = false;

        for (BookedTraveler bookedTraveler : bookedTravelerList) {
            thereAreQtyZeroAncillaries = PurchaseOrderUtil.thereAreQtyZeroAncillaries(
                    bookedTraveler.getAncillaries().getCollection()
            );

            if (thereAreQtyZeroAncillaries) {
                break;
            }
        }

        if (!thereAreQtyZeroAncillaries) {
            return;
        }

        log(recordLocator, cartId, "Update Reservation: removing zero quantity ancillaries");

        List<ReservationUpdateItemType> reservationUpdateItemTypeList = new ArrayList<>();

        for (BookedTraveler bookedTraveler : bookedTravelerList) {
            List<TravelerAncillary> unpaidAncillaries = PurchaseOrderUtil.getUnpaidAncillariesOnReservation(
                    bookedTraveler.getAncillaries().getCollection()
            );

            for (TravelerAncillary travelerAncillary : unpaidAncillaries) {
                // only the ancillaries that are currently part of reservation
                // can have a quantity less or equal to zero
                // the new ancillaries are removed from shopping cart on PUT
                // operations
                if (travelerAncillary.getQuantity() <= 0) {

                    AEUtil.addDeleteItem(
                            travelerAncillary.getAncillaryId(),
                            bookedTraveler,
                            reservationUpdateItemTypeList
                    );
                }
            }
        }

        if (!reservationUpdateItemTypeList.isEmpty()) {
            try {
                UpdateReservationRQ updateReservationRQ;
                updateReservationRQ = AEUtil.getUpdateReservationRQ(
                        recordLocator,
                        reservationUpdateItemTypeList
                );

                UpdateReservationRS updateReservationRS;
                updateReservationRS = updateReservationService.deleteAE(
                        updateReservationRQ,
                        serviceCallList
                );

                //this call will throw an exception if something was wrong
                AEUtil.checkUpdateReservationErrors(updateReservationRS);

            } catch (Exception ex) {
                throw ex;
            }

            for (BookedTraveler bookedTraveler : bookedTravelerList) {

                Iterator<TravelerAncillary> itr;
                itr = PurchaseOrderUtil.getUnpaidAncillariesOnReservation(
                        bookedTraveler.getAncillaries().getCollection()
                ).iterator();

                while (itr.hasNext()) {
                    TravelerAncillary travelerAncillary = itr.next(); // must be called before you can call itr.remove()                                

                    // only the ancillaries that are currently part of reservation
                    // can have a quantity less or equal to zero
                    // the new ancillaries are removed from shopping cart on PUT
                    // operations
                    if (travelerAncillary.getQuantity() <= 0) {
                        bookedTraveler.setTouched(true);
                        itr.remove();
                    }
                }
            }
        }
    }

    /**
     *
     * @param bookedTravelerList
     * @param recordLocator
     * @param cartId
     * @param serviceCallList
     * @throws Exception
     */
    public void removeSeatAncillariesFromReservationToChangeSeats(
            List<BookedTraveler> bookedTravelerList,
            String recordLocator, String cartId,
            List<ServiceCall> serviceCallList
    ) throws Exception {

        UpdateReservationRQ updateReservationRQ;
        updateReservationRQ = AEUtil.getDeleteReservationRQToChangeSeats(
                bookedTravelerList, recordLocator
        );

        if (null != updateReservationRQ.getReservationUpdateList()
                && null != updateReservationRQ.getReservationUpdateList().getReservationUpdateItem()
                && !updateReservationRQ.getReservationUpdateList().getReservationUpdateItem().isEmpty()) {

            log(recordLocator, cartId, "Update Reservation: Delete AE RQ " + GSON.toJson(updateReservationRQ));

            UpdateReservationRS updateReservationRS = updateReservationService.deleteAE(
                    updateReservationRQ,
                    serviceCallList
            );

            log(recordLocator, cartId, "Update Reservation: Delete AE RS");
            log(recordLocator, cartId, GSON.toJson(updateReservationRS));

            //this call will throw an exception if something was wrong
            AEUtil.checkUpdateReservationErrors(updateReservationRS);
        }
    }

    /**
     *
     * @param updateReservationRQ
     * @param recordLocator
     * @param cartId
     * @param serviceCallList
     * @throws Exception
     */
    public void deleteAE(
            UpdateReservationRQ updateReservationRQ,
            String recordLocator,
            String cartId,
            List<ServiceCall> serviceCallList
    ) throws Exception {
        log(recordLocator, cartId, "Update Reservation: Delete AE RQ, " + GSON.toJson(updateReservationRQ));

        UpdateReservationRS updateReservationRS;
        updateReservationRS = updateReservationService.deleteAE(
                updateReservationRQ,
                serviceCallList
        );

        log(recordLocator, cartId, "Update Reservation: Delete AE RS, " + GSON.toJson(updateReservationRS));

        //this call will throw an exception if something was wrong
        AEUtil.checkUpdateReservationErrors(updateReservationRS);
    }

    /**
     *
     * @param bookedTraveler
     * @param segmentCode
     * @param recordLocator
     * @param cartId
     * @param serviceCallList
     * @return
     * @throws Exception
     */
    public boolean removeSeatAncillaryFromReservation(
            BookedTraveler bookedTraveler, String segmentCode,
            String recordLocator, String cartId,
            List<ServiceCall> serviceCallList
    ) throws Exception {

        boolean deleted = false;

        if (null == segmentCode || segmentCode.isEmpty()) {
            LOGGER.info("Itinerary code can not be null in order to remove an ancillary");
            return deleted;
        }

        Iterator<AbstractAncillary> itr = bookedTraveler.getSeatAncillaries().getCollection().iterator();

        while (itr.hasNext()) {

            // must be called before you can call itr.remove()
            AbstractAncillary abstractAncillary = itr.next();

            if (abstractAncillary instanceof TravelerAncillary) {
                TravelerAncillary travelerAncillary = (TravelerAncillary) abstractAncillary;

                if (travelerAncillary.isPartOfReservation()
                        && null == travelerAncillary.getEmd()
                        && ActionCodeType.HD.toString().equalsIgnoreCase(travelerAncillary.getActionCode())) {

                    String linkedAncillaryId = travelerAncillary.getLinkedID();
                    String ancillaryId = travelerAncillary.getAncillaryId();

                    if (null != ancillaryId && !ancillaryId.isEmpty()) {

                        if (SegmentCodeUtil.compareSegmentCodeWithoutTime(segmentCode, travelerAncillary.getSegmentCodeAux())) {

                            UpdateReservationRQ updateReservationRQ = AEUtil.getDeleteReservationRQ(bookedTraveler, ancillaryId, recordLocator);

                            log(recordLocator, cartId, "Update Reservation: Delete AE RQ");
                            log(recordLocator, cartId, GSON.toJson(updateReservationRQ));

                            UpdateReservationRS updateReservationRS = updateReservationService.deleteAE(updateReservationRQ, serviceCallList);

                            log(recordLocator, cartId, "Update Reservation: Delete AE RS");
                            log(recordLocator, cartId, GSON.toJson(updateReservationRS));

                            //this call will throw an exception if something was wrong
                            AEUtil.checkUpdateReservationErrors(updateReservationRS);

                            deleted = true;

                            // remove ancillary from DB
                            itr.remove();

                            // remove seat from DB if ancillary is a seat paid
                            Iterator<AbstractSegmentChoice> i = bookedTraveler.getSegmentChoices().getCollection().iterator();

                            if (null != linkedAncillaryId && !linkedAncillaryId.isEmpty()) {

                                try {
                                    while (i.hasNext()) {
                                        // must be called before you can call i.remove()
                                        AbstractSegmentChoice abstractSegmentChoiceDB = i.next();

                                        log(recordLocator, cartId, abstractSegmentChoiceDB.getSegmentCode());

                                        if (abstractSegmentChoiceDB instanceof SegmentChoice) {

                                            SegmentChoice segmentChoice = (SegmentChoice) abstractSegmentChoiceDB;

                                            LOGGER.info("Removing ID: " + ancillaryId + ": " + GSON.toJson(segmentChoice));

                                            if ((ancillaryId.equalsIgnoreCase(
                                                    segmentChoice.getAncillaryId())
                                                    && linkedAncillaryId.equalsIgnoreCase(segmentChoice.getLinkedID()))
                                                    || SegmentCodeUtil.compareSegmentCodeWithoutTime(segmentCode, segmentChoice.getSegmentCode())) {
                                                i.remove();
                                                break;
                                            }
                                        } else {
                                            LOGGER.info(abstractSegmentChoiceDB.getClass().getName());
                                        }
                                    }
                                } catch (Exception ex) {
                                    LOGGER.error(ex.getMessage());
                                }
                            } else {
                                LOGGER.info("Linked Ancillary ID can not be null in order to remove an ancillary");
                            }
                        }
                    } else {
                        LOGGER.info("Ancillary ID can not be null in order to remove an ancillary");
                    }
                } else {
                    LOGGER.info("This ancillary is not currently part of the reservation");
                }
            } else {
                LOGGER.info("Already paid ancillaries can not be removed");
            }
        }

        return deleted;
    }

    /**
     *
     * @param bookedTraveler
     * @param segmentCode
     * @param recordLocator
     * @param cartId
     * @param serviceCallList
     * @throws Exception
     */
    public void removeUpgradeAncillaryFromReservation(
            BookedTraveler bookedTraveler, String segmentCode,
            String recordLocator, String cartId,
            List<ServiceCall> serviceCallList
    ) throws Exception {

        Iterator<AbstractAncillary> itr = bookedTraveler.getAncillaries().getCollection().iterator();
        while (itr.hasNext()) {
            AbstractAncillary abstractAncillary = itr.next(); // must be called before you
            // can call itr.remove()

            if (abstractAncillary instanceof CabinUpgradeAncillary) {

                CabinUpgradeAncillary cabinUpgradeAncillary = (CabinUpgradeAncillary) abstractAncillary;

                // only the ancillaries that are currently part of reservation
                // can have a quantity less or equal to zero
                // the new ancillaries are removed from shopping cart on PUT
                // operations
                if (null != cabinUpgradeAncillary.getSegmentCode()
                        && cabinUpgradeAncillary.getSegmentCode().equalsIgnoreCase(segmentCode)) {

                    UpdateReservationRQ updateReservationRQ;
                    updateReservationRQ = AEUtil.getDeleteReservationRQ(
                            bookedTraveler, cabinUpgradeAncillary.getAncillaryId(), recordLocator
                    );

                    LOGGER.info(recordLocator, cartId, "Update Reservation: Delete AE RQ");
                    LOGGER.info(recordLocator, cartId, GSON.toJson(updateReservationRQ));

                    UpdateReservationRS updateReservationRS;
                    updateReservationRS = updateReservationService.deleteAE(
                            updateReservationRQ,
                            serviceCallList
                    );

                    LOGGER.info(recordLocator, cartId, "Update Reservation: Delete AE RS");
                    LOGGER.info(recordLocator, cartId, GSON.toJson(updateReservationRS));

                    //this call will throw an exception if something was wrong
                    AEUtil.checkUpdateReservationErrors(updateReservationRS);

                    // remove ancillary from DB
                    itr.remove();
                    break;
                }
            }
        }
    }

    /**
     * This method will remove the seatAncillary and the segmentChoice from the
     * collection when the remove AE call was success
     *
     * @param bookedTraveler
     * @param segmentCode
     * @param recordLocator
     * @param cartId
     * @param serviceCallList
     * @throws Exception
     */
    public void removeSeatPaidTravelerAncillaryFromReservation(
            BookedTraveler bookedTraveler, String segmentCode,
            String recordLocator, String cartId,
            List<ServiceCall> serviceCallList
    ) throws Exception {
        boolean removeSeatAncillary = true;
        boolean removeSegmentChoice = true;

        this.removeSeatPaidTravelerAncillaryFromReservation(
                bookedTraveler, segmentCode, recordLocator,
                cartId, removeSeatAncillary, removeSegmentChoice,
                serviceCallList
        );
    }

    /**
     *
     * @param bookedTraveler
     * @param segmentCode
     * @param recordLocator
     * @param cartId
     * @param removeSeatAncillary
     * @param removeSegmentChoice
     * @param serviceCallList
     * @throws Exception
     */
    public void removeSeatPaidTravelerAncillaryFromReservation(
            BookedTraveler bookedTraveler, String segmentCode,
            String recordLocator, String cartId,
            boolean removeSeatAncillary, boolean removeSegmentChoice,
            List<ServiceCall> serviceCallList
    ) throws Exception {

        if (null == segmentCode || segmentCode.isEmpty()) {
            LOGGER.info("Itinerary code can not be null in order to remove an ancillary");
            return;
        }

        Iterator<AbstractAncillary> itr = bookedTraveler.getSeatAncillaries().getCollection().iterator();
        while (itr.hasNext()) {
            AbstractAncillary abstractAncillary = itr.next(); // must be called before you
            // can call itr.remove()

            if (abstractAncillary instanceof PaidTravelerAncillary) {
                PaidTravelerAncillary paidTravelerAncillary = (PaidTravelerAncillary) abstractAncillary;

                if (paidTravelerAncillary.isPartOfReservation()) {

                    String linkedAncillaryId = paidTravelerAncillary.getLinkedID();
                    String ancillaryId = paidTravelerAncillary.getAncillaryId();

                    if (null != ancillaryId && !ancillaryId.isEmpty()) {

                        if (SegmentCodeUtil.compareSegmentCodeWithoutTime(
                                segmentCode, paidTravelerAncillary.getSegmentCodeAux()
                        )) {

                            UpdateReservationRQ updateReservationRQ;
                            updateReservationRQ = AEUtil.getDeleteReservationRQ(
                                    bookedTraveler, ancillaryId, recordLocator
                            );

                            log(recordLocator, cartId, "Update Reservation: Delete AE RQ");
                            log(recordLocator, cartId, GSON.toJson(updateReservationRQ));

                            UpdateReservationRS updateReservationRS;
                            updateReservationRS = updateReservationService.deleteAE(
                                    updateReservationRQ,
                                    serviceCallList
                            );

                            log(recordLocator, cartId, "Update Reservation: Delete AE RS");
                            log(recordLocator, cartId, GSON.toJson(updateReservationRS));

                            //this call will throw an exception if something was wrong
                            AEUtil.checkUpdateReservationErrors(updateReservationRS);

                            if (removeSeatAncillary) {
                                // remove ancillary from DB
                                itr.remove();
                            }

                            if (removeSegmentChoice) {
                                if (null != linkedAncillaryId && !linkedAncillaryId.isEmpty()) {

                                    // remove seat from DB if ancillary is a seat paid
                                    Iterator<AbstractSegmentChoice> i;
                                    i = bookedTraveler.getSegmentChoices().getCollection().iterator();
                                    while (i.hasNext()) {
                                        // must be called before you can call i.remove()
                                        AbstractSegmentChoice abstractSegmentChoiceDB = i.next();
                                        if (abstractSegmentChoiceDB instanceof PaidSegmentChoice) {
                                            PaidSegmentChoice paidSegmentChoice = (PaidSegmentChoice) abstractSegmentChoiceDB;

                                            if (ancillaryId.equalsIgnoreCase(paidSegmentChoice.getAncillaryId())
                                                    && linkedAncillaryId.equalsIgnoreCase(paidSegmentChoice.getLinkedID())) {

                                                i.remove();

                                                break;
                                            }
                                        }
                                    }
                                } else {
                                    LOGGER.info("Linked Ancillary ID can not be null in order to remove an ancillary");
                                }
                            }
                        }
                    } else {
                        LOGGER.info("Ancillary ID can not be null in order to remove an ancillary");
                    }
                }
            }
        }
    }

    /**
     *
     * @param updateReservationRQ
     * @param serviceCallList
     * @throws Exception
     */
    public void updateReservationOSI(
            UpdateReservationRQ updateReservationRQ,
            List<ServiceCall> serviceCallList
    ) throws Exception {
        List<ReservationItem> listOfReservationItems = null;
        byte[] rawContent = updateReservationRQ.getPnrContent().getRawContent();
        String recordLocator = new String(rawContent);
        log(recordLocator, null, "Updating reservation on SABRE system");

        try {
            log(recordLocator, null, "Invoking data to service");
            updateReservationService.updateAE(
                    recordLocator,
                    listOfReservationItems,
                    serviceCallList
            );
        } catch (Exception e) {
            logError(recordLocator, null, "Error to update reservation and consume the SABRE service");
            logError(recordLocator, null, "Error: " + e.getMessage());
        }
    }

    /**
     *
     * @param recordLocator
     * @param ffcProgram
     * @param ffcNumber
     * @param nameRefNumber
     * @param serviceCallList
     * @return 
     * @throws Exception
     */
    public String createOSI(
            String recordLocator,
            String ffcProgram,
            String ffcNumber,
            String nameRefNumber,
            List<ServiceCall> serviceCallList
    ) throws Exception {
        try {
            UpdateReservationRS createOSI = updateReservationService.createOSI(
                    recordLocator,
                    ffcProgram,
                    ffcNumber,
                    nameRefNumber,
                    serviceCallList
            );

            return updateReservationService.toString(createOSI);
        } catch (Exception e) {
            logError(recordLocator, null, "Cannot create the OSI in SABRE, Error: " + e.getMessage());
            return "ERROR: " + e.getMessage();
        }
    }

    /**
     *
     * @param recordLocator
     * @param ssr
     * @param serviceCallList
     * @return 
     * @throws Exception
     */
    public String createSSR(
            String recordLocator,
            String ssr,
            List<ServiceCall> serviceCallList
    ) throws Exception {
        try {
            UpdateReservationRS createSSR = updateReservationService.createSSR(
                    recordLocator,
                    ssr,
                    serviceCallList
            );
            
            return updateReservationService.toString(createSSR);
        } catch (Exception e) {
            logError(recordLocator, null, "Cannot create the SSR in SABRE, Error: " + e.getMessage());
            return "ERROR: " + e.getMessage();
        }
    }

    /**
     *
     * @param authorizationResult
     * @param recordLocator
     * @param cartId
     * @param serviceCallList
     */
    public void addRemark(
            AuthorizationResult authorizationResult,
            String recordLocator,
            String cartId,
            List<ServiceCall> serviceCallList
    ) {
        try {
            ArrayList<String> remarks = new ArrayList<>();

            remarks.addAll(authorizationResult.getRemarks());
            if (null != authorizationResult.getCardReaderResult()) {
                remarks.add(authorizationResult.getCardReaderResult().getRemarks());
            }

            addRemark(remarks, recordLocator, cartId, "CHECKIN_API", serviceCallList);

        } catch (Exception ex) {
            LOGGER.error(LogUtil.getLogError(SERVICE, "Could not add remarks", recordLocator, cartId, ex));
        }
    }

    /**
     *
     * @param authorizationResult
     * @param recordLocator
     * @param cartId
     * @param serviceCallList
     */
    @SimpleAsync
    public void addRemarkSync(
            AuthorizationResult authorizationResult,
            String recordLocator,
            String cartId,
            List<ServiceCall> serviceCallList
    ) {
        try {
            ArrayList<String> remarks = new ArrayList<>();

            remarks.addAll(authorizationResult.getRemarks());
            if (null != authorizationResult.getCardReaderResult()) {
                remarks.add(authorizationResult.getCardReaderResult().getRemarks());
            }

            addRemark(remarks, recordLocator, cartId, "CHECKIN_API", serviceCallList);

        } catch (Exception ex) {
            LOGGER.error(LogUtil.getLogError(SERVICE, "Could not add remarks", recordLocator, cartId, ex));
        }
    }

    /**
     *
     * @param remarks
     * @param recordLocator
     * @param cartId
     * @param receibeFrom
     * @param serviceCallList
     */
    private void addRemark(
            ArrayList<String> remarks,
            String recordLocator,
            String cartId,
            String receibeFrom,
            List<ServiceCall> serviceCallList
    ) {
        try {
            updateReservationService.createRemark(recordLocator, remarks, receibeFrom, serviceCallList);
        } catch (Exception ex) {
            logError(recordLocator, cartId, "Update Reservation: Error adding remarks, " + ex.getMessage());
        }
    }
    
    /**
     * 
     * @param recordLocator
     * @param remark 
     * @return  
     */
    public boolean addRemarkPayments(String recordLocator, String remark) {
        List<String> remarks = new ArrayList<>();
        remarks.add(remark);
        try {
            return updateReservationService.createRemark(recordLocator, remarks, "BOOKING_API");
        } catch (Exception ex) {
            logError(recordLocator, null, "Update Reservation: Error adding remarks, " + ex.getMessage());
            return false;
        }
    }

    /**
     * @return the pnrLookupDao
     */
    public IPNRLookupDao getPnrLookupDao() {
        return pnrLookupDao;
    }

    /**
     * @param pnrLookupDao the pnrLookupDao to set
     */
    public void setPnrLookupDao(IPNRLookupDao pnrLookupDao) {
        this.pnrLookupDao = pnrLookupDao;
    }

    /**
     *
     * @param recordLocator
     * @param cartId
     * @param title
     * @param updateReservationRQ
     * @param updateReservationRS
     */
    public void log(
            String recordLocator, String cartId, String title,
            UpdateReservationRQ updateReservationRQ,
            UpdateReservationRS updateReservationRS
    ) {
        try {
            StringBuilder json = new StringBuilder();
            json.append("\n").append("Update reservation Service: ").append(title).append("\n");
            json.append("recordLocator: ").append(recordLocator).append("\n");
            json.append("cartId: ").append(cartId).append("\n");
            json.append("RQ: ").append(GSON.toJson(updateReservationRQ)).append("\n");
            json.append("RS: ").append(GSON.toJson(updateReservationRS)).append("\n");
            LOGGER.debug(json.toString());

            StringBuilder sb = new StringBuilder();
            sb.append("\n").append("Update reservation Service: ").append(title).append("\n");
            sb.append("recordLocator: ").append(recordLocator).append("\n");
            sb.append("cartId: ").append(cartId).append("\n");
            sb.append("RQ: ").append(updateReservationService.toString(updateReservationRQ)).append("\n");
            sb.append("RS: ").append(updateReservationService.toString(updateReservationRS)).append("\n");

            LOGGER.info(sb.toString());
        } catch (Exception ex) {
            //LOG.error(ex.getMessage());
        }
    }

    /**
     *
     * @param recordLocator
     * @param cartId
     * @param title
     */
    public void log(String recordLocator, String cartId, String title) {
        try {

            StringBuilder sb = new StringBuilder();
            sb.append("\n").append("Update reservation Service: ").append(title).append("\n");
            sb.append("recordLocator: ").append(recordLocator).append("\n");
            sb.append("cartId: ").append(cartId).append("\n");

            LOGGER.info(sb.toString());
        } catch (Exception ex) {
            //LOG.error(ex.getMessage());
        }
    }

    /**
     *
     * @param recordLocator
     * @param cartId
     * @param title
     * @param updateReservationRQ
     * @param message
     */
    public void logError(
            String recordLocator, String cartId, String title,
            UpdateReservationRQ updateReservationRQ, String message
    ) {
        try {
            StringBuilder json = new StringBuilder();
            json.append("\n").append("Update reservation Service: ").append(title).append("\n");
            json.append("recordLocator: ").append(recordLocator).append("\n");
            json.append("cartId: ").append(cartId).append("\n");
            json.append("RQ: ").append(GSON.toJson(updateReservationRQ)).append("\n");
            json.append("RS: ").append(message).append("\n");
            LOGGER.debug(json.toString());

            StringBuilder sb = new StringBuilder();
            sb.append("\n").append("Update reservation Service: ").append(title).append("\n");
            sb.append("recordLocator: ").append(recordLocator).append("\n");
            sb.append("cartId: ").append(cartId).append("\n");
            sb.append("RQ: ").append(updateReservationService.toString(updateReservationRQ)).append("\n");
            sb.append("RS: ").append(message).append("\n");

            LOGGER.error(sb.toString());
        } catch (Exception ex) {
            //LOG.error(ex.getMessage());
        }
    }

    /**
     *
     * @param recordLocator
     * @param cartId
     * @param title
     */
    public void logError(String recordLocator, String cartId, String title) {
        try {

            StringBuilder sb = new StringBuilder();
            sb.append("\n").append("Update reservation Service: ").append(title).append("\n");
            sb.append("recordLocator: ").append(recordLocator).append("\n");
            sb.append("cartId: ").append(cartId).append("\n");

            LOGGER.error(sb.toString());
        } catch (Exception ex) {
            //LOG.error(ex.getMessage());
        }
    }

    /**
     *
     * @param recordLocator
     * @param cartId
     * @param title
     */
    public void logDebug(String recordLocator, String cartId, String title) {
        try {

            StringBuilder sb = new StringBuilder();
            sb.append("\n").append("Update reservation Service: ").append(title).append("\n");
            sb.append("recordLocator: ").append(recordLocator).append("\n");
            sb.append("cartId: ").append(cartId).append("\n");

            LOGGER.debug(sb.toString());
        } catch (Exception ex) {
            //LOG.error(ex.getMessage());
        }
    }
}
