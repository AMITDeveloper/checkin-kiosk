package com.am.checkin.web.service;



import com.aeromexico.commons.model.IMetadataFactory;
import com.aeromexico.commons.model.Metadata;
import com.aeromexico.commons.model.IRootAndMetadataService;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@ApplicationScoped
public class RootAndMetadataService implements IRootAndMetadataService {

    @Inject
    private IMetadataFactory metadataFactory;    

    /**
     * @return the metadata
     */
    @Override
    public Metadata getRootAndMetadata() {        

        return metadataFactory.getInstance();
    }

    /**
     * @return the metadataFactory
     */
    public IMetadataFactory getMetadataFactory() {
        return metadataFactory;
    }

    /**
     * @param metadataFactory the metadataFactory to set
     */
    public void setMetadataFactory(IMetadataFactory metadataFactory) {
        this.metadataFactory = metadataFactory;
    }
}
