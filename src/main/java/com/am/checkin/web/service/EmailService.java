package com.am.checkin.web.service;

import com.aeromexico.cloudfiles.AMCloudFile;
import com.aeromexico.commons.bigquery.prototypes.DatePrototype;
import com.aeromexico.commons.model.AbstractSegmentChoice;
import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.PNR;
import com.aeromexico.commons.model.PNRCollection;
import com.aeromexico.commons.model.PaymentLinkedList;
import com.aeromexico.commons.model.SegmentDocument;
import com.aeromexico.commons.model.SegmentDocumentList;
import com.aeromexico.commons.model.SystemPropertiesFactory;

import static com.aeromexico.commons.model.mailing.BigQueryErrors.CAST_CHK_ERROR;
import static com.aeromexico.commons.model.mailing.BigQueryErrors.ELEMENT_REQUIRED_CHK_ERROR;
import static com.aeromexico.commons.model.mailing.BigQueryErrors.EMPTY_DOCUMENT_LIST_CHK_ERROR;
import static com.aeromexico.commons.model.mailing.BigQueryErrors.NULL_PNR_CHK_ERROR;

import com.aeromexico.commons.model.mailing.CreateBoardingPassRQ;
import com.aeromexico.commons.model.mailing.DocumentTypeEnum;
import com.aeromexico.commons.model.mailing.Infant;
import com.aeromexico.commons.model.mailing.Leg;

import static com.aeromexico.commons.model.mailing.MailingConstants.BRAZIL_STORE_CODE;
import static com.aeromexico.commons.model.mailing.MailingConstants.FRENCH_STORE_CODE;
import static com.aeromexico.commons.model.mailing.MailingConstants.INCIDENT;
import static com.aeromexico.commons.model.mailing.MailingConstants.LANG;
import static com.aeromexico.commons.model.mailing.MailingConstants.LANGUAGE_EN;
import static com.aeromexico.commons.model.mailing.MailingConstants.LANGUAGE_ES;
import static com.aeromexico.commons.model.mailing.MailingConstants.LANGUAGE_FR;
import static com.aeromexico.commons.model.mailing.MailingConstants.LANGUAGE_PT;
import static com.aeromexico.commons.model.mailing.MailingConstants.MEXICO_STORE_CODE;
import static com.aeromexico.commons.model.mailing.MailingConstants.NOT_FOUND;
import static com.aeromexico.commons.model.mailing.MailingConstants.NULL_TRANSACTION;
import static com.aeromexico.commons.model.mailing.MailingConstants.POS;
import static com.aeromexico.commons.model.mailing.MailingConstants.STORE;
import static com.aeromexico.commons.model.mailing.MailingConstants.USA_STORE_CODE;

import com.aeromexico.commons.model.mailing.Passenger;
import com.aeromexico.commons.model.mailing.PaymentTable;
import com.aeromexico.commons.model.mailing.Segment;
import com.aeromexico.commons.model.rq.EmailBoardingPassRQ;
import com.aeromexico.commons.model.rq.EmailReceiptRQ;
import com.aeromexico.commons.model.utils.FileNamingUtil;
import com.aeromexico.commons.web.types.PosType;
import com.aeromexico.commons.web.types.SeatCharacteristicsType;
import com.aeromexico.commons.web.util.SegmentCodeUtil;
import com.aeromexico.dao.async.SimpleAsync;
import com.aeromexico.sharedservices.clients.HttpClientSingleton;
import com.am.checkin.web.event.dispatcher.SendEmailDispatcher;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named
@ApplicationScoped
public class EmailService {

    private static Gson gson = new Gson();
    private static final Logger logger = LoggerFactory.getLogger(EmailService.class);
    private static final String CREATE_BOARDING_PASS_ENDPOINT = "/boarding-pass/create";
    private static final String SEND_BOARDING_PASS_ENDPOINT = "/boarding-pass/email";
    private static final String SEND_CHECKIN_RECEIPT_ENDPOINT = "/receipt/email";
    private String environment;

    @Inject
    private SystemPropertiesFactory systemPropertiesFactory;

    @Inject
    private SendEmailDispatcher sendEmailDispatcher;

    @PostConstruct
    public void init() {
        if (null == systemPropertiesFactory) {
            systemPropertiesFactory = new SystemPropertiesFactory();
        }
    }

    /**
     * Send email when boarding pass already created
     *
     * @param emailBoardingPassRQ
     * @throws java.lang.Exception
     */
    public void sendBoardingPassMailing(EmailBoardingPassRQ emailBoardingPassRQ) throws Exception {

        logger.info("EMAIL-RQ BP: {}", emailBoardingPassRQ);

        com.aeromexico.commons.model.mailing.EmailSendRQ email = new com.aeromexico.commons.model.mailing.EmailSendRQ();

        try {
            email.setPnr(emailBoardingPassRQ.getBoardingPassId());
            email.setEmails(emailBoardingPassRQ.getEmailList());
            email.setLanguage(emailBoardingPassRQ.getLanguage());
            email.setStore(emailBoardingPassRQ.getStore());
            email.setChannel(emailBoardingPassRQ.getPos());
            email.setLegCode(emailBoardingPassRQ.getLegCode());
            email.setDocumentType(DocumentTypeEnum.getDocumentTypeEnum(
                    emailBoardingPassRQ.getPos(),
                    DocumentTypeEnum.BOARDING_PASS));
            try {
                email.setCartId(emailBoardingPassRQ.getCartId());
            } catch (Exception ign) {
            }
        } catch (NullPointerException e) {
            throw new Exception(ELEMENT_REQUIRED_CHK_ERROR);
        }

        // If ticketNumbers is null or empty, will send all boarding passes
        String[] ticketNumbers = StringUtils.isNotBlank(emailBoardingPassRQ.getTicketNumbers()) ? emailBoardingPassRQ.getTicketNumbers().split(",") : null;

        if (ticketNumbers != null && ticketNumbers.length > 0) {
            email.setTicketNumbers(new ArrayList<>(Arrays.asList(ticketNumbers)));
        }

        sendAndValidate(SEND_BOARDING_PASS_ENDPOINT, email.toString(), null);
    }

    public void sendBoardingPassReceiptMailing(EmailReceiptRQ emailReceiptRQ, String userAuth)
            throws Exception {
        com.aeromexico.commons.model.mailing.EmailSendRQ email = new com.aeromexico.commons.model.mailing.EmailSendRQ();

        logger.info("EMAIL CHK RECIEPT RQ: {} USER AUTH: {}", emailReceiptRQ, userAuth);
        try {
            email.setStore(emailReceiptRQ.getStore());
            email.setLanguage(emailReceiptRQ.getLanguage());
            email.setEmails(emailReceiptRQ.getEmailList());
            email.setChannel(emailReceiptRQ.getPos());

            email.setDocumentType(DocumentTypeEnum.getDocumentTypeEnum(
                    emailReceiptRQ.getPos(),
                    DocumentTypeEnum.PAYMENT_RECEIPT));
            email.setCartId(emailReceiptRQ.getCartId() != null
                    ? emailReceiptRQ.getCartId()
                    : NULL_TRANSACTION);
            try {
                email.setPnr(emailReceiptRQ.getPnr());
            } catch (Exception ign) {
            }
        } catch (NullPointerException e) {
            throw new Exception(ELEMENT_REQUIRED_CHK_ERROR);
        }

        if (email.getPnr() == null && userAuth != null && userAuth.indexOf("reservation=\"") != 0) {
            int position = userAuth.indexOf("reservation=\"") + 13;
            email.setPnr(userAuth.substring(position, position + 6));
        }

        if (email.getPnr() != null) {
            sendAndValidate(SEND_CHECKIN_RECEIPT_ENDPOINT, email.toString(), null);
        } else {
            logger.error("Failed to send out receipt email. NULL PNR.");
            throw new Exception(NULL_PNR_CHK_ERROR);
        }
    }

    public File downloadBoardingPass(String pnr, String ticketNumber, String language) throws Exception {
        File pdfFile = null;

        String templatePath = systemPropertiesFactory.getInstance().getMaillingTemporalFiles();
        try {
            File folder = new File(templatePath);
            File[] files = folder.listFiles();
            for (File file : files) {
                if (FileNamingUtil.matchesBoardingPassPdfFile(file.getName(), ticketNumber, pnr)) {
                    pdfFile = file;
                    break;
                }
            }
        } catch (Exception ex) {
            logger.error("Error to find files in FS for email, {}", ex);
        }
        if (pdfFile == null) {
            //Bring File from RackSpace
            AMCloudFile amCloudFile = new AMCloudFile();
            List<File> cloudFiles = amCloudFile.getAllBoarding(templatePath, pnr);
            //List<File> cloudFiles = amCloudFile.getFilesByPrefixByPnr(templatePath, pnr);
            for (File file : cloudFiles) {
                if (FileNamingUtil.matchesBoardingPassPdfFile(file.getName(), ticketNumber, pnr)) {
                    pdfFile = file;
                }
            }
        }
        if (pdfFile == null) {
            //Send error pdf File
            if (language == null || !language.equals("en")) {
                pdfFile = new File(templatePath + "Error_MX.pdf");
                if (!pdfFile.exists()) {
                    FileUtils.copyInputStreamToFile(EmailService.class.getResourceAsStream("/email/Error_MX.pdf"), pdfFile);
                }
            } else {
                pdfFile = new File(templatePath + "Error_US.pdf");
                if (!pdfFile.exists()) {
                    FileUtils.copyInputStreamToFile(EmailService.class.getResourceAsStream("/email/Error_US.pdf"), pdfFile);
                }

            }
        }
        return pdfFile;
    }

    /**
     * * @deprecated As of release 3.4.11, replaced by
     * {@link #downloadMobileBP(String, String, String, String, String, String)}
     *
     * @param pnr
     * @param ticketNumber
     * @param origin
     * @param destination
     * @param date
     * @param language
     * @return
     * @throws Exception
     */
    public File downloadMobileBoardingPass(String pnr, String ticketNumber,
                                           String origin, String destination,
                                           String date, String language) throws Exception {
        File mobileBoardingPass = null;

        String templatePath = systemPropertiesFactory.getInstance().getMaillingTemporalFiles();
        try {
            File folder = new File(templatePath);
            File[] files = folder.listFiles((file, s) -> FileNamingUtil.matchesMobileBoardingPassFile(file.getName(), ticketNumber, pnr, origin, destination, date));
            mobileBoardingPass = files.length >= 1 ? files[0] : null;
        } catch (Exception ex) {
            logger.error("Error to find files in FS for email, {}", ex);
        }
        if (mobileBoardingPass == null) {
            //Bring File from RackSpace
            AMCloudFile amCloudFile = new AMCloudFile();
            List<File> cloudFiles = amCloudFile.getAllBoarding(templatePath, pnr);
            for (File file : cloudFiles) {
                if (FileNamingUtil.matchesMobileBoardingPassFile(file.getName(), ticketNumber, pnr, origin, destination, date)) {
                    mobileBoardingPass = file;
                }
            }
        }
        return mobileBoardingPass;
    }

    /**
     * Get mobile boarding pass
     *
     * @param pnr          Strnig
     * @param ticketNumber String
     * @param origin       String
     * @param destination  String
     * @param date
     * @param language     String
     * @return
     */
    public File downloadMobileBP(String pnr, String ticketNumber,
                                 String origin, String destination,
                                 String date, String language) {
        File mobileBoardingPass = null;
        //Bring File from RackSpace
        AMCloudFile amCloudFile = new AMCloudFile();

        String templatePath = systemPropertiesFactory.getInstance().getMaillingTemporalFiles();

        List<File> cloudFiles = null;
        try {
            cloudFiles = amCloudFile.getFilesByPrefixByPnr(templatePath, pnr, false);
        } catch (IOException ex) {
            logger.error("ERROR TO DOWNLOADS FILES FROM CLOUD: {}", ex.getMessage());
        }

        if (cloudFiles != null) {
            for (File file : cloudFiles) {
                if (FileNamingUtil.matchesMobileBoardingPassFile(file.getName(), ticketNumber, pnr, origin, destination, date)) {
                    mobileBoardingPass = file;
                }
            }
        }

        return mobileBoardingPass;
    }

    /**
     * Method to send information to create boarding pass
     *
     * @param pnrCollection
     */
    public void createBoardingPass(PNRCollection pnrCollection) throws Exception {
        String language = null;
        String store = null;

        try {
            store = pnrCollection.getCollection().get(0).getCarts().getCollection().get(0).getMeta().getPos().getStore();
        } catch (Exception e) {
            store = USA_STORE_CODE;
        }

        if (null == pnrCollection.getCollection().get(0).getCarts().getCollection().get(0).getMeta().getPos()
                || pnrCollection.getCollection().get(0).getCarts().getCollection().get(0).getMeta().getPos().getLanguage() == null
                || pnrCollection.getCollection().get(0).getCarts().getCollection().get(0).getMeta().getPos().getLanguage().isEmpty()) {

            //default language
            switch (store.toLowerCase()) {
                case MEXICO_STORE_CODE:
                    language = LANGUAGE_ES;
                    break;
                case BRAZIL_STORE_CODE:
                    language = LANGUAGE_PT;
                    break;
                case FRENCH_STORE_CODE:
                    language = LANGUAGE_FR;
                    break;
                default:
                    language = LANGUAGE_EN;
                    break;
            }

        } else {
            language = pnrCollection.getCollection().get(0).getCarts().getCollection().get(0).getMeta().getPos().getLanguage();
        }

        MultivaluedMap<String, Object> queryParams = new MultivaluedHashMap<String, Object>();
        queryParams.add(LANG, language);
        queryParams.add(STORE, store);
        try {
            queryParams.add(POS, pnrCollection.getPos());
        } catch (NullPointerException e) {
            queryParams.add(POS, PosType.WEB.toString());
        }

        logger.info("Cast PNRCollection to CreateBPRQ: {} Languague: {}", pnrCollection, language);
        List<CreateBoardingPassRQ> boardingPasses = null;

        try {
            boardingPasses = castCreateBoardingPassRQ(pnrCollection, language);
        } catch (Exception e) {
            throw new Exception(CAST_CHK_ERROR + ExceptionUtils.getStackTrace(e));
        }

        logger.info("Boarding passes mailing RQ: {}", boardingPasses.toString());

        boardingPasses = validateCastBPRQ(boardingPasses);

        if (boardingPasses != null && boardingPasses.size() > 0) { //Check if there boarding passes requests   
            sendAndValidate(CREATE_BOARDING_PASS_ENDPOINT, boardingPasses.toString(), queryParams);
        } else {
            logger.info("NO DOCUMENTS LIST IN PNR COLLECTION");
            throw new Exception(EMPTY_DOCUMENT_LIST_CHK_ERROR);
        }
    }

    /**
     * Cast PNRCollection to CreateBoardingPassRQ object
     *
     * @param pnrCollection
     * @param lang          String
     * @return
     */
    public static List<CreateBoardingPassRQ> castCreateBoardingPassRQ(
            PNRCollection pnrCollection,
            String lang
    ) {
        List<CreateBoardingPassRQ> boardingPassesRQ = new ArrayList<>();

        for (PNR pnr : pnrCollection.getCollection()) {
            for (CartPNR cart : pnr.getCarts().getCollection()) {

                CreateBoardingPassRQ createBoardingPassRQ = null;

                createBoardingPassRQ = new CreateBoardingPassRQ();
                createBoardingPassRQ.setPnr(pnr.getPnr());

                //validate if existe travelers 
                createBoardingPassRQ.setCartId(cart.getMeta().getCartId());
                Map<String, String> paxEmds = new HashMap<>();

                // The documented carts must have at least a pectab pass list

                BookedLeg bookedLeg = pnr.getLegs().getBookedLegByLegCode(cart.getLegCode());

                boolean areThereBoardingDocumentsForThisLeg = false;

                Leg leg = new Leg();
                List<Segment> segments = new ArrayList<>();

                if (null != bookedLeg) {

                    leg.setLegCode(bookedLeg.getSegments().getLegCode());

                    for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) { //Iterar por segmento

                        com.aeromexico.commons.model.Segment pnrSegment = bookedSegment.getSegment();

                        Segment segment = new Segment();

                        List<Passenger> passengers = new ArrayList<>();

                        for (BookedTraveler bookedTraveler : cart.getTravelerInfo().getCollection()) { //Iterate by passenger
                            //Validar si ya hizo checkin y contiene barCode y qrCode
                            if (bookedTraveler.isCheckinStatus()
                                    && bookedTraveler.getSegmentDocumentsList() != null
                                    && !bookedTraveler.getSegmentDocumentsList().isEmpty()
                                    && bookedTraveler.getSegmentDocumentsList().get(0) != null
                                    && bookedTraveler.getSegmentDocumentsList().get(0).getBarcode() != null
                                    && !bookedTraveler.getSegmentDocumentsList().get(0).getBarcode().isEmpty()
                                    && bookedTraveler.getSegmentDocumentsList().get(0).getBarcodeImage() != null
                                    && !bookedTraveler.getSegmentDocumentsList().get(0).getBarcodeImage().isEmpty()
                                    && bookedTraveler.getSegmentDocumentsList().get(0).getQrcodeImage() != null
                                    && !bookedTraveler.getSegmentDocumentsList().get(0).getQrcodeImage().isEmpty()

                            ) {

                                boolean addPassenger = false;

                                Passenger passenger = new Passenger();

                                //Populate map with ticker number by passenger
                                paxEmds.put(
                                        bookedTraveler.getFirstName() + " " + bookedTraveler.getLastName(),
                                        bookedTraveler.getTicketNumber()
                                );

                                segment.setFreeBaggageAllowance(bookedTraveler.getFreeBaggageAllowance());

                                passenger.setFirstName(bookedTraveler.getFirstName());
                                passenger.setLastName(bookedTraveler.getLastName());

                                if (bookedTraveler.getFrequentFlyerNumber() != null) {
                                    passenger.setFfNumber(bookedTraveler.getFrequentFlyerNumber());
                                }

                                if (bookedTraveler.getFrequentFlyerProgram() != null) {
                                    passenger.setFfProgram(bookedTraveler.getFrequentFlyerProgram());
                                }

                                if (bookedTraveler.getCorporateFrequentFlyerNumber() != null) {
                                    passenger.setCorporateffNumber(bookedTraveler.getCorporateFrequentFlyerNumber());
                                }

                                passenger.setTicketNumber(bookedTraveler.getTicketNumber());

                                if (!bookedTraveler.getSegmentChoices().getCollection().isEmpty()) {
                                    AbstractSegmentChoice abstractSegmentChoice = bookedTraveler.getSegmentChoices().getBySegmentCode(pnrSegment.getSegmentCode());

                                    if (null != abstractSegmentChoice && null != abstractSegmentChoice.getSeat()) {

                                        passenger.setSeatCode(abstractSegmentChoice.getSeat().getCode());

                                        if (null != abstractSegmentChoice.getSeat().getSeatCharacteristics()
                                                && !abstractSegmentChoice.getSeat().getSeatCharacteristics().isEmpty()) {
                                            passenger.setSeatType(abstractSegmentChoice.getSeat().
                                                    getSeatCharacteristics().get(0).name()
                                                    .equals(SeatCharacteristicsType.q.name())
                                                    ? SeatCharacteristicsType.M.name()
                                                    : abstractSegmentChoice.getSeat().
                                                    getSeatCharacteristics().get(0).name());
                                        }
                                    }
                                    passenger.setSeatSectionCode("-");
                                } else {
                                    passenger.setSeatCode("-");
                                    passenger.setSeatType("-");
                                    passenger.setSeatSectionCode("-");
                                }

                                try {
                                    passenger.setBookingCabin(bookedTraveler.getBookingClasses().
                                            getBookingClass(pnrSegment.getSegmentCode()).getBookingCabin());
                                    passenger.setBookingClass(bookedTraveler.getBookingClasses().
                                            getBookingClass(pnrSegment.getSegmentCode()).getBookingClass());
                                } catch (NullPointerException n) {
                                    //Segments not operated by AM
                                    passenger.setBookingCabin("Y");
                                    passenger.setBookingClass("-");
                                }

                                passenger.setSelectee(bookedTraveler.isSelectee());

                                if (bookedTraveler.getInfant() != null) {
                                    com.aeromexico.commons.model.Infant pnrInfant = bookedTraveler.getInfant();
                                    Infant infant = new Infant();

                                    infant.setFirstName(pnrInfant.getFirstName());
                                    infant.setLastName(pnrInfant.getLastName());
                                    infant.setTicketNumber(pnrInfant.getTicketNumber());
                                    SegmentDocument segmentDocumentInfant = getSegmentDocumentBySegmentCode(
                                            pnrInfant.getSegmentDocumentsList(),
                                            pnrSegment.getSegmentCode());

                                    if (segmentDocumentInfant != null
                                            && segmentDocumentInfant.getBarcode() != null
                                            && !segmentDocumentInfant.getBarcode().isEmpty()
                                            && segmentDocumentInfant.getBarcodeImage() != null
                                            && !segmentDocumentInfant.getBarcodeImage().isEmpty()
                                            && segmentDocumentInfant.getQrcodeImage() != null
                                            && !segmentDocumentInfant.getQrcodeImage().isEmpty()) {

                                        addPassenger = true;

                                        String chkNumber = segmentDocumentInfant.getCheckInNumber();
                                        if (chkNumber == null || chkNumber.isEmpty()) {
                                            infant.setCheckinControl(segmentDocumentInfant.getCheckInNumber());
                                        } else {
                                            infant.setCheckinControl(chkNumber);
                                        }

                                        infant.setBarcodeImage(segmentDocumentInfant.getBarcodeImage());
                                        infant.setQrcodeImage(segmentDocumentInfant.getQrcodeImage());
                                    }

                                    passenger.setInfant(infant);
                                }

                                SegmentDocument segmentDocument = getSegmentDocumentBySegmentCode(
                                        bookedTraveler.getSegmentDocumentsList(),
                                        pnrSegment.getSegmentCode()
                                );

                                if (segmentDocument != null
                                        && segmentDocument.getBarcode() != null
                                        && !segmentDocument.getBarcode().isEmpty()
                                        && segmentDocument.getBarcodeImage() != null
                                        && !segmentDocument.getBarcodeImage().isEmpty()
                                        && segmentDocument.getQrcodeImage() != null
                                        && !segmentDocument.getQrcodeImage().isEmpty()) {
                                    addPassenger = true;

                                    areThereBoardingDocumentsForThisLeg = true;

                                    passenger.setBarcodeImage(segmentDocument.getBarcodeImage());
                                    passenger.setQrcodeImage(segmentDocument.getQrcodeImage());
                                    passenger.setSkyPriority(segmentDocument.isSkyPriority());
                                    passenger.setTsaPreCheck(segmentDocument.isTsaPreCheck());
                                    passenger.setCheckinControl(segmentDocument.getCheckInNumber());

                                    if (!segmentDocument.isSkyPriority()
                                            && !segmentDocument.isSeamlessCheckin()
                                            && !"1".equalsIgnoreCase(segmentDocument.getBoardingZone())
                                            && !"2".equalsIgnoreCase(segmentDocument.getBoardingZone())
                                            && null != segmentDocument.getCardID()
                                            && ("AMXPLT".equalsIgnoreCase(segmentDocument.getCardID())
                                            || "AMXGLD".equalsIgnoreCase(segmentDocument.getCardID())
                                            || "SEPLT".equalsIgnoreCase(segmentDocument.getCardID())
                                            || "SEIFNT".equalsIgnoreCase(segmentDocument.getCardID()))) {
                                        segment.setBoardingZone("2");

                                        passenger.setPriorityBoarding(segmentDocument.isPriorityBoarding());
                                        passenger.setCardID(segmentDocument.getCardID());
                                    } else {
                                        segment.setBoardingZone(segmentDocument.getBoardingZone());
                                    }
                                }

                                if (addPassenger) {
                                    passengers.add(passenger);
                                }
                            }
                        }

                        segment.setPassengers(passengers);

                        segment.setCode(pnrSegment.getSegmentCode());
                        segment.setDepartureAirport(pnrSegment.getDepartureAirport());
                        segment.setDepartureDateTime(pnrSegment.getDepartureDateTime());
                        segment.setArrivalAirport(pnrSegment.getArrivalAirport());
                        segment.setArrivalDateTime(pnrSegment.getArrivalDateTime());
                        segment.setFlightDurationInMinutes(pnrSegment.getFlightDurationInMinutes());
                        segment.setCarrier(pnrSegment.getOperatingCarrier());
                        segment.setFlightCode(pnrSegment.getOperatingFlightCode());
                        segment.setBoardingTime(pnrSegment.getBoardingTime());
                        segment.setBoardingTerminal(pnrSegment.getBoardingTerminal());
                        segment.setBoardingGate(pnrSegment.getBoardingGate());

                        segments.add(segment);
                    }
                }

                leg.setSegments(segments);
                leg.setFlightDurationInMinutes(bookedLeg.getSegments().getTotalFlightTimeInMinutes());

                PaymentLinkedList paymentLinkedList = cart.getPaymentLinkedList();
                //Check if there payment
                if (paymentLinkedList != null
                        && paymentLinkedList.size() > 0
                        && paymentLinkedList.get(0).getAncillaries() != null
                        && paymentLinkedList.get(0).getAncillaries().size() > 0) {

                    PaymentTable paymentTable = new PaymentTable();

                    paymentTable.setPaxEMDs(paxEmds);
                    paymentTable.setCurrencyCode(cart.getMeta().getTotal().getCurrency().getCurrencyCode());
                    paymentTable.sumarizeAncillariesCheckin(paymentTable, cart.getTravelerInfo().getCollection());
                    paymentTable.sumarizeSeatsCheckin(paymentTable, cart.getTravelerInfo().getCollection());
                    paymentTable.setOneWayFlight(pnr.getLegs().getCollection().size() == 1);
                    paymentTable.setTransactionTime(DatePrototype.convertDateTime(LocalDateTime.ofInstant(
                            paymentLinkedList.get(0).getTransactionDate().toInstant(),
                            ZoneId.systemDefault()), lang));

                    createBoardingPassRQ.setPaymentTable(paymentTable);
                }

                //Validation just documented one leg
                if (areThereBoardingDocumentsForThisLeg || null != createBoardingPassRQ.getPaymentTable()) { //Only for payment receipt
                    createBoardingPassRQ.setLeg(leg);
                    boardingPassesRQ.add(createBoardingPassRQ);
                }
            }
        }

        return boardingPassesRQ;
    }

    @SimpleAsync
    private void sendAndValidateAsync(String endpoint, String body, MultivaluedMap<String, Object> queryParams) {
        sendEmailDispatcher.publish(endpoint, body, queryParams);
    }

    private void sendAndValidate(String endpoint, String body, MultivaluedMap<String, Object> queryParams) {
        doPostMailing(endpoint, body, queryParams);
    }

    /**
     * Call to mailing service
     *
     * @param endpoint    Endpoint mailing service
     * @param body
     * @param queryParams headers
     * @return
     */
    public String doPostMailing(String endpoint, String body, MultivaluedMap<String, Object> queryParams) {
        logger.info("SENT BODY BP-RQ: {}", body);

        Client clientBuilder = null;
        try {
            clientBuilder = new ResteasyClientBuilder()
                    .httpEngine(HttpClientSingleton.getEngine())
                    .build();

            logger.info("URL: {}", systemPropertiesFactory.getInstance().getAmMailingApiUrl() + endpoint);

            WebTarget webTarget
                    = clientBuilder.target(
                    systemPropertiesFactory.getInstance().getAmMailingApiUrl() + endpoint);

            if (null != queryParams) {
                for (Map.Entry<String, List<Object>> entry : queryParams.entrySet()) {
                    String key = entry.getKey();
                    List<Object> value = entry.getValue();
                    webTarget = webTarget.queryParam(key, value);
                }
            }

            Response response = webTarget.request().post(Entity.entity(body, MediaType.APPLICATION_JSON));

            String jsonResponse = response.readEntity(String.class);

            logger.info("Response: {} {}", response.getStatus(), jsonResponse);

            return jsonResponse;
        } catch (RuntimeException e) {
            logger.error(e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        } finally {
            if (null != clientBuilder) {
                try {
                    clientBuilder.close();
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                }
            }

            logger.info("Email::response: final and close client");
        }
    }

    /**
     * Validate the mailing response
     *
     * @param mailingResponse
     * @throws Exception
     */
    public void validateMailingResponse(String mailingResponse) {//throws Exception {
        if (mailingResponse == null) {
            //throw new Exception(REST_CLIENT_ERROR);
        } else if (mailingResponse.contains(NOT_FOUND)) {
            //throw new Exception(NOT_FOUND_SERVICE_ERROR);
        } else if (mailingResponse.contains(INCIDENT)) {
            logger.error("INCIDENT IN AM DOCUMENTS API");
            //throw new Exception(mailingResponse.replace("\"", ""));
        }
    }

    /**
     * Validate the boarding pass list and remove the boarding pass without leg
     * information
     *
     * @param boardingPasses
     * @return
     */
    private static List<CreateBoardingPassRQ> validateCastBPRQ(List<CreateBoardingPassRQ> boardingPasses) {
        Iterator<CreateBoardingPassRQ> boardingPassIterator = boardingPasses.iterator();

        while (boardingPassIterator.hasNext()) {
            CreateBoardingPassRQ boardingPass = boardingPassIterator.next();
            if (!((null != boardingPass.getLeg()
                    && null != boardingPass.getLeg().getSegments()
                    && boardingPass.getLeg().getSegments().size() > 0)
                    || null != boardingPass.getPaymentTable())) {
                boardingPassIterator.remove();
            }
        }

        return boardingPasses;
    }

    /**
     * Return Itinerary Document by segment code
     *
     * @param segmentDocumentList
     * @param segmentCode
     * @return
     */
    public static SegmentDocument getSegmentDocumentBySegmentCode(
            SegmentDocumentList segmentDocumentList,
            String segmentCode
    ) {
        for (SegmentDocument segmentDocument : segmentDocumentList) {
            if (segmentDocument.getSegmentCode().equalsIgnoreCase(segmentCode)) {
                return segmentDocument;
            } else if (SegmentCodeUtil.compareSegmentCodeWithoutTime(segmentDocument.getSegmentCode(), segmentCode)) {
                return segmentDocument;
            }
        }

        return null;
    }

    public String getEnvironment() {
        return systemPropertiesFactory.getInstance().getEnvironment();
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

}
