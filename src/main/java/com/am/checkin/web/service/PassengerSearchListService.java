package com.am.checkin.web.service;

import com.aeromexico.commons.model.rq.PassengerSearchListRQ;
import com.aeromexico.sabre.api.session.exception.SabreEmptyResponseException;
import com.aeromexico.sabre.api.session.exception.SabreLayerUnavailableException;
import com.sabre.services.acs.bso.passengerlist.v3.ACSPassengerListRSACS;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.aeromexico.commons.exception.config.ReadProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named
@ApplicationScoped
public class PassengerSearchListService {

    private static final Logger LOG = LoggerFactory.getLogger(PassengerSearchListService.class);

    @Inject
    private PassengersListService passengersListService;

    @Inject
    private ReadProperties prop;

    public ACSPassengerListRSACS getPassengerSearchList(PassengerSearchListRQ passengersListRQ) {

        ACSPassengerListRSACS passengerList = null;

        try {
            passengerList = passengersListService.getPassengerList(
                    passengersListRQ.getAirline(),
                    passengersListRQ.getFlightNumber(),
                    passengersListRQ.getOrigin(),
                    passengersListRQ.getDepartureDate(),
                    passengersListRQ.getArrival(),
                    passengersListRQ.getEdits()
            );

        }catch (SabreEmptyResponseException ser) {
            LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110302).toString(), ser.fillInStackTrace());
        } catch (SabreLayerUnavailableException me) {
            LOG.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_12110301,"PassengerSearchListService.java:45", me.getMessage());
        } catch (Exception e) {
            LOG.error(e.getMessage());
        }

        return passengerList;
    }
    
    /*
    public String getPassengerDataList(PassengerDataListRQ passengerDataListRQ) throws Exception{
        GetReservationRS getReservationRS = amGetReservationService
                .getReservationFull(passengerDataListRQ.getPnr());
        UpdatePassengersRequest upr= new UpdatePassengersRequest();
        
        if(getReservationRS != null && getReservationRS.getReservation() != null
                && getReservationRS.getReservation().getPassengerReservation() != null
                && getReservationRS.getReservation().getPassengerReservation().getPassengers() != null
                && getReservationRS.getReservation().getPassengerReservation().getPassengers().getPassenger() != null
                && getReservationRS.getReservation().getPassengerReservation().getPassengers().getPassenger().size() > 0){
         
            for(PassengerPNRB passenger: getReservationRS.getReservation()
                    .getPassengerReservation().getPassengers().getPassenger()){
                UpdatePassenger up= new UpdatePassenger();
                
            }
        }
        
        return null;
    }*/
}
