package com.am.checkin.web.service;

import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.PNR;
import com.aeromexico.commons.model.PNRCollection;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.util.Constants;
import com.aeromexico.dao.client.exception.MongoDisconnectException;
import com.aeromexico.dao.util.CommonUtil;
import com.aeromexico.dao.services.IPNRLookupDao;
import com.aeromexico.sabre.api.session.common.ServiceCall;
import com.aeromexico.commons.exception.model.GenericException;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Pedro Reyes
 */
@Named
@ApplicationScoped
public class AncillariesWaiveService {

    private static final Logger logger = LoggerFactory.getLogger(AncillariesWaiveService.class);

    @Inject
    private IPNRLookupDao pnrLookupDao;

    @Inject
    private AEService aeService;

    /**
     *
     * @param pnr
     * @param cartId
     * @param serviceCallList
     * @throws Exception
     */
    public void wavedFirstBaggage(
            PNR pnr,
            String cartId,
            List<ServiceCall> serviceCallList,
            String pos
    ) throws Exception {
        //Add waived first bag  per leg
        for (CartPNR cartPNR : pnr.getCarts().getCollection()) {
            BookedLeg bookedLeg = pnr.getBookedLegByLegCode(cartPNR.getLegCode());
            aeService.addNewFirstBagToReservation(
                    cartPNR.getTravelerInfo().getCollection(),
                    bookedLeg,
                    pnr.getPnr(),
                    cartId,
                    serviceCallList,
                    pos
            );
        }
    }

    private PNR getPNR(String cartId) throws Exception {
        // First read the pnrCollection from the database
        PNRCollection pnrCollection;
                try{
                    pnrCollection= pnrLookupDao.readPNRCollectionByCartId(cartId);
                }catch (MongoDisconnectException me){
                    throw me;
                }


        // If the pnrCollection is null, throw an expired session exception
        if (pnrCollection == null) {
            logger.error(CommonUtil.getMessageByCode(Constants.CODE_004) + cartId);
            throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.EXPIRED_SESSION,
                    CommonUtil.getMessageByCode(Constants.CODE_004));
        }

        logger.info("pnrCollection( " + cartId + ") - " + pnrCollection);

        return pnrCollection.getPNRByCartId(cartId);
    }
}
