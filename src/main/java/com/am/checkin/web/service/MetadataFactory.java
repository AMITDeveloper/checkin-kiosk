package com.am.checkin.web.service;

import com.aeromexico.commons.model.IMetadataFactory;
import com.aeromexico.commons.model.Metadata;
import com.aeromexico.commons.model.SystemPropertiesFactory;
import com.am.checkin.web.config.Version;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adrian
 */
@Named
@ApplicationScoped
public class MetadataFactory implements IMetadataFactory {

    private static final Logger LOG = LoggerFactory.getLogger(MetadataFactory.class);

    @Inject
    private SystemPropertiesFactory systemPropertiesFactory;

    private volatile Metadata metadata;

    @PostConstruct
    public void init() {
        if (null == systemPropertiesFactory) {
            systemPropertiesFactory = new SystemPropertiesFactory();
        }
    }

    @Override
    public void reset() {
        metadata = null;
    }

    @Override
    public Metadata getInstance() throws RuntimeException {
        Metadata metadataLocal = metadata;
        if (null == metadataLocal) { // check 1
            synchronized (this) {
                metadataLocal = metadata;
                if (null == metadataLocal) { // check 2
                    try {

                        metadataLocal = createInstance();
                        metadata = metadataLocal;

                    } catch (RuntimeException ex) {
                        throw ex;
                    } catch (Exception ex) {
                        throw new RuntimeException(ex.getMessage());
                    }

                    LOG.info("Initializing Metadata: " + ((null != metadata) ? metadata.toString() : null));
                }

            }
        }
        return metadataLocal;
    }

    private Metadata createInstance() {
        String version = Version.getVersion() + "-" + Version.getRevision();
        String env;
        try {
            env = systemPropertiesFactory.getInstance().getEnvironment();

            if (null == env || env.isEmpty()) {
                env = "NA";
            }
        } catch (Exception ex) {
            env = "NA";
        }

        Metadata metadataLocal = new Metadata(version, env);

        metadataLocal.addEndpoint("pnr", "/pnr?store={storeCode}&pos={posCode}&language={language}", true);
        metadataLocal.addEndpoint("pnrEnhanced", "/pnr/enhanced?store={storeCode}&pos={posCode}", true);
        metadataLocal.addEndpoint("shoppingCart", "/carts/{cartId}?store={storeCode}&pos={posCode}&language={language}", true);
        metadataLocal.addEndpoint("ancillaries", "/ancillaries?store={storeCode}&coupon={couponCode}&cartId={cartId}&pos={posCode}", true);
        metadataLocal.addEndpoint("seatmaps", "/seatmaps?cartId={cartId}&store={storeCode}&coupon={couponCode}&pos={posCode}", true);
        metadataLocal.addEndpoint("preselect", "/seatmaps/preSelectSeat?cartId={cartId}&store={storeCode}&pos={posCode}");
        metadataLocal.addEndpoint("formsOfPayment", "/payment-methods?store={storeCode}&cartId={cartId}&pos={posCode}", true);
        metadataLocal.addEndpoint("purchaseOrder", "/po?store={storeCode}&pos={posCode}", true);
        metadataLocal.addEndpoint("boardingPass", "/boarding-passes/{id}?store={storeCode}&pos={posCode}", true);
        metadataLocal.addEndpoint("flightStatus", "/flight-status?store={storeCode}&pos={posCode}&flight={flightNumber}&date={departureDate}&origin={origin}&destination={destination}", true);
        metadataLocal.addEndpoint("emailBoardingPass", "/email-boarding-pass?store={storeCode}&pos={posCode}&language={language}&email={email}&boarding-pass-id={boardingPassId}&travelers={ticketNumbers}&leg={legCode}&cartId={cartId}", true);
        metadataLocal.addEndpoint("emailReceipt", "/email-receipt?store={storeCode}&pos={posCode}&language={language}&email={email}&pnr={pnr}&transaction={pnr}&cartId={cartId}", true);
        metadataLocal.addEndpoint("upgradeList", "/passengers/upgradeList?legCode={legCode}&store={storeCode}&pos={posCode}&language={language}", true);
        metadataLocal.addEndpoint("standByList", "/passengers/standByList?legCode={legCode}&store={storeCode}&pos={posCode}&language={language}", true);
        metadataLocal.addEndpoint("CheckinUpdatePassenger", "/update-passenger", true);
        metadataLocal.addEndpoint("CheckinReroutePayment", "/po/3ds-reroute-ckin/{result3ds}", true);

        return metadataLocal;
    }

    /**
     * @return the systemPropertiesFactory
     */
    public SystemPropertiesFactory getSystemPropertiesFactory() {
        if (null == systemPropertiesFactory) {
            systemPropertiesFactory = new SystemPropertiesFactory();
        }

        return systemPropertiesFactory;
    }

    /**
     * @param systemPropertiesFactory the systemPropertiesFactory to set
     */
    public void setSystemPropertiesFactory(SystemPropertiesFactory systemPropertiesFactory) {
        this.systemPropertiesFactory = systemPropertiesFactory;
    }
}
