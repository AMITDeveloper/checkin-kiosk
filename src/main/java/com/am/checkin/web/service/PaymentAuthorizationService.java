package com.am.checkin.web.service;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.loggingutils.LogUtil;
import com.aeromexico.commons.model.AbstractAncillary;
import com.aeromexico.commons.model.AuthorizationResult;
import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.PNR;
import com.aeromexico.commons.model.PNRCollection;
import com.aeromexico.commons.model.PaymentLinked;
import com.aeromexico.commons.model.rq.PurchaseOrderRQ;
import com.aeromexico.commons.model.utils.CurrencyUtil;
import com.aeromexico.commons.web.types.AuthorizationPaymentResultType;
import com.aeromexico.commons.web.types.CurrencyType;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.types.ItemsToBePaidType;
import com.aeromexico.commons.web.util.FilterPassengersUtil;
import com.aeromexico.sabre.api.session.common.ServiceCall;
import com.am.checkin.web.util.AuthorizationResultUtil;
import com.am.checkin.web.util.PnrCollectionUtil;
import com.am.checkin.web.util.PostCheckinProcess;
import com.am.checkin.web.util.PurchaseOrderUtil;
import com.google.gson.Gson;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.opentravel.ota.payments.PaymentRQ;
import org.opentravel.ota.payments.PaymentRS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author adrian
 */
@Named
@ApplicationScoped
public class PaymentAuthorizationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentAuthorizationService.class);

    private static final Gson GSON = new Gson();

    private static final String SERVICE = "Payment Authorization";

    @Inject
    private PaymentService paymentService;

    @Inject
    private AEService aeService;

    @Inject
    private PostCheckinProcess postCheckinProcess;

    @Inject
    private PnrCollectionService updatePnrCollectionService;

    @Inject
    private LogOnDatabaseService logOnDatabaseService;

    /**
     *
     * @param bookedTravelersEligibleForCheckin
     * @param bookedLeg
     * @param purchaseOrderRQ
     * @param abstractAncillaryList
     * @param toPaidItemsType
     * @param recordLocator
     * @param cartId
     * @param serviceCallList
     * @return
     * @throws ClassNotFoundException
     * @throws IOException
     * @throws Exception
     */
    public List<AuthorizationResult> callAuthorization(
            List<BookedTraveler> bookedTravelersEligibleForCheckin,
            BookedLeg bookedLeg,
            PurchaseOrderRQ purchaseOrderRQ,
            List<AbstractAncillary> abstractAncillaryList,
            ItemsToBePaidType toPaidItemsType,
            String recordLocator,
            String cartId,
            List<ServiceCall> serviceCallList
    ) throws ClassNotFoundException, IOException, Exception {

        PaymentRQ paymentRQ = paymentService.getPaymentAuthRQ(
                bookedTravelersEligibleForCheckin,
                bookedLeg,
                purchaseOrderRQ,
                abstractAncillaryList,
                recordLocator,
                toPaidItemsType
        );

        try {

            //This call will throw an exception if something goes wrong
            PaymentRS paymentRS = paymentService.getPaymentAuthRS(
                    paymentRQ, purchaseOrderRQ.getPos(), purchaseOrderRQ.getStore(),
                    recordLocator, cartId, "Doing Payment Authorization",
                    serviceCallList
            );

            List<AuthorizationResult> authorizationResultList;
            authorizationResultList = AuthorizationResultUtil.getAuthorizationResult(paymentRS.getAuthorizationResult());

            for (AuthorizationResult authorizationResult : authorizationResultList) {
                if (null == authorizationResult.getT3dsResult() && null == authorizationResult.getRedirectForm()) {
                    if (AuthorizationPaymentResultType.APPROVED.toString().equalsIgnoreCase(authorizationResult.getResponseCode())) {
                        //log on success
                        logOnDatabaseService.logPaymentAuthOnSuccess(purchaseOrderRQ, paymentRQ, paymentRS);
                    } else {
                        //log on error
                        logOnDatabaseService.logPaymentAuthOnError(purchaseOrderRQ, paymentRQ, paymentRS, null);
                    }
                }
            }

            return authorizationResultList;
        } catch (Exception ex) {
            //log on error
            logOnDatabaseService.logPaymentAuthOnError(purchaseOrderRQ, paymentRQ, null, ex);
            throw ex;
        }
    }

    /**
     *
     * @param purchaseOrderRQ
     * @param pnrCollection
     * @param pnrCollectionAfterCheckin
     * @param bookedTravelersRequested
     * @param serviceCallList
     * @return
     * @throws Exception
     */
    private List<AuthorizationResult> doPaymentAuthorizationExtraWeight(
            PurchaseOrderRQ purchaseOrderRQ, PNRCollection pnrCollection,
            PNRCollection pnrCollectionAfterCheckin,
            List<BookedTraveler> bookedTravelersRequested,
            List<ServiceCall> serviceCallList
    ) throws Exception {
        String cartId = purchaseOrderRQ.getPurchaseOrder().getCartId();
        PNR pnr = pnrCollection.getPNRByCartId(cartId);
        String recordLocator = pnr.getPnr();
        CartPNR cartPNR = pnr.getCartPNRByCartId(cartId);
        String legCode = cartPNR.getLegCode();
        BookedLeg bookedLeg = pnr.getBookedLegByLegCode(legCode);
        CartPNR cartPNRAfterCheckin = pnrCollectionAfterCheckin.getCartPNRByRecordLocatorAndLegCode(recordLocator, legCode);

        //this wil thrown an exception when there are missing information
        //PurchaseOrderUtil.checkMissingProperties(bookedTravelersRequested);
        boolean thereAreUnpaidItems = PurchaseOrderUtil.areThereExtraWeightUnpaidItems(bookedTravelersRequested);

        boolean thereIsPaymentMethod = (null != purchaseOrderRQ.getPurchaseOrder().getPaymentInfo()
                && null != purchaseOrderRQ.getPurchaseOrder().getPaymentInfo().getCollection()
                && !purchaseOrderRQ.getPurchaseOrder().getPaymentInfo().getCollection().isEmpty());

        if (thereAreUnpaidItems && !thereIsPaymentMethod) {
            LOGGER.error(LogUtil.getLogError(SERVICE, "There are unpaid upgrade items, you need to specify a payment method", recordLocator, cartId));
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PAYMENT_METHOD_IS_REQUIRED, "There are unpaid upgrade items, you need to specify a payment method");
        }

        String currencyCode = (null == cartPNR.getMeta().getTotal().getCurrency().getTotal()) ? CurrencyType.MXN.toString() : cartPNR.getMeta().getTotal().getCurrency().getCurrencyCode();

        BigDecimal totalInCart;
        totalInCart = (null == cartPNR.getMeta().getTotal().getCurrency().getTotal()) ? BigDecimal.ZERO : cartPNR.getMeta().getTotal().getCurrency().getTotal();

        if (thereAreUnpaidItems) {
            BigDecimal totalInPurchase;
            if (thereIsPaymentMethod) {
                totalInPurchase = CurrencyUtil.getTotal(
                        purchaseOrderRQ.getPurchaseOrder().getPaymentInfo(),
                        currencyCode
                );
            } else {
                totalInPurchase = CurrencyUtil.getAmount(BigDecimal.ZERO, currencyCode);
            }

            if (0 != totalInCart.compareTo(totalInPurchase)) {
                LOGGER.error(LogUtil.getLogError(SERVICE, "Prices on cart and purchase order does not match", recordLocator, cartId));
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PRICES_NOT_MATCH, ErrorType.PRICES_NOT_MATCH.getFullDescription());
            }
        }

        try {
            if (thereAreUnpaidItems && 0 < totalInCart.compareTo(BigDecimal.ZERO)) {

                //Form request for payment
                /**
                 * TravelerAncillaryList is a collection to store the
                 * ancillaries added to this payment request
                 *
                 */
                List<AbstractAncillary> abstractAncillaryList = new ArrayList<>();

                List<AuthorizationResult> authorizationResultList;
                authorizationResultList = callAuthorization(
                        bookedTravelersRequested,
                        bookedLeg,
                        purchaseOrderRQ,
                        abstractAncillaryList,
                        ItemsToBePaidType.EXTRAWEIGHT,
                        recordLocator,
                        cartId,
                        serviceCallList
                );

                for (AuthorizationResult authorizationResult : authorizationResultList) {
                    if (AuthorizationPaymentResultType.APPROVED.toString().equalsIgnoreCase(authorizationResult.getResponseCode())) {
                        PaymentLinked paymentLinked;
                        paymentLinked = PnrCollectionUtil.addPaymentId(
                                bookedTravelersRequested,
                                abstractAncillaryList,
                                authorizationResult,
                                purchaseOrderRQ.getTransactionTime()
                        );
                        cartPNR.getPaymentLinkedList().addIfNotExist(paymentLinked);
                        cartPNRAfterCheckin.getPaymentLinkedList().addIfNotExist(paymentLinked);
                        cartPNRAfterCheckin.setTouched(true);
                    }

                    aeService.addRemarkSync(authorizationResult, recordLocator, cartId, serviceCallList);
                }

                //updatePnrCollection(pnrCollection);
                try {
                    postCheckinProcess.unasynchronousMutePostCheckin(pnrCollectionAfterCheckin, recordLocator, cartId, false);
                } catch (Exception ex) {
                    //
                }

                return authorizationResultList;

            } else if (thereAreUnpaidItems) {
                LOGGER.error(LogUtil.getLogError(SERVICE, "There are unpaid extra weight items, you need to specify a payment method with an amount", recordLocator, cartId));
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PAYMENT_METHOD_IS_REQUIRED, "There are unpaid extra weight items, you need to specify a payment method with an amount");
            } else {//nothing to be paid
                LOGGER.error(LogUtil.getLogError(SERVICE, "There are no extra weight items to be paid", recordLocator, cartId));
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.THERE_ARE_NO_ITEMS_TO_BE_PAID, "There are no extra weight items to be paid");
            }
        } catch (GenericException ex) {
            updateOnError(pnrCollection, pnrCollectionAfterCheckin, recordLocator, cartId);
            throw ex;
        } catch (Exception ex) {
            updateOnError(pnrCollection, pnrCollectionAfterCheckin, recordLocator, cartId);
            LOGGER.error(LogUtil.getLogError(SERVICE, "Doing Payment Authorization Extra Weight", recordLocator, cartId, ex));
            throw ex;
        }
    }

    /**
     *
     * @param purchaseOrderRQ
     * @param pnrCollection
     * @param pnrCollectionAfterCheckin
     * @param bookedTravelersEligibleForCheckin
     * @param serviceCallList
     * @return
     * @throws Exception
     */
    private List<AuthorizationResult> doPaymentAuthorizationUpgrade(
            PurchaseOrderRQ purchaseOrderRQ, PNRCollection pnrCollection,
            PNRCollection pnrCollectionAfterCheckin,
            List<BookedTraveler> bookedTravelersEligibleForCheckin,
            List<ServiceCall> serviceCallList
    ) throws Exception {

        String cartId = purchaseOrderRQ.getPurchaseOrder().getCartId();
        PNR pnr = pnrCollection.getPNRByCartId(cartId);
        String recordLocator = pnr.getPnr();
        CartPNR cartPNR = pnr.getCartPNRByCartId(cartId);
        String legCode = cartPNR.getLegCode();
        BookedLeg bookedLeg = pnr.getBookedLegByLegCode(legCode);
        CartPNR cartPNRAfterCheckin = pnrCollectionAfterCheckin.getCartPNRByRecordLocatorAndLegCode(recordLocator, legCode);

        //this wil thrown an exception when there are missing information
        //PurchaseOrderUtil.checkMissingProperties(bookedTravelersRequested);
        boolean thereAreUnpaidItems = PurchaseOrderUtil.areThereUpgradeUnpaidItems(bookedTravelersEligibleForCheckin);

        boolean thereIsPaymentMethod = (null != purchaseOrderRQ.getPurchaseOrder().getPaymentInfo()
                && null != purchaseOrderRQ.getPurchaseOrder().getPaymentInfo().getCollection()
                && !purchaseOrderRQ.getPurchaseOrder().getPaymentInfo().getCollection().isEmpty());

        if (thereAreUnpaidItems && !thereIsPaymentMethod) {
            LOGGER.error(LogUtil.getLogError(SERVICE, "There are unpaid upgrade items, you need to specify a payment method", recordLocator, cartId));
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PAYMENT_METHOD_IS_REQUIRED, "There are unpaid upgrade items, you need to specify a payment method");
        }

        String currencyCode = (null == cartPNR.getMeta().getTotal().getCurrency().getTotal()) ? CurrencyType.MXN.toString() : cartPNR.getMeta().getTotal().getCurrency().getCurrencyCode();

        BigDecimal totalInCart;
        totalInCart = (null == cartPNR.getMeta().getTotal().getCurrency().getTotal()) ? BigDecimal.ZERO : cartPNR.getMeta().getTotal().getCurrency().getTotal();

        if (thereAreUnpaidItems) {
            BigDecimal totalInPurchase;
            if (thereIsPaymentMethod) {
                totalInPurchase = CurrencyUtil.getTotal(
                        purchaseOrderRQ.getPurchaseOrder().getPaymentInfo(),
                        currencyCode
                );
            } else {
                totalInPurchase = CurrencyUtil.getAmount(BigDecimal.ZERO, currencyCode);
            }

            if (0 != totalInCart.compareTo(totalInPurchase)) {
                LOGGER.error(LogUtil.getLogError(SERVICE, "Prices on cart and purchase order does not match", recordLocator, cartId));
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PRICES_NOT_MATCH, ErrorType.PRICES_NOT_MATCH.getFullDescription());
            }
        }

        try {

            if (thereAreUnpaidItems && 0 < totalInCart.compareTo(BigDecimal.ZERO)) {

                //Form request for payment
                /**
                 * TravelerAncillaryList is a collection to store the
                 * ancillaries added to this payment request
                 *
                 */
                List<AbstractAncillary> abstractAncillaryList = new ArrayList<>();

                List<AuthorizationResult> authorizationResultList;
                authorizationResultList = callAuthorization(
                        bookedTravelersEligibleForCheckin,
                        bookedLeg,
                        purchaseOrderRQ,
                        abstractAncillaryList,
                        ItemsToBePaidType.UPGRADE,
                        recordLocator,
                        cartId,
                        serviceCallList
                );

                for (AuthorizationResult authorizationResult : authorizationResultList) {
                    if (AuthorizationPaymentResultType.APPROVED.toString().equalsIgnoreCase(authorizationResult.getResponseCode())) {
                        PaymentLinked paymentLinked;
                        paymentLinked = PnrCollectionUtil.addPaymentId(
                                bookedTravelersEligibleForCheckin, abstractAncillaryList,
                                authorizationResult, purchaseOrderRQ.getTransactionTime()
                        );
                        cartPNR.getPaymentLinkedList().addIfNotExist(paymentLinked);
                        cartPNRAfterCheckin.getPaymentLinkedList().addIfNotExist(paymentLinked);
                        cartPNRAfterCheckin.setTouched(true);
                    }

                    aeService.addRemarkSync(authorizationResult, recordLocator, cartId, serviceCallList);
                }

                //updatePnrCollection(pnrCollection);
                try {
                    postCheckinProcess.unasynchronousMutePostCheckin(pnrCollectionAfterCheckin, recordLocator, cartId, false);
                } catch (Exception ex) {
                    //
                }

                return authorizationResultList;

            } else if (thereAreUnpaidItems) {
                LOGGER.error(LogUtil.getLogError(SERVICE, "There are unpaid upgrade items, you need to specify a payment method with an amount", recordLocator, cartId));
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PAYMENT_METHOD_IS_REQUIRED, "There are unpaid upgrade items, you need to specify a payment method with an amount");
            } else {//nothing to be paid
                LOGGER.error(LogUtil.getLogError(SERVICE, "There are no upgrade items to be paid", recordLocator, cartId));
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.THERE_ARE_NO_ITEMS_TO_BE_PAID, "There are no upgrade items to be paid");
            }

        } catch (GenericException ex) {
            updateOnError(pnrCollection, pnrCollectionAfterCheckin, recordLocator, cartId);
            throw ex;
        } catch (Exception ex) {
            updateOnError(pnrCollection, pnrCollectionAfterCheckin, recordLocator, cartId);
            LOGGER.error(LogUtil.getLogError(SERVICE, "Doing Payment Authorization Upgrade", recordLocator, cartId, ex));
            throw ex;
        }
    }

    /**
     *
     * @param purchaseOrderRQ
     * @param pnrCollection
     * @param pnrCollectionAfterCheckin
     * @param bookedTravelersEligibleForCheckin
     * @param serviceCallList
     * @return
     * @throws Exception
     */
    private List<AuthorizationResult> doPaymentAuthorizationPreCheckin(
            PurchaseOrderRQ purchaseOrderRQ, PNRCollection pnrCollection,
            PNRCollection pnrCollectionAfterCheckin,
            List<BookedTraveler> bookedTravelersEligibleForCheckin,
            List<ServiceCall> serviceCallList
    ) throws Exception {

        String cartId = purchaseOrderRQ.getPurchaseOrder().getCartId();
        PNR pnr = pnrCollection.getPNRByCartId(cartId);
        String recordLocator = pnr.getPnr();
        CartPNR cartPNR = pnr.getCartPNRByCartId(cartId);
        String legCode = cartPNR.getLegCode();
        BookedLeg bookedLeg = pnr.getBookedLegByLegCode(legCode);
        CartPNR cartPNRAfterCheckin = pnrCollectionAfterCheckin.getCartPNRByRecordLocatorAndLegCode(recordLocator, legCode);

        //this wil thrown an exception when there are missing information
        //PurchaseOrderUtil.checkMissingProperties(bookedTravelersRequested);
        boolean thereAreUnpaidItems = PurchaseOrderUtil.areThereUnpaidItems(bookedTravelersEligibleForCheckin);
        boolean thereIsPaymentMethod = (null != purchaseOrderRQ.getPurchaseOrder().getPaymentInfo()
                && null != purchaseOrderRQ.getPurchaseOrder().getPaymentInfo().getCollection()
                && !purchaseOrderRQ.getPurchaseOrder().getPaymentInfo().getCollection().isEmpty());

        if (thereAreUnpaidItems && !thereIsPaymentMethod) {
            LOGGER.error(LogUtil.getLogError(SERVICE, "There are unpaid items, you need to specify a payment method", recordLocator, cartId));
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PAYMENT_METHOD_IS_REQUIRED, "There are unpaid items, you need to specify a payment method");
        }

        String currencyCode = (null == cartPNR.getMeta().getTotal().getCurrency().getTotal()) ? CurrencyType.MXN.toString() : cartPNR.getMeta().getTotal().getCurrency().getCurrencyCode();

        BigDecimal totalInCart;
        totalInCart = (null == cartPNR.getMeta().getTotal().getCurrency().getTotal()) ? BigDecimal.ZERO : cartPNR.getMeta().getTotal().getCurrency().getTotal();

        if (thereAreUnpaidItems) {
            BigDecimal totalInPurchase;
            if (thereIsPaymentMethod) {
                totalInPurchase = CurrencyUtil.getTotal(
                        purchaseOrderRQ.getPurchaseOrder().getPaymentInfo(),
                        currencyCode
                );
            } else {
                totalInPurchase = CurrencyUtil.getAmount(BigDecimal.ZERO, currencyCode);
            }

            if (0 != totalInCart.compareTo(totalInPurchase)) {
                LOGGER.error(LogUtil.getLogError(SERVICE, "Prices on cart and purchase order does not match", recordLocator, cartId));
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PRICES_NOT_MATCH, ErrorType.PRICES_NOT_MATCH.getFullDescription());
            }
        }

        try {

            //remove unpaid ancillaries when quantity is zero
            aeService.removeZeroQuantityAncillariesFromReservation(
                    bookedTravelersEligibleForCheckin,
                    recordLocator, cartId,
                    serviceCallList
            );

            if (thereAreUnpaidItems && 0 < totalInCart.compareTo(BigDecimal.ZERO)) {

                //Form request for payment
                /**
                 * TravelerAncillaryList is a collection to store the
                 * ancillaries added to this payment request
                 *
                 */
                List<AbstractAncillary> abstractAncillaryList = new ArrayList<>();

                List<AuthorizationResult> authorizationResultList;
                authorizationResultList = callAuthorization(
                        bookedTravelersEligibleForCheckin,
                        bookedLeg,
                        purchaseOrderRQ,
                        abstractAncillaryList,
                        ItemsToBePaidType.SEAT_AND_BAGGAGE,
                        recordLocator,
                        cartId,
                        serviceCallList
                );

                for (AuthorizationResult authorizationResult : authorizationResultList) {
                    if (AuthorizationPaymentResultType.APPROVED.toString().equalsIgnoreCase(authorizationResult.getResponseCode())) {
                        PaymentLinked paymentLinked = PnrCollectionUtil.addPaymentId(bookedTravelersEligibleForCheckin, abstractAncillaryList, authorizationResult, purchaseOrderRQ.getTransactionTime());
                        cartPNR.getPaymentLinkedList().addIfNotExist(paymentLinked);
                        cartPNRAfterCheckin.getPaymentLinkedList().addIfNotExist(paymentLinked);
                        cartPNRAfterCheckin.setTouched(true);
                    }

                    aeService.addRemarkSync(authorizationResult, recordLocator, cartId, serviceCallList);
                }

                //updatePnrCollection(pnrCollection);
                try {
                    postCheckinProcess.unasynchronousMutePostCheckin(pnrCollectionAfterCheckin, recordLocator, cartId, false);
                } catch (Exception ex) {
                    //
                }

                return authorizationResultList;

            } else if (thereAreUnpaidItems) {
                LOGGER.error(LogUtil.getLogError(SERVICE, "There are unpaid items, you need to specify a payment method with an amount", recordLocator, cartId));
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PAYMENT_METHOD_IS_REQUIRED, "There are unpaid items, you need to specify a payment method with an amount");
            } else {//nothing to be paid
                LOGGER.error(LogUtil.getLogError(SERVICE, "There are no items to be paid", recordLocator, cartId));
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.THERE_ARE_NO_ITEMS_TO_BE_PAID, "There are no items to be paid");
            }

        } catch (GenericException ex) {
            updateOnError(pnrCollection, pnrCollectionAfterCheckin, recordLocator, cartId);
            throw ex;
        } catch (Exception ex) {
            updateOnError(pnrCollection, pnrCollectionAfterCheckin, recordLocator, cartId);
            LOGGER.error(LogUtil.getLogError(SERVICE, "Doing Payment Authorization Pre Checkin", recordLocator, cartId, ex));
            throw ex;
        }
    }

    /**
     *
     * @param purchaseOrderRQ
     * @param pnrCollection
     * @param pnrCollectionAfterCheckin
     * @param bookedTravelersEligibleForCheckin
     * @param serviceCallList
     * @return
     * @throws Exception
     */
    private List<AuthorizationResult> doPaymentAuthorizationPostCheckin(
            PurchaseOrderRQ purchaseOrderRQ, PNRCollection pnrCollection,
            PNRCollection pnrCollectionAfterCheckin,
            List<BookedTraveler> bookedTravelersEligibleForCheckin,
            List<ServiceCall> serviceCallList
    ) throws Exception {

        String cartId = purchaseOrderRQ.getPurchaseOrder().getCartId();
        PNR pnr = pnrCollection.getPNRByCartId(cartId);
        String recordLocator = pnr.getPnr();
        CartPNR cartPNR = pnr.getCartPNRByCartId(cartId);
        String legCode = cartPNR.getLegCode();
        BookedLeg bookedLeg = pnr.getBookedLegByLegCode(legCode);
        CartPNR cartPNRAfterCheckin = pnrCollectionAfterCheckin.getCartPNRByRecordLocatorAndLegCode(recordLocator, legCode);

        boolean thereAreUnpaidItems = PurchaseOrderUtil.areThereUnpaidItems(bookedTravelersEligibleForCheckin);
        boolean thereIsPaymentMethod = (null != purchaseOrderRQ.getPurchaseOrder().getPaymentInfo()
                && null != purchaseOrderRQ.getPurchaseOrder().getPaymentInfo().getCollection()
                && !purchaseOrderRQ.getPurchaseOrder().getPaymentInfo().getCollection().isEmpty());

        if (thereAreUnpaidItems && !thereIsPaymentMethod) {
            LOGGER.error(LogUtil.getLogError(SERVICE, "There are unpaid items, you need to specify a payment method", recordLocator, cartId));
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PAYMENT_METHOD_IS_REQUIRED, "There are unpaid items, you need to specify a payment method");
        }

        String currencyCode = (null == cartPNR.getMeta().getTotal().getCurrency().getTotal()) ? CurrencyType.MXN.toString() : cartPNR.getMeta().getTotal().getCurrency().getCurrencyCode();
        BigDecimal totalInCart;
        totalInCart = (null == cartPNR.getMeta().getTotal().getCurrency().getTotal()) ? BigDecimal.ZERO : cartPNR.getMeta().getTotal().getCurrency().getTotal();

        if (thereAreUnpaidItems) {
            BigDecimal totalInPurchase;
            if (thereIsPaymentMethod) {
                totalInPurchase = CurrencyUtil.getTotal(
                        purchaseOrderRQ.getPurchaseOrder().getPaymentInfo(),
                        currencyCode
                );
            } else {
                totalInPurchase = CurrencyUtil.getAmount(BigDecimal.ZERO, currencyCode);
            }

            if (0 != totalInCart.compareTo(totalInPurchase)) {
                LOGGER.error(LogUtil.getLogError(SERVICE, "Prices on cart and purchase order does not match", recordLocator, cartId));
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PRICES_NOT_MATCH, ErrorType.PRICES_NOT_MATCH.getFullDescription());
            }
        }

        try {

            if (thereAreUnpaidItems && 0 < totalInCart.compareTo(BigDecimal.ZERO)) {

                //Form request for payment
                /**
                 * TravelerAncillaryList is a collection to store the
                 * ancillaries added to this payment request
                 *
                 */
                List<AbstractAncillary> abstractAncillaryList = new ArrayList<>();

                List<AuthorizationResult> authorizationResultList;
                authorizationResultList = callAuthorization(
                        bookedTravelersEligibleForCheckin,
                        bookedLeg,
                        purchaseOrderRQ,
                        abstractAncillaryList,
                        ItemsToBePaidType.SEAT_AND_BAGGAGE,
                        recordLocator,
                        cartId,
                        serviceCallList
                );

                for (AuthorizationResult authorizationResult : authorizationResultList) {
                    if (AuthorizationPaymentResultType.APPROVED.toString().equalsIgnoreCase(authorizationResult.getResponseCode())) {
                        PaymentLinked paymentLinked;
                        paymentLinked = PnrCollectionUtil.addPaymentId(
                                bookedTravelersEligibleForCheckin,
                                abstractAncillaryList,
                                authorizationResult,
                                purchaseOrderRQ.getTransactionTime()
                        );
                        cartPNR.getPaymentLinkedList().addIfNotExist(paymentLinked);
                        cartPNRAfterCheckin.getPaymentLinkedList().addIfNotExist(paymentLinked);
                        cartPNRAfterCheckin.setTouched(true);
                    }

                    aeService.addRemarkSync(authorizationResult, recordLocator, cartId, serviceCallList);
                }

                LOGGER.info(LogUtil.getLogDebug(SERVICE, recordLocator, cartId, "Pnr Collection After Payment Authorization\n" + GSON.toJson(pnrCollectionAfterCheckin) + "\n"));

                //updatePnrCollection(pnrCollection);
                try {
                    postCheckinProcess.unasynchronousMutePostCheckin(pnrCollectionAfterCheckin, recordLocator, cartId, false);
                } catch (Exception ex) {
                    LOGGER.error(ex.getMessage(), ex);
                }

                return authorizationResultList;

            } else if (thereAreUnpaidItems) {
                LOGGER.error(LogUtil.getLogError(SERVICE, "There are unpaid items, you need to specify a payment method with an amount", recordLocator, cartId));
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PAYMENT_METHOD_IS_REQUIRED, "There are unpaid items, you need to specify a payment method with an amount");
            } else {//nothing to be paid
                LOGGER.error(LogUtil.getLogError(SERVICE, "There are no items to be paid", recordLocator, cartId));
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.THERE_ARE_NO_ITEMS_TO_BE_PAID, "There are no items to be paid");
            }

        } catch (GenericException ex) {
            updateOnError(pnrCollection, pnrCollectionAfterCheckin, recordLocator, cartId); //
            //
            throw ex;
        } catch (Exception ex) {
            updateOnError(pnrCollection, pnrCollectionAfterCheckin, recordLocator, cartId);
            LOGGER.error(LogUtil.getLogError(SERVICE, "Doing Payment Authorization Post Checkin", recordLocator, cartId, ex));
            throw ex;
        }
    }

    /**
     *
     * @param purchaseOrderRQ
     * @param serviceCallList
     * @return
     * @throws Exception
     */
    public List<AuthorizationResult> doPaymentAuthorization(
            PurchaseOrderRQ purchaseOrderRQ,
            List<ServiceCall> serviceCallList
    ) throws Exception {
        String cartId = purchaseOrderRQ.getPurchaseOrder().getCartId();

        PNRCollection pnrCollection = updatePnrCollectionService.getPNRCollection(cartId, purchaseOrderRQ.getPos());

        PNR pnr = pnrCollection.getPNRByCartId(cartId);
        if (null == pnr) {
            LOGGER.error(LogUtil.getLogError(SERVICE, "Pnr not found", null, cartId));
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION, "Pnr not found");
        }

        if (null != pnrCollection.getStore() && !pnrCollection.getStore().trim().isEmpty()) {
            purchaseOrderRQ.setStore(pnrCollection.getStore());
        }

        if (null != pnrCollection.getPos() && !pnrCollection.getPos().trim().isEmpty()) {
            purchaseOrderRQ.setPos(pnrCollection.getPos());
        }

        String recordLocator = pnr.getPnr();

        if (null == recordLocator || recordLocator.trim().isEmpty()) {
            LOGGER.error(LogUtil.getLogError(SERVICE, "Pnr not found", null, cartId));
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION, "Pnr not found");
        }

        purchaseOrderRQ.setRecordLocator(recordLocator);

        //PNRCollection pnrCollectionAfterCheckin = getPNRCollectionByRecordLocatorAfterCheckin(recordLocator, cartId);
        boolean create = true;
        boolean mustBeAsynchronous = true;
        PNRCollection pnrCollectionAfterCheckin;
        pnrCollectionAfterCheckin = updatePnrCollectionService.createOrUpdatePnrCollectionAfterCheckin(
                pnrCollection, recordLocator, cartId, create, mustBeAsynchronous
        );

        CartPNR cartPNR = pnr.getCartPNRByCartId(cartId);
        if (null == cartPNR) {
            LOGGER.error(LogUtil.getLogError(SERVICE, "Cart not found", recordLocator, cartId));
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION, "Cart not found");
        }
        String legCode = cartPNR.getLegCode();

        BookedLeg bookedLeg = pnr.getBookedLegByLegCode(legCode);
        if (null == bookedLeg) {
            LOGGER.error(LogUtil.getLogError(SERVICE, "BookedLeg not found", recordLocator, cartId));
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION, "BookedLeg not found");
        }

        CartPNR cartPNRAfterCheckin = pnrCollectionAfterCheckin.getCartPNRByRecordLocatorAndLegCode(recordLocator, legCode);

        if (null == cartPNRAfterCheckin) {
            LOGGER.error(LogUtil.getLogError(SERVICE, "Cart not found", recordLocator, cartId));
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION, "Purchase: Cart not found in after checkin collection");
        }

        //This object will be a reference to the original collection
        //any change on this will change the original collection and vice versa
        List<BookedTraveler> bookedTravelersRequested = FilterPassengersUtil.getRequestedForCheckin(cartPNR.getTravelerInfo().getCollection());

        if (null == bookedTravelersRequested || bookedTravelersRequested.isEmpty()) {
            LOGGER.error(LogUtil.getLogError(SERVICE, "Not passengers eligible for checkin", recordLocator, cartId));
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.NO_PASSENGERS_SELECTED_FOR_CHECKIN, "Not passengers eligible for checkin");
        }

        if (null == purchaseOrderRQ.getPurchaseOrder().getOperation()) {
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.OPERATION_NOT_SUPPORTED, "Invalid operation field");
        } else {
            switch (purchaseOrderRQ.getPurchaseOrder().getOperation()) {
                case UPGRADE:
                    return doPaymentAuthorizationUpgrade(
                            purchaseOrderRQ,
                            pnrCollection,
                            pnrCollectionAfterCheckin,
                            bookedTravelersRequested,
                            serviceCallList
                    );
                case EXTRAWEIGHT:
                    return doPaymentAuthorizationExtraWeight(
                            purchaseOrderRQ,
                            pnrCollection,
                            pnrCollectionAfterCheckin,
                            bookedTravelersRequested,
                            serviceCallList
                    );
                case CHECKIN:
                    if (bookedTravelersRequested.get(0).isCheckinStatus()) {
                        return doPaymentAuthorizationPostCheckin(
                                purchaseOrderRQ,
                                pnrCollection,
                                pnrCollectionAfterCheckin,
                                FilterPassengersUtil.getAlreadyCheckedInPassengers(bookedTravelersRequested),
                                serviceCallList
                        );
                    } else {
                        return doPaymentAuthorizationPreCheckin(
                                purchaseOrderRQ,
                                pnrCollection,
                                pnrCollectionAfterCheckin,
                                FilterPassengersUtil.getNotCheckedInPassengers(bookedTravelersRequested),
                                serviceCallList
                        );
                    }
                default:
                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.OPERATION_NOT_SUPPORTED, "Invalid operation field");
            }
        }
    }

    /**
     * @param pnrCollection
     * @param pnrCollectionAfterCheckin
     * @param recordLocator
     * @param cartId
     */
    private void updateOnError(
            PNRCollection pnrCollection, PNRCollection pnrCollectionAfterCheckin,
            String recordLocator, String cartId
    ) {
        try {
            updatePnrCollectionService.fireUpdatePnrCollection(pnrCollection);
        } catch (Exception ex1) {
            //
        }
        try {
            postCheckinProcess.unasynchronousMutePostCheckin(pnrCollectionAfterCheckin, recordLocator, cartId, false);
        } catch (Exception ex1) {
            //
        }
    }
}
