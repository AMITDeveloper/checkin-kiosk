package com.am.checkin.web.service;

import com.aeromexico.commons.flightStatus.model.FlightStatusSearch;
import com.aeromexico.commons.model.SystemPropertiesFactory;
import com.aeromexico.commons.model.TokenSabreMongo;
import com.aeromexico.commons.model.utils.StoreFrontUtils;
import com.aeromexico.commons.web.util.Constants;
import com.aeromexico.commons.web.util.ErrorCodeDescriptions;
import com.aeromexico.dao.services.TokenSabreDao;
import com.aeromexico.sabre.api.auth.TokenSabreClient;
import com.aeromexico.sabre.api.models.exception.AbstractSabreError;
import com.aeromexico.sabre.api.util.CheckErrorUtil;
import com.aeromexico.sabre.api.util.SabreErrorUtil;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.*;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Named
@ApplicationScoped
public class SabreRESTClientCheckIn {
    private static final Logger log = LoggerFactory.getLogger(SabreRESTClientCheckIn.class);

    private boolean use2SG;
    private String mx_jipcc;
    private String us_jipcc;
    private String targetURL;
    private String appID;
    private String jipccParam;
    private String token;

    @Inject
    private SystemPropertiesFactory systemPropertiesFactory;

    @Inject
    private TokenSabreClient tokenSabreClient;

    @Inject
    private TokenSabreDao tokenSabreDao;


    @PostConstruct
    public void init() {
        if (null == systemPropertiesFactory) {
            systemPropertiesFactory = new SystemPropertiesFactory();
        }

        jipccParam = Constants.SABRE_JIPCC_PARAM;
        use2SG = systemPropertiesFactory.getInstance().isUse2sg();
        targetURL = systemPropertiesFactory.getInstance().getSabreUrlApi();
        appID = systemPropertiesFactory.getInstance().getSabreAppId();
        mx_jipcc = systemPropertiesFactory.getInstance().getSabreMxJipccValue();
        us_jipcc = systemPropertiesFactory.getInstance().getSabreUsJipccValue();
    }

    public SabreRESTClientCheckIn() {
    }

    
    public Map<String, String> doGetRequest(String optionService, String service, FlightStatusSearch flightStatusSearch, String storeParameter) {       
        Map<String, String> responseMap = null;
        Response httpResponse = null;
        int status = -1;
        String SABREResponse = "";
        try {
            log.info("doGetReqest service: {}", service);
            log.info("doGetReqest: {}", flightStatusSearch.toString());
            Invocation.Builder targetClient;
            
            if ("status".equalsIgnoreCase(optionService)) {
                targetClient = this.getTargetClientFlightStatus(optionService, service, flightStatusSearch, storeParameter);
            } else {
                targetClient = this.getTargetClientFlightStatus(optionService, service, flightStatusSearch, storeParameter);
            }
                        
            httpResponse = targetClient.get();
            responseMap = new HashMap<>(); 
            if (null != httpResponse) {
                status = httpResponse.getStatus();
                SABREResponse = httpResponse.readEntity(String.class);
                responseMap.put("status", String.valueOf(status));
                responseMap.put("response", SABREResponse);
                log.info("doGetReqest Status: {}", status);
                log.info("doGetReqest Response: {}", SABREResponse);
            } else {
                log.info("doGetReqest: null");
            }
            checkResponseForErrors(SABREResponse, status, service);
        } catch(Exception err) {
            responseMap = new HashMap<>();
            responseMap.put("status", "500");
            responseMap.put("response", "");
            log.info("doGetReqest error: Message - {}, Cause - {}", err.getMessage(), err.getCause());
            checkResponseForErrors(SABREResponse, status, service);
            
        } finally {
            try {
                httpResponse.close();
            } catch (Exception ignored) {

            }
        }
        return responseMap;
    }

    
    private Invocation.Builder getTargetClientFlightStatus(
            String optionService,
            String service,
            FlightStatusSearch flightStatusSearch,
            String storeParameter
    ) throws Exception {
        if (use2SG) {
            TokenSabreMongo tokenSabre = tokenSabreDao.getToken("1");
            if (null == tokenSabre || null == tokenSabre.getToken()) {
                this.token = "Bearer " + tokenSabreClient.getToken(appID);
                tokenSabreDao.mergeToken(
                    new TokenSabreMongo(
                            "1",
                            6,
                            this.token)
                );
            } else {
                this.token = tokenSabre.getToken();
            }
        }
        ResteasyClient client = new ResteasyClientBuilder()
                .establishConnectionTimeout(5, TimeUnit.SECONDS)
                .socketTimeout(5, TimeUnit.SECONDS).build();

        ResteasyWebTarget target = client.target(targetURL + service);
        MultivaluedMap<String, Object> queryParams = null;
        if ("status".equals(optionService)) {
            queryParams = this.getQueryParamsFlighStatus(flightStatusSearch, storeParameter);
        } else {
            queryParams = this.getQueryParamsSchedules(flightStatusSearch, storeParameter);
        }

        Invocation.Builder targetClient = target.queryParams(queryParams).request()
                .header("Authorization", token)
                .header("Content-Type", MediaType.APPLICATION_JSON)
                .header("Accept", MediaType.APPLICATION_JSON);
        
        return targetClient;
    }

    private MultivaluedMap<String, Object> getQueryParamsFlighStatus(FlightStatusSearch flightStatusSearch, String storeParameter) {
        MultivaluedMap<String, Object> queryParams = new MultivaluedMapImpl<>();
        queryParams.add("flightNumber", flightStatusSearch.getFlightNumber());
        queryParams.add("departureDate", flightStatusSearch.getDepartureDate());
        queryParams.add("airlineCode", flightStatusSearch.getAirlineCode());      
        queryParams.add(jipccParam, this.getJipccParameter(storeParameter));
        return queryParams;
    }
    
    private MultivaluedMap<String, Object> getQueryParamsSchedules(FlightStatusSearch flightStatusSearch, String storeParameter) {
        MultivaluedMap<String, Object> queryParams = new MultivaluedMapImpl<>();      
        queryParams.add("origin", flightStatusSearch.getOrigin());
        queryParams.add("destination", flightStatusSearch.getDestination());
        queryParams.add("departureDate", flightStatusSearch.getDepartureDate());        
        queryParams.add("requestType", "DAY");
        queryParams.add(jipccParam, this.getJipccParameter(storeParameter));
        return queryParams;
    }
    
    private String getJipccParameter(String storeParameter) {
        String store =  storeParameter;
        if (store != null && StoreFrontUtils.isUsStoreFrontBooking(store.toUpperCase())) {
            return us_jipcc;
        } else {            
            return mx_jipcc; //Default to MX store
        }
    }

    private void checkResponseForErrors(String SABREResponse, int status, String service) {
        if (status < 200 || status >= 300) {
            AbstractSabreError abstractSabreError;
            try {
                abstractSabreError = SabreErrorUtil.getSabreErrorType(SABREResponse);

            } catch (Exception ex) {
                log.error(ErrorCodeDescriptions.MSG_CODE_INVALID_JSON_TRANSFORMATION + " | " + SABREResponse + "|" + ex.getMessage());
                abstractSabreError = null;
            }
            //this will throw an exception
            CheckErrorUtil.checkSabreErrors(abstractSabreError, status, service);
        }
    }


    public SystemPropertiesFactory getSystemPropertiesFactory() {
        return systemPropertiesFactory;
    }

    public void setSystemPropertiesFactory(SystemPropertiesFactory systemPropertiesFactory) {
        this.systemPropertiesFactory = systemPropertiesFactory;
    }
}
