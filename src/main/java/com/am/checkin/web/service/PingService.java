package com.am.checkin.web.service;

import com.aeromexico.commons.model.Ping;
import com.aeromexico.commons.model.SystemPropertiesFactory;
import com.aeromexico.commons.web.types.PingStatusType;
import com.aeromexico.sabre.api.sessionping.SessionPingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@RequestScoped
public class PingService {

    private static final Logger LOG = LoggerFactory.getLogger(PingService.class);
    private static Ping ping = null;

    @Inject
    private SessionPingService sessionPingService;

    @Inject
    private SystemPropertiesFactory systemPropertiesFactory;

    @PostConstruct
    public void init() {

    	LOG.info( "+PingService.init()" );

    	if (null == systemPropertiesFactory) {
            systemPropertiesFactory = new SystemPropertiesFactory();
        }

        if ( ping == null ) {
        	ping = new Ping();
        }

        ping.setHostName(systemPropertiesFactory.getInstance().getHostname());
        ping.setStatus( PingStatusType.AVAILABLE );
        LOG.info( "1PingService.init()" );
    }

    public Ping getPingStatus() {

    	/*
        Ping ping = null;
        try {
            ping = getSabreWebServiceStatus();
            ping.setHostName(systemPropertiesFactory.getInstance().getHostname());
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
		*/

    	return ping;
    }

    /**
     * This method should always return a Ping object In case of an exception,
     * the response should be that the service/server is unavailable.
     *
     * @return
     */
    private Ping getSabreWebServiceStatus() {

        boolean result = false;
        Ping pingObject = new Ping();
        pingObject.setStatus(PingStatusType.UNAVAILABLE);

        try {
            result = sessionPingService.Ping();
            if (result) {
                pingObject.setStatus(PingStatusType.AVAILABLE);
            }
        } catch (Exception ex) {
        }

        return pingObject;
    }

    /**
     * @return the systemPropertiesFactory
     */
    public SystemPropertiesFactory getSystemPropertiesFactory() {
        if (null == systemPropertiesFactory) {
            systemPropertiesFactory = new SystemPropertiesFactory();
        }
        return systemPropertiesFactory;
    }

    /**
     * @param systemPropertiesFactory the systemPropertiesFactory to set
     */
    public void setSystemPropertiesFactory(SystemPropertiesFactory systemPropertiesFactory) {
        this.systemPropertiesFactory = systemPropertiesFactory;
    }

}
