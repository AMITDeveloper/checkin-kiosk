package com.am.checkin.web.service;

/**
 * This Class let's Update Document Required.
 */
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;

import com.aeromexico.commons.model.*;
import com.aeromexico.sabre.api.sabrecommand.AMSabreCommandService;
import com.google.gson.Gson;
import com.sabre.webservices.sabrexml._2003._07.SabreCommandLLSRS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.types.PaxType;
import com.aeromexico.commons.web.util.ErrorCodeDescriptions;
import com.aeromexico.sabre.api.acs.AMPassengerSecurityService;
import com.am.checkin.web.util.ShoppingCartErrorUtil;
import com.am.checkin.web.util.ShoppingCartUtil;
import com.sabre.services.checkin.updatesecurityinfo.v4.ItineraryType;
import com.sabre.services.checkin.updatesecurityinfo.v4.PassengerInfoType;
import com.sabre.services.checkin.updatesecurityinfo.v4.PassengerSecurityInfoType;
import com.sabre.services.checkin.updatesecurityinfo.v4.UpdateSecurityInfoRQType;
import com.sabre.services.checkin.updatesecurityinfo.v4.UpdateSecurityInfoRSType;
import com.sabre.services.checkin.updatesecurityinfo.v4.SecurityChecksType;
import com.sabre.services.stl.v3.ErrorOrSuccessCode;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;

import java.time.Month;
import java.time.format.TextStyle;
import java.util.*;
import javax.annotation.PostConstruct;

@Named
@ApplicationScoped
public class UpdateGovService {

    @Inject
    AMSabreCommandService sabreCommandService;

    private static final Logger LOG = LoggerFactory.getLogger(UpdateGovService.class);

    private static boolean ADD_GGOV;
    private static final boolean DEFAULT_ADD_GGOV = false;

    private static Map<String, String> AIRPORTS = new HashMap<>();

    //YVR. VANCOUVER
    //YUL. MONTREAL
    //YYC. CALGARY
    //YYZ. TORONTO
    //LHR. LONDON
    //ICN. SEUL
    private static final String DEFAULT_AIRPORTS = "YVRCA,YULCA,YYCCA,YYZCA,LHRGB,ICNKR";

    @Inject
    private AMPassengerSecurityService passengerSecurityService;

    @PostConstruct
    public void init() {
        try {
            String add_ggov = System.getProperty("add_ggov");

            if (null == add_ggov || add_ggov.isEmpty()) {
                ADD_GGOV = DEFAULT_ADD_GGOV;
            } else {
                ADD_GGOV = Boolean.valueOf(add_ggov);
            }

        } catch (Exception ex) {
            ADD_GGOV = DEFAULT_ADD_GGOV;
        }

        try {
            String airportList = System.getProperty("ggov_airport_list");
            if (null != airportList && !airportList.trim().isEmpty()) {

                List<String> codes = Arrays.asList(airportList.split("\\s*,\\s*"));

                for (String iataStr : codes) {
                    AIRPORTS.put(iataStr.substring(0, 3), iataStr.substring(3));
                }

            } else {
                List<String> codes = Arrays.asList(DEFAULT_AIRPORTS.split("\\s*,\\s*"));
                for (String iataStr : codes) {
                    AIRPORTS.put(iataStr.substring(0, 3), iataStr.substring(3));
                }
            }
        } catch (Exception ex) {
            AIRPORTS = new HashMap<>();

            List<String> codes = Arrays.asList(DEFAULT_AIRPORTS.split("\\s*,\\s*"));
            for (String iataStr : codes) {
                AIRPORTS.put(iataStr.substring(0, 3), iataStr.substring(3));
            }
        }
    }

    private String getCountryCode(String departure, String arrival) {
        if (null == AIRPORTS || AIRPORTS.isEmpty()) {
            return null;
        }

        String country = AIRPORTS.get(departure);

        if (null == country) {
            country = AIRPORTS.get(arrival);
        }

        return country;
    }

    private BookedSegment getElegibleBookedSegment(List<BookedSegment> segments) {
        if (null == AIRPORTS || AIRPORTS.isEmpty() || null == segments || segments.isEmpty()) {
            return null;
        }

        for (BookedSegment bookedSegment : segments) {
            if (AIRPORTS.containsKey(bookedSegment.getSegment().getDepartureAirport())
                    || AIRPORTS.containsKey(bookedSegment.getSegment().getArrivalAirport())) {
                return bookedSegment;
            }
        }

        return null;
    }

    public void updateGov(List<BookedTraveler> bookedTravelers, BookedLeg bookedLeg) throws Exception {

        if (!ADD_GGOV || null == AIRPORTS || AIRPORTS.isEmpty()) {
            return;
        }

        for (BookedTraveler bookedTraveler : bookedTravelers) {
            if (PaxType.ADULT.equals(bookedTraveler.getPaxType()) || PaxType.CHILD.equals(bookedTraveler.getPaxType())) {
                try {
                    updateGov(bookedTraveler, bookedLeg);
                } catch (Exception ex) {
                    LOG.error(ex.getMessage(), ex);
                }
            }
        }
    }

    private void updateGov(BookedTraveler bookedTraveler, BookedLeg bookedLeg) throws Exception {

        BookedSegment bookedSegment = getElegibleBookedSegment(bookedLeg.getSegments().getCollection());

        if (null != bookedSegment) {

            String countryCode = getCountryCode(
                    bookedSegment.getSegment().getDepartureAirport(),
                    bookedSegment.getSegment().getArrivalAirport()
            );

            if (null != countryCode) {

                if("LHR".equalsIgnoreCase(bookedSegment.getSegment().getDepartureAirport())||"LHR".equalsIgnoreCase(bookedSegment.getSegment().getArrivalAirport())){
                    setNativeGGOVEntryGB(bookedTraveler,bookedLeg);
                }

                BookingClass bookingClass = ShoppingCartUtil.getBookingClassMap(
                        bookedTraveler,
                        bookedSegment.getSegment().getSegmentCode()
                );

                if (null == bookingClass) {

                    LOG.error(ErrorCodeDescriptions.MSG_CODE_ERROR_NOT_FOUND_BOOKINGCLASS_FOR_SEGMENTCODE_IN_SEGMENT
                            + bookedSegment.getSegment().getSegmentCode()
                            + " PASSENGER ID : "
                            + bookedTraveler.getId()
                    );

                    throw new GenericException(Response.Status.BAD_REQUEST,
                            ErrorType.NOT_EXIST_BOOKING_CLASS_IN_PASSENGER,
                            ErrorType.NOT_EXIST_BOOKING_CLASS_IN_PASSENGER.getFullDescription()
                            + " FOR SEGMENTCODE : "
                            + bookedSegment.getSegment().getSegmentCode()
                            + " PASSENGER ID : "
                            + bookedTraveler.getId()
                    );

                }

                // adult
                UpdateSecurityInfoRQType updateSecurityInfoRQ = getPassengerSecurityRQGOV(
                        bookedSegment.getSegment(),
                        bookingClass,
                        bookedTraveler,
                        countryCode
                );
                
                LOG.info("updateSecurityInfoRQ: " + passengerSecurityService.toString(updateSecurityInfoRQ));

                UpdateSecurityInfoRSType updateSecurityInfoRS = passengerSecurityService.passengerSecurity(updateSecurityInfoRQ);
                LOG.info("updateSecurityInfoRS: " + passengerSecurityService.toString(updateSecurityInfoRS));

                if (!ErrorOrSuccessCode.SUCCESS.equals(updateSecurityInfoRS.getResult().getStatus())) {
                    ShoppingCartErrorUtil.sabreErrorPassengerSecurity(updateSecurityInfoRS);
                }
            }

        }

    }

    /**
     *
     * @param segment
     * @param bookingClass
     * @param bookedTraveler
     * @param countryCode
     * @return
     * @throws Exception
     */
    private UpdateSecurityInfoRQType getPassengerSecurityRQGOV(Segment segment, BookingClass bookingClass, BookedTraveler bookedTraveler, String countryCode) throws Exception {

        UpdateSecurityInfoRQType passengerSecurityRQ = new UpdateSecurityInfoRQType();

        passengerSecurityRQ.setItinerary(getSecurityItinerayACS(segment, bookingClass));
        passengerSecurityRQ.setPassengerInfo(getPassengerInfoType(bookedTraveler));
        
        passengerSecurityRQ.setPassengerSecurityInfo(
                getTravelGovVerification(
                        countryCode
                )
        );

        return passengerSecurityRQ;
    }

    /**
     * @param segment
     * @param bookingClass
     * @return
     * @throws Exception
     */
    private ItineraryType getSecurityItinerayACS(Segment segment, BookingClass bookingClass) throws Exception {

        ItineraryType itineraryType = new ItineraryType();

        itineraryType.setAirline(segment.getOperatingCarrier());
        itineraryType.setFlight(segment.getOperatingFlightCode());
        itineraryType.setDepartureDate(ShoppingCartUtil.getDepartureDate(segment.getDepartureDateTime()));
        itineraryType.setOrigin(segment.getDepartureAirport());
        itineraryType.setBookingClass(bookingClass.getBookingClass());

        return itineraryType;
    }

    private PassengerInfoType getPassengerInfoType(BookedTraveler bookedTraveler) {
        PassengerInfoType passengerInfoType = new PassengerInfoType();
        passengerInfoType.setLastName(bookedTraveler.getLastName());
        passengerInfoType.setPassengerID(bookedTraveler.getId());
        return passengerInfoType;
    }

    /**
     *
     * @param countryCode
     * @return
     */
    private PassengerSecurityInfoType getTravelGovVerification(String countryCode) {

        PassengerSecurityInfoType passengerSecurityInfo = new PassengerSecurityInfoType();
        
        SecurityChecksType  securityChecksType = new SecurityChecksType();
        passengerSecurityInfo.setValidateSecurityInfo(securityChecksType);
        SecurityChecksType.RequestGOV requestGOV = new SecurityChecksType.RequestGOV();
        securityChecksType.setRequestGOV(requestGOV);
        
        requestGOV.setCountryCode(countryCode);
        requestGOV.setValue("GOV");
        
//        PassengerDocumentsType passengerDocuments = new PassengerDocumentsType();
//
//        GOV gov = new GOV();
//        OverrideGOV overrideGOV = new OverrideGOV();
//        overrideGOV.setAction(ActionType.EDIT);
//        overrideGOV.setCountryCode(countryCode);
//        gov.setOverrideGOV(overrideGOV);
//        passengerDocuments.setGOV(gov);
//        passengerSecurityInfo.setManageSecurityInfo(passengerDocuments);

        return passengerSecurityInfo;
    }

    public String setNativeGGOVEntryGB(BookedTraveler bookedTraveler, BookedLeg bookedLeg) {

        SabreCommandLLSRS sabreCommandLLSRS = null;

        String flightNumber = bookedLeg.getSegments().getLegCode().substring(7, 11);
        String monthNumber = bookedLeg.getSegments().getLegCode().substring(17, 19);
        String monthName = Month.of(Integer.parseInt(monthNumber)).toString().substring(0, 3);
        String day = bookedLeg.getSegments().getLegCode().substring(20, 22);
        String commandToList = "G*L" + flightNumber + "/" + day + monthName + "LHR\u00a5DOCS/XUK";
        LOG.info("commandToList = " + commandToList);

        try {
            sabreCommandLLSRS = sabreCommandService.sabreCommand(commandToList, "SDSXML");

            if (sabreCommandLLSRS != null) {
                if (sabreCommandLLSRS.getXMLContent() != null) {

                    List<GGOVPassenger> ggovPassengerList = readListingResponse(sabreCommandLLSRS.getXMLContent().getAny());
                    for (GGOVPassenger pax : ggovPassengerList) {
                        if (null != pax) {
                            String passengerIdBookingClass = bookedTraveler.getBookingClasses().getCollection().get(0).getPassengerId();
                            if (pax.getPassengerId().equalsIgnoreCase(passengerIdBookingClass)) {
                                String commandToGGOV = "GGOV" + pax.getLineNumber() + "/GB";
                                SabreCommandLLSRS sabreCommandLLSRSGGOV = sabreCommandService.sabreCommand(commandToGGOV, "SDSXML");
                            }
                        }
                    }
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public List<GGOVPassenger> readListingResponse(List<Element> elements){
        List<GGOVPassenger> listResponse = new ArrayList<>();
        GGOVPassenger passenger = null;

        NodeList items =  elements.get(0).getChildNodes();

        for (int i = 0; i < items.getLength(); i++) {

            Node n = items.item(i);

            if (n.getNodeType() != Node.ELEMENT_NODE)
                continue;

            Element e = (Element) n;

            passenger = new GGOVPassenger();

            if("PD0015".equalsIgnoreCase(e.getTagName())){
                passenger.setLineNumber(getNodeValue(e, "lineNumber"));
                passenger.setLastName(getNodeValue(e, "lastName"));
                passenger.setFirstName(getNodeValue(e, "firstName"));
                passenger.setRecordLocator(getNodeValue(e,"recordLocator"));
                passenger.setTicketNumber(getNodeValue(e, "PD001501ZJ"));
                passenger.setPassengerId(getNodeValue(e, "PD001503F1"));

                listResponse.add(passenger);
            }


        }

        LOG.info("List Response : {}", new Gson().toJson(listResponse));

        return listResponse;
    }

    private String getNodeValue( Element e, String name) {

        NodeList nodeList = e.getElementsByTagName(name);
        if ( nodeList != null && nodeList.getLength() > 0 ) {
            Node node = nodeList.item(0);
            if ( node != null && node.hasChildNodes() ) {
                return node.getFirstChild().getNodeValue();
            }
        }

        return null;
    }

    protected class IataAirport {

        private String iataCode;
        private String countryCode;

        public IataAirport() {
        }

        public IataAirport(String iataCode, String countryCode) {
            this.iataCode = iataCode;
            this.countryCode = countryCode;
        }

        /**
         * @return the iataCode
         */
        public String getIataCode() {
            return iataCode;
        }

        /**
         * @param iataCode the iataCode to set
         */
        public void setIataCode(String iataCode) {
            this.iataCode = iataCode;
        }

        /**
         * @return the countryCode
         */
        public String getCountryCode() {
            return countryCode;
        }

        /**
         * @param countryCode the countryCode to set
         */
        public void setCountryCode(String countryCode) {
            this.countryCode = countryCode;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 97 * hash + Objects.hashCode(this.iataCode);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final IataAirport other = (IataAirport) obj;
            if (!Objects.equals(this.iataCode, other.iataCode)) {
                return false;
            }
            return true;
        }

    }
}
