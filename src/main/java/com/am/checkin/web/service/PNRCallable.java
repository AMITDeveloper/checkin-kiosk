package com.am.checkin.web.service;

import com.aeromexico.commons.model.PNR;
import com.aeromexico.commons.model.PNRCollection;
import com.aeromexico.commons.model.rq.PnrRQ;
import com.aeromexico.sabre.api.session.common.ServiceCall;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * PNR Callable Class
 * @author Manuel Paz
 */
public class PNRCallable implements Callable<PNR>{
    
    
    private final PNRLookupService pnrLookupService;    
    private final PnrRQ pnrRQ;
    private final List<ServiceCall> serviceCallList = new ArrayList<>();
    private static final Logger LOG = LoggerFactory.getLogger(PNRCallable.class);
    
    public PNRCallable(PnrRQ pnrRQ, PNRLookupService pnrLookupService){
        this.pnrRQ= pnrRQ;
        this.pnrLookupService= pnrLookupService;
    }

    @Override
    public PNR call() throws Exception {
        try{            
            PNRCollection pnrCollection=
                    pnrLookupService.lookUpReservation(pnrRQ, serviceCallList);

            if(pnrCollection != null){
                if (pnrCollection.getCollection().size() > 0) {
                    return pnrCollection.getCollection().get(0);
                }
            }
        }catch (Exception e) {
            throw e;
        }
        
        return null;
    }
    
}
