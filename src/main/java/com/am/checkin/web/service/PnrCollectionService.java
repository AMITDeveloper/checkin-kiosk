package com.am.checkin.web.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;

import com.aeromexico.commons.web.types.PosType;
import com.aeromexico.commons.web.util.Constants;
import com.aeromexico.dao.client.exception.MongoDisconnectException;
import org.aeromexico.commons.exception.config.ReadProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.loggingutils.LogUtil;
import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.BookedSegmentCollection;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.BookingClass;
import com.aeromexico.commons.model.BookingClassMap;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.PNR;
import com.aeromexico.commons.model.PNRCollection;
import com.aeromexico.commons.model.Segment;
import com.aeromexico.commons.web.types.BookedLegCheckinStatusType;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.util.CabinUtil;
import com.aeromexico.dao.async.SimpleAsync;
import com.aeromexico.dao.services.IPNRLookupDao;
import com.am.checkin.web.event.dispatcher.UpdatePNRCollectionAfterCheckinDispatcher;
import com.am.checkin.web.event.dispatcher.UpdatePNRCollectionDispatcher;
import com.am.checkin.web.event.model.UpdatePNRCollectionAfterCheckinEvent;
import com.am.checkin.web.event.model.UpdatePNRCollectionEvent;
import com.google.gson.Gson;
import com.mongodb.MongoException;

/**
 * @author adrian
 */
@Named
@ApplicationScoped
public class PnrCollectionService {

    private static final Logger LOG = LoggerFactory.getLogger(PnrCollectionService.class);
    private static final Gson GSON = new Gson();
    private static final String SERVICE = "Pnr Collection Service";

    @Inject
    private IPNRLookupDao pnrLookupDao;

    @Inject
    private UpdatePNRCollectionDispatcher updatePNRCollectionDispatcher;

    @Inject
    private UpdatePNRCollectionAfterCheckinDispatcher updatePNRCollectionAfterCheckinDispatcher;

    @Inject
    private ReadProperties prop;

    public void savePnrCollection(PNRCollection pnrCollection) throws Exception {
        try {
            int code = pnrLookupDao.savePNRCollection(pnrCollection);

            LOG.info("DB code:" + code);
        } catch (Exception ex) {
            LOG.info("Error updating data base: " + ex.getMessage(), ex);
        }
    }

    /**
     * @param pnrCollection
     * @param async
     * @throws Exception
     */
    public void updatePnrCollection(PNRCollection pnrCollection, boolean async) throws Exception {

        try {
            if (async) {
                UpdatePNRCollectionEvent updatePNRCollectionEvent = new UpdatePNRCollectionEvent(pnrCollection);
                getUpdatePNRCollectionDispatcher().publish(updatePNRCollectionEvent);
            } else {
                getPnrLookupDao().savePNRCollection(pnrCollection);
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
            throw ex;
        }

    }

    /**
     * @param pnrCollection This method is used to update the PNR collection
     * MongoDB in an asynchronous mode
     * @param async
     * @throws java.lang.Exception
     */
    public void updatePnrCollectionAfterCheckin(PNRCollection pnrCollection, boolean async) throws Exception {

        try {
            if (async) {
                UpdatePNRCollectionAfterCheckinEvent updatePNRCollectionAfterCheckinEvent = new UpdatePNRCollectionAfterCheckinEvent(pnrCollection);
                getUpdatePNRCollectionAfterCheckinDispatcher().publish(updatePNRCollectionAfterCheckinEvent);
            } else {
                getPnrLookupDao().savePNRCollectionAfterCheckin(pnrCollection);
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
            throw ex;
        }

    }

    /**
     * @param pnrCollection
     * @param recordLocator
     * @param cartId
     */
    public void updatePnrCollectionAfterCheckin(PNRCollection pnrCollection, String recordLocator, String cartId) {
        boolean mustBeAsynchronous = false;
        updatePnrCollectionAfterCheckin(pnrCollection, mustBeAsynchronous, recordLocator, cartId);
    }

    /**
     * This method is used to update the PNR collection MongoDB in an
     * asynchronous mode
     *
     * @param pnrCollection
     * @param mustBeAsynchronous
     * @param recordLocator
     * @param cartId
     */
    public void updatePnrCollectionAfterCheckin(
            PNRCollection pnrCollection,
            boolean mustBeAsynchronous,
            String recordLocator,
            String cartId
    ) {
        if (mustBeAsynchronous) {
            try {
                UpdatePNRCollectionAfterCheckinEvent updatePNRCollectionAfterCheckinEvent = new UpdatePNRCollectionAfterCheckinEvent(pnrCollection);
                getUpdatePNRCollectionAfterCheckinDispatcher().publish(updatePNRCollectionAfterCheckinEvent);
            } catch (Exception ex) {
                LOG.error(
                        LogUtil.getLogError(
                                SERVICE, "Update Pnr Collection After Checkin", recordLocator, cartId, ex
                        )
                );
            }
        } else {
            try {
                UpdatePNRCollectionAfterCheckinEvent updatePNRCollectionAfterCheckinEvent = new UpdatePNRCollectionAfterCheckinEvent(pnrCollection);
                int code = getPnrLookupDao().savePNRCollectionAfterCheckin(updatePNRCollectionAfterCheckinEvent.getPnrCollection());

                String message = (0 == code) ? "document inserted" : "document merged";
                LOG.info(
                        LogUtil.getLogInfo(SERVICE, recordLocator, cartId, "DB code:" + code + " " + message)
                );
            } catch (Exception ex) {
                LOG.error(
                        LogUtil.getLogError(
                                SERVICE, "Error updating data base", recordLocator, cartId, ex
                        )
                );
            }
        }
    }

    /**
     * @param pnrCollection This method will fire an asynchronous event
     */
    public void fireUpdatePnrCollection(PNRCollection pnrCollection) {
        UpdatePNRCollectionEvent updatePNRCollectionEvent = new UpdatePNRCollectionEvent(pnrCollection);
        getUpdatePNRCollectionDispatcher().publish(updatePNRCollectionEvent);
    }

    /**
     * @param cartId
     * @return
     * @throws Exception
     */
    public PNRCollection getPNRCollection(String cartId, String pos) throws Exception {
        PNRCollection pnrCollection = null;
        try{
             pnrCollection = getPnrLookupDao().readPNRCollectionByCartId(cartId);
        }catch (MongoDisconnectException ex){
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(pos)) {
                LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), ex.fillInStackTrace());
                throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getCode(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getActionable(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getMessage(),
                        Response.Status.INTERNAL_SERVER_ERROR);
            } else {
            LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), ex.fillInStackTrace());
            throw new GenericException (prop.getMessagesError(Constants.CODE_12110101).getCode(),
                    prop.getMessagesError(Constants.CODE_12110101).getActionable(),
                    prop.getMessagesError(Constants.CODE_12110101).getMessage(),
                    Response.Status.INTERNAL_SERVER_ERROR);
            }
        }
        if (null == pnrCollection || null == pnrCollection.getRecordLocator()) {
            LOG.error(LogUtil.getLogError(SERVICE, "Pnr collection not found to do purchase", null, cartId));
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION, "Pnr collection not found");
        }
        return pnrCollection;

    }

    /**
     * @param recordLocator
     * @param cartId
     * @return
     * @throws Exception
     */
    public PNRCollection getPNRCollectionByRecordLocatorAfterCheckin(String recordLocator, String cartId) throws Exception {
        PNRCollection pnrCollection = getPnrLookupDao().readPNRCollectionByRecordLocatorAfterCheckin(recordLocator);
        if (null == pnrCollection || null == pnrCollection.getRecordLocator()) {
            LOG.error(LogUtil.getLogError(SERVICE, "Pnr collection not found to do purchase", recordLocator, cartId));
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION, "Pnr collection not found");
        }

        return pnrCollection;
    }

    @SimpleAsync
    public PNRCollection createOrUpdatePnrCollectionAfterCheckinAsync(
            PNRCollection pnrCollection, String recordLocator, String cartId, boolean create
    ) throws ClassNotFoundException, IOException {
        boolean mustBeAsynchronous = true;
        return createOrUpdatePnrCollectionAfterCheckin(
                pnrCollection, recordLocator, cartId, create, mustBeAsynchronous
        );
    }

    public PNRCollection createOrUpdatePnrCollectionAfterCheckin(
            PNRCollection pnrCollection, String recordLocator, String cartId, boolean create
    ) throws ClassNotFoundException, IOException {
        boolean mustBeAsynchronous = false;

        return createOrUpdatePnrCollectionAfterCheckin(
                pnrCollection, recordLocator, cartId, create, mustBeAsynchronous
        );
    }

    /**
     * @param pnrCollection
     * @param recordLocator
     * @param cartId
     * @param create
     * @param mustBeAsynchronous
     * @return
     * @throws ClassNotFoundException
     * @throws IOException
     */
    public PNRCollection createOrUpdatePnrCollectionAfterCheckin(
            PNRCollection pnrCollection,
            String recordLocator,
            String cartId,
            boolean create,
            boolean mustBeAsynchronous
    ) throws ClassNotFoundException, IOException {
        try {
            PNRCollection pnrCollectionDB = null;
            long startTime;
            startTime = Calendar.getInstance().getTimeInMillis();

            try {
                pnrCollectionDB = getPnrLookupDao().readPNRCollectionByRecordLocatorAfterCheckin(recordLocator);
            } catch (Exception ex) {
                //
            }

            //Set cabin type
            PNR pnr = pnrCollection.getPNRByRecordLocator(recordLocator);
            if (null != pnr) {
                for (BookedLeg bookedLeg : pnr.getLegs().getCollection()) {
                    for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {
                        boolean isFirstClassCabin = CabinUtil.isFirstClassCabin(bookedSegment.getSegment().getBookingClass());
                        String cabin = isFirstClassCabin ? "premier" : "main";
                        bookedSegment.getSegment().setCabin(cabin);
                    }
                }
            }

            LOG.info("Time reading collection pnr after checkin: {}", (Calendar.getInstance().getTimeInMillis() - startTime));

            if (null == pnrCollectionDB) {
                startTime = Calendar.getInstance().getTimeInMillis();
                if (create) {
                    updatePnrCollectionAfterCheckin(pnrCollection, mustBeAsynchronous, recordLocator, cartId);
                }
                LOG.info("Time creating collection pnr after checkin: {}", (Calendar.getInstance().getTimeInMillis() - startTime));

                startTime = Calendar.getInstance().getTimeInMillis();
                pnrCollectionDB = new PNRCollection(GSON.toJson(pnrCollection));
                LOG.info("Time creating object collection pnr after checkin: {}", (Calendar.getInstance().getTimeInMillis() - startTime));

                return pnrCollectionDB;
            } else {
                startTime = Calendar.getInstance().getTimeInMillis();
                boolean modified = false;

                if (null != pnr) {

                    //Map<String, BookingClass> bookingClassMapMatcher = this.getBookingClassMap(pnr.getCarts().getCollection());
                    //update changes on carts from pnrCollection to after checkin
                    for (CartPNR cartPNR : pnr.getCarts().getCollection()) {
                        if (null != cartPNR.getLegCode() && !cartPNR.getLegCode().trim().isEmpty()) {
                            CartPNR cartPNRDB = pnrCollectionDB.getCartPNRByLegCode(cartPNR.getLegCode());
                            if (null == cartPNRDB) {
                                // add cart to collection
                                try {
                                    if (pnrCollectionDB.addCartPNRByPnrAndLegCode(
                                            pnr.getPnr(),
                                            new CartPNR(GSON.toJson(cartPNR)))) {
                                        modified = true;
                                    }
                                } catch (Exception ex) {
                                    //
                                }
                            }
                        }
                    }

                    //update changes on legs from pnrCollection to after checkin
                    for (BookedLeg bookedLeg : pnr.getLegs().getCollection()) {

                        if (null != bookedLeg.getSegments()
                                && null != bookedLeg.getSegments().getLegCode()
                                && !bookedLeg.getSegments().getLegCode().trim().isEmpty()) {

                            BookedLeg legDB = pnrCollectionDB.getBookedLegByLegCode(bookedLeg.getSegments().getLegCode());

                            if (null == legDB) {
                                try {
                                    if (pnrCollectionDB.addBookedLegByPnrAndLegCode(
                                            pnr.getPnr(),
                                            new BookedLeg(GSON.toJson(bookedLeg)))) {
                                        modified = true;
                                    }
                                } catch (Exception ex) {

                                }
                            }
                        }
                    }

                    //update changes on legs from after checkin to pnrCollection
                    PNR pnrDB = pnrCollectionDB.getPNRByRecordLocator(recordLocator);

                    for (BookedLeg legDB : pnrDB.getLegs().getCollection()) {
                        if (null != legDB.getSegments()
                                && null != legDB.getSegments().getLegCode()
                                && !legDB.getSegments().getLegCode().trim().isEmpty()) {

                            BookedLeg bookedLeg = pnrCollection.getBookedLegByLegCode(legDB.getSegments().getLegCode());

                            if (null == bookedLeg) {
                                legDB.setCheckinStatus(BookedLegCheckinStatusType.CANCEL);
                                modified = true;
                            } else {
                                legDB.setSegments(bookedLeg.getSegments());
                                modified = true;
                            }
                        }
                    }

                }
                LOG.info("Time modifying collection pnr after checkin: {}", (Calendar.getInstance().getTimeInMillis() - startTime));

                if (modified) {
                    startTime = Calendar.getInstance().getTimeInMillis();
                    updatePnrCollectionAfterCheckin(pnrCollectionDB, mustBeAsynchronous, recordLocator, cartId);
                    LOG.info("Time updating collection pnr after checkin: {}", (Calendar.getInstance().getTimeInMillis() - startTime));
                    LOG.info("PNRCollection updated in DB, {} {}", recordLocator, cartId);
                }

                return pnrCollectionDB;
            }
        } catch (MongoException ex) {
            LOG.error("Error saving document into data base, {} {} {} {}",prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(),recordLocator, cartId, ex);
        } catch (Exception ex) {
            LOG.error("Error saving document into data base, {} {} {}", recordLocator, cartId, ex);
        }

        return new PNRCollection(GSON.toJson(pnrCollection));
    }

    private Map<String, BookingClass> getBookingClassMap(List<CartPNR> cartPNRList) {
        Map<String, BookingClass> resultMap = new HashMap<>();
        for (int j = 0; j < cartPNRList.size(); j++) {
            CartPNR cartPNR = cartPNRList.get(j);
            BookedTraveler bookedTraveler = cartPNR.getTravelerInfo().getCollection().get(0);
            BookingClassMap bookingClassMap = bookedTraveler.getBookingClasses();
            List<BookingClass> bookingClassList = bookingClassMap.getCollection();

            for (int k = 0; k < bookingClassList.size(); k++) {
                BookingClass bookingClass = bookingClassList.get(k);
                resultMap.put(bookingClass.getSegmentCode(), bookingClass);
            }
        }
        return resultMap;
    }

    /**
     * @return the pnrLookupDao
     */
    public IPNRLookupDao getPnrLookupDao() {
        return pnrLookupDao;
    }

    /**
     * @param pnrLookupDao the pnrLookupDao to set
     */
    public void setPnrLookupDao(IPNRLookupDao pnrLookupDao) {
        this.pnrLookupDao = pnrLookupDao;
    }

    /**
     * @return the updatePNRCollectionDispatcher
     */
    public UpdatePNRCollectionDispatcher getUpdatePNRCollectionDispatcher() {
        return updatePNRCollectionDispatcher;
    }

    /**
     * @param updatePNRCollectionDispatcher the updatePNRCollectionDispatcher to
     * set
     */
    public void setUpdatePNRCollectionDispatcher(UpdatePNRCollectionDispatcher updatePNRCollectionDispatcher) {
        this.updatePNRCollectionDispatcher = updatePNRCollectionDispatcher;
    }

    /**
     * @return the updatePNRCollectionAfterCheckinDispatcher
     */
    public UpdatePNRCollectionAfterCheckinDispatcher getUpdatePNRCollectionAfterCheckinDispatcher() {
        return updatePNRCollectionAfterCheckinDispatcher;
    }

    /**
     * @param updatePNRCollectionAfterCheckinDispatcher the
     * updatePNRCollectionAfterCheckinDispatcher to set
     */
    public void setUpdatePNRCollectionAfterCheckinDispatcher(UpdatePNRCollectionAfterCheckinDispatcher updatePNRCollectionAfterCheckinDispatcher) {
        this.updatePNRCollectionAfterCheckinDispatcher = updatePNRCollectionAfterCheckinDispatcher;
    }
}
