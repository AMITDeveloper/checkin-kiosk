package com.am.checkin.web.service;

import com.aeromexico.commons.flightStatus.model.FlightInfo;
import com.aeromexico.commons.flightStatus.model.FlightSchedule;
import com.aeromexico.commons.flightStatus.model.FlightScheduleResult;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.aeromexico.commons.flightStatus.model.FlightStatusResult;
import com.aeromexico.commons.flightStatus.model.FlightStatusSearch;
import com.am.checkin.web.sabreFacades.FlightStatusFacade;
import com.am.checkin.web.sabreFacades.SabreDCServiceFacadeCheckIn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeromexico.commons.model.FlightStatusCollection;
import com.aeromexico.commons.model.Segment;
import com.aeromexico.commons.model.SegmentStatus;
import com.aeromexico.commons.model.SegmentStatusCollection;
import com.aeromexico.commons.model.rq.FlightStatusRQ;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Armando Arroyo
 */
@Named
@ApplicationScoped
public class FlightStatusService extends SabreDCServiceFacadeCheckIn {

    private static final Logger log = LoggerFactory.getLogger(FlightStatusService.class);

    @Inject
    @Named("CheckInSetUpConfigFactoryWrapper")
    private CheckInSetUpConfigFactoryWrapper setUpConfigFactory;

    @PostConstruct
    public void init() {
        if (null == setUpConfigFactory) {
            setUpConfigFactory = new CheckInSetUpConfigFactoryWrapper();
        }
    }

    @Inject
    private FlightStatusFacade flightStatusFacade;

    public FlightStatusCollection callFlightStatus(FlightStatusRQ flightStatusRQ) throws Exception {
        if (null == flightStatusRQ) {
            throw new Exception("Flight Status Request is null");
        }
        log.info("FlightStatusRQ: {}", flightStatusRQ.toString());
        //Do we have the flight number in the request?
        if (null != flightStatusRQ.getFlight() && !flightStatusRQ.getFlight().trim().isEmpty()) {
            return getStatusForFlight(flightStatusRQ);

        } else if (null != flightStatusRQ.getOrigin() && null != flightStatusRQ.getDestination()) {
            return getStatusByOriginAndDestination(flightStatusRQ);

        } else {
            log.error("Invalid FlightStatusService request: " + flightStatusRQ);
        }

        return new FlightStatusCollection();
    }

    private FlightStatusCollection getStatusForFlight(FlightStatusRQ flightStatusRQ) throws Exception {
        FlightStatusSearch flightStatusSearch = new FlightStatusSearch();
        flightStatusSearch.setFlightNumber(Integer.valueOf(flightStatusRQ.getFlight()));
        flightStatusSearch.setDepartureDate(flightStatusRQ.getDate());
        flightStatusSearch.setAirlineCode("AM");
        flightStatusSearch.setOrigin(flightStatusRQ.getOrigin());
        flightStatusSearch.setDestination(flightStatusRQ.getDestination());

        FlightStatusResult flightStatusResult = 
                flightStatusFacade.getSabreFlightStatus(
                    flightStatusSearch,
                    flightStatusRQ.getStore()
                );
        FlightStatusCollection flightStatusCollection = new FlightStatusCollection();
        if (null != flightStatusResult) {
            log.info("FlightStatusResult: {}", flightStatusResult.toString());
            SegmentStatusCollection segmentStatusCollection = new SegmentStatusCollection();
            List<SegmentStatus> listSegmentStatus = new ArrayList<>();
            listSegmentStatus.add(this.getSegmentStatus(flightStatusResult));
            segmentStatusCollection.setCollection(listSegmentStatus);
            
            List<SegmentStatusCollection> listSegmentStatusCollection = new ArrayList<>();
            listSegmentStatusCollection.add(segmentStatusCollection); 
                        
            flightStatusCollection.setCollection(listSegmentStatusCollection);
        }

        return flightStatusCollection;
    }

    private FlightStatusCollection getStatusByOriginAndDestination(FlightStatusRQ flightStatusRQ) throws Exception {
        FlightStatusSearch flightStatusSearch = new FlightStatusSearch();
        flightStatusSearch.setDepartureDate(flightStatusRQ.getDate());
        flightStatusSearch.setOrigin(flightStatusRQ.getOrigin());
        flightStatusSearch.setDestination(flightStatusRQ.getDestination());

        FlightScheduleResult flightScheduleResult = flightStatusFacade.getSabreSchedules(
                flightStatusSearch,
                flightStatusRQ.getStore()
        );
        FlightStatusCollection flightStatusCollection = new FlightStatusCollection();
        if (null != flightScheduleResult) {
            log.info("FlightScheduleResult: {}", flightScheduleResult.toString());
            List<SegmentStatusCollection> listSegmentStatusCollection = new ArrayList<>();
            for (FlightSchedule flightSchedule : flightScheduleResult.getFlightSchedules()) {
                SegmentStatusCollection segmentStatusCollection = new SegmentStatusCollection();
                List<SegmentStatus> listSegmentStatus = new ArrayList<>();
                
                SegmentStatus segmentStatus = new SegmentStatus();
                segmentStatus.setStatus("");
                for (FlightInfo flightInfo : flightSchedule.getFlights()) {
                    segmentStatus.setSegment(this.getSegmentInfo(flightInfo));                    
                    listSegmentStatus.add(segmentStatus);
                }              
                segmentStatusCollection.setCollection(listSegmentStatus);

                segmentStatusCollection.setTotalFlightTimeInMinutes(flightSchedule.getTotalDuration());
                if (flightSchedule.getTotalStops() > 1) {
                    segmentStatusCollection.setLegType("CONNECTING");
                } else {
                    segmentStatusCollection.setLegType("NON-STOP");
                }
                listSegmentStatusCollection.add(segmentStatusCollection);
            }
            flightStatusCollection.setCollection(listSegmentStatusCollection);    
        }                
        return new FlightStatusCollection();
    }
    
    private SegmentStatus getSegmentStatus(FlightStatusResult flightStatusResult) {
        SegmentStatus segmentStatus = new SegmentStatus();
        
        segmentStatus.setStatus(flightStatusResult.getFlights()[0].getDepartureInfo().getFlightStatus());
        segmentStatus.setBoardingTerminal(flightStatusResult.getFlights()[0].getDepartureInfo().getTerminal());
        segmentStatus.setBoardingGate(flightStatusResult.getFlights()[0].getDepartureInfo().getGate());
        
        segmentStatus.setArrivalTerminal(flightStatusResult.getFlights()[0].getArrivalInfo().getTerminal());
        segmentStatus.setArrivalGate(flightStatusResult.getFlights()[0].getArrivalInfo().getTerminal());
                
        segmentStatus.setSegment(this.getSegmentInfo(flightStatusResult));
        return segmentStatus;
    }
    
    private Segment getSegmentInfo(FlightStatusResult flightStatusResult) {
        Segment segment = new Segment();
        segment.setMarketingCarrier(flightStatusResult.getAirlineCode());
        segment.setMarketingFlightCode(String.valueOf(flightStatusResult.getFlightNumber()));
        segment.setOperatingCarrier(flightStatusResult.getAirlineCode());
        segment.setOperatingFlightCode(String.valueOf(flightStatusResult.getFlightNumber()));             
        segment.setDepartureDateTime(flightStatusResult.getFlights()[0].getDepartureInfo().getScheduledDateTime());
        segment.setArrivalDateTime(flightStatusResult.getFlights()[0].getArrivalInfo().getScheduledDateTime());
        segment.setDepartureAirport(flightStatusResult.getFlights()[0].getDepartureInfo().getAirportCode());
        segment.setArrivalAirport(flightStatusResult.getFlights()[0].getArrivalInfo().getAirportCode());
        
        String segmentCode = segment.getDepartureAirport()
                + "_" + segment.getArrivalAirport()
                + "_" + segment.getMarketingCarrier()
                + "_" + segment.getDepartureDateTime().split("T")[0]
                + "_" + segment.getDepartureDateTime().split("T")[1].replaceAll(":", "").substring(0, 4);

        segment.setSegmentCode(segmentCode);
        return segment;
    }

    private Segment getSegmentInfo(FlightInfo flightInfo) {
        Segment segment = new Segment();
        segment.setOperatingCarrier(flightInfo.getOperatingCarrier());
        if ("**".equalsIgnoreCase(segment.getOperatingCarrier())) {
            int flightNumber = Integer.parseInt(segment.getMarketingFlightCode());
            if (flightNumber >= 5000 && flightNumber <= 5999) {
                segment.setOperatingCarrier("DL");
            } else if (flightNumber >= 6000 && flightNumber <= 6499) {
                segment.setOperatingCarrier("AF");
            } else if (flightNumber >= 6500 && flightNumber <= 6599) {
                segment.setOperatingCarrier("KL");
            } else if (flightNumber >= 6600 && flightNumber <= 6699) {
                segment.setOperatingCarrier("AZ");
            } else if (flightNumber >= 6700 && flightNumber <= 6799) {
                segment.setOperatingCarrier("KE");
            } else if (flightNumber >= 6800 && flightNumber <= 6999) {
                segment.setOperatingCarrier("UX");
            } else if (flightNumber >= 7000 && flightNumber <= 7099) {
                segment.setOperatingCarrier("OK");
            } else if (flightNumber >= 7900 && flightNumber <= 7999) {
                segment.setOperatingCarrier("CM");
            } else if (flightNumber >= 8220 && flightNumber <= 8269) {
                segment.setOperatingCarrier("AV");
            } else if (flightNumber >= 8270 && flightNumber <= 8319) {
                segment.setOperatingCarrier("AS");
            }
        }
        segment.setOperatingFlightCode(String.valueOf(flightInfo.getFlightNumber()));
        //segment.setMarketingFlightCode(String.valueOf(flightInfo.getFlightNumber()));
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime departureLocalTime = LocalDateTime.parse(flightInfo.getDeparture());
        String departureLocalTimeString = dateTimeFormatter.format(departureLocalTime);
        segment.setDepartureDateTime(departureLocalTimeString);
        
        LocalDateTime arrivalLocalTime = LocalDateTime.parse(flightInfo.getArrival());
        String arrivalLocalTimeString = dateTimeFormatter.format(arrivalLocalTime);
        segment.setArrivalDateTime(arrivalLocalTimeString);
        
        segment.setAircraftType(flightInfo.getEquipmentType());
        segment.setDepartureAirport(flightInfo.getOrigin());
        segment.setArrivalAirport(flightInfo.getDestination());                                
        
        String segmentCode = segment.getDepartureAirport()
                + "_" + segment.getArrivalAirport()
                + "_" + segment.getMarketingCarrier()
                + "_" + segment.getDepartureDateTime().split("T")[0]
                + "_" + segment.getDepartureDateTime().split("T")[1].replaceAll(":", "").substring(0, 4);

        segment.setSegmentCode(segmentCode);
        
        return segment;
    }

}
