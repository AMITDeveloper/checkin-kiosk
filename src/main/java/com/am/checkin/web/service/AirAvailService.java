package com.am.checkin.web.service;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.aeromexico.commons.model.rq.AirAvailRQ;
import com.aeromexico.sabre.api.airavailability.AirAvailablityService;
import com.aeromexico.sabre.api.session.exception.SabreLayerUnavailableException;
import com.aeromexico.sabre.api.session.exception.SabreEmptyResponseException;
import com.sabre.webservices.sabrexml._2011._10.airavailable.OTAAirAvailRS;
import org.aeromexico.commons.exception.config.ReadProperties;

@Named
@ApplicationScoped
public class AirAvailService {

	@Inject
	private AirAvailablityService airAvailablityService;

	public OTAAirAvailRS  getAirAvail(AirAvailRQ airAvailRQ) throws Exception {
		OTAAirAvailRS otaAirAvailRS ;
		try{
			otaAirAvailRS = airAvailablityService.getAirAvail(airAvailRQ.getFrom(), airAvailRQ.getTo(), airAvailRQ.getDate(), airAvailRQ.getIncludeRules());
		}catch (SabreEmptyResponseException ser) {
			throw ser;
		}catch (SabreLayerUnavailableException se){
			throw se;
		}catch (Exception ex){
			throw ex;
		}
		return otaAirAvailRS;
	}
}
