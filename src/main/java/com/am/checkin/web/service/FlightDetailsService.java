package com.am.checkin.web.service;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.Segment;
import com.aeromexico.commons.web.types.PosType;
import com.aeromexico.sabre.api.acs.AMFlightDetailService;
import com.aeromexico.sabre.api.acs.AMFlightSeatMap;
import com.aeromexico.sabre.api.session.common.ServiceCall;
import com.aeromexico.sabre.api.session.exception.SabreEmptyResponseException;
import com.aeromexico.sabre.api.session.exception.SabreLayerUnavailableException;
import com.sabre.services.acs.bso.flightdetail.v3.ACSFlightDetailRSACS;
import com.sabre.services.stl.v3.ErrorOrSuccessCode;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;

import org.aeromexico.commons.exception.config.ReadProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adrian
 */
@Named
@ApplicationScoped
public class FlightDetailsService {

    private static final Logger LOG = LoggerFactory.getLogger(FlightDetailsService.class);

    @Inject
    private AMFlightDetailService flightDetailService;

    @Inject
    private AMFlightSeatMap flightSeatMap;

    @Inject
    private ReadProperties prop;

    /**
     * @param airlineCode
     * @param flightNumber
     * @param departureCity
     * @param departureDate
     * @return
     * @throws Exception
     */
    public ACSFlightDetailRSACS getFlightDetails(
            String airlineCode, String flightNumber,
            String departureCity, String departureDate, String pos
    ) throws Exception {
        return getFlightDetails(airlineCode, flightNumber, departureCity, departureDate, null, pos);
    }

    /**
     * @param airlineCode
     * @param flightNumber
     * @param departureCity
     * @param departureDate
     * @param serviceCallList
     * @return
     * @throws Exception
     */
    public ACSFlightDetailRSACS getFlightDetails(
            String airlineCode, String flightNumber,
            String departureCity, String departureDate,
            List<ServiceCall> serviceCallList, String pos
    ) throws Exception {
        try {


            ACSFlightDetailRSACS flightDetailRS = flightDetailService.flightDetail(
                    airlineCode, flightNumber,
                    departureCity, departureDate,
                    serviceCallList
            );
            if (flightDetailRS != null) {

                if (flightDetailRS.getResult().getStatus() != ErrorOrSuccessCode.SUCCESS) {
                    String errorMessage = flightDetailRS.getResult().getSystemSpecificResults().get(0).getErrorMessage().getValue();
                    throw new Exception(errorMessage);
                }
            }
            return flightDetailRS;
        }catch (SabreEmptyResponseException ser) {
            LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110302).toString(), ser.fillInStackTrace());
            throw ser;
        }catch (SabreLayerUnavailableException se){
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(pos)) {
                LOG.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_22110301,"FlightDetailsService.java:93", se.getMessage());
                throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getCode(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getActionable(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getMessage(),
                        Response.Status.INTERNAL_SERVER_ERROR);
            } else {
                LOG.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_12110301,"FlightDetailsService.java:99", se.getMessage());
                throw se;
            }
        }
    }

    //mahmud
    /**
     * @param currenBookedSegment
     * @param serviceCallList
     * @return
     */
    public Integer getAvailabilityFromSabre(BookedSegment currenBookedSegment, List<ServiceCall> serviceCallList) {
        Segment segment = currenBookedSegment.getSegment();
        try {
            String availability = flightDetailService.getAvailability(segment.getOperatingFlightCode(), segment.getDepartureDateTime(), segment.getDepartureAirport() + segment.getArrivalAirport(), serviceCallList);

            int inentoryCount = getAvailabilityByClassOfService(availability, "F");
            int seatMapCount = flightSeatMap.getFreeSeatsCount(segment.getOperatingCarrier(), segment.getOperatingFlightCode(), segment.getDepartureAirport(), segment.getDepartureDateTime().substring(0, 10), "F", null);

            LOG.info("SEATS FOR UPGRADE: seatmap = {}, inventory = {}", seatMapCount, inentoryCount);

            if (inentoryCount <= seatMapCount) {
                return inentoryCount;
            } else {
                return seatMapCount;
            }

        } catch (Exception e) {
            return 0;
        }
    }

    private Integer getAvailabilityByClassOfService(String availability, String classOfService) {
        if (null == availability || availability.contains("SCHEDULES NOT YET AVAILABLE FOR THIS CARRIER/DATE")) {
            return 0;
        }

        LOG.info("Availability: " + availability);

        String[] availabilityByClass = availability.split(" ");
        for (String availableSeats : availabilityByClass) {
            if (availableSeats.contains(classOfService)) {
                try {
                    String foxAvailability = availableSeats.replace("F", "");

                    if (null != foxAvailability && !foxAvailability.isEmpty()) {
                        return Integer.valueOf(foxAvailability);
                    } else {
                        return 0;
                    }
                } catch (Exception ex) {
                    LOG.error("Error - PNRLookUp - Availability : " + availability + " " + ex.getMessage(), ex);
                    return 0;
                }
            }
        }
        return 0;
    }

    public AMFlightDetailService getFlightDetailService() {
        return flightDetailService;
    }

    public void setFlightDetailService(AMFlightDetailService flightDetailService) {
        this.flightDetailService = flightDetailService;
    }

}
