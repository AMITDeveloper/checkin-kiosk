package com.am.checkin.web.service;

import com.aeromexico.commons.loggingutils.LogUtil;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.PNRCollection;
import com.aeromexico.commons.model.rq.SignatureRQ;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.util.Constants;
import com.aeromexico.dao.services.IPNRLookupDao;
import com.aeromexico.dao.util.CommonUtil;
import com.aeromexico.commons.exception.model.GenericException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;

/**
 *
 * @author adrian
 */
@Named
@ApplicationScoped
public class SignatureService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SignatureService.class);

    private static final String SERVICE = "Signature";

    @Inject
    private IPNRLookupDao pnrLookupDao;

    @Inject
    private PnrCollectionService updatePnrCollectionService;

    /**
     *
     * @param signatureRQ
     * @return
     * @throws Exception
     *
     * This method is used to store the user signature to the PNR collection
     */
    public CartPNR putSignature(SignatureRQ signatureRQ) throws Exception {

        String paymentId = signatureRQ.getCartPNRUpdate().getPaymentId();
        String signature = signatureRQ.getCartPNRUpdate().getSignature();

        // First read the pnrCollection from the database
        PNRCollection pnrCollection = pnrLookupDao.readPNRCollectionByRecordLocatorAfterCheckin(signatureRQ.getRecordLocator());

        // If the pnrCollection is null, throw an expired session exception
        if (null == pnrCollection) {
            LOGGER.error(LogUtil.getLogError(SERVICE, CommonUtil.getMessageByCode(Constants.CODE_004), signatureRQ.getRecordLocator(), null));
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION, "Pnr collection not found");
        }

        CartPNR cartPNR = pnrCollection.getCartPNRByRecordLocatorAndLegCode(signatureRQ.getRecordLocator(), signatureRQ.getCartPNRUpdate().getLegCode());

        if (null == cartPNR) {
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION, "Cart not found");
        }

        // save the signature
        boolean match = cartPNR.associateSignature(paymentId, signature);

        if (!match) {
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION, "Payment id not found");
        }

        updatePnrCollectionService.updatePnrCollectionAfterCheckin(pnrCollection, signatureRQ.getRecordLocator(), null);

        return cartPNR;
    }

}
