package com.am.checkin.web.service;

import com.aeromexico.commons.model.AppleWalletBoardingPassInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.brendamour.jpasskit.PKBarcode;
import de.brendamour.jpasskit.PKField;
import de.brendamour.jpasskit.PKPass;
import de.brendamour.jpasskit.enums.PKBarcodeFormat;
import de.brendamour.jpasskit.enums.PKTransitType;
import de.brendamour.jpasskit.passes.PKBoardingPass;
import de.brendamour.jpasskit.signing.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.awt.*;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

/**
 * Apple Wallet Service
 *
 * @author marc.archin
 *
 */
@Named
@ApplicationScoped
public class AppleWalletService {
   private static final Logger logger = LoggerFactory.getLogger(AppleWalletService.class);

   private static final String LOCALE_STRINGS_FILE = "pass.strings";
   private static final String PASSKIT_RESOURCE_ROOT_DIR = "/apple-wallet";
   private static final String PASSKIT_TEMPLATE_DIR = AppleWalletService.PASSKIT_RESOURCE_ROOT_DIR + "/templates";
   private static final String APPLE_WWDRCA_FILE = AppleWalletService.PASSKIT_RESOURCE_ROOT_DIR + "/AppleWWDRCA.cer";
   private static final String PASSKIT_CERT_FILE = AppleWalletService.PASSKIT_RESOURCE_ROOT_DIR + "/passkit.p12";


   /**
    * Default constructor
    */
   public AppleWalletService() {}


   public byte[] createPass(final AppleWalletBoardingPassInfo boardingPass) throws Exception {
      return this.doCreateBoardingPassPasskit(boardingPass);
   }


   /**
    *
    * @throws ParseException
    */
   private byte[] doCreateBoardingPassPasskit(final AppleWalletBoardingPassInfo boardingPass) throws Exception {
      try {
         /*
          * Header Fields
          */
         final List<PKField> headerFields = new ArrayList<PKField>();
         headerFields.add(this.field("flight", boardingPass.getDepartureDate(), boardingPass.getFlight()));

         /*
          * Primary Fields
          */
         final List<PKField> primaryFields = new ArrayList<PKField>();
         primaryFields.add(this.field("origin"     , boardingPass.getDepartureCity(), boardingPass.getDepartureAirport()));
         primaryFields.add(this.field("destination", boardingPass.getArrivalCity()  , boardingPass.getArrivalAirport()));

         /*
          * Secondary Fields
          */
         final List<PKField> secondaryFields = new ArrayList<PKField>();
         secondaryFields.add(this.field("passenger", "passengerHeading", boardingPass.getPassenger()));
         secondaryFields.add(this.field("group"    , "groupHeading"    , boardingPass.getBoardingGroup()));
         secondaryFields.add(this.field("seat"     , "seatHeading"     , boardingPass.getSeatAssignment()));

         /*
          * Auxiliary Fields
          */
         final List<PKField> auxiliaryFields = new ArrayList<PKField>();
         auxiliaryFields.add(this.field("boardingTime" , "boardingHeading", boardingPass.getBoardingTime()));
         auxiliaryFields.add(this.field("departureTime", "departsHeading" , boardingPass.getDepartureTime()));
         auxiliaryFields.add(this.field("terminal"     , "terminalHeading", boardingPass.getDepartureTerminal()));
         auxiliaryFields.add(this.field("gate"         , "gateHeading"    , boardingPass.getDepartureGate()));
         auxiliaryFields.add(this.field("arrivalTime"  , "arrivesHeading" , boardingPass.getArrivalTime()));

         final PKBarcode barcode = new PKBarcode();
         barcode.setMessage(boardingPass.getBarcodeData() );
         barcode.setFormat(PKBarcodeFormat.PKBarcodeFormatQR);
         barcode.setMessageEncoding(Charset.forName("iso-8859-1"));

         final List<PKBarcode> barcodes = new ArrayList<PKBarcode>();
         barcodes.add(barcode);

         /*
          * Back Fields
          */
         final List<PKField> backFields = new ArrayList<PKField>();
         backFields.add(this.field("departsBack"           , "departsBack"           , String.format("%s: %s \n %s, %s", boardingPass.getDepartureAirport(), boardingPass.getDepartureCity(), boardingPass.getDepartureTime(), boardingPass.getDepartureDate()) ));
         backFields.add(this.field("arrivesBack"           , "arrivesBack"           , String.format("%s: %s \n %s, %s", boardingPass.getArrivalAirport()  , boardingPass.getArrivalCity()  , boardingPass.getArrivalTime()  , boardingPass.getArrivalDate()) ));
         backFields.add(this.field("gateBack"              , "gateBack"              , boardingPass.getDepartureGate()));

         if ( boardingPass.getOperatingCarrier() != null && boardingPass.getOperatingCarrier().equalsIgnoreCase( "AM" )) {
        	 backFields.add(this.field("operatedByBack", "operatedByBack", "Aeromexico"));
         } else {
        	 backFields.add(this.field("operatedByBack", "", ""));
         }

         backFields.add(this.field("clubPremierBack"       , "clubPremierBack"       , boardingPass.getClubPremier()));
         backFields.add(this.field("confirmationNumberBack", "confirmationNumberBack", boardingPass.getPnr()));
         backFields.add(this.field("ticketNumberBack"      , "ticketNumberBack"      , boardingPass.getTicketNumber()));
         

         /*
          * Create the boarding pass
          */
         final PKBoardingPass pkBoardingPass = new PKBoardingPass();
         pkBoardingPass.setTransitType(PKTransitType.PKTransitTypeAir);
         pkBoardingPass.setHeaderFields(headerFields);
         pkBoardingPass.setPrimaryFields(primaryFields);
         pkBoardingPass.setSecondaryFields(secondaryFields);
         pkBoardingPass.setAuxiliaryFields(auxiliaryFields);
         pkBoardingPass.setBackFields(backFields);

         /*
          * Configure the PKPass
          */
         final PKPass pass = new PKPass();
         pass.setBoardingPass(pkBoardingPass);
         pass.setFormatVersion(1);
         pass.setPassTypeIdentifier("pass.com.aeromexico.boardingpass");
         pass.setSerialNumber(UUID.randomUUID().toString());

         try {
        	 pass.setRelevantDate( new SimpleDateFormat("yyyy-MM-dd'T'HH:mmX").parse( boardingPass.getRelevantDate() ) );
         } catch(Exception e) {
        	 logger.error( "Failed to parse relevantDate: " + e.getMessage());
        	 pass.setRelevantDate(null);
         }

         pass.setTeamIdentifier("RAW9QCVJV9");
         pass.setBarcodes(barcodes);
         pass.setOrganizationName("AeroMexico");
         pass.setLogoText("");
         pass.setDescription("description");
         pass.setBackgroundColor("rgb(50, 91, 185)");
         pass.setBackgroundColorAsObject(Color.WHITE);
         pass.setForegroundColor("rgb(22, 55, 110)");

         return this.signAndSavePass(pass, boardingPass);
      } catch (Exception e) {
         logger.error("Error to process data for apple wallet", e);
         throw e;
      }
   }

   private PKField field(final String key, final String label, final String value) {
      final PKField field = new PKField();

      field.setKey(key);
      field.setLabel(label);
      field.setValue(value);

      return field;
   }

   private byte[] signAndSavePass(final PKPass pass, final AppleWalletBoardingPassInfo boardingPass) throws UnrecoverableKeyException, NoSuchAlgorithmException, CertificateException,
            KeyStoreException, NoSuchProviderException, IOException, PKSigningException {

      final PKPassTemplateInMemory passTemplate = new PKPassTemplateInMemory();

      if ( "true".equalsIgnoreCase( boardingPass.getSkyPriority() )  && "true".equalsIgnoreCase( boardingPass.getTsaPre() ) ) {
          passTemplate.addFile("footer.png", AppleWalletService.class.getResourceAsStream(AppleWalletService.PASSKIT_TEMPLATE_DIR + "/skyPriorityTSA/footer.png")); 
          passTemplate.addFile("footer@2x.png", AppleWalletService.class.getResourceAsStream(AppleWalletService.PASSKIT_TEMPLATE_DIR + "/skyPriorityTSA/footer@2x.png"));    	  
      } else if ( "true".equalsIgnoreCase( boardingPass.getSkyPriority() ) ) {
          passTemplate.addFile("footer.png", AppleWalletService.class.getResourceAsStream(AppleWalletService.PASSKIT_TEMPLATE_DIR + "/skyPriority/footer.png")); 
          passTemplate.addFile("footer@2x.png", AppleWalletService.class.getResourceAsStream(AppleWalletService.PASSKIT_TEMPLATE_DIR + "/skyPriority/footer@2x.png"));
      } else if ( "true".equalsIgnoreCase( boardingPass.getTsaPre() ) ) {
          passTemplate.addFile("footer.png", AppleWalletService.class.getResourceAsStream(AppleWalletService.PASSKIT_TEMPLATE_DIR + "/TSA/footer.png")); 
          passTemplate.addFile("footer@2x.png", AppleWalletService.class.getResourceAsStream(AppleWalletService.PASSKIT_TEMPLATE_DIR + "/TSA/footer@2x.png"));    	  
      }

      passTemplate.addFile(PKPassTemplateInMemory.PK_LOGO,
               AppleWalletService.class.getResourceAsStream(AppleWalletService.PASSKIT_TEMPLATE_DIR + "/logo.png"));
      passTemplate.addFile(PKPassTemplateInMemory.PK_LOGO_RETINA,
               AppleWalletService.class.getResourceAsStream(AppleWalletService.PASSKIT_TEMPLATE_DIR + "/logo@2x.png"));
      passTemplate.addFile(PKPassTemplateInMemory.PK_ICON,
               AppleWalletService.class.getResourceAsStream(AppleWalletService.PASSKIT_TEMPLATE_DIR + "/icon.png"));
      passTemplate.addFile(PKPassTemplateInMemory.PK_ICON_RETINA,
               AppleWalletService.class.getResourceAsStream(AppleWalletService.PASSKIT_TEMPLATE_DIR + "/icon@2x.png"));

      final Map<Locale, String> resourcesMap = this.getLocaleResources();
      for (final Locale locale : this.getLocaleResources().keySet()) {
         final String resource = resourcesMap.get(locale);
         final InputStream is = AppleWalletService.class.getResourceAsStream(resource);
         if (is != null) {
            passTemplate.addFile(AppleWalletService.LOCALE_STRINGS_FILE, locale, is);
         }
      }

      final PKFileBasedSigningUtil pkSigningUtil = new PKFileBasedSigningUtil(new ObjectMapper());

      String keyStore = "mysecretpassw0rd";
      final PKSigningInformation pkSigningInformation =
               new PKSigningInformationUtil().loadSigningInformationFromPKCS12AndIntermediateCertificate(
                        AppleWalletService.class.getResourceAsStream(AppleWalletService.PASSKIT_CERT_FILE), keyStore,
                        AppleWalletService.class.getResourceAsStream(AppleWalletService.APPLE_WWDRCA_FILE));
      final byte[] archive = pkSigningUtil.createSignedAndZippedPkPassArchive(pass, passTemplate, pkSigningInformation);

      // final File pkPass = new File("/tmp/" + pass.getSerialNumber() + ".pkpass");
      // FileUtils.writeByteArrayToFile(pkPass, archive);
      // AppleWalletService.logger.info(String.format("Pass generated: %s", pkPass));

      return archive;
   }

   private Map<Locale, String> getLocaleResources() {
      final Map<Locale, String> resourcesMap = new HashMap<Locale, String>();

      for (final Locale locale : Locale.getAvailableLocales()) {
         resourcesMap.put(locale, String.format("%s/%s.lproj/%s", AppleWalletService.PASSKIT_TEMPLATE_DIR, locale.getLanguage(), AppleWalletService.LOCALE_STRINGS_FILE));
      }

      return resourcesMap;
   }
}
