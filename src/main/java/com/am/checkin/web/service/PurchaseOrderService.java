package com.am.checkin.web.service;

import com.aeromexico.codes.ICodesGenerator;
import com.aeromexico.codes.impl.CodesGenerator;
import com.aeromexico.commons.bigquery.services.BigQueryService;
import com.aeromexico.commons.exception.model.CybersourceException;
import com.aeromexico.commons.exception.model.CybersourceInternalErrorException;
import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.exception.model.PaymentRetryException;
import com.aeromexico.commons.exception.model.RetryCheckinException;
import com.aeromexico.commons.loggingutils.LogUtil;
import com.aeromexico.commons.model.AbstractAncillary;
import com.aeromexico.commons.model.AbstractPaymentRequest;
import com.aeromexico.commons.model.AbstractSegmentChoice;
import com.aeromexico.commons.model.Address;
import com.aeromexico.commons.model.AuthorizationPaymentConfig;
import com.aeromexico.commons.model.AuthorizationResult;
import com.aeromexico.commons.model.BaggageInfo;
import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.BookedTravelerCollection;
import com.aeromexico.commons.model.BookingClass;
import com.aeromexico.commons.model.CabinCapacity;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.CartPNRConfirmation;
import com.aeromexico.commons.model.CartPNRUpdate;
import com.aeromexico.commons.model.CheckedInTraveler;
import com.aeromexico.commons.model.CheckinConfirmation;
import com.aeromexico.commons.model.CheckinConfirmationBase;
import com.aeromexico.commons.model.CreditCardPaymentRequest;
import com.aeromexico.commons.model.ErrorCauseWrapper;
import com.aeromexico.commons.model.ErrorJsonCheckIn;
import com.aeromexico.commons.model.ExtraInfo;
import com.aeromexico.commons.model.ExtraWeightAncillary;
import com.aeromexico.commons.model.FormsOfPaymentWrapper;
import com.aeromexico.commons.model.IssueBagRP;
import com.aeromexico.commons.model.IssuedBagTagList;
import com.aeromexico.commons.model.IssuedEmds;
import com.aeromexico.commons.model.KioskProfiles;
import com.aeromexico.commons.model.OvsPassengerList;
import com.aeromexico.commons.model.PNR;
import com.aeromexico.commons.model.PNRCollection;
import com.aeromexico.commons.model.PaidSegmentChoice;
import com.aeromexico.commons.model.PassengerDetail;
import com.aeromexico.commons.model.PaymentLinked;
import com.aeromexico.commons.model.PectabBoardingPass;
import com.aeromexico.commons.model.PectabReceipt;
import com.aeromexico.commons.model.PectabReceiptData;
import com.aeromexico.commons.model.PurchaseOrder;
import com.aeromexico.commons.model.SeatChoice;
import com.aeromexico.commons.model.Segment;
import com.aeromexico.commons.model.SegmentChoice;
import com.aeromexico.commons.model.SegmentDocument;
import com.aeromexico.commons.model.ThreeDsFormResponse;
import com.aeromexico.commons.model.UatpPaymentRequest;
import com.aeromexico.commons.model.UpgradeAncillary;
import com.aeromexico.commons.model.VisaCheckoutPaymentRequest;
import com.aeromexico.commons.model.Warning;
import com.aeromexico.commons.model.WarningCollection;
import com.aeromexico.commons.model.chubb.BindPolicyResponse;
import com.aeromexico.commons.model.chubb.Offer;
import com.aeromexico.commons.model.chubb.PriceQuoteResponse;
import com.aeromexico.commons.model.mailing.BigQueryTableEnum;
import com.aeromexico.commons.model.mailing.DocumentTypeEnum;
import com.aeromexico.commons.model.rq.IssueBagRQ;
import com.aeromexico.commons.model.rq.PurchaseOrderRQ;
import com.aeromexico.commons.model.rq.SeatmapCheckInRQ;
import com.aeromexico.commons.model.utils.CurrencyUtil;
import com.aeromexico.commons.model.weather.DailyForecasts;
import com.aeromexico.commons.web.types.AirlineCodeType;
import com.aeromexico.commons.web.types.AuthorizationPaymentResultType;
import com.aeromexico.commons.web.types.CurrencyType;
import com.aeromexico.commons.web.types.ErrorCauseType;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.types.ItemsToBePaidType;
import com.aeromexico.commons.web.types.LanguageType;
import com.aeromexico.commons.web.types.PassengerFareType;
import com.aeromexico.commons.web.types.PosType;
import com.aeromexico.commons.web.util.FilterPassengersUtil;
import com.aeromexico.commons.web.util.TimeUtil;
import com.aeromexico.cybersource.model.CybersourceResponse;
import com.aeromexico.dao.async.SimpleAsync;
import com.aeromexico.dao.client.GenericRestClient;
import com.aeromexico.dao.client.exception.ChubbServiceUnavailableException;
import com.aeromexico.dao.client.exception.MongoDisconnectException;
import com.aeromexico.dao.qualifiers.KioskProfileCache;
import com.aeromexico.dao.qualifiers.OverBookingConfigCache;
import com.aeromexico.dao.services.CheckInServiceDao;
import com.aeromexico.dao.services.IAirportCodesDao;
import com.aeromexico.dao.services.IKioskProfileDao;
import com.aeromexico.dao.services.IOverBookingConfigDao;
import com.aeromexico.dao.services.ShoppingCartDao;
import com.aeromexico.dao.util.CommonUtil;
import com.aeromexico.profile.services.AMSabreProfileService;
import com.aeromexico.sabre.api.acs.AMEditPassCharService;
import com.aeromexico.sabre.api.session.common.ServiceCall;
import com.aeromexico.sabre.api.session.exception.SabreEmptyResponseException;
import com.aeromexico.sabre.api.session.exception.SabreLayerUnavailableException;
import com.aeromexico.sabre.api.sessionpool.factory.SabreSession;
import com.aeromexico.services.ChubbService;
import com.aeromexico.util.ChubbUtil;
import com.aeromexico.weather.service.WeatherService;
import com.aeromexico.webe4.web.common.types.ErrorCodeType;
import com.aeromexico.webe4.web.shoppingcart.model.CarBookRQ;
import com.aeromexico.webe4.web.shoppingcart.model.CarBookResponse;
import com.aeromexico.webe4.web.shoppingcart.model.RateReferenceMongo;
import com.am.checkin.web.util.BoardingPassUtil;
import com.am.checkin.web.util.CommonUtilsCheckIn;
import com.am.checkin.web.util.PNRLookUpServiceUtil;
import com.am.checkin.web.util.PnrCollectionUtil;
import com.am.checkin.web.util.PostCheckinProcess;
import com.am.checkin.web.util.PurchaseOrderUtil;
import com.am.checkin.web.util.TimeMonitorUtil;
import com.am.checkin.web.v2.seamless.service.SeamlessCheckinService;
import com.am.checkin.web.v2.services.AddPaxToPriorityListService;
import com.am.checkin.web.v2.services.CovidRestrictionStorageService;
import com.am.checkin.web.v2.util.BoardingPassesUtil;
import com.am.checkin.web.v2.util.SystemVariablesUtil;
import com.cybersource.schemas.transaction_data_1.RequestMessage;
import com.google.gson.Gson;
import com.sabre.ns.ticketing.dc.GetTicketingDocumentRS;
import com.sabre.services.acs.bso.checkinpassenger.v3.ACSCheckInPassengerRSACS;
import com.sabre.services.acs.bso.flightdetail.v3.ACSFlightDetailRSACS;
import com.sabre.services.acs.bso.seatchange.v3.ACSSeatChangeRSACS;
import com.sabre.services.micellaneous.CollectMiscFeeRQ;
import com.sabre.services.micellaneous.CollectMiscFeeRS;
import com.sabre.webservices.sabrexml._2003._07.SabreCommandLLSRS;
import org.aeromexico.commons.exception.config.ReadProperties;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.aeromexico.commons.model.SeatmapCollection;
import com.aeromexico.commons.model.AbstractSeatMap;
import com.aeromexico.commons.model.SeatMap;
import com.aeromexico.commons.model.SeatSummary;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.aeromexico.commons.model.mailing.MailingConstants.NULL_PNR;
import static com.aeromexico.commons.model.mailing.MailingConstants.NULL_TRANSACTION;

/**
 * @author adrian
 */
@Named
@ApplicationScoped
public class PurchaseOrderService {

    private static final Logger LOG = LoggerFactory.getLogger(PurchaseOrderService.class);

    private static final String _3DS_EMAIL_TRIGGER = "reject@aeromexico.com";

    private static final String AGENT_NAME = "CHECKINAPI";

    private static final Gson GSON = new Gson();

    private static final String SERVICE = "Purchase Order Service";

    private static final ICodesGenerator CODES_GENERATOR = new CodesGenerator();

    @Inject
    private KioskProfileService kioskProfileService;

    @Inject
    private AMEditPassCharService editPassCharService;

    @Inject
    private CheckInService checkInService;

    @Inject
    private SeamlessCheckinService seamlessCheckinService;

    @Inject
    private SeatsService seatsService;

    @Inject
    private PaymentAuthorizationService paymentAuthorizationService;

    @Inject
    private AEService aeService;

    @Inject
    private EMDService emdService;

    @Inject
    private CommandService commandService;

    @Inject
    private PostCheckinProcess postCheckinProcess;

    @Inject
    private PnrCollectionService updatePnrCollectionService;

    @Inject
    private EmailService emailService;

    @Inject
    private IssueBagTagService issueBagTagService;

    @Inject
    private LogOnDatabaseService logOnDatabaseService;

    @Inject
    @KioskProfileCache
    private IKioskProfileDao kioskProfileDao;

    @Inject
    @OverBookingConfigCache
    private IOverBookingConfigDao overBookingConfigDao;

    @Inject
    private PectabReceiptService pectabReceiptService;

    @Inject
    private PassengersListService passengersListService;

    @Inject
    private CheckinCybersourceService cybersourceService;

    @Inject
    private AMSabreProfileService sabreProfileService;

    @Inject
    private UpdateGovService updateGovService;

    @Inject
    private ShoppingCartDao shoppingCartDao;

    @Inject
    private AddPaxToPriorityListService addPaxToPriorityListService;

    @Inject
    private CovidRestrictionStorageService covidRestrictionStorageService;

    @Inject
    private CheckInServiceDao checkInServiceDao;

    @Inject
    private AncillariesService ancillariesService;

    @Inject
    private ChubbService chubbService;

    @Inject
    private SeatMapsServiceSoap seatMapsServiceSoap;

    @Inject
    @Named("CheckInSetUpConfigFactoryWrapper")
    private CheckInSetUpConfigFactoryWrapper setUpConfigFactory;

    @Inject
    private ReadProperties prop;

    @Inject
    private FlightDetailsService flightDetailsService;

    @Inject
    private IAirportCodesDao airportsCodesDao;

    @PostConstruct
    public void init() {
        if (null == setUpConfigFactory) {
            setUpConfigFactory = new CheckInSetUpConfigFactoryWrapper();
        }
    }

    /**
     * @param purchaseOrderRQ
     * @param recordLocator
     * @param cartId
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws GenericException
     */
    private void validateAuthorizationForKiosk(PurchaseOrderRQ purchaseOrderRQ, String recordLocator, String cartId) throws IOException, ClassNotFoundException, GenericException {

    	PurchaseOrder purchaseOrder = purchaseOrderRQ.getPurchaseOrder();

        if (null == purchaseOrder.getAuthorizationResult()) {
            LOG.error(LogUtil.getLogError(SERVICE, "AUTHORIZATION PAYMENT IS REQUIRED", recordLocator, cartId));
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.AUTHORIZATION_PAYMENT_IS_REQUIRED, ErrorType.AUTHORIZATION_PAYMENT_IS_REQUIRED.getFullDescription());
        }

        if (!AuthorizationPaymentResultType.APPROVED.toString().equalsIgnoreCase(purchaseOrder.getAuthorizationResult().getResponseCode())) {
            LOG.error(LogUtil.getLogError(SERVICE, "AUTHORIZATION PAYMENT NOT APPROVED", recordLocator, cartId));
            if (PosType.WEB.toString().equalsIgnoreCase(purchaseOrderRQ.getPos())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.AUTHORIZATION_PAYMENT_NOT_APPROVED_WEB, ErrorType.AUTHORIZATION_PAYMENT_NOT_APPROVED_WEB.getFullDescription());
            } else {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.AUTHORIZATION_PAYMENT_NOT_APPROVED, ErrorType.AUTHORIZATION_PAYMENT_NOT_APPROVED.getFullDescription());
            }
        }

        if (null == purchaseOrder.getAuthorizationResult().getApprovalCode()) {
            LOG.error(LogUtil.getLogError(SERVICE, "MISSING APPROVAL CODE", recordLocator, cartId));
            if (PosType.WEB.toString().equalsIgnoreCase(purchaseOrderRQ.getPos())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.AUTHORIZATION_PAYMENT_NOT_APPROVED_WEB, ErrorType.AUTHORIZATION_PAYMENT_NOT_APPROVED_WEB.getFullDescription());
            } else {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.AUTHORIZATION_PAYMENT_NOT_APPROVED, ErrorType.AUTHORIZATION_PAYMENT_NOT_APPROVED.getFullDescription());
            }
        }
    }

    /**
     * @param authorizationResultList
     * @param purchaseOrderRQ
     * @param issuedEmds
     * @param recordLocator
     * @param cartId
     * @param currencyCode
     * @return
     */
    private PectabReceipt getPectabReceipt(
            List<AuthorizationResult> authorizationResultList,
            PurchaseOrderRQ purchaseOrderRQ,
            IssuedEmds issuedEmds,
            String recordLocator,
            String cartId,
            String currencyCode
    ) {

        if (null == authorizationResultList || authorizationResultList.isEmpty()) {
            return null;
        }

        AuthorizationResult authorizationResult = authorizationResultList.get(0);

        if (null != authorizationResult) {
            try {

                LanguageType languageType;
                if (LanguageType.ES.toString().equalsIgnoreCase(purchaseOrderRQ.getLanguage())) {
                    languageType = LanguageType.ES;
                } else {
                    languageType = LanguageType.EN;
                }

                PectabReceiptData pectabReceiptData;
                pectabReceiptData = PectabReceiptService.getPectabReceiptData(
                        purchaseOrderRQ.getPurchaseOrder(),
                        authorizationResult,
                        issuedEmds,
                        languageType,
                        recordLocator,
                        cartId,
                        purchaseOrderRQ.getPos()
                );

                PectabReceipt pectabReceipt;
                pectabReceipt = pectabReceiptService.getPectabReceipt(pectabReceiptData, languageType, currencyCode);
                pectabReceipt.setPaymentId(authorizationResult.getPaymentId());

                return pectabReceipt;

            } catch (IOException | ClassNotFoundException ex) {
                //ignore
                LogUtil.getLogError(SERVICE, "Error Forming Pectab Receipt", recordLocator, cartId, ex);
                return null;
            }
        }

        return null;
    }

    /**
     * @param headers
     * @return
     */
    private String getRealIP(HttpHeaders headers) {
        String ipAddr;
        try {
            ipAddr = headers.getHeaderString("X-Forwarded-For");
            if (null == ipAddr || ipAddr.trim().isEmpty()) {
                ipAddr = "";
            } else {
                LOG.info("real IP: " + ipAddr);
                ipAddr = ipAddr.replaceAll("localhost", "127.0.0.1");

                if (ipAddr.contains(",")) {
                    ipAddr = (ipAddr.split(","))[0];
                }

                if (ipAddr.contains(":")) {
                    ipAddr = (ipAddr.split(":"))[0];
                }
            }
        } catch (Exception ex) {
            ipAddr = "";
        }
        return ipAddr;
    }

    /**
     * @param creditCardPaymentRequestList
     * @param address
     * @param purchaseOrderRQ
     * @param bookedTravelersRequested
     * @param bookedLegList
     * @param toPaidItemsType
     * @param recordLocator
     * @param creationDate
     * @param retryAttempt
     * @param maxRetries
     * @param fingerprintSessionId
     * @param errorCauseWrapper
     * @return
     * @throws Exception
     */
    private List<CybersourceResponse> callCybersource(
            List<CreditCardPaymentRequest> creditCardPaymentRequestList,
            Address address,
            PurchaseOrderRQ purchaseOrderRQ,
            List<BookedTraveler> bookedTravelersRequested,
            List<BookedLeg> bookedLegList,
            ItemsToBePaidType toPaidItemsType,
            String recordLocator,
            String creationDate,
            int retryAttempt,
            int maxRetries,
            String fingerprintSessionId,
            ErrorCauseWrapper errorCauseWrapper,
            String kioskID
    ) throws Exception {
        List<CybersourceResponse> preTicketResponseList = null;

        PurchaseOrder purchaseOrder = purchaseOrderRQ.getPurchaseOrder();
        String store = purchaseOrderRQ.getStore();
        String pos = purchaseOrderRQ.getPos();
        String transactionTime = TimeUtil.getStrTime(purchaseOrderRQ.getTransactionTime());
        String cartId = purchaseOrder.getCartId();
        String ipAddr = getRealIP(purchaseOrderRQ.getHeaders());

        List<RequestMessage> requestMessageList = cybersourceService.getCybersourceRQ(
                creditCardPaymentRequestList,
                address,
                purchaseOrderRQ,
                bookedTravelersRequested,
                bookedLegList,
                toPaidItemsType,
                fingerprintSessionId,
                ipAddr,
                recordLocator,
                creationDate,
                cartId,
                store,
                pos,
                transactionTime,
                kioskID
        );

        if (null == requestMessageList || requestMessageList.isEmpty()) {
            GenericException ex = new GenericException(
                    Response.Status.BAD_REQUEST,
                    ErrorType.ERROR_DOING_CHECKIN, "Error forming request to cybersource"
            );
            ex.setWasLogged(true);
            //logOnDatabaseService.logPreCheckinOnError(purchaseOrderRQ, ex);
            throw ex;
        }

        int retryCallOnInternalError = 0;
        boolean valid = false;

        while (!valid) {
            try {
                preTicketResponseList = cybersourceService.preTicketCall(
                        requestMessageList, cartId
                );

                valid = true;
            } catch (CybersourceException ex) {
                LOG.error("Error al realizar el primer llamado a cybersource");
                LOG.error("Error: {}", ex);
                LOG.error("PurchaseOrderService", cartId, "callCyberSource", ex.getErrorMessage());
                LOG.error("PurchaseOrderService", cartId, "Request Service", ex.getErrorCode() + ex.getMessage());

                if (retryAttempt >= maxRetries/* Limit o retries */) {
                    break;
                } else {
                    errorCauseWrapper.setErrorCauseType(ErrorCauseType.CYBERSOURCE);
                    throw new PaymentRetryException(Response.Status.BAD_REQUEST.getStatusCode(), ex.getMessage(),
                            ErrorCodeType.PAYMENT_RETRY.getErrorCode());
                }
            } catch (CybersourceInternalErrorException ce) {
                retryCallOnInternalError++;
                if (retryCallOnInternalError > maxRetries) {
                    LOG.error("Error al realizar el llamado a cybersource", ce);

                    if (retryAttempt >= maxRetries/* Limit o retries */) {
                        break;
                    } else {
                        errorCauseWrapper.setErrorCauseType(ErrorCauseType.CYBERSOURCE);
                        throw new PaymentRetryException(Response.Status.BAD_REQUEST.getStatusCode(), ce.getMessage(),
                                ErrorCodeType.PAYMENT_RETRY.getErrorCode());
                    }
                }
            } catch (Throwable t) {
                retryCallOnInternalError++;
                if (retryCallOnInternalError > maxRetries) {
                    LOG.error("Error: {}", t);
                    LOG.error("PurchaseOrderService", cartId, "callCyberSource", t.getMessage());
                    LOG.error("PurchaseOrderService", cartId, "Request Service", t.getLocalizedMessage());

                    if (retryAttempt >= maxRetries/* Limit o retries */) {
                        break;
                    } else {
                        errorCauseWrapper.setErrorCauseType(ErrorCauseType.CYBERSOURCE);
                        throw new PaymentRetryException(Response.Status.BAD_REQUEST.getStatusCode(), t.getMessage(),
                                ErrorCodeType.PAYMENT_RETRY.getErrorCode());
                    }
                }
            }
        }

        return preTicketResponseList;
    }

    public void loadProfileFormOfPayments(FormsOfPaymentWrapper formsOfPaymentWrapper, String pos) throws GenericException {

        for (CreditCardPaymentRequest creditCardPaymentRequest : formsOfPaymentWrapper.getCreditCardPaymentRequestList()) {
            int ccNumberLenght = creditCardPaymentRequest.getCcNumber().length();
            if (null != creditCardPaymentRequest.getCardId()
                    && 0 != creditCardPaymentRequest.getCardId()
                    && ccNumberLenght > 16) {
                try {
                    creditCardPaymentRequest.setCcNumber(sabreProfileService.decryptData(creditCardPaymentRequest.getCcNumber()));
                } catch (MongoDisconnectException me) {
                    if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(pos)) {
                        LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
                        throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getCode(),
                                prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getActionable(),
                                prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getMessage(),
                                Response.Status.INTERNAL_SERVER_ERROR);
                    } else {
                        LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
                        throw me;
                    }
                } catch (Exception ex) {
                    LOG.error("Error ", ex);
                    throw ex;
                }
            }
        }

        for (UatpPaymentRequest uatpPaymentRequest : formsOfPaymentWrapper.getUatpPaymentRequestList()) {
            int uatpNumberLenght = uatpPaymentRequest.getCardCode().length();

            if (null != uatpPaymentRequest.getCardId()
                    && 0 != uatpPaymentRequest.getCardId()
                    && uatpNumberLenght > 15) {
                try {
                    uatpPaymentRequest.setCardCode(sabreProfileService.decryptData(uatpPaymentRequest.getCardCode()));
                } catch (Exception ex) {
                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.USER_PROFILE_NOT_FOUND_IN_DB, PosType.getType(pos));
                }
            }
        }
    }

    public void createBoardingPasses(
            PNRCollection pnrCollection,
            String recordLocator,
            String cartId,
            String pos
    ) {
        try {

            BoardingPassesUtil.setPriorityBoarding(pnrCollection, recordLocator);

            emailService.createBoardingPass(pnrCollection);
            LOG.info("SEND BOARDING PASS REQUEST");
        } catch (Exception e) {
            LOG.info("BOARDING PASS NOT CREATED: " + e.getMessage());
            LOG.error("Cannot create files for email {}", e);

            if (SystemVariablesUtil.isBigQueryEnabled()) {

                try {
                    BigQueryService.notReceivedNotification(
                            recordLocator != null ? recordLocator : NULL_PNR,
                            cartId != null ? cartId : NULL_TRANSACTION,
                            "",
                            System.currentTimeMillis(),
                            DocumentTypeEnum.getDocumentTypeEnum(
                                    pos != null
                                            ? pos : PosType.WEB.toString(),
                                    DocumentTypeEnum.BOARDING_PASS).toString(),
                            BigQueryTableEnum.DOCUMENT_CREATION.toString(),
                            false,
                            e.getMessage(),
                            emailService.getEnvironment());
                } catch (Exception ex) {
                    LOG.error("ERROR TO SEND NOTIFCATION BIG QUERY: {}", ex);
                }
            }
        }
    }

    private Map<String, String> callChubbPolicy(PNR pnr, PurchaseOrderRQ purchaseOrderRQ, String cartId) {

        Map<String, String> mappedPolicyChubb = null;

        CartPNR cartPNR = pnr.getCartPNRByCartId(cartId);

        if (ChubbUtil.hasChubbAncillaries(cartPNR.getTravelerInfo())
                &&
                ChubbUtil.isCandidateForChubb(
                        purchaseOrderRQ.getStore(),
                        purchaseOrderRQ.getPos(),
                        pnr.getFlightItineraryType()
                )) {
            try {

                AuthorizationPaymentConfig authorizationPaymentConfig = setUpConfigFactory
                        .getInstance()
                        .getAuthorizationPaymentConfig(
                                purchaseOrderRQ.getStore(),
                                purchaseOrderRQ.getPos()
                        );

                Address billingAddress = ChubbUtil.getAddress(purchaseOrderRQ.getPurchaseOrder().getPaymentInfo());

                String billingEmail = ChubbUtil.getEmail(purchaseOrderRQ.getPurchaseOrder().getPaymentInfo());

                PriceQuoteResponse priceQuoteResponse = ancillariesService.getPriceQuoteResponse(
                        cartPNR.getTravelerInfo(),
                        pnr.getLegs(),
                        purchaseOrderRQ.getStore(),
                        authorizationPaymentConfig.getCurrency(),
                        pnr.getPnr(),
                        authorizationPaymentConfig.getStationNumber(),
                        cartId,
                        true
                );

                if (priceQuoteResponse != null) {
                    mappedPolicyChubb = new HashMap<>();

                    for (Offer offer : priceQuoteResponse.getOffers()) {

                        BookedTraveler travelerHolder = ChubbUtil.getBookedTraveler("01.01", cartPNR.getTravelerInfo().getCollection());

                        List<BookedTraveler> bookedTravelerList = ChubbUtil.getChubbTravelers(
                                offer.getExternal().getCode(),
                                cartPNR.getTravelerInfo()
                        );

                        if (null != bookedTravelerList && !bookedTravelerList.isEmpty()) {
                            String sendPolicy = "Bind Policy Request is null";

                            try {
                                BindPolicyResponse bindPolicyResponse = chubbService.callBindPolicy(
                                        pnr.getLegs(),
                                        bookedTravelerList,
                                        travelerHolder,
                                        offer,
                                        billingAddress,
                                        billingEmail,
                                        purchaseOrderRQ.getStore(),
                                        pnr.getPnr(),
                                        authorizationPaymentConfig.getStationNumber(),
                                        SystemVariablesUtil.getChubbAgentName(),
                                        SystemVariablesUtil.getChubbAgentCode(),
                                        SystemVariablesUtil.getChubbAgentInternalCode()
                                );

                                sendPolicy = "Success";
                            } catch (ChubbServiceUnavailableException csu) {
                                sendPolicy = csu.getMessage();
                                LOG.error(ChubbUtil.buildLogChubb(csu, this.getClass().getName()));
                            } catch (Exception ex) {
                                sendPolicy = ex.getMessage();
                                LOG.error(ChubbUtil.buildLogChubbBind(null, ex.getMessage()));
                            }
                            mappedPolicyChubb.put(offer.getExternal().getCode(), sendPolicy);
                        } else {
                            LOG.info("Chubb {} was not purchased", offer.getExternal().getCode());
                        }
                    }
                } else {
                    LOG.error("ERROR_CHUBB_POST_BIND {}, priceQuoteResponse null ", cartPNR.getMeta().getCartId());
                }
            } catch (Exception ex) {
                LOG.error(ex.getMessage(), ex);
            }
        } else {
            LOG.info("Chubb is not candidate");
        }

        return mappedPolicyChubb;
    }

    /**
     * @param purchaseOrderRQ
     * @param pnrCollection
     * @param pnrCollectionAfterCheckin
     * @param bookedTravelersEligibleForCheckin
     * @param serviceCallList
     * @return
     * @throws Exception
     */
    private CheckinConfirmationBase doPurchasePreCheckin(
            PurchaseOrderRQ purchaseOrderRQ,
            PNRCollection pnrCollection,
            PNRCollection pnrCollectionAfterCheckin,
            List<BookedTraveler> bookedTravelersEligibleForCheckin,
            List<ServiceCall> serviceCallList
    ) throws Exception {

        long startTime;
        ErrorCauseWrapper errorCauseWrapper = new ErrorCauseWrapper();
        PurchaseOrder purchaseOrder = purchaseOrderRQ.getPurchaseOrder();
        String cartId = purchaseOrder.getCartId();
        String store = purchaseOrderRQ.getStore();
        String pos = purchaseOrderRQ.getPos();
        PNR pnr = pnrCollection.getPNRByCartId(cartId);
        boolean checkOverbooking = (null != pnr.getOverBooking() && PosType.KIOSK.toString().equalsIgnoreCase(purchaseOrderRQ.getPos()));
        boolean volunteerOffer = pnr.isVolunteerOffer();

        String recordLocator = pnr.getPnr();
        CartPNR cartPNR = pnr.getCartPNRByCartId(cartId);
        String legCode = cartPNR.getLegCode();
        BookedLeg bookedLeg = pnr.getBookedLegByLegCode(legCode);
        CartPNR cartPNRAfterCheckin;
        cartPNRAfterCheckin = pnrCollectionAfterCheckin.getCartPNRByRecordLocatorAndLegCode(recordLocator, legCode);

        //get the total present in cart
        BigDecimal totalInPurchase = BigDecimal.ZERO;
        BigDecimal totalInCart;
        totalInCart = cartPNR.getMeta().getTotal().getCurrency().getTotal();
        if (null == totalInCart) {
            totalInCart = BigDecimal.ZERO;
        }

        purchaseOrder.setPaymentAmount(cartPNR.getMeta().getTotal().getCurrency());

        String currencyCode = cartPNR.getMeta().getTotal().getCurrency().getCurrencyCode();
        if (null == currencyCode) {

        }

        //PurchaseOrderUtil.checkMissingProperties(bookedTravelersRequested);
        boolean areThereUnpaidItems = PurchaseOrderUtil.areThereUnpaidItems(bookedTravelersEligibleForCheckin);
        boolean areThereWaiveItems = PurchaseOrderUtil.areThereWaiveItems(bookedTravelersEligibleForCheckin);

        boolean isTherePaymentMethod = (null != purchaseOrder.getPaymentInfo()
                && null != purchaseOrder.getPaymentInfo().getCollection()
                && !purchaseOrder.getPaymentInfo().getCollection().isEmpty());

        if (areThereUnpaidItems && !isTherePaymentMethod) {
            LOG.error(LogUtil.getLogError(SERVICE, " There are unpaid items, you need to specify a payment method", recordLocator, cartId));
            GenericException ex = new GenericException(Response.Status.BAD_REQUEST, ErrorType.PAYMENT_METHOD_IS_REQUIRED, "There are unpaid items, you need to specify a payment method");
            ex.setWasLogged(true);
            logOnDatabaseService.logPreCheckinOnError(purchaseOrderRQ, cartId, recordLocator, ex);
            throw ex;
        }

        FormsOfPaymentWrapper formsOfPaymentWrapper;
        formsOfPaymentWrapper = new FormsOfPaymentWrapper(
                purchaseOrder.getPaymentInfo()
        );

        if (areThereUnpaidItems) {
            //valid forms of payment on checkin are credit card and uatp card and visa checkout
            boolean isThereValidPaymentMethod = !formsOfPaymentWrapper.getCreditCardPaymentRequestList().isEmpty()
                    || !formsOfPaymentWrapper.getUatpPaymentRequestList().isEmpty()
                    || !formsOfPaymentWrapper.getVisaCheckoutPaymentRequestList().isEmpty();

            if (!isThereValidPaymentMethod) {
                LOG.error(LogUtil.getLogError(SERVICE, " There are unpaid items, you need to specify a valid payment method", recordLocator, cartId));
                GenericException ex = new GenericException(Response.Status.BAD_REQUEST, ErrorType.PAYMENT_METHOD_IS_REQUIRED, "There are unpaid items, you need to specify a valid payment method");
                ex.setWasLogged(true);
                logOnDatabaseService.logPreCheckinOnError(purchaseOrderRQ, cartId, recordLocator, ex);
                throw ex;
            }

            //compare the total present in the payment info request
            totalInPurchase = PurchaseOrderUtil.getTotal(formsOfPaymentWrapper, totalInCart);

            if (0 != totalInCart.compareTo(totalInPurchase)) {
                LOG.error(LogUtil.getLogError(SERVICE, "Prices on cart and purchase order does not match", recordLocator, cartId));
                GenericException ex = new GenericException(Response.Status.BAD_REQUEST, ErrorType.PRICES_NOT_MATCH, ErrorType.PRICES_NOT_MATCH.getFullDescription());
                ex.setWasLogged(true);
                logOnDatabaseService.logPreCheckinOnError(purchaseOrderRQ, cartId, recordLocator, ex);
                throw ex;
            }
        }

        try {
            WarningCollection warningCollection = new WarningCollection();

            boolean paymentWasIssued = false;

            try {
                LOG.info(LogUtil.getLogInfo(SERVICE, recordLocator, cartId, "Removing zero cost ancillaries"));
                //remove unpaid ancillaries when quantity is zero
                aeService.removeZeroQuantityAncillariesFromReservation(
                        bookedTravelersEligibleForCheckin,
                        recordLocator, cartId,
                        serviceCallList
                );
            } catch (Exception ex) {
                //
                LOG.error(LogUtil.getLogError(SERVICE, "Removing zero cost ancillaries", recordLocator, cartId, ex));
            }

            PnrCollectionUtil.copyTouchedTravelers(
                    cartPNR.getTravelerInfo().getCollection(),
                    cartPNRAfterCheckin.getTravelerInfo().getCollection()
            );

            try {
                LOG.info(LogUtil.getLogInfo(SERVICE, recordLocator, cartId, "Adding unreserved seat"));
                //add zero ancillaries for not pre-reserved seats
                aeService.addAncillariesToReservationForUnreservedSeat(
                        bookedTravelersEligibleForCheckin,
                        bookedLeg,
                        currencyCode,
                        recordLocator,
                        cartId,
                        store,
                        serviceCallList,
                        purchaseOrderRQ.getPos()
                );
            } catch (Exception ex) {
                //
                LOG.error(LogUtil.getLogError(SERVICE, "Adding unreserved seat", recordLocator, cartId, ex));
            }

            PnrCollectionUtil.copyTouchedTravelers(
                    cartPNR.getTravelerInfo().getCollection(),
                    cartPNRAfterCheckin.getTravelerInfo().getCollection()
            );

            if (areThereUnpaidItems && 0 < totalInPurchase.compareTo(BigDecimal.ZERO)) {
                //call authorization payment only if the request comes from KIOSK

                List<AuthorizationResult> authorizationResultList = new ArrayList<>();

                if (PosType.KIOSK.toString().equalsIgnoreCase(purchaseOrderRQ.getPos())
                        && !purchaseOrder.isPinPadNotPresent()) {
                    validateAuthorizationForKiosk(purchaseOrderRQ, recordLocator, cartId);

                    authorizationResultList.add(purchaseOrder.getAuthorizationResult());

                    cartPNR.setPectabReceiptList(cartPNRAfterCheckin.getPectabReceiptList());
                    cartPNR.setPaymentLinkedList(cartPNRAfterCheckin.getPaymentLinkedList());

                    PnrCollectionUtil.setDefaultCardInfo(purchaseOrder.getPaymentInfo());
                } else if (purchaseOrder.isThreeDsPayment() && null != purchaseOrder.getAuthorizationResult()) {
                    validateAuthorizationForKiosk(purchaseOrderRQ, recordLocator, cartId);

                    authorizationResultList.add(purchaseOrder.getAuthorizationResult());
                } else {
                    if (PosType.KIOSK.toString().equalsIgnoreCase(purchaseOrderRQ.getPos())
                            && purchaseOrder.isPinPadNotPresent()) {

                        if (!formsOfPaymentWrapper.getCreditCardPaymentRequestList().isEmpty()) {
                            int maxRetries = 1;// 0 and 1 (two retries)
                            int retryAttempt = (null == purchaseOrder.getRetryCount()) ? 0 : purchaseOrder.getRetryCount();

                            Address address = getAddresFromKioskId(purchaseOrderRQ, recordLocator, cartId);

                            try {

                                this.callCybersource(
                                        formsOfPaymentWrapper.getCreditCardPaymentRequestList(),
                                        address,
                                        purchaseOrderRQ,
                                        bookedTravelersEligibleForCheckin,
                                        pnr.getLegs().getCollection(),
                                        ItemsToBePaidType.SEAT_AND_BAGGAGE,
                                        recordLocator,
                                        TimeUtil.getStrTime(purchaseOrderRQ.getTransactionTime(), "yyyy-MM-dd'T'HH:mm:ss"),
                                        retryAttempt,
                                        maxRetries,
                                        currencyCode,
                                        errorCauseWrapper,
                                        purchaseOrderRQ.getKioskId()
                                );

                                purchaseOrder.setThreeDsPayment(false);
                                purchaseOrder.setThreeDSRedirectURL(null);

                            } catch (Exception ex) {
                                LOG.error(ex.getMessage(), ex);

                                if (null != purchaseOrder.getThreeDSRedirectURL()) {
                                    for (CreditCardPaymentRequest creditCardPaymentRequest : formsOfPaymentWrapper.getCreditCardPaymentRequestList()) {
                                        creditCardPaymentRequest.setEmail(_3DS_EMAIL_TRIGGER);
                                    }
                                }

                            }
                        }
                    }

                    //form request for payment
                    /**
                     * TravelerAncillaryList is a collection to store the
                     * ancillaries added to this payment request
                     *
                     */
                    List<AbstractAncillary> abstractAncillaryList = new ArrayList<>();

                    authorizationResultList = paymentAuthorizationService.callAuthorization(
                            bookedTravelersEligibleForCheckin,
                            bookedLeg,
                            purchaseOrderRQ,
                            abstractAncillaryList,
                            ItemsToBePaidType.SEAT_AND_BAGGAGE,
                            recordLocator,
                            cartId,
                            serviceCallList
                    );


                    for (AuthorizationResult authorizationResult : authorizationResultList) {

                        if (null != authorizationResult.getT3dsResult() && null != authorizationResult.getRedirectForm()) {
                            ThreeDsFormResponse threeDsFormResponse = new ThreeDsFormResponse();
                            threeDsFormResponse.setRedirectForm(authorizationResult.getRedirectForm());
                            threeDsFormResponse.setT3dsResult(authorizationResult.getT3dsResult());

                            return threeDsFormResponse;
                        }

                        if (AuthorizationPaymentResultType.APPROVED.toString().equalsIgnoreCase(authorizationResult.getResponseCode())) {
                            PaymentLinked paymentLinked;
                            paymentLinked = PnrCollectionUtil.addPaymentId(
                                    bookedTravelersEligibleForCheckin, abstractAncillaryList,
                                    authorizationResult, purchaseOrderRQ.getTransactionTime()
                            );
                            cartPNR.getPaymentLinkedList().addIfNotExist(paymentLinked);
                            cartPNRAfterCheckin.getPaymentLinkedList().addIfNotExist(paymentLinked);
                        } else {
                            if (PosType.WEB.toString().equalsIgnoreCase(purchaseOrderRQ.getPos())) {
                                LOG.error(LogUtil.getLogError(SERVICE, "AUTHORIZATION PAYMENT NOT APPROVED", recordLocator, cartId));
                                GenericException ex = new GenericException(Response.Status.BAD_REQUEST, ErrorType.AUTHORIZATION_PAYMENT_NOT_APPROVED_WEB, ErrorType.AUTHORIZATION_PAYMENT_NOT_APPROVED_WEB.getFullDescription());
                                ex.setWasLogged(true);
                                logOnDatabaseService.logPreCheckinOnError(purchaseOrderRQ, cartId, recordLocator, ex);
                            } else {
                                LOG.error(LogUtil.getLogError(SERVICE, "AUTHORIZATION PAYMENT NOT APPROVED", recordLocator, cartId));
                                GenericException ex = new GenericException(Response.Status.BAD_REQUEST, ErrorType.AUTHORIZATION_PAYMENT_NOT_APPROVED, ErrorType.AUTHORIZATION_PAYMENT_NOT_APPROVED.getFullDescription());
                                ex.setWasLogged(true);
                                logOnDatabaseService.logPreCheckinOnError(purchaseOrderRQ, cartId, recordLocator, ex);
                            }
                        }

                        aeService.addRemark(authorizationResult, recordLocator, cartId, serviceCallList);
                    }
                }

                //add new ancillaries in shopping cart to reservation
                aeService.addNewAncillariesToReservation(
                        purchaseOrderRQ,
                        bookedTravelersEligibleForCheckin,
                        bookedLeg,
                        pnr.getGlobalMarketFlight(),
                        recordLocator,
                        cartId,
                        store,
                        serviceCallList
                );

                PnrCollectionUtil.copyTouchedTravelers(
                        cartPNR.getTravelerInfo().getCollection(),
                        cartPNRAfterCheckin.getTravelerInfo().getCollection()
                );

                //LOG.debug(LogUtil.getLogDebug(SERVICE, recordLocator, cartId, "Pnr Collection After Update Reservation\n" + GSON.toJson(pnrCollection) + "\n"));
                //form request for emd
                GetTicketingDocumentRS ticketingDocumentRS = null;
                CollectMiscFeeRQ collectMiscFeeRQ = emdService.getCollectMiscFeeRQ(
                        bookedTravelersEligibleForCheckin,
                        bookedLeg,
                        purchaseOrderRQ,
                        authorizationResultList,
                        ticketingDocumentRS,
                        recordLocator,
                        cartId,
                        serviceCallList
                );

                //LOG.debug(LogUtil.getLogDebug(SERVICE, recordLocator, cartId, "Collect Misc Fee RQ\n" + GSON.toJson(collectMiscFeeRQ) + "\n"));
                if (null != collectMiscFeeRQ
                        && null != collectMiscFeeRQ.getFees()
                        && !collectMiscFeeRQ.getFees().isEmpty()) {

                    boolean checkForRetry;
                    CollectMiscFeeRS collectMiscFeeRS = null;
                    try {
                        checkForRetry = true;

                        //call issue emds
                        collectMiscFeeRS = emdService.issueEmdCodes(
                                collectMiscFeeRQ, purchaseOrderRQ,
                                "Collect Misc Fee", checkForRetry,
                                serviceCallList
                        );
                    } catch (RetryCheckinException ex) {
                        checkForRetry = false;
                        boolean retry = true;

                        if (ErrorType.INVALID_COUPON_NUMBER.equals(ex.getErrorCodeType())
                                || ErrorType.INVALID_COUPON_STATUS.equals(ex.getErrorCodeType())) {

                            LOG.info("INVALID COUPON NUMBER/STATUS: changing coupon");
                            retry = emdService.changeCouponNumber(
                                    collectMiscFeeRQ, ticketingDocumentRS,
                                    recordLocator, cartId, serviceCallList,
                                    purchaseOrderRQ.getPos()
                            );
                        }

                        if (retry) //call issue emds
                        {
                            collectMiscFeeRS = emdService.issueEmdCodes(
                                    collectMiscFeeRQ, purchaseOrderRQ,
                                    "Collect Misc Fee", checkForRetry,
                                    serviceCallList
                            );
                        }
                    }

                    paymentWasIssued = true;

                    //ref to get emds
                    int groupEmdsBy = 3;
                    IssuedEmds issuedEmds = new IssuedEmds(groupEmdsBy);
                    if (null != collectMiscFeeRS && null != collectMiscFeeRS.getFees()) {
                        PnrCollectionUtil.changeUnpaidAncillariesToPaid(
                                bookedTravelersEligibleForCheckin, collectMiscFeeRS.getFees(), issuedEmds
                        );
                    }

                    PnrCollectionUtil.copyTouchedTravelers(
                            cartPNR.getTravelerInfo().getCollection(),
                            cartPNRAfterCheckin.getTravelerInfo().getCollection()
                    );

                    LOG.info(LogUtil.getLogDebug(SERVICE, recordLocator, cartId, "Issued emds: " + issuedEmds.toString()));
                    PectabReceipt pectabReceipt;
                    pectabReceipt = getPectabReceipt(
                            authorizationResultList,
                            purchaseOrderRQ,
                            issuedEmds,
                            recordLocator,
                            cartId,
                            currencyCode
                    );
                    if (null != pectabReceipt) {
                        cartPNR.getPectabReceiptList().addIfNotExist(pectabReceipt);
                        cartPNRAfterCheckin.getPectabReceiptList().addIfNotExist(pectabReceipt);
                    } else {
                        LOG.error("No receipt was generated");
                    }

                    LOG.debug(LogUtil.getLogDebug(SERVICE, recordLocator, cartId, "Pnr Collection After Issue EMDs\n" + GSON.toJson(pnrCollection) + "\n"));
                    LOG.debug(LogUtil.getLogDebug(SERVICE, recordLocator, cartId, "Pnr Collection After Checkin After Issue EMDs\n" + GSON.toJson(pnrCollectionAfterCheckin) + "\n"));
                } else {
                    LOG.error(LogUtil.getLogError(SERVICE, "There was not fees (possible cause action code different that HD)\n", recordLocator, cartId));
                    GenericException ex = new GenericException(Response.Status.BAD_REQUEST, ErrorType.NO_UNFULFILLED_AE_ITEM);
                    ex.setWasLogged(true);
                    logOnDatabaseService.logPreCheckinOnError(purchaseOrderRQ, cartId, recordLocator, ex);
                    throw ex;
                }

            } else if (areThereWaiveItems) {
                //add new ancillaries in shopping cart to reservation
                aeService.addNewAncillariesToReservation(
                        purchaseOrderRQ,
                        bookedTravelersEligibleForCheckin,
                        bookedLeg,
                        pnr.getGlobalMarketFlight(),
                        recordLocator,
                        cartId,
                        store,
                        serviceCallList
                );

                PnrCollectionUtil.copyTouchedTravelers(
                        cartPNR.getTravelerInfo().getCollection(),
                        cartPNRAfterCheckin.getTravelerInfo().getCollection()
                );
            } else if (areThereUnpaidItems) {
                LOG.error(LogUtil.getLogError(SERVICE, "There are unpaid items, you need to specify a payment method with an amount", recordLocator, cartId));
                GenericException ex = new GenericException(Response.Status.BAD_REQUEST, ErrorType.PAYMENT_METHOD_IS_REQUIRED, "There are unpaid items, you need to specify a payment method with an amount");
                ex.setWasLogged(true);
                logOnDatabaseService.logPreCheckinOnError(purchaseOrderRQ, cartId, recordLocator, ex);
                throw ex;
            } else {//nothing to be paid
                LOG.info(LogUtil.getLogInfo(SERVICE, recordLocator, cartId, "Nothing to be paid"));
            }

            //the requested cart is always updated in this step,
            //the other carts are updated only if the checkin is done for all flights
            cartPNR.setTouched(true);

            //reference parameter
            Set<BookedTraveler> passengersSuccessfulCheckedIn = new HashSet<>();
            List<ACSCheckInPassengerRSACS> acsCheckInPassengerRSACSList = null;

            bookedTravelersEligibleForCheckin = FilterPassengersUtil.getEligibleForCheckin(bookedTravelersEligibleForCheckin);

            boolean volunteerQualifier = false;
            boolean checkinSuccessful = false;

            if (!bookedTravelersEligibleForCheckin.isEmpty()) {

                updateGovService.updateGov(bookedTravelersEligibleForCheckin, bookedLeg);

                if (paymentWasIssued) {
                    try {
                        acsCheckInPassengerRSACSList = checkInService.doCheckin(
                                purchaseOrderRQ,
                                bookedTravelersEligibleForCheckin,
                                passengersSuccessfulCheckedIn,
                                volunteerOffer,
                                warningCollection,
                                purchaseOrderRQ.getPos(),
                                purchaseOrderRQ.getLanguage(),
                                bookedLeg,
                                recordLocator,
                                cartId
                        );

                        checkinSuccessful = true;

                        for(ACSCheckInPassengerRSACS acsCheckInPassengerRSACSpax : acsCheckInPassengerRSACSList ){
                            String flight = acsCheckInPassengerRSACSpax.getItineraryPassengerList().getItineraryPassenger().get(0).getItineraryDetail().getAirline()+acsCheckInPassengerRSACSpax.getItineraryPassengerList().getItineraryPassenger().get(0).getItineraryDetail().getFlight();
                            String passengerId = acsCheckInPassengerRSACSpax.getItineraryPassengerList().getItineraryPassenger().get(0).getPassengerDetailList().getPassengerDetail().get(0).getPassengerID();
                            LOG.info("CHECKIN_WAS: SUCCESSFUL PNR: {} FLIGHT: {} PASSENGER_ID: {} CHANNEL: {}",recordLocator,flight,passengerId,pos);
                        }

                    } catch (GenericException ex) {
                        if (ErrorType.CANADA_CLEARANCE_REQD_FOR_PASSENGER.equals(ex.getErrorCodeType())) {
                            Warning warning = new Warning(ErrorType.VERIFY_CANADIAN_VISA_OR_PSGR_CONTINUE.getErrorCode(), ErrorType.VERIFY_CANADIAN_VISA_OR_PSGR_CONTINUE.getFullDescription() + ": " + ex.getMessage());
                            warningCollection.addToList(warning);
                        } else if (ErrorType.UK_BORDER_CLEARANCE_REQD_FOR_PSGR.equals(ex.getErrorCodeType())) {
                            Warning warning = new Warning(ErrorType.UK_BORDER_CLEARANCE_REQD_FOR_PSGR_CONTINUE.getErrorCode(), ErrorType.AUTHORIZATION_PAYMENT_NOT_APPROVED_WEB.getFullDescription() + ": " + ex.getMessage());
                            warningCollection.addToList(warning);
                        } else {
                            Warning warning = new Warning(ErrorType.ERROR_DOING_CHECKIN.getErrorCode(), ErrorType.ERROR_DOING_CHECKIN.getFullDescription() + ": " + ex.getMessage());
                            warningCollection.addToList(warning);
                        }

                        logOnDatabaseService.logPreCheckinOnError(purchaseOrderRQ, cartId, recordLocator, ex);
                    } catch (Exception ex) {

                        if (ex.getMessage().contains(ErrorType.CANADA_CLEARANCE_REQD_FOR_PASSENGER.getSearchableCode())) {
                            Warning warning = new Warning(ErrorType.VERIFY_CANADIAN_VISA_OR_PSGR_CONTINUE.getErrorCode(), ErrorType.VERIFY_CANADIAN_VISA_OR_PSGR_CONTINUE.getFullDescription() + ": " + ex.getMessage());
                            warningCollection.addToList(warning);
                        } else if (ex.getMessage().contains(ErrorType.UK_BORDER_CLEARANCE_REQD_FOR_PSGR.getSearchableCode())) {
                            Warning warning = new Warning(ErrorType.UK_BORDER_CLEARANCE_REQD_FOR_PSGR_CONTINUE.getErrorCode(), ErrorType.AUTHORIZATION_PAYMENT_NOT_APPROVED_WEB.getFullDescription() + ": " + ex.getMessage());
                            warningCollection.addToList(warning);
                        } else {
                            Warning warning = new Warning(ErrorType.ERROR_DOING_CHECKIN.getErrorCode(), ErrorType.ERROR_DOING_CHECKIN.getFullDescription() + ": " + ex.getMessage());
                            warningCollection.addToList(warning);
                        }

                        logOnDatabaseService.logPreCheckinOnError(purchaseOrderRQ, cartId, recordLocator, ex);
                    }
                } else {
                    try {
                        acsCheckInPassengerRSACSList = checkInService.doCheckin(
                                purchaseOrderRQ,
                                bookedTravelersEligibleForCheckin,
                                passengersSuccessfulCheckedIn,
                                volunteerOffer,
                                warningCollection,
                                purchaseOrderRQ.getPos(),
                                purchaseOrderRQ.getLanguage(),
                                bookedLeg,
                                recordLocator,
                                cartId
                        );

                        checkinSuccessful = true;

                        for(ACSCheckInPassengerRSACS acsCheckInPassengerRSACSpax : acsCheckInPassengerRSACSList ){
                            String flight = acsCheckInPassengerRSACSpax.getItineraryPassengerList().getItineraryPassenger().get(0).getItineraryDetail().getAirline()+acsCheckInPassengerRSACSpax.getItineraryPassengerList().getItineraryPassenger().get(0).getItineraryDetail().getFlight();
                            String passengerId = acsCheckInPassengerRSACSpax.getItineraryPassengerList().getItineraryPassenger().get(0).getPassengerDetailList().getPassengerDetail().get(0).getPassengerID();
                            LOG.info("CHECKIN_WAS: SUCCESSFUL PNR: {} FLIGHT: {} PASSENGER_ID: {} CHANNEL: {}",recordLocator,flight,passengerId,pos);
                        }

                    } catch (GenericException ex) {

                        logOnDatabaseService.logPreCheckinOnError(purchaseOrderRQ, cartId, recordLocator, ex);
                        //last call and add to warning
                       if(ErrorType.NOT_ENOUGH_SEATS_AVAILABLE_WEB.equals(ex.getErrorCodeType())){

                            SimpleDateFormat formatterAeromexico = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                            SimpleDateFormat flightDateFormart = new SimpleDateFormat("yyyy-MM-dd");
                            SimpleDateFormat flightHourFormat = new SimpleDateFormat("hh:mma");
                           
                            String flagPassengerOnStandByList = System.getProperty("flagShowPassengerOnStandByList");
                            boolean flagActiveStandByList = Boolean.parseBoolean(flagPassengerOnStandByList);

                            String flightRs = bookedLeg.getSegments().getCollection().get(0).getSegment().getOperatingFlightCode();
                            String originRs = bookedLeg.getSegments().getCollection().get(0).getSegment().getDepartureAirport();
                            String destinationRs = bookedLeg.getSegments().getCollection().get(0).getSegment().getArrivalAirport();
                            String dateTimeFlight = bookedLeg.getSegments().getCollection().get(0).getSegment().getDepartureDateTime();

                            Date dateFormatFlightDeparture = formatterAeromexico.parse(dateTimeFlight);
                            String dateFlightRs = flightDateFormart.format(dateFormatFlightDeparture);
                            String hourFlightRs = flightHourFormat.format(dateFormatFlightDeparture);

                            List<String> airportsCodeOvs = null;
                            airportsCodeOvs = airportsCodesDao.getAirportOvsCodes();
                            boolean flagDestinationValid = false;
                            flagDestinationValid = CommonUtilsCheckIn.isDestinationValid(airportsCodeOvs,destinationRs);

                            SeatMap seatMapCollection = null;
                            seatMapCollection = getSeatMapCapacity(purchaseOrderRQ);
                            int seatAmPlus = 0;
                            int seatPrefered = 0;
                            int seatExitRow = 0;
                            int seatCoach = 0;
                            Map<String, CabinCapacity> mapSeatCapacity = new HashMap<>();
                            if(seatMapCollection != null){
                                ArrayList<CabinCapacity> totalCapacity = null;
                                totalCapacity  = seatMapCollection.getSegment().getCapacity();
                                SeatSummary seatSumaryFlight = seatMapCollection.getSeatSummary();
                                seatAmPlus = seatSumaryFlight.getAmPlus();
                                seatPrefered = seatSumaryFlight.getPreferred();
                                seatExitRow = seatSumaryFlight.getExitRow();
                                seatCoach = seatSumaryFlight.getCoach();
                                
                                for(CabinCapacity cap: totalCapacity){
                                    mapSeatCapacity.put(cap.getCabin(), cap);
                                }
                            }

                            boolean flagCountSeatFight = false;
                            String section = "empty";
                            if(seatAmPlus == 0 && seatPrefered == 0 && seatExitRow == 0 && seatCoach == 0){
                                //sobreventa
                                flagCountSeatFight = true;
                            }else{
                                if(seatAmPlus != 0 && seatPrefered == 0 && seatExitRow == 0 && seatCoach == 0){
                                    section = "LESS_AMPLUS";
                                }else if(seatAmPlus == 0 && seatPrefered != 0 && seatExitRow == 0 && seatCoach == 0){
                                    section = "LESS_PREFERED";
                                }else if(seatAmPlus == 0 && seatPrefered == 0 && seatExitRow != 0 && seatCoach == 0){
                                    section = "LESS_EXITROW";
                                }else if(seatAmPlus != 0 && seatPrefered != 0 && seatExitRow != 0 && seatCoach == 0){
                                    section = "LESS_AMPLUS_PREFERED_EXITROW";
                                }else if(seatAmPlus != 0 && seatPrefered != 0 && seatExitRow == 0 && seatCoach == 0){
                                    section = "LESS_AMPLUS_PREFERED";
                                }else if(seatAmPlus == 0 && seatPrefered != 0 && seatExitRow != 0 && seatCoach == 0){
                                    section = "LESS_PREFERED_EXITROW";
                                }else{
                                    section = "OTHERS";
                                }
                            }
                            
                            boolean flagWeb = false;
                            if(purchaseOrderRQ.getPos().equalsIgnoreCase("WEB")){
                                flagWeb  = true;
                            }

                            if(flagActiveStandByList && flagWeb && flagDestinationValid && flagCountSeatFight) {
                                
                                String flightFinal = "";
                                flightFinal = flightRs.substring(0,1);
                                if(flightFinal.equalsIgnoreCase("0")) {
                                    flightFinal = flightRs.substring(1); 
                                } else {
                                    flightFinal = flightRs;
                                }

                                OvsPassengerList totalPassengerFlight = null;
                                totalPassengerFlight = checkInServiceDao.getPassengerOvsStandByList(flightFinal, originRs, destinationRs, dateFlightRs, hourFlightRs);
                                
                                List<String> listIdPassengers = new ArrayList<>();
                                List<String> listIdDetailPassengers = new ArrayList<>();
                                for (BookedTraveler bookedTravelerPassenger: bookedTravelersEligibleForCheckin) {
                                    String passId = bookedTravelerPassenger.getId();
                                    listIdPassengers.add(passId);
                                }
                                if(null != totalPassengerFlight){
                                    for(PassengerDetail PassengerDetail: totalPassengerFlight.getPassengerDetail()){
                                        String passDetailId = PassengerDetail.getPassengerId();
                                        listIdDetailPassengers.add(passDetailId);
                                    }
                                }
                                boolean flagContainPassengerId = false;
                                flagContainPassengerId = listIdDetailPassengers.containsAll(listIdPassengers);
                                if(flagContainPassengerId){
                                    if(null == totalPassengerFlight || totalPassengerFlight.getSizeFlight() <= 5){
                                        ErrorJsonCheckIn errorJsonCheckinDos = new ErrorJsonCheckIn();
                                        errorJsonCheckinDos.setStandByList("SUCCESSFUL");
                                        errorJsonCheckinDos.setPnrRecortLocator(recordLocator);
                                        errorJsonCheckinDos.setReason(ErrorType.PASSENGER_ALREADY_ON_PRIORITY_LIST.getFullDescription());
                                        LOG.info("CHECKIN {} ",new Gson().toJson(errorJsonCheckinDos));
    
                                        CheckinConfirmationBase checkinConfirmationBase = null;
                                        for (BookedTraveler bookedTravelerWithClassE : bookedTravelersEligibleForCheckin) {
                                                bookedTravelerWithClassE.setCheckinStatus(true);
                                                for (BookedSegment segment : bookedLeg.getSegments().getCollection()) {
                                                    AbstractSegmentChoice abstractSegmentChoice;
                                                    abstractSegmentChoice = bookedTravelerWithClassE.getSegmentChoices().getBySegmentCode(
                                                            segment.getSegment().getSegmentCode()
                                                    );
                    
                                                    if (null == abstractSegmentChoice) {
                                                        SeatChoice seatChoice = new SeatChoice();
                    
                                                        seatChoice.setCode("STB");
                                                        PaidSegmentChoice paidSegmentChoice = new PaidSegmentChoice();
                                                        paidSegmentChoice.setSeat(seatChoice);
                                                        paidSegmentChoice.setSegmentCode(segment.getSegment().getSegmentCode());
                                                        bookedTravelerWithClassE.getSegmentChoices().getCollection().add(paidSegmentChoice);
                                                    } else {
                                                        abstractSegmentChoice.getSeat().setCode("STB");
                                                    }
    
                                                    BookedSegment firstSegmentOperatedByAM = bookedLeg.getSegments().getFirstOpenSegmentForCheckinOperatedBy(AirlineCodeType.AM);
                                                    SegmentDocument segmentDocument;
                                                    segmentDocument = getStbSegmentDocument(
                                                            bookedTravelerWithClassE,
                                                            firstSegmentOperatedByAM,
                                                            recordLocator
                                                    );
                        
                                                    bookedTravelerWithClassE.getSegmentDocumentsList().addOrReplace(segmentDocument);
                                                }
                                                if(null != bookedTravelerWithClassE.getInfant()){
                                                    for (BookedSegment segment : bookedLeg.getSegments().getCollection()) {
                                                        AbstractSegmentChoice abstractSegmentChoice;
                                                        abstractSegmentChoice = bookedTravelerWithClassE.getInfant().getSegmentChoices().getBySegmentCode(
                                                                segment.getSegment().getSegmentCode()
                                                        );
                        
                                                        if (null == abstractSegmentChoice) {
                                                            SeatChoice seatChoice = new SeatChoice();
                        
                                                            seatChoice.setCode("INF");
                                                            PaidSegmentChoice paidSegmentChoice = new PaidSegmentChoice();
                                                            paidSegmentChoice.setSeat(seatChoice);
                                                            paidSegmentChoice.setSegmentCode(segment.getSegment().getSegmentCode());
                                                            bookedTravelerWithClassE.getInfant().getSegmentChoices().getCollection().add(paidSegmentChoice);
                                                        } else {
                                                            abstractSegmentChoice.getSeat().setCode("INF");
                                                        }
        
                                                        BookedSegment firstSegmentOperatedByAM = bookedLeg.getSegments().getFirstOpenSegmentForCheckinOperatedBy(AirlineCodeType.AM);
                                                        SegmentDocument segmentDocument;
                                                        segmentDocument = getStbSegmentDocument(
                                                                bookedTravelerWithClassE,
                                                                firstSegmentOperatedByAM,
                                                                recordLocator
                                                        );
                            
                                                        bookedTravelerWithClassE.getInfant().getSegmentDocumentsList().addOrReplace(segmentDocument);
                                                    }
                                                }
                                        }
                                        bookedLeg.setOnStandByList(true);
                                        bookedLeg.setOvsReservation(true);
                                        String pnrAdult = purchaseOrderRQ.getRecordLocator();

                                        doPurchasePostCheckin(
                                            purchaseOrderRQ,
                                            pnrCollection,
                                            pnrCollectionAfterCheckin,
                                            FilterPassengersUtil.getAlreadyCheckedInPassengers(bookedTravelersEligibleForCheckin),
                                            serviceCallList
                                        );

                                        LOG.info("CHECKIN_SUCCESSFUL WITH INFANT OVS {}", pnrAdult);

                                    }else{
                                        Warning warning = new Warning(ErrorType.NOT_ENOUGH_SEATS_AVAILABLE_WEB.getErrorCode(), ErrorType.NOT_ENOUGH_SEATS_AVAILABLE_WEB.getFullDescription() + ": " + ex.getMessage());
                                        warningCollection.addToList(warning);
                                    }

                                
                                }else{
                                    Warning warning = new Warning(ErrorType.NOT_ENOUGH_SEATS_AVAILABLE_WEB.getErrorCode(), ErrorType.NOT_ENOUGH_SEATS_AVAILABLE_WEB.getFullDescription() + ": " + ex.getMessage());
                                    warningCollection.addToList(warning);
                                }
                            }else{
                                /*Warning warning = new Warning(ErrorType.NOT_ENOUGH_SEATS_AVAILABLE_WEB.getErrorCode(), ErrorType.NOT_ENOUGH_SEATS_AVAILABLE_WEB.getFullDescription() + ": " + ex.getMessage());
                                warningCollection.addToList(warning);*/
                                addWarningOvs(warningCollection, ex, section);
                            }
                        }else if (ErrorType.CANADA_CLEARANCE_REQD_FOR_PASSENGER.equals(ex.getErrorCodeType())) {
                            Warning warning = new Warning(ErrorType.VERIFY_CANADIAN_VISA_OR_PSGR_CONTINUE.getErrorCode(), ErrorType.VERIFY_CANADIAN_VISA_OR_PSGR_CONTINUE.getFullDescription() + ": " + ex.getMessage());
                            warningCollection.addToList(warning);
                        } else if (ErrorType.UK_BORDER_CLEARANCE_REQD_FOR_PSGR.equals(ex.getErrorCodeType())) {
                            Warning warning = new Warning(ErrorType.UK_BORDER_CLEARANCE_REQD_FOR_PSGR_CONTINUE.getErrorCode(), ErrorType.AUTHORIZATION_PAYMENT_NOT_APPROVED_WEB.getFullDescription() + ": " + ex.getMessage());
                            warningCollection.addToList(warning);
                        } else {
                            throw ex;
                        }
                    } catch (Exception ex) {

                        logOnDatabaseService.logPreCheckinOnError(purchaseOrderRQ, cartId, recordLocator, ex);

                        if (ex.getMessage().contains(ErrorType.CANADA_CLEARANCE_REQD_FOR_PASSENGER.getSearchableCode())) {
                            Warning warning = new Warning(ErrorType.VERIFY_CANADIAN_VISA_OR_PSGR_CONTINUE.getErrorCode(), ErrorType.VERIFY_CANADIAN_VISA_OR_PSGR_CONTINUE.getFullDescription() + ": " + ex.getMessage());
                            warningCollection.addToList(warning);
                        } else if (ex.getMessage().contains(ErrorType.UK_BORDER_CLEARANCE_REQD_FOR_PSGR.getSearchableCode())) {
                            Warning warning = new Warning(ErrorType.UK_BORDER_CLEARANCE_REQD_FOR_PSGR_CONTINUE.getErrorCode(), ErrorType.AUTHORIZATION_PAYMENT_NOT_APPROVED_WEB.getFullDescription() + ": " + ex.getMessage());
                            warningCollection.addToList(warning);
                        } else {
                            throw ex;
                        }
                    }
                }
            } else {
                LOG.info("No passengers eligible for checkin");
            }

            //LOG.debug(LogUtil.getLogDebug(SERVICE, recordLocator, cartId, "Passengers Successful Checked In\n" + GSON.toJson(passengersSuccessfulCheckedIn) + "\n"));
            //form the response and return
            CheckinConfirmation checkinConfirmation;

            if (checkinSuccessful && checkOverbooking) {
                List<CartPNR> carts = pnrCollection.getCartsPNRByRecordLocator(recordLocator);
                if (null != carts) {
                    for (CartPNR cart : carts) {
                        boolean eligible = setOverbookingEligibility(
                                cart.getTravelerInfo().getCollection(),
                                bookedLeg
                        );

                        volunteerQualifier = volunteerQualifier || eligible;
                    }
                }
            }

            startTime = Calendar.getInstance().getTimeInMillis();
            boolean checkForCheckin = true;
            if (null == acsCheckInPassengerRSACSList || acsCheckInPassengerRSACSList.isEmpty()) {
                //All passengers are ready checked in
                checkinConfirmation = PurchaseOrderUtil.getCheckInResponse(
                        purchaseOrder,
                        null,
                        pnr.getCarts(),
                        pnr.getLegs(),
                        recordLocator,
                        purchaseOrderRQ.getTransactionTime(),
                        purchaseOrderRQ.getPos(),
                        legCode,
                        checkForCheckin
                );

                checkinConfirmation.setWarnings(warningCollection);

            } else {

                if (PurchaseOrderUtil.searchInBoardingPasses("NOT VALID WITHOUT FLIGHT COUPON", acsCheckInPassengerRSACSList)) {
                    if (PurchaseOrderUtil.searchInAllBoardingPasses("NOT VALID WITHOUT FLIGHT COUPON", acsCheckInPassengerRSACSList, warningCollection)) {
                        throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.NOT_VALID_WITHOUT_FLIGHT_COUPON, PosType.getType(pos));
                    }
                }

                //this iteration is because sabre do checkin for all open flights
                List<CartPNR> carts = pnrCollection.getCartsPNRByRecordLocator(recordLocator);
                if (null != carts) {
                    for (CartPNR cartPNRLocal : carts) {

                        BookedLeg bookedLegLocal = pnrCollection.getBookedLegByLegCode(cartPNRLocal.getLegCode());

                        boolean touched = PnrCollectionUtil.updateAfterSuccessCheckin(
                                passengersSuccessfulCheckedIn,
                                acsCheckInPassengerRSACSList,
                                cartPNRLocal.getTravelerInfo().getCollection(),
                                bookedLegLocal,
                                purchaseOrderRQ.getPos(),
                                recordLocator,
                                pnr.isTsaRequired(),
                                warningCollection
                        );

                        cartPNRLocal.setTouched(touched);

                        CartPNR cartPNRPersisted;
                        cartPNRPersisted = pnrCollectionAfterCheckin.getCartPNRByRecordLocatorAndLegCode(
                                recordLocator, cartPNRLocal.getLegCode()
                        );

                        if (null != cartPNRPersisted) {
                            cartPNRPersisted.setTouched(touched);

                            PnrCollectionUtil.copyCheckinInfoFromCheckedInTravelers(
                                    passengersSuccessfulCheckedIn,
                                    cartPNRLocal.getTravelerInfo().getCollection(),
                                    cartPNRPersisted.getTravelerInfo().getCollection()
                            );
                        }

                        try {
                            if (SystemVariablesUtil.isAddRevenuePaxToPriorityListEnabled()
                                    && !passengersSuccessfulCheckedIn.isEmpty()
                                    && PassengerFareType.F.getCode().equalsIgnoreCase(cartPNRLocal.getTravelerInfo().getCollection().get(0).getPassengerType())) {

                                List<BookedSegment> checkInSegments = bookedLegLocal.getSegments().getOpenSegmentsOperatedBy(AirlineCodeType.AM);

                                if (null != checkInSegments && !checkInSegments.isEmpty()) {
                                    addPaxToPriorityListService.addTierPassengersToPriorityList(
                                            cartPNRLocal.getTravelerInfo().getCollection(),
                                            checkInSegments,
                                            pos
                                    );
                                }
                            }
                        } catch (Exception ex) {
                            LOG.error(ex.getMessage(), ex);
                        }
                    }
                }

                List<PectabBoardingPass> pectabDataList = PurchaseOrderUtil.getPectacBoardingPassList(
                        acsCheckInPassengerRSACSList,
                        recordLocator
                );

                if (null != pectabDataList && !pectabDataList.isEmpty()) {
                    cartPNR.getPectabBoardingPassList().addAllIfNotExist(pectabDataList);
                    cartPNRAfterCheckin.getPectabBoardingPassList().addAllIfNotExist(pectabDataList);
                }

                checkinConfirmation = PurchaseOrderUtil.getCheckInResponse(
                        purchaseOrder,
                        acsCheckInPassengerRSACSList,
                        pnr.getCarts(),
                        pnr.getLegs(),
                        recordLocator,
                        purchaseOrderRQ.getTransactionTime(),
                        purchaseOrderRQ.getPos(),
                        legCode,
                        checkForCheckin
                );

                checkinConfirmation.setWarnings(warningCollection);
            }

            //for weather
            startTime = Calendar.getInstance().getTimeInMillis();
            try {
                if (PosType.KIOSK.toString().equalsIgnoreCase(purchaseOrderRQ.getPos())) {
                    boolean isLanguageMX = false;
                    if (null != purchaseOrderRQ.getLanguage()) {
                        isLanguageMX = LanguageType.ES.name().equalsIgnoreCase(purchaseOrderRQ.getLanguage());
                    }
                    String formatArrival;
                    formatArrival = bookedLeg.getSegments().getCollection().get(0).getSegment().getArrivalDateTime().substring(0, 10);
                    String iataCity = bookedLeg.getSegments().getCollection().get(0).getSegment().getArrivalAirport();

                    DailyForecasts wheatherCityDest = WeatherService.getWeather(iataCity, formatArrival, isLanguageMX);
                    checkinConfirmation.setDailyForecasts(wheatherCityDest);

                    LOG.debug(LogUtil.getLogDebug(SERVICE, recordLocator, cartId, "Trying to get the weather of destination\n" + wheatherCityDest + "\n"));
                }
            } catch (Exception ex) {
                LOG.error(LogUtil.getLogError(SERVICE, "Error Calling Weather Service", recordLocator, cartId, ex));
            }
            TimeMonitorUtil.addCallToMonitor(serviceCallList, "Getting weather", startTime);

            startTime = Calendar.getInstance().getTimeInMillis();
            updateOnSuccess(pnrCollection, pnrCollectionAfterCheckin, recordLocator, cartId, false, purchaseOrderRQ);
            TimeMonitorUtil.addCallToMonitor(serviceCallList, "Update pnr collections before and after checkin", startTime);

            checkinConfirmation.setMustOfferOverbooking(volunteerQualifier);

            Map<String, String> mappedPolicyChubb = callChubbPolicy(pnr, purchaseOrderRQ, cartId);

            checkinConfirmation.setSendPolicyChubb(mappedPolicyChubb);
            //checkinConfirmation.setSendPolicyChubb(null);

            return checkinConfirmation;

        } catch (GenericException ex) {
            updateOnError(pnrCollection, pnrCollectionAfterCheckin, recordLocator, cartId);

            throw ex;
        } catch (Exception ex) {
            updateOnError(pnrCollection, pnrCollectionAfterCheckin, recordLocator, cartId);

            LOG.error(LogUtil.getLogError(SERVICE, "Doing Purchase Pre Checkin", recordLocator, cartId, ex));
            throw ex;
        }
    }

    public void addWarningOvs(WarningCollection warningCollection, GenericException ex, String section){
        String messageError = ex.getMessage() == null ? section : ex.getMessage();
        Warning warning = new Warning(ErrorType.NOT_ENOUGH_SEATS_AVAILABLE_WEB.getErrorCode(), ErrorType.NOT_ENOUGH_SEATS_AVAILABLE_WEB.getFullDescription() + ": " + messageError);
        warningCollection.addToList(warning);
    }

    public List<ACSFlightDetailRSACS.PassengerCounts> getCapaityByFlightDetails( String airlineCode, String flightNumber,String departureCity, String departureDate, String pos) {

        List<ACSFlightDetailRSACS.PassengerCounts> passengerCount = null;

        ACSFlightDetailRSACS acsFlightDetailRSACS = null;
        try {
            acsFlightDetailRSACS = flightDetailsService.getFlightDetails(airlineCode, flightNumber, departureCity, departureDate, pos);
            passengerCount = acsFlightDetailRSACS.getPassengerCounts();
        } catch (Exception e) {
            LOG.error("ERROR_getCapaityByFlightDetails {} ", e.getMessage());
        }
        return passengerCount;
    }

    public SeatMap getSeatMapCapacity(PurchaseOrderRQ purchaseOrderRQ){

        SeatmapCheckInRQ seatmapCheckInRQ  =new SeatmapCheckInRQ();
        SeatMap seatMapCollectionFinal = null;
        try {
            seatmapCheckInRQ.setCartId(purchaseOrderRQ.getPurchaseOrder().getCartId());
            seatmapCheckInRQ.setPos(purchaseOrderRQ.getPos());
            seatmapCheckInRQ.setLanguage(purchaseOrderRQ.getLanguage());

            SeatmapCollection seatMapCollection = null;
        
            seatMapCollection = seatMapsServiceSoap.getSeatMaps(seatmapCheckInRQ);
       
            if(null != seatMapCollection){
                AbstractSeatMap collection = (SeatMap) seatMapCollection.getCollection().get(0);

                if (collection instanceof SeatMap) {
                        seatMapCollectionFinal = (SeatMap) collection;
                }
            }
        } catch (SabreEmptyResponseException ser) {
            LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110302).toString(), ser.fillInStackTrace());
            throw ser;
        }catch (SabreLayerUnavailableException se){
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(purchaseOrderRQ.getPos())) {
                LOG.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_22110301,"PurchaseService.java:1670", se.getMessage());
                throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getCode(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getActionable(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getMessage(),
                        Response.Status.INTERNAL_SERVER_ERROR);
            } else {
                LOG.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_12110301,"PurchaseService.java:1676", se.getMessage());
                throw se;
            }
        } catch (Exception e) {
            return null;
        }

        return seatMapCollectionFinal;
    }

    /**
     *
     */
    void sanatizeData(CheckinConfirmation checkinConfirmation) {
        for (CartPNRConfirmation cartPNRConfirmation : checkinConfirmation.getCheckedInCarts().getCollection()) {
            for (CheckedInTraveler checkedInTraveler : cartPNRConfirmation.getTravelerInfo().getCollection()) {
                for (SegmentDocument segmentDocument : checkedInTraveler.getSegmentDocumentsList()) {
                    if (segmentDocument != null && segmentDocument.getTravelDoc() != null) {
                        LOG.info("TravelDocData BEFORE: " + segmentDocument.getTravelDoc());
                        segmentDocument.setTravelDoc(segmentDocument.getTravelDoc().replaceAll("&lt;", ">"));
                        LOG.info("TravelDocData AFTER: " + segmentDocument.getTravelDoc());
                    }
                }
            }
        }
    }

    /**
     * @return
     */
    private Address getDefaultAddress() {
        Address address = new Address();
        address.setAddressOne("1295 Charleston Road");
        address.setAddressTwo("");
        address.setCity("Mountain View");
        address.setCountry("US");
        address.setState("CA");
        address.setZipCode("94043");
        return address;
    }

    /**
     * @param purchaseOrderRQ
     * @param recordLocator
     * @param cartId
     * @return
     * @throws GenericException
     * @throws Exception
     */
    private Address getAddresFromKioskId(PurchaseOrderRQ purchaseOrderRQ, String recordLocator, String cartId) throws GenericException, Exception {
        try {
            //call cybersource
            KioskProfiles kioskProfiles = kioskProfileDao.findProfile(purchaseOrderRQ.getKioskId());
            if (null == kioskProfiles) {
//            LOG.error(LogUtil.getLogError(SERVICE, ErrorType.UNKNOWN_KIOSK_PROFILE_ID.getFullDescription(), recordLocator, cartId));
//            GenericException ex = new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNKNOWN_KIOSK_PROFILE_ID, ErrorType.UNKNOWN_KIOSK_PROFILE_ID.getFullDescription());
//            ex.setWasLogged(true);
//            logOnDatabaseService.logPreCheckinOnError(purchaseOrderRQ, cartId, recordLocator, ex);
//            throw ex;
                return getDefaultAddress();
            }
            Address address = kioskProfiles.getBillingAddress();
            if (null == address) {
//            LOG.error(LogUtil.getLogError(SERVICE, ErrorType.BILLING_ADDRESS_REQUIRED.getFullDescription(), recordLocator, cartId));
//            GenericException ex = new GenericException(Response.Status.BAD_REQUEST, ErrorType.BILLING_ADDRESS_REQUIRED, ErrorType.BILLING_ADDRESS_REQUIRED.getFullDescription());
//            ex.setWasLogged(true);
//            logOnDatabaseService.logPreCheckinOnError(purchaseOrderRQ, cartId, recordLocator, ex);
//            throw ex;
                return getDefaultAddress();
            }
            return address;
        } catch (Exception ex) {
            LOG.error("Using default address");
            LOG.error(ex.getMessage(), ex);

            return getDefaultAddress();
        }
    }

    /**
     * @param bookedTravelersEligibleForCheckin
     * @param bookedLeg
     * @return
     */
    private boolean setOverbookingEligibility(
            List<BookedTraveler> bookedTravelersEligibleForCheckin,
            BookedLeg bookedLeg
    ) {

        boolean volunteerQualifier = false;

        try {
            LOG.info("Calling overbooking service");

            boolean atLeastOne = false;
            for (BookedTraveler bookedTraveler : bookedTravelersEligibleForCheckin) {
                if (bookedTraveler.isSelectedToCheckin()) {
                    boolean overBookingEligibility = PNRLookUpServiceUtil.isOverbookingEligible(bookedTraveler);
                    LOG.info("overBookingEligibility: {}", overBookingEligibility);
                    bookedTraveler.setIsOverBookingEligible(overBookingEligibility);
                    if (overBookingEligibility) {
                        atLeastOne = true;
                    }
                } else {
                    //If the passenger is NOT selected for check-in, by default is NOT eligibile for overbooking offer
                    bookedTraveler.setIsOverBookingEligible(false);
                }
            }

            if (atLeastOne) {
                BookedSegment bookedSegment = bookedLeg.getSegments().getFirstSegmentOperatedBy(AirlineCodeType.AM);

                int volunteeres = passengersListService.getVolunteeredCount(
                        bookedSegment.getSegment().getOperatingCarrier(),
                        bookedSegment.getSegment().getOperatingFlightCode(),
                        bookedSegment.getSegment().getDepartureAirport(),
                        bookedSegment.getSegment().getDepartureDateTime().split("T")[0]
                );

                int maxVolunteers = overBookingConfigDao.getMaxVolunteers(
                        bookedSegment.getSegment().getDepartureAirport(),
                        bookedSegment.getSegment().getOperatingFlightCode(),
                        bookedSegment.getSegment().getDepartureDateTime()
                );

                if (volunteeres < maxVolunteers) {
                    volunteerQualifier = true;
                }
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
        return volunteerQualifier;
    }

    /**
     * @param purchaseOrderRQ
     * @param pnrCollection
     * @param pnrCollectionAfterCheckin
     * @param bookedTravelersRequested
     * @param serviceCallList
     * @return
     * @throws Exception
     */
    private CheckinConfirmationBase doPurchaseExtraWeight(
            PurchaseOrderRQ purchaseOrderRQ,
            PNRCollection pnrCollection,
            PNRCollection pnrCollectionAfterCheckin,
            List<BookedTraveler> bookedTravelersRequested,
            List<ServiceCall> serviceCallList
    ) throws Exception {
        long startTime;
        ErrorCauseWrapper errorCauseWrapper = new ErrorCauseWrapper();
        PurchaseOrder purchaseOrder = purchaseOrderRQ.getPurchaseOrder();
        String cartId = purchaseOrder.getCartId();
        String store = purchaseOrderRQ.getStore();
        PNR pnr = pnrCollection.getPNRByCartId(cartId);
        String recordLocator = pnr.getPnr();
        CartPNR cartPNR = pnr.getCartPNRByCartId(cartId);
        String legCode = cartPNR.getLegCode();
        BookedLeg bookedLeg = pnr.getBookedLegByLegCode(legCode);
        CartPNR cartPNRAfterCheckin;
        cartPNRAfterCheckin = pnrCollectionAfterCheckin.getCartPNRByRecordLocatorAndLegCode(
                recordLocator, legCode
        );

        //get the total present in cart
        BigDecimal totalInPurchase = BigDecimal.ZERO;
        BigDecimal totalInCart;
        totalInCart = cartPNR.getMeta().getTotal().getCurrency().getTotal();
        if (null == totalInCart) {
            totalInCart = BigDecimal.ZERO;
        }

        purchaseOrder.setPaymentAmount(cartPNR.getMeta().getTotal().getCurrency());

        String currencyCode;
        try {
            currencyCode = cartPNR.getMeta().getTotal().getCurrency().getCurrencyCode();
        } catch (Exception ex) {
            //default currency code
            currencyCode = CurrencyType.MXN.toString();
        }

        boolean areThereUnpaidItems = PurchaseOrderUtil.areThereExtraWeightUnpaidItems(bookedTravelersRequested);
        boolean isTherePaymentMethod = (null != purchaseOrder.getPaymentInfo()
                && null != purchaseOrder.getPaymentInfo().getCollection()
                && !purchaseOrder.getPaymentInfo().getCollection().isEmpty());

        if (areThereUnpaidItems && !isTherePaymentMethod) {
            LOG.error(LogUtil.getLogError(SERVICE, " There are unpaid extra weight items, you need to specify a payment method", recordLocator, cartId));
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PAYMENT_METHOD_IS_REQUIRED, "There are unpaid extra weight items, you need to specify a payment method");
        }

        FormsOfPaymentWrapper formsOfPaymentWrapper;
        formsOfPaymentWrapper = new FormsOfPaymentWrapper(
                purchaseOrder.getPaymentInfo()
        );

        if (areThereUnpaidItems) {

            //valid forms of payment on checkin are credit card and uatp card and visa checkout
            boolean isThereValidPaymentMethod = !formsOfPaymentWrapper.getCreditCardPaymentRequestList().isEmpty()
                    || !formsOfPaymentWrapper.getUatpPaymentRequestList().isEmpty() || !formsOfPaymentWrapper.getVisaCheckoutPaymentRequestList().isEmpty();

            if (areThereUnpaidItems && !isThereValidPaymentMethod) {
                LOG.error(LogUtil.getLogError(SERVICE, " There are unpaid extra weight items, you need to specify a valid payment method", recordLocator, cartId));
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PAYMENT_METHOD_IS_REQUIRED, "There are unpaid extra weight items, you need to specify a valid payment method");
            }

            //compare the total present in the payment info request
            totalInPurchase = PurchaseOrderUtil.getTotal(formsOfPaymentWrapper, totalInCart);

            if (0 != totalInCart.compareTo(totalInPurchase)) {
                LOG.error(LogUtil.getLogError(SERVICE, "Prices on cart and purchase order does not match", recordLocator, cartId));
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PRICES_NOT_MATCH, ErrorType.PRICES_NOT_MATCH.getFullDescription());
            }

        }

        try {

            IssuedBagTagList issueBagRPList = new IssuedBagTagList();

            if (areThereUnpaidItems && 0 < totalInPurchase.compareTo(BigDecimal.ZERO)) {

                //call authorization payment only if the request comes from KIOSK
                List<AuthorizationResult> authorizationResultList = new ArrayList<>();

                if (PosType.KIOSK.toString().equalsIgnoreCase(purchaseOrderRQ.getPos())
                        && !purchaseOrderRQ.getPurchaseOrder().isPinPadNotPresent()) {

                    validateAuthorizationForKiosk(purchaseOrderRQ, recordLocator, cartId);

                    authorizationResultList.add(purchaseOrder.getAuthorizationResult());

                    cartPNR.setPectabReceiptList(cartPNRAfterCheckin.getPectabReceiptList());
                    cartPNR.setPaymentLinkedList(cartPNRAfterCheckin.getPaymentLinkedList());

                    PnrCollectionUtil.setDefaultCardInfo(purchaseOrder.getPaymentInfo());
                } else if (purchaseOrder.isThreeDsPayment() && null != purchaseOrder.getAuthorizationResult()) {
                    validateAuthorizationForKiosk(purchaseOrderRQ, recordLocator, cartId);

                    authorizationResultList.add(purchaseOrder.getAuthorizationResult());
                } else {
                    if (PosType.KIOSK.toString().equalsIgnoreCase(purchaseOrderRQ.getPos())
                            && purchaseOrder.isPinPadNotPresent()) {

                        if (!formsOfPaymentWrapper.getCreditCardPaymentRequestList().isEmpty()) {
                            int maxRetries = 1;// 0 and 1 (two retries)
                            int retryAttempt = (null == purchaseOrder.getRetryCount()) ? 0 : purchaseOrder.getRetryCount();

                            //call cybersource
                            Address address = getAddresFromKioskId(purchaseOrderRQ, recordLocator, cartId);

                            try {
                                this.callCybersource(
                                        formsOfPaymentWrapper.getCreditCardPaymentRequestList(),
                                        address,
                                        purchaseOrderRQ,
                                        bookedTravelersRequested,
                                        pnr.getLegs().getCollection(),
                                        ItemsToBePaidType.EXTRAWEIGHT,
                                        recordLocator,
                                        TimeUtil.getStrTime(purchaseOrderRQ.getTransactionTime(), "yyyy-MM-dd'T'HH:mm:ss"),
                                        retryAttempt,
                                        maxRetries,
                                        currencyCode,
                                        errorCauseWrapper,
                                        purchaseOrderRQ.getKioskId()
                                );

                                purchaseOrder.setThreeDsPayment(false);
                                purchaseOrder.setThreeDSRedirectURL(null);

                            } catch (Exception ex) {
                                LOG.error(ex.getMessage(), ex);

                                if (null != purchaseOrder.getThreeDSRedirectURL()) {
                                    for (CreditCardPaymentRequest creditCardPaymentRequest : formsOfPaymentWrapper.getCreditCardPaymentRequestList()) {
                                        creditCardPaymentRequest.setEmail(_3DS_EMAIL_TRIGGER);
                                    }
                                }

                            }
                        }
                    }

                    //form request for payment
                    /**
                     * TravelerAncillaryList is a collection to store the
                     * ancillaries added to this payment request
                     *
                     */
                    List<AbstractAncillary> abstractAncillaryList = new ArrayList<>();

                    authorizationResultList = paymentAuthorizationService.callAuthorization(
                            bookedTravelersRequested,
                            bookedLeg,
                            purchaseOrderRQ,
                            abstractAncillaryList,
                            ItemsToBePaidType.EXTRAWEIGHT,
                            recordLocator,
                            cartId,
                            serviceCallList
                    );

                    for (AuthorizationResult authorizationResult : authorizationResultList) {

                        if (null != authorizationResult.getT3dsResult() && null != authorizationResult.getRedirectForm()) {
                            ThreeDsFormResponse threeDsFormResponse = new ThreeDsFormResponse();
                            threeDsFormResponse.setRedirectForm(authorizationResult.getRedirectForm());
                            threeDsFormResponse.setT3dsResult(authorizationResult.getT3dsResult());

                            return threeDsFormResponse;
                        }

                        if (AuthorizationPaymentResultType.APPROVED.toString().equalsIgnoreCase(authorizationResult.getResponseCode())) {
                            PaymentLinked paymentLinked = PnrCollectionUtil.addPaymentId(
                                    bookedTravelersRequested,
                                    abstractAncillaryList,
                                    authorizationResult,
                                    purchaseOrderRQ.getTransactionTime()
                            );
                            cartPNR.getPaymentLinkedList().addIfNotExist(paymentLinked);
                            cartPNRAfterCheckin.getPaymentLinkedList().addIfNotExist(paymentLinked);
                        } else {
                            if (PosType.WEB.toString().equalsIgnoreCase(purchaseOrderRQ.getPos())) {
                                LOG.error(LogUtil.getLogError(SERVICE, "AUTHORIZATION PAYMENT NOT APPROVED", recordLocator, cartId));
                                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.AUTHORIZATION_PAYMENT_NOT_APPROVED_WEB, ErrorType.AUTHORIZATION_PAYMENT_NOT_APPROVED_WEB.getFullDescription());
                            } else {
                                LOG.error(LogUtil.getLogError(SERVICE, "AUTHORIZATION PAYMENT NOT APPROVED", recordLocator, cartId));
                                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.AUTHORIZATION_PAYMENT_NOT_APPROVED, ErrorType.AUTHORIZATION_PAYMENT_NOT_APPROVED.getFullDescription());
                            }
                        }

                        aeService.addRemark(authorizationResult, recordLocator, cartId, serviceCallList);
                    }
                }

                //add new ancillaries in shopping cart to reservation
                aeService.addNewExtraWeightAncillariesToReservation(
                        bookedTravelersRequested,
                        bookedLeg,
                        recordLocator,
                        cartId,
                        store,
                        serviceCallList,
                        purchaseOrderRQ.getPos()
                );

                PnrCollectionUtil.copyTouchedTravelers(
                        cartPNR.getTravelerInfo().getCollection(),
                        cartPNRAfterCheckin.getTravelerInfo().getCollection()
                );

                //LOG.debug(LogUtil.getLogDebug(SERVICE, recordLocator, cartId, "Pnr Collection After Update Reservation\n" + GSON.toJson(pnrCollection) + "\n"));
                //form request for emd
                GetTicketingDocumentRS ticketingDocumentRS = null;
                CollectMiscFeeRQ collectMiscFeeRQ = emdService.getCollectMiscFeeRQForExtraWeight(
                        bookedTravelersRequested,
                        bookedLeg,
                        purchaseOrderRQ,
                        authorizationResultList,
                        ticketingDocumentRS,
                        recordLocator,
                        cartId,
                        serviceCallList
                );

                //LOG.debug(LogUtil.getLogDebug(SERVICE, recordLocator, cartId, "Collect Misc Fee RQ\n" + GSON.toJson(collectMiscFeeRQ) + "\n"));
                if (null != collectMiscFeeRQ
                        && null != collectMiscFeeRQ.getFees()
                        && !collectMiscFeeRQ.getFees().isEmpty()) {

                    boolean checkForRetry;
                    CollectMiscFeeRS collectMiscFeeRS = null;
                    try {
                        checkForRetry = true;

                        //call issue emds
                        collectMiscFeeRS = emdService.issueEmdCodes(
                                collectMiscFeeRQ, purchaseOrderRQ,
                                "Collect Misc Fee", checkForRetry,
                                serviceCallList
                        );
                    } catch (RetryCheckinException ex) {
                        checkForRetry = false;
                        boolean retry = true;

                        if (ErrorType.INVALID_COUPON_NUMBER.equals(ex.getErrorCodeType())) {
                            LOG.info("INVALID COUPON NUMBER: changing coupon");
                            retry = emdService.changeCouponNumber(
                                    collectMiscFeeRQ, ticketingDocumentRS,
                                    recordLocator, cartId, serviceCallList,
                                    purchaseOrderRQ.getPos()
                            );
                        }

                        if (retry) //call issue emds
                        {
                            collectMiscFeeRS = emdService.issueEmdCodes(
                                    collectMiscFeeRQ, purchaseOrderRQ,
                                    "Collect Misc Fee", checkForRetry,
                                    serviceCallList
                            );
                        }
                    }

                    try {
                        issueBagRPList = callIssueBagTag(
                                pnrCollection,
                                bookedTravelersRequested,
                                cartId,
                                recordLocator,
                                purchaseOrderRQ
                        );
                    } catch (Exception ex) {
                        //
                    }

                    //ref to get emds
                    int groupEmdsBy = 3;
                    IssuedEmds issuedEmds = new IssuedEmds(groupEmdsBy);
                    if (null != collectMiscFeeRS && null != collectMiscFeeRS.getFees()) {
                        PnrCollectionUtil.changeUnpaidAncillariesToPaid(
                                bookedTravelersRequested, collectMiscFeeRS.getFees(), issuedEmds
                        );
                    }

                    PnrCollectionUtil.copyTouchedTravelers(
                            cartPNR.getTravelerInfo().getCollection(),
                            cartPNRAfterCheckin.getTravelerInfo().getCollection()
                    );

                    LOG.info(LogUtil.getLogDebug(SERVICE, recordLocator, cartId, "Issued emds: " + issuedEmds.toString()));
                    PectabReceipt pectabReceipt;
                    pectabReceipt = getPectabReceipt(
                            authorizationResultList,
                            purchaseOrderRQ,
                            issuedEmds,
                            recordLocator,
                            cartId,
                            currencyCode
                    );

                    if (null != pectabReceipt) {
                        cartPNR.getPectabReceiptList().addIfNotExist(pectabReceipt);
                        cartPNRAfterCheckin.getPectabReceiptList().addIfNotExist(pectabReceipt);
                    } else {
                        LOG.error("No receipt was generated");
                    }

                    //LOG.debug(LogUtil.getLogDebug(SERVICE, recordLocator, cartId, "Pnr Collection After Issue EMDs\n" + GSON.toJson(pnrCollection) + "\n"));
                    //LOG.debug(LogUtil.getLogDebug(SERVICE, recordLocator, cartId, "Pnr Collection After Issue EMDs\n" + GSON.toJson(pnrCollectionAfterCheckin) + "\n"));
                } else {
                    LOG.error(LogUtil.getLogDebug(SERVICE, recordLocator, cartId, "There was not fees (possible cause action code different that HD)\n"));
                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.NO_UNFULFILLED_AE_ITEM);
                }

            } else if (areThereUnpaidItems) {
                LOG.error(LogUtil.getLogError(SERVICE, "There are unpaid extra weight items, you need to specify a payment method with an amount", recordLocator, cartId));
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PAYMENT_METHOD_IS_REQUIRED, "There are unpaid extra weight items, you need to specify a payment method with an amount");
            } else {//nothing to be paid
                LOG.info(LogUtil.getLogInfo(SERVICE, "Nothing to be paid", recordLocator, cartId));
            }

            //the requested cart is always updated in this step,
            //the other carts are updated only if the checkin is done for all flights
            cartPNR.setTouched(true);
            cartPNRAfterCheckin.setTouched(true);
            boolean checkForCheckin = false;
            CheckinConfirmation checkinConfirmation = PurchaseOrderUtil.getCheckInResponse(
                    purchaseOrder, null, pnr.getCarts(), pnr.getLegs(), recordLocator,
                    purchaseOrderRQ.getTransactionTime(), purchaseOrderRQ.getPos(), legCode, checkForCheckin
            );

            checkinConfirmation.setIssuedBagTags(issueBagRPList);

            //Remove EmdConsummedAtIssuance ancillaries from the collection
            PurchaseOrderUtil.removeEmdConsummedAtIssuance(pnr.getCarts());

            startTime = Calendar.getInstance().getTimeInMillis();
            updateOnSuccess(pnrCollection, pnrCollectionAfterCheckin, recordLocator, cartId, false, purchaseOrderRQ);
            TimeMonitorUtil.addCallToMonitor(serviceCallList, "Update pnr collections before and after checkin", startTime);

            return checkinConfirmation;

        } catch (GenericException ex) {
            updateOnError(pnrCollection, pnrCollectionAfterCheckin, recordLocator, cartId);

            throw ex;
        } catch (Exception ex) {
            updateOnError(pnrCollection, pnrCollectionAfterCheckin, recordLocator, cartId);

            LOG.error(LogUtil.getLogError(SERVICE, "Doing Purchase Pre Checkin", recordLocator, cartId, ex));
            throw ex;
        }
    }

    /**
     * @param pnrCollection
     * @param bookedTravelersRequested
     * @param cartId
     * @param recordLocator
     * @param purchaseOrderRQ
     * @return
     */
    private IssuedBagTagList callIssueBagTag(
            PNRCollection pnrCollection,
            List<BookedTraveler> bookedTravelersRequested,
            String cartId,
            String recordLocator,
            PurchaseOrderRQ purchaseOrderRQ
    ) {
        IssuedBagTagList issueBagRPList = new IssuedBagTagList();

        for (BookedTraveler bookedTraveler : bookedTravelersRequested) {
            for (ExtraWeightAncillary extraWeightAncillary : PurchaseOrderUtil.getExtraWeightUnpaidAncillariesOnReservation(bookedTraveler.getAncillaries().getCollection())) {
                try {
                    CartPNRUpdate cartPNRUpdate = new CartPNRUpdate();
                    BookedTravelerCollection travelerInfo = new BookedTravelerCollection();
                    BaggageInfo baggageInfo = new BaggageInfo();
                    baggageInfo.setUnit(extraWeightAncillary.getWeightUnit());
                    baggageInfo.setWeight(extraWeightAncillary.getTotalWeight());
                    bookedTraveler.setBaggageInfo(baggageInfo);
                    travelerInfo.getCollection().add(bookedTraveler);
                    cartPNRUpdate.setTravelerInfo(travelerInfo);

                    //issue bag tags
                    IssueBagRQ issueBagRQ = new IssueBagRQ();
                    issueBagRQ.setCartId(cartId);
                    issueBagRQ.setRecordLocator(recordLocator);
                    issueBagRQ.setLocation(purchaseOrderRQ.getLocation());
                    issueBagRQ.setStore(purchaseOrderRQ.getStore());
                    issueBagRQ.setPos(purchaseOrderRQ.getPos());
                    issueBagRQ.setCartPNRUpdate(cartPNRUpdate);

                    IssueBagRP issueBagRP = issueBagTagService.getBagTag(
                            bookedTraveler, issueBagRQ, pnrCollection
                    );
                    issueBagRPList.add(issueBagRP);
                } catch (Exception ex) {
                    LOG.error(ex.getMessage(), ex);
                }
            }
        }

        return issueBagRPList;
    }

    /**
     * @param purchaseOrderRQ
     * @param pnrCollection
     * @param pnrCollectionAfterCheckin
     * @param bookedTravelersEligibleForCheckin
     * @param serviceCallList
     * @return
     * @throws Exception
     */
    private CheckinConfirmationBase doPurchaseUpgrade(
            PurchaseOrderRQ purchaseOrderRQ,
            PNRCollection pnrCollection,
            PNRCollection pnrCollectionAfterCheckin,
            List<BookedTraveler> bookedTravelersEligibleForCheckin,
            List<ServiceCall> serviceCallList
    ) throws Exception {
        long startTime;
        ErrorCauseWrapper errorCauseWrapper = new ErrorCauseWrapper();
        PurchaseOrder purchaseOrder = purchaseOrderRQ.getPurchaseOrder();
        String cartId = purchaseOrder.getCartId();
        String store = purchaseOrderRQ.getStore();
        PNR pnr = pnrCollection.getPNRByCartId(cartId);
        String recordLocator = pnr.getPnr();
        CartPNR cartPNR = pnr.getCartPNRByCartId(cartId);
        String legCode = cartPNR.getLegCode();
        BookedLeg bookedLeg = pnr.getBookedLegByLegCode(legCode);
        CartPNR cartPNRAfterCheckin = pnrCollectionAfterCheckin.getCartPNRByRecordLocatorAndLegCode(recordLocator, legCode);

        //get the total present in cart
        BigDecimal totalInPurchase = BigDecimal.ZERO;
        BigDecimal totalInCart;
        totalInCart = cartPNR.getMeta().getTotal().getCurrency().getTotal();
        if (null == totalInCart) {
            totalInCart = BigDecimal.ZERO;
        }

        purchaseOrder.setPaymentAmount(cartPNR.getMeta().getTotal().getCurrency());

        String currencyCode;
        try {
            currencyCode = cartPNR.getMeta().getTotal().getCurrency().getCurrencyCode();
        } catch (Exception ex) {
            //default currency code
            currencyCode = CurrencyType.MXN.toString();
        }

        boolean areThereUnpaidItems = PurchaseOrderUtil.areThereUpgradeUnpaidItems(bookedTravelersEligibleForCheckin);
        boolean isTherePaymentMethod = (null != purchaseOrder.getPaymentInfo()
                && null != purchaseOrder.getPaymentInfo().getCollection()
                && !purchaseOrder.getPaymentInfo().getCollection().isEmpty());

        if (areThereUnpaidItems && !isTherePaymentMethod) {
            LOG.error(LogUtil.getLogError(SERVICE, " There are unpaid upgrade items, you need to specify a payment method", recordLocator, cartId));
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PAYMENT_METHOD_IS_REQUIRED, "There are unpaid upgrade items, you need to specify a payment method");
        }

        FormsOfPaymentWrapper formsOfPaymentWrapper;
        formsOfPaymentWrapper = new FormsOfPaymentWrapper(
                purchaseOrder.getPaymentInfo()
        );

        if (areThereUnpaidItems) {

            //valid forms of payment on checkin are credit card and uatp card and visa checkout
            boolean isThereValidPaymentMethod = !formsOfPaymentWrapper.getCreditCardPaymentRequestList().isEmpty()
                    || !formsOfPaymentWrapper.getUatpPaymentRequestList().isEmpty() || !formsOfPaymentWrapper.getVisaCheckoutPaymentRequestList().isEmpty();

            if (areThereUnpaidItems && !isThereValidPaymentMethod) {
                LOG.error(LogUtil.getLogError(SERVICE, " There are unpaid items, you need to specify a valid payment method", recordLocator, cartId));
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PAYMENT_METHOD_IS_REQUIRED, "There are unpaid items, you need to specify a valid payment method");
            }

            //compare the total present in the payment info request
            totalInPurchase = PurchaseOrderUtil.getTotal(formsOfPaymentWrapper, totalInCart);

            if (0 != totalInCart.compareTo(totalInPurchase)) {
                LOG.error(LogUtil.getLogError(SERVICE, "Prices on cart and purchase order does not match", recordLocator, cartId));
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PRICES_NOT_MATCH, ErrorType.PRICES_NOT_MATCH.getFullDescription());
            }

        }

        boolean checkinSuccessful = false;

        try {
            WarningCollection warningCollection = new WarningCollection();

            if (areThereUnpaidItems && 0 < totalInPurchase.compareTo(BigDecimal.ZERO)) {
                //call authorization payment only if the request comes from KIOSK
                List<AuthorizationResult> authorizationResultList = new ArrayList<>();

                if (PosType.KIOSK.toString().equalsIgnoreCase(purchaseOrderRQ.getPos())
                        && !purchaseOrderRQ.getPurchaseOrder().isPinPadNotPresent()) {
                    validateAuthorizationForKiosk(purchaseOrderRQ, recordLocator, cartId);

                    authorizationResultList.add(purchaseOrder.getAuthorizationResult());

                    cartPNR.setPectabReceiptList(cartPNRAfterCheckin.getPectabReceiptList());
                    cartPNR.setPaymentLinkedList(cartPNRAfterCheckin.getPaymentLinkedList());

                    PnrCollectionUtil.setDefaultCardInfo(purchaseOrder.getPaymentInfo());
                } else if (purchaseOrder.isThreeDsPayment() && null != purchaseOrder.getAuthorizationResult()) {
                    validateAuthorizationForKiosk(purchaseOrderRQ, recordLocator, cartId);

                    authorizationResultList.add(purchaseOrder.getAuthorizationResult());
                } else {
                    if (PosType.KIOSK.toString().equalsIgnoreCase(purchaseOrderRQ.getPos())
                            && purchaseOrder.isPinPadNotPresent()) {

                        if (!formsOfPaymentWrapper.getCreditCardPaymentRequestList().isEmpty()) {
                            int maxRetries = 1;// 0 and 1 (two retries)
                            int retryAttempt = (null == purchaseOrder.getRetryCount()) ? 0 : purchaseOrder.getRetryCount();

                            //call cybersource
                            Address address = getAddresFromKioskId(purchaseOrderRQ, recordLocator, cartId);

                            try {
                                this.callCybersource(
                                        formsOfPaymentWrapper.getCreditCardPaymentRequestList(),
                                        address,
                                        purchaseOrderRQ,
                                        bookedTravelersEligibleForCheckin,
                                        pnr.getLegs().getCollection(),
                                        ItemsToBePaidType.UPGRADE,
                                        recordLocator,
                                        TimeUtil.getStrTime(purchaseOrderRQ.getTransactionTime(), "yyyy-MM-dd'T'HH:mm:ss"),
                                        retryAttempt,
                                        maxRetries,
                                        currencyCode,
                                        errorCauseWrapper,
                                        purchaseOrderRQ.getKioskId()
                                );

                                purchaseOrder.setThreeDsPayment(false);
                                purchaseOrder.setThreeDSRedirectURL(null);

                            } catch (Exception ex) {
                                LOG.error(ex.getMessage(), ex);

                                if (null != purchaseOrder.getThreeDSRedirectURL()) {
                                    for (CreditCardPaymentRequest creditCardPaymentRequest : formsOfPaymentWrapper.getCreditCardPaymentRequestList()) {
                                        creditCardPaymentRequest.setEmail(_3DS_EMAIL_TRIGGER);
                                    }
                                }

                            }
                        }
                    }

                    //form request for payment
                    /**
                     * TravelerAncillaryList is a collection to store the
                     * ancillaries added to this payment request
                     *
                     */
                    List<AbstractAncillary> abstractAncillaryList = new ArrayList<>();

                    authorizationResultList = paymentAuthorizationService.callAuthorization(
                            bookedTravelersEligibleForCheckin,
                            bookedLeg,
                            purchaseOrderRQ,
                            abstractAncillaryList,
                            ItemsToBePaidType.UPGRADE,
                            recordLocator,
                            cartId,
                            serviceCallList
                    );

                    for (AuthorizationResult authorizationResult : authorizationResultList) {

                        if (null != authorizationResult.getT3dsResult() && null != authorizationResult.getRedirectForm()) {
                            ThreeDsFormResponse threeDsFormResponse = new ThreeDsFormResponse();
                            threeDsFormResponse.setRedirectForm(authorizationResult.getRedirectForm());
                            threeDsFormResponse.setT3dsResult(authorizationResult.getT3dsResult());

                            return threeDsFormResponse;
                        }

                        if (AuthorizationPaymentResultType.APPROVED.toString().equalsIgnoreCase(authorizationResult.getResponseCode())) {
                            PaymentLinked paymentLinked = PnrCollectionUtil.addPaymentId(bookedTravelersEligibleForCheckin, abstractAncillaryList, authorizationResult, purchaseOrderRQ.getTransactionTime());
                            cartPNR.getPaymentLinkedList().addIfNotExist(paymentLinked);
                            cartPNRAfterCheckin.getPaymentLinkedList().addIfNotExist(paymentLinked);
                        } else {
                            if (PosType.WEB.toString().equalsIgnoreCase(purchaseOrderRQ.getPos())) {
                                LOG.error(LogUtil.getLogError(SERVICE, "AUTHORIZATION PAYMENT NOT APPROVED", recordLocator, cartId));
                                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.AUTHORIZATION_PAYMENT_NOT_APPROVED_WEB, ErrorType.AUTHORIZATION_PAYMENT_NOT_APPROVED_WEB.getFullDescription());
                            } else {
                                LOG.error(LogUtil.getLogError(SERVICE, "AUTHORIZATION PAYMENT NOT APPROVED", recordLocator, cartId));
                                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.AUTHORIZATION_PAYMENT_NOT_APPROVED, ErrorType.AUTHORIZATION_PAYMENT_NOT_APPROVED.getFullDescription());
                            }

                        }

                        aeService.addRemark(authorizationResult, recordLocator, cartId, serviceCallList);
                    }
                }

                //form request for emd
                List<UpgradeAncillary> upgradeAncillaryList;
                try {
                    upgradeAncillaryList = cartPNR.getUpgradeAncillaries().getCollection();
                } catch (Exception ex) {
                    upgradeAncillaryList = new ArrayList<>();
                }

                SabreSession sabreSession = null;
                try {
                    //upgrade process
                    sabreSession = aeService.getSession();

                    boolean forceSetParameters = true;
                    boolean closeSession = false;

                    commandService.setParameters(
                            sabreSession,
                            forceSetParameters,
                            purchaseOrderRQ.getStore(),
                            purchaseOrderRQ.getPos(),
                            serviceCallList
                    );

//                    GetReservationRS getReservationRS;
//                    getReservationRS = getReservationService.getReservationStateful(
//                            sabreSession.getSessionId(), recordLocator, cartId
//                    );
                    String command = "*" + recordLocator;
                    SabreCommandLLSRS sabreCommandLLSRS = commandService.executeCommand(
                            sabreSession, command, "SCREEN",
                            recordLocator, cartId,
                            serviceCallList
                    );

                    if (!sabreCommandLLSRS.getResponse().trim().startsWith(recordLocator)) {
                        throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNABLE_TO_LOAD_RESERVATION);
                    }

//                    if (null == getReservationRS) {
//                        throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNABLE_TO_LOAD_RESERVATION);
//                    }
                    Map<String, Segment> segmentIds;
                    segmentIds = PurchaseOrderUtil.getSegmentIdsWithUpgrade(
                            bookedTravelersEligibleForCheckin.get(0), bookedLeg.getSegments()
                    );

                    if (null != segmentIds && !segmentIds.isEmpty()) {
                        for (Map.Entry<String, Segment> entry : segmentIds.entrySet()) {
                            //String key = entry.getKey();
                            Segment value = entry.getValue();
                            int segmentId = Integer.valueOf(value.getSegmentNumber());

                            command = "WC" + segmentId + "F";
                            sabreCommandLLSRS = commandService.executeCommand(
                                    sabreSession, command, "SCREEN",
                                    recordLocator, cartId,
                                    serviceCallList
                            );

                            if (!sabreCommandLLSRS.getResponse().contains("OK - CLASS OF SERVICE CHANGED")) {
                                try {
                                    commandService.executeCommand(
                                            sabreSession, "I", "SCREEN",
                                            recordLocator, cartId,
                                            serviceCallList
                                    );
                                } catch (Exception ex) {
                                    LOG.error(LogUtil.getLogError(SERVICE, "Error executing command ignore", recordLocator, cartId, ex));
                                }

                                try {
                                    aeService.closeSession(sabreSession);
                                } catch (Exception ex) {
                                    LOG.error(LogUtil.getLogError(SERVICE, "Error closing session", recordLocator, cartId, ex));
                                }

                                //UNABLE TO BOOK A NEW CLASS
                                if (sabreCommandLLSRS.getResponse().contains(ErrorType.UNABLE_TO_BOOK_A_NEW_CLASS.getSearchableCode())) {
                                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNABLE_TO_BOOK_A_NEW_CLASS, "Itinerary: " + segmentId);
                                }

                                //NO ELIGIBLE ITIN
                                if (sabreCommandLLSRS.getResponse().contains(ErrorType.NO_ELIGIBLE_ITIN.getSearchableCode())) {
                                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.NO_ELIGIBLE_ITIN, "Itinerary: " + segmentId);
                                }

                                //TODO return an error
                                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.SEATS_ARE_NO_LONGER_AVAILABLE, "Itinerary: " + segmentId);
                            }
                        }
                    } else {
                        LOG.info("There is not segments with upgrades");
                    }

                    try {
                        commandService.executeCommand(
                                sabreSession, "6" + AGENT_NAME, "SCREEN",
                                recordLocator, cartId,
                                serviceCallList
                        );
                    } catch (Exception ex) {
                        LOG.error(LogUtil.getLogError(SERVICE, "Error executing command received from", recordLocator, cartId, ex));
                    }

                    try {
                        commandService.executeCommand(
                                sabreSession, "ER", "SCREEN",
                                recordLocator, cartId,
                                serviceCallList
                        );
                    } catch (Exception ex) {
                        LOG.error(LogUtil.getLogError(SERVICE, "Error executing command end and retrive", recordLocator, cartId, ex));
                    }

                    //create ae for upgrades
                    cartPNR.setTouched(true);
                    forceSetParameters = false;
                    aeService.addNewUpgradeAncillariesToReservation(
                            sabreSession,
                            bookedTravelersEligibleForCheckin,
                            bookedLeg,
                            recordLocator,
                            cartId,
                            store,
                            upgradeAncillaryList,
                            closeSession,
                            serviceCallList,
                            purchaseOrderRQ.getPos()
                    );
                    PnrCollectionUtil.copyTouchedTravelers(cartPNR.getTravelerInfo().getCollection(), cartPNRAfterCheckin.getTravelerInfo().getCollection());

                    try {
                        commandService.executeCommand(
                                sabreSession, "6" + AGENT_NAME, "SCREEN",
                                recordLocator, cartId,
                                serviceCallList
                        );
                    } catch (Exception ex) {
                        LOG.error(LogUtil.getLogError(SERVICE, "Error executing command received from", recordLocator, cartId, ex));
                    }

                    try {
                        commandService.executeCommand(
                                sabreSession, "E", "SCREEN",
                                recordLocator, cartId,
                                serviceCallList
                        );
                    } catch (Exception ex) {
                        LOG.error(LogUtil.getLogError(SERVICE, "Error executing command end and retrive", recordLocator, cartId, ex));
                    }

                    //form request for emd
                    GetTicketingDocumentRS ticketingDocumentRS = null;
                    CollectMiscFeeRQ collectMiscFeeRQ = emdService.getCollectMiscFeeRQForUpgrades(
                            bookedTravelersEligibleForCheckin,
                            bookedLeg,
                            purchaseOrderRQ,
                            authorizationResultList,
                            recordLocator,
                            cartId,
                            upgradeAncillaryList,
                            ticketingDocumentRS,
                            serviceCallList
                    );

                    //LOG.info(LogUtil.getLogInfo(SERVICE, recordLocator, cartId, "Collect Misc Fee RQ\n" + GSON.toJson(collectMiscFeeRQ) + "\n"));
                    if (null != collectMiscFeeRQ
                            && null != collectMiscFeeRQ.getFees()
                            && !collectMiscFeeRQ.getFees().isEmpty()) {

                        boolean checkForRetry;
                        CollectMiscFeeRS collectMiscFeeRS = null;
                        try {
                            checkForRetry = true;

                            //call issue emds
                            collectMiscFeeRS = emdService.issueEmdCodes(
                                    sabreSession,
                                    collectMiscFeeRQ,
                                    purchaseOrderRQ,
                                    "Collect Misc Fee",
                                    closeSession,
                                    forceSetParameters,
                                    checkForRetry,
                                    serviceCallList
                            );
                        } catch (RetryCheckinException ex) {
                            checkForRetry = false;
                            boolean retry = true;

                            if (ErrorType.INVALID_COUPON_NUMBER.equals(ex.getErrorCodeType())) {
                                LOG.info("INVALID COUPON NUMBER: changing coupon");
                                retry = emdService.changeCouponNumber(
                                        collectMiscFeeRQ, ticketingDocumentRS,
                                        recordLocator, cartId, serviceCallList,
                                        purchaseOrderRQ.getPos()
                                );
                            }

                            if (retry) //call issue emds
                            {
                                collectMiscFeeRS = emdService.issueEmdCodes(
                                        sabreSession, collectMiscFeeRQ, purchaseOrderRQ,
                                        "Collect Misc Fee", closeSession,
                                        forceSetParameters, checkForRetry,
                                        serviceCallList
                                );
                            }

                        }

                        //ref to get emds
                        int groupEmdsBy = 3;
                        IssuedEmds issuedEmds = new IssuedEmds(groupEmdsBy);
                        if (null != collectMiscFeeRS && null != collectMiscFeeRS.getFees()) {
                            PnrCollectionUtil.changeUnpaidAncillariesToPaid(
                                    bookedTravelersEligibleForCheckin, collectMiscFeeRS.getFees(), issuedEmds
                            );
                        }

                        PnrCollectionUtil.copyTouchedTravelers(
                                cartPNR.getTravelerInfo().getCollection(),
                                cartPNRAfterCheckin.getTravelerInfo().getCollection()
                        );

                        LOG.info(LogUtil.getLogDebug(SERVICE, recordLocator, cartId, "Issued emds: " + issuedEmds.toString()));

                        PectabReceipt pectabReceipt = getPectabReceipt(
                                authorizationResultList,
                                purchaseOrderRQ,
                                issuedEmds,
                                recordLocator,
                                cartId,
                                currencyCode
                        );

                        if (null != pectabReceipt) {
                            cartPNRAfterCheckin.getPectabReceiptList().addIfNotExist(pectabReceipt);
                            cartPNR.getPectabReceiptList().addIfNotExist(pectabReceipt);
                        } else {
                            LOG.error("No receipt was generated");
                        }

                        LOG.info(LogUtil.getLogDebug(SERVICE, recordLocator, cartId, "Pnr Collection After Issue EMDs\n" + GSON.toJson(pnrCollection) + "\n"));
                        LOG.info(LogUtil.getLogDebug(SERVICE, recordLocator, cartId, "Pnr Collection After Issue EMDs\n" + GSON.toJson(pnrCollectionAfterCheckin) + "\n"));
                    } else {
                        LOG.error(LogUtil.getLogError(SERVICE, "There were no fees (possible cause action code different that HD)", recordLocator, cartId));
                        throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.NO_UNFULFILLED_AE_ITEM);
                    }

                    try {
                        aeService.closeSession(sabreSession);
                        LOG.info(LogUtil.getLogInfo(SERVICE, "Closing session", recordLocator, cartId));
                    } catch (Exception ex) {
                        LOG.error(LogUtil.getLogError(SERVICE, "Error closing session", recordLocator, cartId, ex));
                    }

                } catch (Exception ex) {
                    LOG.error(ex.getMessage(), ex);
                    try {
                        aeService.closeSession(sabreSession);
                        LOG.info(LogUtil.getLogInfo(SERVICE, "Closing session", recordLocator, cartId));
                    } catch (Exception ex1) {
                        LOG.error(LogUtil.getLogError(SERVICE, "Error closing session", recordLocator, cartId, ex1));
                    }

                    throw ex;
                }

            } else if (areThereUnpaidItems) {
                LOG.error(LogUtil.getLogError(SERVICE, "There are unpaid upgrade items, you need to specify a payment method with an amount", recordLocator, cartId));
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PAYMENT_METHOD_IS_REQUIRED, "There are unpaid upgrade items, you need to specify a payment method with an amount");
            } else {//nothing to be paid
                LOG.info(LogUtil.getLogInfo(SERVICE, "Nothing to be paid", recordLocator, cartId));
            }

            boolean checkForCheckin = false;
            CheckinConfirmation checkinConfirmation = PurchaseOrderUtil.getCheckInResponse(
                    purchaseOrder,
                    null,
                    pnr.getCarts(),
                    pnr.getLegs(),
                    recordLocator,
                    purchaseOrderRQ.getTransactionTime(),
                    purchaseOrderRQ.getPos(),
                    legCode,
                    checkForCheckin
            );

            checkinConfirmation.setWarnings(warningCollection);

            startTime = Calendar.getInstance().getTimeInMillis();
            updateOnSuccess(pnrCollection, pnrCollectionAfterCheckin, recordLocator, cartId, false, purchaseOrderRQ);
            TimeMonitorUtil.addCallToMonitor(serviceCallList, "Update pnr collections before and after checkin", startTime);

            return checkinConfirmation;

        } catch (GenericException ex) {
            updateOnError(pnrCollection, pnrCollectionAfterCheckin, recordLocator, cartId);
            throw ex;
        } catch (Exception ex) {
            updateOnError(pnrCollection, pnrCollectionAfterCheckin, recordLocator, cartId);
            LOG.error(LogUtil.getLogError(SERVICE, "Doing Purchase Upgrade", recordLocator, cartId, ex));
            throw ex;
        }

    }

    /**
     * @param purchaseOrderRQ
     * @param pnrCollection
     * @param pnrCollectionAfterCheckin
     * @param bookedTravelersEligibleForCheckin
     * @param serviceCallList
     * @return
     * @throws Exception
     */
    private CheckinConfirmationBase doPurchasePostCheckin(
            PurchaseOrderRQ purchaseOrderRQ,
            PNRCollection pnrCollection,
            PNRCollection pnrCollectionAfterCheckin,
            List<BookedTraveler> bookedTravelersEligibleForCheckin,
            List<ServiceCall> serviceCallList
    ) throws Exception {

        long startTime;
        ErrorCauseWrapper errorCauseWrapper = new ErrorCauseWrapper();
        PurchaseOrder purchaseOrder = purchaseOrderRQ.getPurchaseOrder();
        String cartId = purchaseOrder.getCartId();
        String store = purchaseOrderRQ.getStore();
        PNR pnr = pnrCollection.getPNRByCartId(cartId);
        String recordLocator = pnr.getPnr();
        CartPNR cartPNR = pnr.getCartPNRByCartId(cartId);
        String legCode = cartPNR.getLegCode();
        BookedLeg bookedLeg = pnr.getBookedLegByLegCode(legCode);
        CartPNR cartPNRAfterCheckin = pnrCollectionAfterCheckin.getCartPNRByRecordLocatorAndLegCode(recordLocator, legCode);

        //get the total present in cart
        BigDecimal totalInPurchase = BigDecimal.ZERO;
        BigDecimal totalInCart;
        totalInCart = cartPNR.getMeta().getTotal().getCurrency().getTotal();
        if (null == totalInCart) {
            totalInCart = BigDecimal.ZERO;
        }

        purchaseOrder.setPaymentAmount(cartPNR.getMeta().getTotal().getCurrency());

        String currencyCode;
        try {
            currencyCode = cartPNR.getMeta().getTotal().getCurrency().getCurrencyCode();
        } catch (Exception ex) {
            //default currency code
            currencyCode = CurrencyType.MXN.toString();
        }

        boolean areThereUnpaidItems = PurchaseOrderUtil.areThereUnpaidItems(bookedTravelersEligibleForCheckin);
        boolean areThereWaiveItems = PurchaseOrderUtil.areThereWaiveItems(bookedTravelersEligibleForCheckin);
        boolean areThereWaiveSeats = PurchaseOrderUtil.areThereWaiveSeats(bookedTravelersEligibleForCheckin);

        boolean isTherePaymentMethod = (null != purchaseOrder.getPaymentInfo()
                && null != purchaseOrder.getPaymentInfo().getCollection()
                && !purchaseOrder.getPaymentInfo().getCollection().isEmpty());

        if (areThereUnpaidItems && !isTherePaymentMethod) {
            LOG.error(LogUtil.getLogError(SERVICE, " There are unpaid items, you need to specify a payment method", recordLocator, cartId));
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PAYMENT_METHOD_IS_REQUIRED, "There are unpaid items, you need to specify a payment method");
        }

        FormsOfPaymentWrapper formsOfPaymentWrapper;
        formsOfPaymentWrapper = new FormsOfPaymentWrapper(
                purchaseOrder.getPaymentInfo()
        );

        if (areThereUnpaidItems) {

            //valid forms of payment on checkin are credit card and uatp card
            boolean isThereValidPaymentMethod = !formsOfPaymentWrapper.getCreditCardPaymentRequestList().isEmpty()
                    || !formsOfPaymentWrapper.getUatpPaymentRequestList().isEmpty() || !formsOfPaymentWrapper.getVisaCheckoutPaymentRequestList().isEmpty();

            if (areThereUnpaidItems && !isThereValidPaymentMethod) {
                LOG.error(LogUtil.getLogError(SERVICE, " There are unpaid items, you need to specify a valid payment method", recordLocator, cartId));
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PAYMENT_METHOD_IS_REQUIRED, "There are unpaid items, you need to specify a valid payment method");
            }

            //compare the total present in the payment info request
            totalInPurchase = PurchaseOrderUtil.getTotal(formsOfPaymentWrapper, totalInCart);

            if (0 != totalInCart.compareTo(totalInPurchase)) {
                LOG.error(LogUtil.getLogError(SERVICE, "Prices on cart and purchase order does not match", recordLocator, cartId));
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PRICES_NOT_MATCH, ErrorType.PRICES_NOT_MATCH.getFullDescription());
            }
        }

        try {
            WarningCollection warningCollection = new WarningCollection();

            boolean paymentWasEmmited = false;

            Set<BookedTraveler> passengersSuccessfulCheckedIn = new HashSet<>();
            List<ACSCheckInPassengerRSACS> acsCheckInPassengerRSACSList = new ArrayList<>();

            if (areThereUnpaidItems && 0 < totalInPurchase.compareTo(BigDecimal.ZERO)) {
                //call authorization payment only if the request comes from KIOSK
                List<AuthorizationResult> authorizationResultList = new ArrayList<>();

                if (PosType.KIOSK.toString().equalsIgnoreCase(purchaseOrderRQ.getPos())
                        && !purchaseOrderRQ.getPurchaseOrder().isPinPadNotPresent()) {
                    validateAuthorizationForKiosk(purchaseOrderRQ, recordLocator, cartId);

                    authorizationResultList.add(purchaseOrder.getAuthorizationResult());

                    cartPNR.setPectabReceiptList(cartPNRAfterCheckin.getPectabReceiptList());
                    cartPNR.setPaymentLinkedList(cartPNRAfterCheckin.getPaymentLinkedList());

                    PnrCollectionUtil.setDefaultCardInfo(purchaseOrder.getPaymentInfo());
                } else if (purchaseOrder.isThreeDsPayment() && null != purchaseOrder.getAuthorizationResult()) {
                    validateAuthorizationForKiosk(purchaseOrderRQ, recordLocator, cartId);

                    authorizationResultList.add(purchaseOrder.getAuthorizationResult());
                } else {
                    if (PosType.KIOSK.toString().equalsIgnoreCase(purchaseOrderRQ.getPos())
                            && purchaseOrder.isPinPadNotPresent()) {

                        if (!formsOfPaymentWrapper.getCreditCardPaymentRequestList().isEmpty()) {
                            int maxRetries = 1;// 0 and 1 (two retries)
                            int retryAttempt = (null == purchaseOrder.getRetryCount()) ? 0 : purchaseOrder.getRetryCount();

                            //call cybersource
                            Address address = getAddresFromKioskId(purchaseOrderRQ, recordLocator, cartId);

                            try {
                                this.callCybersource(
                                        formsOfPaymentWrapper.getCreditCardPaymentRequestList(),
                                        address,
                                        purchaseOrderRQ,
                                        bookedTravelersEligibleForCheckin,
                                        pnr.getLegs().getCollection(),
                                        ItemsToBePaidType.SEAT_AND_BAGGAGE,
                                        recordLocator,
                                        TimeUtil.getStrTime(purchaseOrderRQ.getTransactionTime(), "yyyy-MM-dd'T'HH:mm:ss"),
                                        retryAttempt,
                                        maxRetries,
                                        currencyCode,
                                        errorCauseWrapper,
                                        purchaseOrderRQ.getKioskId()
                                );

                                purchaseOrder.setThreeDsPayment(false);
                                purchaseOrder.setThreeDSRedirectURL(null);

                            } catch (Exception ex) {
                                LOG.error(ex.getMessage(), ex);

                                if (null != purchaseOrder.getThreeDSRedirectURL()) {
                                    for (CreditCardPaymentRequest creditCardPaymentRequest : formsOfPaymentWrapper.getCreditCardPaymentRequestList()) {
                                        creditCardPaymentRequest.setEmail(_3DS_EMAIL_TRIGGER);
                                    }
                                }

                            }
                        }
                    }

                    //form request for payment
                    /**
                     * TravelerAncillaryList is a collection to store the
                     * ancillaries added to this payment request
                     *
                     */
                    List<AbstractAncillary> abstractAncillaryList = new ArrayList<>();

                    authorizationResultList = paymentAuthorizationService.callAuthorization(
                            bookedTravelersEligibleForCheckin,
                            bookedLeg,
                            purchaseOrderRQ,
                            abstractAncillaryList,
                            ItemsToBePaidType.SEAT_AND_BAGGAGE,
                            recordLocator,
                            cartId,
                            serviceCallList
                    );

                    for (AuthorizationResult authorizationResult : authorizationResultList) {

                        if (null != authorizationResult.getT3dsResult() && null != authorizationResult.getRedirectForm()) {
                            ThreeDsFormResponse threeDsFormResponse = new ThreeDsFormResponse();
                            threeDsFormResponse.setRedirectForm(authorizationResult.getRedirectForm());
                            threeDsFormResponse.setT3dsResult(authorizationResult.getT3dsResult());

                            return threeDsFormResponse;
                        }

                        if (AuthorizationPaymentResultType.APPROVED.toString().equalsIgnoreCase(authorizationResult.getResponseCode())) {
                            PaymentLinked paymentLinked = PnrCollectionUtil.addPaymentId(bookedTravelersEligibleForCheckin, abstractAncillaryList, authorizationResult, purchaseOrderRQ.getTransactionTime());
                            cartPNR.getPaymentLinkedList().addIfNotExist(paymentLinked);
                            cartPNRAfterCheckin.getPaymentLinkedList().addIfNotExist(paymentLinked);
                        } else {
                            if (PosType.WEB.toString().equalsIgnoreCase(purchaseOrderRQ.getPos())) {
                                LOG.error(LogUtil.getLogError(SERVICE, "AUTHORIZATION PAYMENT NOT APPROVED", recordLocator, cartId));
                                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.AUTHORIZATION_PAYMENT_NOT_APPROVED_WEB, ErrorType.AUTHORIZATION_PAYMENT_NOT_APPROVED_WEB.getFullDescription());
                            } else {
                                LOG.error(LogUtil.getLogError(SERVICE, "AUTHORIZATION PAYMENT NOT APPROVED", recordLocator, cartId));
                                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.AUTHORIZATION_PAYMENT_NOT_APPROVED, ErrorType.AUTHORIZATION_PAYMENT_NOT_APPROVED.getFullDescription());
                            }

                        }

                        aeService.addRemark(authorizationResult, recordLocator, cartId, serviceCallList);
                    }
                }

                //remove ancillaries for old seat
                aeService.removeSeatAncillariesFromReservationToChangeSeats(
                        bookedTravelersEligibleForCheckin,
                        recordLocator, cartId,
                        serviceCallList
                );

                //add new ancillaries in shopping cart to reservation
                aeService.addNewAncillariesToReservation(
                        purchaseOrderRQ,
                        bookedTravelersEligibleForCheckin,
                        bookedLeg,
                        pnr.getGlobalMarketFlight(),
                        recordLocator,
                        cartId,
                        store,
                        serviceCallList
                );
                PnrCollectionUtil.copyTouchedTravelers(cartPNR.getTravelerInfo().getCollection(), cartPNRAfterCheckin.getTravelerInfo().getCollection());

                LOG.debug(LogUtil.getLogDebug(SERVICE, recordLocator, cartId, "Pnr Collection After Update Reservation\n" + GSON.toJson(pnrCollection) + "\n"));

                //form request for emd
                GetTicketingDocumentRS ticketingDocumentRS = null;
                CollectMiscFeeRQ collectMiscFeeRQ = emdService.getCollectMiscFeeRQ(
                        bookedTravelersEligibleForCheckin,
                        bookedLeg,
                        purchaseOrderRQ,
                        authorizationResultList,
                        ticketingDocumentRS,
                        recordLocator,
                        cartId,
                        serviceCallList
                );

                //LOG.debug(LogUtil.getLogDebug(SERVICE, recordLocator, cartId, "Collect Misc Fee RQ\n" + GSON.toJson(collectMiscFeeRQ) + "\n"));
                if (null != collectMiscFeeRQ && null != collectMiscFeeRQ.getFees() && !collectMiscFeeRQ.getFees().isEmpty()) {

                    changeSeatsPostCheckin(
                            bookedTravelersEligibleForCheckin,
                            bookedLeg,
                            acsCheckInPassengerRSACSList,
                            passengersSuccessfulCheckedIn
                    );

                    boolean checkForRetry;
                    CollectMiscFeeRS collectMiscFeeRS = null;
                    try {
                        checkForRetry = true;
                        //call issue emds
                        collectMiscFeeRS = emdService.issueEmdCodes(
                                collectMiscFeeRQ, purchaseOrderRQ,
                                "Collect Misc Fee", checkForRetry,
                                serviceCallList
                        );
                    } catch (RetryCheckinException ex) {
                        checkForRetry = false;
                        boolean retry = true;

                        if (ErrorType.INVALID_COUPON_NUMBER.equals(ex.getErrorCodeType())) {
                            LOG.info("INVALID COUPON NUMBER: changing coupon");
                            retry = emdService.changeCouponNumber(
                                    collectMiscFeeRQ, ticketingDocumentRS,
                                    recordLocator, cartId, serviceCallList,
                                    purchaseOrderRQ.getPos()
                            );
                        }

                        if (retry) //call issue emds
                        {
                            collectMiscFeeRS = emdService.issueEmdCodes(
                                    collectMiscFeeRQ, purchaseOrderRQ,
                                    "Collect Misc Fee", checkForRetry,
                                    serviceCallList
                            );
                        }
                    }

                    paymentWasEmmited = true;

                    //ref to get emds
                    int groupEmdsBy = 3;
                    IssuedEmds issuedEmds = new IssuedEmds(groupEmdsBy);
                    if (null != collectMiscFeeRS && null != collectMiscFeeRS.getFees()) {
                        PnrCollectionUtil.changeUnpaidAncillariesToPaid(
                                bookedTravelersEligibleForCheckin, collectMiscFeeRS.getFees(), issuedEmds
                        );
                    }

                    PnrCollectionUtil.copyTouchedTravelers(
                            cartPNR.getTravelerInfo().getCollection(),
                            cartPNRAfterCheckin.getTravelerInfo().getCollection()
                    );

                    LOG.info(LogUtil.getLogDebug(SERVICE, recordLocator, cartId, "Issued emds: " + issuedEmds.toString()));

                    PectabReceipt pectabReceipt = getPectabReceipt(
                            authorizationResultList,
                            purchaseOrderRQ,
                            issuedEmds,
                            recordLocator,
                            cartId,
                            currencyCode
                    );

                    if (null != pectabReceipt) {
                        cartPNRAfterCheckin.getPectabReceiptList().addIfNotExist(pectabReceipt);
                        cartPNR.getPectabReceiptList().addIfNotExist(pectabReceipt);
                    } else {
                        LOG.error("No receipt was generated");
                    }

                    //LOG.info(LogUtil.getLogDebug(SERVICE, recordLocator, cartId, "Pnr Collection After Issue EMDs\n" + GSON.toJson(pnrCollection) + "\n"));
                    //LOG.info(LogUtil.getLogDebug(SERVICE, recordLocator, cartId, "Pnr Collection After Issue EMDs\n" + GSON.toJson(pnrCollectionAfterCheckin) + "\n"));
                } else {
                    LOG.error(LogUtil.getLogDebug(SERVICE, recordLocator, cartId, "There was not fees (possible cause action code different that HD)\n"));
                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.NO_UNFULFILLED_AE_ITEM);
                }

            } else if (areThereWaiveItems) {
                //add new ancillaries in shopping cart to reservation
                aeService.addNewAncillariesToReservation(
                        purchaseOrderRQ,
                        bookedTravelersEligibleForCheckin,
                        bookedLeg,
                        pnr.getGlobalMarketFlight(),
                        recordLocator,
                        cartId,
                        store,
                        serviceCallList
                );

                changeSeatsPostCheckin(
                        bookedTravelersEligibleForCheckin,
                        bookedLeg,
                        acsCheckInPassengerRSACSList,
                        passengersSuccessfulCheckedIn
                );

                PnrCollectionUtil.copyTouchedTravelers(
                        cartPNR.getTravelerInfo().getCollection(),
                        cartPNRAfterCheckin.getTravelerInfo().getCollection()
                );
            } else if (areThereUnpaidItems) {
                LOG.error(LogUtil.getLogError(SERVICE, "There are unpaid items, you need to specify a payment method with an amount", recordLocator, cartId));
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PAYMENT_METHOD_IS_REQUIRED, "There are unpaid items, you need to specify a payment method with an amount");
            } else {//nothing to be paid
                LOG.info(LogUtil.getLogInfo(SERVICE, recordLocator, cartId, "Nothing to be paid"));
            }

            CheckinConfirmation checkinConfirmation;
            boolean checkForCheckin = true;
            if (acsCheckInPassengerRSACSList.isEmpty()) {
                cartPNR.setTouched(true);

                checkinConfirmation = PurchaseOrderUtil.getCheckInResponse(
                        purchaseOrder, null, pnr.getCarts(),
                        pnr.getLegs(), recordLocator, purchaseOrderRQ.getTransactionTime(),
                        purchaseOrderRQ.getPos(), legCode, checkForCheckin
                );

                checkinConfirmation.setWarnings(warningCollection);

            } else {
                //this iteration is because sabre do checkin for all open flights
                List<CartPNR> carts = pnrCollection.getCartsPNRByRecordLocator(recordLocator);
                if (null != carts) {
                    for (CartPNR cartPNRLocal : carts) {

                        BookedLeg bookedLegLocal = pnrCollection.getBookedLegByLegCode(cartPNRLocal.getLegCode());

                        boolean touched = PnrCollectionUtil.updateAfterSuccessCheckin(
                                passengersSuccessfulCheckedIn, acsCheckInPassengerRSACSList,
                                cartPNRLocal.getTravelerInfo().getCollection(),
                                bookedLegLocal,
                                purchaseOrderRQ.getPos(),
                                recordLocator,
                                pnr.isTsaRequired(),
                                warningCollection
                        );
                        cartPNRLocal.setTouched(touched);

                        CartPNR cartPNRPersisted;
                        cartPNRPersisted = pnrCollectionAfterCheckin.getCartPNRByRecordLocatorAndLegCode(
                                recordLocator, cartPNRLocal.getLegCode()
                        );

                        if (null != cartPNRPersisted) {
                            cartPNRPersisted.setTouched(touched);

                            PnrCollectionUtil.copyCheckinInfoFromCheckedInTravelers(
                                    passengersSuccessfulCheckedIn,
                                    cartPNRLocal.getTravelerInfo().getCollection(),
                                    cartPNRPersisted.getTravelerInfo().getCollection()
                            );

                        }
                    }
                }

                List<PectabBoardingPass> pectabDataList = PurchaseOrderUtil.getPectacBoardingPassList(acsCheckInPassengerRSACSList, recordLocator);

                if (null != pectabDataList && !pectabDataList.isEmpty()) {
                    cartPNRAfterCheckin.getPectabBoardingPassList().addAllIfNotExist(pectabDataList);
                    cartPNR.getPectabBoardingPassList().addAllIfNotExist(pectabDataList);
                }

                LOG.info(LogUtil.getLogDebug(SERVICE, recordLocator, cartId, "Pnr Collection After Check-In\n" + GSON.toJson(pnrCollection) + "\n"));

                checkinConfirmation = PurchaseOrderUtil.getCheckInResponse(
                        purchaseOrder, acsCheckInPassengerRSACSList,
                        pnr.getCarts(), pnr.getLegs(), recordLocator, purchaseOrderRQ.getTransactionTime(),
                        purchaseOrderRQ.getPos(), legCode, checkForCheckin
                );

                checkinConfirmation.setWarnings(warningCollection);
            }

            startTime = Calendar.getInstance().getTimeInMillis();
            updateOnSuccess(pnrCollection, pnrCollectionAfterCheckin, recordLocator, cartId, false, purchaseOrderRQ);
            TimeMonitorUtil.addCallToMonitor(serviceCallList, "Update pnr collections before and after checkin", startTime);

            Map<String, String> mappedPolicyChubb = callChubbPolicy(pnr, purchaseOrderRQ, cartId);

            checkinConfirmation.setSendPolicyChubb(mappedPolicyChubb);
            //checkinConfirmation.setSendPolicyChubb(null);

            return checkinConfirmation;

        } catch (GenericException ex) {
            updateOnError(pnrCollection, pnrCollectionAfterCheckin, recordLocator, cartId); //
            //
            throw ex;
        } catch (Exception ex) {
            updateOnError(pnrCollection, pnrCollectionAfterCheckin, recordLocator, cartId);
            LOG.error(LogUtil.getLogError(SERVICE, "Doing Purchase Post Checkin", recordLocator, cartId, ex));
            throw ex;
        }

    }

    private void changeSeatsPostCheckin(
            List<BookedTraveler> bookedTravelersEligibleForCheckin,
            BookedLeg bookedLeg,
            List<ACSCheckInPassengerRSACS> acsCheckInPassengerRSACSList,
            Set<BookedTraveler> passengersSuccessfulCheckedIn
    ) throws Exception {
        //change seat for passengers already checked
        for (BookedTraveler bookedTraveler : bookedTravelersEligibleForCheckin) {
            for (AbstractSegmentChoice abstractSegmentChoice : bookedTraveler.getSegmentChoices().getCollection()) {

                if (abstractSegmentChoice instanceof SegmentChoice) {
                    SegmentChoice segmentChoice = (SegmentChoice) abstractSegmentChoice;
                    String segmentCode = segmentChoice.getSegmentCode();

                    String oldSeat;
                    String newSeat = segmentChoice.getSeat().getCode();

                    AbstractSegmentChoice originalAbstractSegmentChoice = bookedTraveler.getOriginalSegmentChoicesMap().get(segmentCode);

                    if (null != originalAbstractSegmentChoice) {
                        oldSeat = originalAbstractSegmentChoice.getSeat().getCode();

                        if (null != oldSeat
                                && null != newSeat
                                && !oldSeat.trim().isEmpty()
                                && !newSeat.trim().isEmpty()
                                && !oldSeat.equalsIgnoreCase(newSeat)) {

                            BookedSegment bookedSegment = bookedLeg.getBookedSegment(segmentCode);

                            if (null != bookedSegment) {
                                BookingClass bookingClass = bookedTraveler.getBookingClasses().getBookingClass(segmentCode);

                                if (null != bookingClass) {

                                    ACSSeatChangeRSACS acsSeatChangeRSACS = seatsService.changeSeatChecked(
                                            bookedSegment, bookingClass, oldSeat, newSeat
                                    );

                                    ACSCheckInPassengerRSACS acsCheckInPassengerRSACS;
                                    acsCheckInPassengerRSACS = PnrCollectionUtil.transformCheckInPassengerRS(acsSeatChangeRSACS);
                                    acsCheckInPassengerRSACSList.add(acsCheckInPassengerRSACS);
                                    passengersSuccessfulCheckedIn.add(bookedTraveler);

                                    boolean removed = bookedTraveler.getOriginalSegmentChoicesMap().remove(
                                            segmentCode
                                    );

                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @param purchaseOrderRQ
     * @param serviceCallList
     * @return
     * @throws Exception
     */
    public CheckinConfirmationBase doPurchase(PurchaseOrderRQ purchaseOrderRQ, List<ServiceCall> serviceCallList) throws Exception {

        //the cartId is required in the required
        PurchaseOrder purchaseOrder = purchaseOrderRQ.getPurchaseOrder();

        if (null == purchaseOrder.getOperation()) {
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.OPERATION_NOT_SUPPORTED, "Invalid operation field");
        }

        String cartId = purchaseOrder.getCartId();
        LOG.debug(LogUtil.getLogDebug(SERVICE, null, cartId, "Doing purchase"));

        long startTime = Calendar.getInstance().getTimeInMillis();
        PNRCollection pnrCollection;
        try {
            pnrCollection = updatePnrCollectionService.getPNRCollection(cartId, purchaseOrderRQ.getPos());
            if (null != pnrCollection.getStore() && !pnrCollection.getStore().trim().isEmpty()) {
                purchaseOrderRQ.setStore(pnrCollection.getStore());
            }

            if (null != pnrCollection.getPos() && !pnrCollection.getPos().trim().isEmpty()) {
                purchaseOrderRQ.setPos(pnrCollection.getPos());
            }
        } catch (GenericException ex) {
            ex.setWasLogged(true);
            logOnDatabaseService.logPreCheckinOnError(purchaseOrderRQ, cartId, null, ex);
            throw ex;
        }

        TimeMonitorUtil.addCallToMonitor(serviceCallList, "DATABASE, get pnr collection", startTime);

        LOG.debug(LogUtil.getLogDebug(SERVICE, null, cartId, "Finding PNR by cartid " + cartId));
        PNR pnr = pnrCollection.getPNRByCartId(cartId);
        if (null == pnr) {
            LOG.error(LogUtil.getLogError(SERVICE, null, cartId, "Pnr not found"));
            GenericException ex = new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION, "Pnr not found");
            ex.setWasLogged(true);
            logOnDatabaseService.logPreCheckinOnError(purchaseOrderRQ, cartId, null, ex);
            throw ex;
        }

        String recordLocator = pnr.getPnr();
        if (null == recordLocator || recordLocator.trim().isEmpty()) {
            LOG.error(LogUtil.getLogError(SERVICE, "Pnr not found", null, cartId));

            GenericException ex = new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION, "Pnr not found");
            ex.setWasLogged(true);
            logOnDatabaseService.logPreCheckinOnError(purchaseOrderRQ, cartId, null, ex);
            throw ex;
        }

        //just copy the record locator to be used in log
        purchaseOrderRQ.setRecordLocator(recordLocator);

        LOG.debug(LogUtil.getLogDebug(SERVICE, recordLocator, cartId, "Finding CartPNR by cartid " + cartId));
        CartPNR cartPNR = pnr.getCartPNRByCartId(cartId);
        if (null == cartPNR) {
            LOG.error(LogUtil.getLogError(SERVICE, "Cart not found", recordLocator, cartId));
            GenericException ex = new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION, "Cart not found");
            ex.setWasLogged(true);
            logOnDatabaseService.logPreCheckinOnError(purchaseOrderRQ, cartId, recordLocator, ex);
            throw ex;
        }

        String legCode = cartPNR.getLegCode();
        BookedLeg bookedLeg = pnr.getBookedLegByLegCode(legCode);
        if (null == bookedLeg) {
            LOG.error(LogUtil.getLogError(SERVICE, "BookedLeg not found: " + legCode, recordLocator, cartId));
            GenericException ex = new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION, "BookedLeg not found");
            ex.setWasLogged(true);
            logOnDatabaseService.logPreCheckinOnError(purchaseOrderRQ, cartId, recordLocator, ex);
            throw ex;
        }

        CheckinConfirmationBase checkinConfirmationBase = null;

        if (cartPNR.isSeamlessCheckin()) {
            checkinConfirmationBase = seamlessCheckinService.doCheckin(purchaseOrderRQ, cartPNR, bookedLeg, pnr.isTsaRequired());

            updateOnSuccess(pnrCollection, null, recordLocator, cartId, false, purchaseOrderRQ);
        } else {

            covidRestrictionStorageService.saveCovidRestrictionForm(recordLocator, cartPNR, bookedLeg);

            //create or update pnr collection after checkin
            startTime = Calendar.getInstance().getTimeInMillis();
            boolean create = true;
            boolean mustBeAsynchronous = true;
            PNRCollection pnrCollectionAfterCheckin;
            pnrCollectionAfterCheckin = updatePnrCollectionService.createOrUpdatePnrCollectionAfterCheckin(
                    pnrCollection, recordLocator, cartId, create, mustBeAsynchronous
            );
            TimeMonitorUtil.addCallToMonitor(serviceCallList, "DATABASE, create or update pnr collection after checkin", startTime);

            CartPNR cartPNRAfterCheckin = pnrCollectionAfterCheckin.getCartPNRByRecordLocatorAndLegCode(recordLocator, legCode);
            if (null == cartPNRAfterCheckin) {
                LOG.error(LogUtil.getLogError(SERVICE, "Cart not found", recordLocator, cartId));
                GenericException ex = new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION, "Cart not found in after checkin collection");
                ex.setWasLogged(true);
                logOnDatabaseService.logPreCheckinOnError(purchaseOrderRQ, cartId, recordLocator, ex);
                throw ex;
            }

            //check profile
            loadProfileFormOfPayments(
                    new FormsOfPaymentWrapper(
                            purchaseOrderRQ.getPurchaseOrder().getPaymentInfo()
                    ),
                    purchaseOrderRQ.getPos()
            );

            //check for 3ds
            if (null != purchaseOrderRQ.getPurchaseOrder().getThreeDSecureRequest()) {
                purchaseOrderRQ.getPurchaseOrder().setThreeDsPayment(true);
                AuthorizationResult auth = new AuthorizationResult();
                auth.setPaymentId(purchaseOrderRQ.getPurchaseOrder().getThreeDSecureRequest().getPaymentRef());
                auth.setResponseCode(purchaseOrderRQ.getPurchaseOrder().getThreeDSecureRequest().getResponseCode());
                auth.setApprovalCode(purchaseOrderRQ.getPurchaseOrder().getThreeDSecureRequest().getApprovalCode());
                purchaseOrderRQ.getPurchaseOrder().setAuthorizationResult(auth);
            }

            setMissingBillingEmailAddress(purchaseOrder, cartPNR);

            //This object will be a reference to the original collection
            //any change on this will change the original collection and vice versa
            List<BookedTraveler> bookedTravelersRequested = FilterPassengersUtil.getRequestedForCheckin( cartPNR.getTravelerInfo().getCollection() );

            if (null == bookedTravelersRequested || bookedTravelersRequested.isEmpty()) {
                LOG.error(LogUtil.getLogError(SERVICE, "Not passengers selected", recordLocator, cartId));
                GenericException ex = new GenericException(Response.Status.BAD_REQUEST, ErrorType.NO_PASSENGERS_SELECTED_FOR_CHECKIN, "Not passengers selected");
                ex.setWasLogged(true);
                logOnDatabaseService.logPreCheckinOnError(purchaseOrderRQ, cartId, recordLocator, ex);
                throw ex;
            }

            switch (purchaseOrder.getOperation()) {
                case UPGRADE:
                    checkinConfirmationBase = doPurchaseUpgrade(
                            purchaseOrderRQ,
                            pnrCollection,
                            pnrCollectionAfterCheckin,
                            bookedTravelersRequested,
                            serviceCallList
                    );
                    break;
                case EXTRAWEIGHT:
                    checkinConfirmationBase = doPurchaseExtraWeight(
                            purchaseOrderRQ,
                            pnrCollection,
                            pnrCollectionAfterCheckin,
                            bookedTravelersRequested,
                            serviceCallList
                    );
                    break;
                case CHECKIN:

                	if ( purchaseOrderRQ.getKioskId() != null ) {

                		// Get the kiosk profile from DB
                        if (PosType.KIOSK.toString().equalsIgnoreCase(purchaseOrderRQ.getPos()) && purchaseOrderRQ.getKioskId() != null ) {

                        	// Get profile from database
                        	KioskProfiles profile = kioskProfileService.getProfiles ( purchaseOrderRQ.getKioskId() );
                        	if ( profile != null ) {
                        		// Add DOCV if enabled on profile
                        		if ( "true".equalsIgnoreCase ( profile.getAttributeValue( "DOCV" ) ) ) {
                            		LOG.info( "Calling AddEditCode..." );
                                    addEditCode( bookedTravelersRequested, legCode, pnr, "DOCV");	
                        		}
                        	}
                        	
                        }
                	}

                	if (bookedTravelersRequested.get(0).isCheckinStatus()) {
                        checkinConfirmationBase = doPurchasePostCheckin(
                                purchaseOrderRQ,
                                pnrCollection,
                                pnrCollectionAfterCheckin,
                                FilterPassengersUtil.getAlreadyCheckedInPassengers(bookedTravelersRequested),
                                serviceCallList
                        );
                    } else {
                        checkinConfirmationBase = doPurchasePreCheckin(
                                purchaseOrderRQ,
                                pnrCollection,
                                pnrCollectionAfterCheckin,
                                FilterPassengersUtil.getNotCheckedInPassengers(bookedTravelersRequested),
                                serviceCallList
                        );
                    }
                    break;
                default:
                    GenericException ex = new GenericException(Response.Status.BAD_REQUEST, ErrorType.OPERATION_NOT_SUPPORTED, "Invalid operation field");
                    ex.setWasLogged(true);
                    logOnDatabaseService.logPreCheckinOnError(purchaseOrderRQ, cartId, recordLocator, ex);
                    throw ex;
            }
        }

        if (null != checkinConfirmationBase && checkinConfirmationBase instanceof CheckinConfirmation) {

            CheckinConfirmation checkinConfirmation = (CheckinConfirmation) checkinConfirmationBase;

            checkinConfirmation.setTsaRequired(pnr.isTsaRequired());

            //Remove any coded characters from boarding pass and barcode data.
            sanatizeData(checkinConfirmation);

            processCarRental(purchaseOrderRQ, cartPNR, checkinConfirmation, cartId, recordLocator);

            for (CartPNRConfirmation cartPNRConfirmation : checkinConfirmation.getCheckedInCarts().getCollection()) {
                cartPNRConfirmation.getMeta().getTotal().getCurrency().setTaxDetails(
                        CurrencyUtil.fixTaxDescriptions(
                                cartPNRConfirmation.getMeta().getTotal().getCurrency().getTaxDetails(),
                                purchaseOrderRQ.getLanguage(),
                                purchaseOrderRQ.getStore()
                        )
                );
            }
        }

        return checkinConfirmationBase;

    }

    private boolean addEditCode(List<BookedTraveler> bookedTravelersList, String bookedLegCode, PNR pnr, String editCode) throws Exception {

    	if ( bookedTravelersList == null || bookedTravelersList.size() == 0) {
    		return false;
    	}

		BookedSegment bookedSegment = pnr.getFirstBookedSegmentOnTime(bookedLegCode);
		
		if (null == bookedSegment || null == bookedSegment.getSegment()) {
			LOG.error(ErrorCodeDescriptions.MSG_CODE_BOOKEDSEGMENT_NOT_FOUND + " LegCode:{} PNR:{}", bookedLegCode, pnr.getPnr());
			return false;
		}

		for ( BookedTraveler bookedTraveler: bookedTravelersList) {
			BookingClass bookingClass = ShoppingCartUtil.getBookingClassMap(bookedTraveler, bookedSegment.getSegment().getSegmentCode());
	
			if (null == bookingClass) {
				LOG.error( ErrorCodeDescriptions.MSG_CODE_ERROR_NOT_FOUND_BOOKINGCLASS_FOR_SEGMENTCODE_IN_SEGMENT + "SEGMENT:{} PNR:{} PASSENGER ID:{}", bookedSegment.getSegment().getSegmentCode(), pnr.getPnr(), bookedTraveler.getId());
				return false;
			}
			
			editPassCharService.editPassengerChararcteristics( ShoppingCartUtil.getEditPassengerCharacteristicsRQCode( bookedSegment.getSegment(), bookingClass, bookedTraveler, editCode) );
	
			LOG.info("addEditCode({}, {}) was added.", bookedTraveler.getId(), editCode);
		}

		return true;
	}

    public void setMissingBillingEmailAddress(PurchaseOrder purchaseOrder, CartPNR cartPNR) {
        String billingEmailAddress = null;
        for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
            if (null != bookedTraveler.getEmail() && !bookedTraveler.getEmail().trim().isEmpty()) {
                billingEmailAddress = bookedTraveler.getEmail();
                break;
            }
        }

        if (null != purchaseOrder.getPaymentInfo()) {
            for (AbstractPaymentRequest abstractPaymentRequest : purchaseOrder.getPaymentInfo().getCollection()) {
                if (abstractPaymentRequest instanceof VisaCheckoutPaymentRequest) {
                    LOG.info("PaymentInfo is visa checkout");
                } else if (abstractPaymentRequest instanceof CreditCardPaymentRequest) {
                    CreditCardPaymentRequest creditCardPaymentRequest = (CreditCardPaymentRequest) abstractPaymentRequest;
                    if (null == creditCardPaymentRequest.getEmail() || creditCardPaymentRequest.getEmail().trim().isEmpty()) {
                        creditCardPaymentRequest.setEmail(billingEmailAddress);
                    }
                }
            }
        }
    }

    public void processCarRental(
            PurchaseOrderRQ purchaseOrderRQ,
            CartPNR cartPNR,
            CheckinConfirmation checkinConfirmation,
            String cartId,
            String recordLocator
    ) throws Exception {

        // Hertz Process start
        CarBookResponse carReservation = null;
        Warning warningHertz = null;

        //Hertz Confirmation
        RateReferenceMongo rateReferenceMongo = shoppingCartDao.getRateReferenceMongoDoc(cartId);

        if (null != rateReferenceMongo) {

            BookedTraveler firstBookedTraveler = cartPNR.getBookedTravelerByNameref("01.01");
            CarBookRQ carBookRQ = null;

            String phoneNumber;
            try {
                phoneNumber = firstBookedTraveler.getPhones().getCollection().get(0).getNumber();
            } catch (Exception e) {
                phoneNumber = "1234567890";
            }

            if (null == firstBookedTraveler.getEmail() || firstBookedTraveler.getEmail().isEmpty()) {
                LOG.error("Email missing for car reservation");
                warningHertz = new Warning(ErrorCodeType.CAR_RESERVATION_FAILED.getErrorCode(), ErrorCodeType.CAR_RESERVATION_FAILED.getValue(), new ExtraInfo("Email missing for car reservation"));
            } else {
                try {
                    carBookRQ = new CarBookRQ(
                            firstBookedTraveler.getEmail(),
                            firstBookedTraveler.getFirstName(),
                            firstBookedTraveler.getLastName(),
                            firstBookedTraveler.getDateOfBirth(),
                            rateReferenceMongo.getDiscount(),
                            phoneNumber
                    );
                } catch (Exception e) {
                    LOG.error(e.getMessage(), e);
                    carBookRQ = null;
                }
            }

            try {
                if (carBookRQ != null) {
                    String response = callServiceHertzBook(
                            carBookRQ,
                            rateReferenceMongo.getRateReference(),
                            purchaseOrderRQ.getLanguage()
                    );

                    if (response.contains("warning")) {
                        //warningCollection = new Gson().fromJson(response, WarningCollection.class);
                        warningHertz = new Warning(ErrorCodeType.CAR_RESERVATION_FAILED.getErrorCode(), ErrorCodeType.CAR_RESERVATION_FAILED.getValue(), new ExtraInfo(response));
                    } else {
                        carReservation = new Gson().fromJson(response, CarBookResponse.class);
                    }
                }
            } catch (Exception e) {
                LOG.error(e.getMessage(), e);
                warningHertz = new Warning(ErrorCodeType.CAR_RESERVATION_FAILED.getErrorCode(), ErrorCodeType.CAR_RESERVATION_FAILED.getValue(), new ExtraInfo(e.getMessage()));
            }
        }

        if (null != carReservation
                && null != carReservation.getConfirmation()
                && null != cartPNR.getCar()) {
            LOG.info("Reservation Hertz: " + carReservation.toString());
            carReservation.setCar(cartPNR.getCar());
        } //End Hertz Process


        if (null != warningHertz) {
            checkinConfirmation.getWarnings().getCollection().add(warningHertz);
        } else {
            //Show Hertz Reservation
            if (null != carReservation && null != carReservation.getCar()) {
                checkinConfirmation.setCarReservation(carReservation);

                if (!checkinConfirmation.isBookedCar()) {
                    aeService.createSSR(recordLocator, "Car Rental " + carReservation.getConfirmation(), null);
                    checkinConfirmation.setBookedCar(true);
                }
            }
        }
    }

    /**
     * @param pnrCollection
     * @param pnrCollectionAfterCheckin
     * @param recordLocator
     * @param cartId
     */
    @SimpleAsync
    private void updateOnError(PNRCollection pnrCollection, PNRCollection pnrCollectionAfterCheckin, String recordLocator, String cartId) {
        try {
            updatePnrCollectionService.updatePnrCollection(pnrCollection, false);
        } catch (Exception ex1) {
            //
        }
        try {
            boolean clean = false;
            postCheckinProcess.unasynchronousMutePostCheckin(pnrCollectionAfterCheckin, recordLocator, cartId, clean);
        } catch (Exception ex2) {
            //
        }
    }

    /**
     * @param pnrCollection
     * @param pnrCollectionAfterCheckin
     * @param recordLocator
     * @param cartId
     */
    @SimpleAsync
    private void updateOnSuccess(PNRCollection pnrCollection, PNRCollection pnrCollectionAfterCheckin, String recordLocator, String cartId, boolean clean, PurchaseOrderRQ purchaseOrderRQ) {
        if (!clean) {
            try {
                updatePnrCollectionService.updatePnrCollection(pnrCollection, false);
            } catch (Exception ex) {
                //
            }
        }

        try {
            if (null != pnrCollectionAfterCheckin) {
                postCheckinProcess.unasynchronousMutePostCheckin(pnrCollectionAfterCheckin, recordLocator, cartId, clean);
            }
        } catch (Exception ex) {
            //
            LOG.error(ex.getMessage(), ex);
        }

        if (SystemVariablesUtil.isCreateBoardingPassesOnPurchaseEnabled()) {
            createBoardingPasses(pnrCollection, recordLocator, cartId, purchaseOrderRQ.getPos());
        }
    }

    /**
     * @param carBookRQ
     * @param rateReference
     * @param language
     * @return
     */
    public String callServiceHertzBook(CarBookRQ carBookRQ, String rateReference, String language) {
        LOG.info("POST Hertz - Request {}", carBookRQ.toString());
        MultivaluedMap<String, Object> parameters = new MultivaluedMapImpl<>();
        parameters.add("lang", language.substring(0, 2));
        GenericRestClient client = new GenericRestClient(setUpConfigFactory.getSystemPropertiesFactory().getInstance().getAmTitaniumApiUrl());
        String response = client.doPost("/book/" + rateReference, carBookRQ, parameters);

        LOG.info("POST Hertz - Response {}", CommonUtil.removeJsonFormat(response));

        return response;
    }

    private SegmentDocument getStbSegmentDocument(
        BookedTraveler bookedTravelerWithClassE,
        BookedSegment firstSegmentOperatedByAM,
        String recordLocator
    ) {
        String travelDoc = BoardingPassUtil.getStandbyBoardingpassPectab(
                bookedTravelerWithClassE, firstSegmentOperatedByAM, recordLocator
        );
        SegmentDocument segmentDocument = new SegmentDocument();
        segmentDocument.setSegmentCode(firstSegmentOperatedByAM.getSegment().getSegmentCode());
        segmentDocument.setTravelDoc(travelDoc);
        segmentDocument.setCheckinStatus(true);
        String boardingPass;
        try {
            int start = travelDoc.indexOf("^P3");
            boardingPass = travelDoc.substring(start + 3);
            int end = boardingPass.indexOf("^");
            boardingPass = boardingPass.substring(0, end);
        } catch (Exception ex1) {
            LOG.error(ex1.getMessage());
            boardingPass = null;
        }
        if (null != boardingPass && !boardingPass.trim().isEmpty()) {
            try {
                segmentDocument.setBarcode(boardingPass);
                segmentDocument.setBarcodeImage(
                        CODES_GENERATOR.codesGenerator2D(boardingPass)
                );
                segmentDocument.setQrcodeImage(
                        CODES_GENERATOR.codesGeneratorQR(boardingPass)
                );
            } catch (Exception ex1) {
                LOG.error(ex1.getMessage());
            }
        }
        return segmentDocument;
    }


    /**
     * @return the kioskProfileDao
     */
    public IKioskProfileDao getKioskProfileDao() {
        return kioskProfileDao;
    }

    /**
     * @param kioskProfileDao the kioskProfileDao to set
     */
    public void setKioskProfileDao(IKioskProfileDao kioskProfileDao) {
        this.kioskProfileDao = kioskProfileDao;
    }

    /**
     * @return the seatsService
     */
    public SeatsService getSeatsService() {
        return seatsService;
    }

    /**
     * @param seatsService the seatsService to set
     */
    public void setSeatsService(SeatsService seatsService) {
        this.seatsService = seatsService;
    }
}
