package com.am.checkin.web.service;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.AuthorizationPaymentConfig;
import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.BookedTravelerCollection;
import com.aeromexico.commons.model.BookingClass;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.FlxPriceWrapper;
import com.aeromexico.commons.model.PNR;
import com.aeromexico.commons.model.SeatmapCollection;
import com.aeromexico.commons.model.rq.SeatmapCheckInRQ;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.farelogix.api.servicelist.AMServiceListFlx;
import com.am.checkin.web.util.FlxSeatMapPriceUtil;
import com.am.checkin.web.util.FlxServiceListUtil;
import com.farelogix.flx.servicelistrq.ServiceListRQ;
import com.farelogix.flx.servicelistrs.ServiceListRS;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adrianleal
 */
@Named
@ApplicationScoped
public class FlxSeatMapPriceService {

    private static final Logger LOG = LoggerFactory.getLogger(FlxSeatMapPriceService.class);

    @Inject
    private AMServiceListFlx serviceListFlx;

    @Inject
    @Named("CheckInSetUpConfigFactoryWrapper")
    private CheckInSetUpConfigFactoryWrapper setUpConfigFactory;

    public void setPricesFromFareLogix(PNR pnr, SeatmapCollection seatmapCollection, SeatmapCheckInRQ seatmapCheckInRQ) throws Exception {

        if (null == pnr) {
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION, "Pnr not found");
        }

        String pos = seatmapCheckInRQ.getPos();
        String store = seatmapCheckInRQ.getStore();

        AuthorizationPaymentConfig authorizationPaymentConfig = setUpConfigFactory.getInstance().getAuthorizationPaymentConfig(store, pos);

        if (null == authorizationPaymentConfig) {
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PAYMENT_CONFIG_NOT_FOUND, "payment config not found");
        }

        String currencyCode = authorizationPaymentConfig.getCurrency();

        if (null == currencyCode) {
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.CURRENCY_CODE_NOT_FOUND, "currency code not found");
        }

        CartPNR cartPNR = pnr.getCartPNRByCartId(seatmapCheckInRQ.getCartId());

        if (null == cartPNR) {
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION, "Cart not found");
        }

        String legCode = cartPNR.getLegCode();
        BookedLeg bookedLeg = pnr.getBookedLegByLegCode(legCode);
        if (null == bookedLeg) {
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION, "BookedLeg not found");
        }

        BookedTravelerCollection bookedTravelerCollection = cartPNR.getTravelerInfo();

        BookingClass bookingClass;
        bookingClass = bookedTravelerCollection.getCollection().get(0).getBookingClasses().getCollection().get(0);

        if (null == bookingClass
                || null == bookingClass.getBookingClass()
                || bookingClass.getBookingClass().trim().isEmpty()) {
            throw new GenericException(
                    Response.Status.BAD_REQUEST,
                    ErrorType.BOOKING_CLASS_MAY_NOT_BE_EMPTY,
                    "FlxSeatmapPriceService: The booking class of the passenger is required"
            );
        }

        if ("J".equalsIgnoreCase(bookingClass.getBookingCabin())){
            return;
        }

        Map<String, String> segmentReferences = new HashMap<>();

        Map<String, String> travelerReferences = new HashMap<>();

        List<BookedSegment> amBookedSegmentsWithSeatMap;
        amBookedSegmentsWithSeatMap = FlxSeatMapPriceUtil.getSegmentsWithSeatmap(
                seatmapCollection,
                bookedLeg
        );

        if (null != amBookedSegmentsWithSeatMap && !amBookedSegmentsWithSeatMap.isEmpty()) {

            int flightIndex = 1;
            for (BookedSegment bookedSegment : amBookedSegmentsWithSeatMap) {
                segmentReferences.put(bookedSegment.getSegment().getSegmentCode(), "" + flightIndex++);
            }

            int travelerIndex = 1;
            for (BookedTraveler bookedTraveler : bookedTravelerCollection.getCollection()) {
                travelerReferences.put(bookedTraveler.getNameRefNumber(), "t" + travelerIndex++);
            }

            ServiceListRQ serviceListRQ = FlxServiceListUtil.getSeatMapServiceListRQ(
                    seatmapCollection,
                    amBookedSegmentsWithSeatMap,
                    bookedTravelerCollection,
                    pnr.getPnr(),
                    currencyCode
            );

            ServiceListRS serviceListRS = serviceListFlx.getServiceListRS(serviceListRQ);
            
            //NameRef   SegmentCode Section     Seat
            Map<String, Map<String, Map<String, Map<String, FlxPriceWrapper>>>> pricePerPassengerMap;
            pricePerPassengerMap = new HashMap<>();

            for (BookedTraveler bookedTraveler : bookedTravelerCollection.getCollection()) {
                //SegmentCode   Section     Seat
                Map<String, Map<String, Map<String, FlxPriceWrapper>>> pricePerSegmentMap;

                pricePerSegmentMap = FlxSeatMapPriceUtil.getFLXPricePerSections(
                        segmentReferences,
                        serviceListRS,
                        currencyCode,
                        travelerReferences.get(bookedTraveler.getNameRefNumber())
                );

                pricePerPassengerMap.put(bookedTraveler.getNameRefNumber(), pricePerSegmentMap);
            }

            LOG.info("seatPricesPerPassenger: {}", pricePerPassengerMap.toString());

            //Just First Passenger
            Map<String, Map<String, Map<String, FlxPriceWrapper>>> pricePerSegmentMap;
            pricePerSegmentMap = pricePerPassengerMap.get(bookedTravelerCollection.getCollection().get(0).getNameRefNumber());

            FlxSeatMapPriceUtil.updateSeatMapsWithFarelogixPrices(seatmapCollection, pricePerSegmentMap);
        } else {
            LOG.info("There are not segments with available seatmaps");
        }
    }
}
