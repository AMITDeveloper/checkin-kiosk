
package com.am.checkin.web.service;

import com.aeromexico.commons.model.Airport;
import com.aeromexico.commons.model.AirportCollection;
import com.aeromexico.commons.model.Links;
import com.aeromexico.commons.model.Self;
import com.aeromexico.commons.model.rq.AirportFlightListRQ;
import com.aeromexico.dao.util.CommonUtil;
import com.aeromexico.sabre.api.acs.AMAirportFlightList;
import com.sabre.services.acs.bso.airportflightlist.v3.ACSAirportFlightListRSACS;
import com.sabre.services.acs.bso.airportflightlist.v3.AirportFlightACS;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@ApplicationScoped
public class AirportFlightListService {

	@Inject
	AMAirportFlightList airportFlightList;

	public AirportCollection getAirportList(AirportFlightListRQ airportFlightListRQ) throws Exception {

		ACSAirportFlightListRSACS airportFlightListRSACS = airportFlightList.airportFlightList("AM", airportFlightListRQ.getOrigin(), null, false);

		AirportCollection airportCollection = new AirportCollection();
		airportCollection.setLinks( new Links( new Self( CommonUtil.getLink( airportFlightListRQ.getUri() ) ) ) );

		if ( airportFlightListRSACS.getResult().getCompletionStatus().equals(com.sabre.services.stl.v3.CompletionCodes.COMPLETE) ) {

			for ( AirportFlightACS airportFlight: airportFlightListRSACS.getAirportFlightList().getAirportFlight() ) {

				Airport airport = new Airport(airportFlight.getDestination(), "");
				if ( ! airportCollection.getCollection().contains( airport ) ) {
					airportCollection.getCollection().add( new Airport(airportFlight.getDestination(), "") );
				}
			}
		}

		return airportCollection;

	}

	public AMAirportFlightList getAirportFlightList() {
		return airportFlightList;
	}

	public void setAirportFlightList(AMAirportFlightList airportFlightList) {
		this.airportFlightList = airportFlightList;
	}

}
