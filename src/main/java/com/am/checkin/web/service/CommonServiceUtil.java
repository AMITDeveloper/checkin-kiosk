package com.am.checkin.web.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.Infant;
import com.aeromexico.commons.model.TimaticCountryInfo;
import com.aeromexico.commons.model.TimaticInfo;
import com.aeromexico.commons.model.TimaticSectionInfo;
import com.aeromexico.commons.model.TimaticSubSectionInfo;
import com.aeromexico.commons.model.TravelerDocument;
import com.aeromexico.commons.model.VisaInfo;
import com.aeromexico.commons.web.types.CheckinIneligibleReasons;
import com.aeromexico.commons.web.types.GenderType;
import com.aeromexico.dao.services.ICountryISODao;
import com.aeromexico.dao.util.CommonUtil;
import com.am.checkin.web.util.PnrCollectionUtil;
import com.sabre.services.checkin.getpassengerdata.v4.PassengerDataResponseACS;
import com.sabre.services.checkin.getpassengerdata.v4.SectionInfoACS;
import com.sabre.services.checkin.getpassengerdata.v4.SubSectionInfoACS;
import com.sabre.services.checkin.getpassengerdata.v4.TimaticInfoACS;
import com.sabre.services.stl.v3.EditCodeAttributeType;
import com.sabre.services.stl.v3.EditType;

public class CommonServiceUtil {

    private static final Logger LOG = LoggerFactory.getLogger(CommonServiceUtil.class);

    /**
     * @param rawDateOfBirth 25JUN1968
     * @return String in the following format: 25JUN1968
     */
    static public String parseSabreDate(String rawDateOfBirth) {

        if (rawDateOfBirth == null || rawDateOfBirth.isEmpty()) {
            return null;
        }

        SimpleDateFormat df = null;

        if (rawDateOfBirth.trim().length() == 7) {
            df = new SimpleDateFormat("ddMMMyy", Locale.ENGLISH);
        } else {
            df = new SimpleDateFormat("ddMMMyyyy", Locale.ENGLISH);
        }

        Date dateValue = null;

        try {
            dateValue = df.parse(rawDateOfBirth);
        } catch (ParseException e) {
            //LOG.error(e.getMessage(), e);
        }

        SimpleDateFormat targetDF = new SimpleDateFormat("yyyy-MM-dd");
        String dateOfBirth = targetDF.format(dateValue);

        return dateOfBirth;
    }

    /**
     * @param datetime
     * @return IN "2016-06-04T20:29:00"
     */
    static public Date parseSabreDateTime(String datetime) {

        if (datetime == null || datetime.isEmpty()) {
            return null;
        }

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        Date dateValue = null;

        try {
            dateValue = df.parse(datetime);

        } catch (ParseException e) {

            df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");

            try {
                dateValue = df.parse(datetime);
            } catch (ParseException ex) {
                //LOG.error(e.getMessage(), e);
            }
        }

        return dateValue;
    }

    /**
     * *
     * DOCO can be used to capture VISA information or redress number
     * <p>
     * The VISA format: /V/1111/YYYY/11NOV17/MX / VISA indicator / VISA number /
     * issue city / issue date / applicable country code
     * <p>
     * Redress number: /K/985322190
     */
    public static void parseDOCO(BookedTraveler bookedTraveler, String tlStr) {

        if (tlStr == null || tlStr.isEmpty()) {
            return;
        }

        //This DOCO is for a VISA information  
        if (tlStr.startsWith("/V/")) {

            String[] result = tlStr.split("/");

            if (result == null || result.length < 6) {
                // TODO: log error
                return;
            }

            VisaInfo visaInfo = new VisaInfo();

            visaInfo.setNumber(result[2]);
            visaInfo.setIssueCity(result[3]);
            visaInfo.setIssueDate(parseSabreDate(result[4]));
            visaInfo.setApplicableCountryCode(result[5]);

            if (result.length == 6) {
                // DOCO  /V/222333/MEX/11NOV17/US
                bookedTraveler.setVisaInfo(visaInfo);
            } else if (result.length == 7) {
                // DOCO /V/223311/MEX/12DEC17/US/I
                if (bookedTraveler.getInfant() != null) {
                    bookedTraveler.getInfant().setVisaInfo(visaInfo);
                }
            }

        } else if (tlStr.startsWith("/K/")) {
            // This DOCO is for a redress number
            bookedTraveler.setKtn(tlStr.substring(3));
        }

    }

    /**
     * *
     *
     * @param bookedTraveler
     * @param tlStr          INF format: MOOREAMAYA,MARIE,19OCT16 last name, first name, date of birth
     *                       <p>
     *                       MOOREAMAYA MARIE,19OCT16
     */
    public static void parseINF(BookedTraveler bookedTraveler, String tlStr) {

        LOG.info("+parseINF()");

        if (bookedTraveler == null) {
            return;
        }

        if (tlStr == null || tlStr.trim().isEmpty()) {
            return;
        }

        LOG.info("parseINF({}, {})", bookedTraveler.getId(), tlStr);

        String[] result = tlStr.split(",");

        if (result == null || result.length < 2) {
            return;
        }

        if (bookedTraveler.getInfant() == null) {
            bookedTraveler.setInfant(new Infant());
        }

        if (result.length == 3) {

            bookedTraveler.getInfant().setFirstName(result[1]);
            bookedTraveler.getInfant().setLastName(result[0]);
            bookedTraveler.getInfant().setDateOfBirth(parseSabreDate(result[2]));

        } else if (result.length == 2) {

            //IF INF COMES WITHOUT COMMA FIRST,LASTNAME
            String[] name = result[0].split(" ");
            if (name.length == 2) {
                bookedTraveler.getInfant().setFirstName(name[1]);
                bookedTraveler.getInfant().setLastName(name[0]);

            } else if (name.length == 1) {
                bookedTraveler.getInfant().setLastName(name[0]);
            }

            bookedTraveler.getInfant().setDateOfBirth(parseSabreDate(result[1]));

        }

        LOG.info("-parseINF({})", bookedTraveler.getInfant());

    }

    /**
     * @param countryISODao
     * @param bookedTraveler
     * @param tlStr
     */
    public static void parseDOCS(ICountryISODao countryISODao, BookedTraveler bookedTraveler, String tlStr) {

        if (bookedTraveler == null) {
            // This should never happen
            return;
        }

        if (tlStr == null || tlStr.trim().isEmpty()) {
            // This should never happen
            return;
        }

        if (tlStr.trim().startsWith("DB/")) {
            parseDB(bookedTraveler, tlStr);
        } else if (tlStr.trim().startsWith("P/")) {
            parsePassport(countryISODao, bookedTraveler, tlStr);
        }
    }

    /**
     * *
     *
     * @param bookedTraveler
     * @param tlStr          P/US/333345678909876/US/11JAN81/M/11JAN20/TEST/PASSENGER
     *                       P/US/591187597/US/13APR91/F/11JUN28/PARRA/VANESSA///S1
     *                       P/USA/591187596/USA/19OCT16/FI/11JUN23/MOORE/AMAYA/MARIE//S1
     *                       <p>
     *                       0	1	2	3	4	5	6	7	8
     *                       passport / issue country / passport number / nationality / date of birth
     *                       / gender / expire date / last name / first name P / US / 333345678909876
     *                       / US / 11JAN81 / M / 11JAN20 / TEST / PASSENGER
     */
    private static void parsePassport(ICountryISODao countryISODao, BookedTraveler bookedTraveler, String tlStr) {

        String[] result = tlStr.split("/");

        if (result == null || result.length < 9) {
            // This should never happen
            return;
        }

        try {
            if (null != countryISODao && 3 == result[3].length()) {
                result[3] = countryISODao.getCountryISO2ByIso3(result[3]);
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }

        try {
            if (null != countryISODao && 3 == result[1].length()) {
                result[1] = countryISODao.getCountryISO2ByIso3(result[1]);
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }

        // MI or FI
        if (result[5] != null && result[5].endsWith("I")) {
            parseInfantPassport(bookedTraveler, tlStr, result);
        } else {
            parseAdultPassport(bookedTraveler, tlStr, result);
        }

    }

    private static void parseInfantPassport(BookedTraveler bookedTraveler, String tlStr, String[] result) {

        if (bookedTraveler.getInfant() == null) {
            bookedTraveler.setInfant(new Infant());
        }

        if (bookedTraveler.getInfant().getTravelDocument() == null) {
            bookedTraveler.getInfant().setTravelDocument(new TravelerDocument());
        }

        bookedTraveler.getInfant().getTravelDocument().setDocumentNumber(result[2]);
        bookedTraveler.getInfant().getTravelDocument().setIssuingCountry(result[1]);
        bookedTraveler.getInfant().getTravelDocument().setNationality(result[3]);
        bookedTraveler.getInfant().getTravelDocument().setExpirationDate(parseSabreDate(result[6]));

        bookedTraveler.getInfant().setDateOfBirth(parseSabreDate(result[4]));
        bookedTraveler.getInfant().setFirstName(result[8]);
        bookedTraveler.getInfant().setLastName(result[7]);

        try {
            bookedTraveler.getInfant().setGender(GenderType.fromCode(result[5].replace("I", "")));
        } catch (IllegalArgumentException i) {
            //Ignore 
        }

        //TODO: Need to get other GDS scanned code
        if (tlStr.endsWith("S1")) {
            bookedTraveler.getTravelDocument().setPassportScanned(true);
        } else {
            bookedTraveler.getTravelDocument().setPassportScanned(false);
        }

    }

    private static void parseAdultPassport(BookedTraveler bookedTraveler, String tlStr, String[] result) {

        if (bookedTraveler.getTravelDocument() == null) {
            bookedTraveler.setTravelDocument(new TravelerDocument());
        }

        bookedTraveler.getTravelDocument().setDocumentNumber(result[2]);
        bookedTraveler.getTravelDocument().setIssuingCountry(result[1]);
        bookedTraveler.getTravelDocument().setNationality(result[3]);
        bookedTraveler.getTravelDocument().setExpirationDate(parseSabreDate(result[6]));

        bookedTraveler.setDateOfBirth(parseSabreDate(result[4]));

        try {
            bookedTraveler.setGender(GenderType.fromCode(result[5]));
        } catch (IllegalArgumentException i) {
            //Ignore 
        }

        //TODO: Need to get other GDS scanned code
        if (tlStr.endsWith("S1")) {
            bookedTraveler.getTravelDocument().setPassportScanned(true);
        } else {
            bookedTraveler.getTravelDocument().setPassportScanned(false);
        }

        //TODO: get gender and date of birth
    }

    /**
     * *
     *
     * @param bookedTraveler
     * @param tlStr          DB/25JUN1968/M/ALNAJAR/SAMMY DB/25JUN1968/FI/ALNAJAR/SAMMY
     *                       DB/25JUN1968/MI/ALNAJAR/SAMMY
     */
    public static void parseDB(BookedTraveler bookedTraveler, String tlStr) {

        String[] result = tlStr.split("/");

        if (result == null || result.length < 5) {
            return;
        }

        String gender = result[2];
        String dateOfBirth = result[1];

        // Is this an infant DB?
        if (gender != null && gender.contains("I")) {
            parseInfantDB(bookedTraveler, dateOfBirth, gender, result[4], result[3]);
        } else {
            parseAdultDB(bookedTraveler, dateOfBirth, gender);
        }

    }

    public static void parseAdultDB(BookedTraveler bookedTraveler, String dateOfBirth, String gender) {

        // 25JUN1968 or 25JUN68
        bookedTraveler.setDateOfBirth(parseSabreDate(dateOfBirth));
        try {
            bookedTraveler.setGender(GenderType.fromCode(gender));
        } catch (IllegalArgumentException i) {
            //Ignore 
        }

    }

    public static void parseInfantDB(BookedTraveler bookedTraveler, String dateOfBirth, String gender, String firstName, String lastName) {

        if (bookedTraveler == null) {
            return;
        }

        if (bookedTraveler.getInfant() == null) {
            bookedTraveler.setInfant(new Infant());
        }

        bookedTraveler.getInfant().setFirstName(firstName);
        bookedTraveler.getInfant().setLastName(lastName);

        // 25JUN1968 or 25JUN68
        bookedTraveler.getInfant().setDateOfBirth(parseSabreDate(dateOfBirth));

        // MI or FI
        try {
            bookedTraveler.getInfant().setGender(GenderType.fromCode(gender.replace("I", "")));
        } catch (IllegalArgumentException i) {
            //Ignore 
        }

    }

    /**
     * This function is shared between pnrLookupService and shoppingCartService
     *
     * @param bookedTraveler
     * @param passengerData
     * @param isInfant
     */
    public static void setTimaticInfo(BookedTraveler bookedTraveler, PassengerDataResponseACS passengerData, boolean isInfant) {

        LOG.info("CommonServiceUtil.setTimaticInfo( bookedTraveler={}, isInfant={} );", bookedTraveler.getId(), isInfant);

        if (passengerData == null || passengerData.getTimaticInfoList() == null || passengerData.getTimaticInfoList().getTimaticInfo() == null) {
            LOG.info("TIMATIC-setTimaticInfo: No TimaticInfo in passenger data response.");
            return;
        }

        if (passengerData.getTimaticInfoList().getTimaticInfo().isEmpty()) {
            LOG.error("TIMATIC-setTimaticInfo: TimaticInfo is empty");
            return;
        }

        com.sabre.services.checkin.getpassengerdata.v4.TimaticInfoListACS.TimaticInfo acsTimaticInfo = null;

        for (com.sabre.services.checkin.getpassengerdata.v4.TimaticInfoListACS.TimaticInfo timaticInfoTemp : passengerData.getTimaticInfoList().getTimaticInfo()) {
            if (null != timaticInfoTemp) {
                if (isInfant) {
                    if (Boolean.TRUE.equals(timaticInfoTemp.isInfant())) {
                        acsTimaticInfo = timaticInfoTemp;
                        break;
                    }
                } else {
                    if (null == timaticInfoTemp.isInfant() || Boolean.FALSE.equals(timaticInfoTemp.isInfant())) {
                        acsTimaticInfo = timaticInfoTemp;
                        break;
                    }
                }
            }
        }

        //initialize, remove any ineligible reason
        if (isInfant) {
            PnrCollectionUtil.removeIneligibleReasonVerification(bookedTraveler, CheckinIneligibleReasons.TIMI_NOT_OK.toString());
            PnrCollectionUtil.removeIneligibleReasonVerification(bookedTraveler, CheckinIneligibleReasons.TIM_INF_CONDITIONAL.toString());
        } else {
            PnrCollectionUtil.removeIneligibleReasonVerification(bookedTraveler, CheckinIneligibleReasons.TIM_NOT_OK.toString());
            PnrCollectionUtil.removeIneligibleReasonVerification(bookedTraveler, CheckinIneligibleReasons.TIM_CONDITIONAL.toString());
        }

        if (null != acsTimaticInfo) {

            TimaticInfo timaticInfo = new TimaticInfo();
            timaticInfo.setStatus(sanitizeTimaticStatus(acsTimaticInfo.getTimaticStatus()));

            if (acsTimaticInfo.getTimaticStatus() != null
                    && acsTimaticInfo.getTimaticStatus().contains("NOT OK TO BOARD")
                    && !acsTimaticInfo.getTimaticStatus().contains("INF")) {

                if (!bookedTraveler.isCheckinStatus()) {
                    // Adult
                    bookedTraveler.getIneligibleReasons().add(CheckinIneligibleReasons.TIM_NOT_OK.getCheckinIneligibleReason());
                }

            } else if (acsTimaticInfo.getTimaticStatus() != null
                    && acsTimaticInfo.getTimaticStatus().contains("NOT OK TO BOARD")
                    && acsTimaticInfo.getTimaticStatus().contains("INF")) {

                if (!bookedTraveler.isCheckinStatus()) {
                    // Infant
                    bookedTraveler.getIneligibleReasons().add(CheckinIneligibleReasons.TIMI_NOT_OK.getCheckinIneligibleReason());
                }

            } else if (acsTimaticInfo.getTimaticStatus() != null
                    && acsTimaticInfo.getTimaticStatus().contains("CONDITIONAL")
                    && !acsTimaticInfo.getTimaticStatus().contains("INF")) {

                if (!bookedTraveler.isCheckinStatus()) {
                    // Adult - CONDITIONAL
                    bookedTraveler.getIneligibleReasons().add(CheckinIneligibleReasons.TIM_CONDITIONAL.getCheckinIneligibleReason());
                }
                if (!bookedTraveler.isCheckinStatus() && bookedTraveler.isCovidRestrictionConfirmed()) {
                    // Adult - CONDITIONAL
                    for(TimaticInfoACS.TimaticCountryInfo tim : acsTimaticInfo.getTimaticCountryInfo()){
                        if(tim.getCountry().getCountryCode().equalsIgnoreCase("US")){
                            for(SectionInfoACS section :tim.getSectionInfo()){
                                for(SubSectionInfoACS subsection : section.getSubSectionInfo()){
                                    if(null != subsection.getSubSectionName() && subsection.getSubSectionName().equalsIgnoreCase("WARNING") && subsection.getSubSectionText().contains("IN THE PAST 14 DAYS ARE NOT ALLOWED TO ENTER THE USA.")){
                                        bookedTraveler.getIneligibleReasons().remove(CheckinIneligibleReasons.TIM_CONDITIONAL.getCheckinIneligibleReason());
                                    }
                                }
                            }
                        }
                    }
                }

            } else if (acsTimaticInfo.getTimaticStatus() != null
                    && acsTimaticInfo.getTimaticStatus().contains("CONDITIONAL")
                    && acsTimaticInfo.getTimaticStatus().contains("INF")) {

                if (!bookedTraveler.isCheckinStatus()) {
                    // Infant - CONDITIONAL
                    bookedTraveler.getIneligibleReasons().add(CheckinIneligibleReasons.TIM_INF_CONDITIONAL.getCheckinIneligibleReason());
                }

                if (!bookedTraveler.isCheckinStatus() && bookedTraveler.isCovidRestrictionConfirmed()) {
                    // Adult - CONDITIONAL
                    for(TimaticInfoACS.TimaticCountryInfo tim : acsTimaticInfo.getTimaticCountryInfo()){
                        if(tim.getCountry().getCountryCode().equalsIgnoreCase("US")){
                            for(SectionInfoACS section :tim.getSectionInfo()){
                                for(SubSectionInfoACS subsection : section.getSubSectionInfo()){
                                    if(null != subsection.getSubSectionName() && subsection.getSubSectionName().equalsIgnoreCase("WARNING") && subsection.getSubSectionText().contains("IN THE PAST 14 DAYS ARE NOT ALLOWED TO ENTER THE USA.")){
                                        bookedTraveler.getIneligibleReasons().remove(CheckinIneligibleReasons.TIM_INF_CONDITIONAL.getCheckinIneligibleReason());
                                    }
                                }
                            }
                        }
                    }
                }

            } else if (acsTimaticInfo.getTimaticStatus() != null
                    && acsTimaticInfo.getTimaticStatus().contains("OK TO BOARD")
                    && !acsTimaticInfo.getTimaticStatus().contains("INF")) {

                // Adult - OK TO BOARD
                PnrCollectionUtil.removeMissingInformation(bookedTraveler, CheckinIneligibleReasons.TIM.toString());

            } else if (acsTimaticInfo.getTimaticStatus() != null
                    && acsTimaticInfo.getTimaticStatus().contains("OK TO BOARD")
                    && acsTimaticInfo.getTimaticStatus().contains("INF")) {

                // Infant - OK TO BOARD
                PnrCollectionUtil.removeMissingInformation(bookedTraveler, CheckinIneligibleReasons.TIMI.toString());

            }

            boolean visaRequired = false;

            for (TimaticInfoACS.TimaticCountryInfo acstimaticCountryInfo : acsTimaticInfo.getTimaticCountryInfo()) {

                TimaticCountryInfo amTimaticCountryInfo = new TimaticCountryInfo();

                amTimaticCountryInfo.setCountryCode(acstimaticCountryInfo.getCountry().getCountryCode());
                amTimaticCountryInfo.setCountryName(acstimaticCountryInfo.getCountry().getCountryName());
                amTimaticCountryInfo.setTimaticStatus(sanitizeTimaticStatus(acstimaticCountryInfo.getTimaticStatus()));

                timaticInfo.getTimaticCountryInfoList().add(amTimaticCountryInfo);

                for (SectionInfoACS sectionInfoACS : acstimaticCountryInfo.getSectionInfo()) {

                    TimaticSectionInfo timaticSectionInfo = new TimaticSectionInfo();

                    timaticSectionInfo.setSectionName(sectionInfoACS.getSectionName());
                    timaticSectionInfo.setSectionType(sectionInfoACS.getSectionType());
                    timaticSectionInfo.setSectionText(sectionInfoACS.getSectionText());

                    //Set the Visa required flag if there is a section name = VISA
                    if ("VISA".equalsIgnoreCase(timaticSectionInfo.getSectionName())) {
                        timaticInfo.setVisaRequired(true);
                        visaRequired = true;
                    }

                    amTimaticCountryInfo.getTimaticSectionInfoList().add(timaticSectionInfo);

                    for (SubSectionInfoACS subSectionInfoACS : sectionInfoACS.getSubSectionInfo()) {

                        TimaticSubSectionInfo timaticSubSectionInfo = new TimaticSubSectionInfo();

                        timaticSubSectionInfo.setSubSectionText(subSectionInfoACS.getSubSectionText());
                        timaticSubSectionInfo.setSubSectionType(subSectionInfoACS.getSubSectionType());
                        timaticSubSectionInfo.setSubSectionName(subSectionInfoACS.getSubSectionName());

                        timaticSectionInfo.getTimaticSubSectionInfoList().add(timaticSubSectionInfo);
                    }
                }
            }

            if (isInfant && bookedTraveler.getInfant() != null) {
                bookedTraveler.getInfant().setTimaticInfo(timaticInfo);
                if (visaRequired) {
                    bookedTraveler.getMissingCheckinRequiredFields().add(CheckinIneligibleReasons.INFANT_VISA.getCheckinIneligibleReason());
                }
            } else {
                bookedTraveler.setTimaticInfo(timaticInfo);
                if (visaRequired) {
                    bookedTraveler.getMissingCheckinRequiredFields().add(CheckinIneligibleReasons.VISA.getCheckinIneligibleReason());
                }
            }
        }

    }


    /**
     * *
     */
    private static String sanitizeTimaticStatus(String status) {

        if (status == null) {
            return status;
        }
        // remove an leading or trailing spaces
        status = status.trim();

        // Remove "TIM "
        status = status.replace("TIM ", "");

        // Remove "TIM-"
        status = status.replace("TIM-", "");

        // Remove "INF "
        //status = status.replace("INF ", "");

        // Remove "INF-"
        //status = status.replace("INF-", "");

        return status;
    }

    private static String getDocumentTypeCode(String DocumentType) {
        if ("PASSPORT NORMAL".equalsIgnoreCase(DocumentType)) {
            return "1";
        }

        return DocumentType;
    }

    /*
	01 
		ALIENSPASSPORT 
		ALIENS PASSPORT 

	02 
		REENTRYPERMIT 
  		RE ENTRY PERMIT 

	03 
		RESIDENCEPERMIT 
  		RESIDENCE PERMIT ALIENS PASSPORT

	04 
		PERMANENTRESIDENTRESIDENTALIENCARDFORMI551   
		PERMANENT RESIDENT RESIDENT ALIEN CARD FORM I 551   
		PERMANENT RESIDENT RESIDENT ALIEN CARD FORM I
     */
    private static String getResidencyDocumentTypeCode(String residencyDocumentType) {
        if ("ALIENSPASSPORT".equalsIgnoreCase(residencyDocumentType)) {
            return "1";
        } else if ("REENTRYPERMIT".equalsIgnoreCase(residencyDocumentType)) {
            return "2";
        } else if ("RESIDENCEPERMIT".equalsIgnoreCase(residencyDocumentType)) {
            return "3";
        } else if ("PERMANENTRESIDENTRESIDENTALIENCARDFORMI551".equalsIgnoreCase(residencyDocumentType)) {
            return "4";
        }

        return residencyDocumentType;
    }

    /*
               <PassengerEditList>
                  <ns2:Edit name="TIM">
                     <ns2:EditCodeAttributeList>

                        <ns2:EditCodeAttribute name="StayType">
                           <ns2:Values>
                              <ns2:Value>BUSINESS</ns2:Value>
                           </ns2:Values>
                        </ns2:EditCodeAttribute>

                        <ns2:EditCodeAttribute name="DocumentType">
                           <ns2:Values>
                              <ns2:Value>PASSPORT NORMAL</ns2:Value>
                           </ns2:Values>
                        </ns2:EditCodeAttribute>

                        <ns2:EditCodeAttribute name="ResidencyDocumentType">
                           <ns2:Values>
                              <ns2:Value>ALIENSPASSPORT</ns2:Value>
                           </ns2:Values>
                        </ns2:EditCodeAttribute>

                        <ns2:EditCodeAttribute name="ResidencyCountry">
                           <ns2:Values>
                              <ns2:Value>US</ns2:Value>
                           </ns2:Values>
                        </ns2:EditCodeAttribute>

                        <ns2:EditCodeAttribute name="ReturnDate">
                           <ns2:Values>
                              <ns2:Value>11NOV18</ns2:Value>
                           </ns2:Values>
                        </ns2:EditCodeAttribute>

                     </ns2:EditCodeAttributeList>
                  </ns2:Edit>
               </PassengerEditList>
     */
    public static void parseTimaticFreeText(BookedTraveler bookedTraveler, PassengerDataResponseACS passengerData, boolean isInfant) {

        LOG.info("parseTimaticFreeText( bookedTraveler={}, isInfant={} );", bookedTraveler.getId(), isInfant);

        if (passengerData == null || passengerData.getPassengerEditList() == null || passengerData.getPassengerEditList().getEdit() == null) {
            return;
        }

        if (passengerData.getPassengerEditList().getEdit().size() == 0) {
            return;
        }

        TimaticInfo timaticInfo = null;

        if (isInfant) {

            // Infant passenger
            if (bookedTraveler.getInfant().getTimaticInfo() == null) {
                bookedTraveler.getInfant().setTimaticInfo(new TimaticInfo());
            }

            timaticInfo = bookedTraveler.getInfant().getTimaticInfo();

        } else {

            if (bookedTraveler.getTimaticInfo() == null) {
                bookedTraveler.setTimaticInfo(new TimaticInfo());
            }

            timaticInfo = bookedTraveler.getTimaticInfo();
        }

        for (EditType editType : passengerData.getPassengerEditList().getEdit()) {

            if ("TIM".equalsIgnoreCase(editType.getName())) {

                for (EditCodeAttributeType editCodeAttributeType : editType.getEditCodeAttributeList().getEditCodeAttribute()) {

                    if ("StayType".equalsIgnoreCase(editCodeAttributeType.getName())) {

                        if (editCodeAttributeType.getValues() != null && !editCodeAttributeType.getValues().getValue().isEmpty()) {
                            timaticInfo.setStayType(editCodeAttributeType.getValues().getValue().get(0));
                        }

                    } else if ("DocumentType".equalsIgnoreCase(editCodeAttributeType.getName())) {

                        if (editCodeAttributeType.getValues() != null && !editCodeAttributeType.getValues().getValue().isEmpty()) {
                            timaticInfo.setDocumentType(getDocumentTypeCode(editCodeAttributeType.getValues().getValue().get(0)));
                        }

                    } else if ("ResidencyDocumentType".equalsIgnoreCase(editCodeAttributeType.getName())) {

                        if (editCodeAttributeType.getValues() != null && !editCodeAttributeType.getValues().getValue().isEmpty()) {
                            timaticInfo.setResidencyDocumentType(getResidencyDocumentTypeCode(editCodeAttributeType.getValues().getValue().get(0)));
                        }

                    } else if ("ResidencyCountry".equalsIgnoreCase(editCodeAttributeType.getName())) {

                        if (editCodeAttributeType.getValues() != null && !editCodeAttributeType.getValues().getValue().isEmpty()) {
                            timaticInfo.setResidencyCountry(editCodeAttributeType.getValues().getValue().get(0));
                        }

                    } else if ("ReturnDate".equalsIgnoreCase(editCodeAttributeType.getName())) {

                        if (editCodeAttributeType.getValues() != null && !editCodeAttributeType.getValues().getValue().isEmpty()) {
                            timaticInfo.setReturnDate(CommonUtil.ddMMMyyToStandard(editCodeAttributeType.getValues().getValue().get(0)));
                        }
                    }
                }
            }
        }
    }
}
