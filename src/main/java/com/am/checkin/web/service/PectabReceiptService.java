package com.am.checkin.web.service;

import com.aeromexico.commons.model.AuthorizationResult;
import com.aeromexico.commons.model.CardToken;
import com.aeromexico.commons.model.CreditCardPaymentRequest;
import com.aeromexico.commons.model.CurrencyValue;
import com.aeromexico.commons.model.FormsOfPaymentWrapper;
import com.aeromexico.commons.model.IssuedEmds;
import com.aeromexico.commons.model.PectabReceipt;
import com.aeromexico.commons.model.PectabReceiptData;
import com.aeromexico.commons.model.PectabReceiptLabels;
import com.aeromexico.commons.model.PurchaseOrder;
import com.aeromexico.commons.model.ReceiptEmdsWrapper;
import com.aeromexico.commons.model.UatpPaymentRequest;
import com.aeromexico.commons.model.utils.CurrencyUtil;
import com.aeromexico.commons.web.types.CreditCardType;
import com.aeromexico.commons.web.types.EmdReceiptType;
import com.aeromexico.commons.web.types.LanguageType;
import com.aeromexico.commons.web.util.MaskCreditCardUtil;
import com.aeromexico.commons.web.util.TimeUtil;
import com.am.checkin.web.messages.ReceiptPectabMessageProvider;
import java.math.BigDecimal;
import java.util.*;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adrian
 */
@Named
@ApplicationScoped
public class PectabReceiptService {

    private static final Logger LOG = LoggerFactory.getLogger(PectabReceiptService.class);
    private static final String PECATB_DATA_EN = getPectabDataEn();
    private static final String PECTAB_DATA_ES = getPectabDataEs();

    private static String getPectabDataEn() {
        int row = 1;
        StringBuilder sb = new StringBuilder();
        sb.append("PT^^?B9Z^@@^C?ATK^C?ACI^C?ABD");
        sb.append("^").append(twoDigitFormat(row++)).append("01110112011301210122012301310132013301410142014301");
        sb.append("^").append(twoDigitFormat(row++)).append("011160216031604160");
        sb.append("^").append(twoDigitFormat(row++)).append("20D01L");
        sb.append("^").append(twoDigitFormat(row++)).append("10B25L");
        sb.append("^").append(twoDigitFormat(row++)).append("40C20L");
        sb.append("^").append(twoDigitFormat(row++)).append("40D20L");
        sb.append("^").append(twoDigitFormat(row++)).append("08F01L");
        sb.append("^").append(twoDigitFormat(row++)).append("08F06L");
        sb.append("^").append(twoDigitFormat(row++)).append("06F12L");
        sb.append("^").append(twoDigitFormat(row++)).append("04F16L");
        sb.append("^").append(twoDigitFormat(row++)).append("20F22L");
        sb.append("^").append(twoDigitFormat(row++)).append("08F35L");
        sb.append("^").append(twoDigitFormat(row++)).append("10F40L");
        sb.append("^").append(twoDigitFormat(row++)).append("12H01C");
        sb.append("^").append(twoDigitFormat(row++)).append("06H13C");
        sb.append("^").append(twoDigitFormat(row++)).append("14J03L");
        sb.append("^").append(twoDigitFormat(row++)).append("10J11L");
        sb.append("^").append(twoDigitFormat(row++)).append("09J20L");
        sb.append("^").append(twoDigitFormat(row++)).append("16J26L");
        sb.append("^").append(twoDigitFormat(row++)).append("14K03L");
        sb.append("^").append(twoDigitFormat(row++)).append("10K11L");
        sb.append("^").append(twoDigitFormat(row++)).append("09K20L");
        sb.append("^").append(twoDigitFormat(row++)).append("16K26L");
        sb.append("^").append(twoDigitFormat(row++)).append("14L03L");
        sb.append("^").append(twoDigitFormat(row++)).append("10L11L");
        sb.append("^").append(twoDigitFormat(row++)).append("09L20L");
        sb.append("^").append(twoDigitFormat(row++)).append("16L26L");
        sb.append("^").append(twoDigitFormat(row++)).append("09M20L");
        sb.append("^").append(twoDigitFormat(row++)).append("32M26L");
        sb.append("^").append(twoDigitFormat(row++)).append("40O19L");
        sb.append("^").append(twoDigitFormat(row++)).append("40P19L");
        sb.append("^").append(twoDigitFormat(row++)).append("73Q01L");
        sb.append("^").append(twoDigitFormat(row++)).append("73R01L");
        sb.append("^").append(twoDigitFormat(row++)).append("20D49L");
        sb.append("^").append(twoDigitFormat(row++)).append("25F49");
        sb.append("^").append(twoDigitFormat(row++)).append("42G49L");
        sb.append("^").append(twoDigitFormat(row++)).append("42H49L");
        sb.append("^").append(twoDigitFormat(row++)).append("42I49L");
        sb.append("^").append(twoDigitFormat(row++)).append("42J49L");
        sb.append("^").append(twoDigitFormat(row++)).append("25K49");
        sb.append("^").append(twoDigitFormat(row++)).append("42L49L");
        sb.append("^").append(twoDigitFormat(row++)).append("42M49L");
        sb.append("^").append(twoDigitFormat(row++)).append("42N49L");
        sb.append("^").append(twoDigitFormat(row++)).append("42O49L");
        sb.append("^").append(twoDigitFormat(row++)).append("25P49");
        sb.append("^").append(twoDigitFormat(row++)).append("42Q49L");
        sb.append("^").append(twoDigitFormat(row++)).append("42R49L");

        sb.append("^");

        return sb.toString();
    }

    private static String getPectabDataEs() {
        int row = 1;
        StringBuilder sb = new StringBuilder();
        sb.append("PT^^?B9Z^@@^C?ATK^C?ACI^C?ABD");
        sb.append("^").append(twoDigitFormat(row++)).append("01110112011301210122012301310132013301410142014301");
        sb.append("^").append(twoDigitFormat(row++)).append("011160216031604160");
        sb.append("^").append(twoDigitFormat(row++)).append("20D01L");
        sb.append("^").append(twoDigitFormat(row++)).append("10B25L");
        sb.append("^").append(twoDigitFormat(row++)).append("40C20L");
        sb.append("^").append(twoDigitFormat(row++)).append("40D20L");
        sb.append("^").append(twoDigitFormat(row++)).append("08F01L");
        sb.append("^").append(twoDigitFormat(row++)).append("08F06L");
        sb.append("^").append(twoDigitFormat(row++)).append("06F12L");
        sb.append("^").append(twoDigitFormat(row++)).append("04F16L");
        sb.append("^").append(twoDigitFormat(row++)).append("20F22L");
        sb.append("^").append(twoDigitFormat(row++)).append("08F35L");
        sb.append("^").append(twoDigitFormat(row++)).append("10F40L");
        sb.append("^").append(twoDigitFormat(row++)).append("12H01C");
        sb.append("^").append(twoDigitFormat(row++)).append("06H13C");
        sb.append("^").append(twoDigitFormat(row++)).append("14J03L");
        sb.append("^").append(twoDigitFormat(row++)).append("10J11L");
        sb.append("^").append(twoDigitFormat(row++)).append("09J20L");
        sb.append("^").append(twoDigitFormat(row++)).append("16J26L");
        sb.append("^").append(twoDigitFormat(row++)).append("14K03L");
        sb.append("^").append(twoDigitFormat(row++)).append("10K11L");
        sb.append("^").append(twoDigitFormat(row++)).append("09K20L");
        sb.append("^").append(twoDigitFormat(row++)).append("16K26L");
        sb.append("^").append(twoDigitFormat(row++)).append("14L03L");
        sb.append("^").append(twoDigitFormat(row++)).append("10L11L");
        sb.append("^").append(twoDigitFormat(row++)).append("09L20L");
        sb.append("^").append(twoDigitFormat(row++)).append("16L26L");
        sb.append("^").append(twoDigitFormat(row++)).append("09M20L");
        sb.append("^").append(twoDigitFormat(row++)).append("32M26L");
        sb.append("^").append(twoDigitFormat(row++)).append("40O19L");
        sb.append("^").append(twoDigitFormat(row++)).append("40P19L");
        sb.append("^").append(twoDigitFormat(row++)).append("73Q01L");
        sb.append("^").append(twoDigitFormat(row++)).append("73R01L");
        sb.append("^").append(twoDigitFormat(row++)).append("20D49L");
        sb.append("^").append(twoDigitFormat(row++)).append("25F49");
        sb.append("^").append(twoDigitFormat(row++)).append("42G49L");
        sb.append("^").append(twoDigitFormat(row++)).append("42H49L");
        sb.append("^").append(twoDigitFormat(row++)).append("42I49L");
        sb.append("^").append(twoDigitFormat(row++)).append("42J49L");
        sb.append("^").append(twoDigitFormat(row++)).append("25K49");
        sb.append("^").append(twoDigitFormat(row++)).append("42L49L");
        sb.append("^").append(twoDigitFormat(row++)).append("42M49L");
        sb.append("^").append(twoDigitFormat(row++)).append("42N49L");
        sb.append("^").append(twoDigitFormat(row++)).append("42O49L");
        sb.append("^").append(twoDigitFormat(row++)).append("25P49");
        sb.append("^").append(twoDigitFormat(row++)).append("42Q49L");
        sb.append("^").append(twoDigitFormat(row++)).append("42R49L");

        sb.append("^");

        return sb.toString();
    }

    @Inject
    private ReceiptPectabMessageProvider receiptPectabMessageProvider;

    public static String twoDigitFormat(int in) {
        return String.format("%02d", in);
    }

    public static String removeDoubleQuotes(String value) {
        if (value.startsWith("\"")) {
            value = value.substring(1);
        }

        if (value.endsWith("\"")) {
            value = value.substring(0, value.length() - 1);
        }

        return value;
    }

    public static String getCenterFixedString(String in, int length) {
        if (null == in) {
            return StringUtils.center("", length);
        } else if (in.length() > length) {
            return in.substring(0, length - 1);
        }

        return StringUtils.center(in, length);
    }

    public static String getRightFixedString(String in, int length) {
        if (null == in) {
            return StringUtils.rightPad("", length, " ");
        } else if (in.length() > length) {
            return in.substring(0, length - 1);
        }

        return StringUtils.rightPad(in, length, " ");
    }

    public static String getLeftFixedString(String in, int length) {
        if (null == in) {
            return StringUtils.leftPad("", length, " ");
        } else if (in.length() > length) {
            return in.substring(0, length - 1);
        }

        return StringUtils.leftPad(in, length, " ");
    }

    /**
     *
     * @param language
     * @return
     */
    private PectabReceiptLabels getLabels(LanguageType language) {
        ResourceBundle bundle = receiptPectabMessageProvider.getBundle(language);

        PectabReceiptLabels pectabReceiptLabels = new PectabReceiptLabels();

        try {
            pectabReceiptLabels.setAirline(bundle.getString("receipt.airline"));
            pectabReceiptLabels.setDescription(bundle.getString("receipt.description"));
            pectabReceiptLabels.setOther(bundle.getString("receipt.other"));
            pectabReceiptLabels.setUpgrades(bundle.getString("receipt.upgrades"));
            pectabReceiptLabels.setExtraWeight(bundle.getString("receipt.extra.weight"));
            pectabReceiptLabels.setBags(bundle.getString("receipt.bags"));
            pectabReceiptLabels.setSeats(bundle.getString("receipt.seats"));
            pectabReceiptLabels.setAddressOne(bundle.getString("receipt.address.one"));
            pectabReceiptLabels.setAddressTwo(bundle.getString("receipt.address.two"));
            pectabReceiptLabels.setCardNumber(bundle.getString("receipt.card.number"));
            pectabReceiptLabels.setExpirationDate(bundle.getString("receipt.card.expiration"));
            pectabReceiptLabels.setSale(bundle.getString("receipt.sale"));
            pectabReceiptLabels.setReservationCode(bundle.getString("receipt.reservation.code"));
            pectabReceiptLabels.setMerchant(bundle.getString("receipt.merchant"));
            pectabReceiptLabels.setInstallment(bundle.getString("receipt.installment"));
            pectabReceiptLabels.setAuthorization(bundle.getString("receipt.authorization"));
            pectabReceiptLabels.setAlabel(bundle.getString("receipt.alabel"));
            pectabReceiptLabels.setAprname(bundle.getString("receipt.aprname"));
            pectabReceiptLabels.setArqc(bundle.getString("receipt.arqc"));
            pectabReceiptLabels.setAid(bundle.getString("receipt.aid"));
            pectabReceiptLabels.setSignatureLegend(bundle.getString("receipt.signature.legend"));
            pectabReceiptLabels.setAgreeLegendOne(bundle.getString("receipt.agree.legend.one"));
            pectabReceiptLabels.setAgreeLegendTwo(bundle.getString("receipt.agree.legend.two"));
            pectabReceiptLabels.setAdminFee(bundle.getString("receipt.adminfee"));

            return pectabReceiptLabels;

        } catch (Exception ex) {
            //ignore
            return new PectabReceiptLabels();
        }
    }

    public static enum ExtraParams {
        ALABEL,
        APRNAME,
        ARQC,
        AID;
    }

    public static enum Tokens {
        Q9,
        B2,
        B3;
    }

    /**
     *
     * @param tokens
     * @return
     */
    private static Map<String, String> getExtraInfo(List<CardToken> tokens) {
        Map<String, String> extraInfo = new HashMap<>();

        extraInfo.put(ExtraParams.ALABEL.toString(), "");
        extraInfo.put(ExtraParams.APRNAME.toString(), "");
        extraInfo.put(ExtraParams.ARQC.toString(), "");
        extraInfo.put(ExtraParams.AID.toString(), "");

        tokens.stream().forEach((token) -> {
            if (Tokens.Q9.toString().equalsIgnoreCase(token.getName())) {
                try {
                    extraInfo.put(ExtraParams.ALABEL.toString(), token.getValue().substring(10, 26).trim());
                } catch (Exception ex) {
                    //
                }

                try {
                    extraInfo.put(ExtraParams.APRNAME.toString(), token.getValue().substring(26, 42).trim());
                } catch (Exception ex) {
                    //
                }
            } else if (Tokens.B2.toString().equalsIgnoreCase(token.getName())) {
                try {
                    extraInfo.put(ExtraParams.ARQC.toString(), token.getValue().substring(30, 46).trim());
                } catch (Exception ex) {
                    //
                }
            } else if (Tokens.B3.toString().equalsIgnoreCase(token.getName())) {
                try {
                    String aid = token.getValue().substring(58).trim();
                    extraInfo.put(ExtraParams.AID.toString(), aid.substring(0, 16));
                } catch (Exception ex) {
                    //
                }
            }
        });

        return extraInfo;
    }

    /**
     *
     * @param purchaseOrder
     * @param authorizationResult
     * @param issuedEmds
     * @param language
     * @param recordLocator
     * @param cartId
     * @param pos
     * @return
     */
    public static PectabReceiptData getPectabReceiptData(
            PurchaseOrder purchaseOrder,
            AuthorizationResult authorizationResult,
            IssuedEmds issuedEmds,
            LanguageType language,
            String recordLocator,
            String cartId,
            String pos
    ) {

        Date d = new Date();

        String datetime = TimeUtil.getStrTime(d.getTime(), "dd-MMM-yyyy HH:mm:ss", language.toString());
        String folio = "";
        String cardNumber = "";
        String expiration = "";
        String cardName = "";
        String total = "";
        String cardHolder = "";
        String alabel = "";
        String aprname = "";
        String arqc = "";
        String aid = "";
        String merchant = authorizationResult.getMerchantAccountNumber();
        String installment = "00";
        String authorization = authorizationResult.getApprovalCode();
        boolean digitalSignatureProvided = false;

        FormsOfPaymentWrapper formsOfPaymentWrapper;
        formsOfPaymentWrapper = new FormsOfPaymentWrapper(
                purchaseOrder.getPaymentInfo()
        );

        CurrencyValue totalInCart = purchaseOrder.getPaymentAmount();
        MaskCreditCardUtil maskCreditCard = new MaskCreditCardUtil();

        if (!formsOfPaymentWrapper.getCreditCardPaymentRequestList().isEmpty()) {
            CreditCardPaymentRequest creditCardPaymentRequest;
            creditCardPaymentRequest = formsOfPaymentWrapper.getCreditCardPaymentRequestList().get(0);
            try {
                cardNumber = maskCreditCard.maskCardNumber(creditCardPaymentRequest.getCcNumber()).substring(8);// "XXXX1110";            
                try {
                    expiration = creditCardPaymentRequest.isDefaultExpirationDateUsed() ? "XXXX" : creditCardPaymentRequest.getExpiryDate().substring(5) + creditCardPaymentRequest.getExpiryDate().substring(2, 4);// "1015" from 2015-10;
                } catch (Exception ex) {
                    expiration = "XXXX";
                }
                cardName = creditCardPaymentRequest.getCardType().getFullName();

                BigDecimal t = CurrencyUtil.getAmount(totalInCart.getTotal(), totalInCart.getCurrencyCode());

                total = creditCardPaymentRequest.getPaymentAmount().getCurrencyCode() + " " + t;
                cardHolder = creditCardPaymentRequest.getCardHolderName();

                try {

                    if (null != creditCardPaymentRequest.getCardReader()) {

                        digitalSignatureProvided = creditCardPaymentRequest.getCardReader().isDigitalSignatureProvided();

                        Map<String, String> extraInfo = getExtraInfo(creditCardPaymentRequest.getCardReader().getCardTokenList());

                        try {
                            alabel = extraInfo.get(ExtraParams.ALABEL.toString());
                        } catch (Exception ex) {
                            //ignore
                            LOG.error(recordLocator, cartId, "Error reading ALABEL card: " + ex.getMessage());
                        }

                        try {
                            aprname = extraInfo.get(ExtraParams.APRNAME.toString());
                        } catch (Exception ex) {
                            //ignore
                            LOG.error(recordLocator, cartId, "Error reading APRNAME card: " + ex.getMessage());
                        }

                        try {
                            arqc = extraInfo.get(ExtraParams.ARQC.toString());
                        } catch (Exception ex) {
                            //ignore
                            LOG.error(recordLocator, cartId, "Error reading ARQC card: " + ex.getMessage());
                        }

                        try {
                            aid = extraInfo.get(ExtraParams.AID.toString());
                        } catch (Exception ex) {
                            //ignore
                            LOG.error(recordLocator, cartId, "Error reading AID card: " + ex.getMessage());
                        }

                        LOG.info(recordLocator, cartId, "Error reading AID card: ");
                    }
                } catch (Exception ex) {
                    //ignore
                    LOG.error(recordLocator, cartId, "Error reading extra fields for card: " + ex.getMessage());
                }
            } catch (Exception ex) {
                //ignore
                LOG.error(recordLocator, cartId, "Error reading card: " + ex.getMessage());
            }
        } else if (!formsOfPaymentWrapper.getUatpPaymentRequestList().isEmpty()) {
            UatpPaymentRequest uatpPaymentRequest;
            uatpPaymentRequest = formsOfPaymentWrapper.getUatpPaymentRequestList().get(0);
            try {
                cardNumber = maskCreditCard.maskCardNumber(uatpPaymentRequest.getCardCode()).substring(8);// "XXXX1110";            
                try {
                    expiration = uatpPaymentRequest.isDefaultExpirationDateUsed() ? "XXXX" : uatpPaymentRequest.getExpiryDate().substring(5) + uatpPaymentRequest.getExpiryDate().substring(2, 4);// "1015" from 2015-10;
                } catch (Exception ex) {
                    expiration = "XXXX";
                }
                cardName = CreditCardType.UATP.getFullName();

                BigDecimal t = CurrencyUtil.getAmount(totalInCart.getTotal(), totalInCart.getCurrencyCode());

                total = totalInCart.getCurrencyCode() + " " + t;
                cardHolder = uatpPaymentRequest.getEmail();

                try {

                    if (null != uatpPaymentRequest.getCardReader()) {

                        digitalSignatureProvided = uatpPaymentRequest.getCardReader().isDigitalSignatureProvided();

                        Map<String, String> extraInfo = getExtraInfo(uatpPaymentRequest.getCardReader().getCardTokenList());

                        try {
                            alabel = extraInfo.get(ExtraParams.ALABEL.toString());
                        } catch (Exception ex) {
                            //ignore
                            LOG.error(recordLocator, cartId, "Error reading ALABEL card: " + ex.getMessage());
                        }

                        try {
                            aprname = extraInfo.get(ExtraParams.APRNAME.toString());
                        } catch (Exception ex) {
                            //ignore
                            LOG.error(recordLocator, cartId, "Error reading APRNAME card: " + ex.getMessage());
                        }

                        try {
                            arqc = extraInfo.get(ExtraParams.ARQC.toString());
                        } catch (Exception ex) {
                            //ignore
                            LOG.error(recordLocator, cartId, "Error reading ARQC card: " + ex.getMessage());
                        }

                        try {
                            aid = extraInfo.get(ExtraParams.AID.toString());
                        } catch (Exception ex) {
                            //ignore
                            LOG.error(recordLocator, cartId, "Error reading AID card: " + ex.getMessage());
                        }

                        LOG.info(recordLocator, cartId, "Error reading AID card: ");
                    }
                } catch (Exception ex) {
                    //ignore
                    LOG.error(recordLocator, cartId, "Error reading extra fields for card: " + ex.getMessage());
                }
            } catch (Exception ex) {
                //ignore
                LOG.error(recordLocator, cartId, "Error reading card: " + ex.getMessage());
            }
        }

        ReceiptEmdsWrapper receiptEmdsWrapper = new ReceiptEmdsWrapper();

        receiptEmdsWrapper.setWeightUnit(issuedEmds.getWeightUnit());
        receiptEmdsWrapper.setTotal(issuedEmds.getIssuedEmdsTotal(EmdReceiptType.EMD_OTHER), EmdReceiptType.EMD_OTHER);
        receiptEmdsWrapper.setTotal(issuedEmds.getIssuedEmdsTotal(EmdReceiptType.EMD_ADMIN_FEE), EmdReceiptType.EMD_ADMIN_FEE);
        receiptEmdsWrapper.setTotal(issuedEmds.getIssuedEmdsTotal(EmdReceiptType.EMD_EXTRA_BAGGAGE), EmdReceiptType.EMD_EXTRA_BAGGAGE);        
        receiptEmdsWrapper.setTotal(issuedEmds.getIssuedEmdsTotal(EmdReceiptType.EMD_EXTRA_WEIGHT), EmdReceiptType.EMD_EXTRA_WEIGHT);
        receiptEmdsWrapper.setTotal(issuedEmds.getIssuedEmdsTotal(EmdReceiptType.EMD_UPGRADE), EmdReceiptType.EMD_UPGRADE);
        receiptEmdsWrapper.setTotal(issuedEmds.getIssuedEmdsTotal(EmdReceiptType.EMD_SEAT), EmdReceiptType.EMD_SEAT);

        receiptEmdsWrapper.setQty(issuedEmds.getIssuedEmdsQty(EmdReceiptType.EMD_OTHER), EmdReceiptType.EMD_OTHER);
        receiptEmdsWrapper.setQty(issuedEmds.getIssuedEmdsQty(EmdReceiptType.EMD_ADMIN_FEE), EmdReceiptType.EMD_ADMIN_FEE);
        receiptEmdsWrapper.setQty(issuedEmds.getIssuedEmdsQty(EmdReceiptType.EMD_EXTRA_BAGGAGE), EmdReceiptType.EMD_EXTRA_BAGGAGE);
        receiptEmdsWrapper.setQty(issuedEmds.getIssuedEmdsQty(EmdReceiptType.EMD_EXTRA_WEIGHT), EmdReceiptType.EMD_EXTRA_WEIGHT);
        receiptEmdsWrapper.setQty(issuedEmds.getIssuedEmdsQty(EmdReceiptType.EMD_UPGRADE), EmdReceiptType.EMD_UPGRADE);
        receiptEmdsWrapper.setQty(issuedEmds.getIssuedEmdsQty(EmdReceiptType.EMD_SEAT), EmdReceiptType.EMD_SEAT);

        String emdOtherStr = issuedEmds.getIssuedEmdsStr(EmdReceiptType.EMD_OTHER);
        if (null != emdOtherStr && !emdOtherStr.isEmpty()) {
            if (emdOtherStr.contains("|")) {
                String[] otherStr = emdOtherStr.split("\\|");
                for (int i = 0; i < otherStr.length; i++) {
                    receiptEmdsWrapper.putString(otherStr[i], i, EmdReceiptType.EMD_OTHER, pos);
                }
            } else {
                receiptEmdsWrapper.putString(emdOtherStr, 0, EmdReceiptType.EMD_OTHER, pos);
            }
        }

        String emdEWStr = issuedEmds.getIssuedEmdsStr(EmdReceiptType.EMD_EXTRA_WEIGHT);
        if (null != emdEWStr && !emdEWStr.isEmpty()) {
            if (emdEWStr.contains("|")) {
                String[] otherStr = emdEWStr.split("\\|");
                for (int i = 0; i < otherStr.length; i++) {
                    receiptEmdsWrapper.putString(otherStr[i], i, EmdReceiptType.EMD_EXTRA_WEIGHT, pos);
                }
            } else {
                receiptEmdsWrapper.putString(emdEWStr, 0, EmdReceiptType.EMD_EXTRA_WEIGHT, pos);
            }
        }

        String emdUPStr = issuedEmds.getIssuedEmdsStr(EmdReceiptType.EMD_UPGRADE);
        if (null != emdUPStr && !emdUPStr.isEmpty()) {
            if (emdUPStr.contains("|")) {
                String[] otherStr = emdUPStr.split("\\|");
                for (int i = 0; i < otherStr.length; i++) {
                    receiptEmdsWrapper.putString(otherStr[i], i, EmdReceiptType.EMD_UPGRADE, pos);
                }
            } else {
                receiptEmdsWrapper.putString(emdUPStr, 0, EmdReceiptType.EMD_UPGRADE, pos);
            }
        }

        String emdBGStr = issuedEmds.getIssuedEmdsStr(EmdReceiptType.EMD_EXTRA_BAGGAGE);
        if (null != emdBGStr && !emdBGStr.isEmpty()) {
            if (emdBGStr.contains("|")) {
                String[] otherStr = emdBGStr.split("\\|");
                for (int i = 0; i < otherStr.length; i++) {
                    receiptEmdsWrapper.putString(otherStr[i], i, EmdReceiptType.EMD_EXTRA_BAGGAGE, pos);
                }
            } else {
                receiptEmdsWrapper.putString(emdBGStr, 0, EmdReceiptType.EMD_EXTRA_BAGGAGE, pos);
            }
        }
        
        String emdAdminStr = issuedEmds.getIssuedEmdsStr(EmdReceiptType.EMD_ADMIN_FEE);
        if (null != emdAdminStr && !emdAdminStr.isEmpty()) {
            if (emdAdminStr.contains("|")) {
                String[] otherStr = emdAdminStr.split("\\|");
                for (int i = 0; i < otherStr.length; i++) {
                    receiptEmdsWrapper.putString(otherStr[i], i, EmdReceiptType.EMD_ADMIN_FEE, pos);
                }
            } else {
                receiptEmdsWrapper.putString(emdAdminStr, 0, EmdReceiptType.EMD_ADMIN_FEE, pos);
            }
        }

        String emdSAStr = issuedEmds.getIssuedEmdsStr(EmdReceiptType.EMD_SEAT);
        if (null != emdSAStr && !emdSAStr.isEmpty()) {
            if (emdSAStr.contains("|")) {
                String[] otherStr = emdSAStr.split("\\|");
                for (int i = 0; i < otherStr.length; i++) {
                    receiptEmdsWrapper.putString(otherStr[i], i, EmdReceiptType.EMD_SEAT, pos);
                }
            } else {
                receiptEmdsWrapper.putString(emdSAStr, 0, EmdReceiptType.EMD_SEAT, pos);
            }
        }

        PectabReceiptData pectabReceiptData = new PectabReceiptData(
                datetime, folio, cardNumber, expiration, cardName,
                total, recordLocator, cardHolder, alabel, aprname, arqc,
                aid, merchant, installment, authorization, digitalSignatureProvided,
                receiptEmdsWrapper
        );

        return pectabReceiptData;
    }

    /**
     *
     * @param purchaseOrder
     * @param authorizationResult
     * @param issuedEmds
     * @param language
     * @param currencyCode
     * @param recordLocator
     * @param cartId
     * @param pos
     * @return
     */
    public PectabReceipt getPectabReceipt(
            PurchaseOrder purchaseOrder,
            AuthorizationResult authorizationResult,
            IssuedEmds issuedEmds,
            LanguageType language,
            String currencyCode,
            String recordLocator,
            String cartId,
            String pos
    ) {
        PectabReceiptData pectabReceiptData = getPectabReceiptData(
                purchaseOrder,
                authorizationResult,
                issuedEmds,
                language,
                recordLocator,
                cartId,
                pos
        );
        return getPectabReceipt(pectabReceiptData, language, currencyCode);
    }

    /**
     *
     * @param pectabReceiptData
     * @param language
     * @param currencyCode
     * @return
     */
    public PectabReceipt getPectabReceipt(
            PectabReceiptData pectabReceiptData,
            LanguageType language,
            String currencyCode
    ) {

        String pectabData;
        if (null != language) {
            switch (language) {
                case ES:
                    pectabData = PECTAB_DATA_ES;
                    break;
                case EN:
                    pectabData = PECATB_DATA_EN;
                    break;
                default:
                    pectabData = PECTAB_DATA_ES;
                    break;
            }
        } else {
            //default spanish
            pectabData = PECTAB_DATA_ES;
        }

        PectabReceipt pectabReceipt = new PectabReceipt();
        pectabReceipt.setPectab(pectabData);

        PectabReceiptLabels pectabReceiptLabels = getLabels(language);

        int row = 1;

        StringBuilder sb = new StringBuilder();
        sb.append("CP^A^01B^CP^1C01");
        sb.append("^").append(twoDigitFormat(row++)).append("W");
        sb.append("^").append(twoDigitFormat(row++)).append("1");
        sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getDatetime());
        sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptLabels.getAirline());
        sb.append("^").append(twoDigitFormat(row++)).append(getCenterFixedString(pectabReceiptLabels.getAddressOne(), 40));
        sb.append("^").append(twoDigitFormat(row++)).append(getCenterFixedString(pectabReceiptLabels.getAddressTwo(), 40));
        sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptLabels.getCardNumber());
        sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getCardNumber());
        sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptLabels.getExpirationDate());
        sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getExpiration());
        sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getCardName());
        sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptLabels.getSale());
        sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getTotal());
        sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptLabels.getReservationCode());
        sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getReservationCode());
        sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptLabels.getMerchant());
        sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getMerchant());
        sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptLabels.getAlabel());
        sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getAlabel());
        sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptLabels.getInstallment());
        sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getInstallment());
        sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptLabels.getAprname());
        sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getAprname());
        sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptLabels.getAuthorization());
        sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getAuthorization());
        sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptLabels.getArqc());
        sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getArqc());
        sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptLabels.getAid());
        sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getAid());
        sb.append("^").append(twoDigitFormat(row++)).append(getCenterFixedString(pectabReceiptData.getCardHolder(), 40));
        sb.append("^").append(twoDigitFormat(row++)).append(getCenterFixedString((pectabReceiptData.isDigitalSignatureProvided() ? pectabReceiptLabels.getSignatureLegend() : ""), 40));
        sb.append("^").append(twoDigitFormat(row++)).append(getCenterFixedString(pectabReceiptLabels.getAgreeLegendOne(), 73));
        sb.append("^").append(twoDigitFormat(row++)).append(getCenterFixedString(pectabReceiptLabels.getAgreeLegendTwo(), 73));
        sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptLabels.getDescription());

        if (pectabReceiptData.getReceiptEmdsWrapper().getQty(EmdReceiptType.EMD_SEAT) > 0) {
            sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getReceiptEmdsWrapper().getQty(EmdReceiptType.EMD_SEAT)).append(" ").append(pectabReceiptLabels.getSeats()).append(" - ").append(pectabReceiptData.getReceiptEmdsWrapper().getTotal(EmdReceiptType.EMD_SEAT)).append(currencyCode);
            sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getReceiptEmdsWrapper().getString(0, EmdReceiptType.EMD_SEAT));
            sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getReceiptEmdsWrapper().getString(1, EmdReceiptType.EMD_SEAT));
            sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getReceiptEmdsWrapper().getString(2, EmdReceiptType.EMD_SEAT));
            sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getReceiptEmdsWrapper().getString(3, EmdReceiptType.EMD_SEAT));
        }

        if (pectabReceiptData.getReceiptEmdsWrapper().getQty(EmdReceiptType.EMD_ADMIN_FEE) > 0) {
            sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getReceiptEmdsWrapper().getQty(EmdReceiptType.EMD_ADMIN_FEE)).append(" ").append(pectabReceiptLabels.getAdminFee()).append(" - ").append(pectabReceiptData.getReceiptEmdsWrapper().getTotal(EmdReceiptType.EMD_ADMIN_FEE)).append(currencyCode);
            sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getReceiptEmdsWrapper().getString(0, EmdReceiptType.EMD_ADMIN_FEE));
            sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getReceiptEmdsWrapper().getString(1, EmdReceiptType.EMD_ADMIN_FEE));
            sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getReceiptEmdsWrapper().getString(2, EmdReceiptType.EMD_ADMIN_FEE));
            sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getReceiptEmdsWrapper().getString(3, EmdReceiptType.EMD_ADMIN_FEE));
        }

        if (pectabReceiptData.getReceiptEmdsWrapper().getQty(EmdReceiptType.EMD_EXTRA_BAGGAGE) > 0) {
            sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getReceiptEmdsWrapper().getQty(EmdReceiptType.EMD_EXTRA_BAGGAGE)).append(" ").append(pectabReceiptLabels.getBags()).append(" - ").append(pectabReceiptData.getReceiptEmdsWrapper().getTotal(EmdReceiptType.EMD_EXTRA_BAGGAGE)).append(currencyCode);
            sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getReceiptEmdsWrapper().getString(0, EmdReceiptType.EMD_EXTRA_BAGGAGE));
            sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getReceiptEmdsWrapper().getString(1, EmdReceiptType.EMD_EXTRA_BAGGAGE));
            sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getReceiptEmdsWrapper().getString(2, EmdReceiptType.EMD_EXTRA_BAGGAGE));
            sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getReceiptEmdsWrapper().getString(3, EmdReceiptType.EMD_EXTRA_BAGGAGE));
        }

        if (pectabReceiptData.getReceiptEmdsWrapper().getQty(EmdReceiptType.EMD_CARRY_ON_BAGGAGE) > 0) {
            sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getReceiptEmdsWrapper().getQty(EmdReceiptType.EMD_CARRY_ON_BAGGAGE)).append(" ").append(pectabReceiptLabels.getCarryOn()).append(" - ").append(pectabReceiptData.getReceiptEmdsWrapper().getTotal(EmdReceiptType.EMD_EXTRA_BAGGAGE)).append(currencyCode);
            sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getReceiptEmdsWrapper().getString(0, EmdReceiptType.EMD_CARRY_ON_BAGGAGE));
            sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getReceiptEmdsWrapper().getString(1, EmdReceiptType.EMD_CARRY_ON_BAGGAGE));
            sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getReceiptEmdsWrapper().getString(2, EmdReceiptType.EMD_CARRY_ON_BAGGAGE));
            sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getReceiptEmdsWrapper().getString(3, EmdReceiptType.EMD_CARRY_ON_BAGGAGE));
        }

        if (pectabReceiptData.getReceiptEmdsWrapper().getQty(EmdReceiptType.EMD_EXTRA_WEIGHT) > 0) {
            String weightUnit = pectabReceiptData.getReceiptEmdsWrapper().getWeightUnit();
            sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getReceiptEmdsWrapper().getQty(EmdReceiptType.EMD_EXTRA_WEIGHT)).append(" ").append(weightUnit).append(" ").append(pectabReceiptLabels.getExtraWeight()).append(" - ").append(pectabReceiptData.getReceiptEmdsWrapper().getTotal(EmdReceiptType.EMD_EXTRA_WEIGHT)).append(currencyCode);
            sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getReceiptEmdsWrapper().getString(0, EmdReceiptType.EMD_EXTRA_WEIGHT));
            sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getReceiptEmdsWrapper().getString(1, EmdReceiptType.EMD_EXTRA_WEIGHT));
            sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getReceiptEmdsWrapper().getString(2, EmdReceiptType.EMD_EXTRA_WEIGHT));
            sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getReceiptEmdsWrapper().getString(3, EmdReceiptType.EMD_EXTRA_WEIGHT));
        }

        if (pectabReceiptData.getReceiptEmdsWrapper().getQty(EmdReceiptType.EMD_UPGRADE) > 0) {
            sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getReceiptEmdsWrapper().getQty(EmdReceiptType.EMD_UPGRADE)).append(" ").append(pectabReceiptLabels.getUpgrades()).append(" - ").append(pectabReceiptData.getReceiptEmdsWrapper().getTotal(EmdReceiptType.EMD_UPGRADE)).append(currencyCode);
            sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getReceiptEmdsWrapper().getString(0, EmdReceiptType.EMD_UPGRADE));
            sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getReceiptEmdsWrapper().getString(1, EmdReceiptType.EMD_UPGRADE));
            sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getReceiptEmdsWrapper().getString(2, EmdReceiptType.EMD_UPGRADE));
            sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getReceiptEmdsWrapper().getString(3, EmdReceiptType.EMD_UPGRADE));
        }

        if (pectabReceiptData.getReceiptEmdsWrapper().getQty(EmdReceiptType.EMD_OTHER) > 0) {
            sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getReceiptEmdsWrapper().getQty(EmdReceiptType.EMD_OTHER)).append(" ").append(pectabReceiptLabels.getOther()).append(" - ").append(pectabReceiptData.getReceiptEmdsWrapper().getTotal(EmdReceiptType.EMD_OTHER)).append(currencyCode);
            sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getReceiptEmdsWrapper().getString(0, EmdReceiptType.EMD_OTHER));
            sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getReceiptEmdsWrapper().getString(1, EmdReceiptType.EMD_OTHER));
            sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getReceiptEmdsWrapper().getString(2, EmdReceiptType.EMD_OTHER));
            sb.append("^").append(twoDigitFormat(row++)).append(pectabReceiptData.getReceiptEmdsWrapper().getString(3, EmdReceiptType.EMD_OTHER));
        }

        sb.append("^");

        pectabReceipt.setReceipt(sb.toString());

        return pectabReceipt;
    }

    /**
     * @return the receiptPectabMessageProvider
     */
    public ReceiptPectabMessageProvider getReceiptPectabMessageProvider() {
        return receiptPectabMessageProvider;
    }

    /**
     * @param receiptPectabMessageProvider the receiptPectabMessageProvider to
     * set
     */
    public void setReceiptPectabMessageProvider(ReceiptPectabMessageProvider receiptPectabMessageProvider) {
        this.receiptPectabMessageProvider = receiptPectabMessageProvider;
    }

}
