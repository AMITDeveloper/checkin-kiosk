package com.am.checkin.web.service;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.AuthorizationPaymentConfig;
import com.aeromexico.commons.model.SystemPropertiesFactory;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.types.PosType;
import com.aeromexico.dao.client.exception.MongoDisconnectException;
import com.aeromexico.sabre.api.sabrecommand.AMSabreCommandService;
import com.aeromexico.sabre.api.session.common.ServiceCall;
import com.aeromexico.sabre.api.session.exception.SabreEmptyResponseException;
import com.aeromexico.sabre.api.session.exception.SabreLayerUnavailableException;
import com.aeromexico.sabre.api.sessionpool.factory.SabreSession;
import com.google.gson.Gson;
import com.sabre.webservices.sabrexml._2003._07.SabreCommandLLSRS;
import org.aeromexico.commons.exception.config.ReadProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * @author adrian
 */
@Named
@ApplicationScoped
public class CommandService {

    private static final Logger LOG = LoggerFactory.getLogger(CommandService.class);

    @Inject
    private AMSabreCommandService sabreCommandService;

    @Inject
    private SystemPropertiesFactory systemPropertiesFactory;

    @Inject
    @Named("CheckInSetUpConfigFactoryWrapper")
    private CheckInSetUpConfigFactoryWrapper setUpConfigFactory;

    @Inject
    private ReadProperties prop;

    @PostConstruct
    public void init() {
        if (null == setUpConfigFactory) {
            setUpConfigFactory = new CheckInSetUpConfigFactoryWrapper();
        }
    }

    /**
     * @param sabreSession
     * @param forceSetParameters
     * @param store
     * @param pos
     * @param serviceCallList
     * @throws Exception
     */
    public void setParameters(
            SabreSession sabreSession,
            boolean forceSetParameters,
            String store,
            String pos,
            List<ServiceCall> serviceCallList
    ) throws Exception {

        try {
            AuthorizationPaymentConfig authorizationPaymentConfig = setUpConfigFactory.getInstance().getAuthorizationPaymentConfig(store, pos);

            String dutyCodeCommand = setUpConfigFactory.getInstance().getDutyCodeCommand();
            String dutyCode = setUpConfigFactory.getInstance().getDutyCode();
            String cityCodeCommand = setUpConfigFactory.getInstance().getCityCodeCommand();
            String cityCode = authorizationPaymentConfig.getCityCode();
            String printerCodeCommand = setUpConfigFactory.getInstance().getTicketPrinterCommand();
            String printerCode = setUpConfigFactory.getInstance().getTicketLNIATA();
            String stationNumber = authorizationPaymentConfig.getStationNumber();

            sabreCommandService.setParameters(
                    sabreSession, dutyCode, dutyCodeCommand,
                    cityCode, cityCodeCommand,
                    printerCodeCommand, printerCode,
                    stationNumber, serviceCallList
            );
        } catch (MongoDisconnectException me) {
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(pos)) {
                LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
                throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getCode(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getActionable(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getMessage(),
                        Response.Status.INTERNAL_SERVER_ERROR);
            } else {
                LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
                throw me;
            }
        } catch (SabreLayerUnavailableException se) {
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(pos)) {
                LOG.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_22110301,"CommandService.java:102", se.getMessage());
                throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getCode(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getActionable(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getMessage(),
                        Response.Status.INTERNAL_SERVER_ERROR);
            } else {
                LOG.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_12110301,"CommandService.java:108", se.getMessage());
                throw se;
            }
        }catch (SabreEmptyResponseException ser) {
            LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110302).toString(), ser.fillInStackTrace());
            throw ser;
        }
    }

    /**
     * @param command
     * @param output
     * @param recordLocator
     * @param cartId
     * @param serviceCallList
     * @return
     * @throws Exception
     */
    public SabreCommandLLSRS executeCommand(
            String command, String output,
            String recordLocator, String cartId,
            List<ServiceCall> serviceCallList
    ) throws Exception {
        try {
            SabreCommandLLSRS sabreCommandLLSRS = sabreCommandService.sabreCommand(command, output);

            log(recordLocator, cartId, "Command Response: " + command, new Gson().toJson(sabreCommandLLSRS));

            if (sabreCommandLLSRS == null) {
                logError(recordLocator, cartId, "Command host response null", "Command: " + command);
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNKNOWN_CODE, "Command host response null");
            } else if (sabreCommandLLSRS.getErrorRS() != null) {
                String msg = sabreCommandLLSRS.getErrorRS().getErrors().getError().getErrorCode() + " | " + sabreCommandLLSRS.getErrorRS().getErrors().getError().getErrorMessage();

                logError(recordLocator, cartId, "Error executing command: " + command, msg);

                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNKNOWN_CODE, msg);
            }

            return sabreCommandLLSRS;

        } catch (SabreLayerUnavailableException se) {
            throw se;
        }catch (SabreEmptyResponseException ser) {
            LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110302).toString(), ser.fillInStackTrace());
            throw ser;
        } catch (GenericException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNKNOWN_CODE, ex.getMessage());
        }
    }

    /**
     * @param sabreSession
     * @param command
     * @param output
     * @param recordLocator
     * @param cartId
     * @param serviceCallList
     * @return
     * @throws Exception
     */
    public SabreCommandLLSRS executeCommand(
            SabreSession sabreSession,
            String command,
            String output,
            String recordLocator,
            String cartId,
            List<ServiceCall> serviceCallList
    ) {
        try {
            SabreCommandLLSRS sabreCommandLLSRS = sabreCommandService.sabreCommand(
                    sabreSession, command, output);

            log(recordLocator, cartId, "Command Response: " + command, new Gson().toJson(sabreCommandLLSRS));

            if (sabreCommandLLSRS == null) {
                logError(recordLocator, cartId, "Command host response null", "Command: " + command);
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNKNOWN_CODE, "Command host response null");
            } else if (sabreCommandLLSRS.getErrorRS() != null) {
                String msg = sabreCommandLLSRS.getErrorRS().getErrors().getError().getErrorCode() + " | " + sabreCommandLLSRS.getErrorRS().getErrors().getError().getErrorMessage();

                logError(recordLocator, cartId, "Error executing command: " + command, msg);

                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNKNOWN_CODE, msg);
            }

            return sabreCommandLLSRS;

        } catch (SabreLayerUnavailableException se) {
            throw se;
        }catch (SabreEmptyResponseException ser) {
            LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110302).toString(), ser.fillInStackTrace());
            throw ser;
        } catch (GenericException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNKNOWN_CODE, ex.getMessage());
        }
    }

    /**
     * @param recordLocator
     * @param cartId
     * @param title
     * @param msg
     */
    public void log(String recordLocator, String cartId, String title, String msg) {
        try {

            StringBuilder sb = new StringBuilder();
            sb.append("\n").append("Command Service: ").append("\n");
            sb.append("recordLocator: ").append(recordLocator).append("\n");
            sb.append("cartId: ").append(cartId).append("\n");
            sb.append("title: ").append(title).append("\n");
            sb.append("msg: ").append(msg).append("\n");
            LOG.info(sb.toString());
        } catch (Exception ex) {
            //ignore
        }
    }

    /**
     * @param recordLocator
     * @param cartId
     * @param title
     * @param msg
     */
    public void logDebug(String recordLocator, String cartId, String title, String msg) {
        try {

            StringBuilder sb = new StringBuilder();
            sb.append("\n").append("Command Service: ").append("\n");
            sb.append("recordLocator: ").append(recordLocator).append("\n");
            sb.append("cartId: ").append(cartId).append("\n");
            sb.append("title: ").append(title).append("\n");
            sb.append("msg: ").append(msg).append("\n");
            LOG.debug(sb.toString());
        } catch (Exception ex) {
            //ignore
        }
    }

    /**
     * @param recordLocator
     * @param cartId
     * @param title
     * @param msg
     */
    public void logError(String recordLocator, String cartId, String title, String msg) {
        try {

            StringBuilder sb = new StringBuilder();
            sb.append("\n").append("Command Service: ").append("\n");
            sb.append("recordLocator: ").append(recordLocator).append("\n");
            sb.append("cartId: ").append(cartId).append("\n");
            sb.append("title: ").append(title).append("\n");
            sb.append("msg: ").append(msg).append("\n");
            LOG.error(sb.toString());
        } catch (Exception ex) {
            //ignore
        }
    }
}
