package com.am.checkin.web.service;

import com.aeromexico.codes.ICodesGenerator;
import com.aeromexico.codes.impl.CodesGenerator;
import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.exception.model.NotPassengersForCheckinException;
import com.aeromexico.commons.exception.model.RetryCheckinException;
import com.aeromexico.commons.model.*;
import com.aeromexico.commons.model.rq.CheckInRQ;
import com.aeromexico.commons.model.rq.PurchaseOrderRQ;
import com.aeromexico.commons.model.rq.SeatmapCheckInRQ;
import com.aeromexico.commons.web.types.*;
import com.aeromexico.commons.web.util.Constants;
import com.aeromexico.commons.web.util.FilterPassengersUtil;
import com.aeromexico.commons.web.util.FlightNumberUtil;
import com.aeromexico.dao.services.CheckInServiceDao;
import com.aeromexico.dao.services.IAirportCodesDao;
import com.aeromexico.dao.services.PriorityCodesDaoCache;
import com.aeromexico.sabre.api.acs.AMCheckInPassService;
import com.aeromexico.sabre.api.acs.AMEditPassCharService;
import com.aeromexico.sabre.api.session.exception.SabreEmptyResponseException;
import com.aeromexico.sabre.api.session.exception.SabreLayerUnavailableException;
import com.aeromexico.sabre.api.sessionpool.factory.SabreSession;
import com.am.checkin.web.exception.model.NotEnoughSeatAvailableException;
import com.am.checkin.web.util.*;
import com.am.checkin.web.v2.services.AddPaxToPriorityListService;
import com.am.checkin.web.v2.util.SystemVariablesUtil;
import com.sabre.services.acs.bso.checkinpassenger.v3.PrintFormatACS;
import com.sabre.services.acs.bso.addtoprioritylist.v3.ACSAddToPriorityListRSACS;
import com.sabre.services.acs.bso.checkinpassenger.v3.*;
import com.sabre.services.acs.bso.editpassengercharacteristics.v3.ACSEditPassengerCharacteristicsRQACS;
import com.sabre.services.acs.bso.editpassengercharacteristics.v3.ACSEditPassengerCharacteristicsRSACS;
import com.sabre.services.acs.bso.editpassengercharacteristics.v3.PassengerInfoListRQACS;
import com.sabre.services.acs.bso.editpassengercharacteristics.v3.PassengerInfoRQACS;
import com.sabre.services.stl.v3.*;
import com.sabre.webservices.sabrexml._2011._10.ContextChangeRQ;
import com.sabre.webservices.sabrexml._2011._10.ContextChangeRS;
import org.aeromexico.commons.exception.config.ReadProperties;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;
import java.util.*;

/**
 * @author adrian
 */
@Named
@ApplicationScoped
public class CheckInService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CheckInService.class);
    private static final ICodesGenerator CODES_GENERATOR = new CodesGenerator();

    private static final String VERSION = "3.1.1";

    @Inject
    private AMCheckInPassService checkInPassService;

    @Inject
    private AMEditPassCharService editPassCharService;

    @Inject
    private PriorityCodesDaoCache priorityCodesDaoCache;

    @Inject
    private LogOnDatabaseService logOnDatabaseService;

    @Inject
    private SeatsService seatsService;

    @Inject
    private SeatMapsServiceSoap seatMapsServiceSoap;

    @Inject
    private ShoppingCartService shoppingCartService;

    @Inject
    private ChangeContexService changeContexService;

    @Inject
    private AEService aeService;

    @Inject
    @Named("CheckInSetUpConfigFactoryWrapper")
    private CheckInSetUpConfigFactoryWrapper setUpConfigFactory;

    @Inject
    private ReadProperties prop;

    @Inject
    private AddPaxToPriorityListService addToPriorityListService;

    @Inject
    private CheckInServiceDao checkInServiceDao;

    @Inject
    private IAirportCodesDao airportsCodesDao;

    @PostConstruct
    public void init() {
        if (null == setUpConfigFactory) {
            setUpConfigFactory = new CheckInSetUpConfigFactoryWrapper();
        }
    }

    /**
     * @param purchaseOrderRQ
     * @param bookedTravelersEligibleForCheckin
     * @param passengersSuccessfulCheckedIn
     * @param volunteerOffer
     * @param warningCollection
     * @param pos
     * @param language
     * @param bookedLeg
     * @param recordLocator
     * @param cartId
     * @return
     * @throws GenericException
     * @throws Exception
     */
    public List<ACSCheckInPassengerRSACS> doCheckin(
            PurchaseOrderRQ purchaseOrderRQ,
            List<BookedTraveler> bookedTravelersEligibleForCheckin,
            Set<BookedTraveler> passengersSuccessfulCheckedIn,
            boolean volunteerOffer,
            WarningCollection warningCollection,
            String pos,
            String language,
            BookedLeg bookedLeg,
            String recordLocator,
            String cartId
    ) throws GenericException, Exception {
        if (null == passengersSuccessfulCheckedIn) {
            passengersSuccessfulCheckedIn = new HashSet<>();
        }

        List<BookedSegment> openSegments = bookedLeg.getSegments().getOpenSegments();

        if (null == openSegments || openSegments.isEmpty()) {
            throw new GenericException(
                    Response.Status.BAD_REQUEST,
                    ErrorType.FLIGHT_NOT_OPEN_FOR_CHECKIN,
                    "Checkin: Flight not open for checkin"
            );
        }

        BookedSegment firstSegmentOperatedByAM = bookedLeg.getSegments().getFirstOpenSegmentForCheckinOperatedBy(AirlineCodeType.AM);

        if (null == firstSegmentOperatedByAM) {
            throw new GenericException(
                    Response.Status.BAD_REQUEST,
                    ErrorType.OPERATING_CARRIER_NOT_AM,
                    "Checkin: Flight not operated by aeromexico"
            );
        }

        List<List<BookedTraveler>> passengersGroupedByClass;
        passengersGroupedByClass = FilterPassengersUtil.groupPassengersByBookinClassWithSegmentCode(
                bookedTravelersEligibleForCheckin,
                firstSegmentOperatedByAM.getSegment().getSegmentCode()
        );

        if (null == passengersGroupedByClass || passengersGroupedByClass.isEmpty()) {
            logError(recordLocator, cartId, "CheckIn: Not passengers eligible for checkin");
            throw new GenericException(
                    Response.Status.BAD_REQUEST,
                    ErrorType.NO_PASSENGERS_SELECTED_FOR_CHECKIN,
                    "Checkin: Not passengers eligible for checkin"
            );
        }

        List<ACSCheckInPassengerRSACS> acsCheckInPassengerRSACSList = new ArrayList<>();

        for (List<BookedTraveler> passengersWithSameClass : passengersGroupedByClass) {

            if (null == passengersWithSameClass
                    || passengersWithSameClass.isEmpty()) {
                throw new NotPassengersForCheckinException(
                        Response.Status.BAD_REQUEST,
                        ErrorType.PASSENGER_ALREADY_CHECKED,
                        "Checkin: Not passengers eligible for checkin"
                );
            }

            if (null == passengersWithSameClass.get(0)
                    || null == passengersWithSameClass.get(0).getBookingClasses()
                    || null == passengersWithSameClass.get(0).getBookingClasses().getCollection()
                    || passengersWithSameClass.get(0).getBookingClasses().getCollection().isEmpty()) {

                logError(recordLocator, cartId, "CheckIn: the booking class of the passenger is required");
                throw new GenericException(
                        Response.Status.BAD_REQUEST,
                        ErrorType.BOOKING_CLASS_MAY_NOT_BE_EMPTY,
                        "Checkin: The booking class of the passenger is required"
                );
            }

            BookingClass bookingClassObj;
            bookingClassObj = passengersWithSameClass.get(0).getBookingClasses().getBookingClass(
                    firstSegmentOperatedByAM.getSegment().getSegmentCode()
            );

            if (null == bookingClassObj
                    || null == bookingClassObj.getBookingClass()
                    || bookingClassObj.getBookingClass().trim().isEmpty()) {

                logError(recordLocator, cartId, "CheckIn: the booking class of the passenger is required");
                throw new GenericException(
                        Response.Status.BAD_REQUEST,
                        ErrorType.BOOKING_CLASS_MAY_NOT_BE_EMPTY,
                        "Checkin: The booking class of the passenger is required"
                );
            }

            String bookingClass = bookingClassObj.getBookingClass();

            ItineraryACS itinerary = PurchaseOrderUtil.getItineraryFromBookedLegFirstSegmentOperatedBy(
                    bookedLeg, bookingClass, AirlineCodeType.AM
            );

            if (null == itinerary) {
                throw new GenericException(
                        Response.Status.BAD_REQUEST,
                        ErrorType.ITINERARY_INFORMATION_IS_REQUIRED,
                        "Checkin: Flight association is required"
                );
            }

            List<BookedSegment> checkInSegments = bookedLeg.getSegments().getOpenSegmentsOperatedBy(AirlineCodeType.AM);
            BookedSegment bookedSegment = PurchaseOrderUtil.getFirstOpenSegment(checkInSegments);

            setGender(passengersWithSameClass, itinerary);

            List<List<BookedTraveler>> groupedPassengersByPassengerType;
            groupedPassengersByPassengerType = FilterPassengersUtil.groupPassengersByPassengerType(
                    passengersWithSameClass
            );

            //index 0 is for passengers type P
            int index = 0;
            if (!groupedPassengersByPassengerType.get(index).isEmpty()) {
                boolean thereWasSuccess = false;

                for (BookedTraveler bookedTravelerWithClassP : groupedPassengersByPassengerType.get(index)) {
                    try {
                        List<BookedTraveler> passengersWithClassP = new ArrayList<>();
                        passengersWithClassP.add(bookedTravelerWithClassP);

                        ACSCheckInPassengerRSACS acsCheckInPassengerRSACS;
                        acsCheckInPassengerRSACS = realCheckIn(
                                purchaseOrderRQ,
                                bookedSegment,
                                itinerary,
                                passengersWithClassP,
                                pos,
                                language,
                                recordLocator,
                                cartId,
                                null,
                                false,
                                warningCollection
                        );

                        if (null != acsCheckInPassengerRSACS) {
                            acsCheckInPassengerRSACSList.add(acsCheckInPassengerRSACS);
                            passengersSuccessfulCheckedIn.addAll(passengersWithClassP);
                            thereWasSuccess = true;
                        }
                    } catch (Exception ex) {
                        if (thereWasSuccess && null != warningCollection && null != warningCollection.getCollection()) {
                            //add warning
                            Warning warning;
                            warning = new Warning(
                                    ErrorType.ERROR_DOING_CHECKIN.getErrorCode(),
                                    ErrorType.ERROR_DOING_CHECKIN.getFullDescription() + ": " + ex.getMessage() + ", passengerId:" + bookedTravelerWithClassP.getId()
                            );
                            warningCollection.addToList(warning);
                        } else {
                            //thrown exception
                            throw ex;
                        }
                    }
                }
            }

            //index 1 is for passengers type E
            index = 1;
            if (!groupedPassengersByPassengerType.get(index).isEmpty()) {
                boolean thereWasSuccess = false;

                boolean onStandBy = true;
                boolean onUpgrade = true;

                for (BookedTraveler bookedTravelerWithClassE : groupedPassengersByPassengerType.get(index)) {
                    ACSCheckInPassengerRSACS acsCheckInPassengerRSACS = null;
                    List<BookedTraveler> passengersWithClassE = new ArrayList<>();
                    passengersWithClassE.add(bookedTravelerWithClassE);

                    try {

                        acsCheckInPassengerRSACS = realCheckIn(
                                purchaseOrderRQ,
                                bookedLeg.isStandByReservation(),
                                bookedLeg.isGroundHandled(),
                                bookedSegment,
                                itinerary,
                                passengersWithClassE,
                                pos,
                                language,
                                recordLocator,
                                cartId,
                                null,
                                false,
                                warningCollection
                        );

                        if (null != acsCheckInPassengerRSACS) {
                            acsCheckInPassengerRSACSList.add(acsCheckInPassengerRSACS);
                            passengersSuccessfulCheckedIn.addAll(passengersWithClassE);

                            thereWasSuccess = true;
                        }
                    } catch (Exception ex) {
                        boolean passengerClassE = false;
                        if (ex instanceof GenericException) {
                            GenericException ge = (GenericException) ex;

                            if (ErrorType.PASSENGER_ALREADY_ON_PRIORITY_LIST.equals(ge.getErrorCodeType())
                                    || ErrorType.PASSENGER_ON_PRIORITY_LIST.equals(ge.getErrorCodeType())) {

                                if (null != acsCheckInPassengerRSACS) {
                                    acsCheckInPassengerRSACSList.add(acsCheckInPassengerRSACS);
                                    passengersSuccessfulCheckedIn.addAll(passengersWithClassE);
                                }

                                bookedTravelerWithClassE.setCheckinStatus(true);

                                if (null != bookedTravelerWithClassE.getUpgradeCode()) {
                                    bookedTravelerWithClassE.setOnUpgradeList(true);
                                }

                                if (null != bookedTravelerWithClassE.getPriorityCode()) {
                                    PriorityCode priorityCode = priorityCodesDaoCache.getPriorityCodeByCode(
                                            bookedTravelerWithClassE.getPriorityCode()
                                    );

                                    if (null != priorityCode) {
                                        if (priorityCode.getGroup().contains(PriorityGroup.STANDBY)) {
                                            bookedTravelerWithClassE.setOnStandByList(true);
                                        }

                                        if (priorityCode.getGroup().contains(PriorityGroup.UPGRADE)) {
                                            bookedTravelerWithClassE.setOnUpgradeList(true);
                                        }
                                    }
                                }

                                for (BookedSegment segment : bookedLeg.getSegments().getCollection()) {
                                    AbstractSegmentChoice abstractSegmentChoice;
                                    abstractSegmentChoice = bookedTravelerWithClassE.getSegmentChoices().getBySegmentCode(
                                            segment.getSegment().getSegmentCode()
                                    );

                                    if (null == abstractSegmentChoice) {
                                        SeatChoice seatChoice = new SeatChoice();

                                        seatChoice.setCode("STB");
                                        PaidSegmentChoice paidSegmentChoice = new PaidSegmentChoice();
                                        paidSegmentChoice.setSeat(seatChoice);
                                        paidSegmentChoice.setSegmentCode(segment.getSegment().getSegmentCode());
                                        bookedTravelerWithClassE.getSegmentChoices().getCollection().add(paidSegmentChoice);
                                    } else {
                                        abstractSegmentChoice.getSeat().setCode("STB");
                                    }
                                }

                                SegmentDocument segmentDocument;
                                segmentDocument = getStbSegmentDocument(
                                        bookedTravelerWithClassE,
                                        firstSegmentOperatedByAM,
                                        recordLocator
                                );

                                bookedTravelerWithClassE.getSegmentDocumentsList().addOrReplace(segmentDocument);

                                passengerClassE = true;
                                thereWasSuccess = true;
                            }
                        }

                        if (!passengerClassE) {
                            if (thereWasSuccess && null != warningCollection && null != warningCollection.getCollection()) {
                                //add warning
                                Warning warning;
                                warning = new Warning(
                                        ErrorType.ERROR_DOING_CHECKIN.getErrorCode(),
                                        ErrorType.ERROR_DOING_CHECKIN.getFullDescription() + ": " + ex.getMessage() + ", passengerId:" + bookedTravelerWithClassE.getId()
                                );
                                warningCollection.addToList(warning);
                            } else {
                                //thrown exception
                                throw ex;
                            }
                        }
                    }

                    onStandBy &= bookedTravelerWithClassE.isOnStandByList();
                    onUpgrade &= bookedTravelerWithClassE.isOnUpgradeList();
                }

                bookedLeg.setOnStandByList(onStandBy);
                bookedLeg.setOnUpgradeList(onUpgrade);
            }

            //index 2 is for passengers type F different than P and E   ACSCheckInPassengerRSACS
            index = 2;
            if (!groupedPassengersByPassengerType.get(index).isEmpty()) {

                try {
                    ACSCheckInPassengerRSACS checkInPassengerRS = realCheckIn(
                            purchaseOrderRQ,
                            bookedSegment,
                            itinerary,
                            groupedPassengersByPassengerType.get(index),
                            pos,
                            language,
                            recordLocator,
                            cartId,
                            null,
                            volunteerOffer,
                            warningCollection
                    );

                    if (null != checkInPassengerRS) {
                        acsCheckInPassengerRSACSList.add(checkInPassengerRS);
                        passengersSuccessfulCheckedIn.addAll(groupedPassengersByPassengerType.get(index));
                    }
                } catch (NotEnoughSeatAvailableException ex) {
                    ACSCheckInPassengerRSACS responseCheckInPassengerRS = ex.getCheckInPassengerRS();
                                            ex.setPos(PosType.getType(pos));
                                            ex.getGenericException().setPos(PosType.getType(pos));
                    GenericException ge = ex.getGenericException();
                    
                    //error NOT_ENOUGH_SEATS_AVAILABLE
                    String flagpassengerOnStandByList = System.getProperty("flagShowPassengerOnStandByList");
                    boolean flagActiveStandByList = Boolean.parseBoolean(flagpassengerOnStandByList);

                    String destinationRs = responseCheckInPassengerRS.getFlightSeatMapInfoList().get(0).getFlightInfo().getDestination();
                    List<String> airportsCodeOvs = null;
                    airportsCodeOvs = airportsCodesDao.getAirportOvsCodes();
                    boolean flagDestinationValid = false;
                    flagDestinationValid = CommonUtilsCheckIn.isDestinationValid(airportsCodeOvs, destinationRs);


                    SeatMap seatMapCollection = null;
                    seatMapCollection = getSeatMapCapacity(purchaseOrderRQ);
                    String bookingClassSeatMap = null;
                    int seatAmPlus = 0;
                    int seatPrefered = 0;
                    int seatExitRow = 0;
                    int seatCoach = 0;
                    Map<String, CabinCapacity> mapSeatCapacity = new HashMap<>();
                    if(seatMapCollection != null){
                        bookingClassSeatMap = seatMapCollection.getSegment().getBookingClass();
                        ArrayList<CabinCapacity> totalCapacity = null;
                        totalCapacity  = seatMapCollection.getSegment().getCapacity();
                        SeatSummary seatSumaryFlight = seatMapCollection.getSeatSummary();
                        seatAmPlus = seatSumaryFlight.getAmPlus();
                        seatPrefered = seatSumaryFlight.getPreferred();
                        seatExitRow = seatSumaryFlight.getExitRow();
                        seatCoach = seatSumaryFlight.getCoach();
                        
                        for(CabinCapacity cap: totalCapacity){
                            mapSeatCapacity.put(cap.getCabin(), cap);
                        }
                    }
                    CabinCapacity mainCapacity = mapSeatCapacity.get("main");
                    CabinCapacity premierCapacity = mapSeatCapacity.get("premier");
                    LOGGER.info("SEATSCAPACITYFLIGHTAUT_{}_BOOK_{}_TBP_{}_AUT_{}_BOOK_{}_TBP_{}_AMP_{}_PRF_{}_ER_{}_SC_{}_BC_{}_PNR_{}",
                    mainCapacity.getAuthorized(),mainCapacity.getBooked(),mainCapacity.getTotalBoardingPassIssued(),
                    premierCapacity.getAuthorized(),premierCapacity.getBooked(),premierCapacity.getTotalBoardingPassIssued(),
                    seatAmPlus,seatPrefered,seatExitRow,seatCoach,bookingClassSeatMap,purchaseOrderRQ.getRecordLocator()
                    );

                    boolean flagCountSeatFight = false;
                    if(seatAmPlus == 0 && seatPrefered == 0 && seatExitRow == 0 && seatCoach == 0){
                        //sobreventa
                        flagCountSeatFight = true;
                    }else{
                        if(seatAmPlus != 0 && seatPrefered == 0 && seatExitRow == 0 && seatCoach == 0){
                            LOGGER.info("LESS_AMPLUS");
                            flagCountSeatFight = false;
                        }else if(seatAmPlus == 0 && seatPrefered != 0 && seatExitRow == 0 && seatCoach == 0){
                            LOGGER.info("LESS_PREFERED");
                            flagCountSeatFight = false;
                        }else if(seatAmPlus == 0 && seatPrefered == 0 && seatExitRow != 0 && seatCoach == 0){
                            LOGGER.info("LESS_EXITROW");
                            flagCountSeatFight = false;
                        }else if(seatAmPlus != 0 && seatPrefered != 0 && seatExitRow != 0 && seatCoach == 0){
                            LOGGER.info("LESS_AMPLUS_PREFERED_EXITROW");
                            flagCountSeatFight = false;
                        }else if(seatAmPlus != 0 && seatPrefered != 0 && seatExitRow == 0 && seatCoach == 0){
                            LOGGER.info("LESS_AMPLUS_PREFERED");
                            flagCountSeatFight = false;
                        }else if(seatAmPlus == 0 && seatPrefered != 0 && seatExitRow != 0 && seatCoach == 0){
                            LOGGER.info("LESS_PREFERED_EXITROW");
                            flagCountSeatFight = false;
                        }
                    }

                    boolean flagWeb = false;
                    if(purchaseOrderRQ.getPos().equalsIgnoreCase("WEB")){
                        flagWeb  = true;
                    }
                    if(flagActiveStandByList && flagWeb && flagDestinationValid && flagCountSeatFight){    
                        if(Integer.valueOf(ErrorType.NOT_ENOUGH_SEATS_AVAILABLE_WEB.getErrorCode()).equals(Integer.valueOf(ge.getErrorCodeType().getErrorCode()))){
                                
                                String flightRs = responseCheckInPassengerRS.getItineraryPassengerList().getItineraryPassenger().get(0).getItineraryDetail().getFlight();
                                String originRs = responseCheckInPassengerRS.getFlightSeatMapInfoList().get(0).getFlightInfo().getOrigin();
                                destinationRs = responseCheckInPassengerRS.getFlightSeatMapInfoList().get(0).getFlightInfo().getDestination();
                                int sizePassengerReservation = responseCheckInPassengerRS.getItineraryPassengerList().getItineraryPassenger().get(0).getPassengerDetailList().getPassengerDetail().size();
                                String departureDateRs = responseCheckInPassengerRS.getFlightSeatMapInfoList().get(0).getFlightInfo().getDepartureDate();
                                String departureTimeRs = responseCheckInPassengerRS.getFlightSeatMapInfoList().get(0).getFlightInfo().getDepartureTime();
                                /**
                                 * method to add code ovs into passengers
                                 */
                                OvsPassengerList totalPassengerFlight = null;
                                totalPassengerFlight = checkInServiceDao.getPassengerOvsStandByList(flightRs, originRs, destinationRs, departureDateRs, departureTimeRs);
                                OvsPassengerList ovsPassengerListSave  = null;
                                int sumTotalPass = 0;
                                if(null != totalPassengerFlight){
                                    sumTotalPass = totalPassengerFlight.getSizeFlight() + sizePassengerReservation;
                                }
                                ACSAddToPriorityListRSACS addToPriorityListRQACS = null;
                                int sumTotalPassanger = 0;

                                if(null == totalPassengerFlight){    
                                    if(sizePassengerReservation  <= 5){
                                        LOGGER.info("STANDBYLIST_NOT_ENOUGH_SEATS_AVAILABLE_WEB_IF PNR {}", purchaseOrderRQ.getRecordLocator());
                                        addToPriorityListRQACS = 
                                        addToPriorityListService.addPassengerToPriorityListCheckIn(bookedSegment,bookedTravelersEligibleForCheckin,bookedSegment.getSegment().getSegmentCode(),pos);
                                        String flight = addToPriorityListRQACS.getItineraryPassengerList().getItineraryPassenger().get(0).getItineraryDetail().getFlight();
                                        int passSizePriority = addToPriorityListRQACS.getItineraryPassengerList().getItineraryPassenger().get(0).getPassengerDetailList().getPassengerDetail().size();
                                        List<PassengerDetailACS> passengerListDetailACS = addToPriorityListRQACS.getItineraryPassengerList().getItineraryPassenger().get(0).getPassengerDetailList().getPassengerDetail();
                                        ovsPassengerListSave  = new OvsPassengerList();
                                        ovsPassengerListSave.setFlightAm(flight);
                                        ovsPassengerListSave.setOriginAm(originRs);
                                        ovsPassengerListSave.setDestinationAm(destinationRs);
                                        ovsPassengerListSave.setDepartureDate(departureDateRs);
                                        ovsPassengerListSave.setDepartureTime(departureTimeRs);
                                        PassengerDetail passengerDetail = null;
                                        List<PassengerDetail> listPassengerDetail = new ArrayList<>();
                                        for(PassengerDetailACS detailPassenger: passengerListDetailACS){
                                            String fN = detailPassenger.getFirstName();
                                            String lN = detailPassenger.getLastName();
                                            String pI = detailPassenger.getPassengerID();
                                            passengerDetail = new PassengerDetail(fN,lN,pI);
                                            listPassengerDetail.add(passengerDetail);
                                        }
                                        ovsPassengerListSave.setPassengerDetail(listPassengerDetail);

                                        if(null == totalPassengerFlight){
                                            sumTotalPassanger = passSizePriority;
                                        } else {
                                            sumTotalPassanger = totalPassengerFlight.getSizeFlight() + passSizePriority;
                                        }

                                        ovsPassengerListSave.setSizeFlight(sumTotalPassanger);
                                        checkInServiceDao.saveOvsPassengerList(ovsPassengerListSave, flight);
                                    }
                                } else {

                                    if(totalPassengerFlight.getSizeFlight() <= 5 && sumTotalPass <= 5){
                                        LOGGER.info("STANDBYLIST_NOT_ENOUGH_SEATS_AVAILABLE_WEB_ELSE PNR {}", purchaseOrderRQ.getRecordLocator());
                                        addToPriorityListRQACS = 
                                        addToPriorityListService.addPassengerToPriorityListCheckIn(bookedSegment,bookedTravelersEligibleForCheckin,bookedSegment.getSegment().getSegmentCode(),pos);
                                        String flight = addToPriorityListRQACS.getItineraryPassengerList().getItineraryPassenger().get(0).getItineraryDetail().getFlight();
                                        int passSizePriority = addToPriorityListRQACS.getItineraryPassengerList().getItineraryPassenger().get(0).getPassengerDetailList().getPassengerDetail().size();
                                        List<PassengerDetailACS> passengerListDetailACS = addToPriorityListRQACS.getItineraryPassengerList().getItineraryPassenger().get(0).getPassengerDetailList().getPassengerDetail();
                                        ovsPassengerListSave  = new OvsPassengerList();
                                        ovsPassengerListSave.setFlightAm(totalPassengerFlight.getFlightAm());
                                        ovsPassengerListSave.setOriginAm(totalPassengerFlight.getOriginAm());
                                        ovsPassengerListSave.setDestinationAm(totalPassengerFlight.getDestinationAm());
                                        ovsPassengerListSave.setDepartureDate(totalPassengerFlight.getDepartureDate());
                                        ovsPassengerListSave.setDepartureTime(totalPassengerFlight.getDepartureTime());
                                        PassengerDetail passengerDetail = null;
                                        List<PassengerDetail> listPassengerDetail = new ArrayList<>();
                                        listPassengerDetail.addAll(totalPassengerFlight.getPassengerDetail());
                                        for(PassengerDetailACS detailPassenger: passengerListDetailACS){
                                            String fN = detailPassenger.getFirstName();
                                            String lN = detailPassenger.getLastName();
                                            String pI = detailPassenger.getPassengerID();
                                            passengerDetail = new PassengerDetail(fN,lN,pI);
                                            listPassengerDetail.add(passengerDetail);
                                        }
                                        ovsPassengerListSave.setPassengerDetail(listPassengerDetail);

                                        if(null == totalPassengerFlight){
                                            sumTotalPassanger = passSizePriority;
                                        }else{
                                            sumTotalPassanger = totalPassengerFlight.getSizeFlight() + passSizePriority;
                                        }
                                        ovsPassengerListSave.setSizeFlight(sumTotalPassanger);
                                        checkInServiceDao.updateOvsPassengerList(ovsPassengerListSave, flight);
                                    }
                                }
                        }
                    }

                    if (!ErrorType.NOT_ENOUGH_SEATS_AVAILABLE.equals(ge.getErrorCodeType())
                            && !ErrorType.CHECK_SEATMAP.equals(ge.getErrorCodeType()) ) {
                        throw ge;
                    }

                    LOGGER.info("Retrying checkin with random seat assigment");

                    if (null == responseCheckInPassengerRS) {
                        LOGGER.error("Random seat assignment: Checkin response is null");
                        throw ge;
                    }

                    boolean areTherePasses = checkForBoardingPasses(responseCheckInPassengerRS);

                    if (areTherePasses) {
                        acsCheckInPassengerRSACSList.add(responseCheckInPassengerRS);
                        passengersSuccessfulCheckedIn.addAll(groupedPassengersByPassengerType.get(index));
                    } else {

                        LOGGER.info(responseCheckInPassengerRS.toString());

                        if (null == responseCheckInPassengerRS.getFlightSeatMapInfoList()
                                || responseCheckInPassengerRS.getFlightSeatMapInfoList().isEmpty()
                                || !BookedLegCheckinStatusType.OPEN.equals(bookedLeg.getCheckinStatus())) {

                            LOGGER.error("CHECKIN_ERROR: Random seat assignment: Initital validation fails");
                            throw ge;
                        }

                        SeatmapCollection seatmapCollection;
                        try {
                            seatmapCollection = seatsService.getSeatMapCollection(cartId);
                        } catch (Exception ex1) {
                            seatmapCollection = getSeatMap(purchaseOrderRQ, cartId);
                        }

                        if (null == seatmapCollection) {
                            LOGGER.error("CHECKIN_ERROR: Random seat assignment: Error retrieving seat map collection");
                            throw ge;
                        }

                        List<ACSCheckInPassengerRSACS> checkInPassengerRSList;
                        checkInPassengerRSList = doCheckInWithRandomSeatAssignment(
                                purchaseOrderRQ,
                                responseCheckInPassengerRS,
                                passengersWithSameClass,
                                bookedLeg,
                                seatmapCollection,
                                passengersSuccessfulCheckedIn,
                                warningCollection,
                                ge
                        );

                        if (null == checkInPassengerRSList || checkInPassengerRSList.isEmpty()) {
                            throw ge;
                        } else {
                            acsCheckInPassengerRSACSList.addAll(checkInPassengerRSList);
                        }
                    }
                }
            }
        }

        for (BookedTraveler bookedTraveler : bookedTravelersEligibleForCheckin) {
            if ("F".equalsIgnoreCase(bookedTraveler.getPassengerType()) && bookedTraveler.isOnUpgradeList()) {
                bookedLeg.setUpgradeReservation(true);
                bookedLeg.setShowUpgradeList(true);
                break;
            }
        }

        return acsCheckInPassengerRSACSList;
    }

    private boolean checkForBoardingPasses(ACSCheckInPassengerRSACS responseCheckInPassengerRS) {

        if (null != responseCheckInPassengerRS.getItineraryPassengerList() && null != responseCheckInPassengerRS.getItineraryPassengerList().getItineraryPassenger()) {
            for (ItineraryPassengerACS itineraryPassengerACS : responseCheckInPassengerRS.getItineraryPassengerList().getItineraryPassenger()) {
                if (null != itineraryPassengerACS.getPassengerDetailList() && null != itineraryPassengerACS.getPassengerDetailList().getPassengerDetail()) {
                    for (PassengerDetailACS passengerDetailACS : itineraryPassengerACS.getPassengerDetailList().getPassengerDetail()) {
                        if (null != passengerDetailACS.getTravelDocDataList() && null != passengerDetailACS.getTravelDocDataList().getTravelDocData()) {
                            for (TravelDocDataACS travelDocDataACS : passengerDetailACS.getTravelDocDataList().getTravelDocData()) {
                                if (null != travelDocDataACS.getTravelDoc()) {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

    /**
     * Retries the seat assignment when not enough seats.
     *
     * @param purchaseOrderRQ
     * @param checkInPassengerRS
     * @param bookedLeg
     * @param passengersWithSameClass
     * @param seatmapCollection
     * @param passengersSuccessfulCheckedIn
     * @param warningCollection
     * @param originalException
     * @return
     * @throws java.lang.Exception
     */
    public List<ACSCheckInPassengerRSACS> doCheckInWithRandomSeatAssignment(
            PurchaseOrderRQ purchaseOrderRQ,
            ACSCheckInPassengerRSACS checkInPassengerRS,
            List<BookedTraveler> passengersWithSameClass,
            BookedLeg bookedLeg,
            SeatmapCollection seatmapCollection,
            Set<BookedTraveler> passengersSuccessfulCheckedIn,
            WarningCollection warningCollection,
            GenericException originalException
    ) throws GenericException, Exception {

        if (null == warningCollection) {
            warningCollection = new WarningCollection();
        }

        List<ACSCheckInPassengerRSACS> checkInPassengerRSList = new ArrayList<>();

        List<BookedSegment> bookedSegmentListAMOperated = bookedLeg.getSegments().getSegmentsOperatedBy(AirlineCodeType.AM);

        if (null == bookedSegmentListAMOperated || bookedSegmentListAMOperated.isEmpty()) {
            LOGGER.error("CHECKIN_ERROR: Random seat assignment: Flights not operated by aeromexico DESCRIPTION: {}",
            ErrorType.OPERATING_CARRIER_NOT_AM);

            throw new GenericException(
                    Response.Status.BAD_REQUEST,
                    ErrorType.OPERATING_CARRIER_NOT_AM,
                    "Checkin: Flights not operated by aeromexico"
            );
        }

        boolean thereWasSuccess = false;
        boolean skip;

        String flightNumber = null;

        for (BookedSegment bookedSegment : bookedSegmentListAMOperated) {
            skip = false;

            boolean isSameFlight = FlightNumberUtil.compareFlightNumbers(
                    bookedSegment.getSegment().getOperatingFlightCode(), flightNumber
            );

            isSameFlight = isSameFlight || FlightNumberUtil.compareFlightNumbers(
                    bookedSegment.getSegment().getMarketingFlightCode(), flightNumber
            );

            // first iteratio will pass becasue flight number is null
            if (!isSameFlight) {

                flightNumber = bookedSegment.getSegment().getOperatingFlightCode();

                List<BookedSegment> bookedSegmentList = bookedLeg.getSegments().getSegmentsWithFlightNumber(
                        bookedSegment.getSegment().getOperatingFlightCode()
                );

                FlightSeatMapInfoACS flightSeatMapInfoACS;
                Queue<String> seatsQueue = null;
                String segmentCode = null;

                try {
                    flightSeatMapInfoACS = SeatMapUtil.getSeatMapBySegment(
                            checkInPassengerRS.getFlightSeatMapInfoList(),
                            bookedSegment.getSegment()
                    );

                    if (null == flightSeatMapInfoACS
                            || null == flightSeatMapInfoACS.getFlightInfo()
                            || !BookedLegCheckinStatusType.OPENCI.toString().equals(flightSeatMapInfoACS.getFlightInfo().getStatus())
                            || null == flightSeatMapInfoACS.getFlightSeatMapDetail()
                            || null == flightSeatMapInfoACS.getFlightSeatMapDetail().getSeatMapDetail()
                            || flightSeatMapInfoACS.getFlightSeatMapDetail().getSeatMapDetail().isEmpty()) {

                        LOGGER.error("CHECKIN_ERROR: Random seat assignment: validation flightSeatMapInfoACS fails DESCRIPTION: {}",
                        ErrorType.NO_SEAT_MAP_DETAIL_IN_RESPONSE_OF_CHECKIN);

                        throw new GenericException(
                                Response.Status.BAD_REQUEST,
                                ErrorType.NO_SEAT_MAP_DETAIL_IN_RESPONSE_OF_CHECKIN,
                                "Checkin: No seat map detail during checkin"
                        );
                    }

                    segmentCode = bookedSegment.getSegment().getSegmentCode();

                    List<SeatmapSection> seatmapSectionList;
                    seatmapSectionList = SeatMapUtil.getSeatMapSectionList(seatmapCollection, segmentCode);

                    if (null == seatmapSectionList || seatmapSectionList.isEmpty()) {
                        LOGGER.error("Random seat assignment: seatmapSectionList empty");

                        throw new GenericException(
                                Response.Status.BAD_REQUEST,
                                ErrorType.NO_SEAT_MAP_SECTION_LIST_IN_CHECKIN,
                                "Checkin: Flights not operated by aeromexico"
                        );
                    }

                    //the returned queue will be ordered in the same order of this list
                    List<SeatSectionCodeType> validSectionListOrdered = new ArrayList<>();

                    validSectionListOrdered.add(SeatSectionCodeType.COACH);
                    if (PosType.KIOSK.toString().equalsIgnoreCase(purchaseOrderRQ.getPos())) {
                        validSectionListOrdered.add(SeatSectionCodeType.PREFERRED);
                        validSectionListOrdered.add(SeatSectionCodeType.EXIT_ROW);
                    }

                    Map<SeatSectionCodeType, Map<String, SeatmapSeat>> seatmapSectionMap;
                    seatmapSectionMap = SeatMapUtil.getMapFromSeatmapCollection(
                            seatmapSectionList, validSectionListOrdered
                    );

                    if (null == seatmapSectionMap || seatmapSectionMap.isEmpty()) {
                        LOGGER.error("Random seat assignment: seatmapSectionMap is empty");

                        throw new GenericException(
                                Response.Status.BAD_REQUEST,
                                ErrorType.NO_SEAT_MAP_SECTION_MAP_IN_CHECKIN,
                                "Checkin: No seat map section in checkin"
                        );
                    }

                    //create a queue with available seats starting form the last row
                    seatsQueue = SeatMapUtil.getQueueFromSeatMapDetailList(
                            flightSeatMapInfoACS.getFlightSeatMapDetail().getSeatMapDetail(),
                            Arrays.asList(
                                    "Y"
                            ),
                            SeatMapUtil.getFREE_SEAT_CODES(),
                            seatmapSectionMap,
                            validSectionListOrdered
                    );

                    if (null == seatsQueue || seatsQueue.isEmpty()
                            || seatsQueue.size() < passengersWithSameClass.size()) {
                        LOGGER.error("Random seat assignment: not seats available");

                        throw new GenericException(
                                Response.Status.BAD_REQUEST,
                                ErrorType.NO_SEAT_QUEUE_IN_CHECKIN_RANDOM_SEAT,
                                "Checkin: No seat queue in checkin random seat"
                        );
                    }

                } catch (GenericException e) {
                    LOGGER.error(e.getMessage(), e);

                    //if at least one passenger have been checked in successfully we collect warnings instead of errors
                    if (thereWasSuccess) {
                        //add warning
                        Warning warning;
                        warning = new Warning(
                                ErrorType.ERROR_DOING_CHECKIN.getErrorCode(),
                                e.getMsg()
                        );
                        warning.setExtraInfo(null);
                        warningCollection.addToList(warning);

                        skip = true;
                    } else {
                        //thrown exception
                        throw originalException;
                    }

                    seatsQueue = new LinkedList<>();
                }

                if (!skip) {
                    for (BookedTraveler bookedTraveler : passengersWithSameClass) {
                        try {

                            AbstractSegmentChoice abstractSegmentChoice;
                            abstractSegmentChoice = bookedTraveler.getSegmentChoices().getBySegmentCode(segmentCode);

                            boolean preReservedSeat = false;

                            if (null != abstractSegmentChoice) {
                                if (abstractSegmentChoice instanceof PaidSegmentChoice) {
                                    preReservedSeat = true;
                                } else if (abstractSegmentChoice instanceof SegmentChoice
                                        && ((SegmentChoice) abstractSegmentChoice).isPartOfReservation()) {
                                    preReservedSeat = true;
                                }
                            }

                            if (!bookedTraveler.isCheckinStatus() && !preReservedSeat) {

                                if (seatsQueue.isEmpty()) {
                                    LOGGER.error("Random seat assignment: no more seats");
                                    break;
                                }

                                String newSeatCode = seatsQueue.poll();

                                if (null == newSeatCode) {
                                    LOGGER.error("Random seat assignment: no more seats");
                                    break;
                                }

                                BookingClass bookingClassObj;
                                bookingClassObj = bookedTraveler.getBookingClasses().getBookingClass(segmentCode);

                                if (null == bookingClassObj
                                        || null == bookingClassObj.getBookingClass()
                                        || bookingClassObj.getBookingClass().trim().isEmpty()) {

                                    logError(purchaseOrderRQ.getRecordLocator(), purchaseOrderRQ.getCartId(), "CheckIn: the booking class of the passenger is required");
                                    throw new GenericException(
                                            Response.Status.BAD_REQUEST,
                                            ErrorType.BOOKING_CLASS_MAY_NOT_BE_EMPTY,
                                            "Checkin: The booking class of the passenger is required"
                                    );
                                }

                                ACSEditPassengerCharacteristicsRQACS editPassengerCharacteristicsRQ;
                                editPassengerCharacteristicsRQ = ShoppingCartUtil.getEditPassengerCharacteristicsRQSegmentChoices(
                                        bookedSegment.getSegment(), bookingClassObj,
                                        bookedTraveler, newSeatCode, false
                                );

                                ACSEditPassengerCharacteristicsRSACS editPassengerCharacteristicsRS;
                                editPassengerCharacteristicsRS = editPassCharService.editPassengerChararcteristics(
                                        editPassengerCharacteristicsRQ
                                );

                                if (!ErrorOrSuccessCode.SUCCESS.equals(editPassengerCharacteristicsRS.getResult().getStatus())) {
                                    LOGGER.error(
                                            ErrorType.PRE_RESERVED_SEAT_NOT_SUCCESSFUL.getFullDescription()
                                                    + " FOR PASSENGER ID : "
                                                    + bookedTraveler.getId()
                                                    + " SEGMENTCODE : "
                                                    + segmentCode
                                                    + " - PNR : "
                                                    + purchaseOrderRQ.getRecordLocator()
                                    );

                                    Warning warning = new Warning();
                                    warning.setCode(ErrorType.PRE_RESERVED_SEAT_NOT_SUCCESSFUL.getErrorCode());
                                    warning.setMsg(
                                            ErrorType.PRE_RESERVED_SEAT_NOT_SUCCESSFUL.getFullDescription()
                                                    + " FOR PASSENGER ID : "
                                                    + bookedTraveler.getId()
                                                    + " SEGMENTCODE : "
                                                    + segmentCode
                                                    + " - PNR : "
                                                    + purchaseOrderRQ.getRecordLocator()
                                    );
                                    warningCollection.addToList(warning);

                                } else {

                                    //TODO: before checkin maybe it requieres the creation
                                    // of waived AEs for the seats in order to success checkin
                                    ACSCheckInPassengerRSACS secondCheckInPassengerRS;
                                    secondCheckInPassengerRS = doCheckinForOneSegment(
                                            purchaseOrderRQ,
                                            bookedTraveler,
                                            bookingClassObj,
                                            bookedSegmentList,
                                            passengersSuccessfulCheckedIn,
                                            warningCollection
                                    );

                                    if (null != secondCheckInPassengerRS) {
                                        checkInPassengerRSList.add(secondCheckInPassengerRS);

                                        thereWasSuccess = true;
                                    }
                                }
                            }
                        }catch (SabreEmptyResponseException ser) {
                            LOGGER.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110302).toString(), ser.fillInStackTrace());
                            throw ser;
                        }catch (SabreLayerUnavailableException se){
                            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(purchaseOrderRQ.getPos())) {
                                LOGGER.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_22110301,"CheckInService.java:818", se.getMessage());
                                throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getCode(),
                                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getActionable(),
                                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getMessage(),
                                        Response.Status.INTERNAL_SERVER_ERROR);
                            } else {
                                LOGGER.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_12110301,"CheckInService.java:824", se.getMessage());
                                throw se;
                            }
                        } catch (Exception e) {
                            LOGGER.error(e.getMessage(), e);

                            //if at least one passenger have been checked in successfully we collect warnings instead of errors
                            if (thereWasSuccess) {
                                //add warning
                                Warning warning;
                                warning = new Warning(
                                        ErrorType.ERROR_DOING_CHECKIN.getErrorCode(),
                                        ErrorType.ERROR_DOING_CHECKIN.getFullDescription() + ": " + e.getMessage() + ", passengerId:" + bookedTraveler.getId()
                                );
                                warningCollection.addToList(warning);
                            } else {
                                //thrown exception
                                throw originalException;
                            }
                        }
                    }
                }
            }
        }

        return checkInPassengerRSList;
    }

    /**
     * @param bookedTravelerWithClassE
     * @param firstSegmentOperatedByAM
     * @param recordLocator
     * @return
     */
    private SegmentDocument getStbSegmentDocument(
            BookedTraveler bookedTravelerWithClassE,
            BookedSegment firstSegmentOperatedByAM,
            String recordLocator
    ) {
        String travelDoc = BoardingPassUtil.getStandbyBoardingpassPectab(
                bookedTravelerWithClassE, firstSegmentOperatedByAM, recordLocator
        );
        SegmentDocument segmentDocument = new SegmentDocument();
        segmentDocument.setSegmentCode(firstSegmentOperatedByAM.getSegment().getSegmentCode());
        segmentDocument.setTravelDoc(travelDoc);
        segmentDocument.setCheckinStatus(true);
        String boardingPass;
        try {
            int start = travelDoc.indexOf("^P3");
            boardingPass = travelDoc.substring(start + 3);
            int end = boardingPass.indexOf("^");
            boardingPass = boardingPass.substring(0, end);
        } catch (Exception ex1) {
            LOGGER.error(ex1.getMessage());
            boardingPass = null;
        }
        if (null != boardingPass && !boardingPass.trim().isEmpty()) {
            try {
                segmentDocument.setBarcode(boardingPass);
                segmentDocument.setBarcodeImage(
                        CODES_GENERATOR.codesGenerator2D(boardingPass)
                );
                segmentDocument.setQrcodeImage(
                        CODES_GENERATOR.codesGeneratorQR(boardingPass)
                );
            } catch (Exception ex1) {
                LOGGER.error(ex1.getMessage());
            }
        }
        return segmentDocument;
    }

    /**
     * @param passengersWithSameClass
     * @param itinerary
     * @throws Exception
     */
    private void setGender(List<BookedTraveler> passengersWithSameClass, ItineraryACS itinerary) throws Exception {
        ACSEditPassengerCharacteristicsRQACS editPassengerCharacteristicsRQ;
        editPassengerCharacteristicsRQ = getEditPassengerCharacteristicsRQForGender(
                passengersWithSameClass, itinerary
        );

        ACSEditPassengerCharacteristicsRSACS editPassengerCharacteristicsRS;
        editPassengerCharacteristicsRS = editPassCharService.editPassengerChararcteristics(editPassengerCharacteristicsRQ);

        //StringBuilder msg = new StringBuilder();
        //msg.append("Set gender").append("\n");
        //msg.append("EditPassengerCharacteristicsRQ: ").append(editPassCharService.toString(editPassengerCharacteristicsRQ)).append("\n");
        //msg.append("EditPassengerCharacteristicsRS: ").append(editPassCharService.toString(editPassengerCharacteristicsRS)).append("\n");
        //LOGGER.info(msg.toString());
        if (!ErrorOrSuccessCode.SUCCESS.equals(editPassengerCharacteristicsRS.getResult().getStatus())) {
            ShoppingCartErrorUtil.checkErrorPassengerService(editPassengerCharacteristicsRS);
        }
    }

    /**
     * @param purchaseOrderRQ
     * @param passengersSuccessfulCheckedIn
     * @param carts
     * @param legs
     * @param pos
     * @param language
     * @param recordLocator
     * @param cartId
     * @param warningCollection
     * @return
     * @throws Exception
     */
    public List<ACSCheckInPassengerRSACS> doCheckinForEachSegment(
            PurchaseOrderRQ purchaseOrderRQ,
            Set<BookedTraveler> passengersSuccessfulCheckedIn,
            CartPNRCollection carts,
            BookedLegCollection legs,
            String pos,
            String language,
            String recordLocator,
            String cartId,
            WarningCollection warningCollection
    ) throws Exception {
        if (null == passengersSuccessfulCheckedIn) {
            passengersSuccessfulCheckedIn = new HashSet<>();
        }

        List<ACSCheckInPassengerRSACS> acsCheckInPassengerRSACSList = new ArrayList<>();

        for (CartPNR cartPNR : carts.getCollection()) {

            List<BookedTraveler> bookedTravelersEligibleForCheckin = FilterPassengersUtil.getEligibleForCheckin(cartPNR.getTravelerInfo().getCollection());

            BookedLeg bookedLeg = legs.getBookedLegByLegCode(cartPNR.getLegCode());

            List<ItineraryACS> itineraryACSList = PurchaseOrderUtil.getItineraryListWithoutClassFromBookedLeg(bookedLeg);

            for (ItineraryACS itineraryACS : itineraryACSList) {
                if (AirlineCodeType.AM.name().equalsIgnoreCase(itineraryACS.getAirline())) {

                    BookedSegment bookedSegment = PurchaseOrderUtil.getBookedSegmentByFlightNumber(bookedLeg, itineraryACS.getFlight());

                    Map<String, List<BookedTraveler>> passengersGroupedByClass = FilterPassengersUtil.groupPassengersByBookinClassWithFlightNumber(bookedTravelersEligibleForCheckin, itineraryACS.getFlight());

                    for (Map.Entry<String, List<BookedTraveler>> entry : passengersGroupedByClass.entrySet()) {
                        String bookingClass = entry.getKey();
                        itineraryACS.setBookingClass(bookingClass);

                        List<BookedTraveler> passengersWithSameClassList = entry.getValue();

                        try {

                            //this call will throw an exception if something was wrong
                            ACSCheckInPassengerRSACS acsCheckInPassengerRSACS;
                            acsCheckInPassengerRSACS = realCheckIn(
                                    purchaseOrderRQ,
                                    bookedSegment,
                                    itineraryACS,
                                    passengersWithSameClassList,
                                    pos,
                                    language,
                                    recordLocator,
                                    cartId,
                                    null,
                                    false,
                                    warningCollection
                            );

                            if (null != acsCheckInPassengerRSACS) {
                                acsCheckInPassengerRSACSList.add(acsCheckInPassengerRSACS);
                                passengersSuccessfulCheckedIn.addAll(passengersWithSameClassList);
                            }

                        } catch (NotPassengersForCheckinException ex) {
                            //just ignore
                        }
                    }
                }

            }

        }

        return acsCheckInPassengerRSACSList;
    }

    /**
     * @param purchaseOrderRQ
     * @param bookedTraveler
     * @param passengersSuccessfulCheckedIn
     * @param bookedSegmentList
     * @param bookingClassObj
     * @param warningCollection
     * @return
     * @throws Exception
     */
    public ACSCheckInPassengerRSACS doCheckinForOneSegment(
            PurchaseOrderRQ purchaseOrderRQ,
            BookedTraveler bookedTraveler,
            BookingClass bookingClassObj,
            List<BookedSegment> bookedSegmentList,
            Set<BookedTraveler> passengersSuccessfulCheckedIn,
            WarningCollection warningCollection
    ) throws Exception {

        String pos = purchaseOrderRQ.getPos();
        String language = purchaseOrderRQ.getLanguage();
        String recordLocator = purchaseOrderRQ.getRecordLocator();
        String cartId = purchaseOrderRQ.getCartId();

        if (null == passengersSuccessfulCheckedIn) {
            passengersSuccessfulCheckedIn = new HashSet<>();
        }

        ItineraryACS itineraryACS = PurchaseOrderUtil.getItineraryListFromSameSegments(
                bookedSegmentList, bookingClassObj.getBookingClass()
        );

        if (null == itineraryACS) {
            throw new GenericException(
                    Response.Status.BAD_REQUEST,
                    ErrorType.ITINERARY_INFORMATION_IS_REQUIRED,
                    "Checkin: Flight association is required"
            );
        }

        if (AirlineCodeType.AM.name().equalsIgnoreCase(itineraryACS.getAirline())) {

            itineraryACS.setBookingClass(bookingClassObj.getBookingClass());

            List<BookedTraveler> passengersWithSameClassList = new ArrayList<>();
            passengersWithSameClassList.add(bookedTraveler);

            try {
                //this call will throw an exception if something was wrong
                ACSCheckInPassengerRSACS acsCheckInPassengerRSACS;
                acsCheckInPassengerRSACS = realCheckIn(
                        purchaseOrderRQ,
                        bookedSegmentList.get(0),
                        itineraryACS,
                        passengersWithSameClassList,
                        pos,
                        language,
                        recordLocator,
                        cartId,
                        null,
                        false,
                        warningCollection
                );

                if (null != acsCheckInPassengerRSACS) {
                    passengersSuccessfulCheckedIn.addAll(passengersWithSameClassList);
                }

                return acsCheckInPassengerRSACS;

            } catch (NotPassengersForCheckinException ex) {
                //just ignore
            }
        }

        return null;
    }

    /**
     * @param bookedTraveler
     * @param bookedLeg
     * @param recordLocator
     * @param checkInRQ
     * @param overBookingPriorityCode
     * @throws GenericException
     * @throws Exception
     */
    public void addPassengersToPriorityListPo(
            List<BookedTraveler> bookedTraveler,
            BookedLeg bookedLeg,
            String recordLocator,
            PurchaseOrderRQ checkInRQ,
            String overBookingPriorityCode
    ) throws GenericException, Exception {
        WarningCollection warningCollection = new WarningCollection();
        String pos = checkInRQ.getPos();
        String cartId = checkInRQ.getPurchaseOrder().getCartId();
        String language = checkInRQ.getLanguage();
        
        if (null == bookedTraveler && bookedTraveler.isEmpty() ){
            throw new GenericException(
                    Response.Status.BAD_REQUEST,
                    ErrorType.BAD_REQUEST,
                    "Checkin: bookedtraveler is null"
            );
        } 
        /*else if (!bookedTraveler.isCheckinStatus()) {
            throw new GenericException(
                    Response.Status.BAD_REQUEST,
                    ErrorType.BAD_REQUEST,
                    "Checkin: passenger is not checked in"
            );
        }*/

        BookedSegment bookedSegment = bookedLeg.getSegments().getFirstSegmentOperatedBy(AirlineCodeType.AM);

        if (null == bookedSegment) {
            throw new GenericException(
                    Response.Status.BAD_REQUEST,
                    ErrorType.OPERATING_CARRIER_NOT_AM,
                    "Checkin: Flight not operated by aeromexico"
            );
        }

        BookingClass bookingClassObj = null;
        List<BookedTraveler> passengers = new ArrayList<>();;
        ItineraryACS itinerary = null;
        for(BookedTraveler bookedTravel: bookedTraveler) {
            if (null == bookedTravel.getBookingClasses()
                    || null == bookedTravel.getBookingClasses().getCollection()
                    || bookedTravel.getBookingClasses().getCollection().isEmpty()) {

                logError(recordLocator, cartId, "CheckIn: the booking class of the passenger is required");
                throw new GenericException(
                        Response.Status.BAD_REQUEST,
                        ErrorType.BOOKING_CLASS_MAY_NOT_BE_EMPTY,
                        "Checkin: The booking class of the passenger is required"
                );
            }

            bookingClassObj = bookedTravel.getBookingClasses().getBookingClass(
                bookedSegment.getSegment().getSegmentCode()
            );

            if (null == bookingClassObj
                || null == bookingClassObj.getBookingClass()
                || bookingClassObj.getBookingClass().trim().isEmpty()) {

                logError(recordLocator, cartId, "CheckIn: the booking class of the passenger is required");
                throw new GenericException(
                        Response.Status.BAD_REQUEST,
                        ErrorType.BOOKING_CLASS_MAY_NOT_BE_EMPTY,
                        "Checkin: The booking class of the passenger is required"
                );
            }

            String bookingClass = bookingClassObj.getBookingClass();

            itinerary = PurchaseOrderUtil.getItineraryFromBookedLegFirstSegmentOperatedBy(
                    bookedLeg, bookingClass, AirlineCodeType.AM
            );
    
            if (null == itinerary) {
                throw new GenericException(
                        Response.Status.BAD_REQUEST,
                        ErrorType.ITINERARY_INFORMATION_IS_REQUIRED,
                        "Checkin: Flight association is required"
                );
            }

            passengers.add(bookedTravel);

        }

        try {

            ACSCheckInPassengerRSACS acsCheckInPassengerRSACS = null;
            if(null != passengers && !passengers.isEmpty()){
                if(bookedTraveler.size() == passengers.size() ){

                    acsCheckInPassengerRSACS = realCheckIn(
                            checkInRQ,
                            bookedSegment,
                            itinerary,
                            passengers,
                            pos,
                            language,
                            recordLocator,
                            cartId,
                            overBookingPriorityCode,
                            false,
                            warningCollection
                    );

                    if (null != acsCheckInPassengerRSACS) {
                        boolean success = checkPriorityResponse(acsCheckInPassengerRSACS);

                        if (!success) {
                            throw new GenericException(
                                    Response.Status.BAD_REQUEST,
                                    ErrorType.BAD_REQUEST,
                                    "Checkin: passenger was not added to priority list"
                            );
                        }
                    } else {
                        throw new GenericException(
                                Response.Status.BAD_REQUEST,
                                ErrorType.BAD_REQUEST,
                                "Checkin: passenger was not added to priority list"
                        );
                    }
                }
            }
        }catch (SabreEmptyResponseException ser) {
            LOGGER.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110302).toString(), ser.fillInStackTrace());
            throw ser;
        }catch (SabreLayerUnavailableException se){
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(checkInRQ.getPos())) {
                LOGGER.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_22110301,"CheckInService.java:1215", se.getMessage());
                throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getCode(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getActionable(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getMessage(),
                        Response.Status.INTERNAL_SERVER_ERROR);
            } else {
                LOGGER.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_12110301,"CheckInService.java:1221", se.getMessage());
                throw se;
            }
        } catch (Exception ex) {
            if (!checkPriorityError(
                    ex,
                    bookedTraveler.get(0),
                    warningCollection,
                    false
            )) {
                throw ex;
            }
        }
    }

    public void addPassengersToPriorityList(
            BookedTraveler bookedTraveler,
            BookedLeg bookedLeg,
            String recordLocator,
            CheckInRQ checkInRQ,
            String overBookingPriorityCode
    ) throws GenericException, Exception {
        WarningCollection warningCollection = new WarningCollection();
        String pos = checkInRQ.getPos();
        String cartId = checkInRQ.getCartId();
        String language = checkInRQ.getLanguage();
        
        if (null == bookedTraveler) {
            throw new GenericException(
                    Response.Status.BAD_REQUEST,
                    ErrorType.BAD_REQUEST,
                    "Checkin: bookedtraveler is null"
            );
        } else if (!bookedTraveler.isCheckinStatus()) {
            throw new GenericException(
                    Response.Status.BAD_REQUEST,
                    ErrorType.BAD_REQUEST,
                    "Checkin: passenger is not checked in"
            );
        }

        BookedSegment bookedSegment = bookedLeg.getSegments().getFirstSegmentOperatedBy(AirlineCodeType.AM);

        if (null == bookedSegment) {
            throw new GenericException(
                    Response.Status.BAD_REQUEST,
                    ErrorType.OPERATING_CARRIER_NOT_AM,
                    "Checkin: Flight not operated by aeromexico"
            );
        }

        if (null == bookedTraveler.getBookingClasses()
                || null == bookedTraveler.getBookingClasses().getCollection()
                || bookedTraveler.getBookingClasses().getCollection().isEmpty()) {

            logError(recordLocator, cartId, "CheckIn: the booking class of the passenger is required");
            throw new GenericException(
                    Response.Status.BAD_REQUEST,
                    ErrorType.BOOKING_CLASS_MAY_NOT_BE_EMPTY,
                    "Checkin: The booking class of the passenger is required"
            );
        }

        BookingClass bookingClassObj;
        bookingClassObj = bookedTraveler.getBookingClasses().getBookingClass(
                bookedSegment.getSegment().getSegmentCode()
        );

        if (null == bookingClassObj
                || null == bookingClassObj.getBookingClass()
                || bookingClassObj.getBookingClass().trim().isEmpty()) {

            logError(recordLocator, cartId, "CheckIn: the booking class of the passenger is required");
            throw new GenericException(
                    Response.Status.BAD_REQUEST,
                    ErrorType.BOOKING_CLASS_MAY_NOT_BE_EMPTY,
                    "Checkin: The booking class of the passenger is required"
            );
        }

        String bookingClass = bookingClassObj.getBookingClass();

        ItineraryACS itinerary = PurchaseOrderUtil.getItineraryFromBookedLegFirstSegmentOperatedBy(
                bookedLeg, bookingClass, AirlineCodeType.AM
        );

        if (null == itinerary) {
            throw new GenericException(
                    Response.Status.BAD_REQUEST,
                    ErrorType.ITINERARY_INFORMATION_IS_REQUIRED,
                    "Checkin: Flight association is required"
            );
        }

        try {
            List<BookedTraveler> passengers = new ArrayList<>();
            passengers.add(bookedTraveler);

            ACSCheckInPassengerRSACS acsCheckInPassengerRSACS;
            acsCheckInPassengerRSACS = realCheckIn(
                    checkInRQ,
                    bookedSegment,
                    itinerary,
                    passengers,
                    pos,
                    language,
                    recordLocator,
                    cartId,
                    overBookingPriorityCode,
                    false,
                    warningCollection
            );

            if (null != acsCheckInPassengerRSACS) {
                boolean success = checkPriorityResponse(acsCheckInPassengerRSACS);

                if (!success) {
                    throw new GenericException(
                            Response.Status.BAD_REQUEST,
                            ErrorType.BAD_REQUEST,
                            "Checkin: passenger was not added to priority list"
                    );
                }
            } else {
                throw new GenericException(
                        Response.Status.BAD_REQUEST,
                        ErrorType.BAD_REQUEST,
                        "Checkin: passenger was not added to priority list"
                );
            }

        }catch (SabreEmptyResponseException ser) {
            LOGGER.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110302).toString(), ser.fillInStackTrace());
            throw ser;
        }catch (SabreLayerUnavailableException se){
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(checkInRQ.getPos())) {
                LOGGER.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_22110301,"CheckInService.java:1215", se.getMessage());
                throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getCode(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getActionable(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getMessage(),
                        Response.Status.INTERNAL_SERVER_ERROR);
            } else {
                LOGGER.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_12110301,"CheckInService.java:1221", se.getMessage());
                throw se;
            }
        } catch (Exception ex) {
            if (!checkPriorityError(
                    ex,
                    bookedTraveler,
                    warningCollection,
                    false
            )) {
                throw ex;
            }
        }
    }

    /**
     * @param acsCheckInPassengerRSACS
     * @return
     */
    private boolean checkPriorityResponse(ACSCheckInPassengerRSACS acsCheckInPassengerRSACS) {
        try {
            List<ItineraryPassengerACS> itineraryPassengerACSList;
            itineraryPassengerACSList = acsCheckInPassengerRSACS.getItineraryPassengerList().getItineraryPassenger();
            if (null != itineraryPassengerACSList) {
                for (ItineraryPassengerACS itineraryPassengerACS : itineraryPassengerACSList) {
                    List<PassengerDetailACS> passengerDetailACSList;
                    passengerDetailACSList = itineraryPassengerACS.getPassengerDetailList().getPassengerDetail();
                    if (null != passengerDetailACSList) {
                        for (PassengerDetailACS passengerDetailACS : passengerDetailACSList) {
                            List<FreeTextInfoACS> freeTextInfoACSList;
                            freeTextInfoACSList = passengerDetailACS.getFreeTextInfoList().getFreeTextInfo();
                            if (null != freeTextInfoACSList) {
                                for (FreeTextInfoACS freeTextInfoACS : freeTextInfoACSList) {
                                    List<String> textList;
                                    textList = freeTextInfoACS.getTextLine().getText();
                                    if (null != textList) {
                                        for (String string : textList) {
                                            if (ErrorType.PASSENGER_ALREADY_ON_PRIORITY_LIST.getFullDescription().equalsIgnoreCase(string)
                                                    || ErrorType.PASSENGER_ON_PRIORITY_LIST.getFullDescription().equalsIgnoreCase(string)) {
                                                return true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }

        return false;
    }

    /**
     * @param ex
     * @param bookedTraveler
     * @param warningCollection
     * @param thereWasSuccess
     * @return
     * @throws Exception
     */
    private boolean checkPriorityError(
            Exception ex,
            BookedTraveler bookedTraveler,
            WarningCollection warningCollection,
            boolean thereWasSuccess
    ) throws Exception {
        if (null == warningCollection) {
            warningCollection = new WarningCollection();
        }

        if (ex instanceof GenericException) {
            GenericException ge = (GenericException) ex;

            if (ErrorType.PASSENGER_ALREADY_ON_PRIORITY_LIST.equals(ge.getErrorCodeType())
                    || ErrorType.PASSENGER_ON_PRIORITY_LIST.equals(ge.getErrorCodeType())) {
                return true;
            } else {
                if (thereWasSuccess) {
                    //add warning
                    Warning warning;
                    warning = new Warning(
                            ErrorType.ERROR_DOING_CHECKIN.getErrorCode(),
                            ErrorType.ERROR_DOING_CHECKIN.getFullDescription() + ": " + ex.getMessage() + ", passengerId:" + bookedTraveler.getId()
                    );
                    warningCollection.addToList(warning);

                    return false;
                } else {
                    //thrown exception
                    throw ex;
                }
            }
        } else if (thereWasSuccess) {
            //add warning
            Warning warning;
            warning = new Warning(
                    ErrorType.ERROR_DOING_CHECKIN.getErrorCode(),
                    ErrorType.ERROR_DOING_CHECKIN.getFullDescription() + ": " + ex.getMessage() + ", passengerId:" + bookedTraveler.getId()
            );
            warningCollection.addToList(warning);

            return false;
        } else {
            //thrown exception
            throw ex;
        }
    }

    /**
     * @param checkInRQ
     * @param bookedSegment
     * @param itinerary
     * @param passengersWithSameClass
     * @param pos
     * @param language
     * @param recordLocator
     * @param cartId
     * @param overBookingPriorityCode
     * @param volunteerOffer
     * @param warningCollection
     * @return
     * @throws NotEnoughSeatAvailableException
     * @throws GenericException
     * @throws Exception
     */
    private ACSCheckInPassengerRSACS realCheckIn(
            CheckInRQ checkInRQ,
            BookedSegment bookedSegment,
            ItineraryACS itinerary,
            List<BookedTraveler> passengersWithSameClass,
            String pos,
            String language,
            String recordLocator,
            String cartId,
            String overBookingPriorityCode,
            boolean volunteerOffer,
            WarningCollection warningCollection
    ) throws NotEnoughSeatAvailableException, GenericException, Exception {
        return realCheckIn(
                checkInRQ,
                false,
                false,
                bookedSegment,
                itinerary,
                passengersWithSameClass,
                pos,
                language,
                recordLocator,
                cartId,
                overBookingPriorityCode,
                volunteerOffer,
                warningCollection
        );
    }

    /**
     * @param checkInRQ
     * @param isStandby
     * @param isGroundHandled
     * @param bookedSegment
     * @param itinerary
     * @param passengersWithSameClass
     * @param pos
     * @param language
     * @param recordLocator
     * @param cartId
     * @param overBookingPriorityCode
     * @param volunteerOffer
     * @param warningCollection
     * @return
     * @throws NotEnoughSeatAvailableException
     * @throws GenericException
     * @throws Exception
     */
    private ACSCheckInPassengerRSACS realCheckIn(
            CheckInRQ checkInRQ,
            boolean isStandby,
            boolean isGroundHandled,
            BookedSegment bookedSegment,
            ItineraryACS itinerary,
            List<BookedTraveler> passengersWithSameClass,
            String pos,
            String language,
            String recordLocator,
            String cartId,
            String overBookingPriorityCode,
            boolean volunteerOffer,
            WarningCollection warningCollection
    ) throws NotEnoughSeatAvailableException, GenericException, Exception {

        if (null == warningCollection) {
            warningCollection = new WarningCollection();
        }

        if (SystemVariablesUtil.isTimaticEnabled() && !isStandby) {

            List<BookedTraveler> passengersNotOkToBoard;
            passengersNotOkToBoard = FilterPassengersUtil.getTimaticNotOkToBoardStatus(passengersWithSameClass);

            if (null != passengersNotOkToBoard && !passengersNotOkToBoard.isEmpty()) {

                Warning warning;
                if(pos.equalsIgnoreCase("WEB")){    
                    warning = new Warning(
                            ErrorType.PASSENGERS_WITH_TIMATIC_STATUS_NOT_OK_TO_BOARD_WEB.getErrorCode(),
                            ErrorType.PASSENGERS_WITH_TIMATIC_STATUS_NOT_OK_TO_BOARD_WEB.getFullDescription()
                    );
                } else {
                    warning = new Warning(
                            ErrorType.PASSENGERS_WITH_TIMATIC_STATUS_NOT_OK_TO_BOARD.getErrorCode(),
                            ErrorType.PASSENGERS_WITH_TIMATIC_STATUS_NOT_OK_TO_BOARD.getFullDescription()
                    );
                }

                warningCollection.addToList(warning);
            }

            passengersWithSameClass = FilterPassengersUtil.getTimaticOkToBoardStatus(passengersWithSameClass);
        }

        ACSCheckInPassengerRSACS checkInPassengerRS = null;

        if (null == passengersWithSameClass || passengersWithSameClass.isEmpty()) {
            return checkInPassengerRS;
        }

        PrintFormatACS printFormatACS = PrintFormatACS.PECTAB;

        PosType posType;
        if (PosType.KIOSK.toString().equalsIgnoreCase(pos)) {
            posType = PosType.KIOSK;
        } else {
            posType = PosType.WEB;
        }

        LanguageType languageType;
        if (LanguageType.EN.toString().equalsIgnoreCase(language)) {
            languageType = LanguageType.EN;
        } else {
            languageType = LanguageType.ES;
        }

        PassengerInfoListACS passengerInfoList;
        passengerInfoList = getCheckInPassengerInfo(
                passengersWithSameClass,
                itinerary.getFlight(),
                pos,
                recordLocator,
                overBookingPriorityCode,
                cartId
        );

        ACSCheckInPassengerRQACS checkInPassengerRQ = new ACSCheckInPassengerRQACS();
        checkInPassengerRQ.setItinerary(itinerary);
        checkInPassengerRQ.setPassengerInfoList(passengerInfoList);
        PrintingOptionsACS printingOptionsACS = new PrintingOptionsACS();
        printingOptionsACS.setPrintFormat(printFormatACS);
        checkInPassengerRQ.setPrintingOptions(printingOptionsACS);
        checkInPassengerRQ.setVersion(VERSION);

        SabreSession sabreSession = null;

        try {
            LOGGER.info("Ground handled variables: {}, {}, {}", SystemVariablesUtil.isEarlyCheckinEnabled(), isStandby, isGroundHandled);

            if (SystemVariablesUtil.isEarlyCheckinEnabled() && isStandby && isGroundHandled) {
                if (null != SystemVariablesUtil.getEarlyCheckinPassword()
                        && !SystemVariablesUtil.getEarlyCheckinPassword().isEmpty()) {

                    sabreSession = aeService.getSession();

                    ContextChangeRQ contextChangeRQ = ChangeContexService.createContextChangeOverSignFirstStepRQ(SystemVariablesUtil.getEarlyCheckinUser());

                    ContextChangeRS contextChangeRS = changeContexService.changeContext(sabreSession, contextChangeRQ);

                    ChangeContexService.validateContextChangeResponse(contextChangeRS);

                    contextChangeRQ = ChangeContexService.createContextChangeOverSignSecondStepRQ(Constants.DUTY_CODE, SystemVariablesUtil.getEarlyCheckinUser(), SystemVariablesUtil.getEarlyCheckinPassword());

                    contextChangeRS = changeContexService.changeContext(sabreSession, contextChangeRQ);

                    ChangeContexService.validateContextChangeResponse(contextChangeRS);

                    checkInPassengerRS = callCheckIn(sabreSession, checkInPassengerRQ);

                    if (null != sabreSession) {
                        aeService.closeSession(sabreSession);
                    }
                } else {
                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNKNOWN_CODE_ABORT, "Early checkin pass null");
                }
            } else {
                //call ChekinFinal
                checkInPassengerRS = callCheckIn(checkInPassengerRQ);
            }

            log(recordLocator, cartId, "checking first try", checkInPassengerRQ, checkInPassengerRS);
            boolean checkForRetry = true;

            String msg = checkErrorCheckIn(
                    checkInPassengerRS,
                    posType,
                    languageType,
                    recordLocator,
                    cartId,
                    checkForRetry,
                    checkInPassengerRQ
            );

            if (null != msg
                    && (msg.toUpperCase().contains(ErrorType.PASSENGER_ALREADY_ON_PRIORITY_LIST.getSearchableCode())
                    || msg.toUpperCase().contains(ErrorType.PASSENGER_ON_PRIORITY_LIST.getSearchableCode()))) {

                if (PassengerFareType.F.getCode().equalsIgnoreCase(passengersWithSameClass.get(0).getPassengerType())) {
                    for (BookedTraveler passengersWithSameClas : passengersWithSameClass) {
                        boolean onPriorityList = wasAdded2PriorityList(checkInPassengerRS, passengersWithSameClas.getId());
                        passengersWithSameClas.setOnUpgradeList(onPriorityList);
                    }
                } else {
                    for (BookedTraveler passengersWithSameClas : passengersWithSameClass) {
                        if (null != passengersWithSameClas.getUpgradeCode()) {
                            passengersWithSameClas.setOnUpgradeList(true);
                        }

                        if (null != passengersWithSameClas.getPriorityCode()) {
                            PriorityCode pc = priorityCodesDaoCache.getPriorityCodeByCode(
                                    passengersWithSameClas.getPriorityCode()
                            );

                            if (null != pc) {
                                if (pc.getGroup().contains(PriorityGroup.STANDBY)) {
                                    passengersWithSameClas.setOnStandByList(true);
                                }

                                if (pc.getGroup().contains(PriorityGroup.UPGRADE)) {
                                    passengersWithSameClas.setOnUpgradeList(true);
                                }
                            }
                        }
                    }
                }

            }

            logOnDatabaseService.logCheckinOnSuccess(checkInRQ, cartId, recordLocator, checkInPassengerRQ, checkInPassengerRS);

            return checkInPassengerRS;
        } catch (RetryCheckinException ex) {
            if (null != sabreSession) {
                aeService.closeSession(sabreSession);
            }

            switch (ex.getErrorCodeType()) {
                case ABOVE_AUTHORIZED_COUNT:

                    if (volunteerOffer && PosType.KIOSK.toString().equalsIgnoreCase(pos)) {

                        overBookingPriorityCode = PriorityCodeType.OVI.getCode();
                        passengerInfoList = getCheckInPassengerInfo(
                                passengersWithSameClass,
                                itinerary.getFlight(),
                                pos,
                                recordLocator,
                                overBookingPriorityCode,
                                cartId
                        );

                        checkInPassengerRQ = new ACSCheckInPassengerRQACS();
                        checkInPassengerRQ.setItinerary(itinerary);
                        checkInPassengerRQ.setPassengerInfoList(passengerInfoList);
                        checkInPassengerRQ.setPrintingOptions(printingOptionsACS);
                        checkInPassengerRQ.setVersion(VERSION);

                        ACSCheckInPassengerRSACS checkInPassengerRSACS = callRetryCheckin(
                                checkInPassengerRQ,
                                recordLocator,
                                cartId,
                                posType,
                                languageType,
                                checkInRQ
                        );

                        String msg = getTextInfo(checkInPassengerRS);

                        if (null != msg
                                && (msg.toUpperCase().contains(ErrorType.PASSENGER_ALREADY_ON_PRIORITY_LIST.getSearchableCode())
                                || msg.toUpperCase().contains(ErrorType.PASSENGER_ON_PRIORITY_LIST.getSearchableCode()))) {

                            if ("F".equalsIgnoreCase(passengersWithSameClass.get(0).getPassengerType())) {
                                for (BookedTraveler passengersWithSameClas : passengersWithSameClass) {
                                    boolean onPriorityList = wasAdded2PriorityList(checkInPassengerRS, passengersWithSameClas.getId());
                                    passengersWithSameClas.setOnUpgradeList(onPriorityList);
                                }
                            } else {
                                for (BookedTraveler passengersWithSameClas : passengersWithSameClass) {
                                    if (null != passengersWithSameClas.getUpgradeCode()) {
                                        passengersWithSameClas.setOnUpgradeList(true);
                                    }

                                    if (null != passengersWithSameClas.getPriorityCode()) {
                                        PriorityCode pc = priorityCodesDaoCache.getPriorityCodeByCode(
                                                passengersWithSameClas.getPriorityCode()
                                        );

                                        if (null != pc) {
                                            if (pc.getGroup().contains(PriorityGroup.STANDBY)) {
                                                passengersWithSameClas.setOnStandByList(true);
                                            }

                                            if (pc.getGroup().contains(PriorityGroup.UPGRADE)) {
                                                passengersWithSameClas.setOnUpgradeList(true);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        Warning warning;
                        warning = new Warning(
                                ErrorType.PASSENGERS_ADDED_TO_THE_PRIORITY_LIST.getErrorCode(),
                                ErrorType.PASSENGERS_ADDED_TO_THE_PRIORITY_LIST.getFullDescription()
                        );

                        warningCollection.getCollection().add(warning);

                        return checkInPassengerRSACS;
                    } else {
                        throw ex.getGenericException();
                    }
                case PASSENGER_CONTACT_INFO_REQUIRED:

                    for (BookedTraveler passengersWithSameClas : passengersWithSameClass) {
                        passengersWithSameClas.setDeclinedEmergencyContact(true);
                    }

                    boolean successDecline = true;
                    try {
                        shoppingCartService.declineEmergencyContact(
                                passengersWithSameClass,
                                bookedSegment,
                                recordLocator
                        );
                    } catch (Exception e) {
                        successDecline = false;
                    }

                    if (successDecline) {
                        return callRetryCheckin(
                                checkInPassengerRQ,
                                recordLocator,
                                cartId,
                                posType,
                                languageType,
                                checkInRQ
                        );
                    } else {
                        throw ex.getGenericException();
                    }

                case NOT_ENOUGH_SEATS_AVAILABLE:
                case CHECK_SEATMAP:
                        NotEnoughSeatAvailableException newEx = new NotEnoughSeatAvailableException(ex);
                        newEx.setCheckInPassengerRS(checkInPassengerRS);
                        throw newEx;
                default:

                    return callRetryCheckin(
                            checkInPassengerRQ,
                            recordLocator, cartId,
                            posType,
                            languageType,
                            checkInRQ
                    );
            }

        }catch (SabreEmptyResponseException ser) {
            LOGGER.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110302).toString(), ser.fillInStackTrace());
            throw ser;
        }catch (SabreLayerUnavailableException se){
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(checkInRQ.getPos())) {
                LOGGER.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_22110301,"CheckInService.java:1697", se.getMessage());
                throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getCode(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getActionable(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getMessage(),
                        Response.Status.INTERNAL_SERVER_ERROR);
            } else {
                LOGGER.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_12110301,"CheckInService.java:1703", se.getMessage());
                throw se;
            }
        } catch (GenericException ex) {
            if (null != sabreSession) {
                aeService.closeSession(sabreSession);
            }

            logOnDatabaseService.logCheckinOnError(checkInRQ, cartId, recordLocator, checkInPassengerRQ, checkInPassengerRS, ex);
            logError(recordLocator, cartId, "error doing checkin", checkInPassengerRQ, ex.getMessage());
            LOGGER.error(ex.getMsg(), ex);
            throw ex;
        } catch (Exception ex) {
            if (null != sabreSession) {
                aeService.closeSession(sabreSession);
            }

            logOnDatabaseService.logCheckinOnError(checkInRQ, cartId, recordLocator, checkInPassengerRQ, checkInPassengerRS, ex);
            logError(recordLocator, cartId, "error calling checkin remote service", checkInPassengerRQ, ex.getMessage());
            LOGGER.error(ex.getMessage(), ex);
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNKNOWN_CODE_ABORT, ex.getMessage());
        }
    }

    private boolean wasAdded2PriorityList(ACSCheckInPassengerRSACS checkInPassengerRS, String passengerId) {
        if (null == passengerId || passengerId.isEmpty()) {
            return false;
        }

        if (null != checkInPassengerRS && null != checkInPassengerRS.getItineraryPassengerList()
                && null != checkInPassengerRS.getItineraryPassengerList().getItineraryPassenger()
                && !checkInPassengerRS.getItineraryPassengerList().getItineraryPassenger().isEmpty()) {

            for (ItineraryPassengerACS itineraryPassengerACS : checkInPassengerRS.getItineraryPassengerList().getItineraryPassenger()) {

                if (null != itineraryPassengerACS.getPassengerDetailList()
                        && null != itineraryPassengerACS.getPassengerDetailList().getPassengerDetail()
                        && !itineraryPassengerACS.getPassengerDetailList().getPassengerDetail().isEmpty()) {

                    for (PassengerDetailACS passengerDetailACS : itineraryPassengerACS.getPassengerDetailList().getPassengerDetail()) {

                        if (passengerId.equalsIgnoreCase(passengerDetailACS.getPassengerID())) {

                            FreeTextInfoListACS freeTextInfoListACS = passengerDetailACS.getFreeTextInfoList();

                            if (null != freeTextInfoListACS) {

                                for (FreeTextInfoACS freeTextInfoACS : freeTextInfoListACS.getFreeTextInfo()) {
                                    for (String text : freeTextInfoACS.getTextLine().getText()) {
                                        if (null != text
                                                && (text.contains(ErrorType.PASSENGER_ALREADY_ON_PRIORITY_LIST.getSearchableCode())
                                                || text.contains(ErrorType.PASSENGER_ON_PRIORITY_LIST.getSearchableCode()))) {
                                            return true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    private SeatmapCollection getSeatMap(CheckInRQ checkInRQ, String cartId) {
        // TODO Auto-generated method stub

        SeatmapCheckInRQ seatMapCheckInRQ = new SeatmapCheckInRQ();
        seatMapCheckInRQ.setCartId(cartId);
        seatMapCheckInRQ.setPos(checkInRQ.getPos());
        seatMapCheckInRQ.setStore(checkInRQ.getStore());
        try {
            SeatmapCollection seatmapCollection = seatMapsServiceSoap.getSeatMaps(seatMapCheckInRQ);

            return seatmapCollection;
        }catch (SabreEmptyResponseException ser) {
            LOGGER.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110302).toString(), ser.fillInStackTrace());
            throw ser;
        }catch (SabreLayerUnavailableException se){
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(checkInRQ.getPos())) {
                LOGGER.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_22110301,"CheckInService.java:1784", se.getMessage());
                throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getCode(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getActionable(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getMessage(),
                        Response.Status.INTERNAL_SERVER_ERROR);
            } else {
                LOGGER.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_12110301,"CheckInService.java:1790", se.getMessage());
                throw se;
            }
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @param checkInPassengerRQ
     * @param recordLocator
     * @param cartId
     * @param posType
     * @param languageType
     * @param checkInRQ
     * @return
     */
    private ACSCheckInPassengerRSACS callRetryCheckin(
            ACSCheckInPassengerRQACS checkInPassengerRQ,
            String recordLocator,
            String cartId,
            PosType posType,
            LanguageType languageType,
            CheckInRQ checkInRQ
    ) {
        ACSCheckInPassengerRSACS checkInPassengerRS = null;

        int retries = 1;
        try {
            checkInPassengerRS = retryCheckIn(
                    checkInPassengerRQ, recordLocator, cartId,
                    posType, languageType, retries
            );

            logOnDatabaseService.logCheckinOnSuccess(checkInRQ, cartId, recordLocator, checkInPassengerRQ, checkInPassengerRS);
            log(recordLocator, cartId, "checking retry", checkInPassengerRQ, checkInPassengerRS);

            return checkInPassengerRS;
        }catch (SabreEmptyResponseException ser) {
            LOGGER.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110302).toString(), ser.fillInStackTrace());
            throw ser;
        }catch (SabreLayerUnavailableException se){
            if (PosType.CHECKIN_MOBILE.equals(posType)) {
                LOGGER.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).toString(), se.fillInStackTrace());
                throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getCode(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getActionable(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getMessage(),
                        Response.Status.INTERNAL_SERVER_ERROR);
            } else {
                LOGGER.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_12110301,"CheckInService.java:1839", se.getMessage());
                throw se;
            }
        } catch (RetryCheckinException ex1) {
            ex1.setWasLogged(true);
            logError(recordLocator, cartId, "error retrying checkin", checkInPassengerRQ, ex1.getMessage());
            logOnDatabaseService.logCheckinOnError(checkInRQ, cartId, recordLocator, checkInPassengerRQ, checkInPassengerRS, ex1);
            throw ex1.getGenericException();
        } catch (GenericException ex1) {
            ex1.setWasLogged(true);
            logError(recordLocator, cartId, "error retrying checkin", checkInPassengerRQ, ex1.getMessage());
            logOnDatabaseService.logCheckinOnError(checkInRQ, cartId, recordLocator, checkInPassengerRQ, checkInPassengerRS, ex1);
            throw ex1;
        } catch (Exception ex1) {
            logOnDatabaseService.logCheckinOnError(checkInRQ, cartId, recordLocator, checkInPassengerRQ, checkInPassengerRS, ex1);
            logError(recordLocator, cartId, "error retry calling checkin remote service", checkInPassengerRQ, ex1.getMessage());
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNKNOWN_CODE_ABORT, ex1.getMessage());
        }
    }

    /**
     * @param checkInPassengerRQ
     * @param recordLocator
     * @param cartId
     * @param posType
     * @param languageType
     * @param retries
     * @return
     * @throws RetryCheckinException
     * @throws GenericException
     * @throws Exception
     */
    private ACSCheckInPassengerRSACS retryCheckIn(
            ACSCheckInPassengerRQACS checkInPassengerRQ,
            String recordLocator, String cartId,
            PosType posType, LanguageType languageType,
            int retries
    ) throws RetryCheckinException, GenericException, Exception {
        ACSCheckInPassengerRSACS checkInPassengerRS = null;
        int retry = 1;
        boolean checkForRetry = true;
        while (retry <= retries) {
            try {
                checkInPassengerRS = callCheckIn(checkInPassengerRQ);

                if (retry >= retries) {
                    checkForRetry = false;
                }

                String msg = checkErrorCheckIn(
                        checkInPassengerRS,
                        posType,
                        languageType,
                        recordLocator,
                        cartId,
                        checkForRetry,
                        checkInPassengerRQ
                );

                break;
            }catch (SabreEmptyResponseException ser) {
                LOGGER.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110302).toString(), ser.fillInStackTrace());
                throw ser;
            }catch (SabreLayerUnavailableException se){
                if (PosType.CHECKIN_MOBILE.equals(posType)) {
                    LOGGER.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).toString(), se.fillInStackTrace());
                    throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getCode(),
                            prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getActionable(),
                            prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getMessage(),
                            Response.Status.INTERNAL_SERVER_ERROR);
                } else {
                    LOGGER.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_12110301,"CheckInService.java:1909", se.getMessage());
                    throw se;
                }
            } catch (RetryCheckinException ex1) {
                if (retry >= retries) {
                    throw ex1;
                }
                retry++;
            }
        }
        return checkInPassengerRS;
    }

    /**
     * @param checkInPassengerRQ
     * @return
     * @throws RetryCheckinException
     * @throws GenericException
     * @throws Exception
     */
    private ACSCheckInPassengerRSACS callCheckIn(
            ACSCheckInPassengerRQACS checkInPassengerRQ
    ) throws RetryCheckinException, GenericException, Exception {
        String boardingPrinterCommand = setUpConfigFactory.getInstance().getBoardingPassPrinterCommand();
        String boardingPrinter = setUpConfigFactory.getInstance().getBoardingLNIATA();
        ACSCheckInPassengerRSACS checkInPassengerRS;
        checkInPassengerRS = checkInPassService.checkInPassenger(
                checkInPassengerRQ, boardingPrinter, boardingPrinterCommand
        );

        return checkInPassengerRS;
    }

    /**
     * @param sabreSession
     * @param checkInPassengerRQ
     * @return
     * @throws RetryCheckinException
     * @throws GenericException
     * @throws Exception
     */
    private ACSCheckInPassengerRSACS callCheckIn(
            SabreSession sabreSession,
            ACSCheckInPassengerRQACS checkInPassengerRQ
    ) throws RetryCheckinException, GenericException, Exception {
        String boardingPrinterCommand = setUpConfigFactory.getInstance().getBoardingPassPrinterCommand();
        String boardingPrinter = setUpConfigFactory.getInstance().getBoardingLNIATA();
        ACSCheckInPassengerRSACS checkInPassengerRS;
        checkInPassengerRS = checkInPassService.checkInPassenger(
                sabreSession, checkInPassengerRQ, boardingPrinter, boardingPrinterCommand, false, null
        );

        return checkInPassengerRS;
    }

    /**
     * @param bookedTravelerList
     * @param itinerary
     * @return
     */
    private ACSEditPassengerCharacteristicsRQACS getEditPassengerCharacteristicsRQForGender(
            List<BookedTraveler> bookedTravelerList,
            ItineraryACS itinerary
    ) {

        com.sabre.services.acs.bso.editpassengercharacteristics.v3.ItineraryACS itineraryRQACS;
        itineraryRQACS = new com.sabre.services.acs.bso.editpassengercharacteristics.v3.ItineraryACS();
        itineraryRQACS.setAirline(itinerary.getAirline());
        itineraryRQACS.setFlight(itinerary.getFlight());
        itineraryRQACS.setBookingClass(itinerary.getBookingClass());
        itineraryRQACS.setDepartureDate(itinerary.getDepartureDate());
        itineraryRQACS.setOrigin(itinerary.getOrigin());
        itineraryRQACS.setDestination(itinerary.getDestination());

        PassengerInfoListRQACS passengerInfoListRQACS = new PassengerInfoListRQACS();

        for (BookedTraveler bookedTraveler : bookedTravelerList) {

            //Just for GENDER
            if (null == bookedTraveler.getGender() || GenderType.DEFAULT.equals(bookedTraveler.getGender())) {
                //ignore right now
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.INELIGIBLE_REASON_MISSING_GENDER);
            }

            PassengerInfoRQACS passengerInfoRQACS = new PassengerInfoRQACS();

            passengerInfoRQACS.setPassengerID(bookedTraveler.getId());
            passengerInfoRQACS.setFirstName(bookedTraveler.getFirstName());
            passengerInfoRQACS.setLastName(bookedTraveler.getLastName());
            com.sabre.services.acs.bso.editpassengercharacteristics.v3.EditCodeDetailsListACS editCodeDetailsListACS;
            editCodeDetailsListACS = new com.sabre.services.acs.bso.editpassengercharacteristics.v3.EditCodeDetailsListACS();
            com.sabre.services.acs.bso.editpassengercharacteristics.v3.EditCodeDetailsListACS.OtherEdit otherEdit;
            otherEdit = new com.sabre.services.acs.bso.editpassengercharacteristics.v3.EditCodeDetailsListACS.OtherEdit();

            otherEdit.setActionCode("EDIT");
            otherEdit.setCode(bookedTraveler.getGender().toString());
            editCodeDetailsListACS.getOtherEdit().add(otherEdit);
            passengerInfoRQACS.setEditCodeList(editCodeDetailsListACS);

            passengerInfoListRQACS.getPassengerInfoRequest().add(passengerInfoRQACS);
        }

        ACSEditPassengerCharacteristicsRQACS editPaxCharacteristicsRQACS = new ACSEditPassengerCharacteristicsRQACS();
        editPaxCharacteristicsRQACS.setItinerary(itineraryRQACS);
        editPaxCharacteristicsRQACS.setPassengerInfoRequestList(passengerInfoListRQACS);

        return editPaxCharacteristicsRQACS;
    }

    /**
     * @param bookedTravelerList
     * @param flight
     * @param pos
     * @param recordLocator
     * @param cartId
     * @param overBookingPriorityCode
     * @return
     * @throws GenericException
     */
    private PassengerInfoListACS getCheckInPassengerInfo(
            List<BookedTraveler> bookedTravelerList,
            String flight,
            String pos,
            String recordLocator,
            String overBookingPriorityCode,
            String cartId
    ) throws GenericException {

        PassengerInfoListACS passengerInfoList = new PassengerInfoListACS();
        List<PassengerInfoACS> passengerInfoACSList = passengerInfoList.getPassengerInfo();

        for (BookedTraveler bookedTraveler : bookedTravelerList) {

            PassengerInfoACS passengerInfoACS = new PassengerInfoACS();
            passengerInfoACS.setLastName(bookedTraveler.getLastName());

            BookingClass bookingClass = FilterPassengersUtil.getBookingClassByFlight(
                    bookedTraveler.getBookingClasses().getCollection(), flight
            );

            if (null == bookingClass) {
                logError(recordLocator, cartId, "CheckIn: The booking class of the passenger is required");
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.BOOKING_CLASS_MAY_NOT_BE_EMPTY, "The booking class of the passenger is required");
            }

            if (null != overBookingPriorityCode) {
                PriorityClassificationInfoACS priorityClassificationInfoACS = new PriorityClassificationInfoACS();
                priorityClassificationInfoACS.setPriorityCode(overBookingPriorityCode);

                passengerInfoACS.setPriorityClassificationInfo(priorityClassificationInfoACS);
            } else if (
                    //(
                            !PassengerFareType.F.getCode().equalsIgnoreCase(bookedTraveler.getPassengerType()) //No full fare
                            //|| "PSC".equalsIgnoreCase(bookedTraveler.getPriorityCode())) //except the case when is a commission
                            && ((!bookedTraveler.isOnStandByList() && null != bookedTraveler.getPriorityCode())
                            || (!bookedTraveler.isOnUpgradeList() && null != bookedTraveler.getUpgradeCode()))
            ) {
                PriorityClassificationInfoACS priorityClassificationInfoACS = new PriorityClassificationInfoACS();

                if (null != bookedTraveler.getPriorityCode()) {
                    priorityClassificationInfoACS.setPriorityCode(bookedTraveler.getPriorityCode().trim().toUpperCase());
                }

                if (null != bookedTraveler.getUpgradeCode()) {
                    priorityClassificationInfoACS.setUpgradePriorityCode(bookedTraveler.getUpgradeCode().trim().toUpperCase());
                }

                passengerInfoACS.setPriorityClassificationInfo(priorityClassificationInfoACS);
            }

            passengerInfoACS.setPassengerID(bookingClass.getPassengerId());

            EditCodeDetailsListACS editCodeDetailsListACS = new EditCodeDetailsListACS();
            List<EditCodeDetailsListACS.EditCode> editCodeList = editCodeDetailsListACS.getEditCode();

            //Just for PASSENGER TYPE
            //if (null == bookedTraveler.getPaxType()) {
            //ignore right now
            //throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.INELIGIBLE_REASON_MISSING_PASSENGER_TYPE);
            //}
            if (PosType.WEB.toString().equalsIgnoreCase(pos)) {
                EditCodeDetailsListACS.EditCode editCode = new EditCodeDetailsListACS.EditCode();
                editCode.setCode("WB");
                editCode.setActionCode("EDIT");
                editCodeList.add(editCode);
            } else if (PosType.KIOSK.toString().equalsIgnoreCase(pos)) {
                EditCodeDetailsListACS.EditCode editCode = new EditCodeDetailsListACS.EditCode();
                editCode.setCode("KS");
                editCode.setActionCode("EDIT");
                editCodeList.add(editCode);
            } else {
                EditCodeDetailsListACS.EditCode editCode = new EditCodeDetailsListACS.EditCode();
                editCode.setCode("MC");
                editCode.setActionCode("EDIT");
                editCodeList.add(editCode);
            }

            if (!editCodeList.isEmpty()) {
                passengerInfoACS.setEditCodeList(editCodeDetailsListACS);
            }

            passengerInfoACSList.add(passengerInfoACS);
        }

        return passengerInfoList;
    }

    /**
     * This function will throw an exception if there was an error in the
     * response
     *
     * @param checkInPassengerRS
     * @param posType
     * @param languageType
     * @param recordLocator
     * @param cartId
     * @param checkForRetry
     * @return
     * @throws RetryCheckinException
     * @throws GenericException
     * @throws Exception
     */
    private String checkErrorCheckIn(
            ACSCheckInPassengerRSACS checkInPassengerRS,
            PosType posType,
            LanguageType languageType,
            String recordLocator,
            String cartId,
            boolean checkForRetry,
            ACSCheckInPassengerRQACS checkInPassengerRQ
    ) throws RetryCheckinException, GenericException, Exception {

        String msg = getTextInfo(checkInPassengerRS);

        if ((ErrorOrSuccessCode.SUCCESS != checkInPassengerRS.getResult().getStatus()
                || (null != checkInPassengerRS.getResult().getSystemSpecificResults()
                && !checkInPassengerRS.getResult().getSystemSpecificResults().isEmpty())) //&& ! PurchaseOrderUtil.thereAreBoardingPasses(checkInPassengerRS)
        ) {

            for (SystemSpecificResults systemSpecificResults : checkInPassengerRS.getResult().getSystemSpecificResults()) {
                msg = msg + systemSpecificResults.getErrorMessage().getValue() + " , ";
            }

            if (msg.length() >= 3) {
                msg = msg.substring(0, msg.length() - 3);
            }

            String extra = checkInPassengerRS.getResult().getStatus().name();

            logError(recordLocator, cartId, "CheckIn: Error doing checkin");
            logError(recordLocator, cartId, msg);

            if (StringUtils.containsIgnoreCase(msg, ErrorType.PASSENGER_ALREADY_CHECKED.getSearchableCode())) {
                //throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PASSENGER_ALREADY_CHECKED, msg, extra, posType);
                return msg;
            }

            String flight = checkInPassengerRQ.getItinerary().getAirline()+checkInPassengerRQ.getItinerary().getFlight();
            String passengerId = checkInPassengerRQ.getPassengerInfoList().getPassengerInfo().get(0).getPassengerID();
            LOGGER.info("CHECKIN_WAS: UNSUCCESSFUL PNR: {} FLIGHT: {} PASSENGER_ID: {} CHANNEL: {} REASON: {}",recordLocator,flight,passengerId,posType.getCode(),((null!=msg)?msg.toUpperCase().replaceAll("\\s+", "_"):""));

            if (checkForRetry) {
                if (StringUtils.containsIgnoreCase(msg, ErrorType.ASSIGN_BOARDING_PASS_PRINTER.getSearchableCode())) {
                    throw new RetryCheckinException(Response.Status.BAD_REQUEST, ErrorType.ASSIGN_BOARDING_PASS_PRINTER, msg, extra, posType);
                }

                if (StringUtils.containsIgnoreCase(msg, ErrorType.ABOVE_AUTHORIZED_COUNT.getSearchableCode())) {
                    throw new RetryCheckinException(Response.Status.BAD_REQUEST, ErrorType.ABOVE_AUTHORIZED_COUNT, msg, extra, posType);
                }

                if (StringUtils.containsIgnoreCase(msg, ErrorType.NOT_ENOUGH_SEATS_AVAILABLE.getSearchableCode())) {
                    throw new RetryCheckinException(Response.Status.BAD_REQUEST, ErrorType.NOT_ENOUGH_SEATS_AVAILABLE, posType);
                }

                if (StringUtils.containsIgnoreCase(msg, ErrorType.CHECK_PREVIOUS_VCR_COUPON_STATUS_WARNING.getSearchableCode())) {
                    throw new RetryCheckinException(Response.Status.BAD_REQUEST, ErrorType.CHECK_PREVIOUS_VCR_COUPON_STATUS_WARNING, posType);
                }

                if (StringUtils.containsIgnoreCase(msg, ErrorType.CHECK_SEATMAP.getSearchableCode())) {
                    throw new RetryCheckinException(Response.Status.BAD_REQUEST, ErrorType.CHECK_SEATMAP, msg, extra, posType);
                }

                if (StringUtils.containsIgnoreCase(msg, ErrorType.PASSENGER_CONTACT_INFO_REQUIRED.getSearchableCode())) {
                    throw new RetryCheckinException(Response.Status.BAD_REQUEST, ErrorType.PASSENGER_CONTACT_INFO_REQUIRED, msg, extra, posType);
                }
            }

            if (StringUtils.containsIgnoreCase(msg, ErrorType.ASSIGN_BOARDING_PASS_PRINTER.getSearchableCode())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.ASSIGN_BOARDING_PASS_PRINTER, msg, extra, posType);
            }

            if (StringUtils.containsIgnoreCase(msg, ErrorType.ABOVE_AUTHORIZED_COUNT.getSearchableCode())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.ABOVE_AUTHORIZED_COUNT, msg, extra, posType);
            }

            if (StringUtils.containsIgnoreCase(msg, ErrorType.NOT_ENOUGH_SEATS_AVAILABLE.getSearchableCode())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.NOT_ENOUGH_SEATS_AVAILABLE, posType);
            }

            if (StringUtils.containsIgnoreCase(msg, ErrorType.NOT_VALID_FOR_CANCELED_SEGMENT.getSearchableCode())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.NOT_VALID_FOR_CANCELED_SEGMENT, msg, extra, posType);
            }

            if (StringUtils.containsIgnoreCase(msg, ErrorType.PASSENGER_ALREADY_ON_PRIORITY_LIST.getSearchableCode())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PASSENGER_ALREADY_ON_PRIORITY_LIST, msg, extra, posType);
            }

            if (StringUtils.containsIgnoreCase(msg, ErrorType.NOT_ALLOWED_THIS_PRIORITY_CODE.getSearchableCode())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.NOT_ALLOWED_THIS_PRIORITY_CODE, msg, extra, posType);
            }

            if (StringUtils.containsIgnoreCase(msg, ErrorType.PASSENGER_ON_PRIORITY_LIST.getSearchableCode())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PASSENGER_ON_PRIORITY_LIST, msg, extra, posType);
            }

            if (StringUtils.containsIgnoreCase(msg, ErrorType.PRIORITY_CODE_REQUIRED.getSearchableCode())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PRIORITY_CODE_REQUIRED, msg, extra, posType);
            }

            if (StringUtils.containsIgnoreCase(msg, ErrorType.EARLY_CHECKIN_NOT_PERMITTED.getSearchableCode())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EARLY_CHECKIN_NOT_PERMITTED, msg, extra, posType);
            }

            if (StringUtils.containsIgnoreCase(msg, ErrorType.CHECK_SEATMAP.getSearchableCode())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.CHECK_SEATMAP, msg, extra, posType);
            }

            if (StringUtils.containsIgnoreCase(msg, ErrorType.ADD_COLLECT_DUE_SEND_PSGR_TO_ATO.getSearchableCode())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.ADD_COLLECT_DUE_SEND_PSGR_TO_ATO, msg, extra, posType);
            }

            if (StringUtils.containsIgnoreCase(msg, ErrorType.TKT_MUST_BE_REISSUED_FOR_PSGR.getSearchableCode())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.TKT_MUST_BE_REISSUED_FOR_PSGR, msg, extra, posType);
            }

            if (StringUtils.containsIgnoreCase(msg, ErrorType.VERIFY_US_VISA.getSearchableCode())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.VERIFY_US_VISA, msg, extra, posType);
            }

            if (StringUtils.containsIgnoreCase(msg, ErrorType.MULTIPLE_VCRS_FOUND.getSearchableCode())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.MULTIPLE_VCRS_FOUND, msg, extra, posType);
            }

            if (StringUtils.containsIgnoreCase(msg, ErrorType.CHECK_PREVIOUS_VCR_COUPON_STATUS_WARNING.getSearchableCode())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.CHECK_PREVIOUS_VCR_COUPON_STATUS_WARNING, msg, extra, posType);
            }

            if (StringUtils.containsIgnoreCase(msg, ErrorType.NOT_VCR_FOUND_ISSUE_TKT_OR_COLLECT_TKT.getSearchableCode())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.NOT_VCR_FOUND_ISSUE_TKT_OR_COLLECT_TKT, msg, extra, posType);
            }

            if (StringUtils.containsIgnoreCase(msg, ErrorType.INVALID_PRIORITY_CLASSIFICATION_CODE.getSearchableCode())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.INVALID_PRIORITY_CLASSIFICATION_CODE, msg, extra, posType);
            }

            if (StringUtils.containsIgnoreCase(msg, ErrorType.PAID_SEAT_REQUIRES_FEE_COLLECTION.getSearchableCode())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PAID_SEAT_REQUIRES_FEE_COLLECTION, msg, extra, posType);
            }

            if (StringUtils.containsIgnoreCase(msg, ErrorType.PASSENGER_QUALIFIES_FOR_PAID_SEAT.getSearchableCode())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PASSENGER_QUALIFIES_FOR_PAID_SEAT, msg, extra, posType);
            }

            if (StringUtils.containsIgnoreCase(msg, ErrorType.VERIFY_TRAVEL_DOCUMENT_DATA.getSearchableCode())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.VERIFY_TRAVEL_DOCUMENT_DATA, msg, extra, posType);
            }

            if (StringUtils.containsIgnoreCase(msg, ErrorType.CANADA_CLEARANCE_REQD_FOR_PASSENGER.getSearchableCode())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.CANADA_CLEARANCE_REQD_FOR_PASSENGER, msg, extra, posType);
            }

            if (StringUtils.containsIgnoreCase(msg, ErrorType.CANNOT_VALIDATE_BOOKING_CODE_OR_CLASS_OF_SERVICE.getSearchableCode())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.CANNOT_VALIDATE_BOOKING_CODE_OR_CLASS_OF_SERVICE, msg, extra, posType);
            }

            if (StringUtils.containsIgnoreCase(msg, ErrorType.CLASS_OF_SERVICE_NOT_DEFINED.getSearchableCode())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.CLASS_OF_SERVICE_NOT_DEFINED, msg, extra, posType);
            }

            if (StringUtils.containsIgnoreCase(msg, ErrorType.DHS_CLEARANCE_REQD_FOR_PASSENGER.getSearchableCode())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.DHS_CLEARANCE_REQD_FOR_PASSENGER, msg, extra, posType);
            }

            if (StringUtils.containsIgnoreCase(msg, ErrorType.FLIGHT_LEG_RESTRICTED.getSearchableCode())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.FLIGHT_LEG_RESTRICTED, msg, extra, posType);
            }

            if (StringUtils.containsIgnoreCase(msg, ErrorType.INHIBITED_VERIFY_ID_AND_ENTER_VID_EDIT.getSearchableCode())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.INHIBITED_VERIFY_ID_AND_ENTER_VID_EDIT, msg, extra, posType);
            }

            if (StringUtils.containsIgnoreCase(msg, ErrorType.INSUFFICIENT_DATA_FOR_ESTA.getSearchableCode())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.INSUFFICIENT_DATA_FOR_ESTA, msg, extra, posType);
            }

            if (StringUtils.containsIgnoreCase(msg, ErrorType.NO_VCR_FOUND_ISSUE_TKT_OR_COLLECT_TKT.getSearchableCode())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.NO_VCR_FOUND_ISSUE_TKT_OR_COLLECT_TKT, msg, extra, posType);
            }

            if (StringUtils.containsIgnoreCase(msg, ErrorType.PASSENGER_CONTACT_INFO_REQUIRED.getSearchableCode())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PASSENGER_CONTACT_INFO_REQUIRED, msg, extra, posType);
            }

            if (StringUtils.containsIgnoreCase(msg, ErrorType.REQUEST_DHS_STATUS_OR_PERFORM_OVERRIDE_ENTRY.getSearchableCode())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.REQUEST_DHS_STATUS_OR_PERFORM_OVERRIDE_ENTRY, msg, extra, posType);
            }

            if (StringUtils.containsIgnoreCase(msg, ErrorType.UK_BORDER_CLEARANCE_REQD_FOR_PSGR.getSearchableCode())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UK_BORDER_CLEARANCE_REQD_FOR_PSGR, msg, extra, posType);
            }

            if (StringUtils.containsIgnoreCase(msg, ErrorType.VERIFY_CANADIAN_VISA_OR_PSGR.getSearchableCode())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.VERIFY_CANADIAN_VISA_OR_PSGR, msg, extra, posType);
            }

            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNKNOWN_CODE_ABORT, msg, extra, posType);
        }

        return msg;
    }

    private String getTextInfo(ACSCheckInPassengerRSACS checkInPassengerRS) {
        StringBuilder sbInfo = new StringBuilder();
        if (null != checkInPassengerRS && null != checkInPassengerRS.getItineraryPassengerList()
                && null != checkInPassengerRS.getItineraryPassengerList().getItineraryPassenger()
                && !checkInPassengerRS.getItineraryPassengerList().getItineraryPassenger().isEmpty()) {

            for (ItineraryPassengerACS itineraryPassengerACS : checkInPassengerRS.getItineraryPassengerList().getItineraryPassenger()) {

                if (null != itineraryPassengerACS.getPassengerDetailList()
                        && null != itineraryPassengerACS.getPassengerDetailList().getPassengerDetail()
                        && !itineraryPassengerACS.getPassengerDetailList().getPassengerDetail().isEmpty()) {

                    for (PassengerDetailACS passengerDetailACS : itineraryPassengerACS.getPassengerDetailList().getPassengerDetail()) {

                        FreeTextInfoListACS freeTextInfoListACS = passengerDetailACS.getFreeTextInfoList();

                        if (null != freeTextInfoListACS) {

                            for (FreeTextInfoACS freeTextInfoACS : freeTextInfoListACS.getFreeTextInfo()) {
                                for (String text : freeTextInfoACS.getTextLine().getText()) {

                                    sbInfo.append(text).append(" , ");

                                    //!PAID SEAT REQUIRES FEE COLLECTION
                                    //PASSENGER QUALIFIES FOR PAID SEAT
                                    //!INVALID PRIORITY CLASSIFICATION CODE
                                    //!ENSURE PASSENGER MEETS DESIGNATED EXIT ROW CRITERIA
                                    //!PASSENGER ALREADY CHECKED IN - USE GRB
                                    //!PASSENGER NOT CHECKED IN
                                    //TPF | code: 606, value: PROCESSING ERROR - NO UNFUFLFILLED AE ITEM
                                    //TODO: add more exceptions
                                }
                            }
                        }
                    }
                }
            }
        }
        String msg = "";
        try {
            String info = sbInfo.toString();
            if (null != info && !info.trim().isEmpty()) {
                msg = info.trim();
            }
        } catch (Exception ex) {
            //
        }

        return msg;
    }

    public SeatMap getSeatMapCapacity(PurchaseOrderRQ purchaseOrderRQ){

        SeatmapCheckInRQ seatmapCheckInRQ  =new SeatmapCheckInRQ();
        SeatMap seatMapCollectionFinal = null;
        try {
            seatmapCheckInRQ.setCartId(purchaseOrderRQ.getPurchaseOrder().getCartId());
            seatmapCheckInRQ.setPos(purchaseOrderRQ.getPos());
            seatmapCheckInRQ.setLanguage(purchaseOrderRQ.getLanguage());

            SeatmapCollection seatMapCollection = null;
        
            seatMapCollection = seatMapsServiceSoap.getSeatMaps(seatmapCheckInRQ);
       
            if(null != seatMapCollection){
                AbstractSeatMap collection = (SeatMap) seatMapCollection.getCollection().get(0);

                if (collection instanceof SeatMap) {
                        seatMapCollectionFinal = (SeatMap) collection;
                }
            }
        } catch (SabreEmptyResponseException ser) {
            LOGGER.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110302).toString(), ser.fillInStackTrace());
            throw ser;
        }catch (SabreLayerUnavailableException se){
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(purchaseOrderRQ.getPos())) {
                LOGGER.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_22110301,"CheckinService.java:2742", se.getMessage());
                throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getCode(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getActionable(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getMessage(),
                        Response.Status.INTERNAL_SERVER_ERROR);
            } else {
                LOGGER.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_12110301,"CheckinService.java:2748", se.getMessage());
                throw se;
            }
        } catch (Exception e) {
            return null;
        }

        return seatMapCollectionFinal;
    }

    /**
     * @param recordLocator
     * @param cartId
     * @param title
     * @param checkInPassengerRQ
     * @param checkInPassengerRS
     */
    public void log(String recordLocator, String cartId, String title, ACSCheckInPassengerRQACS checkInPassengerRQ, ACSCheckInPassengerRSACS checkInPassengerRS) {
        try {

            StringBuilder sb = new StringBuilder();
            sb.append("\n").append("Checkin Service: ").append(title).append("\n");
            sb.append("recordLocator: ").append(recordLocator).append("\n");
            sb.append("cartId: ").append(cartId).append("\n");
            //sb.append("RQ: ").append(checkInPassService.toString(checkInPassengerRQ)).append("\n");
            //sb.append("RS: ").append(checkInPassService.toString(checkInPassengerRS)).append("\n");
            LOGGER.info(sb.toString());
        } catch (Exception ex) {
            //ignore
        }
    }

    /**
     * @param recordLocator
     * @param cartId
     * @param title
     * @param checkInPassengerRQ
     * @param message
     */
    public void logError(String recordLocator, String cartId, String title, ACSCheckInPassengerRQACS checkInPassengerRQ, String message) {
        try {

            StringBuilder sb = new StringBuilder();
            sb.append("\n").append("Checkin Service: ").append(title).append("\n");
            sb.append("recordLocator: ").append(recordLocator).append("\n");
            sb.append("cartId: ").append(cartId).append("\n");
            //sb.append("RQ: ").append(checkInPassService.toString(checkInPassengerRQ)).append("\n");
            sb.append("RS: ").append(message).append("\n");
            LOGGER.error(sb.toString());
        } catch (Exception ex) {
            //ignore
        }
    }

    /**
     * @param recordLocator
     * @param cartId
     * @param title
     */
    public void logError(String recordLocator, String cartId, String title) {
        try {

            StringBuilder sb = new StringBuilder();
            sb.append("\n").append("Checkin Service: ").append(title).append("\n");
            sb.append("recordLocator: ").append(recordLocator).append("\n");
            sb.append("cartId: ").append(cartId).append("\n");
            LOGGER.error(sb.toString());
        } catch (Exception ex) {
            //ignore
        }
    }

    /**
     * @return the setUpConfigFactory
     */
    public CheckInSetUpConfigFactoryWrapper getSetUpConfigFactory() {
        if (null == setUpConfigFactory) {
            setUpConfigFactory = new CheckInSetUpConfigFactoryWrapper();
        }
        return setUpConfigFactory;
    }

    /**
     * @param setUpConfigFactory the setUpConfigFactory to set
     */
    public void setSetUpConfigFactory(CheckInSetUpConfigFactoryWrapper setUpConfigFactory) {
        this.setUpConfigFactory = setUpConfigFactory;
    }

    /**
     * @return the seatMapsServiceSoap
     */
    public SeatMapsServiceSoap getSeatMapsServiceSoap() {
        return seatMapsServiceSoap;
    }

    /**
     * @param seatMapsServiceSoap the seatMapsServiceSoap to set
     */
    public void setSeatMapsServiceSoap(SeatMapsServiceSoap seatMapsServiceSoap) {
        this.seatMapsServiceSoap = seatMapsServiceSoap;
    }

    /**
     * @return the editPassCharService
     */
    public AMEditPassCharService getEditPassCharService() {
        return editPassCharService;
    }

    /**
     * @param editPassCharService the editPassCharService to set
     */
    public void setEditPassCharService(AMEditPassCharService editPassCharService) {
        this.editPassCharService = editPassCharService;
    }

}
