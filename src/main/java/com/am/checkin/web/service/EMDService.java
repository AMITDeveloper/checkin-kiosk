package com.am.checkin.web.service;

import com.aeromexico.sabre.api.session.exception.SabreEmptyResponseException;
import com.aeromexico.sabre.api.session.exception.SabreLayerUnavailableException;
import com.aeromexico.sharedservices.services.TicketDocumentService;
import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.exception.model.RetryCheckinException;
import com.aeromexico.commons.model.AbstractAncillary;
import com.aeromexico.commons.model.Ancillary;
import com.aeromexico.commons.model.AuthorizationPaymentConfig;
import com.aeromexico.commons.model.AuthorizationResult;
import com.aeromexico.commons.model.BaggageRoute;
import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.BookingClass;
import com.aeromexico.commons.model.CabinUpgradeAncillary;
import com.aeromexico.commons.model.CreditCardPaymentRequest;
import com.aeromexico.commons.model.CurrencyValue;
import com.aeromexico.commons.model.ExtraWeightAncillary;
import com.aeromexico.commons.model.FormsOfPaymentWrapper;
import com.aeromexico.commons.model.PurchaseOrder;
import com.aeromexico.commons.model.Segment;
import com.aeromexico.commons.model.TravelerAncillary;
import com.aeromexico.commons.model.UatpPaymentRequest;
import com.aeromexico.commons.model.UpgradeAncillary;
import com.aeromexico.commons.model.rq.PurchaseOrderRQ;
import com.aeromexico.commons.model.utils.CurrencyUtil;
import com.aeromexico.commons.web.types.ActionCodeType;
import com.aeromexico.commons.web.types.AirlineCodeType;
import com.aeromexico.commons.web.types.AncillaryGroupType;
import com.aeromexico.commons.web.types.AncillaryPostBookingType;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.types.PosType;
import com.aeromexico.commons.web.types.SabreCreditCardType;
import com.aeromexico.commons.web.util.FilterPassengersUtil;
import com.aeromexico.commons.web.util.SegmentCodeUtil;
import com.aeromexico.commons.web.util.TimeUtil;
import com.aeromexico.dao.config.GlobalConfiguration;
import com.aeromexico.sabre.api.miscservice.AMMiscService;
import com.aeromexico.sabre.api.session.common.ServiceCall;
import com.aeromexico.sabre.api.sessionpool.factory.SabreSession;
import com.aeromexico.sharedservices.util.AncillaryUtil;
import com.am.checkin.web.util.PurchaseOrderUtil;
import com.am.checkin.web.util.TicketDocumentUtil;
import com.sabre.ns.ticketing.dc.GetTicketingDocumentRS;
import com.sabre.services.micellaneous.Amount;
import com.sabre.services.micellaneous.BookingIndicator;
import com.sabre.services.micellaneous.CollectMiscFeeRQ;
import com.sabre.services.micellaneous.CollectMiscFeeRS;
import com.sabre.services.micellaneous.CollectMiscTransInfo;
import com.sabre.services.micellaneous.CreditCardMisc;
import com.sabre.services.micellaneous.CustomerDetailsMisc;
import com.sabre.services.micellaneous.CustomerMisc;
import com.sabre.services.micellaneous.DocumentNumber;
import com.sabre.services.micellaneous.EmdType;
import com.sabre.services.micellaneous.ErrorOrSuccessCode;
import com.sabre.services.micellaneous.FeeDetailsMisc;
import com.sabre.services.micellaneous.FeeLinked;
import com.sabre.services.micellaneous.FeesLinked;
import com.sabre.services.micellaneous.FeesType;
import com.sabre.services.micellaneous.FlightMisc;
import com.sabre.services.micellaneous.FormOfPaymentChoiceMisc;
import com.sabre.services.micellaneous.LocationPOS;
import com.sabre.services.micellaneous.MiscTransInfo;
import com.sabre.services.micellaneous.POSMisc;
import com.sabre.services.micellaneous.PaymentMisc;
import com.sabre.services.micellaneous.RequestedDetailLevel;
import com.sabre.services.micellaneous.SegmentIndicator;
import com.sabre.services.micellaneous.SystemSpecificResults;
import com.sabre.services.micellaneous.TaxMisc;
import com.sabre.services.micellaneous.TransactionCodeMisc;
import org.aeromexico.commons.exception.config.ReadProperties;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

/**
 * @author adrian
 */
@Named
@ApplicationScoped
public class EMDService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EMDService.class);

    private static final String VERSION = "1.4.1";
    private static final String COMPANY = "AM";
    private static final String SINE = "AD";

    @Inject
    private AMMiscService miscService;

    @Inject
    private TicketDocumentService ticketDocumentService;

    @Inject
    private LogOnDatabaseService logOnDatabaseService;

    @Inject
    @Named("CheckInSetUpConfigFactoryWrapper")
    private CheckInSetUpConfigFactoryWrapper setUpConfigFactory;

    @Inject
    private ReadProperties prop;

    @PostConstruct
    public void init() {
        if (null == setUpConfigFactory) {
            setUpConfigFactory = new CheckInSetUpConfigFactoryWrapper();
        }
    }

    /**
     * @param purchaseOrder
     * @param paymentMiscList
     * @param authorizationResultList
     */
    private void setPaymentMisc(
            PurchaseOrder purchaseOrder,
            List<PaymentMisc> paymentMiscList,
            List<AuthorizationResult> authorizationResultList,
            String pos
    ) {

        for (AuthorizationResult authorizationResult : authorizationResultList) {
            if (null == authorizationResult || !"APPROVED".equalsIgnoreCase(authorizationResult.getResponseCode())) {
                if (PosType.WEB.toString().equalsIgnoreCase(pos)) {
                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.AUTHORIZATION_PAYMENT_NOT_APPROVED_WEB, ErrorType.AUTHORIZATION_PAYMENT_NOT_APPROVED_WEB.getFullDescription());
                } else {
                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.AUTHORIZATION_PAYMENT_NOT_APPROVED, ErrorType.AUTHORIZATION_PAYMENT_NOT_APPROVED.getFullDescription());
                }
            }
        }

        FormsOfPaymentWrapper formsOfPaymentWrapper;
        formsOfPaymentWrapper = new FormsOfPaymentWrapper(
                purchaseOrder.getPaymentInfo()
        );

        if (!formsOfPaymentWrapper.getCreditCardPaymentRequestList().isEmpty()) {
            setCreditCardPayment(
                    formsOfPaymentWrapper.getCreditCardPaymentRequestList(),
                    authorizationResultList,
                    paymentMiscList
            );
        } else if (!formsOfPaymentWrapper.getUatpPaymentRequestList().isEmpty()) {
            setUatpPayment(
                    formsOfPaymentWrapper.getUatpPaymentRequestList(),
                    authorizationResultList,
                    paymentMiscList,
                    purchaseOrder.getPaymentAmount()
            );
        }

    }

    /**
     * creditCardPaymentRequest
     *
     * @param uatpPaymentRequestList
     * @param authorizationResultList
     * @param paymentMiscList
     */
    private void setUatpPayment(
            List<UatpPaymentRequest> uatpPaymentRequestList,
            List<AuthorizationResult> authorizationResultList,
            List<PaymentMisc> paymentMiscList,
            CurrencyValue totalInCart
    ) {

        for (UatpPaymentRequest uatpPaymentRequest : uatpPaymentRequestList) {

            PaymentMisc paymentMisc = new PaymentMisc();

            Amount amount = new Amount();
            amount.setValue(CurrencyUtil.getAmount(
                    totalInCart.getTotal(),
                    totalInCart.getCurrencyCode())
            );
            amount.setCurrencyCode(totalInCart.getCurrencyCode());
            paymentMisc.setAmount(amount);

            FormOfPaymentChoiceMisc formOfPaymentChoiceMisc = new FormOfPaymentChoiceMisc();
            CreditCardMisc creditCardMisc = new CreditCardMisc();

            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
                Date expiryDate = simpleDateFormat.parse(uatpPaymentRequest.getExpiryDate());
                simpleDateFormat = new SimpleDateFormat("MMyy");
                String expiryDateStr = simpleDateFormat.format(expiryDate);
                creditCardMisc.setExpiryDate(expiryDateStr);
            } catch (ParseException ex) {
                //set expiry date to december of the next year
                Calendar now = Calendar.getInstance();          // Gets the current date and time
                int year = now.get(Calendar.YEAR) + 1;        // The current year as an int
                uatpPaymentRequest.setExpiryDate("" + year + "-12");
                String yearStr = String.valueOf(year);
                if (yearStr.length() >= 3) {
                    creditCardMisc.setExpiryDate("12" + yearStr.substring(2));
                } else {
                    creditCardMisc.setExpiryDate("12" + yearStr);
                }
            }

            creditCardMisc.setCode(SabreCreditCardType.TP.getCardCode());
            creditCardMisc.setNumber(uatpPaymentRequest.getCardCode());

            formOfPaymentChoiceMisc.setCreditCard(creditCardMisc);
            paymentMisc.setFormOfPayment(formOfPaymentChoiceMisc);

//            AuthorizationResult authorizationResult = PaymentService.getAuthorizationResult(
//                    uatpPaymentRequest.getPaymentRef(),
//                    authorizationResultList
//            );

            AuthorizationResult authorizationResult = authorizationResultList.get(0);

            if (null != authorizationResult) {
                creditCardMisc.setApprovalCode(authorizationResult.getApprovalCode());
                creditCardMisc.setTransactionID(authorizationResult.getPaymentId());

                paymentMiscList.add(paymentMisc);
            }

            //just one credit card for now
            break;
        }

    }

    /**
     * @param creditCardPaymentRequestList
     * @param authorizationResultList
     * @param paymentMiscList
     */
    private void setCreditCardPayment(
            List<CreditCardPaymentRequest> creditCardPaymentRequestList,
            List<AuthorizationResult> authorizationResultList,
            List<PaymentMisc> paymentMiscList
    ) {
        for (CreditCardPaymentRequest creditCardPaymentRequest : creditCardPaymentRequestList) {

            PaymentMisc paymentMisc = new PaymentMisc();

            Amount amount = new Amount();
            amount.setValue(CurrencyUtil.getAmount(
                    creditCardPaymentRequest.getPaymentAmount().getTotal(),
                    creditCardPaymentRequest.getPaymentAmount().getCurrencyCode())
            );
            amount.setCurrencyCode(creditCardPaymentRequest.getPaymentAmount().getCurrencyCode());
            paymentMisc.setAmount(amount);

            FormOfPaymentChoiceMisc formOfPaymentChoiceMisc = new FormOfPaymentChoiceMisc();
            CreditCardMisc creditCardMisc = new CreditCardMisc();

            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
                Date expiryDate = simpleDateFormat.parse(creditCardPaymentRequest.getExpiryDate());
                simpleDateFormat = new SimpleDateFormat("MMyy");
                String expiryDateStr = simpleDateFormat.format(expiryDate);
                creditCardMisc.setExpiryDate(expiryDateStr);
            } catch (ParseException ex) {
                //set expiry date to december of the next year
                Calendar now = Calendar.getInstance();          // Gets the current date and time
                int year = now.get(Calendar.YEAR) + 1;        // The current year as an int
                creditCardPaymentRequest.setExpiryDate("" + year + "-12");
                String yearStr = String.valueOf(year);
                if (yearStr.length() >= 3) {
                    creditCardMisc.setExpiryDate("12" + yearStr.substring(2));
                } else {
                    creditCardMisc.setExpiryDate("12" + yearStr);
                }
            }

            creditCardMisc.setCode(creditCardPaymentRequest.getCardCode());
            creditCardMisc.setNumber(creditCardPaymentRequest.getCcNumber());

            if (null != creditCardPaymentRequest.getSecurityCode()
                    && !creditCardPaymentRequest.getSecurityCode().trim().isEmpty()) {
                creditCardMisc.setSecurityCode(creditCardPaymentRequest.getSecurityCode());
            }

            formOfPaymentChoiceMisc.setCreditCard(creditCardMisc);
            paymentMisc.setFormOfPayment(formOfPaymentChoiceMisc);

//            AuthorizationResult authorizationResult = PaymentService.getAuthorizationResult(
//                    creditCardPaymentRequest.getPaymentRef(),
//                    authorizationResultList
//            );

            AuthorizationResult authorizationResult = authorizationResultList.get(0);

            if (null != authorizationResult) {
                creditCardMisc.setApprovalCode(authorizationResult.getApprovalCode());
                creditCardMisc.setTransactionID(authorizationResult.getPaymentId());
                paymentMiscList.add(paymentMisc);
            }

            //just one credit card for now
            break;

        }
    }

    /**
     * @param bookedTravelerList
     * @param bookedLeg
     * @param purchaseOrderRQ
     * @param authorizationResultList
     * @param recordLocator
     * @param cartId
     * @param upgradeAncillaryList
     * @param ticketingDocumentRS
     * @param serviceCallList
     * @return
     * @throws Exception
     */
    public CollectMiscFeeRQ getCollectMiscFeeRQForUpgrades(
            List<BookedTraveler> bookedTravelerList,
            BookedLeg bookedLeg,
            PurchaseOrderRQ purchaseOrderRQ,
            List<AuthorizationResult> authorizationResultList,
            String recordLocator,
            String cartId,
            List<UpgradeAncillary> upgradeAncillaryList,
            GetTicketingDocumentRS ticketingDocumentRS,
            List<ServiceCall> serviceCallList
    ) throws Exception {

        PurchaseOrder purchaseOrder = purchaseOrderRQ.getPurchaseOrder();
        String store = purchaseOrderRQ.getStore();
        String pos = purchaseOrderRQ.getPos();

        CollectMiscFeeRQ collectMiscFeeRQ = new CollectMiscFeeRQ();
        collectMiscFeeRQ.setVersion(VERSION);
        collectMiscFeeRQ.setDetailLevel(RequestedDetailLevel.FULL);


        GlobalConfiguration config = setUpConfigFactory.getInstance();

        AuthorizationPaymentConfig authorizationPaymentConfig = config.getAuthorizationPaymentConfig(store, pos);

        LocationPOS locationPOS = new LocationPOS();
        locationPOS.setValue(authorizationPaymentConfig.getCityCode());
        locationPOS.setCountry(authorizationPaymentConfig.getIsoCountry());
        locationPOS.setNumber(authorizationPaymentConfig.getStationNumber());

        POSMisc posMisc = new POSMisc();
        posMisc.setCompany(COMPANY);
        posMisc.setAAA(locationPOS);
        posMisc.setDutyCode(config.getDutyCode());
        posMisc.setLniata(config.getTicketLNIATA());
        posMisc.setSine(SINE);
        collectMiscFeeRQ.setAgentPOS(posMisc);
        /*End config*/

        CollectMiscTransInfo collectMiscTransInfo = new CollectMiscTransInfo();
        collectMiscTransInfo.setPrinterLniata(config.getTicketLNIATA());
        collectMiscTransInfo.setPrinterStock(config.getPrinterStock());
        collectMiscTransInfo.setPrintTaxCpn(false);
        collectMiscFeeRQ.setParameters(collectMiscTransInfo);

        MiscTransInfo miscTransInfo = new MiscTransInfo();
        miscTransInfo.setCode(TransactionCodeMisc.EMD);
        collectMiscFeeRQ.setTransaction(miscTransInfo);

        List<FeesType> feesTypeList = collectMiscFeeRQ.getFees();

        for (BookedTraveler bookedTraveler : bookedTravelerList) {
            FeesType feeType = new FeesType();
            FeesLinked feesLinked = new FeesLinked();
            feeType.setLinked(feesLinked);

            CustomerMisc customerMisc = new CustomerMisc();
            customerMisc.setFirstName(bookedTraveler.getFirstName());
            customerMisc.setLastName(bookedTraveler.getLastName());
            CustomerDetailsMisc customerDetailsMisc = new CustomerDetailsMisc();
            customerDetailsMisc.setPnrLocator(recordLocator);
            customerDetailsMisc.setNameRefNumber(new BigDecimal(bookedTraveler.getNameRefNumber()));
            customerMisc.setCustomerDetails(customerDetailsMisc);
            feesLinked.setCustomer(customerMisc);
            List<FeeLinked> feeLinkedList = feesLinked.getFee();
            boolean isThereProducts = false;

            List<CabinUpgradeAncillary> allUnpaidUpgradesAncillaries;
            allUnpaidUpgradesAncillaries = PurchaseOrderUtil.getUpgradeUnpaidAncillariesOnReservation(
                    bookedTraveler.getAncillaries().getCollection()
            );

            for (CabinUpgradeAncillary cabinUpgradeAncillary : allUnpaidUpgradesAncillaries) {
                if (null == cabinUpgradeAncillary.getSegmentCode() || cabinUpgradeAncillary.getSegmentCode().isEmpty()) {
                    LOGGER.error("The ancillary does not have a segmentCode need to be associated with a segment flight");
                } else if (ActionCodeType.HD.toString().equalsIgnoreCase(cabinUpgradeAncillary.getActionCode())
                        || -1 == BigDecimal.ZERO.compareTo(cabinUpgradeAncillary.getCurrency().getTotal())) {

                    UpgradeAncillary upgradeAncillary = PurchaseOrderUtil.getUpgradeAncillary(
                            cabinUpgradeAncillary.getSegmentCode(), upgradeAncillaryList
                    );

                    if (null != upgradeAncillary
                            && null != upgradeAncillary.getAncillaryOffer()
                            && null != upgradeAncillary.getAncillaryOffer().getAncillary()) {

                        Ancillary ancillary = upgradeAncillary.getAncillaryOffer().getAncillary();

                        String currencyCode = cabinUpgradeAncillary.getCurrency().getCurrencyCode();

                        isThereProducts = true;
                        FeeLinked feeLinked = new FeeLinked();
                        feeLinkedList.add(feeLinked);

                        FeeDetailsMisc feeDetailsMisc = new FeeDetailsMisc();

                        Amount base = getAmountBase(cabinUpgradeAncillary.getCurrency());
                        feeDetailsMisc.setBase(base);
                        Amount totalTaxes = getAmountTaxes(cabinUpgradeAncillary.getCurrency());
                        feeDetailsMisc.setTotalTax(totalTaxes);
                        Amount total = getAmountTotal(cabinUpgradeAncillary.getCurrency());
                        feeDetailsMisc.setTotal(total);
                        //feeDetailsMisc.setEquiv(total);
                        feeDetailsMisc.setCode(ancillary.getRficSubcode());
                        feeDetailsMisc.setQuantity(cabinUpgradeAncillary.getQuantity());

                        feeLinked.setFeeDetails(feeDetailsMisc);

                        List<TaxMisc> taxes = feeLinked.getTax();

                        Map<String, BigDecimal> taxesMap = cabinUpgradeAncillary.getCurrency().getTaxDetails();
                        for (Map.Entry<String, BigDecimal> entry : taxesMap.entrySet()) {
                            Amount amount = getAmountTotal(currencyCode, entry.getValue());

                            TaxMisc taxMisc = new TaxMisc();
                            taxMisc.setAmount(amount);
                            taxMisc.setCode(entry.getKey());
                            taxMisc.setExempt(Boolean.FALSE);

                            taxes.add(taxMisc);
                        }

                        FeeLinked.OptionalService optionalService = new FeeLinked.OptionalService();
                        optionalService.setRFIC(ancillary.getRficCode());

                        BookingIndicator bookingIndicator;
                        try {
                            bookingIndicator = BookingIndicator.fromValue(ancillary.getBookingIndicator().toUpperCase());
                        } catch (Exception ex) {
                            bookingIndicator = BookingIndicator.SSR;
                        }

                        optionalService.setBookingIndicator(bookingIndicator);
                        optionalService.setGroup(ancillary.getGroupCode());
                        optionalService.setName(ancillary.getCommercialName());

                        SegmentIndicator segmentIndicator;
                        try {
                            segmentIndicator = SegmentIndicator.fromValue(ancillary.getSegmentIndicator().toUpperCase());
                        } catch (Exception ex) {
                            segmentIndicator = SegmentIndicator.S;
                        }

                        optionalService.setSegmentIndicator(segmentIndicator);
                        optionalService.setSubCode(ancillary.getRficSubcode());
                        optionalService.setEmdType(EmdType.ASSOCIATED);
                        optionalService.setAirExtraItemNumber(new BigInteger(cabinUpgradeAncillary.getAncillaryId()));
                        optionalService.setVendor(ancillary.getVendor());
                        optionalService.setOwningCarrierCode(ancillary.getOwningCarrierCode());
                        optionalService.setSsrCode(ancillary.getSsrCode());

                        feeLinked.setOptionalService(optionalService);

                        List<FlightMisc> flightMiscList = feeLinked.getAssociatedFlight();

                        boolean initialized = false;
                        String flightNumber = "";
                        String classOfService = "";
                        String couponNumber = "";
                        int segmentID = 0;

                        for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {
                            Segment segment = bookedSegment.getSegment();

                            if (null != segment) {
                                BaggageRoute baggageRoute = PurchaseOrderUtil.getBaggageRouteBySegment(
                                        segment, bookedTraveler.getBaggageRouteList()
                                );

                                if (null != baggageRoute) {
                                    classOfService = baggageRoute.getBookingClass();
                                    segmentID = baggageRoute.getSegmentID();
                                }

                                boolean areSegmentCodesEquals = SegmentCodeUtil.compareSegmentCodeWithoutTime(
                                        cabinUpgradeAncillary.getSegmentCode(), segment.getSegmentCode()
                                );

                                if (initialized || areSegmentCodesEquals) {

                                    if (areSegmentCodesEquals) {
                                        initialized = true;
                                        flightNumber = segment.getOperatingFlightCode();

                                        couponNumber = getCouponNumber(
                                                segment, bookedTraveler.getTicketNumber(),
                                                recordLocator, cartId, ticketingDocumentRS,
                                                serviceCallList, purchaseOrderRQ.getPos()
                                        );

                                    } else if (!flightNumber.isEmpty() && !FilterPassengersUtil.compareFlightNumber(
                                            flightNumber, segment.getOperatingFlightCode()
                                    )) {
                                        break;
                                    }

                                    FlightMisc flightMisc = new FlightMisc();
                                    flightMiscList.add(flightMisc);

                                    DocumentNumber documentNumber = new DocumentNumber();
                                    documentNumber.setValue(bookedTraveler.getTicketNumber());
                                    documentNumber.setCouponNumber(couponNumber);
                                    flightMisc.setAssociatedTicketNumber(documentNumber);

                                    flightMisc.setArrivalCity(segment.getArrivalAirport());
                                    flightMisc.setDepartureCity(segment.getDepartureAirport());
                                    flightMisc.setCarrierCode(segment.getOperatingCarrier());
                                    flightMisc.setClassOfService(classOfService);

                                    try {
                                        GregorianCalendar c = new GregorianCalendar();
                                        c.setTimeInMillis(TimeUtil.getTime(segment.getDepartureDateTime()));
                                        XMLGregorianCalendar xmlGregorianCalendar;
                                        xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
                                        xmlGregorianCalendar.setMillisecond(DatatypeConstants.FIELD_UNDEFINED);
                                        xmlGregorianCalendar.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
                                        flightMisc.setDepartureDate(xmlGregorianCalendar);
                                    } catch (ParseException | DatatypeConfigurationException ex) {
                                        LOGGER.error("Error parsing departure datetime: " + ex.getMessage());
                                    }

                                    flightMisc.setFlightNumber(segment.getOperatingFlightCode());
                                    flightMisc.setOperatingCarrierCode(segment.getOperatingCarrier());

                                    try {
                                        flightMisc.setSegmentNumber(new Integer(segment.getSegmentNumber()));
                                    } catch (NumberFormatException ex) {
                                        LOGGER.error("Error parsing segment number: " + ex.getMessage());
                                    }

                                    flightMisc.setSegmentID(segmentID);
                                }
                            } else {
                                LOGGER.error("Itinerary is null, we can not associate a flight for that seat");
                            }
                        }
                    } else {
                        //
                        logError(recordLocator, cartId, "Ancillary upgrade was not found in db collection");
                    }
                } else {
                    //
                    logError(recordLocator, cartId, "There was not fees (possible cause action code different that HD)");
                }
            }

            if (isThereProducts) {
                feesTypeList.add(feeType);
            }
        }

        Amount amount = new Amount();

        String currencyCode = CurrencyUtil.getCurrencyCode(purchaseOrder.getPaymentInfo());
        BigDecimal total = CurrencyUtil.getTotal(purchaseOrder.getPaymentInfo(), currencyCode);

        amount.setValue(CurrencyUtil.getAmount(total, currencyCode));
        amount.setCurrencyCode(currencyCode);
        collectMiscFeeRQ.setTotalCost(amount);

        List<PaymentMisc> paymentMiscList = collectMiscFeeRQ.getPayment();
        setPaymentMisc(purchaseOrder, paymentMiscList, authorizationResultList, pos);

        return collectMiscFeeRQ;
    }

    /**
     * @param flightMisc
     * @param ticketingDocumentRS
     * @param recordLocator
     * @param cartId
     * @param serviceCallList
     * @return
     * @throws Exception
     */
    public String getCouponNumber(
            FlightMisc flightMisc,
            GetTicketingDocumentRS ticketingDocumentRS,
            String recordLocator, String cartId,
            List<ServiceCall> serviceCallList,
            String pos
    ) throws Exception {
        String couponNumber;
        try{


        if (null == ticketingDocumentRS) {
            ticketingDocumentRS = ticketDocumentService.getTicketDocByRecordLocator(
                    recordLocator, cartId
            );
        }

        if (null != ticketingDocumentRS) {
            couponNumber = TicketDocumentUtil.getCoupon(
                    ticketingDocumentRS, flightMisc
            );

            if (null == couponNumber || couponNumber.isEmpty()) {
                couponNumber = "1";
            }
        } else {
            //set default coupon
            couponNumber = "1";
        }
        }catch (SabreLayerUnavailableException se){
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(pos)) {
                LOGGER.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_22110301,"EMDService.java:645", se.getMessage());
                throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getCode(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getActionable(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getMessage(),
                        Response.Status.INTERNAL_SERVER_ERROR);
            } else {
                LOGGER.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_12110301,"EMDService.java:651", se.getMessage());
                throw se;
            }
        }catch (SabreEmptyResponseException ser) {
            LOGGER.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110302).toString(), ser.fillInStackTrace());
            throw ser;
        }
        return couponNumber;
    }

    /**
     * The default coupon number is 1
     *
     * @param segment
     * @param ticketNumber
     * @param recordLocator
     * @param cartId
     * @param ticketingDocumentRS
     * @param serviceCallList
     * @return
     * @throws Exception
     */
    public String getCouponNumber(
            Segment segment, String ticketNumber,
            String recordLocator, String cartId,
            GetTicketingDocumentRS ticketingDocumentRS,
            List<ServiceCall> serviceCallList, String pos
    ) throws Exception {
        String couponNumber;
        try {
            if (null == segment.getCouponNumber() || segment.getCouponNumber().isEmpty()) {
                if (null == ticketingDocumentRS) {
                    ticketingDocumentRS = ticketDocumentService.getTicketDocByRecordLocator(
                            recordLocator, cartId
                    );
                }
                if (null != ticketingDocumentRS) {
                    couponNumber = TicketDocumentUtil.getCoupon(
                            ticketingDocumentRS,
                            segment,
                            ticketNumber
                    );

                    if (couponNumber.isEmpty()) {
                        LOGGER.error("Coupon not found, using defaul coupon number 1");
                        couponNumber = "1";
                    }
                } else {
                    //set default coupon
                    LOGGER.error("Coupon not found using, defaul coupon number 1");
                    couponNumber = "1";
                }
            } else {
                couponNumber = segment.getCouponNumber();
            }
        }catch (SabreLayerUnavailableException se){
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(pos)) {
                LOGGER.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_22110301,"EMDService.java:708", se.getMessage());
                throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getCode(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getActionable(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getMessage(),
                        Response.Status.INTERNAL_SERVER_ERROR);
            } else {
                LOGGER.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_12110301,"EMDService.java:714", se.getMessage());
                throw se;
            }
        }catch (SabreEmptyResponseException ser) {
            LOGGER.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110302).toString(), ser.fillInStackTrace());
            throw ser;
        }
        return couponNumber;
    }

    /**
     * @param collectMiscFeeRQ
     * @param ticketingDocumentRS
     * @param recordLocator
     * @param cartId
     * @param serviceCallList
     * @return
     */
    public boolean changeCouponNumber(
            CollectMiscFeeRQ collectMiscFeeRQ, GetTicketingDocumentRS ticketingDocumentRS,
            String recordLocator, String cartId, List<ServiceCall> serviceCallList, String pos
    ) {
        boolean changed = false;

        for (FeesType feesType : collectMiscFeeRQ.getFees()) {
            for (FeeLinked feeLinked : feesType.getLinked().getFee()) {
                for (FlightMisc flightMisc : feeLinked.getAssociatedFlight()) {
                    String couponNumber;
                    try {
                        couponNumber = getCouponNumber(
                                flightMisc, ticketingDocumentRS,
                                recordLocator, cartId,
                                serviceCallList, pos
                        );
                    } catch (Exception ex) {
                        couponNumber = "1";
                    }

                    if (couponNumber.equals(flightMisc.getAssociatedTicketNumber().getCouponNumber())) {
                        changed = true;
                        flightMisc.getAssociatedTicketNumber().setCouponNumber(couponNumber);
                    }
                }
            }
        }

        return changed;
    }

    /**
     * @param bookedTravelerList
     * @param bookedLeg
     * @param purchaseOrderRQ
     * @param authorizationResultList
     * @param ticketingDocumentRS
     * @param recordLocator
     * @param cartId
     * @param serviceCallList
     * @return
     * @throws Exception
     */
    public CollectMiscFeeRQ getCollectMiscFeeRQ(
            List<BookedTraveler> bookedTravelerList,
            BookedLeg bookedLeg,
            PurchaseOrderRQ purchaseOrderRQ,
            List<AuthorizationResult> authorizationResultList,
            GetTicketingDocumentRS ticketingDocumentRS,
            String recordLocator, String cartId,
            List<ServiceCall> serviceCallList
    ) throws Exception {

        PurchaseOrder purchaseOrder = purchaseOrderRQ.getPurchaseOrder();
        String store = purchaseOrderRQ.getStore();
        String pos = purchaseOrderRQ.getPos();

        CollectMiscFeeRQ collectMiscFeeRQ = new CollectMiscFeeRQ();
        collectMiscFeeRQ.setVersion(VERSION);
        collectMiscFeeRQ.setDetailLevel(RequestedDetailLevel.FULL);

        GlobalConfiguration config = setUpConfigFactory.getInstance();

        AuthorizationPaymentConfig authorizationPaymentConfig = config.getAuthorizationPaymentConfig(store, pos);

        LocationPOS locationPOS = new LocationPOS();
        locationPOS.setValue(authorizationPaymentConfig.getCityCode());
        locationPOS.setCountry(authorizationPaymentConfig.getIsoCountry());
        locationPOS.setNumber(authorizationPaymentConfig.getStationNumber());

        POSMisc posMisc = new POSMisc();
        posMisc.setCompany(COMPANY);
        posMisc.setAAA(locationPOS);
        posMisc.setDutyCode(config.getDutyCode());
        posMisc.setLniata(config.getTicketLNIATA());
        posMisc.setSine(SINE);
        collectMiscFeeRQ.setAgentPOS(posMisc);
        /*End config*/

        CollectMiscTransInfo collectMiscTransInfo = new CollectMiscTransInfo();
        collectMiscTransInfo.setPrinterLniata(config.getTicketLNIATA());
        collectMiscTransInfo.setPrinterStock(config.getPrinterStock());
        collectMiscTransInfo.setPrintTaxCpn(false);
        collectMiscFeeRQ.setParameters(collectMiscTransInfo);

        MiscTransInfo miscTransInfo = new MiscTransInfo();
        miscTransInfo.setCode(TransactionCodeMisc.EMD);
        collectMiscFeeRQ.setTransaction(miscTransInfo);

        List<FeesType> feesTypeList = collectMiscFeeRQ.getFees();

        for (BookedTraveler bookedTraveler : bookedTravelerList) {
            FeesType feeType = new FeesType();
            FeesLinked feesLinked = new FeesLinked();
            feeType.setLinked(feesLinked);

            CustomerMisc customerMisc = new CustomerMisc();
            customerMisc.setFirstName(bookedTraveler.getFirstName());
            customerMisc.setLastName(bookedTraveler.getLastName());
            CustomerDetailsMisc customerDetailsMisc = new CustomerDetailsMisc();
            customerDetailsMisc.setPnrLocator(recordLocator);
            customerDetailsMisc.setNameRefNumber(new BigDecimal(bookedTraveler.getNameRefNumber()));
            customerMisc.setCustomerDetails(customerDetailsMisc);
            feesLinked.setCustomer(customerMisc);
            List<FeeLinked> feeLinkedList = feesLinked.getFee();

            boolean isThereProducts = false;

            List<AbstractAncillary> allAncillaries = PurchaseOrderUtil.getAllAncillaries(bookedTraveler);
            List<TravelerAncillary> allUnpaidAncillaries = PurchaseOrderUtil.getUnpaidAncillariesOnReservation(allAncillaries);
            List<TravelerAncillary> allUnpaidAncillariesWithCost = PurchaseOrderUtil.getUnpaidAncillariesWithCost(allUnpaidAncillaries);

            for (TravelerAncillary travelerAncillary : allUnpaidAncillariesWithCost) {
                if (ActionCodeType.HD.toString().equalsIgnoreCase(travelerAncillary.getActionCode())
                        || -1 == BigDecimal.ZERO.compareTo(travelerAncillary.getAncillary().getCurrency().getTotal())) {

                    AncillaryUtil.convertCodeAncillary_2_0(travelerAncillary, bookedLeg.getMarketFlight());

                    String currencyCode = travelerAncillary.getAncillary().getCurrency().getCurrencyCode();

                    isThereProducts = true;
                    FeeLinked feeLinked = new FeeLinked();
                    feeLinkedList.add(feeLinked);

                    FeeDetailsMisc feeDetailsMisc = new FeeDetailsMisc();

                    Amount base = getAmountBase(travelerAncillary.getAncillary().getCurrency());
                    feeDetailsMisc.setBase(base);

                    Amount total = getAmountTotal(travelerAncillary.getAncillary().getCurrency());
                    feeDetailsMisc.setTotal(total);

                    Amount totalTaxes = getAmountTaxes(travelerAncillary.getAncillary().getCurrency());
                    feeDetailsMisc.setTotalTax(totalTaxes);

                    //feeDetailsMisc.setEquiv(total);
                    feeDetailsMisc.setCode(travelerAncillary.getAncillary().getRficSubcode());
                    feeDetailsMisc.setQuantity(travelerAncillary.getQuantity());

                    feeLinked.setFeeDetails(feeDetailsMisc);

                    List<TaxMisc> taxes = feeLinked.getTax();

                    Map<String, BigDecimal> taxesMap = travelerAncillary.getAncillary().getCurrency().getTaxDetails();
                    for (Map.Entry<String, BigDecimal> entry : taxesMap.entrySet()) {
                        Amount amount = getAmountTotal(currencyCode, entry.getValue());

                        TaxMisc taxMisc = new TaxMisc();
                        taxMisc.setAmount(amount);
                        taxMisc.setCode(entry.getKey());
                        taxMisc.setExempt(Boolean.FALSE);

                        taxes.add(taxMisc);
                    }

                    FeeLinked.OptionalService optionalService = new FeeLinked.OptionalService();
                    optionalService.setRFIC(travelerAncillary.getAncillary().getRficCode());

                    BookingIndicator bookingIndicator;
                    try {
                        bookingIndicator = BookingIndicator.fromValue(
                                travelerAncillary.getAncillary().getBookingIndicator().toUpperCase()
                        );
                    } catch (Exception ex) {
                        bookingIndicator = BookingIndicator.SSR;
                    }

                    optionalService.setBookingIndicator(bookingIndicator);
                    optionalService.setGroup(travelerAncillary.getAncillary().getGroupCode());
                    optionalService.setName(travelerAncillary.getAncillary().getCommercialName());

                    EmdType emdType;
                    if ("2".equals(travelerAncillary.getAncillary().getEmdType())) {
                        emdType = EmdType.ASSOCIATED;
                    } else {
                        emdType = EmdType.STANDALONE;
                    }

                    SegmentIndicator segmentIndicator;
                    try {
                        segmentIndicator = SegmentIndicator.fromValue(
                                travelerAncillary.getAncillary().getSegmentIndicator().toUpperCase()
                        );
                    } catch (Exception ex) {
                        segmentIndicator = SegmentIndicator.P;
                    }

                    optionalService.setSegmentIndicator(segmentIndicator);
                    optionalService.setSubCode(travelerAncillary.getAncillary().getRficSubcode());
                    optionalService.setEmdType(emdType);
                    optionalService.setAirExtraItemNumber(new BigInteger(travelerAncillary.getAncillaryId()));
                    optionalService.setVendor(travelerAncillary.getAncillary().getVendor());
                    optionalService.setOwningCarrierCode(travelerAncillary.getAncillary().getOwningCarrierCode());
                    optionalService.setSsrCode(travelerAncillary.getAncillary().getSsrCode());

                    feeLinked.setOptionalService(optionalService);

                    if (EmdType.ASSOCIATED.equals(emdType)) {

                        List<FlightMisc> flightMiscList = getFlightDetails(
                                travelerAncillary,
                                bookedLeg,
                                bookedTraveler,
                                recordLocator,
                                cartId,
                                ticketingDocumentRS,
                                serviceCallList,
                                purchaseOrderRQ.getPos()
                        );

                        feeLinked.getAssociatedFlight().addAll(flightMiscList);
                    }

                    AncillaryPostBookingType oldAnc = AncillaryUtil.convertNewCodeToOldCode(travelerAncillary.getAncillary().getRficSubcode());
                    if (null != oldAnc) {
                        //Revertir el nuevo codigo de sabre con el codigo anterior
                        String code = oldAnc.getType();
                        travelerAncillary.getAncillary().setRficSubcode(code);
                        travelerAncillary.getAncillary().setType(code);
                    }

                } else {
                    LOGGER.error("Trying to paid an already paid ancillary or zero cost ancillary");
                }
            }

            if (isThereProducts) {
                feesTypeList.add(feeType);
            }
        }

        Amount amount = getMiscAmount(purchaseOrder);
        collectMiscFeeRQ.setTotalCost(amount);

        List<PaymentMisc> paymentMiscList = collectMiscFeeRQ.getPayment();
        setPaymentMisc(purchaseOrder, paymentMiscList, authorizationResultList, pos);

        return collectMiscFeeRQ;
    }

    /**
     * @param travelerAncillary
     * @param bookedLeg
     * @param bookedTraveler
     * @param recordLocator
     * @param cartId
     * @param ticketingDocumentRS
     * @param serviceCallList
     * @throws Exception
     */
    private List<FlightMisc> getFlightDetails(
            TravelerAncillary travelerAncillary,
            BookedLeg bookedLeg,
            BookedTraveler bookedTraveler,
            String recordLocator, String cartId,
            GetTicketingDocumentRS ticketingDocumentRS,
            List<ServiceCall> serviceCallList,
            String pos
    ) throws Exception {
        List<FlightMisc> flightMiscList = new ArrayList<>();

        AncillaryGroupType ancillaryGroupType = AncillaryGroupType.getDefaultType(travelerAncillary.getAncillary().getGroupCode());

        switch (ancillaryGroupType) {
            case SA:
                if (null == travelerAncillary.getSegmentCodeAux() || travelerAncillary.getSegmentCodeAux().isEmpty()) {
                    LOGGER.error("The ancillary does not have a segmentCode needed to be associated with a segment flight");
                } else {
                    addFlightForSegment(
                            bookedLeg, bookedTraveler, travelerAncillary,
                            recordLocator, cartId, ticketingDocumentRS, serviceCallList, flightMiscList, pos
                    );
                }
                break;
            case BG:
                addFlightForLeg(
                        bookedLeg, bookedTraveler, recordLocator, cartId,
                        ticketingDocumentRS, serviceCallList, flightMiscList, pos
                );
                break;
            //case TS:
            //case _99:
            //case IE:
            default:
                if ("2".equals(travelerAncillary.getAncillary().getEmdType())) {
                    if ("P".equals(travelerAncillary.getAncillary().getSegmentIndicator())) {
                        addFlightForLeg(
                                bookedLeg, bookedTraveler, recordLocator, cartId,
                                ticketingDocumentRS, serviceCallList, flightMiscList, pos
                        );
                    } else if ("S".equals(travelerAncillary.getAncillary().getSegmentIndicator())) {
                        addFlightForSegment(
                                bookedLeg, bookedTraveler, travelerAncillary,
                                recordLocator, cartId, ticketingDocumentRS, serviceCallList, flightMiscList, pos
                        );
                    }

                }
                break;
        }

        return flightMiscList;
    }

    /**
     * @param bookedLeg
     * @param bookedTraveler
     * @param travelerAncillary
     * @param recordLocator
     * @param cartId
     * @param ticketingDocumentRS
     * @param serviceCallList
     * @param flightMiscList
     * @throws Exception
     */
    private void addFlightForSegment(
            BookedLeg bookedLeg, BookedTraveler bookedTraveler,
            TravelerAncillary travelerAncillary, String recordLocator,
            String cartId, GetTicketingDocumentRS ticketingDocumentRS,
            List<ServiceCall> serviceCallList, List<FlightMisc> flightMiscList,
            String pos
    ) throws Exception {
        boolean initialized = false;
        String flightNumber = "";
        String classOfService = "";
        String couponNumber = "";
        int segmentID = 0;

        for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {

            Segment segment = bookedSegment.getSegment();
            if (null != segment) {

                BaggageRoute baggageRoute = PurchaseOrderUtil.getBaggageRouteBySegment(
                        segment, bookedTraveler.getBaggageRouteList()
                );

                if (null != baggageRoute) {
                    classOfService = baggageRoute.getBookingClass();
                    segmentID = baggageRoute.getSegmentID();
                }

                boolean areSegmentCodesEquals = SegmentCodeUtil.compareSegmentCodeWithoutTime(
                        travelerAncillary.getSegmentCodeAux(), segment.getSegmentCode()
                );

                if (initialized || areSegmentCodesEquals) {

                    //it is suppose to enter in this condition the first time
                    if (areSegmentCodesEquals) {
                        initialized = true;
                        flightNumber = segment.getOperatingFlightCode();

                        couponNumber = getCouponNumber(
                                segment, bookedTraveler.getTicketNumber(),
                                recordLocator, cartId, ticketingDocumentRS,
                                serviceCallList, pos
                        );

                    } else if (!flightNumber.isEmpty() && !FilterPassengersUtil.compareFlightNumber(
                            flightNumber, segment.getOperatingFlightCode()
                    )) {
                        //break if flight number change, it means this segment must have other seat
                        //if flight number is the same this segment is also asociated with the seat
                        break;
                    }

                    FlightMisc flightMisc = new FlightMisc();
                    flightMiscList.add(flightMisc);

                    DocumentNumber documentNumber = new DocumentNumber();
                    documentNumber.setValue(bookedTraveler.getTicketNumber());
                    documentNumber.setCouponNumber(couponNumber);
                    flightMisc.setAssociatedTicketNumber(documentNumber);

                    flightMisc.setArrivalCity(segment.getArrivalAirport());
                    flightMisc.setDepartureCity(segment.getDepartureAirport());
                    flightMisc.setCarrierCode(segment.getOperatingCarrier());
                    flightMisc.setClassOfService(classOfService);

                    try {
                        GregorianCalendar c = new GregorianCalendar();
                        c.setTimeInMillis(TimeUtil.getTime(segment.getDepartureDateTime()));
                        XMLGregorianCalendar xmlGregorianCalendar;
                        xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
                        xmlGregorianCalendar.setMillisecond(DatatypeConstants.FIELD_UNDEFINED);
                        xmlGregorianCalendar.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
                        flightMisc.setDepartureDate(xmlGregorianCalendar);
                    } catch (ParseException | DatatypeConfigurationException ex) {
                        LOGGER.error("Error parsing departure datetime: " + ex.getMessage());
                    }

                    flightMisc.setFlightNumber(segment.getOperatingFlightCode());
                    flightMisc.setOperatingCarrierCode(segment.getOperatingCarrier());

                    try {
                        flightMisc.setSegmentNumber(new Integer(segment.getSegmentNumber()));
                    } catch (NumberFormatException ex) {
                        LOGGER.error("Error parsing segment number: " + ex.getMessage());
                    }

                    flightMisc.setSegmentID(segmentID);

                }
            } else {
                LOGGER.error("Itinerary is null, we can not associate a flight for that ancillary");
            }
        }
    }

    /**
     * @param bookedLeg
     * @param bookedTraveler
     * @param recordLocator
     * @param cartId
     * @param ticketingDocumentRS
     * @param serviceCallList
     * @param flightMiscList
     * @throws Exception
     */
    private void addFlightForLeg(
            BookedLeg bookedLeg, BookedTraveler bookedTraveler,
            String recordLocator, String cartId,
            GetTicketingDocumentRS ticketingDocumentRS,
            List<ServiceCall> serviceCallList, List<FlightMisc> flightMiscList,
            String pos
    ) throws Exception {
        List<BookedSegment> bookedSegmentList = bookedLeg.getSegments().getSegmentsOperatedBy(AirlineCodeType.AM);
        BaggageRoute baggageRoute;
        BookingClass bookingClass;
        String cabinClass;
        int segmentID;
        int segmentIdIndex = 1;
        GregorianCalendar c;
        XMLGregorianCalendar xmlGregorianCalendar;
        if (null != bookedSegmentList
                && !bookedSegmentList.isEmpty()) {

            String flightNumber = "-1"; //helpful data on purpose

            FlightMisc flightMisc = new FlightMisc();
            for (BookedSegment bookedSegment : bookedSegmentList) {
                Segment segment = bookedSegment.getSegment();

                if (null != segment) {

                    baggageRoute = PurchaseOrderUtil.getBaggageRouteBySegment(
                            segment, bookedTraveler.getBaggageRouteList()
                    );

                    if (null == baggageRoute) {
                        bookingClass = PurchaseOrderUtil.getBookingClassBySegment(
                                bookedSegment.getSegment(),
                                bookedTraveler.getBookingClasses()
                        );
                        if (null != bookingClass) {
                            cabinClass = bookingClass.getBookingClass();

                        } else {
                            cabinClass = "";
                        }

                        segmentID = segmentIdIndex;
                    } else {
                        cabinClass = baggageRoute.getBookingClass();
                        segmentID = baggageRoute.getSegmentID();
                    }

                    if (FilterPassengersUtil.compareFlightNumber(
                            flightNumber, bookedSegment.getSegment().getOperatingFlightCode()
                    )) {
                        flightMisc.setArrivalCity(segment.getArrivalAirport());
                    } else {
                        flightNumber = bookedSegment.getSegment().getOperatingFlightCode();

                        String couponNumber = getCouponNumber(
                                segment, bookedTraveler.getTicketNumber(),
                                recordLocator, cartId, ticketingDocumentRS,
                                serviceCallList, pos
                        );

                        flightMisc = new FlightMisc();
                        flightMiscList.add(flightMisc);

                        DocumentNumber documentNumber = new DocumentNumber();
                        documentNumber.setValue(bookedTraveler.getTicketNumber());
                        documentNumber.setCouponNumber(couponNumber);
                        flightMisc.setAssociatedTicketNumber(documentNumber);

                        flightMisc.setDepartureCity(segment.getDepartureAirport());
                        flightMisc.setCarrierCode(segment.getOperatingCarrier());
                        flightMisc.setClassOfService(cabinClass);

                        try {
                            c = new GregorianCalendar();
                            c.setTimeInMillis(TimeUtil.getTime(segment.getDepartureDateTime()));
                            xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
                            xmlGregorianCalendar.setMillisecond(DatatypeConstants.FIELD_UNDEFINED);
                            xmlGregorianCalendar.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
                            flightMisc.setDepartureDate(xmlGregorianCalendar);
                        } catch (ParseException | DatatypeConfigurationException ex) {
                            LOGGER.error("Error parsing departure datetime: " + ex.getMessage());
                        }

                        flightMisc.setFlightNumber(segment.getOperatingFlightCode());
                        flightMisc.setOperatingCarrierCode(segment.getOperatingCarrier());

                        try {
                            flightMisc.setSegmentNumber(new Integer(segment.getSegmentNumber()));
                        } catch (NumberFormatException ex) {
                            LOGGER.error("Error parsing segment number: " + ex.getMessage());
                        }

                        flightMisc.setSegmentID(segmentID);
                        flightMisc.setArrivalCity(segment.getArrivalAirport());

                        segmentIdIndex++;
                    }
                } else {
                    LOGGER.error("Itinerary is null, we can not associate a flight for that ancillary");
                }
            }
        }
    }

    /**
     * @param bookedTravelerList
     * @param bookedLeg
     * @param purchaseOrderRQ
     * @param authorizationResultList
     * @param ticketingDocumentRS
     * @param recordLocator
     * @param cartId
     * @param serviceCallList
     * @return
     * @throws Exception
     */
    public CollectMiscFeeRQ getCollectMiscFeeRQForExtraWeight(
            List<BookedTraveler> bookedTravelerList,
            BookedLeg bookedLeg,
            PurchaseOrderRQ purchaseOrderRQ,
            List<AuthorizationResult> authorizationResultList,
            GetTicketingDocumentRS ticketingDocumentRS,
            String recordLocator, String cartId,
            List<ServiceCall> serviceCallList
    ) throws Exception {

        PurchaseOrder purchaseOrder = purchaseOrderRQ.getPurchaseOrder();
        String store = purchaseOrderRQ.getStore();
        String pos = purchaseOrderRQ.getPos();

        CollectMiscFeeRQ collectMiscFeeRQ = new CollectMiscFeeRQ();
        collectMiscFeeRQ.setVersion(VERSION);
        collectMiscFeeRQ.setDetailLevel(RequestedDetailLevel.FULL);

        GlobalConfiguration config = setUpConfigFactory.getInstance();

        AuthorizationPaymentConfig authorizationPaymentConfig = config.getAuthorizationPaymentConfig(store, pos);

        LocationPOS locationPOS = new LocationPOS();
        locationPOS.setValue(authorizationPaymentConfig.getCityCode());
        locationPOS.setCountry(authorizationPaymentConfig.getIsoCountry());
        locationPOS.setNumber(authorizationPaymentConfig.getStationNumber());

        POSMisc posMisc = new POSMisc();
        posMisc.setCompany(COMPANY);
        posMisc.setAAA(locationPOS);
        posMisc.setDutyCode(config.getDutyCode());
        posMisc.setLniata(config.getTicketLNIATA());
        posMisc.setSine(SINE);
        collectMiscFeeRQ.setAgentPOS(posMisc);
        /*End config*/

        CollectMiscTransInfo collectMiscTransInfo = new CollectMiscTransInfo();
        collectMiscTransInfo.setPrinterLniata(config.getTicketLNIATA());
        collectMiscTransInfo.setPrinterStock(config.getPrinterStock());
        collectMiscTransInfo.setPrintTaxCpn(false);
        collectMiscFeeRQ.setParameters(collectMiscTransInfo);

        MiscTransInfo miscTransInfo = new MiscTransInfo();
        miscTransInfo.setCode(TransactionCodeMisc.EMD);
        collectMiscFeeRQ.setTransaction(miscTransInfo);

        List<FeesType> feesTypeList = collectMiscFeeRQ.getFees();

        for (BookedTraveler bookedTraveler : bookedTravelerList) {
            FeesType feeType = new FeesType();
            FeesLinked feesLinked = new FeesLinked();
            feeType.setLinked(feesLinked);

            CustomerMisc customerMisc = new CustomerMisc();
            customerMisc.setFirstName(bookedTraveler.getFirstName());
            customerMisc.setLastName(bookedTraveler.getLastName());
            CustomerDetailsMisc customerDetailsMisc = new CustomerDetailsMisc();
            customerDetailsMisc.setPnrLocator(recordLocator);
            customerDetailsMisc.setNameRefNumber(new BigDecimal(bookedTraveler.getNameRefNumber()));
            customerMisc.setCustomerDetails(customerDetailsMisc);
            feesLinked.setCustomer(customerMisc);
            List<FeeLinked> feeLinkedList = feesLinked.getFee();

            boolean isThereProducts = false;

            List<ExtraWeightAncillary> allUnpaidAncillaries = PurchaseOrderUtil.getExtraWeightUnpaidAncillariesOnReservation(bookedTraveler.getAncillaries().getCollection());

            for (ExtraWeightAncillary extraWeightAncillary : allUnpaidAncillaries) {

                if (ActionCodeType.HD.toString().equalsIgnoreCase(extraWeightAncillary.getActionCode())
                        || -1 == BigDecimal.ZERO.compareTo(extraWeightAncillary.getAncillary().getCurrency().getTotal())) {

                	String ancillaryCode = null;
                	if ( extraWeightAncillary.getAncillary().getRealRficSubcode() == null || extraWeightAncillary.getAncillary().getRealRficSubcode().trim().isEmpty() ) {
                		ancillaryCode =  extraWeightAncillary.getAncillary().getRficSubcode();
                	} else {
                		ancillaryCode =  extraWeightAncillary.getAncillary().getRealRficSubcode();
                	}

                	AncillaryUtil.convertCodeAncillary_2_0( ancillaryCode, bookedLeg.getMarketFlight() );

                    String currencyCode = extraWeightAncillary.getAncillary().getCurrency().getCurrencyCode();

                    isThereProducts = true;
                    FeeLinked feeLinked = new FeeLinked();
                    feeLinkedList.add(feeLinked);

                    FeeDetailsMisc feeDetailsMisc = new FeeDetailsMisc();

                    BigDecimal qty = CurrencyUtil.getAmount(
                            new BigDecimal(extraWeightAncillary.getQuantity()),
                            currencyCode
                    );

                    Amount base = getAmountBase(extraWeightAncillary.getAncillary().getCurrency());
                    base.setValue(CurrencyUtil.getAmount(base.getValue().multiply(qty), currencyCode));
                    feeDetailsMisc.setBase(base);
                    Amount totalTaxes = getAmountTaxes(extraWeightAncillary.getAncillary().getCurrency());
                    totalTaxes.setValue(CurrencyUtil.getAmount(totalTaxes.getValue().multiply(qty), currencyCode));
                    feeDetailsMisc.setTotalTax(totalTaxes);
                    Amount total = getAmountTotal(extraWeightAncillary.getAncillary().getCurrency());
                    total.setValue(CurrencyUtil.getAmount(total.getValue().multiply(qty), currencyCode));
                    feeDetailsMisc.setTotal(total);
                    //feeDetailsMisc.setEquiv(total);
                    feeDetailsMisc.setCode(extraWeightAncillary.getAncillary().getRficSubcode());
                    feeDetailsMisc.setQuantity(extraWeightAncillary.getQuantity());

                    feeLinked.setFeeDetails(feeDetailsMisc);

                    List<TaxMisc> taxes = feeLinked.getTax();

                    Map<String, BigDecimal> taxesMap = extraWeightAncillary.getAncillary().getCurrency().getTaxDetails();
                    for (Map.Entry<String, BigDecimal> entry : taxesMap.entrySet()) {
                        Amount amount = getAmountTotal(currencyCode, entry.getValue());

                        TaxMisc taxMisc = new TaxMisc();
                        taxMisc.setAmount(amount);
                        taxMisc.setCode(entry.getKey());
                        taxMisc.setExempt(Boolean.FALSE);

                        taxes.add(taxMisc);
                    }

                    FeeLinked.OptionalService optionalService = new FeeLinked.OptionalService();
                    optionalService.setRFIC(extraWeightAncillary.getAncillary().getRficCode());

                    BookingIndicator bookingIndicator;
                    try {
                        bookingIndicator = BookingIndicator.fromValue(extraWeightAncillary.getAncillary().getBookingIndicator().toUpperCase()
                        );
                    } catch (Exception ex) {
                        bookingIndicator = BookingIndicator.SSR;
                    }

                    optionalService.setBookingIndicator(bookingIndicator);
                    optionalService.setGroup(extraWeightAncillary.getAncillary().getGroupCode());
                    optionalService.setName(extraWeightAncillary.getAncillary().getCommercialName());

                    SegmentIndicator segmentIndicator;
                    try {
                        segmentIndicator = SegmentIndicator.fromValue(extraWeightAncillary.getAncillary().getSegmentIndicator().toUpperCase()
                        );
                    } catch (Exception ex) {
                        segmentIndicator = SegmentIndicator.P;
                    }

                    optionalService.setSegmentIndicator(segmentIndicator);
                    optionalService.setSubCode(extraWeightAncillary.getAncillary().getRficSubcode());
                    optionalService.setEmdType(EmdType.ASSOCIATED);
                    optionalService.setAirExtraItemNumber(new BigInteger(extraWeightAncillary.getAncillaryId()));
                    optionalService.setVendor(extraWeightAncillary.getAncillary().getVendor());
                    optionalService.setOwningCarrierCode(extraWeightAncillary.getAncillary().getOwningCarrierCode());
                    optionalService.setSsrCode(extraWeightAncillary.getAncillary().getSsrCode());

                    feeLinked.setOptionalService(optionalService);

                    List<FlightMisc> flightMiscList = feeLinked.getAssociatedFlight();

                    List<BookedSegment> bookedSegmentList = bookedLeg.getSegments().getSegmentsOperatedBy(AirlineCodeType.AM);

                    BaggageRoute baggageRoute;
                    BookingClass bookingClass;
                    String cabinClass;
                    int segmentID;
                    int segmentIdIndex = 1;
                    GregorianCalendar c;
                    XMLGregorianCalendar xmlGregorianCalendar;
                    if (null != bookedSegmentList
                            && !bookedSegmentList.isEmpty()) {

                        String flightNumber = "-1"; //helpful data op purpose

                        FlightMisc flightMisc = new FlightMisc();
                        for (BookedSegment bookedSegment : bookedSegmentList) {
                            Segment segment = bookedSegment.getSegment();

                            if (null != segment) {

                                baggageRoute = PurchaseOrderUtil.getBaggageRouteBySegment(
                                        segment, bookedTraveler.getBaggageRouteList()
                                );

                                if (null == baggageRoute) {
                                    bookingClass = PurchaseOrderUtil.getBookingClassBySegment(
                                            bookedSegment.getSegment(),
                                            bookedTraveler.getBookingClasses()
                                    );
                                    if (null != bookingClass) {
                                        cabinClass = bookingClass.getBookingClass();

                                    } else {
                                        cabinClass = "";
                                    }

                                    segmentID = segmentIdIndex;
                                } else {
                                    cabinClass = baggageRoute.getBookingClass();
                                    segmentID = baggageRoute.getSegmentID();
                                }

                                if (FilterPassengersUtil.compareFlightNumber(
                                        flightNumber, bookedSegment.getSegment().getOperatingFlightCode()
                                )) {
                                    flightMisc.setArrivalCity(segment.getArrivalAirport());
                                } else {
                                    flightNumber = bookedSegment.getSegment().getOperatingFlightCode();

                                    String couponNumber = getCouponNumber(
                                            segment, bookedTraveler.getTicketNumber(),
                                            recordLocator, cartId, ticketingDocumentRS,
                                            serviceCallList, pos
                                    );

                                    flightMisc = new FlightMisc();
                                    flightMiscList.add(flightMisc);

                                    DocumentNumber documentNumber = new DocumentNumber();
                                    documentNumber.setValue(bookedTraveler.getTicketNumber());
                                    documentNumber.setCouponNumber(couponNumber);
                                    flightMisc.setAssociatedTicketNumber(documentNumber);

                                    flightMisc.setDepartureCity(segment.getDepartureAirport());
                                    flightMisc.setCarrierCode(segment.getOperatingCarrier());
                                    flightMisc.setClassOfService(cabinClass);

                                    try {
                                        c = new GregorianCalendar();
                                        c.setTimeInMillis(TimeUtil.getTime(segment.getDepartureDateTime()));
                                        xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
                                        xmlGregorianCalendar.setMillisecond(DatatypeConstants.FIELD_UNDEFINED);
                                        xmlGregorianCalendar.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
                                        flightMisc.setDepartureDate(xmlGregorianCalendar);
                                    } catch (ParseException | DatatypeConfigurationException ex) {
                                        LOGGER.error("Error parsing departure datetime: " + ex.getMessage());
                                    }

                                    flightMisc.setFlightNumber(segment.getOperatingFlightCode());
                                    flightMisc.setOperatingCarrierCode(segment.getOperatingCarrier());

                                    try {
                                        flightMisc.setSegmentNumber(new Integer(segment.getSegmentNumber()));
                                    } catch (NumberFormatException ex) {
                                        LOGGER.error("Error parsing segment number: " + ex.getMessage());
                                    }

                                    flightMisc.setSegmentID(segmentID);
                                    flightMisc.setArrivalCity(segment.getArrivalAirport());

                                    segmentIdIndex++;
                                }
                            } else {
                                LOGGER.error("Itinerary is null, we can not associate a flight for that ancillary");
                            }
                        }
                    }

                } else {
                    LOGGER.error("Trying to paid an already paid ancillary");
                }
            }

            if (isThereProducts) {
                feesTypeList.add(feeType);
            }
        }

        Amount amount = getMiscAmount(purchaseOrder);
        collectMiscFeeRQ.setTotalCost(amount);

        List<PaymentMisc> paymentMiscList = collectMiscFeeRQ.getPayment();
        setPaymentMisc(purchaseOrder, paymentMiscList, authorizationResultList, pos);

        return collectMiscFeeRQ;
    }

    private Amount getMiscAmount(PurchaseOrder purchaseOrder) {
        FormsOfPaymentWrapper formsOfPaymentWrapper;
        formsOfPaymentWrapper = new FormsOfPaymentWrapper(
                purchaseOrder.getPaymentInfo()
        );
        CurrencyValue totalInCart = purchaseOrder.getPaymentAmount();
        BigDecimal total = BigDecimal.ZERO;
        String currencyCode = totalInCart.getCurrencyCode();
        if (!formsOfPaymentWrapper.getCreditCardPaymentRequestList().isEmpty()) {
            currencyCode = CurrencyUtil.getCurrencyCodeFromCreditCardPayment(
                    formsOfPaymentWrapper.getCreditCardPaymentRequestList()
            );

            total = CurrencyUtil.getTotalCreditCardPayment(
                    formsOfPaymentWrapper.getCreditCardPaymentRequestList(),
                    currencyCode
            );
        } else if (!formsOfPaymentWrapper.getRemotePaymentRequestList().isEmpty()) {
            currencyCode = CurrencyUtil.getCurrencyCodeFromRemotePayment(
                    formsOfPaymentWrapper.getRemotePaymentRequestList()
            );

            total = CurrencyUtil.getTotalRemotePayment(
                    formsOfPaymentWrapper.getRemotePaymentRequestList(),
                    currencyCode
            );
        } else if (!formsOfPaymentWrapper.getPaypalPaymentRequestList().isEmpty()) {
            currencyCode = CurrencyUtil.getCurrencyCodeFromPaypalPayment(
                    formsOfPaymentWrapper.getPaypalPaymentRequestList()
            );

            total = CurrencyUtil.getTotalPaypalPayment(
                    formsOfPaymentWrapper.getPaypalPaymentRequestList(),
                    currencyCode
            );
        } else if (!formsOfPaymentWrapper.getUatpPaymentRequestList().isEmpty()
                || !formsOfPaymentWrapper.getMcoPaymentRequestList().isEmpty()) {

            total = totalInCart.getTotal();
            currencyCode = totalInCart.getCurrencyCode();
        }

        Amount amount = new Amount();
        amount.setValue(CurrencyUtil.getAmount(total, currencyCode));
        amount.setCurrencyCode(currencyCode);
        return amount;
    }

    /**
     * @param collectMiscFeeRQ
     * @param purchaseOrderRQ
     * @param title
     * @param checkForRetry
     * @param serviceCallList
     * @return
     * @throws RetryCheckinException
     * @throws GenericException
     * @throws Exception
     */
    public CollectMiscFeeRS issueEmdCodes(
            CollectMiscFeeRQ collectMiscFeeRQ,
            PurchaseOrderRQ purchaseOrderRQ,
            String title, boolean checkForRetry,
            List<ServiceCall> serviceCallList
    ) throws RetryCheckinException, GenericException, Exception {
        return issueEmdCodes(
                null, collectMiscFeeRQ, purchaseOrderRQ,
                title, true, true, checkForRetry, serviceCallList
        );
    }

    /**
     * @param sabreSession
     * @param collectMiscFeeRQ
     * @param purchaseOrderRQ
     * @param title
     * @param closeSession
     * @param setParameters
     * @param checkForRetry
     * @param serviceCallList
     * @return
     * @throws RetryCheckinException
     * @throws GenericException
     * @throws Exception
     */
    public CollectMiscFeeRS issueEmdCodes(
            SabreSession sabreSession, CollectMiscFeeRQ collectMiscFeeRQ,
            PurchaseOrderRQ purchaseOrderRQ, String title, boolean closeSession,
            boolean setParameters, boolean checkForRetry,
            List<ServiceCall> serviceCallList
    ) throws RetryCheckinException, GenericException, Exception {

        if (null == collectMiscFeeRQ || null == collectMiscFeeRQ.getFees() || collectMiscFeeRQ.getFees().isEmpty()) {
            return new CollectMiscFeeRS();
        }

        String pos = purchaseOrderRQ.getPos();
        String store = purchaseOrderRQ.getStore();
        String recordLocator = purchaseOrderRQ.getRecordLocator();
        String cartId = purchaseOrderRQ.getPurchaseOrder().getCartId();
        GlobalConfiguration config = setUpConfigFactory.getInstance();

        AuthorizationPaymentConfig authorizationPaymentConfig = config.getAuthorizationPaymentConfig(store, pos);

        String dutyCodeCommand = config.getDutyCodeCommand();
        String dutyCode = config.getDutyCode();
        String cityCodeCommand = config.getCityCodeCommand();
        String cityCode = authorizationPaymentConfig.getCityCode();
        String printerCodeCommand = config.getTicketPrinterCommand();
        String printerCode = config.getTicketLNIATA();
        String stationNumber = authorizationPaymentConfig.getStationNumber();
        /*End config*/

        CollectMiscFeeRS collectMiscFeeRS = null;
        try {

            if (null == sabreSession) {
                collectMiscFeeRS = miscService.collectMiscFee(
                        collectMiscFeeRQ, dutyCode, dutyCodeCommand, cityCode,
                        cityCodeCommand, printerCode, stationNumber, printerCodeCommand,
                        serviceCallList
                );
            } else {
                collectMiscFeeRS = miscService.collectMiscFee(
                        sabreSession, collectMiscFeeRQ,
                        dutyCode, dutyCodeCommand, cityCode,
                        cityCodeCommand, printerCode, stationNumber,
                        printerCodeCommand, closeSession, setParameters,
                        serviceCallList
                );
            }

            log(recordLocator, cartId, title, collectMiscFeeRQ, collectMiscFeeRS);

            checkEmdErrors(collectMiscFeeRS, checkForRetry, pos);

            //log on success
            logOnDatabaseService.logEmdOnSuccess(
                    purchaseOrderRQ, collectMiscFeeRQ, collectMiscFeeRS
            );

            return collectMiscFeeRS;

        }catch (SabreEmptyResponseException ser) {
            LOGGER.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110302).toString(), ser.fillInStackTrace());
            logOnDatabaseService.logEmdOnError(
                    purchaseOrderRQ, collectMiscFeeRQ, collectMiscFeeRS, ser
            );
            logError(recordLocator, cartId, title, collectMiscFeeRQ, ser.getMessage());
            throw ser;
        }catch (SabreLayerUnavailableException se){
            logOnDatabaseService.logEmdOnError(
                    purchaseOrderRQ, collectMiscFeeRQ, collectMiscFeeRS, se
            );
            logError(recordLocator, cartId, title, collectMiscFeeRQ, se.getMessage());
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(purchaseOrderRQ.getPos())) {
                LOGGER.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_22110301,"EMDService.java:1700", se.getMessage());
                throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getCode(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getActionable(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getMessage(),
                        Response.Status.INTERNAL_SERVER_ERROR);
            } else {
                LOGGER.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_12110301,"EMDService.java:1706", se.getMessage());
                throw se;
            }
        } catch (RetryCheckinException ex) {
            ex.setWasLogged(true);
            //log on error
            logOnDatabaseService.logEmdOnError(
                    purchaseOrderRQ, collectMiscFeeRQ, collectMiscFeeRS, ex
            );
            logError(recordLocator, cartId, title, collectMiscFeeRQ, ex.getMessage());
            throw ex;
        } catch (GenericException ex) {
            ex.setWasLogged(true);
            //log on error
            logOnDatabaseService.logEmdOnError(
                    purchaseOrderRQ, collectMiscFeeRQ, collectMiscFeeRS, ex
            );
            logError(recordLocator, cartId, title, collectMiscFeeRQ, ex.getMessage());
            throw ex;
        } catch (Exception ex) {
            //log on error
            logOnDatabaseService.logEmdOnError(
                    purchaseOrderRQ, collectMiscFeeRQ, collectMiscFeeRS, ex
            );
            logError(recordLocator, cartId, title, collectMiscFeeRQ, ex.getMessage());
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNKNOWN_CODE, ex.getMessage());
        }
    }

    /**
     * @param collectMiscFeeRS
     * @param checkForRetry
     * @param pos
     * @throws RetryCheckinException
     * @throws GenericException
     */
    private void checkEmdErrors(
            CollectMiscFeeRS collectMiscFeeRS, boolean checkForRetry, String pos
    ) throws RetryCheckinException, GenericException {
        if (ErrorOrSuccessCode.SUCCESS != collectMiscFeeRS.getHeader().getResults().getStatus()) {

            String msg = collectMiscFeeRS.getHeader().getResults().getErrorSource().getValue();

            for (SystemSpecificResults systemSpecificResults : collectMiscFeeRS.getHeader().getResults().getSystemSpecificResults()) {
                msg = msg + " , code: " + systemSpecificResults.getErrorMessage().getCode() + ", value: " + systemSpecificResults.getErrorMessage().getValue();
            }

            //TKT PRT NOT ASSIGNED
            if (StringUtils.containsIgnoreCase(msg, ErrorType.TKT_PRINTER_NOT_ASSIGNED.getSearchableCode())) {
                if (checkForRetry) {
                    throw new RetryCheckinException(Response.Status.BAD_REQUEST, ErrorType.TKT_PRINTER_NOT_ASSIGNED, msg, PosType.getType(pos));
                } else {
                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.TKT_PRINTER_NOT_ASSIGNED);
                }
            }

            //INVALID COUPON NUMBER
            if (StringUtils.containsIgnoreCase(msg, ErrorType.INVALID_COUPON_NUMBER.getSearchableCode())) {
                if (checkForRetry) {
                    throw new RetryCheckinException(Response.Status.BAD_REQUEST, ErrorType.INVALID_COUPON_NUMBER, msg, PosType.getType(pos));
                } else {
                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.INVALID_COUPON_NUMBER);
                }
            }

            //INVALID COUPON STATUS
            if (StringUtils.containsIgnoreCase(msg, ErrorType.INVALID_COUPON_STATUS.getSearchableCode())) {
                if (checkForRetry) {
                    throw new RetryCheckinException(Response.Status.BAD_REQUEST, ErrorType.INVALID_COUPON_STATUS, msg, PosType.getType(pos));
                } else {
                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.INVALID_COUPON_STATUS);
                }
            }

            if (StringUtils.containsIgnoreCase(msg, ErrorType.INVALID_PSS_RESPONSE_EMPTY_MESSAGE.getSearchableCode())) {
                if (checkForRetry) {
                    throw new RetryCheckinException(Response.Status.BAD_REQUEST, ErrorType.INVALID_PSS_RESPONSE_EMPTY_MESSAGE, msg, PosType.getType(pos));
                } else {
                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.INVALID_PSS_RESPONSE_EMPTY_MESSAGE);
                }
            }

            //TPF | code: 606, value: PROCESSING ERROR - NO UNFUFLFILLED AE ITEM
            if (StringUtils.containsIgnoreCase(msg, ErrorType.NO_UNFULFILLED_AE_ITEM.getSearchableCode())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.NO_UNFULFILLED_AE_ITEM);
            }

            //TPF | code: 606, value: PROCESSING ERROR - AE ITEM NOT FOUND
            if (StringUtils.containsIgnoreCase(msg, ErrorType.PROCESSING_ERROR_AE_ITEM_NOT_FOUND.getSearchableCode())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PROCESSING_ERROR_AE_ITEM_NOT_FOUND);
            }

            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNKNOWN_CODE_ABORT, msg);
        }
    }

    /**
     * @param recordLocator
     * @param cartId
     * @param title
     * @param collectMiscFeeRQ
     * @param collectMiscFeeRS
     */
    public void log(String recordLocator, String cartId, String title, CollectMiscFeeRQ collectMiscFeeRQ, CollectMiscFeeRS collectMiscFeeRS) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("\n").append("MISC Service: ").append(title).append("\n");
            sb.append("recordLocator: ").append(recordLocator).append("\n");
            sb.append("cartId: ").append(cartId).append("\n");
            sb.append("RQ: ").append(miscService.toString(collectMiscFeeRQ)).append("\n");
            sb.append("RS: ").append(miscService.toString(collectMiscFeeRS)).append("\n");

            LOGGER.info(sb.toString());
        } catch (Exception ex) {
            //
        }
    }

    /**
     * @param recordLocator
     * @param cartId
     * @param title
     */
    public void log(String recordLocator, String cartId, String title) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("\n").append("MISC Service: ").append(title).append("\n");
            sb.append("recordLocator: ").append(recordLocator).append("\n");
            sb.append("cartId: ").append(cartId).append("\n");

            LOGGER.info(recordLocator, cartId, sb.toString());
        } catch (Exception ex) {
            //
        }
    }

    /**
     * @param recordLocator
     * @param cartId
     * @param title
     * @param collectMiscFeeRQ
     * @param message
     */
    public void logError(String recordLocator, String cartId, String title, CollectMiscFeeRQ collectMiscFeeRQ, String message) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("\n").append("MISC Service: ").append(title).append("\n");
            sb.append("recordLocator: ").append(recordLocator).append("\n");
            sb.append("cartId: ").append(cartId).append("\n");
            sb.append("RQ: ").append(miscService.toString(collectMiscFeeRQ)).append("\n");
            sb.append("RS: ").append(message).append("\n");

            LOGGER.error(sb.toString());
        } catch (Exception ex) {
            //
        }
    }

    /**
     * @param recordLocator
     * @param cartId
     * @param title
     */
    public void logError(String recordLocator, String cartId, String title) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("\n").append("MISC Service: ").append(title).append("\n");
            sb.append("recordLocator: ").append(recordLocator).append("\n");
            sb.append("cartId: ").append(cartId).append("\n");

            LOGGER.error(sb.toString());
        } catch (Exception ex) {
            //
        }
    }

    /**
     * @param recordLocator
     * @param cartId
     * @param title
     */
    public void logDebug(String recordLocator, String cartId, String title) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("\n").append("MISC Service: ").append(title).append("\n");
            sb.append("recordLocator: ").append(recordLocator).append("\n");
            sb.append("cartId: ").append(cartId).append("\n");

            LOGGER.debug(sb.toString());
        } catch (Exception ex) {
            //
        }
    }

    /**
     * @param currencyValue
     * @return
     */
    public static Amount getAmountTaxes(CurrencyValue currencyValue) {
        Amount amount = new Amount();
        amount.setCurrencyCode(currencyValue.getCurrencyCode());
        amount.setValue(CurrencyUtil.getAmount(currencyValue.getTotalTax(), currencyValue.getCurrencyCode()));
        return amount;
    }

    /**
     * @param currencyValue
     * @return
     */
    public static Amount getAmountBase(CurrencyValue currencyValue) {
        Amount amount = new Amount();
        amount.setCurrencyCode(currencyValue.getCurrencyCode());
        amount.setValue(CurrencyUtil.getAmount(currencyValue.getBase(), currencyValue.getCurrencyCode()));
        return amount;
    }

    /**
     * @param currencyValue
     * @return
     */
    public static Amount getAmountTotal(CurrencyValue currencyValue) {
        Amount amount = new Amount();
        amount.setCurrencyCode(currencyValue.getCurrencyCode());
        amount.setValue(CurrencyUtil.getAmount(currencyValue.getTotal(), currencyValue.getCurrencyCode()));
        return amount;
    }

    /**
     * @param currencyValue
     * @param amountValue
     * @return
     */
    public static Amount getAmountTotal(String currencyValue, BigDecimal amountValue) {
        Amount amount = new Amount();
        amount.setCurrencyCode(currencyValue);
        amount.setValue(CurrencyUtil.getAmount(amountValue, currencyValue));
        return amount;
    }

    /**
     * @return the setUpConfigFactory
     */
    public CheckInSetUpConfigFactoryWrapper getSetUpConfigFactory() {
        if (null == setUpConfigFactory) {
            setUpConfigFactory = new CheckInSetUpConfigFactoryWrapper();
        }
        return setUpConfigFactory;
    }

    /**
     * @param setUpConfigFactory the setUpConfigFactory to set
     */
    public void setSetUpConfigFactory(CheckInSetUpConfigFactoryWrapper setUpConfigFactory) {
        this.setUpConfigFactory = setUpConfigFactory;
    }

}
