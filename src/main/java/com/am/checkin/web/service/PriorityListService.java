package com.am.checkin.web.service;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.loggingutils.LogUtil;
import com.aeromexico.commons.model.*;
import com.aeromexico.commons.model.rq.PassengersListRQ;
import com.aeromexico.commons.web.types.AirlineCodeType;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.types.PriorityGroup;
import com.aeromexico.dao.services.PriorityCodesDaoCache;
import com.aeromexico.sabre.api.session.common.ServiceCall;
import com.google.gson.Gson;
import com.sabre.services.acs.bso.flightdetail.v3.ACSFlightDetailRSACS;
import com.sabre.services.acs.bso.passengerlist.v3.ACSPassengerListRSACS;
import com.sabre.services.acs.bso.passengerlist.v3.PassengerInfoACS;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author adrian
 */
@Named
@ApplicationScoped
public class PriorityListService {

    private static final Logger LOG = LoggerFactory.getLogger(PurchaseOrderService.class);

    private static final Gson GSON = new Gson();

    private static final String SERVICE = "Priority List Service";

    @Inject
    private FlightDetailsService flightDetailsService;

    @Inject
    private PassengersListService passengersListService;

    @Inject
    private PriorityCodesDaoCache priorityCodesDaoCache;

    public StandbyList getNewStandbyList(PassengersListRQ passengersListRQ) throws Exception {

        String pos = passengersListRQ.getPos();
        String store = passengersListRQ.getStore();
        String language = passengersListRQ.getLanguage();

        String departureAirport;
        String arrivalAirport;
        String operatingCarrier;
        String operatingFlightCode;
        String departureDate;
        String[] displayCodes = {"PALL"};
        String legCode;

        MultivaluedMap<String, Object> queryParams = new MultivaluedMapImpl<>();

        if (null != passengersListRQ.getLegCode()) {
            String[] parts = passengersListRQ.getLegCode().split("_");
            if (parts.length < 4) {
                GenericException ex = new GenericException(Response.Status.BAD_REQUEST, ErrorType.LEGCODE_NOT_FOUND_IN_THE_PNR, "Invalid leg code");
                throw ex;
            }

            departureAirport = parts[0];
            arrivalAirport = null;
            operatingCarrier = parts[1];
            operatingFlightCode = parts[2];
            departureDate = parts[3];
        } else {
            departureAirport = passengersListRQ.getDepartureAirport();
            arrivalAirport = passengersListRQ.getArrivalAirport();
            operatingCarrier = passengersListRQ.getOperatingCarrier();
            operatingFlightCode = passengersListRQ.getOperatingFlightCode();
            departureDate = passengersListRQ.getDepartureDate();
        }

        StandbyList standbyList = new StandbyList();

        if (!AirlineCodeType.AM.getCode().equalsIgnoreCase(operatingCarrier)) {
            return standbyList;
        }

        queryParams.add("departureAirport", departureAirport);
        if (null != arrivalAirport) {
            queryParams.add("arrivalAirport", arrivalAirport);
        }
        queryParams.add("operatingCarrier", operatingCarrier);
        queryParams.add("operatingFlightCode", operatingFlightCode);
        queryParams.add("departureDate", departureDate);
        queryParams.add("showStaff", true);
        queryParams.add("showFullNames", false);

        try {

            PassengerListRS passengerListRS = passengersListService.getNewPassengerList("/passengers/standByList", queryParams);

            standbyList.setPassengers(passengerListRS.getPassengers());
            standbyList.setItineraryInfo(passengerListRS.getItineraryInfo());
            standbyList.setSeatsAvailability(passengerListRS.getSeatsAvailability());

        } catch (Exception ex) {
            ExtraInfo extraInfo = new ExtraInfo();
            extraInfo.setDebugging_stuff_here(LogUtil.getTrace(ex));
            Warning warning = new Warning();
            warning.setMsg(ex.getMessage());
            warning.setExtraInfo(extraInfo);
            standbyList.getWarnings().addToList(warning);
        }

        return standbyList;
    }


    public UpgradeList getNewUpgradeList(PassengersListRQ passengersListRQ) throws Exception {

        String pos = passengersListRQ.getPos();
        String store = passengersListRQ.getStore();
        String language = passengersListRQ.getLanguage();

        String departureAirport;
        String arrivalAirport;
        String operatingCarrier;
        String operatingFlightCode;
        String departureDate;
        String[] displayCodes = {"PALL"};
        String legCode;

        MultivaluedMap<String, Object> queryParams = new MultivaluedMapImpl<>();

        if (null != passengersListRQ.getLegCode()) {
            String[] parts = passengersListRQ.getLegCode().split("_");
            if (parts.length < 4) {
                GenericException ex = new GenericException(Response.Status.BAD_REQUEST, ErrorType.LEGCODE_NOT_FOUND_IN_THE_PNR, "Invalid leg code");
                throw ex;
            }

            departureAirport = parts[0];
            arrivalAirport = null;
            operatingCarrier = parts[1];
            operatingFlightCode = parts[2];
            departureDate = parts[3];
        } else {
            departureAirport = passengersListRQ.getDepartureAirport();
            arrivalAirport = passengersListRQ.getArrivalAirport();
            operatingCarrier = passengersListRQ.getOperatingCarrier();
            operatingFlightCode = passengersListRQ.getOperatingFlightCode();
            departureDate = passengersListRQ.getDepartureDate();
        }

        UpgradeList upgradeList = new UpgradeList();

        if (!AirlineCodeType.AM.getCode().equalsIgnoreCase(operatingCarrier)) {
            return upgradeList;
        }

        queryParams.add("departureAirport", departureAirport);
        if (null != arrivalAirport) {
            queryParams.add("arrivalAirport", arrivalAirport);
        }
        queryParams.add("operatingCarrier", operatingCarrier);
        queryParams.add("operatingFlightCode", operatingFlightCode);
        queryParams.add("departureDate", departureDate);
        queryParams.add("showStaff", true);
        queryParams.add("showFullNames", false);

        try {

            PassengerListRS passengerListRS = passengersListService.getNewPassengerList("/passengers/upgradeList", queryParams);

            upgradeList.setPassengers(passengerListRS.getPassengers());
            upgradeList.setItineraryInfo(passengerListRS.getItineraryInfo());
            upgradeList.setSeatsAvailability(passengerListRS.getSeatsAvailability());

        } catch (Exception ex) {
            ExtraInfo extraInfo = new ExtraInfo();
            extraInfo.setDebugging_stuff_here(LogUtil.getTrace(ex));
            Warning warning = new Warning();
            warning.setMsg(ex.getMessage());
            warning.setExtraInfo(extraInfo);
            upgradeList.getWarnings().addToList(warning);
        }

        return upgradeList;
    }


    @Deprecated
    public StandbyList getStandbyList(
            PassengersListRQ passengersListRQ, List<ServiceCall> serviceCallList
    ) throws Exception {

        String pos = passengersListRQ.getPos();
        String store = passengersListRQ.getStore();
        String language = passengersListRQ.getLanguage();

        String departureAirport;
        String arrivalAirport;
        String operatingCarrier;
        String operatingFlightCode;
        String departureDate;
        String[] displayCodes = {"PALL"};
        String legCode;

        if (null != passengersListRQ.getLegCode()) {

            legCode = passengersListRQ.getLegCode();

            String[] parts = passengersListRQ.getLegCode().split("_");

            if (parts.length < 4) {
                GenericException ex = new GenericException(Response.Status.BAD_REQUEST, ErrorType.LEGCODE_NOT_FOUND_IN_THE_PNR, "Invalid leg code");
                throw ex;
            }

            departureAirport = parts[0];
            arrivalAirport = null;
            operatingCarrier = parts[1];
            operatingFlightCode = parts[2];
            departureDate = parts[3];
        } else {
            departureAirport = passengersListRQ.getDepartureAirport();
            arrivalAirport = passengersListRQ.getArrivalAirport();
            operatingCarrier = passengersListRQ.getOperatingCarrier();
            operatingFlightCode = passengersListRQ.getOperatingFlightCode();
            departureDate = passengersListRQ.getDepartureDate();

            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder
                    .append(departureAirport)
                    .append("_")
                    .append(operatingCarrier)
                    .append("_")
                    .append(operatingFlightCode)
                    .append("_")
                    .append(departureDate);

            legCode = stringBuilder.toString();
        }

        StandbyList standbyList = new StandbyList();

        if (!AirlineCodeType.AM.getCode().equalsIgnoreCase(operatingCarrier)) {
            return standbyList;
        }

        ACSPassengerListRSACS passengerList = passengersListService.getPassengerList(
                operatingCarrier,
                operatingFlightCode,
                departureAirport,
                arrivalAirport,
                departureDate,
                displayCodes
        );

        if (passengerList != null
                && passengerList.getPassengerInfoList() != null
                && passengerList.getPassengerInfoList().getPassengerInfo() != null
                && !passengerList.getPassengerInfoList().getPassengerInfo().isEmpty()) {

            for (PassengerInfoACS passengerInfo : passengerList.getPassengerInfoList().getPassengerInfo()) {
                PriorityCode priorityCode = priorityCodesDaoCache.getPriorityCodeByCodeAndGroup(passengerInfo.getPriorityCode(), PriorityGroup.STANDBY);

                if (null != priorityCode) {

                    PassengerOnList passengerOnList = new PassengerOnList();

                    if (passengerInfo.getLastName().length() > 1) {
                        passengerOnList.setFirstName(passengerInfo.getFirstName().substring(0, 1));
                    } else {
                        passengerOnList.setFirstName(passengerInfo.getFirstName());
                    }

                    if (passengerInfo.getLastName().length() > 3) {
                        passengerOnList.setLastName(passengerInfo.getLastName().substring(0, 3));
                    } else {
                        passengerOnList.setLastName(passengerInfo.getLastName());
                    }

                    passengerOnList.setId(passengerInfo.getPassengerID());
                    passengerOnList.setPassengerType(passengerInfo.getPassengerType());
                    passengerOnList.setPnr(passengerInfo.getPNRLocator().getValue());
                    Priority priority = new Priority();
                    priority.setCode(priorityCode.getCode());
                    priority.setDescription(priorityCode.getDescription());
                    priority.setSegmentStatus(priorityCode.getSegmentStatus());
                    priority.setSegmentStatusId(priorityCode.getSegmentStatusId());
                    priority.setPriority(priorityCode.getPriority());
                    priority.setPriorityGroup(priorityCode.getGroup().get(0));
                    passengerOnList.setPriorityCode(priority);
                    passengerOnList.setSeat(passengerInfo.getOutboundFlight());
                    passengerOnList.setBoardingPassFlag("*".equalsIgnoreCase(passengerInfo.getBoardingPassFlag()) || "-".equalsIgnoreCase(passengerInfo.getBoardingPassFlag()));

                    standbyList.getPassengers().add(passengerOnList);
                }
            }
        }

        //standbyList.sortByPriority();
        try {

            ACSFlightDetailRSACS flightDetails = flightDetailsService.getFlightDetails(
                    operatingCarrier,
                    operatingFlightCode,
                    departureAirport,
                    departureDate,
                    serviceCallList,
                    passengersListRQ.getPos()
            );

            ArrayList<CabinCapacity> listCabinCapacity = new ArrayList<>();

            if (null != flightDetails && null != flightDetails.getPassengerCounts()) {
                for (ACSFlightDetailRSACS.PassengerCounts passengerCounts : flightDetails.getPassengerCounts()) {
                    if ("Y".equalsIgnoreCase(passengerCounts.getClassOfService())) {
                        listCabinCapacity.add(new CabinCapacity("main", passengerCounts.getBooked(), passengerCounts.getAuthorized(), passengerCounts.getTotalBoardingPassIssued(), passengerCounts.getTotalOnBoard()));
                    } else {
                        listCabinCapacity.add(new CabinCapacity("premier", passengerCounts.getBooked(), passengerCounts.getAuthorized(), passengerCounts.getTotalBoardingPassIssued(), passengerCounts.getTotalOnBoard()));
                    }
                }
            }

            List<SeatsAvailability> seatsAvailabilityList = new ArrayList<>();

            for (CabinCapacity cabinCapacity : listCabinCapacity) {
                SeatsAvailability seatsAvailability = new SeatsAvailability();

                seatsAvailability.setCabin(cabinCapacity.getCabin());
                seatsAvailability.setCheckedIn(cabinCapacity.getTotalBoardingPassIssued());
                seatsAvailability.setRemaining(Math.max(0, (cabinCapacity.getAuthorized() - cabinCapacity.getBooked())));
                seatsAvailability.setReserved(cabinCapacity.getBooked());
                seatsAvailability.setTotalOnBoard(cabinCapacity.getTotalOnBoard());

                seatsAvailabilityList.add(seatsAvailability);
            }

            standbyList.getSeatsAvailability().put(legCode, seatsAvailabilityList);

        } catch (Exception ex) {
            ExtraInfo extraInfo = new ExtraInfo();
            extraInfo.setDebugging_stuff_here(LogUtil.getTrace(ex));
            Warning warning = new Warning();
            warning.setMsg(ex.getMessage());
            warning.setExtraInfo(extraInfo);
            standbyList.getWarnings().addToList(warning);
        }

        return standbyList;
    }

    @Deprecated
    public UpgradeList getUpgradeList(
            PassengersListRQ passengersListRQ, List<ServiceCall> serviceCallList
    ) throws Exception {

        String pos = passengersListRQ.getPos();
        String store = passengersListRQ.getStore();
        String language = passengersListRQ.getLanguage();

        String departureAirport;
        String arrivalAirport;
        String operatingCarrier;
        String operatingFlightCode;
        String departureDate;
        String[] displayCodes = {"PALL"};
        String legCode;

        if (null != passengersListRQ.getLegCode()) {
            legCode = passengersListRQ.getLegCode();

            String[] parts = passengersListRQ.getLegCode().split("_");


            if (parts.length < 4) {
                GenericException ex = new GenericException(Response.Status.BAD_REQUEST, ErrorType.LEGCODE_NOT_FOUND_IN_THE_PNR, "Invalid leg code");
                throw ex;
            }

            departureAirport = parts[0];
            arrivalAirport = null;
            operatingCarrier = parts[1];
            operatingFlightCode = parts[2];
            departureDate = parts[3];
        } else {
            departureAirport = passengersListRQ.getDepartureAirport();
            arrivalAirport = passengersListRQ.getArrivalAirport();
            operatingCarrier = passengersListRQ.getOperatingCarrier();
            operatingFlightCode = passengersListRQ.getOperatingFlightCode();
            departureDate = passengersListRQ.getDepartureDate();

            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder
                    .append(departureAirport)
                    .append("_")
                    .append(operatingCarrier)
                    .append("_")
                    .append(operatingFlightCode)
                    .append("_")
                    .append(departureDate);

            legCode = stringBuilder.toString();
        }

        UpgradeList upgradeList = new UpgradeList();

        if (!AirlineCodeType.AM.getCode().equalsIgnoreCase(operatingCarrier)) {
            return upgradeList;
        }

        ACSPassengerListRSACS passengerList = passengersListService.getPassengerList(
                operatingCarrier,
                operatingFlightCode,
                departureAirport,
                arrivalAirport,
                departureDate,
                displayCodes
        );

        if (passengerList != null
                && passengerList.getPassengerInfoList() != null
                && passengerList.getPassengerInfoList().getPassengerInfo() != null
                && !passengerList.getPassengerInfoList().getPassengerInfo().isEmpty()) {

            for (PassengerInfoACS passengerInfo : passengerList.getPassengerInfoList().getPassengerInfo()) {

                PriorityCode priorityCode;
                if (null != passengerInfo.getUpgradeCode()) {
                    priorityCode = priorityCodesDaoCache.getPriorityCodeByCodeAndGroup(passengerInfo.getUpgradeCode(), PriorityGroup.UPGRADE);
                } else {
                    priorityCode = priorityCodesDaoCache.getPriorityCodeByCodeAndGroup(passengerInfo.getPriorityCode(), PriorityGroup.UPGRADE);
                }

                if (null != priorityCode) {
                    PassengerOnList passengerOnList = new PassengerOnList();

                    if (passengerInfo.getLastName().length() > 1) {
                        passengerOnList.setFirstName(passengerInfo.getFirstName().substring(0, 1));
                    } else {
                        passengerOnList.setFirstName(passengerInfo.getFirstName());
                    }

                    if (passengerInfo.getLastName().length() > 3) {
                        passengerOnList.setLastName(passengerInfo.getLastName().substring(0, 3));
                    } else {
                        passengerOnList.setLastName(passengerInfo.getLastName());
                    }

                    passengerOnList.setId(passengerInfo.getPassengerID());
                    passengerOnList.setPassengerType(passengerInfo.getPassengerType());
                    passengerOnList.setPnr(passengerInfo.getPNRLocator().getValue());
                    Priority priority = new Priority();
                    priority.setCode(priorityCode.getCode());
                    priority.setDescription(priorityCode.getDescription());
                    priority.setSegmentStatus(priorityCode.getSegmentStatus());
                    priority.setSegmentStatusId(priorityCode.getSegmentStatusId());
                    priority.setPriority(priorityCode.getPriority());
                    priority.setPriorityGroup(priorityCode.getGroup().get(0));
                    passengerOnList.setPriorityCode(priority);
                    passengerOnList.setSeat(passengerInfo.getOutboundFlight());
                    passengerOnList.setBoardingPassFlag("*".equalsIgnoreCase(passengerInfo.getBoardingPassFlag()) || "-".equalsIgnoreCase(passengerInfo.getBoardingPassFlag()));

                    upgradeList.getPassengers().add(passengerOnList);
                }
            }
        }

        //upgradeList.sortByPriority();
        List<PassengerOnList> passengersOnList = new ArrayList<>();

        List<PassengerOnList> revenueUpgradeList = upgradeList.getPassengers()
                .stream()
                .filter(i -> i.getPriorityCode() != null
                        && i.getPriorityCode().getCode() != null
                        && !PriorityCodesDaoCache.getUPGRADE_CODES_EMPLOYEE().contains(i.getPriorityCode().getCode()))
                .collect(Collectors.toList());

        passengersOnList.addAll(revenueUpgradeList);

        List<PassengerOnList> employeeUpgradeList = upgradeList.getPassengers()
                .stream()
                .filter(i -> i.getPriorityCode() != null
                        && i.getPriorityCode().getCode() != null
                        && PriorityCodesDaoCache.getUPGRADE_CODES_EMPLOYEE().contains(i.getPriorityCode().getCode()))
                .collect(Collectors.toList());

        employeeUpgradeList.sort(
                (PassengerOnList p1, PassengerOnList p2) -> {
                    return (p1.getPriorityCode().getPriority() < p2.getPriorityCode().getPriority()) ? -1 : (p1.getPriorityCode().getPriority() > p2.getPriorityCode().getPriority()) ? 1 : 0;
                }
        );

        passengersOnList.addAll(employeeUpgradeList);

        upgradeList.setPassengers(passengersOnList);

        try {

            ACSFlightDetailRSACS flightDetails = flightDetailsService.getFlightDetails(
                    operatingCarrier,
                    operatingFlightCode,
                    departureAirport,
                    departureDate,
                    serviceCallList,
                    pos
            );

            ArrayList<CabinCapacity> listCabinCapacity = new ArrayList<>();

            if (null != flightDetails && null != flightDetails.getPassengerCounts()) {
                for (ACSFlightDetailRSACS.PassengerCounts passengerCounts : flightDetails.getPassengerCounts()) {
                    if ("Y".equalsIgnoreCase(passengerCounts.getClassOfService())) {
                        listCabinCapacity.add(new CabinCapacity("main", passengerCounts.getBooked(), passengerCounts.getAuthorized(), passengerCounts.getTotalBoardingPassIssued(), passengerCounts.getTotalOnBoard()));
                    } else {
                        listCabinCapacity.add(new CabinCapacity("premier", passengerCounts.getBooked(), passengerCounts.getAuthorized(), passengerCounts.getTotalBoardingPassIssued(), passengerCounts.getTotalOnBoard()));
                    }
                }
            }

            List<SeatsAvailability> seatsAvailabilityList = new ArrayList<>();

            for (CabinCapacity cabinCapacity : listCabinCapacity) {
                SeatsAvailability seatsAvailability = new SeatsAvailability();

                seatsAvailability.setCabin(cabinCapacity.getCabin());
                seatsAvailability.setCheckedIn(cabinCapacity.getTotalBoardingPassIssued());
                seatsAvailability.setRemaining(Math.max(0, (cabinCapacity.getAuthorized() - cabinCapacity.getBooked())));
                seatsAvailability.setReserved(cabinCapacity.getBooked());
                seatsAvailability.setTotalOnBoard(cabinCapacity.getTotalOnBoard());

                seatsAvailabilityList.add(seatsAvailability);
            }

            upgradeList.getSeatsAvailability().put(legCode, seatsAvailabilityList);
        } catch (Exception ex) {
            ExtraInfo extraInfo = new ExtraInfo();
            extraInfo.setDebugging_stuff_here(LogUtil.getTrace(ex));
            Warning warning = new Warning();
            warning.setMsg(ex.getMessage());
            warning.setExtraInfo(extraInfo);
            upgradeList.getWarnings().addToList(warning);
        }

        return upgradeList;
    }

}
