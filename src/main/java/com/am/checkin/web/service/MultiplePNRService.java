package com.am.checkin.web.service;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.ExtraInfo;
import com.aeromexico.commons.model.PNR;
import com.aeromexico.commons.model.PNRCollection;
import com.aeromexico.commons.model.Warning;
import com.aeromexico.commons.model.WarningCollection;
import com.aeromexico.commons.model.rq.PnrRQ;
import com.aeromexico.commons.web.types.ErrorType;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import javax.inject.Inject;
import javax.ws.rs.core.Response;

import com.aeromexico.commons.web.types.PosType;
import com.aeromexico.sabre.api.session.exception.SabreEmptyResponseException;
import com.aeromexico.sabre.api.session.exception.SabreLayerUnavailableException;
import org.aeromexico.commons.exception.config.ReadProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class to create a thread of pool async for PNR Look up
 *
 * @author Manuel Paz
 */
public class MultiplePNRService {
    private final List<PnrRQ> pnrRQList;
    private final PNRLookupService pnrLookupService;
    private static final Logger LOG = LoggerFactory.getLogger(MultiplePNRService.class);

    @Inject
    private ReadProperties prop;


    public MultiplePNRService(List<PnrRQ> pnrRQList, PNRLookupService pnrLookupService) {
        this.pnrRQList = pnrRQList;
        this.pnrLookupService = pnrLookupService;
    }

    public PNRCollection multiplePNRs() throws InterruptedException {
        List<PNRCallable> callablePNRs = new ArrayList<>();
        PNRCallable pnrCallable;

        //Building callable list
        for (PnrRQ pnrRQSearch : pnrRQList) {
            pnrCallable = new PNRCallable(pnrRQSearch, pnrLookupService);
            callablePNRs.add(pnrCallable);
        }

        ExecutorService executorService = Executors.newFixedThreadPool(pnrRQList.size());

        List<Future<PNR>> futurePNRList = executorService.invokeAll(callablePNRs);

        PNRCollection pnrCollection = new PNRCollection();
        WarningCollection warningCollection = new WarningCollection();

        for (Future future : futurePNRList) {
            try {
                pnrCollection.getCollection().add((PNR) future.get());
            }catch (SabreLayerUnavailableException se){
                if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(pnrCollection.getPos())) {
                    LOG.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_22110301,"MultiplePNRService.java:", se.getMessage());
                    throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getCode(),
                            prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getActionable(),
                            prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getMessage(),
                            Response.Status.INTERNAL_SERVER_ERROR);
                } else {
                    LOG.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_12110301,"MultiplePNRService.java:74", se.getMessage());
                    throw se;
                }
            }catch (SabreEmptyResponseException ser) {
                LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110302).toString(), ser.fillInStackTrace());
                throw ser;
            }  catch (ExecutionException ex) {
                //If exist some error set as warning
                Warning warning = new Warning(ErrorType.RESERVATION_NOT_FOUND.getErrorCode(),
                        ErrorType.RESERVATION_NOT_FOUND.getFullDescription(),
                        new ExtraInfo(ex.getMessage()));
                warningCollection.getCollection().add(warning);
            }
        }

        pnrCollection.setWarnings(warningCollection);
        executorService.shutdown();

        if (pnrCollection.getCollection().isEmpty()) {
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.RESERVATION_NOT_FOUND, ErrorType.NAME_DOES_NOT_MATCH_FREQUENT_TRAVELER_NUMBER.getFullDescription());
        } else {
            return pnrCollection;
        }
    }
}
