package com.am.checkin.web.service;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.AncillaryOffer;
import com.aeromexico.commons.model.AuthorizationPaymentConfig;
import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.BookedTravelerCollection;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.FlxPriceWrapper;
import com.aeromexico.commons.model.PNR;
import com.aeromexico.commons.model.TravelerAncillary;
import com.aeromexico.commons.model.TravelerAncillaryBaseProps;
import com.aeromexico.commons.model.UpgradeAncillary;
import com.aeromexico.commons.model.rq.AncillaryRQ;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.farelogix.api.servicelist.AMServiceListFlx;
import com.am.checkin.web.util.FlxServiceListUtil;
import com.am.checkin.web.util.FlxAncillaryPriceUtil;
import com.farelogix.flx.servicelistrq.ServiceListRQ;
import com.farelogix.flx.servicelistrs.ServiceListRS;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author adrianleal
 */
@Named
@ApplicationScoped
public class FlxAncillaryPriceService {

    private static final Logger LOG = LoggerFactory.getLogger(FlxAncillaryPriceService.class);

    @Inject
    private AMServiceListFlx serviceListFlx;

    @Inject
    @Named("CheckInSetUpConfigFactoryWrapper")
    private CheckInSetUpConfigFactoryWrapper setUpConfigFactory;

    /**
     * @param cartPNR
     * @param bookedLeg
     * @param upgradeAncillaryList
     * @param recordLocator
     * @param store
     * @param pos
     * @throws Exception
     */
    public void setPricesFromFareLogix(
            CartPNR cartPNR,
            BookedLeg bookedLeg,
            List<UpgradeAncillary> upgradeAncillaryList,
            String recordLocator,
            String store,
            String pos
    ) throws Exception {

        AuthorizationPaymentConfig authorizationPaymentConfig = setUpConfigFactory.getInstance().getAuthorizationPaymentConfig(store, pos);

        if (null == authorizationPaymentConfig) {
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PAYMENT_CONFIG_NOT_FOUND, "payment config not found");
        }

        String currencyCode = authorizationPaymentConfig.getCurrency();

        if (null == currencyCode) {
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.CURRENCY_CODE_NOT_FOUND, "currency code not found");
        }

        if (null == cartPNR) {
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION, "Cart not found");
        }

        if (null == bookedLeg) {
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION, "BookedLeg not found");
        }

        BookedTravelerCollection bookedTravelerCollection = cartPNR.getTravelerInfo();

        Map<String, String> segmentReferences = new HashMap<>();

        Map<String, String> travelerReferences = new HashMap<>();

        List<BookedSegment> amBookedSegmentsWithAncillaries = new ArrayList<>();

        for (UpgradeAncillary upgradeAncillary : upgradeAncillaryList) {
            BookedSegment bookedSegmentWithAncillary = bookedLeg.getBookedSegment(upgradeAncillary.getSegmentCode());
            if (null != bookedLeg) {
                amBookedSegmentsWithAncillaries.add(bookedSegmentWithAncillary);
            }
        }

        if (null != amBookedSegmentsWithAncillaries && !amBookedSegmentsWithAncillaries.isEmpty()) {

            int flightIndex = 1;
            for (BookedSegment bookedSegment : amBookedSegmentsWithAncillaries) {
                segmentReferences.put(bookedSegment.getSegment().getSegmentCode(), "" + flightIndex++);
            }

            int travelerIndex = 1;
            for (BookedTraveler bookedTraveler : bookedTravelerCollection.getCollection()) {
                travelerReferences.put(bookedTraveler.getNameRefNumber(), "t" + travelerIndex++);
            }

            ServiceListRQ serviceListRQ = FlxServiceListUtil.getAncillaryServiceListRQ(
                    amBookedSegmentsWithAncillaries,
                    bookedTravelerCollection,
                    recordLocator,
                    currencyCode
            );

            ServiceListRS serviceListRS = serviceListFlx.getServiceListRS(serviceListRQ);

            //NameRef   SegmentCode AncillaryCode
            Map<String, Map<String, Map<String, FlxPriceWrapper>>> pricePerPassengerMap;
            pricePerPassengerMap = new HashMap<>();

            for (BookedTraveler bookedTraveler : bookedTravelerCollection.getCollection()) {

                Map<String, Map<String, FlxPriceWrapper>> pricePerSegmentCodeList;
                pricePerSegmentCodeList = FlxAncillaryPriceUtil.getFLXPricesPerSegment(
                        segmentReferences,
                        serviceListRS,
                        currencyCode,
                        travelerReferences.get(bookedTraveler.getNameRefNumber())
                );

                pricePerPassengerMap.put(bookedTraveler.getNameRefNumber(), pricePerSegmentCodeList);

            }

            LOG.info("pricePerPassengerMap: {}", pricePerPassengerMap.toString());

            for (UpgradeAncillary upgradeAncillary : upgradeAncillaryList) {
                AncillaryOffer ancillaryOffer = upgradeAncillary.getAncillaryOffer();
                String segmentCode = ancillaryOffer.getSegmentCodeAux();

                //Just First Passenger
                Map<String, Map<String, FlxPriceWrapper>> pricePerSegmentCodeList;
                pricePerSegmentCodeList = pricePerPassengerMap.get(bookedTravelerCollection.getCollection().get(0).getNameRefNumber());
                Map<String, FlxPriceWrapper> pricesPerSegment = pricePerSegmentCodeList.get(segmentCode);

                if (null != pricesPerSegment && !pricesPerSegment.isEmpty()) {
                    String code = ancillaryOffer.getAncillary().getType();

                    FlxPriceWrapper flxPriceWrapper = pricesPerSegment.get(code);

                    if (null != flxPriceWrapper && null != flxPriceWrapper.getCurrencyValue()) {

                        boolean isDifferent = FlxAncillaryPriceUtil.isFlxPriceDifferentThanAmPrice(
                                flxPriceWrapper.getCurrencyValue(),
                                ancillaryOffer.getAncillary()
                        );

                        if (isDifferent) {
                            ancillaryOffer.getAncillary().setCurrency(flxPriceWrapper.getCurrencyValue());
                            ancillaryOffer.getAncillary().setPriceCalcList(flxPriceWrapper.getPriceCalcList());
                            ancillaryOffer.getAncillary().setFarelogixPrice(true);
                        }
                    }
                }
            }
        } else {
            LOG.info("There are not segments with available ancillaries");
        }
    }

    public void setPricesFromFareLogix(
            PNR pnr,
            List<TravelerAncillaryBaseProps> travelerAncillaryBasePropsList,
            AncillaryRQ ancillaryRQ
    ) throws Exception {

        if (null == pnr) {
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION, "Pnr not found");
        }

        String pos = ancillaryRQ.getPos();
        String store = ancillaryRQ.getStore();

        AuthorizationPaymentConfig authorizationPaymentConfig = setUpConfigFactory.getInstance().getAuthorizationPaymentConfig(store, pos);

        if (null == authorizationPaymentConfig) {
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PAYMENT_CONFIG_NOT_FOUND, "payment config not found");
        }

        String currencyCode = authorizationPaymentConfig.getCurrency();

        if (null == currencyCode) {
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.CURRENCY_CODE_NOT_FOUND, "currancy code not found");
        }

        CartPNR cartPNR = pnr.getCartPNRByCartId(ancillaryRQ.getCartId());

        if (null == cartPNR) {
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION, "Cart not found");
        }

        String legCode = cartPNR.getLegCode();
        BookedLeg bookedLeg = pnr.getBookedLegByLegCode(legCode);
        if (null == bookedLeg) {
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION, "BookedLeg not found");
        }

        BookedTravelerCollection bookedTravelerCollection = cartPNR.getTravelerInfo();

        Map<String, String> segmentReferences = new HashMap<>();

        Map<String, String> travelerReferences = new HashMap<>();

        List<BookedSegment> amBookedSegmentsWithAncillaries;
        amBookedSegmentsWithAncillaries = FlxAncillaryPriceUtil.getSegmentsWithAncillaries(
                travelerAncillaryBasePropsList, bookedLeg
        );

        if (null != amBookedSegmentsWithAncillaries && !amBookedSegmentsWithAncillaries.isEmpty()) {

            int flightIndex = 1;
            for (BookedSegment bookedSegment : amBookedSegmentsWithAncillaries) {
                segmentReferences.put(bookedSegment.getSegment().getSegmentCode(), "" + flightIndex++);
            }

            int travelerIndex = 1;
            for (BookedTraveler bookedTraveler : bookedTravelerCollection.getCollection()) {
                travelerReferences.put(bookedTraveler.getNameRefNumber(), "t" + travelerIndex++);
            }

            ServiceListRQ serviceListRQ = FlxServiceListUtil.getAncillaryServiceListRQ(
                    amBookedSegmentsWithAncillaries,
                    bookedTravelerCollection,
                    pnr.getPnr(),
                    currencyCode
            );

            ServiceListRS serviceListRS = serviceListFlx.getServiceListRS(serviceListRQ);

            //NameRef   SegmentCode AncillaryCode
            Map<String, Map<String, Map<String, FlxPriceWrapper>>> pricePerPassengerMap;
            pricePerPassengerMap = new HashMap<>();

            for (BookedTraveler bookedTraveler : bookedTravelerCollection.getCollection()) {

                Map<String, Map<String, FlxPriceWrapper>> pricePerSegmentCodeList;
                pricePerSegmentCodeList = FlxAncillaryPriceUtil.getFLXPricesPerSegment(
                        segmentReferences,
                        serviceListRS,
                        currencyCode,
                        travelerReferences.get(bookedTraveler.getNameRefNumber())
                );
                pricePerPassengerMap.put(bookedTraveler.getNameRefNumber(), pricePerSegmentCodeList);

            }

            LOG.info("pricePerPassengerMap: {}", pricePerPassengerMap.toString());

            for (TravelerAncillaryBaseProps travelerAncillaryBaseProps : travelerAncillaryBasePropsList) {
                if (travelerAncillaryBaseProps instanceof TravelerAncillary) {
                    TravelerAncillary travelerAncillary = (TravelerAncillary) travelerAncillaryBaseProps;

                    String segmentCode = travelerAncillary.getSegmentCodeAux();

                    //Just First Passenger
                    Map<String, Map<String, FlxPriceWrapper>> pricePerSegmentCodeList;
                    pricePerSegmentCodeList = pricePerPassengerMap.get(bookedTravelerCollection.getCollection().get(0).getNameRefNumber());
                    Map<String, FlxPriceWrapper> pricesPerSegment = pricePerSegmentCodeList.get(segmentCode);

                    if (null != pricesPerSegment && !pricesPerSegment.isEmpty()) {
                        String code = travelerAncillary.getAncillary().getType();

                        FlxPriceWrapper flxPriceWrapper = pricesPerSegment.get(code);

                        if (null != flxPriceWrapper && null != flxPriceWrapper.getCurrencyValue()) {

                            boolean isDifferent = FlxAncillaryPriceUtil.isFlxPriceDifferentThanAmPrice(
                                    flxPriceWrapper.getCurrencyValue(),
                                    travelerAncillary.getAncillary()
                            );

                            if (isDifferent) {
                                travelerAncillary.getAncillary().setCurrency(flxPriceWrapper.getCurrencyValue());
                                travelerAncillary.getAncillary().setPriceCalcList(flxPriceWrapper.getPriceCalcList());
                                travelerAncillary.getAncillary().setFarelogixPrice(true);
                            }
                        }
                    }
                } else if (travelerAncillaryBaseProps instanceof AncillaryOffer) {
                    AncillaryOffer ancillaryOffer = (AncillaryOffer) travelerAncillaryBaseProps;

                    String segmentCode = ancillaryOffer.getSegmentCodeAux();

                    //Just First Passenger
                    Map<String, Map<String, FlxPriceWrapper>> pricePerSegmentCodeList;
                    pricePerSegmentCodeList = pricePerPassengerMap.get(bookedTravelerCollection.getCollection().get(0).getNameRefNumber());
                    Map<String, FlxPriceWrapper> pricesPerSegment = pricePerSegmentCodeList.get(segmentCode);

                    if (null != pricesPerSegment && !pricesPerSegment.isEmpty()) {
                        String code = ancillaryOffer.getAncillary().getType();

                        FlxPriceWrapper flxPriceWrapper = pricesPerSegment.get(code);

                        if (null != flxPriceWrapper && null != flxPriceWrapper.getCurrencyValue()) {

                            boolean isDifferent = FlxAncillaryPriceUtil.isFlxPriceDifferentThanAmPrice(
                                    flxPriceWrapper.getCurrencyValue(),
                                    ancillaryOffer.getAncillary()
                            );

                            if (isDifferent) {
                                ancillaryOffer.getAncillary().setCurrency(flxPriceWrapper.getCurrencyValue());
                                ancillaryOffer.getAncillary().setPriceCalcList(flxPriceWrapper.getPriceCalcList());
                                ancillaryOffer.getAncillary().setFarelogixPrice(true);
                            }
                        }
                    }
                }
            }
        } else {
            LOG.info("There are not segments with available ancillaries");
        }
    }
}
