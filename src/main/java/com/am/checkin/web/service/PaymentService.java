package com.am.checkin.web.service;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.loggingutils.LogUtil;
import com.aeromexico.commons.model.AbstractAncillary;
import com.aeromexico.commons.model.AuthorizationPaymentConfig;
import com.aeromexico.commons.model.AuthorizationResult;
import com.aeromexico.commons.model.BaggageRoute;
import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.BookingClass;
import com.aeromexico.commons.model.CardToken;
import com.aeromexico.commons.model.CreditCardPaymentRequest;
import com.aeromexico.commons.model.CurrencyValue;
import com.aeromexico.commons.model.FormsOfPaymentWrapper;
import com.aeromexico.commons.model.PaymentRequestCollection;
import com.aeromexico.commons.model.Phone;
import com.aeromexico.commons.model.PurchaseOrder;
import com.aeromexico.commons.model.Segment;
import com.aeromexico.commons.model.UatpPaymentRequest;
import com.aeromexico.commons.model.rq.PinPadInitializationRQ;
import com.aeromexico.commons.model.rq.PurchaseOrderRQ;
import com.aeromexico.commons.model.utils.CurrencyUtil;
import com.aeromexico.commons.model.VisaCheckoutPaymentRequest;
import com.aeromexico.commons.model.PaymentInstrument;
import com.aeromexico.commons.model.Address;
import com.aeromexico.commons.prototypes.JSONPrototype;
import com.aeromexico.commons.web.types.*;
import com.aeromexico.commons.web.util.Constants;
import com.aeromexico.commons.web.util.TimeUtil;
import com.aeromexico.dao.util.CommonUtil;
import com.aeromexico.sabre.api.payment.AMPaymentService;
import com.aeromexico.sabre.api.session.common.ServiceCall;
import com.aeromexico.sabre.api.session.exception.SabreEmptyResponseException;
import com.aeromexico.sabre.api.session.exception.SabreLayerUnavailableException;
import com.aeromexico.visacheckout.common.model.APICardInfo;
import com.aeromexico.visacheckout.integration.service.PaymentFacade;
import com.am.checkin.web.util.PurchaseOrderUtil;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.am.checkin.web.v2.util.SystemVariablesUtil;
import org.aeromexico.commons.exception.config.ReadProperties;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.opentravel.ota.payments.ContactInfoType;
import org.opentravel.ota.payments.FOPType;
import org.opentravel.ota.payments.FlightDetailType;
import org.opentravel.ota.payments.MerchantDetailType;
import org.opentravel.ota.payments.POSType;
import org.opentravel.ota.payments.PassengerDetailType;
import org.opentravel.ota.payments.PaymentCardType;
import org.opentravel.ota.payments.PaymentDetailType;
import org.opentravel.ota.payments.PaymentRQ;
import org.opentravel.ota.payments.PaymentRS;
import org.opentravel.ota.payments.ProductDetailType;
import org.opentravel.ota.payments.ReturnURLsType;
import org.opentravel.ota.payments.TokensType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author adrian
 */
@Named
@ApplicationScoped
public class PaymentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentService.class);

    private static final String SERVICE = "Payment Authorization";
    private static final String DEVICE_ID = "BANAMEX02";
    private static final String VERSION = "3.0.0";
    private static final String MERCHANT_ID = "AM";

    private static final List<String> STORE_FRONTS_FOR_3DS = new ArrayList<>(Arrays.asList("EU", "ES", "FR", "NL", "GB"));

    @Inject
    private AMPaymentService paymentService;

    @Inject
    @Named("CheckInSetUpConfigFactoryWrapper")
    private CheckInSetUpConfigFactoryWrapper setUpConfigFactory;

    @Inject
    private ReadProperties prop;

    @PostConstruct
    public void init() {
        if (null == setUpConfigFactory) {
            setUpConfigFactory = new CheckInSetUpConfigFactoryWrapper();
        }
    }

    /**
     * @param pinPadInitializationRQ
     * @return
     * @throws DatatypeConfigurationException
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws Exception
     */
    public PaymentRQ getEncryptKeyRQ(
            PinPadInitializationRQ pinPadInitializationRQ
    ) throws DatatypeConfigurationException, IOException, ClassNotFoundException, Exception {

        String ipAddr = pinPadInitializationRQ.getHttpRequest().getRemoteAddr();
        GregorianCalendar c = new GregorianCalendar();
        c.setTimeInMillis(pinPadInitializationRQ.getTransactionTime());
        XMLGregorianCalendar xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
        xmlGregorianCalendar.setMillisecond(DatatypeConstants.FIELD_UNDEFINED);
        xmlGregorianCalendar.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
        PaymentRQ paymentRQ = new PaymentRQ();
        paymentRQ.setSystemDateTime(xmlGregorianCalendar);
        paymentRQ.setVersion(VERSION);
        PaymentRQ.Action action = new PaymentRQ.Action();
        //action.setType("DEVICE");
        //action.setValue("CancelAuth");
        action.setValue("GetEncryptKey");
        paymentRQ.setAction(action);

        POSType posType = new POSType();
        xmlGregorianCalendar.setMillisecond(DatatypeConstants.FIELD_UNDEFINED);
        xmlGregorianCalendar.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
        posType.setLocalDateTime(xmlGregorianCalendar);
        posType.setChannelID(SabreChannelIdentifierType.ATO.getCode());//ATO, WEB

        //always Mexico for KIOSK
        AuthorizationPaymentConfig authorizationPaymentConfig;
        authorizationPaymentConfig = setUpConfigFactory.getInstance().getAuthorizationPaymentConfig(
                StoreFrontType.MX.toString(),
                Constants.PROJECT_KIOSK
        );

        posType.setISOCountry(authorizationPaymentConfig.getIsoCountry());
        posType.setPseudoCityCode(authorizationPaymentConfig.getPseudoCityCode());
        posType.setCityCode(authorizationPaymentConfig.getCityCode());
        posType.setStationNumber(authorizationPaymentConfig.getStationNumber());
        posType.setLNIATA(setUpConfigFactory.getInstance().getTicketLNIATA());

        if (null != ipAddr && !ipAddr.isEmpty()) {
            posType.setIPAddress(ipAddr);
        }

        paymentRQ.setPOS(posType);

        PaymentRQ.OrderDetail orderDetail = new PaymentRQ.OrderDetail();
        //orderDetail.setRecordLocator(pnrLocator);

        String orderId = TimeUtil.getStrTime(pinPadInitializationRQ.getTransactionTime());
        orderDetail.setOrderID(orderId);

        //TODO: check that values
        orderDetail.setOneWayInd(Boolean.FALSE);
        orderDetail.setOrderType("O");
        orderDetail.setThirdPartyBookingInd(Boolean.FALSE);

        paymentRQ.setOrderDetail(orderDetail);

        this.setPaymentDetailsForInitialization(paymentRQ, pinPadInitializationRQ);

        return paymentRQ;
    }

    /**
     * @param bookedTravelerList
     * @param bookedLeg
     * @param purchaseOrderRQ
     * @param travelerAncillaryList
     * @param pnrLocator
     * @param toPaidItemsType
     * @return
     * @throws DatatypeConfigurationException
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws Exception
     */
    public PaymentRQ getPaymentAuthRQ(
            List<BookedTraveler> bookedTravelerList,
            BookedLeg bookedLeg,
            PurchaseOrderRQ purchaseOrderRQ,
            List<AbstractAncillary> travelerAncillaryList,
            String pnrLocator,
            ItemsToBePaidType toPaidItemsType
    ) throws DatatypeConfigurationException, IOException, ClassNotFoundException, Exception {

        /**
         * PaymentRequestCollection is a reference parameter to return modified
         * card, it happens when the card is read by the pin pad, there are
         * missing fields that need to be filled
         *
         */
        PaymentRequestCollection paymentRequestCollection = new PaymentRequestCollection();

        PaymentRQ.OrderDetail orderDetail = new PaymentRQ.OrderDetail();

        List<ProductDetailType> productsDetail = orderDetail.getProductDetail();
        productsDetail.addAll(
                PurchaseOrderUtil.getProductsDetails(
                        bookedTravelerList, travelerAncillaryList, toPaidItemsType
                )
        );

        if (productsDetail.isEmpty()) {
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNKNOWN_CODE, "No products found");
        }

        String ipAddr = purchaseOrderRQ.getHttpRequest().getRemoteAddr();
        GregorianCalendar c = new GregorianCalendar();
        c.setTimeInMillis(purchaseOrderRQ.getTransactionTime());
        XMLGregorianCalendar xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
        xmlGregorianCalendar.setMillisecond(DatatypeConstants.FIELD_UNDEFINED);
        xmlGregorianCalendar.setTimezone(DatatypeConstants.FIELD_UNDEFINED);

        PaymentRQ paymentRQ = new PaymentRQ();

        paymentRQ.setSystemDateTime(xmlGregorianCalendar);
        paymentRQ.setVersion(VERSION);
        PaymentRQ.Action action = new PaymentRQ.Action();
        //action.setType("DEVICE");
        //action.setValue("CancelAuth");
        action.setValue("Auth");
        paymentRQ.setAction(action);

        AuthorizationPaymentConfig authorizationPaymentConfig;

        if (PosType.KIOSK.toString().equalsIgnoreCase(purchaseOrderRQ.getPos())) {
            POSType posType = new POSType();


            if (SystemVariablesUtil.isThreeDsKioskEnabled()
                    && STORE_FRONTS_FOR_3DS.contains(purchaseOrderRQ.getStore().toUpperCase())) {

                POSType.BrowserDetail browserDetail = new POSType.BrowserDetail();
                POSType.BrowserDetail.HttpHeaders httpHeaders = new POSType.BrowserDetail.HttpHeaders();

                for (Map.Entry<String, List<String>> stringListEntry : purchaseOrderRQ.getHeaders().getRequestHeaders().entrySet()) {
                    if (null != stringListEntry.getKey() && null != stringListEntry.getValue() && !stringListEntry.getValue().isEmpty()) {
                        String headerName = stringListEntry.getKey();
                        String headerValue = stringListEntry.getValue().get(0);

                        POSType.BrowserDetail.HttpHeaders.HttpHeader httpHeader = new POSType.BrowserDetail.HttpHeaders.HttpHeader();
                        httpHeader.setName(headerName);
                        httpHeader.setValue(headerValue);

                        httpHeaders.getHttpHeader().add(httpHeader);
                    }
                }

                browserDetail.setHttpHeaders(httpHeaders);
                posType.setBrowserDetail(browserDetail);
            }


            xmlGregorianCalendar.setMillisecond(DatatypeConstants.FIELD_UNDEFINED);
            xmlGregorianCalendar.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
            posType.setLocalDateTime(xmlGregorianCalendar);
            posType.setChannelID(SabreChannelIdentifierType.ATO.getCode());//ATO, WEB

            //always mexico for KIOSK
            authorizationPaymentConfig = setUpConfigFactory.getInstance().getAuthorizationPaymentConfig(
                    purchaseOrderRQ.getStore(),
                    purchaseOrderRQ.getPos()
            );

            posType.setISOCountry(authorizationPaymentConfig.getIsoCountry());
            posType.setPseudoCityCode(authorizationPaymentConfig.getPseudoCityCode());
            posType.setCityCode(authorizationPaymentConfig.getCityCode());
            posType.setStationNumber(authorizationPaymentConfig.getStationNumber());
            posType.setLNIATA(setUpConfigFactory.getInstance().getTicketLNIATA());

            posType.setIPAddress(ipAddr);
            paymentRQ.setPOS(posType);
        } else {
            POSType posType = new POSType();

            if (((PosType.WEB.toString().equalsIgnoreCase(purchaseOrderRQ.getPos()) && SystemVariablesUtil.isThreeDsWebEnabled())
                    || (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(purchaseOrderRQ.getPos()) && SystemVariablesUtil.isThreeDsAppEnabled()))
                    && STORE_FRONTS_FOR_3DS.contains(purchaseOrderRQ.getStore().toUpperCase())) {

                POSType.BrowserDetail browserDetail = new POSType.BrowserDetail();
                POSType.BrowserDetail.HttpHeaders httpHeaders = new POSType.BrowserDetail.HttpHeaders();

                for (Map.Entry<String, List<String>> stringListEntry : purchaseOrderRQ.getHeaders().getRequestHeaders().entrySet()) {
                    if (null != stringListEntry.getKey() && null != stringListEntry.getValue() && !stringListEntry.getValue().isEmpty()) {
                        String headerName = stringListEntry.getKey();
                        String headerValue = stringListEntry.getValue().get(0);

                        POSType.BrowserDetail.HttpHeaders.HttpHeader httpHeader = new POSType.BrowserDetail.HttpHeaders.HttpHeader();
                        httpHeader.setName(headerName);
                        httpHeader.setValue(headerValue);

                        httpHeaders.getHttpHeader().add(httpHeader);
                    }
                }

                browserDetail.setHttpHeaders(httpHeaders);
                posType.setBrowserDetail(browserDetail);
            }

            xmlGregorianCalendar.setMillisecond(DatatypeConstants.FIELD_UNDEFINED);
            xmlGregorianCalendar.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
            posType.setLocalDateTime(xmlGregorianCalendar);
            posType.setChannelID(SabreChannelIdentifierType.WEB.getCode());//ATO, WEB

            authorizationPaymentConfig = setUpConfigFactory.getInstance().getAuthorizationPaymentConfig(
                    purchaseOrderRQ.getStore(),
                    purchaseOrderRQ.getPos()
            );

            posType.setISOCountry(authorizationPaymentConfig.getIsoCountry());
            posType.setPseudoCityCode(authorizationPaymentConfig.getPseudoCityCode());
            posType.setStationNumber(authorizationPaymentConfig.getStationNumber());
            posType.setLNIATA(setUpConfigFactory.getInstance().getTicketLNIATA());
            posType.setCityCode(authorizationPaymentConfig.getCityCode());

            posType.setIPAddress(ipAddr);
            paymentRQ.setPOS(posType);
        }

        orderDetail.setRecordLocator(pnrLocator);

        String orderId = pnrLocator + TimeUtil.getStrTime(purchaseOrderRQ.getTransactionTime(), "ddMMyy");
        orderDetail.setOrderID(orderId);

        //TODO: check that values
        orderDetail.setOneWayInd(Boolean.FALSE);
        orderDetail.setOrderType("O");
        orderDetail.setThirdPartyBookingInd(Boolean.FALSE);

        List<PassengerDetailType> passengersDetail = orderDetail.getPassengerDetail();

        for (BookedTraveler bookedTraveler : bookedTravelerList) {
            PassengerDetailType passengerDetailType = new PassengerDetailType();
            passengerDetailType.setNameInPNR(bookedTraveler.getLastName() + "/" + bookedTraveler.getFirstName());
            passengerDetailType.setLastName(bookedTraveler.getLastName());
            passengerDetailType.setFirstName(bookedTraveler.getFirstName());

            List<PassengerDetailType.Document> documents = passengerDetailType.getDocument();

            if (null != bookedTraveler.getTicketNumber() && !bookedTraveler.getTicketNumber().trim().isEmpty()) {
                PassengerDetailType.Document document = new PassengerDetailType.Document();
                document.setDocType(SabreDocumentType.TKT.getCode());
                document.setETicketInd(Boolean.TRUE);
                //document.setBaseFare(BigDecimal.ONE);
                //document.setTaxes(BigDecimal.ONE);
                documents.add(document);
            }

            passengersDetail.add(passengerDetailType);
        }
        //add contact information
        ContactInfoType contactInfoType = new ContactInfoType();
        List<String> emails = contactInfoType.getEmailAddress();
        for (BookedTraveler bookedTraveler : bookedTravelerList) {
            if (null != bookedTraveler.getEmail() && !bookedTraveler.getEmail().isEmpty()) {
                emails.add(bookedTraveler.getEmail());
            }
        }

        Set<String> emailsSet = getEmailsFromPaymentForm(purchaseOrderRQ);
        if (null != emailsSet && !emailsSet.isEmpty()) {
            for (String email : emailsSet) {
                if (!emails.contains(email)) {
                    emails.add(email);
                }
            }
        }

        //add list of phone numbers
        List<org.opentravel.ota.payments.PhoneType> phones = contactInfoType.getPhoneNumber();
        for (BookedTraveler bookedTraveler : bookedTravelerList) {

            if (null != bookedTraveler.getPhones()) {
                for (Phone phone : bookedTraveler.getPhones().getCollection()) {

                    org.opentravel.ota.payments.PhoneType phoneType = new org.opentravel.ota.payments.PhoneType();

                    switch (phone.getType()) {
                        case HOME:
                            phoneType.setType(SabrePhoneType.H.name());
                            break;
                        case MOBILE:
                            phoneType.setType(SabrePhoneType.M.name());
                            break;
                        case OFFICE:
                            phoneType.setType(SabrePhoneType.O.name());
                            break;
                        default:
                            break;
                    }

                    phoneType.setNumber(phone.getNumber());
                    phones.add(phoneType);
                }
            }
            break;
        }

        orderDetail.setContactInfo(contactInfoType);
        //set flights details
        List<FlightDetailType> flightsDetails = orderDetail.getFlightDetail();

        List<BookedSegment> bookedSegmentList = bookedLeg.getSegments().getSegmentsOperatedBy(AirlineCodeType.AM);

        BaggageRoute baggageRoute;
        BookingClass bookingClass;
        String cabinClass;
        int index = 1;
        for (BookedSegment bookedSegment : bookedSegmentList) {

            baggageRoute = PurchaseOrderUtil.getBaggageRouteBySegment(
                    bookedSegment.getSegment(),
                    bookedTravelerList.get(0).getBaggageRouteList()
            );

            if (null == baggageRoute) {
                bookingClass = PurchaseOrderUtil.getBookingClassBySegment(
                        bookedSegment.getSegment(),
                        bookedTravelerList.get(0).getBookingClasses()
                );
                if (null != bookingClass) {
                    cabinClass = bookingClass.getBookingClass();
                } else {
                    cabinClass = "";
                }
            } else {
                cabinClass = baggageRoute.getBookingClass();
            }

            Segment segment = bookedSegment.getSegment();

            FlightDetailType flightDetailType = new FlightDetailType();
            flightDetailType.setAirlineCode(segment.getOperatingCarrier());
            flightDetailType.setFlightNumber(segment.getOperatingFlightCode());

            flightDetailType.setClassOfService(cabinClass);

            FlightDetailType.DepartureInfo departureInfo = new FlightDetailType.DepartureInfo();

            c.setTimeInMillis(TimeUtil.getTime(segment.getDepartureDateTime()));
            XMLGregorianCalendar departureDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);

            departureInfo.setDepartureDateTime(departureDate);
            departureInfo.setCurrentLocalDateTime(xmlGregorianCalendar);
            departureInfo.setDepartureAirport(segment.getDepartureAirport());
            flightDetailType.setDepartureInfo(departureInfo);

            FlightDetailType.ArrivalInfo arrivalInfo = new FlightDetailType.ArrivalInfo();

            if (index++ == bookedSegmentList.size()) {
                arrivalInfo.setFinalDestinationInd(Boolean.TRUE);
            } else {
                arrivalInfo.setFinalDestinationInd(Boolean.FALSE);
            }

            c.setTimeInMillis(TimeUtil.getTime(segment.getArrivalDateTime()));
            XMLGregorianCalendar arrivalDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);

            arrivalInfo.setArrivalDateTime(arrivalDate);
            arrivalInfo.setArrivalAirport(segment.getArrivalAirport());
            flightDetailType.setArrivalInfo(arrivalInfo);

            flightsDetails.add(flightDetailType);
        }

        paymentRQ.setOrderDetail(orderDetail);

        setPaymentDetails(paymentRQ, purchaseOrderRQ, paymentRequestCollection);

        //Update card info request with modified payment collection
        purchaseOrderRQ.getPurchaseOrder().setPaymentInfo(paymentRequestCollection);

        return paymentRQ;
    }

    public Set<String> getEmailsFromPaymentForm(PurchaseOrderRQ purchaseOrderRQ) throws IOException, ClassNotFoundException {

        FormsOfPaymentWrapper formsOfPaymentWrapper;
        formsOfPaymentWrapper = new FormsOfPaymentWrapper(
                purchaseOrderRQ.getPurchaseOrder().getPaymentInfo()
        );

        Set<String> emails = new HashSet<>();

        List<CreditCardPaymentRequest> creditCardPaymentRequestList = formsOfPaymentWrapper.getCreditCardPaymentRequestList();
        if (null != creditCardPaymentRequestList && !creditCardPaymentRequestList.isEmpty()) {
            for (CreditCardPaymentRequest creditCardPaymentRequest : creditCardPaymentRequestList) {
                emails.add(creditCardPaymentRequest.getEmail());
            }
        }

        List<UatpPaymentRequest> uatpPaymentRequestList = formsOfPaymentWrapper.getUatpPaymentRequestList();
        if (null != uatpPaymentRequestList && !uatpPaymentRequestList.isEmpty()) {
            for (UatpPaymentRequest uatpPaymentRequest : uatpPaymentRequestList) {
                emails.add(uatpPaymentRequest.getEmail());
            }
        }

        return emails;
    }

    /**
     * @param paymentRQ
     * @param purchaseOrderRQ
     * @param paymentRequestCollection
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void setPaymentDetails(
            PaymentRQ paymentRQ,
            PurchaseOrderRQ purchaseOrderRQ,
            PaymentRequestCollection paymentRequestCollection
    ) throws Exception {

        //set payment information
        if (null != purchaseOrderRQ.getPurchaseOrder().getPaymentInfo()
                && null != purchaseOrderRQ.getPurchaseOrder().getPaymentInfo().getCollection()
                && !purchaseOrderRQ.getPurchaseOrder().getPaymentInfo().getCollection().isEmpty()) {

            FormsOfPaymentWrapper formsOfPaymentWrapper;
            formsOfPaymentWrapper = new FormsOfPaymentWrapper(
                    purchaseOrderRQ.getPurchaseOrder().getPaymentInfo()
            );

            if (!formsOfPaymentWrapper.getCreditCardPaymentRequestList().isEmpty()) {
                setCreditCardPaymentRQ(
                        formsOfPaymentWrapper.getCreditCardPaymentRequestList(),
                        purchaseOrderRQ,
                        paymentRQ,
                        paymentRequestCollection
                );
            } else if (!formsOfPaymentWrapper.getUatpPaymentRequestList().isEmpty()) {
                setUatpPaymentRQ(
                        formsOfPaymentWrapper.getUatpPaymentRequestList(),
                        purchaseOrderRQ,
                        paymentRQ,
                        paymentRequestCollection
                );
            } else if (!formsOfPaymentWrapper.getVisaCheckoutPaymentRequestList().isEmpty()) {
                setVisaCheckoutPaymentRQ(
                        formsOfPaymentWrapper.getVisaCheckoutPaymentRequestList(),
                        purchaseOrderRQ,
                        paymentRQ,
                        paymentRequestCollection
                );

            }
        }
    }

    /**
     * @param uatpPaymentRequestList
     * @param purchaseOrderRQ
     * @param paymentRQ
     * @param paymentRequestCollection
     * @throws GenericException
     */
    private void setUatpPaymentRQ(
            List<UatpPaymentRequest> uatpPaymentRequestList,
            PurchaseOrderRQ purchaseOrderRQ,
            PaymentRQ paymentRQ,
            PaymentRequestCollection paymentRequestCollection
    ) throws GenericException, IOException, ClassNotFoundException {

        List<PaymentDetailType> paymentsDetailList = paymentRQ.getPaymentDetail();

        for (UatpPaymentRequest uatpPaymentRequest : uatpPaymentRequestList) {

            PaymentDetailType paymentDetail = new PaymentDetailType();

            FOPType fop = new FOPType();
            fop.setType(SabreFOPType.CC.getCode());
            paymentDetail.setFOP(fop);

            PurchaseOrder purchaseOrder = purchaseOrderRQ.getPurchaseOrder();

            if (null != purchaseOrder.getThreeDSRedirectURL()
                    && ((PosType.WEB.toString().equalsIgnoreCase(purchaseOrderRQ.getPos()) && SystemVariablesUtil.isThreeDsWebEnabled())
                    || (PosType.KIOSK.toString().equalsIgnoreCase(purchaseOrderRQ.getPos()) && SystemVariablesUtil.isThreeDsKioskEnabled())
                    || (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(purchaseOrderRQ.getPos()) && SystemVariablesUtil.isThreeDsAppEnabled()))
                    && STORE_FRONTS_FOR_3DS.contains(purchaseOrderRQ.getStore().toUpperCase())) {

                ReturnURLsType returnURLsType = new ReturnURLsType();
                returnURLsType.setApprovedURL(purchaseOrder.getThreeDSRedirectURL() + "/aproved");
                returnURLsType.setCancelURL(purchaseOrder.getThreeDSRedirectURL() + "/cancel");
                returnURLsType.setDeclinedURL(purchaseOrder.getThreeDSRedirectURL() + "/declined");
                returnURLsType.setErrorURL(purchaseOrder.getThreeDSRedirectURL() + "/error");
                returnURLsType.setExpiredURL(purchaseOrder.getThreeDSRedirectURL() + "/expired");
                returnURLsType.setPendingURL(purchaseOrder.getThreeDSRedirectURL() + "/pending");
                returnURLsType.setDefaultURL(purchaseOrder.getThreeDSRedirectURL() + "/default");

                paymentDetail.setReturnURLs(returnURLsType);
            }

            PaymentDetailType.PaymentCard paymentCard = new PaymentDetailType.PaymentCard();

            //just for kiosk
            if (PosType.KIOSK.toString().equalsIgnoreCase(purchaseOrderRQ.getPos())) {
                if (null == uatpPaymentRequest.getCardReader()) {
                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNKNOWN_CODE, "You need to specify the card reader properties");
                }

                PaymentCardType.CardReader cardReader = new PaymentCardType.CardReader();
                cardReader.setChipCardInd(uatpPaymentRequest.getCardReader().isChipCardInd());
                //cardReader.setDeviceID("");

                if (null != uatpPaymentRequest.getCardReader().getDeviceType() && !uatpPaymentRequest.getCardReader().getDeviceType().trim().isEmpty()) {
                    cardReader.setDeviceType(uatpPaymentRequest.getCardReader().getDeviceType());
                } else {
                    cardReader.setDeviceType(DEVICE_ID);
                }

                List<TokensType.Token> tokenList = cardReader.getToken();

                for (CardToken cardToken : uatpPaymentRequest.getCardReader().getCardTokenList()) {
                    TokensType.Token token = new TokensType.Token();
                    token.setName(cardToken.getName());
                    token.setValue(cardToken.getValue());
                    tokenList.add(token);
                }

                paymentCard.setCardReader(cardReader);

                if (null == uatpPaymentRequest.getExpiryDate()
                        || uatpPaymentRequest.getExpiryDate().trim().isEmpty()
                        || !uatpPaymentRequest.getExpiryDate().matches("\\d{4}-\\d{2}")) {

                    //set expiry date to december of the next year
                    Calendar now = Calendar.getInstance();          // Gets the current date and time
                    int year = now.get(Calendar.YEAR) + 1;        // The current year as an int
                    uatpPaymentRequest.setExpiryDate("" + year + "-12");
                    paymentCard.setExpireDate("12" + year);

                    uatpPaymentRequest.setDefaultExpirationDateUsed(true);
                } else {
                    try {
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
                        Date expiryDate = simpleDateFormat.parse(uatpPaymentRequest.getExpiryDate());
                        simpleDateFormat = new SimpleDateFormat("MMyyyy");
                        String expiryDateStr = simpleDateFormat.format(expiryDate);
                        paymentCard.setExpireDate(expiryDateStr);
                    } catch (ParseException ex) {
                        //set expiry date to december of the next year
                        Calendar now = Calendar.getInstance();          // Gets the current date and time
                        int year = now.get(Calendar.YEAR) + 1;        // The current year as an int
                        uatpPaymentRequest.setExpiryDate("" + year + "-12");
                        paymentCard.setExpireDate("12" + year);

                        uatpPaymentRequest.setDefaultExpirationDateUsed(true);
                    }
                }

                paymentCard.setTrack2(uatpPaymentRequest.getCardCode());
            } else {
                if (null == uatpPaymentRequest.getExpiryDate()
                        || uatpPaymentRequest.getExpiryDate().trim().isEmpty()
                        || !uatpPaymentRequest.getExpiryDate().matches("\\d{4}-\\d{2}")) {

                    //set expiry date to december of the next year
                    Calendar now = Calendar.getInstance();          // Gets the current date and time
                    int year = now.get(Calendar.YEAR) + 1;        // The current year as an int
                    uatpPaymentRequest.setExpiryDate("" + year + "-12");
                    paymentCard.setExpireDate("12" + year);

                    uatpPaymentRequest.setDefaultExpirationDateUsed(true);
                } else {
                    try {
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
                        Date expiryDate = simpleDateFormat.parse(uatpPaymentRequest.getExpiryDate());
                        simpleDateFormat = new SimpleDateFormat("MMyyyy");
                        String expiryDateStr = simpleDateFormat.format(expiryDate);
                        paymentCard.setExpireDate(expiryDateStr);
                    } catch (ParseException ex) {
                        //set expiry date to december of the next year
                        Calendar now = Calendar.getInstance();          // Gets the current date and time
                        int year = now.get(Calendar.YEAR) + 1;        // The current year as an int
                        uatpPaymentRequest.setExpiryDate("" + year + "-12");
                        paymentCard.setExpireDate("12" + year);

                        uatpPaymentRequest.setDefaultExpirationDateUsed(true);
                    }
                }
            }

            MerchantDetailType merchantDetail;
            merchantDetail = new MerchantDetailType();
            merchantDetail.setMerchantID(MERCHANT_ID);
            paymentRQ.setMerchantDetail(merchantDetail);

            paymentCard.setCardNumber(uatpPaymentRequest.getCardCode());
            paymentCard.setCardCode(SabreCreditCardType.TP.getCardCode());

            paymentDetail.setPaymentCard(paymentCard);

            CurrencyValue totalInCart = purchaseOrderRQ.getPurchaseOrder().getPaymentAmount();

            PaymentDetailType.AmountDetail amountDetail = new PaymentDetailType.AmountDetail();
            amountDetail.setCurrencyCode(totalInCart.getCurrencyCode());
            amountDetail.setAmount(CurrencyUtil.getAmount(totalInCart.getTotal(), totalInCart.getCurrencyCode()));

            paymentDetail.setAmountDetail(amountDetail);

            String ref = CommonUtil.createCartId();
            uatpPaymentRequest.setPaymentRef(ref);
            paymentDetail.setPaymentRef(ref);

            paymentsDetailList.add(paymentDetail);

            if (null != paymentRequestCollection) {
                paymentRequestCollection.getCollection().add(new UatpPaymentRequest(uatpPaymentRequest));
            }

            //just one card for now
            break;
        }
    }

    public static AuthorizationResult getAuthorizationResult(String paymentRef, List<AuthorizationResult> authorizationResultList) {
        if (null == paymentRef || paymentRef.trim().isEmpty() || null == authorizationResultList || authorizationResultList.isEmpty()) {
            return null;
        }

        for (AuthorizationResult authorizationResult : authorizationResultList) {
            if (paymentRef.equalsIgnoreCase(authorizationResult.getPaymentId())) {
                return authorizationResult;
            }
        }

        return null;
    }

    /**
     * @param creditCardPaymentRequestList
     * @param purchaseOrderRQ
     * @param paymentRQ
     * @param paymentRequestCollection
     * @throws GenericException
     */
    private void setCreditCardPaymentRQ(
            List<CreditCardPaymentRequest> creditCardPaymentRequestList,
            PurchaseOrderRQ purchaseOrderRQ,
            PaymentRQ paymentRQ,
            PaymentRequestCollection paymentRequestCollection
    ) throws GenericException, IOException, ClassNotFoundException {
        List<PaymentDetailType> paymentsDetailList = paymentRQ.getPaymentDetail();

        for (CreditCardPaymentRequest creditCardPaymentRequest : creditCardPaymentRequestList) {

            PaymentDetailType paymentDetail = new PaymentDetailType();

            FOPType fop = new FOPType();
            fop.setType(SabreFOPType.CC.getCode());
            paymentDetail.setFOP(fop);

            PurchaseOrder purchaseOrder = purchaseOrderRQ.getPurchaseOrder();

            if (null != purchaseOrder.getThreeDSRedirectURL()
                    && ((PosType.WEB.toString().equalsIgnoreCase(purchaseOrderRQ.getPos()) && SystemVariablesUtil.isThreeDsWebEnabled())
                    || (PosType.KIOSK.toString().equalsIgnoreCase(purchaseOrderRQ.getPos()) && SystemVariablesUtil.isThreeDsKioskEnabled())
                    || (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(purchaseOrderRQ.getPos()) && SystemVariablesUtil.isThreeDsAppEnabled()))
                    && STORE_FRONTS_FOR_3DS.contains(purchaseOrderRQ.getStore().toUpperCase())) {

                ReturnURLsType returnURLsType = new ReturnURLsType();
                returnURLsType.setApprovedURL(purchaseOrder.getThreeDSRedirectURL() + "/aproved");
                returnURLsType.setCancelURL(purchaseOrder.getThreeDSRedirectURL() + "/cancel");
                returnURLsType.setDeclinedURL(purchaseOrder.getThreeDSRedirectURL() + "/declined");
                returnURLsType.setErrorURL(purchaseOrder.getThreeDSRedirectURL() + "/error");
                returnURLsType.setExpiredURL(purchaseOrder.getThreeDSRedirectURL() + "/expired");
                returnURLsType.setPendingURL(purchaseOrder.getThreeDSRedirectURL() + "/pending");
                returnURLsType.setDefaultURL(purchaseOrder.getThreeDSRedirectURL() + "/default");

                paymentDetail.setReturnURLs(returnURLsType);
            }

            PaymentDetailType.PaymentCard paymentCard = new PaymentDetailType.PaymentCard();

            //just for kiosk
            if (PosType.KIOSK.toString().equalsIgnoreCase(purchaseOrderRQ.getPos())
                    && !purchaseOrderRQ.getPurchaseOrder().isPinPadNotPresent()) {
                if (null == creditCardPaymentRequest.getCardReader()) {
                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNKNOWN_CODE, "You need to specify the card reader properties");
                }

                PaymentCardType.CardReader cardReader = new PaymentCardType.CardReader();
                cardReader.setChipCardInd(creditCardPaymentRequest.getCardReader().isChipCardInd());
                //cardReader.setDeviceID("");

                if (null != creditCardPaymentRequest.getCardReader().getDeviceType() && !creditCardPaymentRequest.getCardReader().getDeviceType().trim().isEmpty()) {
                    cardReader.setDeviceType(creditCardPaymentRequest.getCardReader().getDeviceType());
                } else {
                    cardReader.setDeviceType(DEVICE_ID);
                }

                if (null == creditCardPaymentRequest.getCardType() || null == creditCardPaymentRequest.getCardCode() || creditCardPaymentRequest.getCardCode().trim().isEmpty()) {
                    PurchaseOrderUtil.setCardCodeByCardPrefix(creditCardPaymentRequest);
                }

                List<TokensType.Token> tokenList = cardReader.getToken();

                for (CardToken cardToken : creditCardPaymentRequest.getCardReader().getCardTokenList()) {
                    TokensType.Token token = new TokensType.Token();
                    token.setName(cardToken.getName());
                    token.setValue(cardToken.getValue());
                    tokenList.add(token);
                }

                paymentCard.setCardReader(cardReader);

                if (null == creditCardPaymentRequest.getExpiryDate()
                        || creditCardPaymentRequest.getExpiryDate().trim().isEmpty()
                        || !creditCardPaymentRequest.getExpiryDate().matches("\\d{4}-\\d{2}")) {

                    //set expiry date to december of the next year
                    Calendar now = Calendar.getInstance();          // Gets the current date and time
                    int year = now.get(Calendar.YEAR) + 1;        // The current year as an int
                    creditCardPaymentRequest.setExpiryDate("" + year + "-12");
                    paymentCard.setExpireDate("12" + year);

                    creditCardPaymentRequest.setDefaultExpirationDateUsed(true);
                } else {
                    try {
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
                        Date expiryDate = simpleDateFormat.parse(creditCardPaymentRequest.getExpiryDate());
                        simpleDateFormat = new SimpleDateFormat("MMyyyy");
                        String expiryDateStr = simpleDateFormat.format(expiryDate);
                        paymentCard.setExpireDate(expiryDateStr);
                    } catch (ParseException ex) {
                        //set expiry date to december of the next year
                        Calendar now = Calendar.getInstance();          // Gets the current date and time
                        int year = now.get(Calendar.YEAR) + 1;        // The current year as an int
                        creditCardPaymentRequest.setExpiryDate("" + year + "-12");
                        paymentCard.setExpireDate("12" + year);

                        creditCardPaymentRequest.setDefaultExpirationDateUsed(true);
                    }
                }

                if (null != creditCardPaymentRequest.getSecurityCode()
                        && !creditCardPaymentRequest.getSecurityCode().trim().isEmpty()) {
                    paymentCard.setCardSecurityCode(creditCardPaymentRequest.getSecurityCode());
                }

                paymentCard.setTrack2(creditCardPaymentRequest.getCcNumber());
            } else {
                paymentCard.setCardSecurityCode(creditCardPaymentRequest.getSecurityCode());

                if (null == creditCardPaymentRequest.getExpiryDate()
                        || creditCardPaymentRequest.getExpiryDate().trim().isEmpty()
                        || !creditCardPaymentRequest.getExpiryDate().matches("\\d{4}-\\d{2}")) {

                    //set expiry date to december of the next year
                    Calendar now = Calendar.getInstance();          // Gets the current date and time
                    int year = now.get(Calendar.YEAR) + 1;        // The current year as an int
                    creditCardPaymentRequest.setExpiryDate("" + year + "-12");
                    paymentCard.setExpireDate("12" + year);

                    creditCardPaymentRequest.setDefaultExpirationDateUsed(true);
                } else {
                    try {
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
                        Date expiryDate = simpleDateFormat.parse(creditCardPaymentRequest.getExpiryDate());
                        simpleDateFormat = new SimpleDateFormat("MMyyyy");
                        String expiryDateStr = simpleDateFormat.format(expiryDate);
                        paymentCard.setExpireDate(expiryDateStr);
                    } catch (ParseException ex) {
                        //set expiry date to december of the next year
                        Calendar now = Calendar.getInstance();          // Gets the current date and time
                        int year = now.get(Calendar.YEAR) + 1;        // The current year as an int
                        creditCardPaymentRequest.setExpiryDate("" + year + "-12");
                        paymentCard.setExpireDate("12" + year);

                        creditCardPaymentRequest.setDefaultExpirationDateUsed(true);
                    }
                }
            }

            MerchantDetailType merchantDetail;
            merchantDetail = new MerchantDetailType();
            merchantDetail.setMerchantID(MERCHANT_ID);
            paymentRQ.setMerchantDetail(merchantDetail);

            if (creditCardPaymentRequest.getCardType() == null) {
                if (creditCardPaymentRequest.getCardCode() != null) {
                    paymentCard.setCardCode(creditCardPaymentRequest.getCardCode());
                } else {
                    PurchaseOrderUtil.setCardCodeByCardPrefix(creditCardPaymentRequest);
                }
            } else {
                switch (creditCardPaymentRequest.getCardType()) {
                    case VISA:
                        paymentCard.setCardCode(SabreCreditCardType.VI.getCardCode());
                        break;
                    case MASTER:
                        paymentCard.setCardCode(SabreCreditCardType.CA.getCardCode());
                        break;
                    case AMEX:
                        paymentCard.setCardCode(SabreCreditCardType.AX.getCardCode());
                        break;
                    default:
                        break;
                }
            }

            paymentCard.setCardNumber(creditCardPaymentRequest.getCcNumber());

            //Not installment for now
            //if (null != creditCardPaymentRequest.getInstallments() && creditCardPaymentRequest.getInstallments() > 0) {
            //paymentCard.setExtendPayment(String.valueOf(creditCardPaymentRequest.getInstallments().intValue()));
            //}
            PaymentCardType.CardHolderName cardHolderName = new PaymentCardType.CardHolderName();
            cardHolderName.setName(creditCardPaymentRequest.getCardHolderName());
            paymentCard.setCardHolderName(cardHolderName);
            paymentDetail.setPaymentCard(paymentCard);

            PaymentDetailType.AmountDetail amountDetail = new PaymentDetailType.AmountDetail();
            amountDetail.setCurrencyCode(creditCardPaymentRequest.getPaymentAmount().getCurrencyCode());
            amountDetail.setAmount(CurrencyUtil.getAmount(creditCardPaymentRequest.getPaymentAmount().getTotal(), creditCardPaymentRequest.getPaymentAmount().getCurrencyCode()));

            paymentDetail.setAmountDetail(amountDetail);

            String ref = CommonUtil.createCartId();
            creditCardPaymentRequest.setPaymentRef(ref);
            paymentDetail.setPaymentRef(ref);

            paymentsDetailList.add(paymentDetail);

            if (null != paymentRequestCollection) {
                paymentRequestCollection.getCollection().add(new CreditCardPaymentRequest(creditCardPaymentRequest));
            }

            //just one card for now
            break;
        }
    }

    private void setVisaCheckoutPaymentRQ(
            List<VisaCheckoutPaymentRequest> visaCheckoutPaymentRequestList,
            PurchaseOrderRQ purchaseOrderRQ,
            PaymentRQ paymentRQ,
            PaymentRequestCollection paymentRequestCollection
    ) throws Exception {
        List<PaymentDetailType> paymentsDetailList = paymentRQ.getPaymentDetail();

        for (VisaCheckoutPaymentRequest visaCheckoutPaymentRequest : visaCheckoutPaymentRequestList) {
//            FormsOfPaymentWrapper formsOfPaymentWrapper = new FormsOfPaymentWrapper(purchaseOrderRQ.getPurchaseOrder().getPaymentInfo().getCollection());

//            visaCheckoutPaymentRequest = formsOfPaymentWrapper.getVisaCheckoutPaymentRequestList().get(0);
            LOGGER.info("VisaCheckOut as form of payment validating {}", purchaseOrderRQ.getPurchaseOrder().getCartId());
            //Get APICardInfo from mongoDB
            APICardInfo apiCardInfo = JSONPrototype.convert(PaymentFacade.findData(purchaseOrderRQ.getPurchaseOrder().getCartId()), APICardInfo.class);

            PaymentInstrument profileCCNumber;
            if (apiCardInfo != null) {
                //Get card info and decoded
                profileCCNumber = JSONPrototype.convert(
                        new String(Base64.decodeBase64(apiCardInfo.getData())),
                        PaymentInstrument.class);

                //promoCode = apiCardInfo.getPromoCode();
            } else {
                throw new Exception("Visacheckout: Unable to find data on DB");
            }

//             paymentRequestCollection = getPaymentInfoVisachekout(profileCCNumber, visaCheckoutPaymentRequest);
            PaymentDetailType paymentDetail = new PaymentDetailType();

            FOPType fop = new FOPType();
            fop.setType(SabreFOPType.CC.getCode());
            paymentDetail.setFOP(fop);

            PaymentDetailType.PaymentCard paymentCard = new PaymentDetailType.PaymentCard();

            paymentCard.setCardSecurityCode(visaCheckoutPaymentRequest.getCvvNumber());

            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
                String expDate = String.valueOf(profileCCNumber.getExpirationDate().getYear()) + "-" + StringUtils.leftPad(String.valueOf(profileCCNumber.getExpirationDate().getMonth()), 2, "0");

                LOGGER.info("Expiration date: " + expDate);

                Date expiryDate = simpleDateFormat.parse(expDate);
                simpleDateFormat = new SimpleDateFormat("MMyyyy");
                String expiryDateStr = simpleDateFormat.format(expiryDate);
                paymentCard.setExpireDate(expiryDateStr);
            } catch (ParseException ex) {
                //set expiry date to december of the next year
                Calendar now = Calendar.getInstance();          // Gets the current date and time
                int year = now.get(Calendar.YEAR) + 1;        // The current year as an int
                profileCCNumber.getExpirationDate().setYear(year);
                profileCCNumber.getExpirationDate().setMonth(12);
                paymentCard.setExpireDate("12" + year);

//                         visaCheckoutPaymentRequest.setDefaultExpirationDateUsed(true);
            }

            MerchantDetailType merchantDetail;
            merchantDetail = new MerchantDetailType();
            merchantDetail.setMerchantID(MERCHANT_ID);
            paymentRQ.setMerchantDetail(merchantDetail);

            paymentCard.setCardCode(SabreCreditCardType.VI.getCardCode());

            paymentCard.setCardNumber(profileCCNumber.getAccountNumber());

            //Not installment for now
            //if (null != creditCardPaymentRequest.getInstallments() && creditCardPaymentRequest.getInstallments() > 0) {
            //paymentCard.setExtendPayment(String.valueOf(creditCardPaymentRequest.getInstallments().intValue()));
            //}
            PaymentCardType.CardHolderName cardHolderName = new PaymentCardType.CardHolderName();
            cardHolderName.setName(profileCCNumber.getNameOnCard());
            paymentCard.setCardHolderName(cardHolderName);
            paymentDetail.setPaymentCard(paymentCard);

            PaymentDetailType.AmountDetail amountDetail = new PaymentDetailType.AmountDetail();
            amountDetail.setCurrencyCode(purchaseOrderRQ.getPurchaseOrder().getPaymentAmount().getCurrencyCode());
            amountDetail.setAmount(CurrencyUtil.getAmount(purchaseOrderRQ.getPurchaseOrder().getPaymentAmount().getTotal(), purchaseOrderRQ.getPurchaseOrder().getPaymentAmount().getCurrencyCode()));

            paymentDetail.setAmountDetail(amountDetail);

            String ref = CommonUtil.createCartId();
            visaCheckoutPaymentRequest.setPaymentRef(ref);
            paymentDetail.setPaymentRef(ref);

            paymentsDetailList.add(paymentDetail);

            if (null != paymentRequestCollection) {
                paymentRequestCollection.getCollection().add(
                        new CreditCardPaymentRequest(
                                getPaymentInfoVisachekout(
                                        profileCCNumber,
                                        visaCheckoutPaymentRequest,
                                        purchaseOrderRQ.getPurchaseOrder().getPaymentAmount()
                                )
                        )
                );
            }

        }
    }

    private CreditCardPaymentRequest getPaymentInfoVisachekout(
            PaymentInstrument profileCCNumber,
            VisaCheckoutPaymentRequest visaCheckoutPaymentRequest,
            CurrencyValue currencyValue
    ) throws Exception {
        if (null == profileCCNumber) {
            throw new Exception("Visacheckout: Unable to find data on DB");
        } else if (null == profileCCNumber.getAccountNumber() || profileCCNumber.getAccountNumber().trim().isEmpty()) {
            throw new Exception("Visacheckout: card number missing");
        } else if (null == profileCCNumber.getPaymentType() || null == profileCCNumber.getPaymentType().getCardBrand() || profileCCNumber.getPaymentType().getCardBrand().trim().isEmpty()) {
            throw new Exception("Visacheckout: card type missing");
        } else if (null == profileCCNumber.getAddress()) {
            throw new Exception("Visacheckout: billing address missing");
        }

        return buildCreditCard(
                profileCCNumber.getEmail(),
                profileCCNumber.getAddress(),
                profileCCNumber.getAccountNumber(),
                profileCCNumber.getNameOnCard(),
                profileCCNumber.getPaymentType().getCardBrand(),
                profileCCNumber.getExpirationDate().getYear() + "",
                profileCCNumber.getExpirationDate().getMonth() + "",
                visaCheckoutPaymentRequest.getCvvNumber(),
                currencyValue
        );
    }

    private CreditCardPaymentRequest buildCreditCard(
            String email,
            Address address,
            String cardNumber,
            String nameOnCard,
            String cardBrandType,
            String year,
            String month,
            String cvvNumber,
            CurrencyValue currencyValue
    ) throws Exception {
        CreditCardPaymentRequest creditCard = new CreditCardPaymentRequest();
        creditCard.setPaymentAmount(currencyValue);

        creditCard.setEmail(email);
        creditCard.setBillingAddress(address);
        creditCard.setCcNumber(cardNumber);
        creditCard.setCardHolderName(nameOnCard);

        String cardType = cardBrandType;
        if ("MASTERCARD".equalsIgnoreCase(cardType)) {
            cardType = "MASTER";
        }

        CreditCardType creditCardType = CreditCardType.getType(cardType);

        if (null == creditCardType) {
            throw new Exception("Invalid card type " + cardBrandType);
        }

        creditCard.setCardType(creditCardType);
        creditCard.setCardCode(creditCardType.getCardCode());
        String sb = StringUtils.leftPad(year, 4, "0")
                + "-"
                + StringUtils.leftPad(month, 2, "0");
        creditCard.setExpiryDate(sb);

        String cvv = (null == cvvNumber || cvvNumber.trim().isEmpty()) ? null : cvvNumber;
        creditCard.setSecurityCode(cvv);

        return creditCard;
    }

    /**
     * @param paymentRQ
     * @param pinPadInitializationRQ
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void setPaymentDetailsForInitialization(PaymentRQ paymentRQ, PinPadInitializationRQ pinPadInitializationRQ) throws IOException, ClassNotFoundException {
        //set payment information
        if (null != pinPadInitializationRQ.getPurchaseOrder().getPaymentInfo()
                && null != pinPadInitializationRQ.getPurchaseOrder().getPaymentInfo().getCollection()
                && !pinPadInitializationRQ.getPurchaseOrder().getPaymentInfo().getCollection().isEmpty()) {

            List<PaymentDetailType> paymentsDetailList = paymentRQ.getPaymentDetail();

            FormsOfPaymentWrapper formsOfPaymentWrapper = new FormsOfPaymentWrapper(
                    pinPadInitializationRQ.getPurchaseOrder().getPaymentInfo()
            );

            for (CreditCardPaymentRequest creditCardPaymentRequest : formsOfPaymentWrapper.getCreditCardPaymentRequestList()) {

                PaymentDetailType paymentDetail = new PaymentDetailType();

                FOPType fop = new FOPType();
                fop.setType(SabreFOPType.CC.getCode());
                paymentDetail.setFOP(fop);

                PaymentDetailType.PaymentCard paymentCard = new PaymentDetailType.PaymentCard();

                //just for kiosk
                if (null == creditCardPaymentRequest.getCardReader()) {
                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNKNOWN_CODE, "You need to specify the card reader properties");
                }

                PaymentCardType.CardReader cardReader = new PaymentCardType.CardReader();
                cardReader.setChipCardInd(creditCardPaymentRequest.getCardReader().isChipCardInd());
                //cardReader.setDeviceID("");

                if (null != creditCardPaymentRequest.getCardReader().getDeviceType() && !creditCardPaymentRequest.getCardReader().getDeviceType().trim().isEmpty()) {
                    cardReader.setDeviceType(creditCardPaymentRequest.getCardReader().getDeviceType());
                } else {
                    cardReader.setDeviceType(DEVICE_ID);
                }

                List<TokensType.Token> tokenList = cardReader.getToken();

                for (CardToken cardToken : creditCardPaymentRequest.getCardReader().getCardTokenList()) {
                    TokensType.Token token = new TokensType.Token();
                    token.setName(cardToken.getName());
                    token.setValue(cardToken.getValue());
                    tokenList.add(token);
                }

                paymentCard.setCardReader(cardReader);

                MerchantDetailType merchantDetail = new MerchantDetailType();
                merchantDetail.setMerchantID(MERCHANT_ID);
                paymentRQ.setMerchantDetail(merchantDetail);

                paymentDetail.setPaymentCard(paymentCard);

                PaymentDetailType.AmountDetail amountDetail = new PaymentDetailType.AmountDetail();
                amountDetail.setCurrencyCode(creditCardPaymentRequest.getPaymentAmount().getCurrencyCode());
                amountDetail.setAmount(CurrencyUtil.getAmount(creditCardPaymentRequest.getPaymentAmount().getTotal(), creditCardPaymentRequest.getPaymentAmount().getCurrencyCode()));

                paymentDetail.setAmountDetail(amountDetail);

                String ref = CommonUtil.createCartId();
                creditCardPaymentRequest.setPaymentRef(ref);
                paymentDetail.setPaymentRef(ref);

                paymentsDetailList.add(paymentDetail);

                //just one card for now
                break;
            }
        }
    }

    /**
     * @param paymentRQ
     * @param pos
     * @param store
     * @param recordLocator
     * @param cartId
     * @param title
     * @param serviceCallList
     * @return
     * @throws Exception
     */
    public PaymentRS getPaymentAuthRS(
            PaymentRQ paymentRQ,
            String pos, String store,
            String recordLocator, String cartId,
            String title, List<ServiceCall> serviceCallList
    ) throws Exception {
        if (null == paymentRQ) {
            return new PaymentRS();
        }

        AuthorizationPaymentConfig authorizationPaymentConfig;
        authorizationPaymentConfig = setUpConfigFactory.getInstance().getAuthorizationPaymentConfig(
                store,
                pos
        );

        String dutyCodeCommand = setUpConfigFactory.getInstance().getDutyCodeCommand();
        String dutyCode = setUpConfigFactory.getInstance().getDutyCode();
        String cityCodeCommand = setUpConfigFactory.getInstance().getCityCodeCommand();
        String cityCode = authorizationPaymentConfig.getCityCode();
        String printerCodeCommand = setUpConfigFactory.getInstance().getTicketPrinterCommand();
        String printerCode = setUpConfigFactory.getInstance().getTicketLNIATA();
        String stationNumber = authorizationPaymentConfig.getStationNumber();

        try {
            PaymentRS paymentRS = paymentService.payment(
                    paymentRQ, dutyCode, dutyCodeCommand,
                    cityCode, cityCodeCommand,
                    printerCode, stationNumber, printerCodeCommand,
                    serviceCallList
            );

            SabrePaymentErrorCodesType code = SabrePaymentErrorCodesType.getType(paymentRS.getResult().getResultCode());

            if (SabrePaymentErrorCodesType.SUCCESS.equals(code)) {
                LOGGER.info(
                        LogUtil.getLogInfo(
                                SERVICE, title, recordLocator, cartId,
                                this.toSring(paymentRQ), this.toSring(paymentRS)
                        )
                );
            } else {
                LOGGER.error(
                        LogUtil.getLogError(
                                SERVICE, title, recordLocator, cartId,
                                this.toSring(paymentRQ), this.toSring(paymentRS)
                        )
                );

                checkErrorsPayment(code, paymentRS, recordLocator, cartId);
            }

            return paymentRS;

        } catch (SabreLayerUnavailableException ex) {
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(pos)) {
                LOGGER.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_22110301,"PaymentService.java:1279", ex.getMessage());
                throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getCode(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getActionable(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getMessage(),
                        Response.Status.INTERNAL_SERVER_ERROR);
            } else {
                LOGGER.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_12110301,"PaymentService.java:1285", ex.getMessage());
                throw ex;
            }
        }catch (SabreEmptyResponseException ser) {
            LOGGER.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110302).toString(), ser.fillInStackTrace());
            throw ser;
        } catch (Exception ex) {
            LOGGER.error(
                    LogUtil.getLogError(
                            SERVICE, title, recordLocator, cartId,
                            this.toSring(paymentRQ), ex
                    )
            );
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNKNOWN_CODE, ex.getMessage());
        }
    }

    /**
     * @param paymentRQ
     * @param recordLocator
     * @param cartId
     * @param title
     * @param serviceCallList
     * @return
     * @throws Exception
     */
    public PaymentRS getEncryptKeyRS(
            PaymentRQ paymentRQ, String recordLocator,
            String cartId, String title,
            List<ServiceCall> serviceCallList
    ) throws Exception {
        if (null == paymentRQ) {
            return new PaymentRS();
        }

        try {
            PaymentRS paymentRS = paymentService.payment(paymentRQ, serviceCallList);

            SabrePaymentErrorCodesType code = SabrePaymentErrorCodesType.getType(paymentRS.getResult().getResultCode());

            if (SabrePaymentErrorCodesType.SUCCESS.equals(code)) {
                LOGGER.info(
                        LogUtil.getLogInfo(
                                SERVICE, title, recordLocator, cartId,
                                this.toSring(paymentRQ), this.toSring(paymentRS)
                        )
                );
            } else {
                LOGGER.error(
                        LogUtil.getLogError(
                                SERVICE, title, recordLocator, cartId,
                                this.toSring(paymentRQ), this.toSring(paymentRS)
                        )
                );
                checkErrorsPayment(code, paymentRS, recordLocator, cartId);
            }

            return paymentRS;

        } catch (Exception ex) {
            LOGGER.error(
                    LogUtil.getLogError(
                            SERVICE, title, recordLocator, cartId,
                            this.toSring(paymentRQ), ex
                    )
            );

            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNKNOWN_CODE, ex.getMessage());
        }
    }

    /**
     * @param code
     * @param paymentRS
     * @param recordLocator
     * @param cartId
     * @throws GenericException
     */
    private void checkErrorsPayment(SabrePaymentErrorCodesType code, PaymentRS paymentRS, String recordLocator, String cartId) throws GenericException {
        String msg = paymentRS.getResult().getDescription();
        String extra;
        if (null != code) {
            extra = code.getCode() + " , " + code.getDescription() + " , ";
        } else {
            extra = paymentRS.getResult().getResultCode();
        }

        LOGGER.error(
                LogUtil.getLogError(
                        SERVICE, "Error calling remote payment service" + msg,
                        recordLocator, cartId
                )
        );

        if (StringUtils.containsIgnoreCase(msg, ErrorType.SSG_ERR_LINK_DOWN_All.getSearchableCode())) {
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.SSG_ERR_LINK_DOWN_All);
        }

        if (StringUtils.containsIgnoreCase(msg, ErrorType.SUPPLIER_NOT_ASSIGNED.getSearchableCode())) {
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.SUPPLIER_NOT_ASSIGNED);
        }

        if (StringUtils.containsIgnoreCase(msg, ErrorType.INVALID_CC_CHECKSUM.getSearchableCode())) {
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.INVALID_CC_CHECKSUM);
        }

        if (StringUtils.containsIgnoreCase(msg, ErrorType.INVALID_CARD_LENGTH.getSearchableCode())) {
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.INVALID_CARD_LENGTH);
        }

        if (StringUtils.containsIgnoreCase(msg, ErrorType.INVALID_EXP_DATE.getSearchableCode())) {
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.INVALID_EXP_DATE);
        }

        throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNKNOWN_CODE, msg, extra);
    }

    /**
     * @param paymentRQ
     * @return
     */
    public String toSring(PaymentRQ paymentRQ) {
        return paymentService.toString(paymentRQ);
    }

    /**
     * @param paymentRS
     * @return
     */
    public String toSring(PaymentRS paymentRS) {
        return paymentService.toString(paymentRS);
    }

    /**
     * @return the setUpConfigFactory
     */
    public CheckInSetUpConfigFactoryWrapper getSetUpConfigFactory() {
        if (null == setUpConfigFactory) {
            setUpConfigFactory = new CheckInSetUpConfigFactoryWrapper();
        }
        return setUpConfigFactory;
    }

    /**
     * @param setUpConfigFactory the setUpConfigFactory to set
     */
    public void setSetUpConfigFactory(CheckInSetUpConfigFactoryWrapper setUpConfigFactory) {
        this.setUpConfigFactory = setUpConfigFactory;
    }
}
