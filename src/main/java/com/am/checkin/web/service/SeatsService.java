package com.am.checkin.web.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;

import com.aeromexico.commons.web.types.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.AbstractAncillary;
import com.aeromexico.commons.model.AbstractSegmentChoice;
import com.aeromexico.commons.model.BenefitCorporateRecognition;
import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.BookedTravelerAncillaryCollection;
import com.aeromexico.commons.model.BookingClass;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.CurrencyValue;
import com.aeromexico.commons.model.PNR;
import com.aeromexico.commons.model.PNRCollection;
import com.aeromexico.commons.model.PaidSegmentChoice;
import com.aeromexico.commons.model.PaidTravelerAncillary;
import com.aeromexico.commons.model.SeatChoice;
import com.aeromexico.commons.model.SeatChoiceUpsell;
import com.aeromexico.commons.model.SeatmapCollection;
import com.aeromexico.commons.model.SeatmapCollectionWrapper;
import com.aeromexico.commons.model.SeatmapSeat;
import com.aeromexico.commons.model.Segment;
import com.aeromexico.commons.model.SegmentChoice;
import com.aeromexico.commons.model.TravelerAncillary;
import com.aeromexico.commons.model.Warning;
import com.aeromexico.commons.model.WarningCollection;
import com.aeromexico.commons.model.utils.CurrencyUtil;

import com.aeromexico.commons.web.util.ErrorCodeDescriptions;
import com.aeromexico.commons.web.util.TimeUtil;
import com.aeromexico.dao.services.IPNRLookupDao;
import com.aeromexico.dao.util.CommonUtil;
import com.aeromexico.sabre.api.acs.AMEditPassCharService;
import com.aeromexico.sabre.api.acs.AMSeatChange;
import com.aeromexico.sabre.api.sabrecommand.AMSabreCommandService;
import com.aeromexico.sabre.api.session.common.ServiceCall;
import com.aeromexico.sabre.api.session.exception.SabreLayerUnavailableException;
import com.aeromexico.sabre.api.sessionpool.factory.SabreSession;
import com.aeromexico.sabre.api.sessionpool.manager.SabreSessionPoolManager;
import com.aeromexico.sharedservices.services.GetReservationService;
import com.am.checkin.web.util.PnrCollectionUtil;
import com.am.checkin.web.util.PurchaseOrderUtil;
import com.am.checkin.web.util.ShoppingCartErrorUtil;
import com.am.checkin.web.util.ShoppingCartUtil;
import com.google.gson.Gson;
import com.sabre.services.acs.bso.checkinpassenger.v3.ACSCheckInPassengerRSACS;
import com.sabre.services.acs.bso.editpassengercharacteristics.v3.ACSEditPassengerCharacteristicsRQACS;
import com.sabre.services.acs.bso.editpassengercharacteristics.v3.ACSEditPassengerCharacteristicsRSACS;
import com.sabre.services.acs.bso.seatchange.v3.ACSSeatChangeRSACS;
import com.sabre.services.acs.bso.seatchange.v3.SeatChangePairACS;
import com.sabre.services.stl.v3.ErrorOrSuccessCode;
import com.sabre.services.stl.v3.FreeTextInfoACS;
import com.sabre.webservices.sabrexml._2003._07.SabreCommandLLSRS;
import java.math.BigDecimal;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.annotation.PostConstruct;

/**
 *
 * @author adrian
 */
@Named
@ApplicationScoped
public class SeatsService {

    private static final Logger LOG = LoggerFactory.getLogger(SeatsService.class);

    private static final Gson GSON = new Gson();
    private static final String SERVICE = "Seats Service";

    @Inject
    private IPNRLookupDao pnrLookupDao;

    @Inject
    private AMEditPassCharService editPassCharService;

    @Inject
    private AEService aeService;

    @Inject
    private AMSeatChange seatChangeService;

    @Inject
    private PnrCollectionService updatePnrCollectionService;

    @Inject
    @Named("CheckInSetUpConfigFactoryWrapper")
    private CheckInSetUpConfigFactoryWrapper setUpConfigFactory;
    
    @Inject
    private AMSabreCommandService sabreCommandService;
    
    @Inject
    private SabreSessionPoolManager sabreSessionPoolManager;
    
    @Inject
    private GetReservationService getReservationService;

    @PostConstruct
    public void init() {
        if (null == setUpConfigFactory) {
            setUpConfigFactory = new CheckInSetUpConfigFactoryWrapper();
        }
    }

    /**
     * @param cartId
     * @return
     * @throws Exception
     * @throws GenericException
     */
    public SeatmapCollection getSeatMapCollection(String cartId) throws Exception, GenericException {

        // read the seatmapCollectionWrapper from the database
        SeatmapCollectionWrapper seatmapCollectionWrapper = pnrLookupDao.readSeatMapCollectionByCartId(cartId);

        if (null == seatmapCollectionWrapper) {
            LOG.error(ErrorType.CART_COLLECTION_OBJECT_EMPTY.getFullDescription() + ": seatmapCollectionWrapper!");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNABLE_TO_RETRIEVE_PRICE_FOR_SEAT,
                    ErrorType.UNABLE_TO_RETRIEVE_PRICE_FOR_SEAT.getFullDescription() + "CartId - " + cartId);
        }
        SeatmapCollection seatmapCollection = seatmapCollectionWrapper.getSeatmapCollection();
        if (null == seatmapCollection) {
            LOG.error(ErrorType.CART_COLLECTION_OBJECT_EMPTY.getFullDescription() + ": seatmapCollection!");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNABLE_TO_RETRIEVE_PRICE_FOR_SEAT,
                    ErrorType.UNABLE_TO_RETRIEVE_PRICE_FOR_SEAT.getFullDescription() + "CartId - " + cartId);
        }
        return seatmapCollection;
    }

    /**
     * @param bookedLeg
     * @param bookedTraveler
     * @param segmentCode
     * @return
     * @throws java.lang.Exception
     */
    public boolean removeFreeSeat(
            BookedLeg bookedLeg, BookedTraveler bookedTraveler, String segmentCode, String pos
    ) throws Exception {
        
        boolean removed;

        AbstractSegmentChoice abstractSegmentChoice = PnrCollectionUtil.getAbstractSegmentChoice(
                segmentCode,
                bookedTraveler.getSegmentChoices().getCollection()
        );

        if (null != abstractSegmentChoice && abstractSegmentChoice instanceof SegmentChoice) {
            SegmentChoice segmentChoice = (SegmentChoice) abstractSegmentChoice;

            String code = null;
            if (segmentChoice.getSeat() instanceof SeatChoice) {
                SeatChoice seatChoice = (SeatChoice) segmentChoice.getSeat();
                code = seatChoice.getCode();
            } else if (segmentChoice.getSeat() instanceof SeatChoiceUpsell) {
                SeatChoiceUpsell seatChoiceUpsell = (SeatChoiceUpsell) segmentChoice.getSeat();
                code = seatChoiceUpsell.getCode();
            }

            Segment segment = null;
            try {
                segment = bookedLeg.getBookedSegment(segmentCode).getSegment();
            } catch (Exception ex) {
                if(PosType.WEB.toString().equalsIgnoreCase(pos)){
                    LOG.error(ErrorType.INVALID_ITINERARY_WEB.getFullDescription() + segmentCode + ex.getMessage());
                }else{
                    LOG.error(ErrorType.INVALID_ITINERARY.getFullDescription() + segmentCode + ex.getMessage());
                }
            }

            BookingClass bookingClass = null;
            try {
                bookingClass = ShoppingCartUtil.getBookingClassMap(bookedTraveler, segmentCode);
            } catch (Exception ex) {
                LOG.error(ex.getMessage());
            }

            // Delete seat
            ACSEditPassengerCharacteristicsRQACS editPassengerCharacteristicsRQ;
            editPassengerCharacteristicsRQ = ShoppingCartUtil.getEditPassengerCharacteristicsRQSegmentChoices(
                    segment,
                    bookingClass, 
                    bookedTraveler, 
                    code, 
                    true
            );
            
            ACSEditPassengerCharacteristicsRSACS editPassengerCharacteristicsRS;
            editPassengerCharacteristicsRS = editPassCharService.editPassengerChararcteristics(editPassengerCharacteristicsRQ);
            
            if (!ErrorOrSuccessCode.SUCCESS.equals(editPassengerCharacteristicsRS.getResult().getStatus())) {
                LOG.error(
                        ErrorCodeDescriptions.MSG_CODE_UNABLE_SEAT
                        + ShoppingCartErrorUtil.getSabreError(editPassengerCharacteristicsRS.getResult())
                        + segmentCode
                );
                
                ShoppingCartErrorUtil.checkErrorPassengerService(editPassengerCharacteristicsRS);
            }

            removed = true;
        } else {
            removed = true;
        }

        return removed;
    }

    /**
     *
     * @param bookedLeg
     * @param bookedTraveler
     * @param recordLocator
     * @param cartId
     * @param ancillaryIDs
     * @param serviceCallList
     * @return
     * @throws Exception
     */
    public boolean removeUnpaidSeatAncillaries(
            BookedLeg bookedLeg, 
            BookedTraveler bookedTraveler, 
            String recordLocator,
            String cartId, 
            Set<Integer> ancillaryIDs, 
            List<ServiceCall> serviceCallList,
            String pos
    ) throws Exception {
        if (null == ancillaryIDs) {
            ancillaryIDs = new HashSet<>();
        }

        boolean removed = false;

        if (null != bookedTraveler.getSeatAncillaries() 
                && null != bookedTraveler.getSeatAncillaries().getCollection()
                && !bookedTraveler.getSeatAncillaries().getCollection().isEmpty()) {

            // Create a copy to avoid changing the original collection
            List<TravelerAncillary> travelerAncillaryList;
            travelerAncillaryList = new ArrayList<>(
                    PurchaseOrderUtil.getUnpaidAncillariesOnReservation(
                            bookedTraveler.getSeatAncillaries().getCollection()
                    )
            );

            if (!travelerAncillaryList.isEmpty()) {
                for (TravelerAncillary travelerAncillary : travelerAncillaryList) {

                    try {
                        // this calls can throw an exception
                        removed = removeFreeSeat(bookedLeg, bookedTraveler, travelerAncillary.getSegmentCodeAux(), pos);

                        if (removed) {
                            ancillaryIDs.add(new Integer(travelerAncillary.getAncillaryId()));

                            removed = aeService.removeSeatAncillaryFromReservation(
                                    bookedTraveler,
                                    travelerAncillary.getSegmentCodeAux(),
                                    recordLocator,
                                    cartId,
                                    serviceCallList
                            );
                        }
                    } catch (Exception ex) {
                        // ignore failed ancillary
                        LOG.error(ex.getMessage());
                    }
                }
            } else {
                LOG.info("There is no unpaid seats ancillaries to be removed");
            }
        } else {
            LOG.info("There is no unpaid seats ancillaries to be removed");
        }

        return removed;
    }

    /**
     *
     * @param bookedTraveler
     * @param bookedTravelerUpdate
     * @param bookedLegCode
     * @param pnrCollection
     * @param cartId
     * @param pos
     * @param serviceCallList
     * @return
     * @throws Exception
     */
    public boolean updateSeatChecked(
            BookedTraveler bookedTraveler, BookedTraveler bookedTravelerUpdate,
            String bookedLegCode, PNRCollection pnrCollection, String cartId, String pos,
            List<ServiceCall> serviceCallList
    ) throws Exception {

        // If the request does not include any seat info, return.
        if (null == bookedTravelerUpdate.getSegmentChoices()
                || null == bookedTravelerUpdate.getSegmentChoices().getCollection()) {
            return false;
        }

        if (ShoppingCartUtil.compareSegmentChoiceCollections(
                bookedTraveler.getSegmentChoices().getCollection(),
                bookedTravelerUpdate.getSegmentChoices().getCollection())) {
            // No update to apply from the request.
            return false;
        }

        PNR pnr = pnrCollection.getPNRByCartId(cartId);

        BookedLeg bookedLeg = pnr.getBookedLegByLegCode(bookedLegCode);
        if (null == bookedLeg) {
            LOG.error(ErrorType.UNKNOWN_LEGCODE.getFullDescription() + " LegCode : " + bookedLegCode + " - PNR: "
                    + pnr.getPnr());
            throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.UNKNOWN_LEGCODE,
                    ErrorType.UNKNOWN_LEGCODE.getFullDescription() + " LegCode : " + bookedLegCode + " - PNR: "
                    + pnr.getPnr());
        }

        boolean updated = false;

        for (AbstractSegmentChoice abstractSegmentChoiceNew : bookedTravelerUpdate.getSegmentChoices().getCollection()) {

            // SegmentChoice is the expected type for put seat
            if (abstractSegmentChoiceNew instanceof SegmentChoice) {

                SegmentChoice segmentChoiceNew = (SegmentChoice) abstractSegmentChoiceNew;
                String segmentCodeNew = segmentChoiceNew.getSegmentCode();

                BookedSegment bookedSegment = bookedLeg.getBookedSegment(segmentCodeNew);
                if (null == bookedSegment || null == bookedSegment.getSegment()) {
                    LOG.error(ErrorCodeDescriptions.MSG_CODE_BOOKEDSEGMENT_NOT_FOUND + " LegCode : " + bookedLegCode
                            + " - PNR : " + pnr.getPnr());
                    throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.BOOKEDSEGMENT_NOT_FOUND,
                            ErrorType.BOOKEDSEGMENT_NOT_FOUND.getFullDescription() + " LegCode : " + bookedLegCode
                            + " - PNR: " + pnr.getPnr());
                }

                BookingClass bookingClass = ShoppingCartUtil.getBookingClassMap(bookedTraveler,
                        bookedSegment.getSegment().getSegmentCode());

                if (null == bookingClass) {
                    LOG.error(ErrorCodeDescriptions.MSG_CODE_ERROR_NOT_FOUND_BOOKINGCLASS_FOR_SEGMENTCODE_IN_SEGMENT
                            + bookedSegment.getSegment().getSegmentCode() + " PASSENGER ID : " + bookedTraveler.getId()
                            + " - PNR: " + pnr.getPnr());
                    throw new GenericException(Response.Status.UNAUTHORIZED,
                            ErrorType.NOT_EXIST_BOOKING_CLASS_IN_PASSENGER,
                            ErrorType.NOT_EXIST_BOOKING_CLASS_IN_PASSENGER.getFullDescription() + " FOR SEGMENTCODE : "
                            + bookedSegment.getSegment().getSegmentCode() + " PAX ID : "
                            + bookedTraveler.getId() + " - PNR :" + pnr.getPnr());
                }

                // New seat
                String newSeatCode = ShoppingCartUtil.getCodeSeat(segmentChoiceNew.getSeat(), pnr.getPnr());

                // Delete seat
                if (null == newSeatCode || newSeatCode.isEmpty()) {
                    return deleteSeatFromShoppingCart(segmentCodeNew, bookedTraveler, pnr);
                }

                // to read old seat
                AbstractSegmentChoice abstractSegmentChoiceOld = ShoppingCartUtil.getAbstractSegmentChoiceBySegmentCode(
                        segmentCodeNew, bookedTraveler.getSegmentChoices().getCollection()
                );

                // if the original seat was moved from the normal collection to
                // the original collection we need to restore that
                if (!bookedTraveler.getOriginalSegmentChoicesMap().getMap().isEmpty()) {
                    ShoppingCartUtil.removeSegmentChoiceAndSeatAncillaryBySegmentCode(
                            segmentCodeNew, bookedTraveler
                    );

                    ShoppingCartUtil.moveAncillariesFromOriginalCollectionToNormalCollection(
                            segmentCodeNew,
                            bookedTraveler
                    );
                }

                if (null == abstractSegmentChoiceOld) {
                    LOG.info(ErrorType.NOT_HAVE_SEAT.getFullDescription() + " PAX ID : " + bookedTraveler.getId()
                            + " - PNR : " + pnr.getPnr());
                    throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.NOT_HAVE_SEAT,
                            ErrorType.NOT_HAVE_SEAT.getFullDescription() + " PAX ID : " + bookedTraveler.getId());
                }

                // old seat
                String oldSeatCode = abstractSegmentChoiceOld.getSeat().getCode();

                // get seatmapCollectionWrapper
                SeatmapCollection seatmapCollection = getSeatMapCollection(cartId);

                // The passenger have a free seat
                if (abstractSegmentChoiceOld instanceof SegmentChoice) {

                    // read the seat map
                    SeatmapSeat seatMapSeatNewFullDB = ShoppingCartUtil.readSeatmapCollectionWrapper(seatmapCollection, bookedLegCode, segmentCodeNew, newSeatCode);

                    // free to free
                    if (segmentChoiceNew.getSeat() instanceof SeatChoice) {
                        updated = changeFreeSeatToFreeSeat(
                                abstractSegmentChoiceOld, seatMapSeatNewFullDB, segmentChoiceNew,
                                bookedLegCode, bookedTraveler, bookedSegment,
                                bookingClass, oldSeatCode, newSeatCode,
                                pnrCollection, cartId, pos, pnr, serviceCallList,
                                pnrCollection.getWarnings()
                        );
                    } // free to Unpaid
                    else if (segmentChoiceNew.getSeat() instanceof SeatChoiceUpsell) {
                        SeatChoiceUpsell seatChoiceUpsell = (SeatChoiceUpsell) segmentChoiceNew.getSeat();

                        if (null == seatChoiceUpsell.getCurrency()
                                || null == seatChoiceUpsell.getCurrency().getTotal()
                                || BigDecimal.ZERO.compareTo(seatChoiceUpsell.getCurrency().getTotal()) >= 0) {

                            segmentChoiceNew.setSeat(new SeatChoice(((SeatChoiceUpsell) segmentChoiceNew.getSeat())));

                            updated = changeFreeSeatToFreeSeat(
                                    abstractSegmentChoiceOld, seatMapSeatNewFullDB, segmentChoiceNew,
                                    bookedLegCode, bookedTraveler, bookedSegment,
                                    bookingClass, oldSeatCode, newSeatCode,
                                    pnrCollection, cartId, pos, pnr, serviceCallList,
                                    pnrCollection.getWarnings()
                            );
                            
                        } else {

                            updated = changeFreeSeatToUnpaidSeat(
                                    seatMapSeatNewFullDB,
                                    segmentChoiceNew, bookedLegCode,
                                    bookedTraveler, pnr.getPnr()
                            );
                        }
                    }

                } else if (abstractSegmentChoiceOld instanceof PaidSegmentChoice) {

                    PaidSegmentChoice paidSegmentChoice = (PaidSegmentChoice) abstractSegmentChoiceOld;

                    // The passenger have a paid seat
                    if (null != paidSegmentChoice.getEmd() && (ActionCodeType.HI.toString()
                            .equalsIgnoreCase(paidSegmentChoice.getActionCode())
                            || ActionCodeType.HK.toString().equalsIgnoreCase(paidSegmentChoice.getActionCode()))) {

                        // bookedTraveler.getSeatAncillaries()
                        // already paid seat
                        SeatmapSeat seatMapSeatNewFullDB = ShoppingCartUtil.readSeatmapCollectionWrapper(
                                seatmapCollection, bookedLegCode, segmentCodeNew, newSeatCode);

                        // paid to free
                        if (segmentChoiceNew.getSeat() instanceof SeatChoice) {
                            updated = changePaidSeatToFreeSeat(
                                    paidSegmentChoice, seatMapSeatNewFullDB,
                                    segmentChoiceNew, bookedLegCode,
                                    bookedTraveler, bookedSegment, bookingClass,
                                    oldSeatCode, newSeatCode, pnrCollection, cartId, pos,
                                    pnrCollection.getWarnings()
                            );
                        } // paid to Unpaid
                        else if (segmentChoiceNew.getSeat() instanceof SeatChoiceUpsell) {
                            updated = changePaidSeatToUnPaidSeat(paidSegmentChoice, seatMapSeatNewFullDB,
                                    segmentChoiceNew, bookedLegCode, bookedTraveler, bookedSegment, bookingClass,
                                    oldSeatCode, newSeatCode, pnrCollection, cartId, pos);
                        }
                    } else { // free seat

                        SeatmapSeat seatmapSeatDB = ShoppingCartUtil.readSeatmapCollectionWrapper(
                                seatmapCollection,
                                bookedLegCode, segmentCodeNew, newSeatCode
                        );

                        // free to free
                        if (segmentChoiceNew.getSeat() instanceof SeatChoice) {
                            updated = changeFreeSeatToFreeSeat(
                                    paidSegmentChoice, seatmapSeatDB, segmentChoiceNew,
                                    bookedLegCode, bookedTraveler, bookedSegment, bookingClass, oldSeatCode,
                                    newSeatCode, pnrCollection, cartId, pos, pnr, serviceCallList,
                                    pnrCollection.getWarnings()
                            );
                        } // free to Unpaid
                        else if (segmentChoiceNew.getSeat() instanceof SeatChoiceUpsell) {
                        	SeatChoiceUpsell seatChoiceUpsell = (SeatChoiceUpsell)segmentChoiceNew.getSeat();
                        	if (pnr.getClid() != null &&
                        			pnr.getClid().getApplyCorporateBenefit() && 
                        			seatChoiceUpsell.getType() == SeatUpgradeType.PREFERRED_UPGRADE) {
                        		updated = changeFreeSeatToFreeSeatBenefitClid(
                                    paidSegmentChoice, seatmapSeatDB, segmentChoiceNew,
                                    bookedLegCode, bookedTraveler, bookedSegment, bookingClass, oldSeatCode,
                                    newSeatCode, pnrCollection, cartId, pos, pnr, serviceCallList,
                                    pnrCollection.getWarnings()
                        				);
                        		if (updated) {
                        			segmentChoiceNew.getSeat().setApplyBenefit(true);
                                	bookedTraveler.getBenefitCorporateRecognition().setSeatCode(newSeatCode);
                                	this.createRemarkByPreferredSeat(
                                			bookedTraveler.getBenefitCorporateRecognition(),
                                            pnr.getPnr(),
                                            cartId,
                                            segmentChoiceNew.getSegmentCode(),
                                            bookedTraveler.getNameRefNumber()
                                            ,pos);
                        		}
                        	} else {
                        		updated = changeFreeSeatToUnpaidSeat(seatmapSeatDB, segmentChoiceNew, bookedLegCode,
                                    bookedTraveler, pnr.getPnr());
                        	}
                        }
                    }
                }
            }
        }

        return updated;
    }

    /**
     *
     * Remove a seat after checkin is only posible when user edit shopping cart
     * in that case the seat to be removed is in the normal collection and it
     * does not have an ancillary associated to it. So we only need to remove
     * this from the collection and restore the original seat if there is
     *
     * @param segmentCodeRQ
     * @param bookedTraveler
     * @param pnr
     * @throws GenericException
     */
    private boolean deleteSeatFromShoppingCart(String segmentCodeRQ, BookedTraveler bookedTraveler, PNR pnr)
            throws GenericException {
        AbstractSegmentChoice abstractSegmentChoiceDB = ShoppingCartUtil.getAbstractSegmentChoiceBySegmentCode(
                segmentCodeRQ, bookedTraveler.getSegmentChoices().getCollection());

        if (null == abstractSegmentChoiceDB) {
            LOG.info(ErrorType.NOT_HAVE_SEAT.getFullDescription() + " PAX ID : " + bookedTraveler.getId() + " - PNR : "
                    + pnr.getPnr());
            throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.NOT_HAVE_SEAT,
                    ErrorType.NOT_HAVE_SEAT.getFullDescription() + " PAX ID : " + bookedTraveler.getId());
        }

        String segmentCode = "";
        if (abstractSegmentChoiceDB instanceof SegmentChoice) {
            SegmentChoice segmentChoice = (SegmentChoice) abstractSegmentChoiceDB;
            segmentCode = segmentChoice.getSegmentCode();
        } else if (abstractSegmentChoiceDB instanceof PaidSegmentChoice) {
            PaidSegmentChoice segmentChoice = (PaidSegmentChoice) abstractSegmentChoiceDB;
            segmentCode = segmentChoice.getSegmentCode();
        }
        boolean removed = ShoppingCartUtil.removeSegmentChoiceAndSeatAncillaryBySegmentCode(segmentCode,
                bookedTraveler);
        boolean restored = ShoppingCartUtil.moveAncillariesFromOriginalCollectionToNormalCollection(segmentCode,
                bookedTraveler);

        return removed || restored;
    }
    
    /**
    *
    * @param abstractSegmentChoiceDB
    * @param seatmapSeatDB
    * @param segmentChoiceUpdate
    * @param bookedLegCode
    * @param bookedTraveler
    * @param bookedSegment
    * @param bookingClass
    * @param oldCodeSeat
    * @param newCodeseat
    * @param pnrCollection
    * @param cartId
    * @param pos
    * @param pnr
    * @param serviceCallList
    * @param warningCollection
    * @return
    * @throws Exception
    */
   private boolean changeFreeSeatToFreeSeatBenefitClid(
           AbstractSegmentChoice abstractSegmentChoiceDB, SeatmapSeat seatmapSeatDB,
           SegmentChoice segmentChoiceUpdate, String bookedLegCode, BookedTraveler bookedTraveler,
           BookedSegment bookedSegment, BookingClass bookingClass, String oldCodeSeat, String newCodeseat,
           PNRCollection pnrCollection, String cartId, String pos, PNR pnr, List<ServiceCall> serviceCallList,
           WarningCollection warningCollection
   ) throws Exception {
       boolean updated = false;

       if (seatmapSeatDB.getChoice() instanceof SeatChoiceUpsell) {
           if (segmentChoiceUpdate.getSeat() instanceof SeatChoiceUpsell) {
        	   SeatChoiceUpsell seatChoiceUpsellDB = (SeatChoiceUpsell) seatmapSeatDB.getChoice();
        	   SeatChoice seatChoiceDB = new SeatChoice();
        	   seatChoiceDB.setApplyBenefit(true);
        	   seatChoiceDB.setCode(seatChoiceUpsellDB.getCode());
        	   seatChoiceDB.setSeatCharacteristics(seatChoiceUpsellDB.getSeatCharacteristics());
        	   seatChoiceDB.setSeatFacilities(seatChoiceUpsellDB.getSeatFacilities());

               SegmentChoice segmentChoice = new SegmentChoice();
               seatChoiceDB.setCode(seatChoiceDB.getCode());
               segmentChoice.setSeat(seatChoiceDB);
               segmentChoice.setSegmentCode(segmentChoiceUpdate.getSegmentCode());

               // if the seat is free, change code seat in Sabre
               ACSSeatChangeRSACS acsSeatChangeRSACS = changeSeatChecked(
                       bookedSegment, bookingClass,
                       oldCodeSeat, newCodeseat
               );
               // sameData if the change was success
               if (!ErrorOrSuccessCode.SUCCESS.equals(acsSeatChangeRSACS.getResult().getStatus())) {
                   LOG.error(ErrorType.ERROR_TO_CHANGE_SEAT.getFullDescription() + " PASSENGER ID : "
                           + bookedTraveler.getId() + " - PNR : " + pnr.getPnr());
                   throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.ERROR_TO_CHANGE_SEAT,
                           ErrorType.ERROR_TO_CHANGE_SEAT.getFullDescription()
                           + acsSeatChangeRSACS.getResult().getStatus() + " PASSENGER ID : "
                           + bookedTraveler.getId() + " - PNR : " + pnr.getPnr());
               }

               // remove AE if exist
               deleteAEAfterCheckin(abstractSegmentChoiceDB, bookedTraveler, pnr, cartId, serviceCallList);

               // add new free Seat
               int indexS = bookedTraveler.getSegmentChoices()
                       .getIndexBySegmentCode(segmentChoiceUpdate.getSegmentCode());

               // set new orinigal segment choice
               if (indexS >= 0) {
                   bookedTraveler.getSegmentChoices().getCollection().set(indexS, segmentChoice);
               } else {
                   bookedTraveler.getSegmentChoices().getCollection().add(segmentChoice);
               }

               updated = true;

               // Update boardingPass
               UpdateAfterCheckInCollectionInDB(
                       pnrCollection, bookedTraveler,
                       bookedLegCode, acsSeatChangeRSACS,
                       pos,
                       cartId,
                       warningCollection
               );

           }
       }

       return updated;
   }

    /**
     *
     * @param abstractSegmentChoiceDB
     * @param seatmapSeatDB
     * @param segmentChoiceUpdate
     * @param bookedLegCode
     * @param bookedTraveler
     * @param bookedSegment
     * @param bookingClass
     * @param oldCodeSeat
     * @param newCodeseat
     * @param pnrCollection
     * @param cartId
     * @param pos
     * @param pnr
     * @param serviceCallList
     * @param warningCollection
     * @return
     * @throws Exception
     */
    private boolean changeFreeSeatToFreeSeat(
            AbstractSegmentChoice abstractSegmentChoiceDB, SeatmapSeat seatmapSeatDB,
            SegmentChoice segmentChoiceUpdate, String bookedLegCode, BookedTraveler bookedTraveler,
            BookedSegment bookedSegment, BookingClass bookingClass, String oldCodeSeat, String newCodeseat,
            PNRCollection pnrCollection, String cartId, String pos, PNR pnr, List<ServiceCall> serviceCallList,
            WarningCollection warningCollection
    ) throws Exception {
        boolean updated = false;

        if (seatmapSeatDB.getChoice() instanceof SeatChoice) {
            if (segmentChoiceUpdate.getSeat() instanceof SeatChoice) {

                SeatChoice seatChoiceDB = (SeatChoice) seatmapSeatDB.getChoice();

                SegmentChoice segmentChoice = new SegmentChoice();
                seatChoiceDB.setCode(seatChoiceDB.getCode());
                segmentChoice.setSeat(seatChoiceDB);
                segmentChoice.setSegmentCode(segmentChoiceUpdate.getSegmentCode());

                // if the seat is free, change code seat in Sabre
                ACSSeatChangeRSACS acsSeatChangeRSACS = changeSeatChecked(
                        bookedSegment, bookingClass,
                        oldCodeSeat, newCodeseat
                );
                // sameData if the change was success
                if (!ErrorOrSuccessCode.SUCCESS.equals(acsSeatChangeRSACS.getResult().getStatus())) {
                    LOG.error(ErrorType.ERROR_TO_CHANGE_SEAT.getFullDescription() + " PASSENGER ID : "
                            + bookedTraveler.getId() + " - PNR : " + pnr.getPnr());
                    throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.ERROR_TO_CHANGE_SEAT,
                            ErrorType.ERROR_TO_CHANGE_SEAT.getFullDescription()
                            + acsSeatChangeRSACS.getResult().getStatus() + " PASSENGER ID : "
                            + bookedTraveler.getId() + " - PNR : " + pnr.getPnr());
                }

                // remove AE if exist
                deleteAEAfterCheckin(abstractSegmentChoiceDB, bookedTraveler, pnr, cartId, serviceCallList);

                // add new free Seat
                int indexS = bookedTraveler.getSegmentChoices()
                        .getIndexBySegmentCode(segmentChoiceUpdate.getSegmentCode());

                // set new orinigal segment choice
                if (indexS >= 0) {
                    bookedTraveler.getSegmentChoices().getCollection().set(indexS, segmentChoice);
                } else {
                    bookedTraveler.getSegmentChoices().getCollection().add(segmentChoice);
                }

                updated = true;

                // Update boardingPass
                UpdateAfterCheckInCollectionInDB(
                        pnrCollection, bookedTraveler,
                        bookedLegCode, acsSeatChangeRSACS,
                        pos,
                        cartId,
                        warningCollection
                );

            }
        }

        return updated;
    }

    /**
     *
     * @param seatMapSeatNewFullDB
     * @param segmentChoiceNew
     * @param bookedLegCode
     * @param bookedTraveler
     * @throws Exception
     */
    private boolean changeFreeSeatToUnpaidSeat(SeatmapSeat seatMapSeatNewFullDB,
            SegmentChoice segmentChoiceNew,
            String bookedLegCode, BookedTraveler bookedTraveler,
            String pnr
    ) throws Exception {

        boolean updated = false;

        if (seatMapSeatNewFullDB.getChoice() instanceof SeatChoiceUpsell) {
            if (segmentChoiceNew.getSeat() instanceof SeatChoiceUpsell) {

                SeatChoiceUpsell seatChoiceUpsellNewFullDB = (SeatChoiceUpsell) seatMapSeatNewFullDB.getChoice();
                segmentChoiceNew.setSeat(seatChoiceUpsellNewFullDB);
                // save the old seat to a temporary collection
                boolean added = ShoppingCartUtil.FindAndSaveOriginalSeat(bookedTraveler,
                        segmentChoiceNew.getSegmentCode());
                // only add the original ancillary if the original segment
                // choice was added
                if (added) {
                    ShoppingCartUtil.FindAndSaveOriginalAncillarySeat(bookedTraveler,
                            segmentChoiceNew.getSegmentCode());
                }

                // remove the seat from the collection
                boolean removed = ShoppingCartUtil.removeSegmentChoiceAndSeatAncillaryBySegmentCode(
                        segmentChoiceNew.getSegmentCode(), bookedTraveler);

                if (segmentChoiceNew.getSeat() instanceof SeatChoiceUpsell) {
//                        || (segmentChoiceNew.getSeat() instanceof SeatChoice
//                        && ((SeatChoice) segmentChoiceNew.getSeat()).isSubjectToSeatSelectionFee())) {

                    String seatCode = ShoppingCartUtil.getCodeSeat(segmentChoiceNew.getSeat(), pnr);

                    if (null == seatCode || seatCode.trim().isEmpty()) {
                        seatCode = "00";
                    }

                    // just if the passenger have some benefit
                    if (!ShoppingCartUtil.isPaymentRequired(bookedTraveler, seatChoiceUpsellNewFullDB.getType())) {
                        SeatChoiceUpsell seatChoiceUpsell = (SeatChoiceUpsell) segmentChoiceNew.getSeat();
                        seatChoiceUpsell.setCurrency(
                                CurrencyUtil.getCurrencyValue(seatChoiceUpsell.getCurrency().getCurrencyCode()));
                        seatChoiceUpsell.setApplyBenefit(true);
                    }
                    TravelerAncillary travelerAncillary = PurchaseOrderUtil.createSeatAncillaryEntryForPaidSeats(
                            bookedTraveler, segmentChoiceNew, bookedLegCode, seatCode);

                    complementaryTravelerAncillary(segmentChoiceNew, bookedTraveler, travelerAncillary);
                }

                // add new free Seat
                bookedTraveler.getSegmentChoices().getCollection().add(segmentChoiceNew);

                updated = added || removed;
            }

            return updated;
        } else {
            throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.BAD_REQUEST_SEAT,
                    ErrorType.BAD_REQUEST_SEAT.getFullDescription() + " SeatChoiceUpsell");
        }
    }

    private void complementaryTravelerAncillary(SegmentChoice segmentChoiceNew, BookedTraveler bookedTraveler,
            TravelerAncillary travelerAncillary) {
        if (null != travelerAncillary) {
            segmentChoiceNew.setActionCode(travelerAncillary.getActionCode());
            segmentChoiceNew.setAncillaryId(travelerAncillary.getAncillaryId());
            segmentChoiceNew.setLinkedID(travelerAncillary.getLinkedID());
            segmentChoiceNew.setPartOfReservation(travelerAncillary.isPartOfReservation());
            segmentChoiceNew.setNameNumber(travelerAncillary.getNameNumber());

            bookedTraveler.getSeatAncillaries().getCollection().add(travelerAncillary);
        }
    }

    /**
     *
     * @param paidSegmentChoice
     * @param seatmapSeatDB
     * @param segmentChoiceUpdate
     * @param bookedLegCode
     * @param bookedTraveler
     * @param bookedSegment
     * @param bookingClass
     * @param oldCodeSeat
     * @param newCodeseat
     * @param pnrCollection
     * @param cartId
     * @param pos
     * @return
     */
    private boolean changePaidSeatToFreeSeat(
            PaidSegmentChoice paidSegmentChoice, SeatmapSeat seatmapSeatDB,
            SegmentChoice segmentChoiceUpdate, String bookedLegCode, BookedTraveler bookedTraveler,
            BookedSegment bookedSegment, BookingClass bookingClass, String oldCodeSeat, String newCodeseat,
            PNRCollection pnrCollection, String cartId, String pos,
            WarningCollection warningCollection
    ) throws Exception {

        boolean updated = false;

        PNR pnr = pnrCollection.getPNRByCartId(cartId);

        if (seatmapSeatDB.getChoice() instanceof SeatChoice) {

            if (segmentChoiceUpdate.getSeat() instanceof SeatChoice) {

                ACSSeatChangeRSACS acsSeatChangeRSACS = changeSeatChecked(
                        bookedSegment, bookingClass,
                        oldCodeSeat, newCodeseat
                );

                // sameData if the change was success
                if (!acsSeatChangeRSACS.getResult().getStatus().equals(ErrorOrSuccessCode.SUCCESS)) {
                    LOG.error(ErrorType.ERROR_TO_CHANGE_SEAT.getFullDescription() + " PASSENGER ID : "
                            + bookedTraveler.getId() + " - PNR : " + pnr.getPnr());
                    throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.ERROR_TO_CHANGE_SEAT,
                            ErrorType.ERROR_TO_CHANGE_SEAT.getFullDescription()
                            + acsSeatChangeRSACS.getResult().getStatus() + " PASSENGER ID : "
                            + bookedTraveler.getId() + " - PNR : " + pnr.getPnr());
                }

                updated = true;

                paidSegmentChoice.getSeat().setCode(newCodeseat);
                paidSegmentChoice.getSeat().setSeatCharacteristics(seatmapSeatDB.getChoice().getSeatCharacteristics());

                // Update boardingPass
                UpdateAfterCheckInCollectionInDB(
                        pnrCollection,
                        bookedTraveler,
                        bookedLegCode,
                        acsSeatChangeRSACS,
                        pos,
                        cartId,
                        warningCollection
                );

            }

        }

        return updated;
    }

    private boolean changePaidSeatToUnPaidSeat(PaidSegmentChoice paidSegmentChoice, SeatmapSeat seatmapSeatDB,
            SegmentChoice segmentChoiceRQ, String bookedLegCode, BookedTraveler bookedTraveler,
            BookedSegment bookedSegment, BookingClass bookingClass, String oldCodeSeat, String newCodeseat,
            PNRCollection pnrCollection, String cartId, String pos) throws Exception {

        boolean updated = false;
        PNR pnr = pnrCollection.getPNRByCartId(cartId);
        if (seatmapSeatDB.getChoice() instanceof SeatChoiceUpsell) {
            if (segmentChoiceRQ.getSeat() instanceof SeatChoiceUpsell) {
                //get the present seat
                PaidTravelerAncillary paidTravelerAncillary = getPaidTravelerAncillaryByCodeSeat(paidSegmentChoice.getLinkedID(), bookedTraveler.getSeatAncillaries());
                if (null == paidTravelerAncillary) {
                    throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.NOT_FOUNT_SEAT_ANCILLARY, ErrorType.NOT_FOUNT_SEAT_ANCILLARY.getFullDescription());
                }

                //is the same type and price seat?
                boolean samePrice = getSamePriceAndType(seatmapSeatDB, paidTravelerAncillary);
                if (samePrice) {
                    ACSSeatChangeRSACS acsSeatChangeRSACS = changeSeatChecked(bookedSegment,
                            bookingClass, oldCodeSeat, newCodeseat);

                    if (!acsSeatChangeRSACS.getResult().getStatus().equals(ErrorOrSuccessCode.SUCCESS)) {
                        LOG.error(ErrorType.ERROR_TO_CHANGE_SEAT.getFullDescription() + " PASSENGER ID : "
                                + bookedTraveler.getId() + " - PNR : " + pnr.getPnr());
                        throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.ERROR_TO_CHANGE_SEAT,
                                ErrorType.ERROR_TO_CHANGE_SEAT.getFullDescription()
                                + acsSeatChangeRSACS.getResult().getStatus() + " PASSENGER ID : "
                                + bookedTraveler.getId() + " - PNR : " + pnr.getPnr());
                    }
                    updated = true;

                    paidSegmentChoice.getSeat().setCode(newCodeseat);
                    paidSegmentChoice.getSeat()
                            .setSeatCharacteristics(seatmapSeatDB.getChoice().getSeatCharacteristics());

                    // update codeseat in ancillary paid
                    // TODO
                    // Update boardingPass
                    UpdateAfterCheckInCollectionInDB(
                            pnrCollection,
                            bookedTraveler,
                            bookedLegCode,
                            acsSeatChangeRSACS,
                            pos,
                            cartId,
                            pnrCollection.getWarnings()
                    );

                    if (!acsSeatChangeRSACS.getResult().getStatus().equals(ErrorOrSuccessCode.SUCCESS)) {
                        LOG.error(ErrorType.ERROR_TO_CHANGE_SEAT.getFullDescription() + " PASSENGER ID : "
                                + bookedTraveler.getId() + " - PNR : " + pnr.getPnr());
                        throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.ERROR_TO_CHANGE_SEAT,
                                ErrorType.ERROR_TO_CHANGE_SEAT.getFullDescription()
                                + acsSeatChangeRSACS.getResult().getStatus() + " PASSENGER ID : "
                                + bookedTraveler.getId() + " - PNR : " + pnr.getPnr());
                    }
                    updated = true;

                    paidSegmentChoice.getSeat().setCode(newCodeseat);
                    paidSegmentChoice.getSeat().setSeatCharacteristics(seatmapSeatDB.getChoice().getSeatCharacteristics());

                    //update codeseat in ancillary paid
                    //TODO
                    // Update boardingPass
                    UpdateAfterCheckInCollectionInDB(
                            pnrCollection,
                            bookedTraveler,
                            bookedLegCode,
                            acsSeatChangeRSACS,
                            pos,
                            cartId,
                            pnrCollection.getWarnings()
                    );

                } else {
                    changeFreeSeatToUnpaidSeat(seatmapSeatDB, segmentChoiceRQ, bookedLegCode, bookedTraveler, pnr.getPnr());
                }
            }
        }

        return updated;

    }

    private boolean getSamePriceAndType(SeatmapSeat seatmapSeatDB, PaidTravelerAncillary paidTravelerAncillary) {
        SeatChoiceUpsell seatChoiceUpsellDB = (SeatChoiceUpsell) seatmapSeatDB.getChoice();
        if (seatChoiceUpsellDB.getCurrency().getCurrencyCode().equalsIgnoreCase(paidTravelerAncillary.getTotalPrice().getCurrencyCode())) {
            if (seatChoiceUpsellDB.getCurrency().getTotal().equals(paidTravelerAncillary.getTotalPrice().getTotal())) {
                return true;
            }
        }
        return false;
    }

    private PaidTravelerAncillary getPaidTravelerAncillaryByCodeSeat(String linkedID,
            BookedTravelerAncillaryCollection seatAncillaries) {
        PaidTravelerAncillary paidTravelerAncillary = null;
        for (AbstractAncillary abstractAncillary : seatAncillaries.getCollection()) {
            if (abstractAncillary instanceof PaidTravelerAncillary) {
                paidTravelerAncillary = (PaidTravelerAncillary) abstractAncillary;
                if (paidTravelerAncillary.getLinkedID().equalsIgnoreCase(linkedID)) {
                    return paidTravelerAncillary;
                }
//    			if(paidTravelerAncillary.getSeatCode().equalsIgnoreCase(StringUtils.leftPad(oldCodeSeat, 3, '0'))){
//    				return paidTravelerAncillary;
//    			}
            }
        }
        return null;
    }

    /**
     *
     * @param bookedTraveler
     * @param bookedTravelerUpdate
     * @param bookedLegCode
     * @param pnrCollection
     * @param cartId
     * @param store
     * @param warningCollection
     * @param serviceCallList
     * @return
     * @throws Exception
     */
    public boolean updateSeat(
            BookedTraveler bookedTraveler,
            BookedTraveler bookedTravelerUpdate,
            String bookedLegCode,
            PNRCollection pnrCollection,
            String cartId,
            String store,
            WarningCollection warningCollection,
            List<ServiceCall> serviceCallList,
            String pos
    ) throws Exception {

        boolean updated = false;

        // reset
        if (null == warningCollection) {
            warningCollection = new WarningCollection();
        }

        // is there a seat in the shopping cart request?
        if (null == bookedTravelerUpdate.getSegmentChoices()
                || null == bookedTravelerUpdate.getSegmentChoices().getCollection()
                || bookedTravelerUpdate.getSegmentChoices().getCollection().isEmpty()) {
            return false;
        }

        // Check if the information is different
        boolean sameData = ShoppingCartUtil.compareSegmentChoiceCollections(
                bookedTraveler.getSegmentChoices().getCollection(),
                bookedTravelerUpdate.getSegmentChoices().getCollection()
        );

        // Seat was changed
        if (!sameData) {

            PNR pnr = pnrCollection.getPNRByCartId(cartId);

            BookedLeg bookedLeg = pnr.getBookedLegByLegCode(bookedLegCode);
            if (null == bookedLeg) {
                LOG.error(ErrorType.UNKNOWN_LEGCODE.getFullDescription() + " LegCode: " + bookedLegCode
                        + " - PNR: " + pnr.getPnr());
                throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.UNKNOWN_LEGCODE,
                        ErrorType.UNKNOWN_LEGCODE.getFullDescription() + " LegCode: " + bookedLegCode
                                + " - PNR: " + pnr.getPnr());
            }

            if (PassengerFareType.E.toString().equalsIgnoreCase(pnr.getCartPNRByCartId(cartId).getTravelerInfo().getCollection().get(0).getPassengerType())
                    && bookedLeg.isStandByReservation()) {
                warningCollection.addToList(new Warning(ErrorType.STB_PAX_CANNOT_SELECT_SEAT));
                return false;
            }

            for (AbstractSegmentChoice abstractSegmentChoiceNew : bookedTravelerUpdate.getSegmentChoices().getCollection()) {

                // SegmentChoice is the expected type for put seat
                if (abstractSegmentChoiceNew instanceof SegmentChoice) {

                    SegmentChoice segmentChoiceUpdate = (SegmentChoice) abstractSegmentChoiceNew;
                    String segmentCode = segmentChoiceUpdate.getSegmentCode();

                    AbstractSegmentChoice abstractSegmentChoiceDB = ShoppingCartUtil.getAbstractSegmentChoiceBySegmentCode(
                            segmentCode, bookedTraveler.getSegmentChoices().getCollection()
                    );

                    boolean prePaidSeat = false;

                    if (null != abstractSegmentChoiceDB && abstractSegmentChoiceDB instanceof PaidSegmentChoice) {
                        PaidSegmentChoice paidSegmentChoice = (PaidSegmentChoice) abstractSegmentChoiceDB;
                        if (null != paidSegmentChoice.getEmd() && (ActionCodeType.HI.toString()
                                .equalsIgnoreCase(paidSegmentChoice.getActionCode())
                                || ActionCodeType.HK.toString().equalsIgnoreCase(paidSegmentChoice.getActionCode()))) {
                            prePaidSeat = true;
                        }
                    }

                    BookedSegment bookedSegment = pnr.getBookedSegmentByLegcodeAndSegmentcode(bookedLegCode, segmentChoiceUpdate.getSegmentCode());
                    if (null == bookedSegment || null == bookedSegment.getSegment()) {
                        LOG.error(ErrorCodeDescriptions.MSG_CODE_BOOKEDSEGMENT_NOT_FOUND + " LegCode : " + bookedLegCode
                                + " - PNR: " + pnr.getPnr());
                        throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.BOOKEDSEGMENT_NOT_FOUND,
                                ErrorType.BOOKEDSEGMENT_NOT_FOUND.getFullDescription() + " LegCode : " + bookedLegCode
                                + " - PNR: " + pnr.getPnr());
                    }

                    BookingClass bookingClass = ShoppingCartUtil.getBookingClassMap(bookedTraveler, bookedSegment.getSegment().getSegmentCode());
                    if (null == bookingClass) {
                        LOG.error(ErrorCodeDescriptions.MSG_CODE_ERROR_NOT_FOUND_BOOKINGCLASS_FOR_SEGMENTCODE_IN_SEGMENT
                                + bookedSegment.getSegment().getSegmentCode() + " PASSENGER ID : "
                                + bookedTraveler.getId() + " - PNR : " + pnr.getPnr());
                        throw new GenericException(Response.Status.UNAUTHORIZED,
                                ErrorType.NOT_EXIST_BOOKING_CLASS_IN_PASSENGER,
                                ErrorType.NOT_EXIST_BOOKING_CLASS_IN_PASSENGER.getFullDescription()
                                + " FOR SEGMENTCODE : " + bookedSegment.getSegment().getSegmentCode()
                                + " PASSENGER ID : " + bookedTraveler.getId() + " - PNR : " + pnr.getPnr());
                    }

                    // code new seat
                    String newSeatCode = ShoppingCartUtil.getCodeSeat(segmentChoiceUpdate.getSeat(), pnr.getPnr());
                    if (null == newSeatCode || newSeatCode.isEmpty()) {
                        updated = deleteSeat(
                                abstractSegmentChoiceDB, 
                                bookedTraveler, 
                                bookedSegment, 
                                bookingClass, 
                                pnr, 
                                cartId, 
                                warningCollection, 
                                serviceCallList
                        );
                    } else {
                        updated = addSeat(
                                segmentChoiceUpdate, 
                                bookedLegCode, 
                                abstractSegmentChoiceDB, 
                                bookedTraveler,
                                bookedSegment, 
                                bookedLeg, 
                                bookingClass, 
                                pnr, 
                                cartId, 
                                store, 
                                prePaidSeat, 
                                newSeatCode,
                                warningCollection, 
                                serviceCallList,
                                pos
                        );
                    }

                } // ignore paid seats
            }
        }

        return updated;
    }

    /**
     *
     * @param abstractSegmentChoiceDB
     * @param bookedTraveler
     * @param bookedSegment
     * @param bookingClass
     * @param pnr
     * @param cartId
     * @param serviceCallList
     * @return
     * @throws Exception
     */
    private boolean deleteSeat(
            AbstractSegmentChoice abstractSegmentChoiceDB, 
            BookedTraveler bookedTraveler,
            BookedSegment bookedSegment, 
            BookingClass bookingClass, 
            PNR pnr, 
            String cartId,
            WarningCollection warningCollection, 
            List<ServiceCall> serviceCallList
    ) throws Exception {

        boolean deleted = false;

        LOG.info("+deleteSeat({}, {})", cartId, bookedTraveler.getId());

        // reset
        if (null == warningCollection) {
            warningCollection = new WarningCollection();
        }

        if (null == abstractSegmentChoiceDB) {

            LOG.info(ErrorType.NOT_HAVE_SEAT_TO_DELETE.getFullDescription() + " PAX ID : " + bookedTraveler.getId() + " - PNR : " + pnr.getPnr());
            // Warning warning = new Warning();
            // warning.setCode(ErrorType.NOT_HAVE_SEAT_TO_DELETE.getErrorCode());
            // warning.setMsg(ErrorType.NOT_HAVE_SEAT_TO_DELETE.getFullDescription()
            // + " FOR PASSENGER ID : " + bookedTraveler.getId());
            // warningCollection.addToList(warning);

        } else {

            String seat = ShoppingCartUtil.getCodeSeat(abstractSegmentChoiceDB.getSeat(), pnr.getPnr());

            // delete AE
            deleteAE(abstractSegmentChoiceDB, bookedTraveler, pnr, cartId, serviceCallList);

            boolean delete = true;

            // delete the seat code on Sabre
            ACSEditPassengerCharacteristicsRQACS editPassengerCharacteristicsRQ;
            editPassengerCharacteristicsRQ = ShoppingCartUtil.getEditPassengerCharacteristicsRQSegmentChoices(
                    bookedSegment.getSegment(), bookingClass, bookedTraveler, seat, delete
            );

            ACSEditPassengerCharacteristicsRSACS editPassengerCharacteristicsRS;
            editPassengerCharacteristicsRS = editPassCharService.editPassengerChararcteristics(editPassengerCharacteristicsRQ);

            // sameData if the change was success
            if (!ErrorOrSuccessCode.SUCCESS.equals(editPassengerCharacteristicsRS.getResult().getStatus())) {

                // LOG.error("ACSEditPassengerCharacteristicsRQACS: " +
                // editPassCharService.toString(editPassengerCharacteristicsRQ));
                // LOG.error("ACSEditPassengerCharacteristicsRSACS: " +
                // editPassCharService.toString(editPassengerCharacteristicsRS));
                LOG.error(ErrorType.PRE_RESERVED_SEAT_NOT_SUCCESSFUL.getErrorCode() + " FOR PASSENGER ID : "
                        + bookedTraveler.getId()
                        + ShoppingCartErrorUtil.getSabreError(editPassengerCharacteristicsRS.getResult())
                        + " SEGMENTCODE : " + abstractSegmentChoiceDB.getSegmentCode());

                Warning warning = new Warning(ErrorType.PRE_RESERVED_SEAT_NOT_SUCCESSFUL);
                warning.setMsg(ErrorType.PRE_RESERVED_SEAT_NOT_SUCCESSFUL.getFullDescription() + " FOR PASSENGER ID : "
                        + bookedTraveler.getId());
                warningCollection.addToList(warning);

            }

            deleted = true;
        }

        return deleted;
    }

    /**
     *
     * @param segmentChoiceUpdate
     * @param bookedLegCode
     * @param abstractSegmentChoiceDB
     * @param bookedTraveler
     * @param bookedSegment
     * @param bookedLeg
     * @param bookingClass
     * @param pnr
     * @param cartId
     * @param store
     * @param prePaidSeat
     * @param newSeatCode
     * @param warningCollection
     * @param serviceCallList
     * @throws GenericException
     * @throws Exception
     */
    private boolean addSeat(
            SegmentChoice segmentChoiceUpdate,
            String bookedLegCode,
            AbstractSegmentChoice abstractSegmentChoiceDB,
            BookedTraveler bookedTraveler,
            BookedSegment bookedSegment,
            BookedLeg bookedLeg, BookingClass bookingClass, PNR pnr,
            String cartId, String store, boolean prePaidSeat,
            String newSeatCode,
            WarningCollection warningCollection,
            List<ServiceCall> serviceCallList,
            String pos
    ) throws GenericException, Exception {

        boolean added = false;

        // reset
        if (null == warningCollection) {
            warningCollection = new WarningCollection();
        }
        SeatmapCollection seatmapCollection = getSeatMapCollection(cartId);

        SeatmapSeat seatmapSeatDB = ShoppingCartUtil.readSeatmapCollectionWrapper(
                seatmapCollection, bookedLegCode,
                segmentChoiceUpdate.getSegmentCode(),
                newSeatCode
        );

        if (null == seatmapSeatDB) {
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNABLE_TO_RETRIVE_SEATMAP,
                    ErrorType.UNABLE_TO_RETRIVE_SEATMAP.getFullDescription());
        }
        
        boolean reservationToApplyPreferredSeat = false;

        if (seatmapSeatDB.getChoice() instanceof SeatChoice) {
            SeatChoice seatChoiceDB = (SeatChoice) seatmapSeatDB.getChoice();
            if (segmentChoiceUpdate.getSeat() instanceof SeatChoice) {
                segmentChoiceUpdate.setSeat(seatChoiceDB);
                // segmentChoiceUpdate.setSeatSelectionFee(seatMapDB.getSeatSelectionFee());
            } else {
                throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.BAD_REQUEST_SEAT,
                        ErrorType.BAD_REQUEST_SEAT.getFullDescription() + "SeatChoice");
            }
        } else if (seatmapSeatDB.getChoice() instanceof SeatChoiceUpsell) {
            SeatChoiceUpsell seatChoiceUpsellDB = (SeatChoiceUpsell) seatmapSeatDB.getChoice();
            if (segmentChoiceUpdate.getSeat() instanceof SeatChoiceUpsell) {
                // segmentChoiceUpdate.setSeatSelectionFee(seatMapDB.getSeatSelectionFee());
                segmentChoiceUpdate.setSeat(seatChoiceUpsellDB);

                // just if the passenger have some benefit
                SeatChoiceUpsell seatChoiceUpsell = (SeatChoiceUpsell) segmentChoiceUpdate.getSeat();
                if (!ShoppingCartUtil.isPaymentRequired(bookedTraveler, seatChoiceUpsellDB.getType())) {
                    seatChoiceUpsellDB.setCurrency(
                            CurrencyUtil.getCurrencyValue(seatChoiceUpsell.getCurrency().getCurrencyCode())
                    );
                    seatChoiceUpsellDB.setApplyBenefit(true);
                }
                
                CurrencyValue currencyValue = seatChoiceUpsellDB.getCurrency();
                // Benefit by corporate recognition
                if (null != currencyValue && BigDecimal.ZERO.compareTo(currencyValue.getTotal()) == 0
                		&& seatChoiceUpsell.getType() == SeatUpgradeType.PREFERRED_UPGRADE) {
                	seatChoiceUpsell.setApplyBenefit(true);
                    segmentChoiceUpdate.setSeat(seatChoiceUpsell);
                    reservationToApplyPreferredSeat  = true;
                    LOG.info("Se va aplicar beneficio corporativo de asiento");
                    // TODO: GENERAR ANCILLARY PARA DAR ASIENTO GRATIS
                }

                // if (null != seatMapDB.getSeatSelectionFee()) {
                // if (!ShoppingCartUtil.isPaymentRequired(bookedTraveler,
                // seatChoiceUpsellDB.getType())) {
                // SeatChoiceUpsell seatChoiceUpsell = (SeatChoiceUpsell)
                // segmentChoiceUpdate.getSeat();
                // seatChoiceUpsell.setApplyBenefit(true);
                // segmentChoiceUpdate.setSeatSelectionFee(
                // ShoppingCartUtil.getCurrencyValue(seatChoiceUpsell.getCurrency().getCurrencyCode()));
                // }
                // }
            }
        }

        if (null != bookedTraveler.getInfant() && ShoppingCartUtil.isExitRow(segmentChoiceUpdate)) {

            LOG.error(ErrorType.EXIT_ROW.getFullDescription() + " - PASSENGER ID : " + bookedTraveler.getId()
                    + " - SEGMENTCODE : " + segmentChoiceUpdate.getSegmentCode() + " - PNR : " + pnr.getPnr());

            // checkErrorPassengerService(editPassengerCharacteristicsRS);
            Warning warning = new Warning();
            warning.setCode(ErrorType.EXIT_ROW.getErrorCode());
            warning.setMsg(ErrorType.EXIT_ROW.getFullDescription() + " - PASSENGER ID : " + bookedTraveler.getId()
                    + "- SEGMENTCODE : " + segmentChoiceUpdate.getSegmentCode() + " - PNR : " + pnr.getPnr());
            warningCollection.addToList(warning);

        } else {

            // Add seat
            ACSEditPassengerCharacteristicsRQACS editPassengerCharacteristicsRQ;
            editPassengerCharacteristicsRQ = ShoppingCartUtil.getEditPassengerCharacteristicsRQSegmentChoices(
                    bookedSegment.getSegment(), bookingClass, bookedTraveler, newSeatCode, false);
            ACSEditPassengerCharacteristicsRSACS editPassengerCharacteristicsRS;
            editPassengerCharacteristicsRS = editPassCharService
                    .editPassengerChararcteristics(editPassengerCharacteristicsRQ);

            // sameData if the change was success
            if (!ErrorOrSuccessCode.SUCCESS.equals(editPassengerCharacteristicsRS.getResult().getStatus())) {

                LOG.error(ErrorType.PRE_RESERVED_SEAT_NOT_SUCCESSFUL.getFullDescription() + " FOR PASSENGER ID : "
                        + bookedTraveler.getId() + " SEGMENTCODE : " + segmentChoiceUpdate.getSegmentCode()
                        + " - PNR : " + pnr.getPnr());

                Warning warning = new Warning();
                warning.setCode(ErrorType.PRE_RESERVED_SEAT_NOT_SUCCESSFUL.getErrorCode());
                warning.setMsg(ErrorType.PRE_RESERVED_SEAT_NOT_SUCCESSFUL.getFullDescription() + " FOR PASSENGER ID : "
                        + bookedTraveler.getId() + " SEGMENTCODE : " + segmentChoiceUpdate.getSegmentCode()
                        + " - PNR : " + pnr.getPnr());
                warningCollection.addToList(warning);

            } else {
                added = true;

                if (prePaidSeat) {
                    // LOG.info("prePaidSeat seat exists");
                    abstractSegmentChoiceDB.getSeat().setCode(newSeatCode);
                } else {
                    // LOG.info("prePaidSeat seat does not exists");
                    // edit seat code on Sabre
                    deleteAE(abstractSegmentChoiceDB, bookedTraveler, pnr, cartId, serviceCallList);

                    bookedTraveler.getSegmentChoices().getCollection().add(segmentChoiceUpdate);

                    // create the AE code for paid seat
                    boolean isAfterChecking = false;
                    createAE(
                            segmentChoiceUpdate, bookedTraveler,
                            bookedLegCode, bookedLeg, pnr, cartId, store,
                            isAfterChecking, serviceCallList, pos
                    );
                    
                    // save remark to benefit preferred seats
                    if (null != bookedTraveler.getBenefitCorporateRecognition()
                            && bookedTraveler.getBenefitCorporateRecognition().isPreferredSeatsBenefit() && reservationToApplyPreferredSeat) {
                       
                    	bookedTraveler.getBenefitCorporateRecognition().setSeatCode(newSeatCode);
                    	this.createRemarkByPreferredSeat(
                    			bookedTraveler.getBenefitCorporateRecognition(),
                                pnr.getPnr(),
                                cartId,
                                segmentChoiceUpdate.getSegmentCode(),
                                bookedTraveler.getNameRefNumber()
                                ,pos);
                    }

                    // This was added as a fix to a Sabre problem. The fix is to
                    // call the assign seat again after an AE is created:
                    editPassengerCharacteristicsRS = editPassCharService.editPassengerChararcteristics(editPassengerCharacteristicsRQ);
                    // LOG.info("SECOND CALL TO
                    // ACSEditPassengerCharacteristicsRSACS: " +
                    // editPassCharService.toString(editPassengerCharacteristicsRS));

                    if (!ErrorOrSuccessCode.SUCCESS.equals(editPassengerCharacteristicsRS.getResult().getStatus())) {
                        //ignore
                        LOG.error(editPassengerCharacteristicsRS.getResult().getStatus().toString());
                    }
                }
            }
        }

        return added;
    }
    
    /**
     * Add remark in reservation by preferred seat benefit.
     * p.e. PSEATS 5000000000967 10B 24062020
     */
    private void createRemarkByPreferredSeat(
            BenefitCorporateRecognition benefitCorporateRecognition,
            String pnr,
            String cartId,
            String segmentCode, 
            String NameRefNumber,
            String pos)
    {
        try {
            if (null != benefitCorporateRecognition) {
                String remarkText = "3OSI YY PSEATS BY CORPORATE PRIORITY"
                        .concat(" ")
                        .concat(null != benefitCorporateRecognition.getClid() ? benefitCorporateRecognition.getClid() : "")
                        .concat(" ")
                        .concat(null != segmentCode ? segmentCode.substring(0,7).replace("_", "") : "") //MEX_CUN_
                        .concat(" ")
                        .concat(null != benefitCorporateRecognition.getSeatCode() ? benefitCorporateRecognition.getSeatCode() : "")
                        .concat(" ");

                LocalDate today = LocalDate.now();
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("ddMMyyyy");
                remarkText = remarkText.concat(today.format(formatter));
                try {
                    CeateRemarkOSI(remarkText,pnr,cartId,pos);
                } catch (SabreLayerUnavailableException ex) {
                    LOG.error(ex.getMessage(), ex);
                } catch (Exception ex) {
                    LOG.error("Update Reservation: Error adding remark preferred seat, "
                                    + pnr + "-" + cartId,
                            ex.getMessage()
                    );
                }
            }
        } catch (Exception e) {
            LOG.error("Update Reservation: Error adding remark preferred seat, "
                    + pnr + "-" + cartId,
                    e.getMessage()
            );
        }
    }
    
    /**
     * Add login CP information
     * @param actualBookedTraveler
     * @param updateBookedTraveler 
     */
    
    private void CeateRemarkOSI(String command, String pnr, String cartId, String pos) throws Exception {
        try {
            //SabreSession sabreSession = updateReservationService.getSession();
        	SabreSession sabreSession = sabreSessionPoolManager.acquireSession();
            String token = sabreSession.getSessionId();
            //getReservationSabreService.getReservationStateful(token, pnr, cartId, pos);
            getReservationService.getReservationStateful(
                  sabreSession.getSessionId(), pnr, cartId);

            SabreCommandLLSRS sabreCommandLLSRS = sabreCommandService.sabreCommand(token, command, "SCREEN");
            LOG.info(pnr, cartId, "Command Response: " + command, new Gson().toJson(sabreCommandLLSRS), false);
            if (sabreCommandLLSRS.getResponse().contains("*")) {
                command = "6MANAGE_API";
                sabreCommandLLSRS = sabreCommandService.sabreCommand(token, command, "SCREEN");
                LOG.info(pnr, cartId, "Command Response: " + command, new Gson().toJson(sabreCommandLLSRS), false);

                command = "E";
                sabreCommandLLSRS = sabreCommandService.sabreCommand(token, command, "SCREEN");
                LOG.info(pnr, cartId, "Command Response: " + command, new Gson().toJson(sabreCommandLLSRS), false);

                if (!sabreCommandLLSRS.getResponse().trim().startsWith("OK")) {
                    command = "I";
                    sabreCommandService.sabreCommand(token, command, "SCREEN");
                    LOG.info(pnr, cartId, "Command Response: " + command, new Gson().toJson(sabreCommandLLSRS), true);
                }
            } else {
                command = "I";
                sabreCommandService.sabreCommand(token, command, "SCREEN");
                LOG.info(pnr, cartId, "Command Response: " + command, new Gson().toJson(sabreCommandLLSRS), true);
            }

            sabreSessionPoolManager.closeSession(sabreSession);
        } catch (Exception ex) {
            LOG.error("Remove Voucher", "Error executing command to remove remark emd", pnr, cartId, ex);
            sabreCommandService.sabreCommand("I", "SCREEN");
        }
    }

    /**
     *
     * @param segmentChoiceUpdate
     * @param bookedTraveler
     * @param bookedLegCode
     * @param bookedLeg
     * @param pnr
     * @param cartId
     * @param store
     * @param afterChecking
     * @param serviceCallList
     * @throws Exception
     */
    private void createAE(
            SegmentChoice segmentChoiceUpdate,
            BookedTraveler bookedTraveler,
            String bookedLegCode,
            BookedLeg bookedLeg,
            PNR pnr,
            String cartId,
            String store,
            boolean afterChecking,
            List<ServiceCall> serviceCallList,
            String pos
    ) throws Exception {

        if (afterChecking) {
            CartPNR cartPNR = pnr.getCartPNRByCartId(cartId);
            String currencyCode;
            try {
                currencyCode = cartPNR.getMeta().getTotal().getCurrency().getCurrencyCode();
            } catch (Exception ex) {
                // default currency code
                currencyCode = CurrencyType.MXN.toString();
            }

            String seatCode = segmentChoiceUpdate.getSeat().getCode();
            if (null == seatCode || seatCode.trim().isEmpty()) {
                seatCode = "00";
            }

            String aircraftType = null;
            try {

                BookedSegment bookedSegment = PurchaseOrderUtil.getBookedSegment(
                        bookedLeg.getSegments().getCollection(), segmentChoiceUpdate.getSegmentCode());

                if (null != bookedSegment) {
                    aircraftType = bookedSegment.getSegment().getAircraftType();
                }

            } catch (Exception ex) {
                //
            }

            TravelerAncillary travelerAncillary = PurchaseOrderUtil.createSeatAncillaryEntryForUnreservedSeats(
                    bookedTraveler.getNameRefNumber(),
                    segmentChoiceUpdate.getSegmentCode(),
                    bookedLeg.getSegments().getLegCode(),
                    currencyCode,
                    seatCode,
                    aircraftType
            );

            if (null != travelerAncillary) {

                List<BookedTraveler> bookedTravelerList = new ArrayList<>();
                bookedTravelerList.add(bookedTraveler);

                aeService.addAncillariesToReservationForUnreservedSeat(bookedTravelerList, bookedLeg, currencyCode,
                        pnr.getPnr(), cartId, store, serviceCallList, pos);

                segmentChoiceUpdate.setActionCode(travelerAncillary.getActionCode());
                segmentChoiceUpdate.setAncillaryId(travelerAncillary.getAncillaryId());
                segmentChoiceUpdate.setLinkedID(travelerAncillary.getLinkedID());
                segmentChoiceUpdate.setPartOfReservation(travelerAncillary.isPartOfReservation());
                segmentChoiceUpdate.setNameNumber(travelerAncillary.getNameNumber());

                bookedTraveler.getSeatAncillaries().getCollection().add(travelerAncillary);
            }
        } else {
            if (segmentChoiceUpdate.getSeat() instanceof SeatChoiceUpsell
                    || segmentChoiceUpdate.getSeat() instanceof SeatChoice ) {
//                    && ((SeatChoice) segmentChoiceUpdate.getSeat()).isSubjectToSeatSelectionFee())) {

                String seatCode = ShoppingCartUtil.getCodeSeat(segmentChoiceUpdate.getSeat(), pnr.getPnr());

                if (null == seatCode || seatCode.trim().isEmpty()) {
                    seatCode = "00";
                }

                TravelerAncillary travelerAncillary;
                travelerAncillary = PurchaseOrderUtil.createSeatAncillaryEntryForPaidSeats(
                        bookedTraveler,
                        segmentChoiceUpdate,
                        bookedLegCode,
                        seatCode
                );

                if (null != travelerAncillary) {

                    aeService.addNewAncillaryToReservation(bookedTraveler, travelerAncillary, bookedLeg, pnr.getPnr(),
                            cartId, store, serviceCallList, pos);

                    segmentChoiceUpdate.setActionCode(travelerAncillary.getActionCode());
                    segmentChoiceUpdate.setAncillaryId(travelerAncillary.getAncillaryId());
                    segmentChoiceUpdate.setLinkedID(travelerAncillary.getLinkedID());
                    segmentChoiceUpdate.setPartOfReservation(travelerAncillary.isPartOfReservation());
                    segmentChoiceUpdate.setNameNumber(travelerAncillary.getNameNumber());
                    
                    if (segmentChoiceUpdate.getSeat() instanceof SeatChoiceUpsell ){
                        SeatChoiceUpsell seatChoiceUpsell = (SeatChoiceUpsell) segmentChoiceUpdate.getSeat();
                        if (seatChoiceUpsell.isApplyBenefit()) {

                            travelerAncillary.setApplyBenefit(true);
                            PaidSegmentChoice paidSegmentChoice = segmentChoiceUpdate.transformToPaidSegmentChoice();

                            int index = bookedTraveler.getSegmentChoices()
                                    .getIndexBySegmentCode(paidSegmentChoice.getSegmentCode());
                            if (index >= 0) {
                                bookedTraveler.getSegmentChoices().getCollection().set(index, paidSegmentChoice);
                            }
                        }
                    }
                    
                    bookedTraveler.getSeatAncillaries().getCollection().add(travelerAncillary);
                    if (travelerAncillary.isApplyBenefit()) {

                        PaidTravelerAncillary paidTravelerAncillary = travelerAncillary.transformToPaidAncillary();

                        if (null != paidTravelerAncillary) {
                            int index = bookedTraveler.getSeatAncillaries().getIndexByStoredId(paidTravelerAncillary.getStoredId());
                            if (index >= 0) {
                                bookedTraveler.getSeatAncillaries().getCollection().set(index, paidTravelerAncillary);
                            }
                        }
                    }

                }
            }
        }

    }

    /**
     *
     * @param abstractSegmentChoiceDB
     * @param bookedTraveler
     * @param pnr
     * @param cartId
     * @param serviceCallList
     * @throws Exception
     */
    private void deleteAE(
            AbstractSegmentChoice abstractSegmentChoiceDB, 
            BookedTraveler bookedTraveler, 
            PNR pnr,
            String cartId, 
            List<ServiceCall> serviceCallList
    ) throws Exception {

        // remove AE from the shopping cart database
        if (null == abstractSegmentChoiceDB) {
            LOG.info("There is no seat in DB");
            return;
        }

        if (abstractSegmentChoiceDB instanceof SegmentChoice) {

            // LOG.info("delete AE SegmentChoice");
            SegmentChoice segmentChoice = (SegmentChoice) abstractSegmentChoiceDB;

            // has an associated ancillary
            if (null != segmentChoice.getEmd()
                    && (ActionCodeType.HI.toString().equalsIgnoreCase(segmentChoice.getActionCode())
                    || ActionCodeType.HK.toString().equalsIgnoreCase(segmentChoice.getActionCode()))) {

                LOG.error(ErrorType.CHANGE_PAID_SEAT_NOT_ALLOWED.getFullDescription() + " PASSENGER ID : "
                        + bookedTraveler.getId() + " PNR : " + pnr.getPnr());
                throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.CHANGE_PAID_SEAT_NOT_ALLOWED,
                        ErrorType.CHANGE_PAID_SEAT_NOT_ALLOWED.getFullDescription() + " PASSENGER ID : "
                        + bookedTraveler.getId() + " PNR : " + pnr.getPnr());
            }

            if (ActionCodeType.HD.toString().equalsIgnoreCase(segmentChoice.getActionCode())
                    && null != segmentChoice.getAncillaryId() && !segmentChoice.getAncillaryId().trim().isEmpty()) {

                // remove AE code from sabre
                // this method will also remove the entry for DB
                aeService.removeSeatAncillaryFromReservation(bookedTraveler, segmentChoice.getSegmentCode(), pnr.getPnr(), cartId, serviceCallList);
            } else {
                // this segment choice does not have an associated ancillary
                // remove
                ShoppingCartUtil.removeSegmentChoiceAndSeatAncillaryBySegmentCode(segmentChoice.getSegmentCode(), bookedTraveler);
                ShoppingCartUtil.moveAncillariesFromOriginalCollectionToNormalCollection(segmentChoice.getSegmentCode(), bookedTraveler);
            }

        } else if (abstractSegmentChoiceDB instanceof PaidSegmentChoice) {

            // LOG.info("delete AE PaidSegmentChoice");
            PaidSegmentChoice paidSegmentChoice = (PaidSegmentChoice) abstractSegmentChoiceDB;

            boolean existAncillaryEntry = PurchaseOrderUtil.existAncillaryEntryBySegmentCode(
                    paidSegmentChoice.getSegmentCode(), bookedTraveler.getSeatAncillaries().getCollection());

            // for seats that does not have an associated ancillary to them
            if (null == paidSegmentChoice.getActionCode() && (null == paidSegmentChoice.getAncillaryId()
                    || paidSegmentChoice.getAncillaryId().trim().isEmpty())) {

                ShoppingCartUtil.removeSegmentChoiceAndSeatAncillaryBySegmentCode(paidSegmentChoice.getSegmentCode(), bookedTraveler);

                // for free seat that have an ancillary without EMD number (only
                // for paid)
            } else if (null == paidSegmentChoice.getEmd()
                    && (ActionCodeType.HI.toString().equalsIgnoreCase(paidSegmentChoice.getActionCode())
                    || ActionCodeType.HK.toString().equalsIgnoreCase(paidSegmentChoice.getActionCode()))
                    && existAncillaryEntry) {

                // remove AE code from sabre this method will also remove the
                // entry for DB
                aeService.removeSeatPaidTravelerAncillaryFromReservation(bookedTraveler,
                        paidSegmentChoice.getSegmentCode(), pnr.getPnr(), cartId, serviceCallList);
            } else {
                LOG.error(ErrorType.CHANGE_PAID_SEAT_NOT_ALLOWED.getFullDescription() + " PASSENGER ID : "
                        + bookedTraveler.getId() + " PNR : " + pnr.getPnr());
                throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.CHANGE_PAID_SEAT_NOT_ALLOWED,
                        ErrorType.CHANGE_PAID_SEAT_NOT_ALLOWED.getFullDescription() + " PASSENGER ID : "
                        + bookedTraveler.getId() + " PNR : " + pnr.getPnr());
            }
        }

    }

    /**
     *
     * @param abstractSegmentChoiceDB
     * @param bookedTraveler
     * @param pnr
     * @param cartId
     * @param serviceCallList
     * @throws Exception
     */
    private void deleteAEAfterCheckin(AbstractSegmentChoice abstractSegmentChoiceDB, BookedTraveler bookedTraveler,
            PNR pnr, String cartId, List<ServiceCall> serviceCallList) throws Exception {

        // remove AE from the shopping cart database
        if (null == abstractSegmentChoiceDB) {
            LOG.info("There is not seat in DB");
            return;
        }

        if (abstractSegmentChoiceDB instanceof SegmentChoice) {

            // LOG.info("delete AE SegmentChoice");
            SegmentChoice segmentChoice = (SegmentChoice) abstractSegmentChoiceDB;

            // has an associated ancillary
            if (null != segmentChoice.getEmd()
                    && (ActionCodeType.HI.toString().equalsIgnoreCase(segmentChoice.getActionCode())
                    || ActionCodeType.HK.toString().equalsIgnoreCase(segmentChoice.getActionCode()))) {

                LOG.error(ErrorType.CHANGE_PAID_SEAT_NOT_ALLOWED.getFullDescription() + " PASSENGER ID : "
                        + bookedTraveler.getId() + " PNR : " + pnr.getPnr());
            } else if (null != segmentChoice.getActionCode() && !segmentChoice.getActionCode().isEmpty()
                    && null != segmentChoice.getAncillaryId() && !segmentChoice.getAncillaryId().trim().isEmpty()) {
                // remove AE code from sabre
                // this method will also remove the entry for DB
                aeService.removeSeatAncillaryFromReservation(bookedTraveler, segmentChoice.getSegmentCode(),
                        pnr.getPnr(), cartId, serviceCallList);
            } else {
                // this segment choice does not have an associated ancillary
                // remove
                ShoppingCartUtil.removeSegmentChoiceAndSeatAncillaryBySegmentCode(segmentChoice.getSegmentCode(),
                        bookedTraveler);
                ShoppingCartUtil.moveAncillariesFromOriginalCollectionToNormalCollection(segmentChoice.getSegmentCode(),
                        bookedTraveler);
            }

        } else if (abstractSegmentChoiceDB instanceof PaidSegmentChoice) {

            // LOG.info("delete AE PaidSegmentChoice");
            PaidSegmentChoice segmentChoice = (PaidSegmentChoice) abstractSegmentChoiceDB;

            if (null != segmentChoice.getEmd()
                    && (ActionCodeType.HI.toString().equalsIgnoreCase(segmentChoice.getActionCode())
                    || ActionCodeType.HK.toString().equalsIgnoreCase(segmentChoice.getActionCode()))) {

                LOG.error(ErrorType.CHANGE_PAID_SEAT_NOT_ALLOWED.getFullDescription() + " PASSENGER ID : "
                        + bookedTraveler.getId() + " PNR : " + pnr.getPnr());
            } else if (null != segmentChoice.getActionCode() && !segmentChoice.getActionCode().isEmpty()
                    && null != segmentChoice.getAncillaryId() && !segmentChoice.getAncillaryId().trim().isEmpty()) {
                // remove AE code from sabre
                // this method will also remove the entry for DB
                aeService.removeSeatPaidTravelerAncillaryFromReservation(bookedTraveler, segmentChoice.getSegmentCode(),
                        pnr.getPnr(), cartId, serviceCallList);
            } else {
                // this segment choice does not have an associated ancillary
                // remove
                ShoppingCartUtil.removeSegmentChoiceAndSeatAncillaryBySegmentCode(segmentChoice.getSegmentCode(),
                        bookedTraveler);
                ShoppingCartUtil.moveAncillariesFromOriginalCollectionToNormalCollection(segmentChoice.getSegmentCode(),
                        bookedTraveler);
            }
        }
    }

    /**
     *
     * @param pnrCollection
     * @param bookedTraveler
     * @param bookedLegCode
     * @param acsSeatChangeRSACS
     * @param pos
     * @param cartId
     * @param warningCollection
     * @throws Exception
     */
    private void UpdateAfterCheckInCollectionInDB(
            PNRCollection pnrCollection, BookedTraveler bookedTraveler,
            String bookedLegCode, ACSSeatChangeRSACS acsSeatChangeRSACS,
            String pos, String cartId,
            WarningCollection warningCollection
    ) throws Exception {

        PNR pnr = pnrCollection.getPNRByCartId(cartId);

        // update boardingPass
        PNRCollection pnrCollectionAfterCheckin;
        try {
            pnrCollectionAfterCheckin = pnrLookupDao.readPNRCollectionByRecordLocatorAfterCheckin(pnr.getPnr());

            if (null == pnrCollectionAfterCheckin) {
                LOG.error("pnrCollectionAfterCheckin NOT FOUND to update in afterCheckIn - PNR : " + pnr.getPnr());

                pnrCollectionAfterCheckin = new PNRCollection(pnrCollection.toString());

                // Save pnr After checkin
                LOG.info("Saving collection after checkin because it does not exist");
                pnrLookupDao.savePNRCollectionAfterCheckin(pnrCollection);
            } else {
                LOG.info("Collection is null");
            }

        } catch (Exception ex) {
            // sameData for Document not found
            if (null != ex.getMessage() && (ex.getMessage().contains(CommonUtil.getMessageByCode("001"))
                    || ex.getMessage().contains(CommonUtil.getMessageByCode("012")))) {

                pnrCollectionAfterCheckin = new PNRCollection(pnrCollection.toString());

                // Save pnr After checkin
                LOG.info("Saving collection after checkin because it does not exist");
                pnrLookupDao.savePNRCollectionAfterCheckin(pnrCollection);
            } else {
                throw ex;
            }
        }

        CartPNR cartPNR = pnrCollectionAfterCheckin.getCartPNRByLegCode(bookedLegCode);
        if (null == cartPNR) {
            LOG.error("CartPNR NOT FOUND in pnrCollectionAfterCheckin - PNR: " + pnr.getPnr() + "BOOKEDLEGCODE"
                    + bookedLegCode);
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION,
                    "PnrCollectionAfterCheckin NOT FOUND - PNR : " + pnr.getPnr());
        }

        BookedLeg bookedLeg = pnr.getBookedLegByLegCode(bookedLegCode);
        if (null == bookedLeg) {
            LOG.error("BookedLeg NOT FOUND in pnrCollectionAfterCheckin - PNR: " + pnr.getPnr() + "BOOKEDLEGCODE"
                    + bookedLegCode);
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION,
                    "PnrCollectionAfterCheckin NOT FOUND - PNR : " + pnr.getPnr());
        }

        ACSCheckInPassengerRSACS acsCheckInPassengerRSACS;
        acsCheckInPassengerRSACS = PnrCollectionUtil.transformCheckInPassengerRS(acsSeatChangeRSACS);

        List<ACSCheckInPassengerRSACS> acsCheckInPassengerRSACSList = new ArrayList<>();
        acsCheckInPassengerRSACSList.add(acsCheckInPassengerRSACS);

        Set<BookedTraveler> bookedTravelerList = new HashSet<>();
        bookedTravelerList.add(bookedTraveler);

        PnrCollectionUtil.updateAfterSuccessCheckin(
                bookedTravelerList, acsCheckInPassengerRSACSList,
                cartPNR.getTravelerInfo().getCollection(),
                bookedLeg,
                pos,
                pnr.getPnr(),
                pnr.isTsaRequired(),
                warningCollection
        );

        // update pectabBoardingPassList
        ShoppingCartUtil.UpdatePectabInPnrCollectionAfterCheckIn(cartPNR, acsSeatChangeRSACS);

        // Store/write pnrCollection back to the database
        updatePnrCollectionService.updatePnrCollectionAfterCheckin(pnrCollectionAfterCheckin, false);
    }

    /**
     *
     * @param segment
     * @param bookingClass
     * @return
     */
    private com.sabre.services.acs.bso.seatchange.v3.ItineraryACS getItinerarySeatChange(Segment segment, BookingClass bookingClass) {

        com.sabre.services.acs.bso.seatchange.v3.ItineraryACS itinerary = new com.sabre.services.acs.bso.seatchange.v3.ItineraryACS();

        itinerary.setAirline(segment.getOperatingCarrier());
        itinerary.setFlight(segment.getOperatingFlightCode());
        itinerary.setBookingClass(bookingClass.getBookingClass());
        try {
            itinerary.setDepartureDate(TimeUtil.getStrTime(segment.getDepartureDateTime(), "yyyy-MM-dd'T'HH:mm:ss", "yyyy-MM-dd"));
        } catch (ParseException ex) {
            try {
                itinerary.setDepartureDate(segment.getDepartureDateTime().substring(0, segment.getDepartureDateTime().indexOf("T")));

            } catch (Exception ex1) {
                //
            }
        }
        itinerary.setOrigin(segment.getDepartureAirport());
        itinerary.setDestination(segment.getArrivalAirport());

        return itinerary;
    }

    /**
     *
     * @param bookedSegment
     * @param bookingClass
     * @param oldCodeSeat
     * @param newCodeseat
     * @return
     * @throws Exception
     */
    public ACSSeatChangeRSACS changeSeatChecked(BookedSegment bookedSegment, BookingClass bookingClass, String oldCodeSeat, String newCodeseat) throws Exception {

        //change Seat in Sabre
        com.sabre.services.acs.bso.seatchange.v3.ItineraryACS itinerary;
        itinerary = getItinerarySeatChange(bookedSegment.getSegment(), bookingClass);

        String boardingPrinterCommand = setUpConfigFactory.getInstance().getBoardingPassPrinterCommand();
        String boardingPrinter = setUpConfigFactory.getInstance().getBoardingLNIATA();

        SeatChangePairACS seatChangePairACS = new SeatChangePairACS();
        seatChangePairACS.setAssignedSeat(oldCodeSeat);
        seatChangePairACS.setRequestedSeat(newCodeseat);
        List<SeatChangePairACS> listSeatChangePairACS = new ArrayList<>();
        listSeatChangePairACS.add(seatChangePairACS);

        ACSSeatChangeRSACS acsSeatChangeRSACS = seatChangeService.seatChange(
                itinerary, com.sabre.services.stl.v3.PrintFormatACS.PECTAB,
                listSeatChangePairACS, boardingPrinter, boardingPrinterCommand
        );

        checkErrorChangeSeat(acsSeatChangeRSACS);

        return acsSeatChangeRSACS;
    }

    public void checkErrorChangeSeat(ACSSeatChangeRSACS acsSeatChangeRSACS) throws GenericException, Exception {

        StringBuilder sb = new StringBuilder();

        if (null != acsSeatChangeRSACS.getFreeTextInfoList() && null != acsSeatChangeRSACS.getFreeTextInfoList().getFreeTextInfo()) {
            for (FreeTextInfoACS freeTextInfoACS : acsSeatChangeRSACS.getFreeTextInfoList().getFreeTextInfo()) {
                for (String string : freeTextInfoACS.getTextLine().getText()) {
                    if (string.contains(ErrorType.SEAT_NOT_ASSIGNED_CHECK_MAP.getSearchableCode())) {
                        throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.SEAT_NOT_ASSIGNED_CHECK_MAP);
                    } else {
                        sb.append(string).append(" | ");
                    }
                }
            }
        }

        if (!acsSeatChangeRSACS.getResult().getStatus().equals(ErrorOrSuccessCode.SUCCESS)) {
            LOG.error(ErrorType.ERROR_TO_CHANGE_SEAT.getFullDescription());
            throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.ERROR_TO_CHANGE_SEAT,
                    ErrorType.ERROR_TO_CHANGE_SEAT.getFullDescription() + acsSeatChangeRSACS.getResult().getStatus() + sb.toString());
        }

        if (null == acsSeatChangeRSACS.getItineraryPassengerList()) {
            LOG.error(ErrorType.ERROR_TO_CHANGE_SEAT.getFullDescription());
            throw new GenericException(Response.Status.UNAUTHORIZED, ErrorType.ERROR_TO_CHANGE_SEAT,
                    ErrorType.ERROR_TO_CHANGE_SEAT.getFullDescription() + acsSeatChangeRSACS.getResult().getStatus() + sb.toString());
        }
    }

    /**
     * @return the pnrLookupDao
     */
    public IPNRLookupDao getPnrLookupDao() {
        return pnrLookupDao;
    }

    /**
     * @param pnrLookupDao the pnrLookupDao to set
     */
    public void setPnrLookupDao(IPNRLookupDao pnrLookupDao) {
        this.pnrLookupDao = pnrLookupDao;
    }

    /**
     * @return the editPassCharService
     */
    public AMEditPassCharService getEditPassCharService() {
        return editPassCharService;
    }

    /**
     * @param editPassCharService the editPassCharService to set
     */
    public void setEditPassCharService(AMEditPassCharService editPassCharService) {
        this.editPassCharService = editPassCharService;
    }

    /**
     * @return the aeService
     */
    public AEService getAeService() {
        return aeService;
    }

    /**
     * @param aeService the aeService to set
     */
    public void setAeService(AEService aeService) {
        this.aeService = aeService;
    }

    /**
     * @return the updatePnrCollectionService
     */
    public PnrCollectionService getUpdatePnrCollectionService() {
        return updatePnrCollectionService;
    }

    /**
     * @param updatePnrCollectionService the updatePnrCollectionService to set
     */
    public void setUpdatePnrCollectionService(PnrCollectionService updatePnrCollectionService) {
        this.updatePnrCollectionService = updatePnrCollectionService;
    }

    /**
     * @return the setUpConfigFactory
     */
    public CheckInSetUpConfigFactoryWrapper getSetUpConfigFactory() {
        if (null == setUpConfigFactory) {
            setUpConfigFactory = new CheckInSetUpConfigFactoryWrapper();
        }
        return setUpConfigFactory;
    }

    /**
     * @param setUpConfigFactory the setUpConfigFactory to set
     */
    public void setSetUpConfigFactory(CheckInSetUpConfigFactoryWrapper setUpConfigFactory) {
        this.setUpConfigFactory = setUpConfigFactory;
    }
}
