package com.am.checkin.web.service;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.AuthorizationPaymentConfig;
import com.aeromexico.commons.model.FlightStatusCollection;
import com.aeromexico.commons.model.Segment;
import com.aeromexico.commons.model.SegmentStatus;
import com.aeromexico.commons.model.SegmentStatusCollection;
import com.aeromexico.commons.model.rq.AirFlifoRQ;
import com.aeromexico.commons.model.weather.AirportWeather;
import com.aeromexico.commons.prototypes.JSONPrototype;
import com.aeromexico.commons.web.types.PosType;
import com.aeromexico.commons.web.types.StatusType;
import com.aeromexico.dao.client.exception.MongoDisconnectException;
import com.aeromexico.dao.qualifiers.AirportsCache;
import com.aeromexico.dao.services.IAirportCodesDao;
import com.aeromexico.sabre.api.flifo.FlifoService;
import com.aeromexico.sabre.api.schedule.ScheduleService;
import com.aeromexico.sabre.api.session.common.ServiceCall;
import com.aeromexico.sharedservices.services.FlightSubscriptionService;
import com.aeromexico.timezone.service.TimeZoneService;
import com.am.checkin.web.util.AirFlifoUtil;
import static com.am.checkin.web.util.AirFlifoUtil.getStringToDatetime;
import com.sabre.webservices.sabrexml._2011._10.flifo.OTAAirFlifoRS;
import com.sabre.webservices.sabrexml._2011._10.flifo.OTAAirFlifoRS.FlightInfo.ScheduledInfo.FlightLeg;
import com.sabre.webservices.sabrexml._2011._10.schedule.OTAAirScheduleRS;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;

import org.aeromexico.commons.exception.config.ReadProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Ivan Beltran
 */
@Named
@ApplicationScoped
public class AirFlifoService {

    private static final Logger log = LoggerFactory.getLogger(AirFlifoService.class);

    @Inject
    private FlifoService flifoService;

    @Inject
    private ScheduleService scheduleService;

    @Inject
    @AirportsCache
    private IAirportCodesDao airportsCodesDao;

    @Inject
    @Named("CheckInSetUpConfigFactoryWrapper")
    private CheckInSetUpConfigFactoryWrapper setUpConfigFactory;

    @Inject
    private FlightSubscriptionService flightSubscriptionService;

    @Inject
    private ReadProperties prop;

    private boolean FLIGHT_STATS_ENABLED = false;

    @PostConstruct
    public void init() {
        if (null == setUpConfigFactory) {
            setUpConfigFactory = new CheckInSetUpConfigFactoryWrapper();
        }
        try {
            String flightStatsEnabled = System.getProperty("flight.stats.enabled");
            if (flightStatsEnabled != null && !flightStatsEnabled.isEmpty()) {
                FLIGHT_STATS_ENABLED = Boolean.valueOf(flightStatsEnabled);
            }
        } catch (Exception ex) {
        }

    }

    public FlightStatusCollection callAirFlifo(AirFlifoRQ request) throws Exception {
        log.info("FLIGHT_STATS_ENABLED: " + FLIGHT_STATS_ENABLED);
        if (FLIGHT_STATS_ENABLED) {
            //Llamar el servicio de trips
            String responseFlightStatus = flightSubscriptionService.getFlightStatus(request);

            log.info("Flight status response:" + responseFlightStatus);

            if (responseFlightStatus.isEmpty()) {
                return new FlightStatusCollection();
            }

            if (responseFlightStatus.contains("TC_STATUS_ERROR")) {
                log.info("TC_STATUS_ERROR: " + responseFlightStatus);
                return new FlightStatusCollection();
            }

            return JSONPrototype.convert(
                    responseFlightStatus, FlightStatusCollection.class
            );
        } else {
            //Do we have the flight number in the request?
            if (request.getFlight() != null && !request.getFlight().trim().isEmpty()) {
                return getStatusForFlight(request);
            } else if (request.getOrigin() != null && request.getDestination() != null) {
                return getStatusByOriginAndDestination(request);
            } else {
                log.error("Invalid AirFlifoService request: " + request);
            }
            return new FlightStatusCollection();
        }

    }

    /**
     * *
     *
     * This method is used to lookup flight status by origin and destination
     *
     * @param airFlifoRQ
     * @return
     * @throws Exception
     */
    private FlightStatusCollection getStatusByOriginAndDestination(AirFlifoRQ airFlifoRQ) throws Exception {

        List<ServiceCall> serviceCallList = new ArrayList<>();
        FlightStatusCollection response = null;
        try {
            AuthorizationPaymentConfig authorizationPaymentConfig = getSetUpConfigFactory().getInstance().getAuthorizationPaymentConfig(airFlifoRQ.getStore(), airFlifoRQ.getPos());
            String cityCode = authorizationPaymentConfig.getCityCode();
            String cityCodeCommand = getSetUpConfigFactory().getInstance().getCityCodeCommand() + cityCode;

            OTAAirScheduleRS otaScheduleRS = scheduleService.getSchedule(airFlifoRQ.getOrigin(), airFlifoRQ.getDestination(), airFlifoRQ.getDate(), cityCode, cityCodeCommand);


            response = transformSearchOriginDestination(otaScheduleRS, airFlifoRQ.getDate().split("-")[0]);
            for (SegmentStatusCollection segmentStatusCollection : response.getCollection()) {
                for (SegmentStatus segmentStatus : segmentStatusCollection.getCollection()) {
                    OTAAirFlifoRS otaFlifoRS = flifoService.getFlifo(
                            segmentStatus.getSegment().getMarketingCarrier(),
                            segmentStatus.getSegment().getMarketingFlightCode(),
                            segmentStatus.getSegment().getDepartureDateTime().split("T")[0],
                            serviceCallList
                    );
                    segmentStatus = addInfoSegmentStatus(segmentStatus, otaFlifoRS, airFlifoRQ.getDate().split("-")[0], getTimeZone(segmentStatus.getSegment().getDepartureAirport()), getTimeZone(segmentStatus.getSegment().getArrivalAirport()));
                }
            }

        }catch (MongoDisconnectException me){
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(airFlifoRQ.getPos())) {
                log.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
                throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getCode(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getActionable(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getMessage(),
                        Response.Status.INTERNAL_SERVER_ERROR);
            } else {
                log.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
                throw me;
            }
        } catch (Exception e) {
            log.error("AirFlifo GET", airFlifoRQ.getPath(), e.getMessage());
            throw e;
        }

        log.info("\nGET Flight Status: \n\tPATH: " + airFlifoRQ.getPath() + "\n\tRESPONSE: " + response);

        int i;
        do {
            inicia:
            for (i = 0; i < response.getCollection().size(); i++) {
                SegmentStatusCollection segmentStatusCollection = response.getCollection().get(i);
                for (SegmentStatus segmentStatus : segmentStatusCollection.getCollection()) {
                    if ("".equalsIgnoreCase(segmentStatus.getStatus()) || !"AM".equalsIgnoreCase(segmentStatus.getSegment().getMarketingCarrier())) {
                        response.getCollection().remove(segmentStatusCollection);
                        break inicia;
                    }
                }
            }
        } while (i < response.getCollection().size());

        return response;
    }

    private FlightStatusCollection getStatusForFlight(AirFlifoRQ airFlifoRQ) throws Exception {

        FlightStatusCollection response;

        List<ServiceCall> serviceCallList = new ArrayList<>();
        OTAAirFlifoRS otaFlifo = flifoService.getFlifo("AM",
                airFlifoRQ.getFlight(),
                airFlifoRQ.getDate(),
                serviceCallList
        );

        try {
            response = transformSearchFlightNumbre(otaFlifo, airFlifoRQ.getDate().split("-")[0]);
        } catch (Exception e) {
            response = new FlightStatusCollection();
            log.error("AirFlifo GET", airFlifoRQ.getPath(), e.getMessage());
            throw e;
        }

        int i;
        do {
            inicia:
            for (i = 0; i < response.getCollection().size(); i++) {
                SegmentStatusCollection segmentStatusCollection = response.getCollection().get(i);
                for (SegmentStatus segmentStatus : segmentStatusCollection.getCollection()) {
                    if ("".equalsIgnoreCase(segmentStatus.getStatus()) || !"AM".equalsIgnoreCase(segmentStatus.getSegment().getMarketingCarrier())) {
                        response.getCollection().remove(segmentStatusCollection);
                        break inicia;
                    }
                }
            }
        } while (i < response.getCollection().size());

        log.info("\nGET Flight Status: \n\tPATH: " + airFlifoRQ.getPath() + "\n\tRESPONSE: " + response);
        return response;
    }

    public FlightStatusCollection transformSearchFlightNumbre(OTAAirFlifoRS otaAirFlifoRS, String year) throws Exception {
        FlightStatusCollection flightStatusCollection = new FlightStatusCollection();
        List<SegmentStatusCollection> listSegmentStatusCollection = new ArrayList<>();

        if (otaAirFlifoRS.getApplicationResults().getStatus().value().equalsIgnoreCase("Complete")) {

            List<SegmentStatus> listSegment = getListSegmentStatus(otaAirFlifoRS, year);
            SegmentStatusCollection segmentStatusCollection = new SegmentStatusCollection();

            segmentStatusCollection.setCollection(listSegment);
            int cont = 0;
            for (SegmentStatus segmentStatus : segmentStatusCollection.getCollection()) {

                AirFlifoUtil.setStatus(otaAirFlifoRS, segmentStatus, year, getTimeZone(segmentStatus.getSegment().getDepartureAirport()), getTimeZone(segmentStatus.getSegment().getArrivalAirport()));

                if (cont < (segmentStatusCollection.getCollection().size() - 1)) {
                    segmentStatus.getSegment().setLayoverToNextSegmentsInMinutes(AirFlifoUtil.calculateTimeLayoverToNextSegmentsInMinutes(segmentStatusCollection.getCollection().get(cont), segmentStatusCollection.getCollection().get(cont + 1), year));
                } else {
                    segmentStatus.getSegment().setLayoverToNextSegmentsInMinutes(0);
                }

                cont++;
            }

            segmentStatusCollection.setTotalFlightTimeInMinutes(AirFlifoUtil.calculteFlightTime(segmentStatusCollection.getCollection().get(0).getSegment().getDepartureDateTime(), segmentStatusCollection.getCollection().get(segmentStatusCollection.getCollection().size() - 1).getSegment().getArrivalDateTime()));
            segmentStatusCollection.setLegType("NON-STOP");
            listSegmentStatusCollection.add(segmentStatusCollection);
            flightStatusCollection.setCollection(listSegmentStatusCollection);
        }

        return flightStatusCollection;
    }

    private List<SegmentStatus> getListSegmentStatus(OTAAirFlifoRS otaAirFlifoRS, String year) throws Exception {

        List<SegmentStatus> listStatus = new LinkedList<>();
        SegmentStatus segmentStatus = new SegmentStatus();
        Date fecha = null;
        boolean departure = false, arrival = false;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        Segment segment = new Segment();
        for (FlightLeg flightLeg : otaAirFlifoRS.getFlightInfo().getScheduledInfo().getFlightLeg()) {
            if (flightLeg.getLocationCode() != null) {
                if (flightLeg.getArrivalDateTime() != null) {
                    if (flightLeg.getArrivalDateTime().getTerminal() != null) {
                        segmentStatus.setArrivalTerminal(flightLeg.getArrivalDateTime().getTerminal());
                    } else {
                        segmentStatus.setArrivalTerminal("N/A");
                    }

                    if (flightLeg.getArrivalDateTime().getGate() != null) {
                        segmentStatus.setArrivalGate(flightLeg.getArrivalDateTime().getGate());
                    } else {
                        segmentStatus.setArrivalGate("N/A");
                    }
                    segmentStatus.setEstimatedArrivalTime("");

                    //add parameter arrival segment
                    try {
                        fecha = getStringToDatetime(flightLeg.getArrivalDateTime().getScheduled(), year);
                        segment.setArrivalDateTime(formatter.format(fecha));
                        segment.setArrivalAirport(flightLeg.getLocationCode());
                    } catch (Exception e) {
                        throw e;
                    }
                    arrival = true;
                }

                if (departure && arrival) {
                    AirFlifoUtil.completeSegment(otaAirFlifoRS, segment);
                    segmentStatus.setSegment(segment);
                    listStatus.add(segmentStatus);
                    arrival = false;
                    departure = false;
                    segmentStatus = new SegmentStatus();
                    segment = new Segment();
                }

                if (flightLeg.getDepartureDateTime() != null) {
                    if (flightLeg.getDepartureDateTime().getTerminal() != null) {
                        segmentStatus.setBoardingTerminal(flightLeg.getDepartureDateTime().getTerminal());
                    } else {
                        segmentStatus.setBoardingTerminal("N/A");
                    }

                    if (flightLeg.getDepartureDateTime().getGate() != null) {
                        segmentStatus.setBoardingGate(flightLeg.getDepartureDateTime().getGate());
                    } else {
                        segmentStatus.setBoardingGate("N/A");
                    }

                    try {
                        fecha = AirFlifoUtil.getStringToDatetime(flightLeg.getDepartureDateTime().getScheduled(), year);
                    } catch (Exception e) {
                        throw e;
                    }
                    segmentStatus.setBoardingTime(formatter.format(fecha).split("T")[1].substring(0, 5));
                    segmentStatus.setEstimatedDepartureTime("");

                    //add parameter departure segment
                    try {
                        fecha = getStringToDatetime(flightLeg.getDepartureDateTime().getScheduled(), year);
                        segment.setDepartureDateTime(formatter.format(fecha));
                        segment.setDepartureAirport(flightLeg.getLocationCode());
                    } catch (Exception e) {
                        throw e;
                    }

                    departure = true;
                }
            }
        }

        return listStatus;
    }

    public FlightStatusCollection transformSearchOriginDestination(OTAAirScheduleRS otaAirScheduleRS, String year) throws Exception {
        FlightStatusCollection flightStatusCollection = new FlightStatusCollection();
        List<SegmentStatusCollection> listSegmentStatusCollection = new ArrayList<>();

        if (otaAirScheduleRS.getApplicationResults().getStatus().value().equalsIgnoreCase("Complete")) {

            if (otaAirScheduleRS.getOriginDestinationOptions().getOriginDestinationOption().size() > 0) {
                for (OTAAirScheduleRS.OriginDestinationOptions.OriginDestinationOption option : otaAirScheduleRS.getOriginDestinationOptions().getOriginDestinationOption()) {
                    List<SegmentStatus> listSegment = new ArrayList<>();
                    SegmentStatusCollection segmentStatusCollection = new SegmentStatusCollection();

                    if (option.getFlightSegment().size() > 1) {
                        segmentStatusCollection.setLegType("CONNECTING");
                    } else {
                        segmentStatusCollection.setLegType("NON-STOP");
                    }

                    String status = "";
                    int contadorSegment = 0;
                    for (OTAAirScheduleRS.OriginDestinationOptions.OriginDestinationOption.FlightSegment flightSegment : option.getFlightSegment()) {

                        SegmentStatus segmentStatus = new SegmentStatus();

                        if (flightSegment.getFlightDetails().isCanceled()) {
                            status = String.valueOf(StatusType.fromCode("CANCELLED"));
                        }

                        segmentStatus.setStatus(status);

                        Segment segment = new Segment();
                        Date fecha;
                        String date = "";
                        fecha = AirFlifoUtil.getStringToDatetime(flightSegment.getArrivalDateTime(), year);
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        date = formatter.format(fecha).split(" ")[0] + "T" + formatter.format(fecha).split(" ")[1];
                        segment.setArrivalDateTime(date);

                        fecha = AirFlifoUtil.getStringToDatetime(flightSegment.getDepartureDateTime(), year);
                        date = formatter.format(fecha).split(" ")[0] + "T" + formatter.format(fecha).split(" ")[1];
                        segment.setDepartureDateTime(date);

                        segment.setAircraftType(flightSegment.getEquipment().getAirEquipType());
                        segment.setArrivalAirport(flightSegment.getDestinationLocation().getLocationCode());
                        segment.setDepartureAirport(flightSegment.getOriginLocation().getLocationCode());
                        segment.setMarketingCarrier(flightSegment.getMarketingAirline().getCode());
                        segment.setMarketingFlightCode(flightSegment.getMarketingAirline().getFlightNumber());

                        if (flightSegment.getDisclosureAirline() != null) {
                            for (String text : flightSegment.getDisclosureAirline().getText()) {
                                switch (text) {
                                    case "OPERATED BY AEROLITORAL DBA AEROMEXICO CONNECT":
                                        segment.setOperatingCarrier("5D");
                                        break;
                                    case "OPERATED BY AEROMAR DBA AEROMEXICO EXPRESS":
                                        segment.setOperatingCarrier("VW");
                                        break;
                                    default:
                                        segment.setOperatingCarrier(flightSegment.getDisclosureAirline().getCode());
                                        break;
                                }
                            }

                            if ("**".equalsIgnoreCase(segment.getOperatingCarrier())) {
                                int flightNumber = Integer.parseInt(segment.getMarketingFlightCode());
                                if (flightNumber >= 5000 && flightNumber <= 5999) {
                                    segment.setOperatingCarrier("DL");
                                } else if (flightNumber >= 6000 && flightNumber <= 6499) {
                                    segment.setOperatingCarrier("AF");
                                } else if (flightNumber >= 6500 && flightNumber <= 6599) {
                                    segment.setOperatingCarrier("KL");
                                } else if (flightNumber >= 6600 && flightNumber <= 6699) {
                                    segment.setOperatingCarrier("AZ");
                                } else if (flightNumber >= 6700 && flightNumber <= 6799) {
                                    segment.setOperatingCarrier("KE");
                                } else if (flightNumber >= 6800 && flightNumber <= 6999) {
                                    segment.setOperatingCarrier("UX");
                                } else if (flightNumber >= 7000 && flightNumber <= 7099) {
                                    segment.setOperatingCarrier("OK");
                                } else if (flightNumber >= 7900 && flightNumber <= 7999) {
                                    segment.setOperatingCarrier("CM");
                                } else if (flightNumber >= 8220 && flightNumber <= 8269) {
                                    segment.setOperatingCarrier("AV");
                                } else if (flightNumber >= 8270 && flightNumber <= 8319) {
                                    segment.setOperatingCarrier("AS");
                                }
                            }

                            segment.setOperatingFlightCode(flightSegment.getMarketingAirline().getFlightNumber());
                        } else {
                            segment.setOperatingCarrier(flightSegment.getMarketingAirline().getCode());
                            segment.setOperatingFlightCode(flightSegment.getMarketingAirline().getFlightNumber());
                        }

                        String segmentCode = segment.getDepartureAirport()
                                + "_" + segment.getArrivalAirport()
                                + "_" + segment.getMarketingCarrier()
                                + "_" + segment.getDepartureDateTime().split("T")[0]
                                + "_" + segment.getDepartureDateTime().split("T")[1].replaceAll(":", "").substring(0, 4);

                        segment.setSegmentCode(segmentCode);
                        segment.setFlightDurationInMinutes(AirFlifoUtil.calculteFlightTime(segment.getDepartureDateTime(), segment.getArrivalDateTime()));

                        if (contadorSegment < (option.getFlightSegment().size() - 1)) {
                            segment.setLayoverToNextSegmentsInMinutes(AirFlifoUtil.calculateTimeLayoverToNextSegmentsInMinutes(option.getFlightSegment().get(contadorSegment), option.getFlightSegment().get(contadorSegment + 1), year));
                        } else {
                            segment.setLayoverToNextSegmentsInMinutes(0);
                        }

                        segmentStatus.setSegment(segment);

                        listSegment.add(segmentStatus);
                        contadorSegment++;
                    }

                    try {
                        segmentStatusCollection.setTotalFlightTimeInMinutes(AirFlifoUtil.calculteTimeTotal(option.getFlightSegment(), year));
                    } catch (Exception e) {
                        segmentStatusCollection.setTotalFlightTimeInMinutes(0);
                    }

                    segmentStatusCollection.setCollection(listSegment);
                    listSegmentStatusCollection.add(segmentStatusCollection);
                }
                flightStatusCollection.setCollection(listSegmentStatusCollection);
            } else {
                flightStatusCollection.setCollection(listSegmentStatusCollection);
            }
        }

        return flightStatusCollection;
    }

    public SegmentStatus addInfoSegmentStatus(SegmentStatus segmentStatus, OTAAirFlifoRS otaAirFlifoRS, String year, String originTimeZone, String destinationTimeZone) throws Exception {

        Date fecha;
        String dateTimeEstimate = "";

        if (otaAirFlifoRS.getFlightInfo() != null) {
            for (FlightLeg flightLeg : otaAirFlifoRS.getFlightInfo().getScheduledInfo().getFlightLeg()) {
                if (flightLeg.getLocationCode() != null) {
                    if (flightLeg.getDepartureDateTime() != null) {
                        if (flightLeg.getDepartureDateTime().getTerminal() != null) {
                            segmentStatus.setBoardingTerminal(flightLeg.getDepartureDateTime().getTerminal());
                        } else {
                            segmentStatus.setBoardingTerminal("N/A");
                        }

                        if (flightLeg.getDepartureDateTime().getGate() != null) {
                            segmentStatus.setBoardingGate(flightLeg.getDepartureDateTime().getGate());
                        } else {
                            segmentStatus.setBoardingGate("N/A");
                        }

                        try {
                            fecha = AirFlifoUtil.getStringToDatetime(flightLeg.getDepartureDateTime().getScheduled(), year);
                            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            dateTimeEstimate = formatter.format(fecha).split(" ")[0] + "T" + formatter.format(fecha).split(" ")[1];
                        } catch (Exception e) {
                            throw e;
                        }
                        segmentStatus.setBoardingTime(dateTimeEstimate.split("T")[1].substring(0, 5));
                        segmentStatus.setEstimatedDepartureTime("");

                    } else if (flightLeg.getArrivalDateTime() != null) {

                        if (flightLeg.getArrivalDateTime().getTerminal() != null) {
                            segmentStatus.setArrivalTerminal(flightLeg.getArrivalDateTime().getTerminal());
                        } else {
                            segmentStatus.setArrivalTerminal("N/A");
                        }

                        if (flightLeg.getArrivalDateTime().getGate() != null) {
                            segmentStatus.setArrivalGate(flightLeg.getArrivalDateTime().getGate());
                        } else {
                            segmentStatus.setArrivalGate("N/A");
                        }
                        segmentStatus.setEstimatedArrivalTime("");
                    }
                }
            }

            AirFlifoUtil.setStatus(otaAirFlifoRS, segmentStatus, year, originTimeZone, destinationTimeZone);

        } else {
            segmentStatus.setBoardingGate("N/A");
            segmentStatus.setBoardingTerminal("N/A");
            segmentStatus.setArrivalGate("N/A");
            segmentStatus.setArrivalTerminal("N/A");
            segmentStatus.setEstimatedDepartureTime(segmentStatus.getSegment().getDepartureDateTime());
            segmentStatus.setStatus("");
        }

        return segmentStatus;
    }

    private String getTimeZone(String iataAirport) throws Exception {
        try {
            AirportWeather airportWeather = airportsCodesDao.getAirportWeatherByIata(iataAirport);
            com.aeromexico.timezone.service.TimeZone timeZone = TimeZoneService.getTimeZone(
                    airportWeather.getAirport().getCode(),
                    airportWeather.getAirport().getLatitude(),
                    airportWeather.getAirport().getLongitude()
            );
            return timeZone.getTimeZoneId();
        } catch (Exception ex) {
            log.error("FlightStatus - getLocalDateTimeByAirport Failed to airport details {" + iataAirport + "}: {}", iataAirport, ex.getMessage());
        }

        return null;
    }

    public FlifoService getFlifoService() {
        return flifoService;
    }

    public void setFlifoService(FlifoService flifoService) {
        this.flifoService = flifoService;
    }

    public ScheduleService getScheduleService() {
        return scheduleService;
    }

    public void setScheduleService(ScheduleService scheduleService) {
        this.scheduleService = scheduleService;
    }

    /**
     * @return the setUpConfigFactory
     */
    public CheckInSetUpConfigFactoryWrapper getSetUpConfigFactory() {
        if (null == setUpConfigFactory) {
            setUpConfigFactory = new CheckInSetUpConfigFactoryWrapper();
        }
        return setUpConfigFactory;
    }

    /**
     * @param setUpConfigFactory the setUpConfigFactory to set
     */
    public void setSetUpConfigFactory(CheckInSetUpConfigFactoryWrapper setUpConfigFactory) {
        this.setUpConfigFactory = setUpConfigFactory;
    }

}
