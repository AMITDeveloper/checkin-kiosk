/**
 *
 */
package com.am.checkin.web.service;

import com.aeromexico.commons.model.KioskProfiles;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.dao.services.IKioskProfileDao;
import com.aeromexico.commons.exception.model.GenericException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * @author Carlos Luna
 *
 */
@Named
@ApplicationScoped
public class KioskProfileService {

    private static final Logger logger = LoggerFactory.getLogger(KioskProfileService.class);

    @Inject
    private IKioskProfileDao kioskProfileDao;

    public KioskProfiles getProfiles(String id) throws Exception {

        logger.info("Finding information with the IP Address parameter");
        KioskProfiles profile = findProfileDB(id);

        if (profile == null) {
            logger.error("Information not found in the database");
            throw new GenericException(Response.Status.NOT_FOUND, ErrorType.NOT_FOUND_PROFILE_FOR_KIOSK, "PROFILE NOT FOUND, ID: [" + id + "]");
        }

        return profile;
    }

    public boolean deleteProfiles(String id) throws Exception {

        logger.info("Deleting profile: " + id);

        boolean result = false;

        try {
        	result = kioskProfileDao.deleteKioskProfileInCollection(id);
        } catch (Exception e) {
            logger.error("Failed to delete Kiosk profile: " + e.getMessage());
            throw new GenericException(Response.Status.NOT_FOUND, ErrorType.NOT_FOUND_PROFILE_FOR_KIOSK, "PROFILE NOT FOUND, ID: [" + id + "]");
        }

        return result;
    }

    public List<KioskProfiles> getListProfiles() {

        logger.info("Finding information with the IP Address parameter");
        List<KioskProfiles> profiles = findListProfilesDB();

        if (profiles == null) {
            logger.error("Information not found in the database");
            throw new GenericException(Response.Status.NOT_FOUND, ErrorType.NOT_FOUND_PROFILE_FOR_KIOSK);
        }

        return profiles;
    }

    public KioskProfiles saveProfile(String id, KioskProfiles profile) throws Exception {

        logger.info("Saving or updating profile in the database");
        KioskProfiles kioskProfiles = null;
        if (profile != null) {
            profile.setId(id);
            kioskProfiles = saveKioskProfileDB(profile);
        } else {
            logger.error("Information not stored in the database");
            throw new GenericException(Response.Status.SERVICE_UNAVAILABLE, ErrorType.INFORMATION_NOT_STORED_IN_THE_DATABASE);
        }
        return kioskProfiles;
    }

    private KioskProfiles findProfileDB(String id) {

        logger.info("KioskProfileService.findProfileDB() getting profiles from the database");
        KioskProfiles profile = null;

        try {
            profile = kioskProfileDao.findProfile(id);
        } catch (Exception e) {
            logger.error("Error to get profile information from the database ", e);
        }

        return profile;
    }

    private KioskProfiles saveKioskProfileDB(KioskProfiles profile) {

        logger.info("KioskProfileService.saveProfileDB() saving profile in the database");
        int responseValue = -1;
        try {
            responseValue = kioskProfileDao.saveKioskProfileInCollection(profile);
        } catch (Exception e) {
            logger.error("Error to connect database, information not stored ", e);
        }

        logger.info("Validating the information for response");
        if (responseValue == 0) {
            logger.info("The document was insert in the database, {}", profile);
            return profile;
        } else if (responseValue == 1) {
            logger.info("The document was update in the database {}", profile);
            return profile;
        }

        return null;
    }

    private List<KioskProfiles> findListProfilesDB() {
        logger.info("KioskProfileService.findProfileDB() getting profiles from the database");
        List<KioskProfiles> profiles = null;

        try {
            profiles = kioskProfileDao.findListProfiles();
        } catch (Exception e) {
            logger.error("Error to get profile information from the database ", e);
        }

        return profiles;
    }

}
