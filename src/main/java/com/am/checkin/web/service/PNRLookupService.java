package com.am.checkin.web.service;

import com.aeromexico.commons.clubpremier.response.rs.TierBenefitsRS;
import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.AbstractAncillary;
import com.aeromexico.commons.model.AbstractSegmentChoice;
import com.aeromexico.commons.model.Ancillary;
import com.aeromexico.commons.model.AncillaryOffer;
import com.aeromexico.commons.model.AuthorizationPaymentConfig;
import com.aeromexico.commons.model.BaggageRoute;
import com.aeromexico.commons.model.BaggageRouteList;
import com.aeromexico.commons.model.BenefitCorporateRecognition;
import com.aeromexico.commons.model.BenefitRedeemed;
import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedLegCollection;
import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.BookedSegmentChoiceCollection;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.BookedTravelerAncillaryCollection;
import com.aeromexico.commons.model.BookedTravelerBenefit;
import com.aeromexico.commons.model.BookedTravelerCollection;
import com.aeromexico.commons.model.BookedTravellerBenefitCollection;
import com.aeromexico.commons.model.BookingClass;
import com.aeromexico.commons.model.BookingClassMap;
import com.aeromexico.commons.model.CabinCapacity;
import com.aeromexico.commons.model.CabinUpgradeAncillary;
import com.aeromexico.commons.model.CabinUpgradeAncillaryCollection;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.CartPNRCollection;
import com.aeromexico.commons.model.CheckInStatus;
import com.aeromexico.commons.model.CurrencyValue;
import com.aeromexico.commons.model.FreeBaggageAllowance;
import com.aeromexico.commons.model.FreeBaggageAllowancePerLeg;
import com.aeromexico.commons.model.Incident;
import com.aeromexico.commons.model.Infant;
import com.aeromexico.commons.model.OverBookingInfo;
import com.aeromexico.commons.model.PNR;
import com.aeromexico.commons.model.PNRCollection;
import com.aeromexico.commons.model.POS;
import com.aeromexico.commons.model.PaidSegmentChoice;
import com.aeromexico.commons.model.PaidTravelerAncillary;
import com.aeromexico.commons.model.PassengerOnList;
import com.aeromexico.commons.model.SSRRemarks;
import com.aeromexico.commons.model.SeamlessError;
import com.aeromexico.commons.model.SeatChoice;
import com.aeromexico.commons.model.SeatChoiceUpsell;
import com.aeromexico.commons.model.Segment;
import com.aeromexico.commons.model.SegmentChoice;
import com.aeromexico.commons.model.StandbyList;
import com.aeromexico.commons.model.TierBenefit;
import com.aeromexico.commons.model.TierBenefitCobrand;
import com.aeromexico.commons.model.TierBenefitCobrandList;
import com.aeromexico.commons.model.TravelerAncillary;
import com.aeromexico.commons.model.UpgradeAncillary;
import com.aeromexico.commons.model.UpgradeAncillaryCollection;
import com.aeromexico.commons.model.UpgradeList;
import com.aeromexico.commons.model.corporate.CorporateRecognition;
import com.aeromexico.commons.model.rq.PassengersListRQ;
import com.aeromexico.commons.model.rq.PnrRQ;
import com.aeromexico.commons.model.utils.CurrencyUtil;
import com.aeromexico.commons.model.weather.AirportWeather;
import com.aeromexico.commons.myb.model.Clid;
import com.aeromexico.commons.myb.model.ManageStatus;
import com.aeromexico.commons.seatmaps.util.SeatMapUtil;
import com.aeromexico.commons.web.types.ActionCodeType;
import com.aeromexico.commons.web.types.AirlineCodeType;
import com.aeromexico.commons.web.types.AncillaryGroupType;
import com.aeromexico.commons.web.types.BookedLegCheckinStatusType;
import com.aeromexico.commons.web.types.BookedLegFlightStatusType;
import com.aeromexico.commons.web.types.BookedTravelerCheckinIneligibleReasonsType;
import com.aeromexico.commons.web.types.CheckinIneligibleReasons;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.types.FarebasisFamilyType;
import com.aeromexico.commons.web.types.GenderType;
import com.aeromexico.commons.web.types.LegType;
import com.aeromexico.commons.web.types.MarketType;
import com.aeromexico.commons.web.types.PassengerFareType;
import com.aeromexico.commons.web.types.PaxType;
import com.aeromexico.commons.web.types.PosType;
import com.aeromexico.commons.web.types.SeatCharacteristicsType;
import com.aeromexico.commons.web.types.TierLevelType;
import com.aeromexico.commons.web.types.WindowTimeType;
import com.aeromexico.commons.web.util.FlightNumberUtil;
import com.aeromexico.commons.web.util.ListsUtil;
import com.aeromexico.commons.web.util.SegmentCodeUtil;
import com.aeromexico.commons.web.util.TimeUtil;
import com.aeromexico.commons.web.util.ValidateCheckinIneligibleReason;
import com.aeromexico.dao.client.exception.MongoDisconnectException;
import com.aeromexico.dao.qualifiers.AirportsCache;
import com.aeromexico.dao.qualifiers.CountryISOCache;
import com.aeromexico.dao.qualifiers.KioskProfileCache;
import com.aeromexico.dao.qualifiers.OverBookingCache;
import com.aeromexico.dao.qualifiers.OverBookingConfigCache;
import com.aeromexico.dao.qualifiers.TierBenefitsCache;
import com.aeromexico.dao.services.IAirportCodesDao;
import com.aeromexico.dao.services.ICountryISODao;
import com.aeromexico.dao.services.IKioskProfileDao;
import com.aeromexico.dao.services.IOverBookingConfigDao;
import com.aeromexico.dao.services.IOverBookingDao;
import com.aeromexico.dao.services.ITierBenefitDao;
import com.aeromexico.dao.services.MarketFlightService;
import com.aeromexico.dao.services.PriorityCodesDaoCache;
import com.aeromexico.dao.util.CommonUtil;
import com.aeromexico.profile.services.TierBenefitService;
import com.aeromexico.profile.util.BenefitUtil;
import com.aeromexico.sabre.api.acs.AMPassengerDataService;
import com.aeromexico.sabre.api.ancillaries.AMAncillariesPriceListService;
import com.aeromexico.sabre.api.ancillaries.model.ItineraryUpgrade;
import com.aeromexico.sabre.api.session.common.ServiceCall;
import com.aeromexico.sabre.api.session.exception.SabreEmptyResponseException;
import com.aeromexico.sabre.api.session.exception.SabreLayerUnavailableException;
import com.aeromexico.sabre.api.tripsearch.model.TripSearchOption;
import com.aeromexico.sharedservices.services.BaggageAllowanceService;
import com.aeromexico.sharedservices.services.CorporateRecognitionService;
import com.aeromexico.sharedservices.services.GetReservationService;
import com.aeromexico.sharedservices.services.TicketDocumentService;
import com.aeromexico.sharedservices.util.BrandedFareRulesUtil;
import com.aeromexico.sharedservices.util.PnrUtil;
import com.aeromexico.sharedservices.util.PriorityCodeUtil;
import com.aeromexico.sharedservices.util.TravelerItineraryUtil;
import com.am.checkin.web.event.dispatcher.UpdatePNRCollectionDispatcher;
import com.am.checkin.web.event.model.UpdatePNRCollectionEvent;
import com.am.checkin.web.util.AEUtil;
import com.am.checkin.web.util.CommonUtilsCheckIn;
import com.am.checkin.web.util.PNRLookUpServiceUtil;
import com.am.checkin.web.util.PnrCollectionUtil;
import com.am.checkin.web.util.TicketDocumentUtil;
import com.am.checkin.web.util.TimeMonitorUtil;
import com.am.checkin.web.v2.services.*;
import com.am.checkin.web.v2.seamless.service.SeamlessTravelPartyService;
import com.am.checkin.web.v2.util.PassengerAncillaryUtil;
import com.am.checkin.web.v2.util.PriorityUtil;
import com.am.checkin.web.v2.util.ReservationUtil;
import com.am.checkin.web.v2.util.SeamlessIncidentUtil;
import com.am.checkin.web.v2.util.SearchServiceUtil;
import com.am.checkin.web.v2.util.SystemVariablesUtil;
import com.am.seamless.checkin.common.models.exception.SeamlessException;
import com.google.gson.Gson;
import com.sabre.ns.ticketing.dc.GetTicketingDocumentRS;
import com.sabre.services.checkin.getpassengerdata.v4.GetPassengerDataRQACS;
import com.sabre.services.checkin.getpassengerdata.v4.GetPassengerDataRSACS;
import com.sabre.services.checkin.getpassengerdata.v4.ItineraryACS;
import com.sabre.services.checkin.getpassengerdata.v4.ItineraryAndPassengerInfoACS;
import com.sabre.services.checkin.getpassengerdata.v4.PassengerACS;
import com.sabre.services.checkin.getpassengerdata.v4.PassengerDataResponseACS;
import com.sabre.services.checkin.getpassengerdata.v4.PassengerItineraryACS;
import com.sabre.services.checkin.getpassengerdata.v4.PassengerItineraryListACS;
import com.sabre.services.checkin.getpassengerdata.v4.PassengerListACS;
import com.sabre.services.checkin.getpassengerdata.v4.RequiredInfoACS;
import com.sabre.services.checkin.getpassengerdata.v4.RequiredInfoListACS;
import com.sabre.services.checkin.getpassengerdata.v4.SSRInfoListACS;
import com.sabre.services.checkin.getpassengerdata.v4.VCRInfoACS;
import com.sabre.services.res.or.getreservation.NameAssociationTag;
import com.sabre.services.res.or.getreservation.OpenReservationElementType;
import com.sabre.services.res.or.getreservation.ServiceRequestType;
import com.sabre.services.stl.v3.AEDetailsACS;
import com.sabre.services.stl.v3.BaggageRouteACS;
import com.sabre.services.stl.v3.EditCodeListACS;
import com.sabre.services.stl.v3.ErrorOrSuccessCode;
import com.sabre.services.stl.v3.FreeTextInfoACS;
import com.sabre.services.stl.v3.FreeTextInfoListACS;
import com.sabre.services.stl.v3.PriceDetailsACS;
import com.sabre.services.stl.v3.SystemSpecificResults;
import com.sabre.services.stl.v3.TaxDetailsACS;
import com.sabre.services.stl.v3.TextListACS;
import com.sabre.webservices.pnrbuilder.AncillaryTaxPNRB;
import com.sabre.webservices.pnrbuilder.GetPriceListRS;
import com.sabre.webservices.pnrbuilder.OptionalAncillaryServicesInformationDataPNRB;
import com.sabre.webservices.pnrbuilder.PricedAncillaryServicePNRB;
import com.sabre.webservices.pnrbuilder.getreservation.*;
import com.sabre.webservices.pnrbuilder.updatereservation.ReservationUpdateItemType;
import com.sabre.webservices.pnrbuilder.updatereservation.UpdateReservationRQ;
import org.aeromexico.commons.exception.config.ReadProperties;
import org.drools.javaparser.utils.Log;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;
import javax.xml.datatype.XMLGregorianCalendar;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Named
@ApplicationScoped
public class PNRLookupService {

    private static final Logger LOG = LoggerFactory.getLogger(PNRLookupService.class);

    private static final Gson GSON = new Gson();

    @Inject
    private AMPassengerDataService passengerDataService;

    @Inject
    private TierBenefitService tierBenefitService;

    @Inject
    private GetReservationService getReservationService;

    @Inject
    private TicketDocumentService ticketDocumentService;

    @Inject
    private AMAncillariesPriceListService ancillariesPriceService;

    @Inject
    private FlxAncillaryPriceService flxAncillaryPriceService;

    @Inject
    private UpdatePNRCollectionDispatcher updatePNRCollectionDispatcher;

    @Inject
    private PnrCollectionService pnrCollectionService;

    @Inject
    private AEService aeService;

    @Inject
    private SeatsService seatsService;

    @Inject
    private AncillariesWaiveService ancillariesWaiveService;

    @Inject
    private FlightDetailsService flightDetailsService;

    @Inject
    private PriorityListService priorityListService;

    @Inject
    private BaggageAllowanceService baggageAllowanceService;

    @Inject
    @AirportsCache
    private IAirportCodesDao airportsCodesDao;

    @Inject
    private ReservationLookupService reservationLookupService;

    @Inject
    private PriorityCodesDaoCache priorityCodesDaoCache;

    @Inject
    private MarketFlightService marketFlightService;

    @Inject
    @CountryISOCache
    private ICountryISODao countryISODao;

    @Inject
    @TierBenefitsCache
    private ITierBenefitDao tierBenefitDao;

    @Inject
    @OverBookingCache
    private IOverBookingDao overBookingDao;

    @Inject
    @OverBookingConfigCache
    private IOverBookingConfigDao overBookingConfigDao;

    @Inject
    @KioskProfileCache
    private IKioskProfileDao kioskProfileDao;

    @Inject
    private SearchReservationService searchReservationService;

    @Inject
    private AirportService airportService;

    @Inject
    private ItineraryService itineraryService;

    @Inject
    private SeamlessTravelPartyService seamlessTravelPartyService;

    @Inject
    private ReservationUtilService reservationUtilService;

    @Inject
    @Named("CheckInSetUpConfigFactoryWrapper")
    private CheckInSetUpConfigFactoryWrapper setUpConfigFactory;

    @Inject
    private ReadProperties prop;

    private static final boolean INFANT = true;
    private static final boolean ADULT = !INFANT;

    @Inject
    @Named("CorporateRecognitionService")
    private CorporateRecognitionService corporateRecognitionService;


    @PostConstruct
    public void init() {
        if (null == getSetUpConfigFactory()) {
            setSetUpConfigFactory(new CheckInSetUpConfigFactoryWrapper());
        }
    }

    /**
     * @param pnrCollection This method is used to save the PNR collection
     *                      MongoDB in an synchronous mode
     */
    private void savePnrCollection(PNRCollection pnrCollection) {
        try {
            getPnrCollectionService().savePnrCollection(pnrCollection);
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
    }

    /**
     * @param pnrCollection This method is used to save the PNR collection
     *                      MongoDB in an asynchronous mode
     */
    private void savePnrCollectionAsync(PNRCollection pnrCollection) {
        try {
            getUpdatePNRCollectionDispatcher().publish(new UpdatePNRCollectionEvent(pnrCollection));
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
    }

    /**
     * *
     * <p>
     * Description:
     *
     * @param pnrRQ
     * @param pnrCollection
     */
    private void createOrUpdatePnrCollectionAfterCheckin(PnrRQ pnrRQ, PNRCollection pnrCollection) {

        /**
         * This call need to be done to update the PNR collection after check-in
         * only if PNR collection already exists and the reservation had
         * changes, example: a round trip where the first flight was already
         * use, in this case the PNR collection after check-in was created, now
         * the second flight was canceled and rescheduled, in this case we need
         * to update the collection with the new information (new leg)
         */
        try {
            boolean create = true;
            getPnrCollectionService().createOrUpdatePnrCollectionAfterCheckin(
                    pnrCollection,
                    pnrRQ.getRecordLocator(),
                    pnrRQ.getCartId(),
                    create
            );
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }

    }

    /**
     * @param pnrRQ
     * @param serviceCallList
     * @return
     * @throws Exception
     */
    private List<PNR> searchByFF(
            PnrRQ pnrRQ,
            List<ServiceCall> serviceCallList
    ) throws Exception {

        List<String> recordLocatorList = getSearchReservationService().searchByFF(pnrRQ);

        List<PNR> pnrList = new ArrayList<>();

        int maxReservations = 5;

//        boolean thereAreClosedFlights = false;

        if (null != recordLocatorList && !recordLocatorList.isEmpty()) {

            for (String recordLocator : recordLocatorList) {

                PnrRQ pnrRQNewSearch = new PnrRQ();
                pnrRQNewSearch.setRecordLocator(recordLocator);
                pnrRQNewSearch.setLastName(pnrRQ.getLastName());
                pnrRQNewSearch.setStore(pnrRQ.getStore());
                pnrRQNewSearch.setPos(pnrRQ.getPos());
                pnrRQNewSearch.setStore(pnrRQ.getStore());
                pnrRQNewSearch.setLanguage(pnrRQ.getLanguage());
                pnrRQNewSearch.setTripSearchOption(TripSearchOption.CONFIRMATION_CODE.toString());

                try {
                    PNR pnr = searchByPNR(
                            pnrRQNewSearch,
                            serviceCallList
                    );

                    if (null != pnr) {
                        if (ManageStatus.OPEN.equals(pnr.getCheckinStatus())
                                || ManageStatus.OPEN.equals(pnr.getManageStatus())) {
                            pnrList.add(pnr);

                            if (pnrList.size() >= maxReservations) {
                                break;
                            }
                        }
//                        else {
//                            thereAreClosedFlights = true;
//                        }
                    }
                } catch (GenericException ex) {
//                    if (!ErrorType.PNR_NOT_FOUND.equals(ex.getErrorCodeType())
//                            && !ErrorType.RESERVATION_NOT_FOUND.equals(ex.getErrorCodeType())
//                            && !ErrorType.RESERVATION_NOT_FOUND_WEB.equals(ex.getErrorCodeType())
//                            && !ErrorType.INVALID_ITINERARY.equals(ex.getErrorCodeType())) {
//
//                        //Warning warning = new Warning(ex.getErrorCodeType().getErrorCode(), recordLocator + " " + ex.getMsg());
//                        //pnrCollection.getWarnings().addToList(warning);
//                    } else {
//                        thereAreClosedFlights = true;
//                    }

                    LOG.error(ex.getMessage(), ex);

                } catch (Exception ex) {
                    LOG.error(ex.getMessage(), ex);

                    //Warning warning = new Warning(MybErrorCodeType.INTERNAL_ERROR.getErrorCode(), recordLocator + " " + ex.getMessage());
                    //pnrCollection.getWarnings().addToList(warning);
                }
            }
        }

//        if (thereAreClosedFlights
//                && pnrCollection.getCollection().isEmpty()
//                && pnrCollection.getWarnings().getCollection().isEmpty()) {
//            Warning warning = new Warning(ErrorType.NO_OPEN_PNRS_FOUND.getErrorCode(), ErrorType.NO_OPEN_PNRS_FOUND.getFullDescription());
//            //pnrCollection.getWarnings().addToList(warning);
//        }

        if (!pnrList.isEmpty()) {
            pnrList.sort((first, second) -> {
                try {
                    long firstDeparture;
                    firstDeparture = TimeUtil.getTime(
                            first.getLegs().getCollection().get(0).getFirstSegmentInLeg().getSegment().getDepartureDateTime()
                    );

                    long secondDeparture;
                    secondDeparture = TimeUtil.getTime(
                            second.getLegs().getCollection().get(0).getFirstSegmentInLeg().getSegment().getDepartureDateTime()
                    );
                    return (int) (firstDeparture - secondDeparture);
                } catch (Exception ex) {
                    return 1;
                }
            });
        }

        return pnrList;
    }

    /**
     * @param pnrRQ
     * @param serviceCallList
     * @return
     * @throws Exception This is the entry point to all type of reservations
     *                   lookup.
     */
    public PNRCollection lookUpReservation(PnrRQ pnrRQ, List<ServiceCall> serviceCallList) throws Exception {
        PNRCollection pnrCollection = new PNRCollection();
        pnrRQ = SearchServiceUtil.validateRequest(pnrRQ);
        PNR pnr;
        String recordLocator;

        switch (pnrRQ.getTripSearchOption()) {
            case "CONFIRMATION_CODE":
                pnr = searchByPNR(
                        pnrRQ,
                        serviceCallList
                );

                pnrCollection.getCollection().add(pnr);
                break;
            case "FF_PROGRAM":
                List<PNR> pnrList = searchByFF(
                        pnrRQ,
                        serviceCallList
                );

                pnrCollection.getCollection().addAll(pnrList);
                break;
            case "FLIGHT_NUMBER":
            case "DESTINATION":

                recordLocator = getSearchReservationService().searchByDestinationFlight(
                        pnrRQ
                );

                if (null != recordLocator && !recordLocator.trim().isEmpty()) {
                    pnrRQ.setRecordLocator(recordLocator);
                    pnr = searchByPNR(
                            pnrRQ,
                            serviceCallList
                    );
                    pnrCollection.getCollection().add(pnr);
                }
                break;
            case "VCR_CODE":

                recordLocator = getSearchReservationService().searchByVCR(
                        pnrRQ.getRecordLocator(),
                        pnrRQ.getPos()
                );

                if (null != recordLocator && !recordLocator.trim().isEmpty()) {
                    pnrRQ.setRecordLocator(recordLocator);
                    pnr = searchByPNR(
                            pnrRQ,
                            serviceCallList
                    );
                    pnrCollection.getCollection().add(pnr);
                }

                break;
            default:
                break;
        }

        if (null != pnrCollection) {
            pnrCollection.setPos(pnrRQ.getPos());
            pnrCollection.setStore(pnrRQ.getStore());

            if (null != pnrCollection.getCollection()
                    && !pnrCollection.getCollection().isEmpty()) {

                AuthorizationPaymentConfig authorizationPaymentConfig;
                authorizationPaymentConfig = getSetUpConfigFactory().getInstance().getAuthorizationPaymentConfig(
                        pnrRQ.getStore(),
                        pnrRQ.getPos()
                );
                String currencyCode = authorizationPaymentConfig.getCurrency();

                PnrCollectionUtil.setTotal(
                        pnrCollection,
                        currencyCode,
                        pnrRQ.getStore(),
                        pnrRQ.getPos()
                );

                //If there are no errors creating PnrCOllection we build message info.
                pnrRQ.setMessage(PNRLookUpServiceUtil.buildMessageToBeLogged(pnrCollection));

                long startTime;
                try {
                    startTime = Calendar.getInstance().getTimeInMillis();
                    savePnrCollectionAsync(pnrCollection);
                    TimeMonitorUtil.addCallToMonitor(serviceCallList, "DATABASE, save pnr collection", startTime);
                } catch (Exception ex) {
                    LOG.error(ex.getMessage(), ex);
                }

                try {
                    startTime = Calendar.getInstance().getTimeInMillis();
                    createOrUpdatePnrCollectionAfterCheckin(pnrRQ, pnrCollection);
                    TimeMonitorUtil.addCallToMonitor(serviceCallList, "DATABASE, save pnr collection after checkin", startTime);
                } catch (Exception ex) {
                    LOG.error(ex.getMessage(), ex);
                }
            }
        }

        return pnrCollection;

    }

    /**
     * *
     * <p>
     * This method is used to update all passengers to be ineligible.
     */
    private void setAllPassengersIneligible(PnrRQ pnrRQ, PNRCollection pnrCollection) {

        if (PosType.KIOSK.toString().equalsIgnoreCase(pnrRQ.getPos())) {
            return;
        }

        if (pnrCollection == null) {
            return;
        }

        CartPNR cartPNR = null;
        try {
            cartPNR = pnrCollection.getCartPNRByCartId(pnrRQ.getCartId());

            for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
                bookedTraveler.setIsEligibleToCheckin(false);
                bookedTraveler.getIneligibleReasons().add(
                        BookedTravelerCheckinIneligibleReasonsType.SCANNED_PASSPORT_REQUIRED.toString()
                );
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            return;
        }

    }

    /*
     *
     */
    private void isToHavana(PNR pnr) {

        if (null == pnr) {
            return;
        }

        for (CartPNR cartPNR : pnr.getCarts().getCollection()) {
            BookedLeg bookedLeg = pnr.getLegs().getBookedLegByLegCode(cartPNR.getLegCode());
            BookedSegment lastBookedSegment = bookedLeg.getLatestSegmentInLeg();
            if (lastBookedSegment != null && lastBookedSegment.getSegment() != null) {
                if ("HAV".equalsIgnoreCase(lastBookedSegment.getSegment().getArrivalAirport())) {
                    cartPNR.setToHava(true);
                } else {
                    cartPNR.setToHava(false);
                }
            }
        }
    }

    /*
     *
     */
    private void isETA(PNR pnr) {

        if (null == pnr) {
            return;
        }

        for (CartPNR cartPNR : pnr.getCarts().getCollection()) {
            BookedLeg bookedLeg = pnr.getLegs().getBookedLegByLegCode(cartPNR.getLegCode());
            BookedSegment lastBookedSegment = bookedLeg.getLatestSegmentInLeg();
            if (lastBookedSegment != null && lastBookedSegment.getSegment() != null) {
                try {
                    AirportWeather airport = getAirportService().getAirportFromIata(lastBookedSegment.getSegment().getArrivalAirport());

                    if (airport != null && airport.getAirport() != null
                            && ("US".equalsIgnoreCase(airport.getAirport().getCountry()) ||
                            "CA".equalsIgnoreCase(airport.getAirport().getCountry()) ||
                            "BR".equalsIgnoreCase(airport.getAirport().getCountry()))
                    ) {
                        cartPNR.setIsETA(true);
                    } else {
                        cartPNR.setIsETA(false);
                    }
                } catch (Exception ex) {
                    LOG.error(ex.getMessage(), ex);
                }
            }
        }
    }

    private void showCovidRestriction(PNR pnr) {

        if (null == pnr) {
            return;
        }
        String covidRestrictionStandAlond = System.getProperty("flagShowCovidRestrictionForm");
        boolean flagCovidRestriction = Boolean.parseBoolean(covidRestrictionStandAlond);
        List<String> countryCovid = getTextProperties();
        if(flagCovidRestriction){
            for (CartPNR cartPNR : pnr.getCarts().getCollection()) {
                if (!cartPNR.isSeamlessCheckin()) {
                    BookedLeg bookedLeg = pnr.getLegs().getBookedLegByLegCode(cartPNR.getLegCode());
                    BookedSegment lastBookedSegment = bookedLeg.getLatestSegmentInLeg();
                    if (lastBookedSegment != null && lastBookedSegment.getSegment() != null) {
                        try {
                            //AirportWeather airportCovidDepartur = getAirportService().getAirportFromIata(lastBookedSegment.getSegment().getDepartureAirport());
                            AirportWeather airportCovidArrival = getAirportService().getAirportFromIata(lastBookedSegment.getSegment().getArrivalAirport());
                            AirportWeather airportDeparturConnecting = null;
                            if ("CONNECTING".equalsIgnoreCase(bookedLeg.getSegments().getLegType())) {
                                airportDeparturConnecting = getAirportService().getAirportFromIata(lastBookedSegment.getSegment().getDepartureAirport());
                            }

                            for (String ct : countryCovid) {
                                if (airportCovidArrival != null && airportCovidArrival.getAirport() != null
                                        && ct.equalsIgnoreCase(airportCovidArrival.getAirport().getCountry())) {
                                    cartPNR.setShowCovidRestrictionForm(true);
                                }
                                if(null != airportDeparturConnecting){
                                    if(ct.equalsIgnoreCase(airportDeparturConnecting.getAirport().getCountry())){
                                        cartPNR.setShowCovidRestrictionForm(true);
                                    }
                                }
                            }

                        } catch (Exception ex) {
                            LOG.error(ex.getMessage(), ex);
                        }
                    }
                }
            }
        }
    }

    private void showAttestationBySegment(PNR pnr) {

        if (null == pnr) {
            return;
        }
        String flagshowAttestationBySegment = System.getProperty("flagShowAttestationBySegment");
        boolean flagShowAttestation = Boolean.parseBoolean(flagshowAttestationBySegment);
        List<String> countryCovid = getTextPropertiesAttestation();
        if(flagShowAttestation){
            for (CartPNR cartPNR : pnr.getCarts().getCollection()) {
                if (!cartPNR.isSeamlessCheckin()) {
                    setAttestationBySegment(pnr, cartPNR, countryCovid);
                }
            }
        }
    }
    
    private boolean isAdult(String travelerDateOfBirth) {
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate dateBirth = LocalDate.parse(travelerDateOfBirth, fmt);
        Period period = Period.between(dateBirth, LocalDate.now());
        if (period.getYears() > 17) {
            return true;
        }
    	return false;
    }
    
    private void isLeavingUkBySegment(PNR pnr) {
    	if (null == pnr) {
            return;
        }
        boolean flagLeavingUkBySegment = Boolean.parseBoolean(System.getProperty("flagLeavingUkBySegment"));
        boolean isAdult = false;
        for (CartPNR cartPNR : pnr.getCarts().getCollection()) {
			for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
				if (bookedTraveler.getDateOfBirth() != null && bookedTraveler.getDateOfBirth() != "" &&
					this.isAdult(bookedTraveler.getDateOfBirth())) {
					isAdult = true;
					break;
				}
			}
		}
        if (flagLeavingUkBySegment && pnr.getGlobalMarketFlight().name().equals("INTERNATIONAL") && isAdult) {
        	for (BookedLeg bookedLeg : pnr.getLegs().getCollection()) {
        		boolean isLeavingUk = false;
        		for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {
        			if (bookedSegment.getSegment().getDepartureAirport().equals("LHR") &&
        					bookedSegment.getSegment().getOperatingCarrier().equals("AM")) {
        				isLeavingUk = true;
        			}
        		}
        		bookedLeg.getSegments().setLeavingUk(isLeavingUk);
        	}
        }
    }

    public void setAttestationBySegment(PNR pnr, CartPNR cartPNR,List<String> countryCovid){
        BookedLeg bookedLeg = pnr.getLegs().getBookedLegByLegCode(cartPNR.getLegCode());
        BookedSegment lastBookedSegment = bookedLeg.getSegments().getCollection().get(0);
        if (lastBookedSegment != null && lastBookedSegment.getSegment() != null) {
            try {
                String operatedAm = lastBookedSegment.getSegment().getOperatingCarrier();
                if("AM".equalsIgnoreCase(operatedAm)){
                    AirportWeather airportCovidArrival = getAirportService().getAirportFromIata(lastBookedSegment.getSegment().getArrivalAirport());
                    AirportWeather airportCovidDeparture = getAirportService().getAirportFromIata(lastBookedSegment.getSegment().getDepartureAirport());
                    AirportWeather airportDeparturConnecting = null;
                    if(!"US".equalsIgnoreCase(airportCovidDeparture.getAirport().getCountry())){
                        if ("CONNECTING".equalsIgnoreCase(bookedLeg.getSegments().getLegType())) {
                            List<BookedSegment> conectingBookedLegs = bookedLeg.getSegments().getCollection();
                            if(conectingBookedLegs.size() > 1){
                                attestationByConnection(conectingBookedLegs, countryCovid, cartPNR);
                            }else{
                                airportDeparturConnecting = getAirportService().getAirportFromIata(lastBookedSegment.getSegment().getDepartureAirport());
                            }
                        }
                    }

                    for (String ct : countryCovid) {
                        if (airportCovidArrival != null && airportCovidArrival.getAirport() != null
                                && ct.equalsIgnoreCase(airportCovidArrival.getAirport().getCountry())) {
                            cartPNR.setShowAttestation(true);
                        }
                        if(null != airportDeparturConnecting){
                            if(ct.equalsIgnoreCase(airportDeparturConnecting.getAirport().getCountry())){
                                cartPNR.setShowAttestation(true);
                            }
                        }
                    }
                }

            } catch (Exception ex) {
                LOG.error(ex.getMessage(), ex);
            }
        }
    }

    public void attestationByConnection(List<BookedSegment> conectingBookedLegs, List<String> countryCovid, CartPNR cartPNR){
        AirportWeather airportDeparturConnecting = null;
        AirportWeather airportArrivalConnecting = null;
        try{
            for(BookedSegment bookedLegConecting: conectingBookedLegs){
                String operatedAmConnecting = bookedLegConecting.getSegment().getOperatingCarrier();
                if("AM".equalsIgnoreCase(operatedAmConnecting)){
                    airportDeparturConnecting = getAirportService().getAirportFromIata(bookedLegConecting.getSegment().getDepartureAirport());
                    airportArrivalConnecting = getAirportService().getAirportFromIata(bookedLegConecting.getSegment().getArrivalAirport());
                    for (String ct : countryCovid) {
                        if(null != airportDeparturConnecting || null != airportArrivalConnecting) {
                            if(ct.equalsIgnoreCase(airportDeparturConnecting.getAirport().getCountry())
                                || ct.equalsIgnoreCase(airportArrivalConnecting.getAirport().getCountry()) ){
                                cartPNR.setShowAttestation(true);
                            }
                        }
                    }
                }
            }
        } catch (Exception ex){
            LOG.error(ex.getMessage(), ex);
        }
    }

    /**
     * Description: Remove any unpaid ancillary
     *
     * @param pnr
     * @param serviceCallList
     * @param reservationUpdateItemTypeList
     */
    private void removeUnpaidAncillaries(
            PNR pnr,
            List<ServiceCall> serviceCallList,
            List<ReservationUpdateItemType> reservationUpdateItemTypeList
    ) {

        if (null != reservationUpdateItemTypeList && !reservationUpdateItemTypeList.isEmpty()) {
            try {

                UpdateReservationRQ updateReservationRQ;
                updateReservationRQ = AEUtil.getUpdateReservationRQ(
                        pnr.getPnr(),
                        reservationUpdateItemTypeList
                );

                getAeService().deleteAE(updateReservationRQ, pnr.getPnr(), pnr.getCarts().getCollection().get(0).getMeta().getCartId(), serviceCallList);
                LOG.info("Unpaid ancillaries removed from reservation: " + new Gson().toJson(reservationUpdateItemTypeList));
            } catch (Exception ex) {
                LOG.error("Error deleting unpaid ancillaries from reservation: " + ex.getMessage(), ex);
            }
        }

        try {
            if (!getAeService().removeUnpaidAncillariesForExtraWeight(pnr, serviceCallList)) {
                //setTotal(pnrCollection, pnrRQ.getStore(), pnrRQ.getPos());
            }
        } catch (Exception ex) {
            LOG.error("Error deleting unpaid seat ancillaries without pre-assigned seat: " + ex.getMessage(), ex);
        }

//        try {
//            if (!aeService.removeUnpaidAncillariesForUnassignedSeats(pnrCollection, serviceCallList)) {
//                //setTotal(pnrCollection, pnrRQ.getStore(), pnrRQ.getPos());
//            }
//        } catch (Exception ex) {
//            LOG.error("Error deleting unpaid seat ancillaries without pre-assigned seat: " + ex.getMessage(), ex);
//        }
    }

    /**
     * Description: Remove any unpaid ancillary
     *
     * @param pnr
     * @param serviceCallList
     */
    private void removeUnpaidSeatAncillaries(
            PNR pnr,
            List<ServiceCall> serviceCallList,
            String pos
    ) {

        if (null == pnr) {
            return;
        }

        Set<Integer> ancillaryIDs = new HashSet<>();

        for (CartPNR cartPNR : pnr.getCarts().getCollection()) {
            BookedLeg bookedLeg = pnr.getLegs().getBookedLegByLegCode(cartPNR.getLegCode());

            for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
                if (!bookedTraveler.isCheckinStatus()) {
                    try {
                        boolean removed = getSeatsService().removeUnpaidSeatAncillaries(
                                bookedLeg,
                                bookedTraveler,
                                pnr.getPnr(),
                                cartPNR.getMeta().getCartId(),
                                ancillaryIDs,
                                serviceCallList,
                                pos
                        );
                    } catch (Exception ex) {
                        LOG.error(ex.getMessage(), ex);
                    }
                }
            }
        }

        //remove deleted ancillaries from collection
        if (!ancillaryIDs.isEmpty()) {
            AEUtil.removeDeletedItemsFromCollection(pnr, ancillaryIDs);
        }
    }

    private GetPassengerDataRSACS getPassengerDataUsingItineraryAndName(GetReservationRS getReservationRS, String pos) {

        String recordLocator = getReservationRS.getReservation().getBookingDetails().getRecordLocator();
        PassengerPNRB firstPax = getReservationRS.getReservation().getPassengerReservation().getPassengers().getPassenger().get(0);

        //To get missing information from getReservation
        List<SegmentTypePNRB.Segment> segmentList = getReservationRS.getReservation().getPassengerReservation().getSegments().getSegment()
                .stream().filter(i -> null != i.getAir() && null != i.getAir().getActionCode() && TravelerItineraryUtil.getFLIGHT_STATUS_OK_LIST().contains(i.getAir().getActionCode()))
                .collect(Collectors.toList());

        for (SegmentTypePNRB.Segment segment : segmentList) {

            try {
                boolean isDomestic = getAirportService().isDomesticFlight(
                        segment.getAir().getDepartureAirport(),
                        segment.getAir().getArrivalAirport()
                );

                long timeToDepartureInMinutes = getAirportService().getTimeToDepartureInMinutes(
                        segment.getAir().getDepartureAirport(),
                        segment.getAir().getDepartureDateTime(),
                        pos
                );

                if (isDomestic) {
                    if (timeToDepartureInMinutes > 0 && timeToDepartureInMinutes <= (SystemVariablesUtil.getDomesticCheckinWindowTimeInHrs() * 60)) {
                        GetPassengerDataRSACS getPassengerDataRSACS = this.getPassengerDataByName(
                                recordLocator,
                                segment.getAir().getOperatingAirlineCode(),
                                segment.getAir().getOperatingFlightNumber(),
                                segment.getAir().getDepartureDateTime().substring(0, 10),
                                segment.getAir().getDepartureAirport(),
                                firstPax.getLastName(),
                                firstPax.getFirstName()
                        );

                        return getPassengerDataRSACS;
                    }
                } else {
                    if (timeToDepartureInMinutes > 0 && timeToDepartureInMinutes <= (SystemVariablesUtil.getInternationalCheckinWindowTimeInHrs() * 60)) {
                        GetPassengerDataRSACS getPassengerDataRSACS = this.getPassengerDataByName(
                                recordLocator,
                                segment.getAir().getOperatingAirlineCode(),
                                segment.getAir().getOperatingFlightNumber(),
                                segment.getAir().getDepartureDateTime().substring(0, 10),
                                segment.getAir().getDepartureAirport(),
                                firstPax.getLastName(),
                                firstPax.getFirstName()
                        );

                        return getPassengerDataRSACS;
                    }
                }
            } catch (Exception ex) {

            }
        }

        return null;

    }

    public List<String> getSubjectAreas() {
        List<String> subjectAreas = new ArrayList<>();
        subjectAreas.add("HEADER");
        subjectAreas.add("ACTIVE");
        subjectAreas.add("HISTORICAL");
        subjectAreas.add("AIR_CABIN");
        subjectAreas.add("PRICING_INFORMATION");
        subjectAreas.add("PASSENGERDETAILS");
        subjectAreas.add("NAME");
        subjectAreas.add("TICKETING");
        subjectAreas.add("VCR");
        subjectAreas.add("AFAX");
        return subjectAreas;
    }

    /**
     * @param pnrRQ
     * @param serviceCallList
     * @return
     * @throws Exception
     * @throws ParseException
     */
    public PNR searchByPNR(
            PnrRQ pnrRQ,
            List<ServiceCall> serviceCallList
    ) throws Exception {
        List<ReservationUpdateItemType> reservationUpdateItemTypeList = new ArrayList<>();
        List<ReservationUpdateItemType> reservationUpdateSeatItemTypeList = new ArrayList<>();

        List<String> errorMessagesCaughtList = new ArrayList<>();
        LOG.info("getReservationRS service call: ");
        GetReservationRS getReservationRS = reservationLookupService.getReservationRS(pnrRQ, serviceCallList);
        LOG.info("getReservationRS Result {}", new Gson().toJson(getReservationRS));
        GetPassengerDataRSACS passengerDataRS = getPassengerData(pnrRQ, serviceCallList);

        if (!ErrorOrSuccessCode.SUCCESS.equals(passengerDataRS.getResult().getStatus())) {
            if (null != passengerDataRS.getResult().getSystemSpecificResults()
                    && !passengerDataRS.getResult().getSystemSpecificResults().isEmpty()) {

                StringBuilder sb = new StringBuilder();

                for (SystemSpecificResults systemSpecificResult : passengerDataRS.getResult().getSystemSpecificResults()) {
                    sb.append(systemSpecificResult.getErrorMessage().getValue());
                }

                String msg = sb.toString().toUpperCase();

                if (msg.contains("INVALID OR EXPIRED SESSION")) {
                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION, ErrorType.EXPIRED_SESSION.getFullDescription());
                }

                if (msg.contains("DISPLAY SIMILAR NAME LIST PRIOR TO THIS ENTRY")) {
                    GetPassengerDataRSACS passengerData = getPassengerDataUsingItineraryAndName(getReservationRS, pnrRQ.getPos());

                    if (null != passengerData) {
                        passengerDataRS = passengerData;
                    }
                }

                if (msg.contains("NOT INITIALIZED")) {
                    errorMessagesCaughtList.add(msg);
                    //Continue
                } else if (msg.contains("NO PNR WAS")) {
                    //No PNR was found
                    errorMessagesCaughtList.add("NO_PNR_FOUND");
                } else {
                    errorMessagesCaughtList.add(msg);
                }
            }
        }

        if (null == passengerDataRS.getPassengerDataResponseList()) {
            if (null != passengerDataRS.getResult().getSystemSpecificResults()
                    && !passengerDataRS.getResult().getSystemSpecificResults().isEmpty()) {

                StringBuilder sb = new StringBuilder();

                for (SystemSpecificResults systemSpecificResult : passengerDataRS.getResult().getSystemSpecificResults()) {
                    sb.append(systemSpecificResult.getErrorMessage().getValue());
                }

                String msg = sb.toString();

                if (msg.contains("NOT INITIALIZED")) {
                    errorMessagesCaughtList.add(msg);
                    //Continue
                }
            }
        }

        GetTicketingDocumentRS ticketingDocumentRS = null;

        try {
            ticketingDocumentRS = ticketDocumentService.getTicketDocByRecordLocator(pnrRQ.getRecordLocator(), pnrRQ.getCartId());

        } catch (SabreEmptyResponseException ser) {
            LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110302).toString(), ser.fillInStackTrace());
            throw ser;
        } catch (SabreLayerUnavailableException ex) {
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(pnrRQ.getPos())) {
                LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).toString(), ex.fillInStackTrace());
                throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getCode(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getActionable(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getMessage(),
                        Response.Status.INTERNAL_SERVER_ERROR);
            } else {
                LOG.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_12110301,"PNRLookupService.java:936", ex.getMessage());
                throw ex;
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            throw ex;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date creationDate = sdf.parse(sdf.format(getReservationRS.getReservation().getBookingDetails().getCreationTimestamp().toGregorianCalendar().getTime()));
        String ticketDocumentDateRS = PnrUtil.getTicketDocumentDate(ticketingDocumentRS, creationDate);
        
        LOG.info("PassengerDataRS {} ", new Gson().toJson(passengerDataRS));
        PNR pnr = transformPNR(
                pnrRQ,
                getReservationRS,
                ticketingDocumentRS,
                passengerDataRS,
                reservationUpdateItemTypeList,
                reservationUpdateSeatItemTypeList,
                errorMessagesCaughtList,
                ticketDocumentDateRS,
                serviceCallList
        );

        if (PosType.WEB.toString().equalsIgnoreCase(pnrRQ.getPos())
                && getSetUpConfigFactory().getSystemPropertiesFactory().getInstance().isWaiveFirstBag()) {

            String cartId = pnr.getCarts().getCollection().get(0).getMeta().getCartId();

            getAncillariesWaiveService().wavedFirstBaggage(pnr, cartId, serviceCallList, pnrRQ.getPos());
        }

        removeUnpaidAncillaries(pnr, serviceCallList, reservationUpdateItemTypeList);

        removeUnpaidSeatAncillaries(pnr, serviceCallList, pnrRQ.getPos());

        // If this flight to Havana/Cuba, set the indicator on the cart for that
        isToHavana(pnr);

        // If this flight requires ETA, set the indicator on the cart for that
        isETA(pnr);

        showCovidRestriction(pnr);

        showAttestationBySegment(pnr);
        
        isLeavingUkBySegment(pnr);

        PriorityCodeUtil.setPriorityCode(
        		pnr,
        		getReservationRS,
        		getPriorityCodesDaoCache()
        );

        setStandByInfo(pnr.getLegs(), pnr.getCarts(), pnrRQ);

        return pnr;
    }

    /**
     * @param pnrRQ
     * @param serviceCallList
     * @return
     */
    private GetPassengerDataRSACS getPassengerData(PnrRQ pnrRQ, List<ServiceCall> serviceCallList) {
        try {
            return getPassengerData().passengerDataByPNRLocator(
                    pnrRQ.getRecordLocator(),
                    true,
                    SystemVariablesUtil.isTimaticEnabled(),
                    serviceCallList
            );
        } catch (Exception e) {
            LOG.error("ERROR: Getting PassengerData | PNR:" + pnrRQ.getRecordLocator() + "| SABRE_ERR:" + e.getMessage(), e);
            throw new GenericException(Response.Status.INTERNAL_SERVER_ERROR, ErrorType.BAD_REQUEST, "PNR was not Found:" + e.getMessage());
        }
    }

    /**
     * @param airline
     * @param flightNumber
     * @param departureDate
     * @param origin
     * @param lastName
     * @param firstName
     * @return
     */
    private GetPassengerDataRSACS getPassengerDataByName(
            String recordLocator,
            String airline,
            String flightNumber,
            String departureDate,
            String origin,
            String lastName,
            String firstName
    ) {
        try {
            return getPassengerData().passengerDataByItineraryAndName(
                    airline,
                    flightNumber,
                    departureDate,
                    origin,
                    lastName,
                    firstName,
                    true,
                    SystemVariablesUtil.isTimaticEnabled()
            );
        } catch (Exception e) {
            LOG.error("ERROR: Getting PassengerData by Name | PNR:" + recordLocator + "| SABRE_ERR:" + e.getMessage(), e);
            throw new GenericException(Response.Status.INTERNAL_SERVER_ERROR, ErrorType.BAD_REQUEST, "PNR was not Found:" + e.getMessage());
        }
    }

    /**
     * @param pnrRQ
     * @param getReservationRS
     * @param ticketingDocumentRS
     * @param passengerDataResponse
     * @param reservationUpdateItemTypeList
     * @param reservationUpdateSeatItemTypeList
     * @param errorMessagesCaughtList
     * @param serviceCallList
     * @return
     * @throws Exception
     */
    private PNR transformPNR(
            PnrRQ pnrRQ,
            GetReservationRS getReservationRS,
            GetTicketingDocumentRS ticketingDocumentRS,
            GetPassengerDataRSACS passengerDataResponse,
            List<ReservationUpdateItemType> reservationUpdateItemTypeList,
            List<ReservationUpdateItemType> reservationUpdateSeatItemTypeList,
            List<String> errorMessagesCaughtList,
            String ticketDocumentDateRS,
            List<ServiceCall> serviceCallList
    ) throws Exception {

        PNR pnr = new PNR();

        pnr.setPnr(pnrRQ.getRecordLocator());
        pnr.setBoardingPassId(pnrRQ.getRecordLocator());

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            String creationDate = sdf.format(getReservationRS.getReservation().getBookingDetails().getCreationTimestamp().toGregorianCalendar().getTime());
            pnr.setCreationDate(creationDate);
            pnr.setTicketDocumentDate(ticketDocumentDateRS);

            if (BrandedFareRulesUtil.isDateBeforeBrandedFares_1_0(creationDate)) {
                pnr.setBookedBeforeNewFareFamilies(true);
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }

        boolean isAmTicketingProvider = TicketDocumentUtil.isAmTicketingProvider(ticketingDocumentRS);

        BookedLegCollection bookedLegCollection = getItineraryService().getBookedLegCollection(
                getReservationRS,
                isAmTicketingProvider,
                pnrRQ
        );

        // Legs Part
        pnr.setLegs(
                bookedLegCollection
        );

        PnrUtil.setCouponNumber(bookedLegCollection, ticketingDocumentRS);

        BrandedFareRulesUtil.setRegion(pnr, getMarketFlightService());

        BrandedFareRulesUtil.setFareBasisCode(pnr, ticketingDocumentRS);

        BrandedFareRulesUtil.setSeatSelectionType(pnr);

        BrandedFareRulesUtil.setSeatChangeAllowance(pnr);

        Set<String> formOfPayments = ItineraryService.getFormOfPayment(ticketingDocumentRS);
        pnr.setFormOfPayment(formOfPayments);

        if (SystemVariablesUtil.isTsaRequired()) {
            getItineraryService().setTsaValidation(pnr, formOfPayments, pnrRQ);
        }

        getItineraryService().setManageStatus(pnr, pnrRQ.getPos());

        //TODO: validate this part of code
        //to check why this logic was used before
            /*
            if (messageNotEligibleChk.contains("CARRIER")) {
                //GET BASIC INFO (ERROR RETRIEVING THE INFO FAIL INELIGIBLE CHECK IN )
                for (PersonName personName : passengerDetails) {
                    bookedTravelerList.add(
                            getBookedTravelerBasicInfo(
                                    personName,
                                    bookedLeg.getFirstOpenSegment(),
                                    messageNotEligibleChk
                            )
                    );
                }
            }
            */


        // Carts Part
        pnr.setCarts(
                getCartPNRCollection(
                        pnrRQ,
                        pnr.getCreationDate(),
                        getReservationRS,
                        ticketingDocumentRS,
                        passengerDataResponse,
                        pnr.getLegs(),
                        reservationUpdateItemTypeList,
                        reservationUpdateSeatItemTypeList,
                        serviceCallList
                )
        );

        //sort passengers to be ordered by nameRef number
        for (CartPNR cartPNR : pnr.getCarts().getCollection()) {
            if (null != cartPNR.getTravelerInfo() && null != cartPNR.getTravelerInfo().getCollection()) {
                Collections.sort(cartPNR.getTravelerInfo().getCollection(), (BookedTraveler p1, BookedTraveler p2) -> {
                    if (null != p1.getNameRefNumber() && null != p2.getNameRefNumber()) {
                        return p1.getNameRefNumber().compareTo(p2.getNameRefNumber());
                    } else if (null == p1.getNameRefNumber() && null == p2.getNameRefNumber()) {
                        return 0;
                    } else if (null == p1.getNameRefNumber()) {
                        return 1;
                    } else {// if (null == p2.getNameRefNumber()) {
                        return -1;
                    }
                });
            }
        }

        List<SegmentTypePNRB.Segment> sabreSegments;
        sabreSegments = getReservationRS.getReservation().getPassengerReservation().getSegments().getSegment();

        BrandedFareRulesUtil.setBookingClassesPerPassengerCheckIn(pnr, sabreSegments, ticketDocumentDateRS);

        String covidSeamlessStandAlond = System.getProperty("flagShowCovidSeamlessnForm");
        boolean flagCovidSeamless = Boolean.parseBoolean(covidSeamlessStandAlond);
        //copy itinerary booking classes
        for (CartPNR cartPNR : pnr.getCarts().getCollection()) {
            if (cartPNR.isSeamlessCheckin()) {
                if(flagCovidSeamless){
                    cartPNR.setShowCovidSeamlessForm(true);
                }else{
                    cartPNR.setShowCovidSeamlessForm(false);
                }
                BookedLeg bookedLeg = pnr.getBookedLegByLegCode(cartPNR.getLegCode());

                for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {
                    for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
                        for (BookingClass bookingClass : bookedTraveler.getItineraryBookingClasses().getCollection()) {
                            if (SegmentCodeUtil.compareSegmentCodeWithoutTime(bookedSegment.getSegment().getSegmentCode(), bookingClass.getSegmentCode())) {
                                bookedTraveler.getBookingClasses().getCollection().add(bookingClass);
                            }
                        }
                    }
                }
            }else{
                cartPNR.setShowCovidSeamlessForm(false);
            }
        }

        PnrUtil.setSegmentBookingClasses(pnr);

        PnrUtil.setCouponNumber(pnr, ticketingDocumentRS);

        BrandedFareRulesUtil.setFareBasisCodePerPassenger(pnr, ticketingDocumentRS);

        if (SystemVariablesUtil.isCheckChildrenOnBasicEnabled()) {
            ReservationUtil.setChildrenValidation(pnr);
        }

        ReservationUtil.updateSegmentCodesForSeats(pnr);

        PnrUtil.setLoyalty(pnr, getReservationRS);

        for (CartPNR cartPNR : pnr.getCarts().getCollection()) {
            PriorityUtil.setRevenuePriorityCode(cartPNR.getTravelerInfo().getCollection());
        }

        PnrUtil.setFlightItineraryType(pnr);

        PnrUtil.setTicketNumbersFromDocumentService(pnr, ticketingDocumentRS);

        PnrUtil.setTicketNumbers(pnr, ticketingDocumentRS);

        PnrUtil.setGlobalMarketType(pnr);

        ReservationUtil.updateSegmentsFlagUpgradeAvaiable(pnr);

        //OverBooking (Volunteers passengers)
        getReservationUtilService().addVolunteerOffer(pnr, pnrRQ);

        ReservationUtil.checkExistsBookedCar(pnr, getReservationRS);

        Clid clid = new Clid();
        String clidReservation = getClidReservation(getReservationRS);
        if (null != clidReservation) {
            try {
            	//clid.setCompanyCode("AM");
            	//clid.setCompanyNumber("5000000009066");
                clid.setCompanyCode(clidReservation.isEmpty() ? null : clidReservation.substring(0, 2));
                clid.setCompanyNumber(clidReservation.isEmpty() ? null : clidReservation.substring(2));                
            } catch (Exception e) {
                clid.setCompanyCode(null);
                clid.setCompanyNumber(null);
            }
            if (null == clid.getCompanyCode() || null == clid.getCompanyNumber()) {
                clid.setApplyCorporateBenefit(false);
            }
        }
        /*CorporateRecognition corporateRecognition = new CorporateRecognition();
        corporateRecognition.setCompanyName("KIMBERLY CLARK");
        corporateRecognition.setCorporateMessage("Aeromexico agradece a ti y a KIMBERLY CLARK por viajar con nosotros.");
        corporateRecognition.setLogo(true);
        corporateRecognition.setPreferredSeat(true);
        corporateRecognition.setSpid("5000000009066");
        pnr.setCorporateRecognition(corporateRecognition);*/
        corporateRecognitionService.validCorporateRecognition(getReservationRS, pnr);
        if (null == pnr.getCorporateRecognition()) { // BI not response
        	if (pnrRQ.getPos().equalsIgnoreCase("WEB")) {
        		pnr.setCorporateRecognition(new CorporateRecognition());
            	pnr.getCorporateRecognition().setPreferredSeat(false);
        	} else {
        		pnr.setCorporateRecognition(null);
        		clid.setApplyCorporateBenefit(false);
        	}
        }
        
        if (pnr.getCorporateRecognition() != null) {
        	if (pnr.getCorporateRecognition().isPreferredSeat()) {
            	clid.setApplyCorporateBenefit(true); // aproved by BIc
        	}
            //validate rule seats and domestic - classic bags
        	this.validRulesToOfferPreferredSeatsAndBags(pnr, clid);
        }
        // set corporate recognition information
        pnr.setClid(clid);

        PNRLookUpServiceUtil.autoCheckinValidation(pnr, getReservationRS);

        return pnr;
    }
    
    /**
     * Read SSR CLID in reservation
     *
     * @param getReservationRS
     * @return String AM5000000001199 or null in other case
     */
    private String getClidReservation(GetReservationRS getReservationRS) {
        // Obtener clave corporativa de la reserva
        if (null != getReservationRS
                && null != getReservationRS.getReservation()
                && null != getReservationRS.getReservation().getGenericSpecialRequests()) {

            if (!getReservationRS.getReservation().getGenericSpecialRequests().isEmpty()) {
                for (GenericSpecialRequestPNRB genericSpecialRequestPNRB : getReservationRS.getReservation().getGenericSpecialRequests()) {
                    if (null != genericSpecialRequestPNRB.getCode()) {
                        if ("CLID".equalsIgnoreCase(genericSpecialRequestPNRB.getCode())) {
                            if (null != genericSpecialRequestPNRB.getFreeText()) {
                                LOG.info("CORPORATE RECOGNITION CLID: {}", genericSpecialRequestPNRB.getFreeText());
                                return genericSpecialRequestPNRB.getFreeText();
                            }
                        }
                    }
                }
            }
        }
        return null;
    }
    
    /**
     * Validate rules to offert preferred seat.     
     * @param pnr
     * @param clid
     * @return set result in object CLID and
     * set object BenefitCorporateRecognition
     */
    private void validRulesToOfferPreferredSeatsAndBags(PNR pnr,  Clid clid) {
    	if (null != clid) {
	    	for (CartPNR cartPNR : pnr.getCarts().getCollection()) {
	    		for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
	    			BookingClassMap bookingClassMap = bookedTraveler.getItineraryBookingClasses();
	    			// not apply for booking class V and S
	    	        if (null != bookingClassMap.getCollection()
	    	                    && !bookingClassMap.getCollection().isEmpty()
	    	                    && !"V".equalsIgnoreCase(bookingClassMap.getCollection().get(0).getBookingClass())
	    	                    && !"S".equalsIgnoreCase(bookingClassMap.getCollection().get(0).getBookingClass())) {

                            // only booking class "R", "N", "E", "T", "Q", "L", "H", "K", "U", "M", "B"
                            List<String> family = Arrays.asList("R", "N", "E", "T", "Q", "L", "H", "K", "U", "M", "B");
                            boolean isClassicFamily = false;
		                    for(String listDomesticClassic: family) {
                                if(listDomesticClassic.equalsIgnoreCase(bookingClassMap.getCollection().get(0).getBookingClass())){
                                    isClassicFamily = true;
                                }
                            }
                            MarketType marketFlight = pnr.getGlobalMarketFlight();
                            boolean isMarketFlightDomestic = false;
                            if("DOMESTIC".equalsIgnoreCase(marketFlight.DOMESTIC.toString())) {
                                isMarketFlightDomestic = true;
                            }
	    	            // apply benefit if not have a club premier tier
	    	            if (null == bookedTraveler.getBookedTravellerBenefitCollection()) {
	    	                // verify approved benefit by BI
	    	                if (null != clid.getApplyCorporateBenefit() && clid.getApplyCorporateBenefit()) {
	    	                    BenefitCorporateRecognition benefitCorporateRecognition = new BenefitCorporateRecognition();
                                if(isClassicFamily && isMarketFlightDomestic) {
                                    benefitCorporateRecognition.setPreferredBagsBenefit(true);
                                    clid.setPreferentBag(true);
                                }
                                benefitCorporateRecognition.setPreferredSeatsBenefit(true);
	    	                    benefitCorporateRecognition.setClid(clid.getCompanyNumber());
	    	                    bookedTraveler.setBenefitCorporateRecognition(benefitCorporateRecognition);
	    	                } else {
	    	                    clid.setApplyCorporateBenefit(false);
	    	                    clid.setMessage("Not Apply Corporate Benefit");
	    	                    LOG.info("CORPORATE RECOGNITION: Not Apply Corporate Benefit");
	    	                }
	    	            } else {
	    	                clid.setApplyCorporateBenefit(false);
	    	                clid.setMessage("Benefit priority");
	    	                LOG.info("CORPORATE RECOGNITION: Benefit priority");
	    	            }
	    	        } else {
	    	            clid.setApplyCorporateBenefit(false);
	    	            clid.setMessage("invalid fare");
	    	            LOG.info("CORPORATE RECOGNITION: invalid fare");
	    	        }
	    			break;
	    		}
	    	}
    	}
    }

    /**
     * @param bookedLegCollection
     * @param cartPNRCollection
     */
    private void setStandByInfo(
            BookedLegCollection bookedLegCollection,
            CartPNRCollection cartPNRCollection,
            PnrRQ pnrRQ
    ) {
        //update priority list

        for (CartPNR cartPNR : cartPNRCollection.getCollection()) {
            BookedLeg bookedLeg = bookedLegCollection.getBookedLegByLegCode(cartPNR.getLegCode());

            if (!cartPNR.isSeamlessCheckin()
                    && null != bookedLeg
                    && null != bookedLeg.getCheckinStatus()
                    //&& bookedLeg.isPriorityReservation()
                    && (BookedLegCheckinStatusType.OPEN.equals(bookedLeg.getCheckinStatus())
                    || BookedLegCheckinStatusType.CLOSED.equals(bookedLeg.getCheckinStatus())
                    || bookedLeg.getCheckinStatus().toString().contains("CLOSED_GROUND_HANDLING_BY"))) {

                if (SystemVariablesUtil.isAddRevenuePaxToPriorityListEnabled()) {
                    for (BookedSegment bookedSegment : bookedLeg.getSegments().getOpenSegmentsOperatedBy(AirlineCodeType.AM)) {

                        PassengersListRQ passengersListRQ = new PassengersListRQ();
                        passengersListRQ.setPos(pnrRQ.getPos());
                        passengersListRQ.setStore(pnrRQ.getStore());
                        passengersListRQ.setLanguage(pnrRQ.getLanguage());

                        if (null != bookedSegment && null != bookedSegment.getSegment()) {
                            passengersListRQ.setArrivalAirport(bookedSegment.getSegment().getArrivalAirport());
                            passengersListRQ.setDepartureAirport(bookedSegment.getSegment().getDepartureAirport());
                            passengersListRQ.setDepartureDate(bookedSegment.getSegment().getDepartureDateTime().substring(0, 10));
                            passengersListRQ.setOperatingCarrier(bookedSegment.getSegment().getOperatingCarrier());
                            passengersListRQ.setOperatingFlightCode(bookedSegment.getSegment().getOperatingFlightCode());
                        } else {
                            passengersListRQ.setLegCode(bookedLeg.getSegments().getLegCode());
                        }

                        try {
                            List<ServiceCall> serviceCallList = new ArrayList<>();
                            StandbyList standbyList;

                            if (SystemVariablesUtil.isRobotPassengerListEnabled()) {
                                standbyList = getPriorityListService().getNewStandbyList(
                                        passengersListRQ
                                );
                            } else {
                                standbyList = getPriorityListService().getStandbyList(
                                        passengersListRQ,
                                        serviceCallList
                                );
                            }

                            for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
                                CheckInStatus checkInStatus = bookedTraveler.getCheckinStatusBySegment().getCheckInStatus(
                                        bookedSegment.getSegment().getSegmentCode()
                                );
                                if (null != checkInStatus && checkInStatus.isCheckinStatus()) {
                                    for (PassengerOnList passenger : standbyList.getPassengers()) {
                                        if (null != passenger && null != bookedTraveler.getId()
                                                && bookedTraveler.getId().equalsIgnoreCase(passenger.getId())) {
                                            checkInStatus.setOnStandByList(true);
                                            break;
                                        }
                                    }
                                }
                            }

                        } catch (Exception ex) {
                            LOG.error(ex.getMessage(), ex);
                        }

                        try {
                            List<ServiceCall> serviceCallList = new ArrayList<>();
                            UpgradeList upgradeList;

                            if (SystemVariablesUtil.isRobotPassengerListEnabled()) {
                                upgradeList = getPriorityListService().getNewUpgradeList(
                                        passengersListRQ
                                );
                            } else {
                                upgradeList = getPriorityListService().getUpgradeList(
                                        passengersListRQ,
                                        serviceCallList
                                );
                            }

                            for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
                                CheckInStatus checkInStatus = bookedTraveler.getCheckinStatusBySegment().getCheckInStatus(
                                        bookedSegment.getSegment().getSegmentCode()
                                );
                                if (null != checkInStatus && checkInStatus.isCheckinStatus()) {
                                    for (PassengerOnList passenger : upgradeList.getPassengers()) {
                                        if (null != passenger && null != bookedTraveler.getId()
                                                && bookedTraveler.getId().equalsIgnoreCase(passenger.getId())) {
                                            checkInStatus.setOnUpgradeList(true);
                                            break;
                                        }
                                    }
                                }
                            }
                        } catch (Exception ex) {
                            LOG.error(ex.getMessage(), ex);
                        }
                    }
                }

                BookedSegment bookedSegment = bookedLeg.getSegments().getFirstOpenSegmentOperatedBy(AirlineCodeType.AM);
                PassengersListRQ passengersListRQ = new PassengersListRQ();
                passengersListRQ.setPos(pnrRQ.getPos());
                passengersListRQ.setStore(pnrRQ.getStore());
                passengersListRQ.setLanguage(pnrRQ.getLanguage());

                if (null != bookedSegment && null != bookedSegment.getSegment()) {
                    passengersListRQ.setArrivalAirport(bookedSegment.getSegment().getArrivalAirport());
                    passengersListRQ.setDepartureAirport(bookedSegment.getSegment().getDepartureAirport());
                    passengersListRQ.setDepartureDate(bookedSegment.getSegment().getDepartureDateTime().substring(0, 10));
                    passengersListRQ.setOperatingCarrier(bookedSegment.getSegment().getOperatingCarrier());
                    passengersListRQ.setOperatingFlightCode(bookedSegment.getSegment().getOperatingFlightCode());
                    passengersListRQ.setLegCode(bookedLeg.getSegments().getLegCode());
                } else {
                    passengersListRQ.setLegCode(bookedLeg.getSegments().getLegCode());
                }

                if (bookedLeg.isPriorityReservation() || bookedLeg.isOvsReservation()) {
                    try {
                        List<ServiceCall> serviceCallList = new ArrayList<>();
                        StandbyList standbyList;
                        if (SystemVariablesUtil.isRobotPassengerListEnabled()) {
                            LOG.info("PNR Lookup New Standby List");
                            standbyList = getPriorityListService().getNewStandbyList(
                                    passengersListRQ
                            );
                        } else {
                            LOG.info("PNR Lookup Classic Standby List");
                            standbyList = getPriorityListService().getStandbyList(
                                    passengersListRQ,
                                    serviceCallList
                            );
                        }


                        bookedLeg.setPaxOnStandByList(standbyList.getPassengers().size());

                        int i;
                        boolean onStandByList = true;
                        boolean mustContinue;

                        for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {

                            mustContinue = true;
                            i = 0;
                            while (i < standbyList.getPassengers().size() && mustContinue) {
                                PassengerOnList passenger = standbyList.getPassengers().get(i);

                                if (null != passenger && null != bookedTraveler.getId()
                                        && bookedTraveler.getId().equalsIgnoreCase(passenger.getId())) {
                                    bookedTraveler.setOnStandByList(true);
                                    mustContinue = false;
                                }

                                i++;
                            }

                            onStandByList &= bookedTraveler.isOnStandByList();
                        }

                        bookedLeg.setOnStandByList(onStandByList);

                    } catch (Exception ex) {
                        LOG.error(ex.getMessage(), ex);
                    }
                }

                try {
                    UpgradeList upgradeList;
                    List<ServiceCall> serviceCallList = new ArrayList<>();
                    if (SystemVariablesUtil.isRobotPassengerListEnabled()) {
                        LOG.info("PNR lookup New Upgrade List");
                        upgradeList = getPriorityListService().getNewUpgradeList(
                                passengersListRQ
                        );
                    } else {
                        LOG.info("PNR lookup Classic upgrade list");
                        upgradeList = getPriorityListService().getUpgradeList(
                                passengersListRQ,
                                serviceCallList
                        );
                    }


                    bookedLeg.setPaxOnUpgradeList(upgradeList.getPassengers().size());

                    int i;
                    boolean onUpgradeList = true;
                    boolean showUpgradeList = false;
                    boolean mustContinue;

                    for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
                        if (null != bookedTraveler.getId()) {
                            mustContinue = true;
                            i = 0;
                            while (i < upgradeList.getPassengers().size() && mustContinue) {
                                PassengerOnList passenger = upgradeList.getPassengers().get(i);

                                if (null != passenger && null != bookedTraveler.getId()
                                        && bookedTraveler.getId().equalsIgnoreCase(passenger.getId())) {
                                    bookedTraveler.setOnUpgradeList(true);
                                    //if (PassengerFareType.F.getCode().equalsIgnoreCase(bookedTraveler.getPassengerType())) {
                                    showUpgradeList = true;
                                    //}
                                    mustContinue = false;
                                }

                                i++;
                            }
                        }

                        onUpgradeList &= bookedTraveler.isOnUpgradeList();
                    }

                    bookedLeg.setOnUpgradeList(onUpgradeList);
                    bookedLeg.setShowUpgradeList(showUpgradeList);
                    bookedLeg.setUpgradeReservation(showUpgradeList);

                } catch (Exception ex) {
                    LOG.error(ex.getMessage(), ex);
                }

                if (bookedLeg.isPriorityReservation()) {
                    if (SystemVariablesUtil.getStandByPaxLimitPercentage() > 0.0) {
                        try {
                            Segment segment = getItineraryService().getFirstEligibleSegment(bookedLeg, pnrRQ.getPos());

                            if (null != segment) {
                                int authorized = 0;
                                int booked = 0;
                                int standByListPaxLimit;

                                if (null != segment.getCapacity()) {
                                    for (CabinCapacity cabinCapacity : segment.getCapacity()) {
                                        //only main cabin
                                        if ("main".equalsIgnoreCase(cabinCapacity.getCabin())) {
                                            authorized += cabinCapacity.getAuthorized();
                                            booked += cabinCapacity.getBooked();
                                            break;
                                        }
                                    }
                                }

                                standByListPaxLimit = authorized - booked;
                                standByListPaxLimit = Math.max(0, standByListPaxLimit);
                                standByListPaxLimit += (Math.round(authorized * SystemVariablesUtil.getStandByPaxLimitPercentage()));

                                bookedLeg.setStandByListPaxLimit(standByListPaxLimit);
                                bookedLeg.setCheckInRestrictedByStandbyConstraint(bookedLeg.getPaxOnStandByList() >= standByListPaxLimit);
                            }
                        } catch (Exception ex) {
                            LOG.error(ex.getMessage(), ex);

                            bookedLeg.setStandByListPaxLimit(0);
                            bookedLeg.setCheckInRestrictedByStandbyConstraint(false);
                        }
                    } else {
                        bookedLeg.setStandByListPaxLimit(0);
                        bookedLeg.setCheckInRestrictedByStandbyConstraint(false);
                    }
                }
            }
        }
    }

    /**
     * The default return value is an empty string
     *
     * @param tkneString
     * @param vcrValids
     * @return
     */
    public String getCouponFromTKNE(String tkneString, List<String> vcrValids) {
        if (null == vcrValids || vcrValids.isEmpty()) {
            return "";
        }

        Pattern pattern = Pattern.compile("[0-9]{13}C[0-9]");
        Matcher matcher = pattern.matcher(tkneString);

        Pattern patternVCR = Pattern.compile("[0-9]{13}");
        Matcher matcherVCR = patternVCR.matcher(tkneString);

        if (matcher.find() && matcherVCR.find()) {
            String vcr = matcherVCR.group(0);
            if (vcrValids.contains(vcr)) {
                String vcrCoupon = tkneString.substring(matcher.start(), matcher.end());
                return vcrCoupon.substring(vcrCoupon.indexOf("C") + 1);
            }
        }
        return "";
    }

    /**
     * The default return value is an empty string
     *
     * @param tkneString
     * @return
     */
    /*public String getSegmentInfo(String tkneString) {
        Pattern pattern = Pattern.compile("[A-Z]{6}[0-9]{4}");
        Matcher matcher = pattern.matcher(tkneString);
        if (matcher.find()) {
            String vcrCoupon = matcher.group(0);
            return vcrCoupon;
        }
        return "";
    }*/


    /**
     * @param pnrRQ
     * @param creationDate
     * @param getReservationRS
     * @param ticketingDocumentRS
     * @param passengerDataResponse
     * @param bookedLegCollection
     * @param reservationUpdateItemTypeList
     * @param reservationUpdateSeatItemTypeList
     * @param serviceCallList
     * @return
     * @throws ParseException
     * @throws Exception
     */
    private CartPNRCollection getCartPNRCollection(
            PnrRQ pnrRQ,
            String creationDate,
            GetReservationRS getReservationRS,
            GetTicketingDocumentRS ticketingDocumentRS,
            GetPassengerDataRSACS passengerDataResponse,
            BookedLegCollection bookedLegCollection,
            List<ReservationUpdateItemType> reservationUpdateItemTypeList,
            List<ReservationUpdateItemType> reservationUpdateSeatItemTypeList,
            List<ServiceCall> serviceCallList
    ) throws ParseException, Exception {

        CartPNRCollection cartPNRCollection = new CartPNRCollection();

        List<AncillaryServicesPNRB>  ancillaryList = PassengerAncillaryUtil.getAncillaryList(getReservationRS);

        /*
        Type listType = new TypeToken<ArrayList<AncillaryService>>() {
        }.getType();
        LOG.info("ancillaryList: " + new Gson().toJson(ancillaryList, listType));
        */


        List<PassengerPNRB> passengerDetails = PNRLookUpServiceUtil.getPassengerDetails(getReservationRS);
        List<OpenReservationElementType> specialServiceInfo = PNRLookUpServiceUtil.getSSRInformation(getReservationRS);

        //TODO test with corporate frequent traveler pnr
        String[] corporateResults;
        corporateResults = PNRLookUpServiceUtil.getCorporateNumbers(getReservationRS);
        LOG.info("CorporateResults GetRes {} ", new Gson().toJson(corporateResults));
//        TravelItineraryReadRS travelItineraryReadRS = getTravelItineraryService().getTravelItineraryReadRS(pnrRQ, serviceCallList);
//        String[] corporateResultss;
//        corporateResultss = PNRLookUpServiceUtil.getCorporateNumbers(travelItineraryReadRS);
//        LOG.info("CorporateResults TIR {} ", new Gson().toJson(corporateResultss));


        List<FrequentFlyerPNRB> listCustLoyalty = PNRLookUpServiceUtil.getCustLoyalty(getReservationRS);

        String typeOfTransformation = PNRLookUpServiceUtil.typeOfTransformation(bookedLegCollection.getCollection());

        LOG.info("typeOfTransformation: " + typeOfTransformation);

        BookedLeg bookedLeg;

        switch (typeOfTransformation) {
            case "cancelledFlights":
                /*
                bookedLeg = PNRLookUpServiceUtil.getLastCancelledFlightInLegColl(bookedLegCollection.getCollection());

                setLastCloseFlight(
                        getReservationRS,
                        bookedLeg,
                        cartPNRCollection,
                        ancillaryList,
                        passengerDetails,
                        specialServiceInfo,
                        corporateResults,
                        passengerDataResponse,
                        serviceCallList,
                        pnrRQ,
                        reservationUpdateItemTypeList,
                        reservationUpdateSeatItemTypeList,
                        listCustLoyalty,
                        creationDate
                );
                */
                break;
            case "flownFlights":
                //Return only the latest leg into the cartCollection Object.
                bookedLeg = PNRLookUpServiceUtil.getLastFlownFlightInLegColl(bookedLegCollection.getCollection());

                cartPNRCollection.getCollection().add(
                        getBasicCartPNR(
                                getReservationRS,
                                bookedLeg,
                                passengerDetails,
                                ancillaryList,
                                specialServiceInfo,
                                corporateResults,
                                pnrRQ,
                                reservationUpdateItemTypeList,
                                reservationUpdateSeatItemTypeList,
                                listCustLoyalty,
                                creationDate,
                                serviceCallList
                        )
                );
                break;

            case "closedFlights":
                //return the last leg closed
                //Do common flow.

                bookedLeg = PNRLookUpServiceUtil.getLastClosedFlightInLegColl(bookedLegCollection.getCollection());

                setLastCloseFlight(
                        getReservationRS,
                        bookedLeg,
                        cartPNRCollection,
                        ancillaryList,
                        passengerDetails,
                        specialServiceInfo,
                        corporateResults,
                        passengerDataResponse,
                        serviceCallList,
                        pnrRQ,
                        reservationUpdateItemTypeList,
                        reservationUpdateSeatItemTypeList,
                        listCustLoyalty,
                        creationDate
                );
                break;
            case "openFlights":
            case "groundHandled":
            case "notOpByAM":
                //case "finalFlights":

                List<BookedLeg> notSeamlessLegs = bookedLegCollection.getNotSeamlessLegs();

                BookedLegCollection notSeamlessLegsCollection = new BookedLegCollection();
                notSeamlessLegsCollection.getCollection().addAll(notSeamlessLegs);

                if (!notSeamlessLegsCollection.getCollection().isEmpty()) {

                    //Do common flow.
                    buildCartPNRCollection(
                            getReservationRS,
                            notSeamlessLegsCollection,
                            cartPNRCollection,
                            ancillaryList,
                            passengerDetails,
                            specialServiceInfo,
                            corporateResults,
                            passengerDataResponse,
                            serviceCallList,
                            pnrRQ,
                            reservationUpdateItemTypeList,
                            reservationUpdateSeatItemTypeList,
                            listCustLoyalty,
                            creationDate
                    );
                }

                List<BookedLeg> seamlessLegs = bookedLegCollection.getOpenSeamlessLegs();

                if (null != seamlessLegs && !seamlessLegs.isEmpty()) {

                    for (BookedLeg seamlessLeg : seamlessLegs) {
                        try {
                            CartPNR seamlessCart = seamlessTravelPartyService.getCarts(
                                    pnrRQ,
                                    seamlessLeg,
                                    getReservationRS,
                                    ticketingDocumentRS,
                                    reservationUpdateItemTypeList,
                                    reservationUpdateSeatItemTypeList
                            );

                            cartPNRCollection.getCollection().add(seamlessCart);
                        } catch (Exception ex) {

                            SeamlessError seamlessError = new SeamlessError();

                            boolean afterCheckinEndDateTime = false;
                            boolean beforeCheckinStartDateTime = false;

                            if (ex instanceof SeamlessException) {
                                SeamlessException sEx = (SeamlessException) ex;

                                Incident incident = SeamlessIncidentUtil.getSeamlessIncident(sEx, pnrRQ.getPos());

                                if (null != incident) {

                                    //AFTER CHECKIN END DATE TIME (SEAMLESS/WEB/APP/KIOSK)
                                    if (incident.getCode().equalsIgnoreCase("9020001")
                                            || incident.getCode().equalsIgnoreCase("12111954")
                                            || incident.getCode().equalsIgnoreCase("22111954"))  {
                                        afterCheckinEndDateTime = true;
                                    }

                                    //BEFORE CHECKIN START DATE TIME (SEAMLESS/WEB/APP/KIOSK)
                                    if (incident.getCode().equalsIgnoreCase("9020000")
                                            || incident.getCode().equalsIgnoreCase("12111953")
                                            || incident.getCode().equalsIgnoreCase("22111953"))  {
                                        beforeCheckinStartDateTime = true;
                                    }

                                    seamlessError.setErrorCode(incident.getCode());
                                    seamlessError.setErrorMessage(incident.getMsg());
                                } else {
                                    seamlessError.setErrorMessage(ex.getMessage());
                                }
                            } else {
                                seamlessError.setErrorMessage(ex.getMessage());
                            }

                            seamlessLeg.setCheckinStatus(seamlessLeg.getOriginalCheckinStatus());
                            seamlessLeg.setSeamlessCheckin(false);

                            CartPNR basicSeamlessCart = getBasicCartPNR(
                                    getReservationRS,
                                    seamlessLeg,
                                    passengerDetails,
                                    ancillaryList,
                                    specialServiceInfo,
                                    corporateResults,
                                    pnrRQ,
                                    reservationUpdateItemTypeList,
                                    reservationUpdateSeatItemTypeList,
                                    listCustLoyalty,
                                    creationDate,
                                    serviceCallList
                            );

                            if (afterCheckinEndDateTime || beforeCheckinStartDateTime) {

                                if (afterCheckinEndDateTime) {
                                    seamlessLeg.setFlightStatus(BookedLegFlightStatusType.CLOSED);
                                    seamlessLeg.setCheckinStatus(BookedLegCheckinStatusType.CLOSED);

                                    try {
                                        Segment segment = getItineraryService().getFirstEligibleSegment(seamlessLeg, pnrRQ.getPos());
                                        segment.setCheckinStatus(BookedLegCheckinStatusType.CLOSED);
                                    } catch (Exception exception) {
                                        //
                                    }
                                } else if (beforeCheckinStartDateTime) {
                                    //seamlessLeg.setFlightStatus(BookedLegFlightStatusType.ON_TIME);
                                    seamlessLeg.setCheckinStatus(BookedLegCheckinStatusType.CLOSED_INTERNATIONAL_WINDOW);

                                    try {
                                        Segment segment = getItineraryService().getFirstEligibleSegment(seamlessLeg, pnrRQ.getPos());
                                        segment.setCheckinStatus(BookedLegCheckinStatusType.CLOSED_INTERNATIONAL_WINDOW);
                                    } catch (Exception exception) {
                                        //
                                    }
                                }

                            } else {
                                basicSeamlessCart.setSeamlessError(seamlessError);
                            }

                            cartPNRCollection.getCollection().add(
                                    basicSeamlessCart
                            );

                        }
                    }
                }

                break;

            case "closeFutureFlights":
                //only return the close flight if there is not kiosk channel
                if (!PosType.KIOSK.toString().equalsIgnoreCase(pnrRQ.getPos())) {

                    //Get last closed leg available in the collection
                    bookedLeg = PNRLookUpServiceUtil.getLastFinalClosedFlightInLegColl(bookedLegCollection.getCollection());

                    if (null == bookedLeg) {
                        bookedLeg = PNRLookUpServiceUtil.getLastClosedFlightInLegColl(bookedLegCollection.getCollection());
                    }

                    if (null != bookedLeg) {
                        setLastCloseFlight(
                                getReservationRS,
                                bookedLeg,
                                cartPNRCollection,
                                ancillaryList,
                                passengerDetails,
                                specialServiceInfo,
                                corporateResults,
                                passengerDataResponse,
                                serviceCallList,
                                pnrRQ,
                                reservationUpdateItemTypeList,
                                reservationUpdateSeatItemTypeList,
                                listCustLoyalty,
                                creationDate
                        );
                    }
                }

                //Get first Future leg available in the collection
                bookedLeg = PNRLookUpServiceUtil.getFirstFutureFlightInLegColl(bookedLegCollection.getCollection());
                cartPNRCollection.getCollection().add(
                        getBasicCartPNR(
                                getReservationRS,
                                bookedLeg,
                                passengerDetails,
                                ancillaryList,
                                specialServiceInfo,
                                corporateResults,
                                pnrRQ,
                                reservationUpdateItemTypeList,
                                reservationUpdateSeatItemTypeList,
                                listCustLoyalty,
                                creationDate,
                                serviceCallList
                        )
                );
                break;

            case "futureFlights":
                //Return only the first leg into the cartCollection Object.
                //Get first Future leg available in the collection
                bookedLeg = PNRLookUpServiceUtil.getFirstFutureFlightInLegColl(bookedLegCollection.getCollection());
                cartPNRCollection.getCollection().add(
                        getBasicCartPNR(
                                getReservationRS,
                                bookedLeg,
                                passengerDetails,
                                ancillaryList,
                                specialServiceInfo,
                                corporateResults,
                                pnrRQ,
                                reservationUpdateItemTypeList,
                                reservationUpdateSeatItemTypeList,
                                listCustLoyalty,
                                creationDate,
                                serviceCallList
                        )
                );
                break;

        }

        return cartPNRCollection;
    }

    /**
     * @param getReservationRS
     * @param bookedLeg
     * @param cartPNRCollection
     * @param ancillaryList
     * @param passengerDetails
     * @param specialServiceInfo
     * @param corporateResults
     * @param passengerDataResponse
     * @param serviceCallList
     * @param pnrRQ
     * @param reservationUpdateItemTypeList
     * @param reservationUpdateSeatItemTypeList
     * @param listCustLoyalty
     * @param creationDate
     * @throws Exception
     */
    private void setLastCloseFlight(
            GetReservationRS getReservationRS,
            BookedLeg bookedLeg,
            CartPNRCollection cartPNRCollection,
            List<AncillaryServicesPNRB> ancillaryList,
            List<PassengerPNRB> passengerDetails,
            List<OpenReservationElementType> specialServiceInfo,
            String[] corporateResults,
            GetPassengerDataRSACS passengerDataResponse,
            List<ServiceCall> serviceCallList,
            PnrRQ pnrRQ,
            List<ReservationUpdateItemType> reservationUpdateItemTypeList,
            List<ReservationUpdateItemType> reservationUpdateSeatItemTypeList,
            List<FrequentFlyerPNRB> listCustLoyalty,
//            TravelItineraryReadRS travelItineraryReadRS,
            String creationDate
    ) throws Exception {

        /*
        boolean notInitialized = false;

        if (!ErrorOrSuccessCode.SUCCESS.equals(passengerDataResponse.getResult().getStatus())) {
            if (null != passengerDataResponse.getResult()
                    && null != passengerDataResponse.getResult().getSystemSpecificResults()
                    && !passengerDataResponse.getResult().getSystemSpecificResults().isEmpty()
                    && passengerDataResponse.getResult().getSystemSpecificResults().get(0).getErrorMessage().getValue().contains("NOT INITIALIZED")) {
                notInitialized = true;
            }
        }
        */

        try {

            if (bookedLeg.isSeamlessCheckin()) {

                cartPNRCollection.getCollection().add(
                        getBasicCartPNR(
                                getReservationRS,
                                bookedLeg,
                                passengerDetails,
                                ancillaryList,
                                specialServiceInfo,
                                corporateResults,
                                pnrRQ,
                                reservationUpdateItemTypeList,
                                reservationUpdateSeatItemTypeList,
                                listCustLoyalty,
                                creationDate,
                                serviceCallList
                        )
                );

            } else {

                //return the last leg
                BookedLegCollection lastLeg = new BookedLegCollection();
                lastLeg.getCollection().add(bookedLeg);

                //Do common flow.
                buildCartPNRCollection(
                        getReservationRS,
                        lastLeg,
                        cartPNRCollection,
                        ancillaryList,
                        passengerDetails,
                        specialServiceInfo,
                        corporateResults,
                        passengerDataResponse,
                        serviceCallList,
                        pnrRQ,
                        reservationUpdateItemTypeList,
                        reservationUpdateSeatItemTypeList,
                        listCustLoyalty,
                        creationDate
                );
            }

        } catch (Exception ex) {
            LOG.error("Error getting passenger info for closed flight. " + ex.getMessage(), ex);

            cartPNRCollection.getCollection().add(
                    getBasicCartPNR(
                            getReservationRS,
                            bookedLeg,
                            passengerDetails,
                            ancillaryList,
                            specialServiceInfo,
                            corporateResults,
                            pnrRQ,
                            reservationUpdateItemTypeList,
                            reservationUpdateSeatItemTypeList,
                            listCustLoyalty,
                            creationDate,
                            serviceCallList
                    )
            );
        }
    }

    /**
     * @param getReservationRS
     * @param bookedLeg
     * @param passengerDetails
     * @param ancillaryList
     * @param specialServiceInfo
     * @param corporateResults
     * @param pnrRQ
     * @param reservationUpdateItemTypeList
     * @param reservationUpdateSeatItemTypeList
     * @param listCustLoyalty
     * @param creationDate
     * @param serviceCallList
     * @return
     * @throws Exception
     */
    private CartPNR getBasicCartPNR(
            GetReservationRS getReservationRS,
            BookedLeg bookedLeg,
            List<PassengerPNRB> passengerDetails,
            List<AncillaryServicesPNRB> ancillaryList,
            List<OpenReservationElementType> specialServiceInfo,
            String[] corporateResults,
            PnrRQ pnrRQ,
            List<ReservationUpdateItemType> reservationUpdateItemTypeList,
            List<ReservationUpdateItemType> reservationUpdateSeatItemTypeList,
            List<FrequentFlyerPNRB> listCustLoyalty,
            String creationDate,
            List<ServiceCall> serviceCallList
    ) throws Exception {

        CartPNR cartPNR = new CartPNR();
        cartPNR.setSeamlessCheckin(bookedLeg.isSeamlessCheckin());
        cartPNR.setLegCode(bookedLeg.getSegments().getLegCode());
        cartPNR.setTravelersCode(getTravelersCode(passengerDetails));

        //Retrieve first AM flight from the leg.
        //BookedSegment bookedSegment = bookedLeg.getFirstOpenSegment();

        BookedTravelerCollection bookedTravelerCollection;
        bookedTravelerCollection = getBookedTravelerCollection(
                getReservationRS,
                null,
                passengerDetails,
                specialServiceInfo,
                bookedLeg,
                false,
                corporateResults,
                reservationUpdateItemTypeList,
                reservationUpdateSeatItemTypeList,
                listCustLoyalty,
                creationDate,
                pnrRQ,
                serviceCallList
        );

        cartPNR.setTravelerInfo(bookedTravelerCollection);
        cartPNR.getMeta().setCartId(CommonUtil.createCartId());
        cartPNR.getMeta().setCartExpirationTimeInSeconds(1800);
        cartPNR.getMeta().setPos(new POS(pnrRQ.getPos(), pnrRQ.getStore(), pnrRQ.getLanguage()));

        return cartPNR;
    }

    /**
     * @param getReservationRS
     * @param legs
     * @param cartPNRCollection
     * @param ancillaryList
     * @param passengerDetails
     * @param specialServiceInfo
     * @param corporateResults
     * @param passengerDataResponse
     * @param serviceCallList
     * @param pnrRQ
     * @param reservationUpdateItemTypeList
     * @param reservationUpdateSeatItemTypeList
     * @param listCustLoyalty
     * @param creationDate
     * @throws Exception
     */
    private void buildCartPNRCollection(
            GetReservationRS getReservationRS,
            BookedLegCollection legs,
            CartPNRCollection cartPNRCollection,
            List<AncillaryServicesPNRB> ancillaryList,
            List<PassengerPNRB> passengerDetails,
            List<OpenReservationElementType> specialServiceInfo,
            String[] corporateResults,
            GetPassengerDataRSACS passengerDataResponse,
            List<ServiceCall> serviceCallList,
            PnrRQ pnrRQ,
            List<ReservationUpdateItemType> reservationUpdateItemTypeList,
            List<ReservationUpdateItemType> reservationUpdateSeatItemTypeList,
            List<FrequentFlyerPNRB> listCustLoyalty,
            String creationDate
    ) throws Exception {

        boolean notInitialized = false;
        if (!ErrorOrSuccessCode.SUCCESS.equals(passengerDataResponse.getResult().getStatus())) {
            if (null != passengerDataResponse.getResult()
                    && null != passengerDataResponse.getResult().getSystemSpecificResults()
                    && !passengerDataResponse.getResult().getSystemSpecificResults().isEmpty()
                    && passengerDataResponse.getResult().getSystemSpecificResults().get(0).getErrorMessage().getValue().contains("NOT INITIALIZED")) {
                notInitialized = true;
            }
        }

        List<CartPNR> cartPNRList = cartPNRCollection.getCollection();
        int lastLeg = legs.getCollection().size();
        int legNumber = 0;
        for (BookedLeg bookedLeg : legs.getCollection()) {
            legNumber++;

            BookedSegment bookedSegment = bookedLeg.getFirstOpenSegment();

            String departureDate = bookedSegment.getSegment().getDepartureDateTime();
            Integer daysBeforeDeparture = this.calculateDaysBeforeDeparture(departureDate);
            bookedSegment.getSegment().setDaysBeforeDeparture(daysBeforeDeparture);

            if (PNRLookUpServiceUtil.isEligibleToBeAddedToTheCart(
                    bookedLeg, pnrRQ.getPos(), (legNumber == lastLeg)
            )) {

                CartPNR cartPNR = new CartPNR();
                cartPNR.setLegCode(bookedLeg.getSegments().getLegCode());
                cartPNR.setTravelersCode(getTravelersCode(passengerDetails));

                if (null != passengerDataResponse.getPassengerDataResponseList()) {

                    BookedTravelerCollection bookedTravelerCollection;
                    bookedTravelerCollection = getBookedTravelerCollection(
                            getReservationRS,
                            passengerDataResponse.getPassengerDataResponseList().getPassengerDataResponse(),
                            passengerDetails,
                            specialServiceInfo,
                            bookedLeg,
                            notInitialized,
                            corporateResults,
                            reservationUpdateItemTypeList,
                            reservationUpdateSeatItemTypeList,
                            listCustLoyalty,
                            creationDate,
                            pnrRQ,
                            serviceCallList
                    );
                    cartPNR.setTravelerInfo(bookedTravelerCollection);

                } else {

                    BookedTravelerCollection bookedTravelerCollection;
                    bookedTravelerCollection = getBookedTravelerCollection(
                            getReservationRS,
                            null,
                            passengerDetails,
                            specialServiceInfo,
                            bookedLeg,
                            notInitialized,
                            corporateResults,
                            reservationUpdateItemTypeList,
                            reservationUpdateSeatItemTypeList,
                            listCustLoyalty,
                            creationDate,
                            pnrRQ,
                            serviceCallList
                    );
                    cartPNR.setTravelerInfo(bookedTravelerCollection);
                }

                cartPNR.getMeta().setCartId(CommonUtil.createCartId());
                cartPNR.getMeta().setCartExpirationTimeInSeconds(1800);

                POS pos = new POS();
                pos.setChannel(pnrRQ.getPos());
                pos.setStore(pnrRQ.getStore());
                pos.setLanguage(pnrRQ.getLanguage());

                cartPNR.getMeta().setPos(pos);

                //Upgrade
                if (isEligibleForUpgrade(pnrRQ, cartPNR)
                        && (!BrandedFareRulesUtil.isBrandedFare_1_0(bookedLeg, creationDate)
                        || !BrandedFareRulesUtil.isBasicEconomy(bookedLeg, creationDate))) {

                    try {

                        UpgradeAncillaryCollection upgradeAncillaryCollection;
                        upgradeAncillaryCollection = getUpgradeAncillaryCollection(
                                cartPNR,
                                bookedLeg,
                                pnrRQ,
                                serviceCallList
                        );

                        cartPNR.setUpgradeAncillaries(
                                upgradeAncillaryCollection
                        );
                    } catch (Exception e) {
                        LOG.error(e.getMessage(), e);
                    }

                    try {
                        if (!bookedLeg.isStandByReservation() || bookedLeg.getCheckinStatus().toString().equalsIgnoreCase(bookedLeg.getCheckinStatus().CHECKIN_NOT_ALLOWED_RESTRICTED_CHECKIN.toString())) {
                            CabinUpgradeAncillaryCollection cabinUpgradeAncillaryCollection;
                            cabinUpgradeAncillaryCollection = getUpsellOffers(
                                    cartPNR.getTravelerInfo().getCollection().get(0).getBookingClasses(),
                                    cartPNR.getUpgradeAncillaries(),
                                    legs,
                                    getTotalPax(cartPNR.getTravelersCode()), pnrRQ,
                                    serviceCallList
                            );

                            cartPNR.setUpsellOffers(
                                    cabinUpgradeAncillaryCollection
                            );
                        }
                    } catch (Exception e) {
                        LOG.error(e.getMessage(), e);
                    }
                }

                cartPNRList.add(cartPNR);
            } else {
                //Add case when GroundHandled
                if ((bookedLeg.getCheckinStatus().toString().contains("CLOSED_GROUND_HANDLING_BY")
                        || bookedLeg.getCheckinStatus().toString().contains("CLOSED_NOT"))
                        && BookedLegFlightStatusType.ON_TIME.equals(bookedLeg.getFlightStatus())) {

                    cartPNRList.add(
                            getBasicCartPNR(
                                    getReservationRS,
                                    bookedLeg,
                                    passengerDetails,
                                    ancillaryList,
                                    specialServiceInfo,
                                    corporateResults,
                                    pnrRQ,
                                    reservationUpdateItemTypeList,
                                    reservationUpdateSeatItemTypeList,
                                    listCustLoyalty,
                                    creationDate,
                                    serviceCallList
                            )
                    );
                }
            }
        }
    }

    /**
     * @param pnrRQ
     * @param cartPNR
     * @return
     */
    private boolean isEligibleForUpgrade(PnrRQ pnrRQ, CartPNR cartPNR) {
        return pnrRQ.getPremiereUpgrade() && isPassengerEligibleForUpgrade(cartPNR);
    }

    /**
     * @param cartPNR
     * @return
     */
    private boolean isPassengerEligibleForUpgrade(CartPNR cartPNR) {

        for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {

            //Is there an infant traveling with the adult?
            if (bookedTraveler.getInfant() != null) {
                return false;
            }

            //Is this a non-revenue employee stand-by booking?
            if (!PassengerFareType.F.getCode().equalsIgnoreCase(bookedTraveler.getPassengerType())) {
                return false;
            }

        }

        return true;
    }

    /**
     * @param travelersCode A1_C0_I0_PH0_PC0
     * @return
     */
    private int getTotalPax(String travelersCode) {
        int totalPax = 0;
        String[] pax = travelersCode.split("_");
        //Getting ADT
        try {
            totalPax += Integer.valueOf(pax[0].replace("A", ""));
            totalPax += Integer.valueOf(pax[1].replace("C", ""));
        } catch (Exception ex) {
            //Continue
        }

        //Getting INF
        return totalPax;
    }

    /**
     * @param bookingClassMap
     * @param upgradeAncillaries
     * @return getSegmentCode- MEX_GDL_AM_2016-10-20_2000
     */
    private CabinUpgradeAncillaryCollection getUpsellOffers(
            BookingClassMap bookingClassMap,
            UpgradeAncillaryCollection upgradeAncillaries,
            BookedLegCollection legs,
            int totalPax, PnrRQ pnrRQ,
            List<ServiceCall> serviceCallList
    ) {

        CabinUpgradeAncillaryCollection cabinUpgradeAncillaryCollection = new CabinUpgradeAncillaryCollection();

        for (BookingClass bookingClass : bookingClassMap.getCollection()) {

            CabinUpgradeAncillary cabinUpgradeAncillary = new CabinUpgradeAncillary();
            cabinUpgradeAncillary.setSegmentCode(bookingClass.getSegmentCode());

            BookedLeg bookedLeg = legs.getBookedLegBySegmentCode(bookingClass.getSegmentCode());

            if (null != bookedLeg) {
                cabinUpgradeAncillary.setLegCode(bookedLeg.getSegments().getLegCode());
            }

            try {
                BookedSegment currentBookedSegment = bookedLeg.getBookedSegment(bookingClass.getSegmentCode());

                cabinUpgradeAncillary.setSeatsRemaining(
                        getAvailableSpacesInCabin(
                                currentBookedSegment, serviceCallList
                        )
                );

                cabinUpgradeAncillary.setCurrency(
                        getCurrencyValueFromAncillary(
                                bookingClass.getSegmentCode(),
                                upgradeAncillaries
                        )
                );

                if (PosType.WEB.toString().equalsIgnoreCase(pnrRQ.getPos())) {

                    cabinUpgradeAncillary.setAncillary(
                            getAncillaryFromUpgradeAncillary(
                                    bookingClass.getSegmentCode(),
                                    upgradeAncillaries
                            )
                    );

                    if (isEligibleForUpgrade(bookingClass, cabinUpgradeAncillary.getSeatsRemaining(), totalPax)
                            && null != cabinUpgradeAncillary.getCurrency()) {
                        cabinUpgradeAncillary.setUpgradeAvailable(true);
                        cabinUpgradeAncillaryCollection.getCollection().add(cabinUpgradeAncillary);
                    } else {
                        cabinUpgradeAncillary = new CabinUpgradeAncillary();
                        cabinUpgradeAncillary.setSegmentCode(bookingClass.getSegmentCode());

                        if (null != bookedLeg) {
                            cabinUpgradeAncillary.setLegCode(bookedLeg.getSegments().getLegCode());
                        }

                        cabinUpgradeAncillary.setUpgradeAvailable(false);
                        cabinUpgradeAncillaryCollection.getCollection().add(cabinUpgradeAncillary);
                        LOG.info("Not eligible for upgrade offer.");
                    }
                } else {
                    if (isEligibleForUpgrade(bookingClass, cabinUpgradeAncillary.getSeatsRemaining(), totalPax)
                            && null != cabinUpgradeAncillary.getCurrency()) {
                        cabinUpgradeAncillary.setUpgradeAvailable(true);
                        cabinUpgradeAncillaryCollection.getCollection().add(cabinUpgradeAncillary);
                    } else {
                        LOG.info("Not eligible for upgrade offer.");
                    }
                }
            } catch (Exception ex) {
                //java.util.logging.Logger.getLogger(PNRLookupService.class.getName()).LOG(Level.SEVERE, null, ex);
                LOG.error(ex.getMessage(), ex);
            }
        }

        return cabinUpgradeAncillaryCollection;
    }

    private boolean isEligibleForUpgrade(BookingClass bookingClass, Integer availableSpacesInCabin, int totalPax) {
        //Validating CabinClass.
        if (null != bookingClass) {
            if ("Y".equalsIgnoreCase(bookingClass.getBookingCabin())) {
                if (!("W".equalsIgnoreCase(bookingClass.getBookingClass()))) {
                    if (availableSpacesInCabin > 0 && availableSpacesInCabin >= totalPax) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    private CurrencyValue getCurrencyValueFromAncillary(String segmentCode, UpgradeAncillaryCollection
            upgradeAncillaries) {
        for (UpgradeAncillary upgradeAncillary : upgradeAncillaries.getCollection()) {
            if (segmentCode.equalsIgnoreCase(upgradeAncillary.getSegmentCode())) {
                return new CurrencyValue(upgradeAncillary.getAncillaryOffer().getAncillary().getCurrency());
            }
        }

        return null;
    }

    private Ancillary getAncillaryFromUpgradeAncillary(String segmentCode, UpgradeAncillaryCollection
            upgradeAncillaries) {
        for (UpgradeAncillary upgradeAncillary : upgradeAncillaries.getCollection()) {
            if (SegmentCodeUtil.compareSegmentCodeWithoutTime(segmentCode, upgradeAncillary.getSegmentCode())) {
                return upgradeAncillary.getAncillaryOffer().getAncillary();
            }
        }

        return null;
    }

    private String getFlightNumberBySegmentCode(String segmentCode, List<BookedLeg> legs) {

        for (BookedLeg bookedLeg : legs) {
            for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {
                if (segmentCode.equalsIgnoreCase(bookedSegment.getSegment().getSegmentCode())) {
                    return bookedSegment.getSegment().getOperatingFlightCode();
                }
            }
        }

        return "";
    }

    /**
     * @param currentBookedSegment
     * @param serviceCallList
     * @return
     */
    private Integer getAvailableSpacesInCabin(BookedSegment currentBookedSegment, List<ServiceCall> serviceCallList) {

        if (null == currentBookedSegment) {
            return 0;
        }

        return getFlightDetailsService().getAvailabilityFromSabre(currentBookedSegment, serviceCallList);
    }

    /**
     * @param cartPNR
     * @param bookedLeg
     * @param pnrRQ
     * @param serviceCallList
     * @return
     */
    private UpgradeAncillaryCollection getUpgradeAncillaryCollection(
            CartPNR cartPNR,
            BookedLeg bookedLeg,
            PnrRQ pnrRQ,
            List<ServiceCall> serviceCallList
    ) {

        BookingClassMap bookingClassMap = cartPNR.getTravelerInfo().getCollection().get(0).getBookingClasses();

        UpgradeAncillaryCollection upgradeAncillaryCollection = new UpgradeAncillaryCollection();
        List<UpgradeAncillary> listUpgradeAncillary = new ArrayList<>();

        upgradeAncillaryCollection.setCollection(listUpgradeAncillary);

        for (BookingClass bookingClass : bookingClassMap.getCollection()) {

            PricedAncillaryServicePNRB pricedAncillaryServicePNRB = getAncillariesUpgrade(
                    bookingClass.getSegmentCode(),
                    bookingClass.getBookingClass(),
                    pnrRQ,
                    serviceCallList
            );

            if (null != pricedAncillaryServicePNRB) {

                AncillaryOffer ancillaryOffer = new AncillaryOffer();
                Ancillary ancillary = new Ancillary();
                ancillary.setType(pricedAncillaryServicePNRB.getRficSubcode());
                ancillary.setCommercialName(pricedAncillaryServicePNRB.getCommercialName());
                ancillary.setRficCode(pricedAncillaryServicePNRB.getRficCode());
                ancillary.setRficSubcode(pricedAncillaryServicePNRB.getRficSubcode());
                ancillary.setGroupCode("IE");
                ancillary.setOwningCarrierCode(pricedAncillaryServicePNRB.getOwningCarrierCode());
                ancillary.setVendor(pricedAncillaryServicePNRB.getVendor());
                ancillary.setEmdType(pricedAncillaryServicePNRB.getEMDType());
                ancillary.setPoints(0);

                OptionalAncillaryServicesInformationDataPNRB ancillaryData;
                ancillaryData = pricedAncillaryServicePNRB.getOptionalAncillaryServiceInformation().get(0);
                ancillary.setSegmentIndicator("S");

                CurrencyValue currencyValue;

                try {
                    String currencyCode = ancillaryData.getTTLPrice().getCurrency();

                    currencyValue = CurrencyUtil.getTestCurrencyValue(currencyCode);

                    if (null == currencyValue) {
                        currencyValue = new CurrencyValue();

                        currencyValue.setCurrencyCode(currencyCode);

                        currencyValue.setTotal(ancillaryData.getTTLPrice().getPrice());

                        if (null != ancillaryData.getOriginalBasePrice()
                                && currencyCode.equalsIgnoreCase(ancillaryData.getOriginalBasePrice().getCurrency())) {
                            currencyValue.setBase(ancillaryData.getOriginalBasePrice().getPrice());

                            if (null != ancillaryData.getEquivalentPrice()) {
                                currencyValue.setEquivalent(ancillaryData.getEquivalentPrice().getPrice());
                                currencyValue.setEquivalentCurrencyCode(ancillaryData.getEquivalentPrice().getCurrency());
                            }

                        } else if (null != ancillaryData.getEquivalentPrice()
                                && currencyCode.equalsIgnoreCase(ancillaryData.getEquivalentPrice().getCurrency())) {

                            currencyValue.setBase(ancillaryData.getEquivalentPrice().getPrice());

                            if (null != ancillaryData.getOriginalBasePrice()) {
                                currencyValue.setEquivalent(ancillaryData.getOriginalBasePrice().getPrice());
                                currencyValue.setEquivalentCurrencyCode(ancillaryData.getOriginalBasePrice().getCurrency());
                            }
                        }

                        try {
                            if (null != ancillaryData.getTaxes()) {
                                BigDecimal totalTax = BigDecimal.ZERO;
                                for (AncillaryTaxPNRB ancillaryTaxPNRB : ancillaryData.getTaxes().getTax()) {
                                    totalTax = totalTax.add(ancillaryTaxPNRB.getTaxAmount());
                                    currencyValue.getTaxDetails().put(ancillaryTaxPNRB.getTaxCode(), ancillaryTaxPNRB.getTaxAmount());
                                }
                                currencyValue.setTotalTax(totalTax);
                            }
                        } catch (Exception e) {
                            currencyValue.setTaxDetails(new HashMap<>());
                            currencyValue.setTotalTax(BigDecimal.ZERO);
                        }
                    }
                } catch (Exception e) {
                    currencyValue = new CurrencyValue();
                }

                ancillary.setCurrency(currencyValue);

                ancillaryOffer.setAncillary(ancillary);
                ancillaryOffer.setSegmentCodeAux(bookingClass.getSegmentCode());

                UpgradeAncillary upgradeAncillary = new UpgradeAncillary();
                upgradeAncillary.setType("CABIN_UPGRADE");
                upgradeAncillary.setAncillaryOffer(ancillaryOffer);
                upgradeAncillary.setSegmentCode(bookingClass.getSegmentCode());

                listUpgradeAncillary.add(upgradeAncillary);
            }
        }

        if (null != listUpgradeAncillary && !listUpgradeAncillary.isEmpty()) {
            if (SystemVariablesUtil.isFareLogixUpgradeEnabled()) {
                try {
                    flxAncillaryPriceService.setPricesFromFareLogix(
                            cartPNR,
                            bookedLeg,
                            listUpgradeAncillary,
                            pnrRQ.getRecordLocator(),
                            pnrRQ.getStore(),
                            pnrRQ.getPos()
                    );
                } catch (Exception ex) {
                    LOG.error(ex.getMessage(), ex);
                }
            }
        }


        //Adjust TAX for flight leaving from LHR
        adjustTaxForLHR(upgradeAncillaryCollection);

        LOG.info("upgradeAncillaryCollection: " + upgradeAncillaryCollection);
        return upgradeAncillaryCollection;
    }

    /**
     * *
     * Description: This function is used to add 75 GBP in the form of tax to
     * seat upgrade for flights leaving from LHR
     *
     * @param upgradeAncillaryCollection
     */
    private void adjustTaxForLHR(UpgradeAncillaryCollection upgradeAncillaryCollection) {

        try {
            for (UpgradeAncillary upradeAncillary : upgradeAncillaryCollection.getCollection()) {

                //Found a segment
                if (upradeAncillary.getSegmentCode().startsWith("LHR")
                        && upradeAncillary.getAncillaryOffer().getAncillary().getCurrency().getCurrencyCode().equalsIgnoreCase("GBP")) {
                    LOG.info("Adding 75 GBP TAX to segment: " + upradeAncillary.getSegmentCode());
                    upradeAncillary.getAncillaryOffer().getAncillary().getCurrency().addTax("TAX", new BigDecimal(75.00));
                    upradeAncillary.getAncillaryOffer().getAncillary().getCurrency().setTotal(upradeAncillary.getAncillaryOffer().getAncillary().getCurrency().getTotal().add(new BigDecimal(75.00)));
                    upradeAncillary.getAncillaryOffer().getAncillary().getCurrency().setTotalTax(upradeAncillary.getAncillaryOffer().getAncillary().getCurrency().getTotalTax().add(new BigDecimal(75.00)));
                }

            }
        } catch (Exception ex) {
            //ignore
            LOG.error(ex.getMessage());
        }
    }

    /**
     * @param segmentCode    MEX_GDL_AM_2016-10-20_2000
     * @param classOfService W
     * @return
     */
    private PricedAncillaryServicePNRB getAncillariesUpgrade(
            String segmentCode,
            String classOfService,
            PnrRQ pnrRQ,
            List<ServiceCall> serviceCallList
    ) {
        try {
            AuthorizationPaymentConfig authorizationPaymentConfig;
            authorizationPaymentConfig = getSetUpConfigFactory().getInstance().getAuthorizationPaymentConfig(
                    pnrRQ.getStore(),
                    pnrRQ.getPos()
            );

            String segmentCodeSplit[] = segmentCode.split("_");
            ItineraryUpgrade itineraryUpgrade = new ItineraryUpgrade();
            itineraryUpgrade.setClassOfService("F");
            itineraryUpgrade.setCurrencyCode(authorizationPaymentConfig.getCurrency());
            itineraryUpgrade.setOrigin(segmentCodeSplit[0]);
            itineraryUpgrade.setDestination(segmentCodeSplit[1]);
            itineraryUpgrade.setMktCarrierCode(segmentCodeSplit[2]);


            GetPriceListRS priceListRS = getAncillariesPriceService().ancillaryUpgrade(
                    itineraryUpgrade,
                    authorizationPaymentConfig.getCityCode(),
                    (getSetUpConfigFactory().getInstance().getCityCodeCommand() + authorizationPaymentConfig.getCityCode()),
                    serviceCallList
            );

            LOG.info("priceListRS ancillaryUpgrade: " + new Gson().toJson(priceListRS, GetPriceListRS.class));

            if ("V".equalsIgnoreCase(classOfService)) {
                for (GetPriceListRS.AncillaryPriceList.Itinerary itinerary : priceListRS.getAncillaryPriceList().getItinerary()) {
                    for (GetPriceListRS.AncillaryPriceList.Itinerary.AncillaryGroup ancillaryGroup : itinerary.getAncillaryGroup()) {
                        if ("IE".equalsIgnoreCase(ancillaryGroup.getGroupCode())) {
                            for (PricedAncillaryServicePNRB pricedAncillaryServicePNRB : ancillaryGroup.getAncillaryService()) {
                                if ("UCV".equalsIgnoreCase(pricedAncillaryServicePNRB.getRficSubcode())) {
                                    return pricedAncillaryServicePNRB;
                                }
                            }
                        }
                    }
                }
            } else {
                for (GetPriceListRS.AncillaryPriceList.Itinerary itinerary : priceListRS.getAncillaryPriceList().getItinerary()) {
                    for (GetPriceListRS.AncillaryPriceList.Itinerary.AncillaryGroup ancillaryGroup : itinerary.getAncillaryGroup()) {
                        if ("IE".equalsIgnoreCase(ancillaryGroup.getGroupCode())) {
                            for (PricedAncillaryServicePNRB pricedAncillaryServicePNRB : ancillaryGroup.getAncillaryService()) {
                                if ("UPG".equalsIgnoreCase(pricedAncillaryServicePNRB.getRficSubcode())) {
                                    return pricedAncillaryServicePNRB;
                                }
                            }
                        }
                    }
                }
            }
        } catch (MongoDisconnectException me) {
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(pnrRQ.getPos())) {
                LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
            } else {
                LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
                throw me;
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }

        return null;
    }

    private String getTravelersCode(List<PassengerPNRB> passengerDetails) {

        StringBuilder strBuffer = new StringBuilder();

        int adultCount = 0;
        int childCount = 0;
        int infantCount = 0;
        int paxCount = 0;
        for (PassengerPNRB passenger : passengerDetails) {
            if (passenger != null) {
                paxCount++;
                if ("ADT".equalsIgnoreCase(passenger.getReferenceNumber())) {
                    adultCount++;
                } else if ("CHD".equalsIgnoreCase(passenger.getReferenceNumber())) {
                    childCount++;
                } else if ("INF".equalsIgnoreCase(passenger.getReferenceNumber())) {
                    infantCount++;
                }
            }
        }

        if (adultCount == 0 && childCount == 0 && infantCount == 0) {
            adultCount = paxCount;
        }

        strBuffer.append("A");
        strBuffer.append(adultCount);
        strBuffer.append("_C");
        strBuffer.append(childCount);
        strBuffer.append("_I");
        strBuffer.append(infantCount);
        strBuffer.append("_PH0_PC0");

        return strBuffer.toString();
    }

    /**
     * @param getReservationRS
     * @param passDataList
     * @param passengerDetails
     * @param specialServiceInfo
     * @param bookedLeg
     * @param notInitialized
     * @param corporateResults
     * @param reservationUpdateItemTypeList
     * @param reservationUpdateSeatItemTypeList
     * @param listCustLoyalty
     * @param creationDate
     * @param pnrRQ
     * @param serviceCallList
     * @return
     * @throws ParseException
     */
    private BookedTravelerCollection getBookedTravelerCollection(
            GetReservationRS getReservationRS,
            List<PassengerDataResponseACS> passDataList,
            List<PassengerPNRB> passengerDetails,
            List<OpenReservationElementType> specialServiceInfo,
            BookedLeg bookedLeg,
            boolean notInitialized,
            String[] corporateResults,
            List<ReservationUpdateItemType> reservationUpdateItemTypeList,
            List<ReservationUpdateItemType> reservationUpdateSeatItemTypeList,
            List<FrequentFlyerPNRB> listCustLoyalty,
            String creationDate,
            PnrRQ pnrRQ,
            List<ServiceCall> serviceCallList
    ) throws ParseException {

        BookedTravelerCollection bookedTravelerCollection = new BookedTravelerCollection();
        bookedTravelerCollection.setCollection(
                getBookedTravelerList(
                        getReservationRS,
                        passDataList,
                        passengerDetails,
                        specialServiceInfo,
                        bookedLeg,
                        notInitialized,
                        corporateResults,
                        reservationUpdateItemTypeList,
                        reservationUpdateSeatItemTypeList,
                        listCustLoyalty,
                        creationDate,
                        pnrRQ,
                        serviceCallList
                )
        );

        return bookedTravelerCollection;
    }

    /**
     * @param getReservationRS
     * @param passDataList
     * @param passengerDetails
     * @param specialServiceInfo
     * @param bookedLeg
     * @param notInitialized
     * @param corporateResults
     * @param reservationUpdateItemTypeList
     * @param reservationUpdateSeatItemTypeList
     * @param custLoyaltyList
     * @param creationDate
     * @param pnrRQ
     * @param serviceCallList
     * @return
     * @throws ParseException
     */
    private List<BookedTraveler> getBookedTravelerList(
            GetReservationRS getReservationRS,
            List<PassengerDataResponseACS> passDataList,
            List<PassengerPNRB> passengerDetails,
            List<OpenReservationElementType> specialServiceInfo,
            BookedLeg bookedLeg,
            boolean notInitialized,
            String[] corporateResults,
            List<ReservationUpdateItemType> reservationUpdateItemTypeList,
            List<ReservationUpdateItemType> reservationUpdateSeatItemTypeList,
            List<FrequentFlyerPNRB> custLoyaltyList,
            String creationDate,
            PnrRQ pnrRQ,
            List<ServiceCall> serviceCallList
    ) throws ParseException {

        String pnr = pnrRQ.getRecordLocator();
        String store = pnrRQ.getStore();
        String pos = pnrRQ.getPos();

        String messageNotEligibleChk = null;

        PhoneNumbersPNRB phoneNumbers = null;
        if ( getReservationRS != null && getReservationRS.getReservation() != null ) {
        	phoneNumbers = getReservationRS.getReservation().getPhoneNumbers();
        }

        List<BookedTraveler> bookedTravelerList = new ArrayList<>();

        LOG.info("passDataList: " + new Gson().toJson(passDataList));

        if (null != passDataList) { //GETFULLINFO

            //Call passengerData (All pax - 1 segment)
            List<GetPassengerDataRSACS> listACSPassengerDataRSACS = new ArrayList<>();
            for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {

                if (AirlineCodeType.AM.getCode().equalsIgnoreCase(bookedSegment.getSegment().getOperatingCarrier())) {
                    //Start building the request...
                    GetPassengerDataRQACS acsPassengerDataRQACS = new GetPassengerDataRQACS();

                    ItineraryAndPassengerInfoACS itineraryAndPassengerInfoACS = new ItineraryAndPassengerInfoACS();

                    ItineraryACS itineraryACS = new ItineraryACS();
                    itineraryACS.setAirline(bookedSegment.getSegment().getOperatingCarrier());
                    itineraryACS.setFlight(bookedSegment.getSegment().getOperatingFlightCode());
                    itineraryACS.setDepartureDate(bookedSegment.getSegment().getDepartureDateTime().substring(0, 10));
                    itineraryACS.setOrigin(bookedSegment.getSegment().getDepartureAirport());
                    itineraryAndPassengerInfoACS.setItinerary(itineraryACS);

                    PassengerListACS passengerListACS = new PassengerListACS();
                    for (PassengerPNRB personName : passengerDetails) {
                        //For Corporate PNRs, first name in the PNR is the group name
                        if (null != personName.getFirstName()
                                && !personName.getLastName().trim().isEmpty()
                                && !"INF".equalsIgnoreCase(personName.getReferenceNumber())
                        ) {
                            PassengerACS passenger = new PassengerACS();
                            passenger.setLastName(personName.getLastName());
                            PassengerACS.PNRLocator pnrLocator = new PassengerACS.PNRLocator();
                            pnrLocator.setValue(pnr);
                            passenger.setPNRLocator(pnrLocator);
                            passengerListACS.getPassenger().add(passenger);
                        }
                    }

                    itineraryAndPassengerInfoACS.setPassengerList(passengerListACS);
                    acsPassengerDataRQACS.setItineraryAndPassengerInfo(itineraryAndPassengerInfoACS);

                    try {
                        LOG.info("acsPassengerDataRQACS: " + new Gson().toJson(acsPassengerDataRQACS, GetPassengerDataRQACS.class));
                        GetPassengerDataRSACS aCSPassengerDataRSACS;
                        aCSPassengerDataRSACS = getPassengerData().passengerDataBySegment(
                                acsPassengerDataRQACS,
                                serviceCallList
                        );
                        LOG.info("acsacsPassengerDataRSACS::: {}", new Gson().toJson(aCSPassengerDataRSACS));

                        LOG.info("acsPassengerDataRSACS: " + new Gson().toJson(aCSPassengerDataRSACS, GetPassengerDataRSACS.class));
                        listACSPassengerDataRSACS.add(aCSPassengerDataRSACS);
                    } catch (Exception e) {
                        LOG.error("PnrLookUpService.getBookedTravelerList()|" + "getting PassengerData for:" + acsPassengerDataRQACS.toString() + e.getMessage());
                    }
                }
            }

            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Calendar createDate = Calendar.getInstance();
            createDate.setTime(formatter.parse(getReservationRS.getReservation().getBookingDetails().getCreationTimestamp().toString()));
            RemarksPNRB remarkInfo = getReservationRS.getReservation().getRemarks();
            LOG.info("Remark Info {}", new Gson().toJson(remarkInfo));

            for (PassengerDataResponseACS passengerDataResponse : passDataList) {
                LOG.info("PassengerDataResponse {}", new Gson().toJson(listACSPassengerDataRSACS));
                try {

                    LOG.info("calling getBookedTravelerFullInfo ...");
                    bookedTravelerList.add(
                            getBookedTravelerFullInfo(
                                    getReservationRS,
                                    passengerDataResponse,
                                    passengerDetails,
                                    specialServiceInfo,
                                    bookedLeg,
                                    pnr,
                                    corporateResults,
                                    store,
                                    pos,
                                    listACSPassengerDataRSACS,
                                    reservationUpdateItemTypeList,
                                    reservationUpdateSeatItemTypeList,
                                    custLoyaltyList,
                                    creationDate,
                                    remarkInfo,
                                    phoneNumbers
                            )
                    );
                } catch (Exception e) {
                    LOG.error("Error getting full passenger info, " + e.getMessage(), e);
                    bookedTravelerList.add(
                            getBookedTravelerBasicInfo(
                                    passengerDataResponse,
                                    bookedLeg.getFirstOpenSegment(),
                                    messageNotEligibleChk,
                                    pnrRQ.getPos()
                            )
                    );
                }
            }


        } else {
            //GET BASIC INFO (ERROR RETRIEVING THE INFO FAIL INELIGIBLE CHECK IN )
            if (notInitialized) {
                messageNotEligibleChk = "NOT INITIALIZED";
            } else if (bookedLeg.getCheckinStatus().toString().contains("GROUND")) {
                messageNotEligibleChk = bookedLeg.getCheckinStatus().toString();
            } else if (bookedLeg.getCheckinStatus().toString().contains("CLOSED_NOT_OPERATED")) {
                messageNotEligibleChk = bookedLeg.getCheckinStatus().toString();
            }

            for (PassengerPNRB passenger : passengerDetails) {
                 bookedTravelerList.add(
                        getBookedTravelerBasicInfo(
                                passenger,
                                bookedLeg.getFirstOpenSegment(),
                                messageNotEligibleChk,
                                pnrRQ.getPos()
                        )
                );
            }
        }

        return bookedTravelerList;
    }

    /**
     * @param getReservationRS
     * @param passengerDataResponse
     * @param passengerDetails
     * @param specialServiceInfo
     * @param bookedLeg
     * @param recordLocator
     * @param corporateResults
     * @param store
     * @param pos
     * @param listACSPassengerDataRSACS
     * @param reservationUpdateItemTypeList
     * @param reservationUpdateSeatItemTypeList
     * @param listCustLoyalty
     * @param creationDate
     * @param remarkInfo
     * @param phoneNumbers
     * @return
     * @throws Exception
     */
    private BookedTraveler getBookedTravelerFullInfo(
            GetReservationRS getReservationRS,
            PassengerDataResponseACS passengerDataResponse,
            List<PassengerPNRB> passengerDetails,
            List<OpenReservationElementType> specialServiceInfo,
            BookedLeg bookedLeg,
            String recordLocator,
            String[] corporateResults,
            String store,
            String pos,
            List<GetPassengerDataRSACS> listACSPassengerDataRSACS,
            List<ReservationUpdateItemType> reservationUpdateItemTypeList,
            List<ReservationUpdateItemType> reservationUpdateSeatItemTypeList,
            List<FrequentFlyerPNRB> listCustLoyalty,
            String creationDate,
            RemarksPNRB remarkInfo,
            PhoneNumbersPNRB phoneNumbers
    ) throws Exception {

        BookedSegment bookedSegment = bookedLeg.getFirstOpenSegment();

        AuthorizationPaymentConfig authorizationPaymentConfig = getSetUpConfigFactory().getInstance().getAuthorizationPaymentConfig(store, pos);
        String currencyCode = authorizationPaymentConfig.getCurrency();

        BookedTraveler bookedTraveler = new BookedTraveler();

        PNRLookUpServiceUtil.setVcr(
                passengerDataResponse,
                bookedLeg,
                bookedTraveler
        );

        PNRLookUpServiceUtil.setAllVcrs(
                passengerDataResponse,
                bookedLeg,
                bookedTraveler
        );

        PassengerPNRB passengerPNRB = PNRLookUpServiceUtil.getPassenger(getReservationRS, bookedTraveler.getTicketNumber());

        if (null == passengerPNRB) {
            throw new Exception("Passenger not found by ticket");
        }

        String passengerNameNumber = passengerPNRB.getNameId();

        LOG.info("passengerNameNumber: " + passengerNameNumber);

        bookedTraveler.setNameRefNumber(passengerNameNumber);

        PnrUtil.setAllTicketNumbers(bookedTraveler, getReservationRS);

        PNRLookUpServiceUtil.addFrequentFlyerIfNotExistsInPaxData(bookedTraveler, listCustLoyalty);

        // Tier by Benefit
        TierLevelType tierLevelType = PNRLookUpServiceUtil.getTierLevelType(
                passengerDataResponse,
                listCustLoyalty,
                bookedTraveler
        );

        if (null != tierLevelType) {
            bookedTraveler.setTierLevel(tierLevelType.getCode());
        }

        bookedTraveler.setIsSelectedToCheckin(true);
        String departureCity = (ListsUtil.getFirst(bookedLeg.getSegments().getCollection())).getSegment().getDepartureAirport();
        String arrivalCity = (ListsUtil.getLast(bookedLeg.getSegments().getCollection())).getSegment().getArrivalAirport();
        String pnrPassId = recordLocator + "_" + departureCity + "_" + arrivalCity + "_" + passengerDataResponse.getBaggageRouteList().getBaggageRoute().get(0).getPassengerID();
        bookedTraveler.setPnrPassId(pnrPassId);

        try {
            bookedTraveler.setPassengerType(
                    passengerDataResponse.getPassengerItineraryList().getPassengerItinerary().get(0).getPassengerType()
            );
        } catch (Exception ex) {
            //
        }

        bookedTraveler.setFirstName(passengerDataResponse.getFirstName());
        bookedTraveler.setLastName(passengerDataResponse.getLastName());

        bookedTraveler.setSavedEmails(
                PNRLookUpServiceUtil.getEmailsPerPassenger(
                        passengerNameNumber,
                        passengerDetails
                )
        );

        bookedTraveler.getPhones().setCollection(
                PNRLookUpServiceUtil.getPhonesPerPassenger(
                        passengerNameNumber,
                        phoneNumbers
                )
        );

        bookedTraveler.setShowEmailCapture(
                PNRLookUpServiceUtil.showCaptureEmailRemark(
                        passengerNameNumber,
                        remarkInfo
                )
        );

        bookedTraveler.setDisplayName(
                CommonUtil.removePrefix(passengerDataResponse.getFirstName()) + " " + passengerDataResponse.getLastName()
        );

        PaxType paxType = PNRLookUpServiceUtil.getPaxType(passengerNameNumber, passengerDetails);
        bookedTraveler.setPaxType((paxType == null) ? PaxType.ADULT : paxType);

        setACSInfo(bookedTraveler, passengerDataResponse, bookedTraveler.getTierLevel());

        PaxType clasificationpaxType = PNRLookUpServiceUtil.getClasificationPaxType(passengerNameNumber, passengerDetails, bookedTraveler);
        bookedTraveler.setClassificationPaxType((clasificationpaxType == null) ? PaxType.ADULT : clasificationpaxType);

        bookedTraveler.setCorporateFrequentFlyerNumber(corporateResults[0]);
        bookedTraveler.setCorporateFrequentFlyerProgram(corporateResults[1]);

        List<AncillaryServicesPNRB> ancillariesListForThisPassenger = PassengerAncillaryUtil.getAncillaryListForPassengerAndLeg(getReservationRS, bookedLeg, passengerNameNumber);

        BookedTravelerAncillaryCollection bookedTravelerAncillaryCollection = PassengerAncillaryUtil.getAncillaries(
                ancillariesListForThisPassenger,
                bookedTraveler,
                reservationUpdateItemTypeList,
                bookedLeg.getSegments().getLegCode(),
                pos
        );

        bookedTraveler.setAncillaries(
                bookedTravelerAncillaryCollection
        );

        BookedTravelerAncillaryCollection seatBookedTravelerAncillaryCollection = PassengerAncillaryUtil.getSeatAncillaries(
                ancillariesListForThisPassenger,
                bookedTraveler,
                reservationUpdateSeatItemTypeList,
                bookedLeg.getSegments().getLegCode(),
                currencyCode
        );

        bookedTraveler.setSeatAncillaries(
                seatBookedTravelerAncillaryCollection
        );

        setSeatInfo(
                bookedTraveler,
                passengerDataResponse,
                bookedLeg
        );

        bookedTraveler.setCheckedBags(
                getCheckedBags(passengerDataResponse, bookedLeg)
        );

        String boardingPassFlag = getBoardingPassFlag(
                bookedSegment.getSegment().getDepartureAirport(),
                bookedSegment.getSegment().getDepartureDateTime().substring(0, 10),
                passengerDataResponse
        );

        if (boardingPassFlag != null && !boardingPassFlag.trim().isEmpty()) {
            bookedTraveler.setCheckinStatus(true);
            bookedTraveler.setIsSelectedToCheckin(false);
        } else {
            bookedTraveler.setCheckinStatus(false);
        }

        for (BookedSegment segment : bookedLeg.getSegments().getOpenSegmentsOperatedBy(AirlineCodeType.AM)) {

            boardingPassFlag = getBoardingPassFlag(
                    segment.getSegment().getDepartureAirport(),
                    segment.getSegment().getDepartureDateTime().substring(0, 10),
                    passengerDataResponse
            );

            if (boardingPassFlag != null && !boardingPassFlag.trim().isEmpty()) {
                PnrCollectionUtil.setCheckInStatusBySegment(bookedTraveler, segment.getSegment().getSegmentCode(), true);
            } else {
                PnrCollectionUtil.setCheckInStatusBySegment(bookedTraveler, segment.getSegment().getSegmentCode(), false);
            }
        }

        //this method set eligibility on traveler
        setEligibilityForCheckin(
                bookedTraveler,
                specialServiceInfo,
                bookedLeg,
                pos
        );

        BookingClassMap bookingClassMap = new BookingClassMap();

        PassengerDataResponseACS passengerDataResponseACS = null;

        for (BookedSegment segment : bookedLeg.getSegments().getCollection()) {

            GetPassengerDataRSACS acsPassengerDataRSACSFounded;
            BaggageRouteACS baggageRouteACSFounded = null;

            try {
                //Get passengerData of the list.
                //listACSPassengerDataRSACS
                LOG.info("listACSPassengerDataRSACS: {}", listACSPassengerDataRSACS);
                acsPassengerDataRSACSFounded = getACSPassengerDataRSACSBySegment(
                        listACSPassengerDataRSACS,
                        bookedTraveler,
                        segment
                );

                if (null != acsPassengerDataRSACSFounded && null != acsPassengerDataRSACSFounded.getPassengerDataResponseList()) {
                    passengerDataResponseACS = getPassengerDataResponseACS(acsPassengerDataRSACSFounded, bookedTraveler);
                    baggageRouteACSFounded = getBaggageRouteACSByPax(acsPassengerDataRSACSFounded, bookedTraveler, segment);
                } else {
                    acsPassengerDataRSACSFounded = null;
                }
            } catch (Exception e) {
                LOG.error(e.getMessage(), e);
                acsPassengerDataRSACSFounded = null;
            }

            if (null != acsPassengerDataRSACSFounded
                    && null != acsPassengerDataRSACSFounded.getPassengerDataResponseList()) {

                if (null == baggageRouteACSFounded) {

                    PassengerItineraryACS passengerItineraryACS;
                    passengerItineraryACS = getPassengerItineraryACS(acsPassengerDataRSACSFounded, segment);

                    if (null != passengerItineraryACS) {

                        String segmentCode = PNRLookUpServiceUtil.getSegmentCodeWithFlightNumber(passengerItineraryACS);
                        String bookingClass_ = passengerItineraryACS.getBookingClass();
                        String cabinClass = SeatMapUtil.isFirstClassCabin(bookingClass_) ? "J" : "Y";

                        segment.getSegment().setSegmentStatus(null);

                        addBaggageRouteToBaggageRouteList(
                                bookedTraveler.getBaggageRouteList(),
                                segmentCode,
                                passengerItineraryACS.getAirline(),
                                null,
                                null,
                                bookingClass_,
                                passengerItineraryACS.getDepartureDate(),
                                null,
                                passengerItineraryACS.getDestination(),
                                passengerItineraryACS.getFlight(),
                                passengerItineraryACS.getOrigin(),
                                null,
                                passengerItineraryACS.getSegmentID(),
                                null
                        );

                        BookingClass bookingClass = new BookingClass();
                        bookingClass.setBookingCabin(cabinClass);
                        bookingClass.setBookingClass(bookingClass_);
                        bookingClass.setSegmentCode(segmentCode);
                        bookingClass.setPassengerId(null);
                        bookingClass.setFlight(passengerItineraryACS.getFlight());

                        bookingClassMap.getCollection().add(bookingClass);
                    }

                } else {

                    String segmentCode = PNRLookUpServiceUtil.getSegmentCode(baggageRouteACSFounded);
                    String bookingClass_ = null;

                    if (baggageRouteACSFounded.getBookingClass() != null) {
                        bookingClass_ = baggageRouteACSFounded.getBookingClass();
                    }

                    if (null == bookingClass_ || "".equalsIgnoreCase(bookingClass_.trim())) {
                        bookingClass_ = getBookingClassFromPassengerDataBySegment(passengerDataResponse);
                    }
                    String cabinClass = SeatMapUtil.isFirstClassCabin(bookingClass_) ? "J" : "Y";

                    segment.getSegment().setSegmentStatus(baggageRouteACSFounded.getSegmentStatus());

                    addBaggageRouteToBaggageRouteList(
                            bookedTraveler.getBaggageRouteList(),
                            segmentCode,
                            baggageRouteACSFounded.getAirline(),
                            baggageRouteACSFounded.getArrivalDate(),
                            baggageRouteACSFounded.getArrivalTime(),
                            bookingClass_,
                            baggageRouteACSFounded.getDepartureDate(),
                            baggageRouteACSFounded.getDepartureTime(),
                            baggageRouteACSFounded.getDestination(),
                            baggageRouteACSFounded.getFlight(),
                            baggageRouteACSFounded.getOrigin(),
                            baggageRouteACSFounded.getPassengerID(),
                            baggageRouteACSFounded.getSegmentID(),
                            baggageRouteACSFounded.getSegmentStatus()
                    );

                    BookingClass bookingClass = new BookingClass();
                    bookingClass.setBookingCabin(cabinClass);
                    bookingClass.setBookingClass(bookingClass_);
                    bookingClass.setSegmentCode(segmentCode);
                    bookingClass.setPassengerId(baggageRouteACSFounded.getPassengerID());
                    bookingClass.setFlight(baggageRouteACSFounded.getFlight());

                    bookingClassMap.getCollection().add(bookingClass);
                }
            }
        }

        //Setting properly paxID
        if (null != bookingClassMap.getCollection() && !bookingClassMap.getCollection().isEmpty()) {
            try {
                bookedTraveler.setId(bookingClassMap.getBookingClass(bookedSegment.getSegment().getSegmentCode()).getPassengerId());
            } catch (Exception ex) {
                bookedTraveler.setId(bookingClassMap.getCollection().get(0).getPassengerId());
            }

            if (null == bookedTraveler.getId() || bookedTraveler.getId().trim().isEmpty()) {
                if (passengerDataResponseACS != null) {
                    bookedTraveler.setId(passengerDataResponseACS.getPassengerID());
                } else {
                    bookedTraveler.getIneligibleReasons().add("NO_PAX_ID");
                    bookedTraveler.setId("");
                }
            }
        } else {
            if (passengerDataResponseACS != null) {
                bookedTraveler.setId(passengerDataResponseACS.getPassengerID());
            } else {
                bookedTraveler.getIneligibleReasons().add("NO_PAX_ID");
                bookedTraveler.setId("");
            }
        }

        if ((null == bookedTraveler.getId() || bookedTraveler.getId().isEmpty())
                && !bookedTraveler.getIneligibleReasons().contains("NO_PAX_ID")) {
            bookedTraveler.getIneligibleReasons().add("NO_PAX_ID");
            bookedTraveler.setId("");
        }

        bookedTraveler.setBookingClasses(bookingClassMap);

        XMLGregorianCalendar xmlPnrTicketCreationDate =  getReservationRS.getReservation().getPassengerReservation().getTicketingInfo().getTicketDetails().get(
            getReservationRS.getReservation().getPassengerReservation().getTicketingInfo().getTicketDetails().size() - 1
        ).getTimestamp();

        String pnrCreationTicketDate = convertXmlGregorianToString(xmlPnrTicketCreationDate);

        setBaggageAllowance(bookedTraveler, bookedLeg, bookedSegment, recordLocator, creationDate, pnrCreationTicketDate);

        PNRLookUpServiceUtil.addExtraBaggageForInfant(bookedLeg, bookedTraveler);

        setSSRInfo(bookedTraveler, bookedSegment, passengerDataResponse);

        if (!bookedTraveler.isCheckinStatus()) {
            setMissingRequiredInfo(bookedTraveler, passengerDataResponse, bookedLeg);
        }

        // Require passport to be entered or re-entered (adult and infant) for all international flights even if it was already entered.
        PNRLookUpServiceUtil.setRequiredTravelDocs(bookedTraveler, bookedLeg);

        setPassengersIneligibleCheckinRestricted(bookedTraveler, bookedLeg, pos);

        if (SystemVariablesUtil.isTimaticEnabled() && !bookedTraveler.isCheckinStatus()) {
            try {
                CommonServiceUtil.setTimaticInfo(bookedTraveler, passengerDataResponse, ADULT);
                CommonServiceUtil.parseTimaticFreeText(bookedTraveler, passengerDataResponse, ADULT);

                if (bookedTraveler.getInfant() != null) {
                    CommonServiceUtil.setTimaticInfo(bookedTraveler, passengerDataResponse, INFANT);
                    CommonServiceUtil.parseTimaticFreeText(bookedTraveler, passengerDataResponse, INFANT);
                }

                PnrCollectionUtil.removeTimaticIneligible(bookedTraveler);
            } catch (Exception ex) {
                LOG.error(ex.getMessage(), ex);
            }
        }

        if (PnrCollectionUtil.findMissingInformation(bookedTraveler, CheckinIneligibleReasons.MDA.toString())) {
            //We validate for missing reason DESTINATION_ADDRESS and if the traveler is going to their country of residence we remove the missing reason.
            AirportWeather arrivalAirport = getAirportsCodesDao().getAirportWeatherByIata(bookedSegment.getSegment().getArrivalAirport());
            if (arrivalAirport.getAirport().getCountry().equalsIgnoreCase(bookedTraveler.getCountryOfResidence())) {
                PnrCollectionUtil.removeMissingInformation(bookedTraveler, CheckinIneligibleReasons.MDA.toString());
            }
        }

        //Update freeBags
        if (null != bookedTraveler.getFreeBaggageAllowancePerLegsArray()) {

            for (FreeBaggageAllowancePerLeg freeBaggageAllowancePerLeg : bookedTraveler.getFreeBaggageAllowancePerLegsArray()) {
                if (bookedLeg.getSegments().getLegCode().equalsIgnoreCase(freeBaggageAllowancePerLeg.getLegCode())) {
                    int totalFreeBagAllow = freeBaggageAllowancePerLeg.getFreeBaggageAllowance().getChecked();

                    if (totalFreeBagAllow - bookedTraveler.getCheckedBags() < 0) {
                        updateUsedPrePaidBags(
                                Math.abs(totalFreeBagAllow - bookedTraveler.getCheckedBags()),
                                bookedTraveler
                        );
                    }

                    break;
                }
            }
        }

        //Validate all country codes Information.
        getReservationUtilService().changeCountryCodesToIso2(bookedTraveler);

        String flagpassengerOnStandByList = System.getProperty("flagShowPassengerOnStandByList");
        boolean flagActiveStandByList = Boolean.parseBoolean(flagpassengerOnStandByList);

        String priorityCode = getPriorityOVSCode(passengerDataResponse);
        if (priorityCode != null) {
            //over-booking info
            OverBookingInfo overBookingInfo = new OverBookingInfo();
            overBookingInfo.setAccepted(true);
            bookedTraveler.setOverBookingInfo(overBookingInfo);
            if(flagActiveStandByList){
                bookedLeg.setOvsReservation(true);
            } else {
                bookedLeg.setOvsReservation(false);
            }

        }

        //Set Over-booking flag
        bookedTraveler.setIsOverBookingEligible(PNRLookUpServiceUtil.isOverbookingEligible(bookedTraveler));

        //Set Sky priority flag
        bookedTraveler.setIsSkyPriority(PNRLookUpServiceUtil.isSkyPriority(bookedTraveler));

        //set benefits
        if (null != bookingClassMap.getCollection()
                && !bookingClassMap.getCollection().isEmpty()
                && !"V".equalsIgnoreCase(bookingClassMap.getCollection().get(0).getBookingClass())) {
            if (null != bookedTraveler.getFrequentFlyerProgram()
                    && "AM".equalsIgnoreCase(bookedTraveler.getFrequentFlyerProgram())) {
                if (SystemVariablesUtil.isAmTierBenefitsTurnOff()) {
                    bookedTraveler.setBookedTravellerBenefitCollection(null);
                } else {
                    try {
                        callBenefitAndRedeemedInformation(
                                bookedTraveler,
                                bookingClassMap,
                                bookedLeg,
                                tierLevelType
                        );
                    } catch (Exception e) {
                        LOG.error(e.getMessage(), e);
                        bookedTraveler.setBookedTravellerBenefitCollection(null);
                        LOG.info("Error to show BookedTravellerBenefitCollection AM");
                    }
                    try {
                        BenefitRedeemed benefitRedeemed = getBenefitRedeemed(
                                bookedTraveler,
                                remarkInfo,
                                bookedLeg.getSegments().getLegCode()
                        );

                        if (null != benefitRedeemed) {
                            bookedTraveler.getBenefitRedeemedCollection().getCollection().add(benefitRedeemed);
                        }
                    } catch (Exception e) {
                        LOG.error(e.getMessage(), e);
                        LOG.info("Error to read BenefitRedeemedCollection AM");
                    }
                }

            } else if (null != bookedTraveler.getFrequentFlyerProgram()
                    && "DL".equalsIgnoreCase(bookedTraveler.getFrequentFlyerProgram())) {
                if (SystemVariablesUtil.isDlTierBenefitsTurnOff()) {
                    bookedTraveler.setBookedTravellerBenefitCollection(null);
                } else {
                    try {
                        callBenefitAndRedeemedInformation(
                                bookedTraveler,
                                bookingClassMap,
                                bookedLeg,
                                tierLevelType
                        );
                    } catch (Exception e) {
                        LOG.error(e.getMessage(), e);
                        bookedTraveler.setBookedTravellerBenefitCollection(null);
                        LOG.info("Error to show BookedTravellerBenefitCollectio DL");
                    }
                }
            }
        }

        return bookedTraveler;
    }

    private void setPassengersIneligibleCheckinRestricted(BookedTraveler bookedTraveler, BookedLeg bookedLeg, String pos) {

    	Boolean isRestrictionDepartureArrival = false;
    	String departureArrivalRestriction = System.getProperty("flagDepartureArrivalRestriction");
        Boolean isRestrictedCheckin = false;
        String restrictedCheckinReason = "";
        for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {
            Segment segmnt = bookedSegment.getSegment();
            String restrictedCheckn = getItineraryService().validateCheckinRestrictedSanitaryReasons(segmnt.getDepartureAirport(), segmnt.getArrivalAirport());
            if (!restrictedCheckn.isEmpty()) {
                isRestrictedCheckin = true;
                restrictedCheckinReason = restrictedCheckn;
            }
            if (!isRestrictionDepartureArrival) {
            	isRestrictionDepartureArrival = CommonUtilsCheckIn.isRestrictionByDepartureArrival(departureArrivalRestriction, segmnt);
            }
        }
        if (isRestrictionDepartureArrival) {
        	if (!PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(pos)) {
                bookedLeg.setCheckinStatus(BookedLegCheckinStatusType.CHECKIN_NOT_ALLOWED_SANITARY_RESTRICTIONS);
            }
            bookedTraveler.getIneligibleReasons().add(BookedTravelerCheckinIneligibleReasonsType.CHECKIN_NOT_ALLOWED_SANITARY_RESTRICTIONS.toString());
        }
        try {
            if (isRestrictedCheckin) {
                bookedTraveler.setIsEligibleToCheckin(false);
                if (restrictedCheckinReason.contains("NOT_ALLOWED")) {
                    if (restrictedCheckinReason.contains("SANITARY_RESTRICTIONS")) {
                        if (!PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(pos)) {
                            bookedLeg.setCheckinStatus(BookedLegCheckinStatusType.CHECKIN_NOT_ALLOWED_SANITARY_RESTRICTIONS);
                        }
                        bookedTraveler.getIneligibleReasons().add(
                                BookedTravelerCheckinIneligibleReasonsType.CHECKIN_NOT_ALLOWED_SANITARY_RESTRICTIONS.toString());
                    } else {
                        if (!PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(pos)) {
                            bookedLeg.setCheckinStatus(BookedLegCheckinStatusType.CHECKIN_NOT_ALLOWED_RESTRICTED_CHECKIN);
                        }
                        bookedTraveler.getIneligibleReasons().add(
                                BookedTravelerCheckinIneligibleReasonsType.CHECKIN_NOT_ALLOWED_RESTRICTED_CHECKIN.toString());
                    }
                }
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            return;
        }

    }

    private TierBenefitCobrandList getBenefitsInformationByCard(TierBenefitsRS tierBenefitsRS) throws Exception {
        TierBenefitCobrandList tierBenefitCobrandList = new TierBenefitCobrandList();
        if (null != tierBenefitsRS && !tierBenefitsRS.getMember().isEmpty()) {
            List<String> lstCobrandedCards = Arrays.asList(
                    tierBenefitsRS.getMember().get(0).getCobrandCard().split("\\|")
            );

            int numCard = lstCobrandedCards.size();
            if (numCard > 1) {
                List<TierBenefitCobrand> tierBenefitClubPremierList = new ArrayList<>();
                tierBenefitClubPremierList = getTierBenefitDao().getTierBenefitCobrandByCodeList(lstCobrandedCards);
                if (null != tierBenefitClubPremierList) {
                    tierBenefitCobrandList.addAll(tierBenefitClubPremierList);
                }

            } else if (numCard == 1) {
                TierBenefitCobrand tierBenefitClubPremier = getTierBenefitDao().getTierBenefitCobrandByCode(lstCobrandedCards.get(0));
                if (null != tierBenefitClubPremier) {
                    tierBenefitCobrandList.add(tierBenefitClubPremier);
                }

            }
        }
        return tierBenefitCobrandList;
    }

    private TierBenefitsRS getTierBenefitRSClubPremier(BookedTraveler bookedTraveler) {
        TierBenefitsRS tierBenefitsRS = null;
        try {
            if (null != bookedTraveler.getFrequentFlyerNumber() && null != bookedTraveler.getDateOfBirth()) {
                String dateOfBirthFormat = PNRLookUpServiceUtil.getDateOfBirthFormatToCP(bookedTraveler.getDateOfBirth());
                tierBenefitsRS = getTierBenefitService().getTierBenefits(bookedTraveler.getFrequentFlyerNumber(), dateOfBirthFormat);
                if (null != tierBenefitsRS && null != tierBenefitsRS.getMember() && !tierBenefitsRS.getMember().isEmpty()) {
                    if (tierBenefitsRS.getMember().get(0).getIsSuccess()) {
                        LOG.info("tierBenefitsRS: {}", new Gson().toJson(tierBenefitsRS));
                        return tierBenefitsRS;
                    } else {
                        LOG.info("tierBenefitsRS Error: {}", tierBenefitsRS.getMember().get(0).getErrorDescription());
                    }
                } else {
                    LOG.info("tierBenefitsRS NULL.");
                }
            } else {
                LOG.info("tierBenefitsRS Not Frequent Flyer Number or Date Of Birth.");
            }
            return tierBenefitsRS;
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        return tierBenefitsRS;
    }

    private PassengerItineraryACS getPassengerItineraryACS(
            GetPassengerDataRSACS acsPassengerDataRSACSFounded,
            BookedSegment bookedSegment
    ) {
        if (null == bookedSegment || null == bookedSegment.getSegment()) {
            return null;
        }

        if (null != acsPassengerDataRSACSFounded && null != acsPassengerDataRSACSFounded.getPassengerDataResponseList() && null != acsPassengerDataRSACSFounded.getPassengerDataResponseList().getPassengerDataResponse()) {
            for (PassengerDataResponseACS passengerDataResponseACS : acsPassengerDataRSACSFounded.getPassengerDataResponseList().getPassengerDataResponse()) {
                if (null != passengerDataResponseACS && null != passengerDataResponseACS.getPassengerItineraryList() && null != passengerDataResponseACS.getPassengerItineraryList().getPassengerItinerary()) {
                    for (PassengerItineraryACS passengerItineraryACS : passengerDataResponseACS.getPassengerItineraryList().getPassengerItinerary()) {

                        try {
                            if (bookedSegment.getSegment().getDepartureAirport().equalsIgnoreCase(passengerItineraryACS.getOrigin())
                                    && bookedSegment.getSegment().getArrivalAirport().equalsIgnoreCase(passengerItineraryACS.getDestination())
                                    && FlightNumberUtil.compareFlightNumbers(bookedSegment.getSegment().getOperatingFlightCode(), passengerItineraryACS.getFlight())
                                    && bookedSegment.getSegment().getDepartureDateTime().substring(0, 10).equalsIgnoreCase(passengerItineraryACS.getDepartureDate())) {

                                return passengerItineraryACS;
                            }
                        } catch (Exception ex) {
                            //
                            LOG.error(ex.getMessage(), ex);
                        }
                    }
                }
            }
        }

        return null;
    }

    private void setBaggageAllowance( BookedTraveler bookedTraveler, BookedLeg bookedLeg, BookedSegment bookedSegment, String recordLocator, String creationDate, String creationTicket ) {

    	Log.info("+setBaggageAllowance()");

    	List<BookedSegment> amSegments = bookedLeg.getSegments().getSegmentsOperatedBy( Arrays.asList(AirlineCodeType.AM, AirlineCodeType.DL) );

        String origin = amSegments.get(0).getSegment().getDepartureAirport();
        String destination = ListsUtil.getLast(amSegments).getSegment().getArrivalAirport();

        //set bags
        if (BrandedFareRulesUtil.isDateBeforeBrandedFares_1_0(creationDate)
                || (!BrandedFareRulesUtil.isBrandedFare_1_0(bookedLeg, creationDate))
                && !BrandedFareRulesUtil.isBrandedFare_3_0(bookedLeg, creationDate)) {

            if (null != bookedTraveler.getBookingClasses() && !bookedTraveler.getBookingClasses().getCollection().isEmpty()) {

                try {
                    FreeBaggageAllowance freeBaggageAllowance = getBaggageAllowanceService().getFreeBagageAllowed(
                            origin,
                            destination,
                            false,
                            bookedTraveler.getBookingClasses().getCollection().get(0).getBookingCabin(),
                            recordLocator,
                            creationDate,
                            bookedTraveler.getTicketNumber()
                    );

                    bookedTraveler.setFreeBaggageAllowance(freeBaggageAllowance);

                } catch (Exception ex) {
                    LOG.error("-1 Error.Free Bag Allowance |" + bookedSegment.getSegment().getDepartureAirport() + "-" + bookedSegment.getSegment().getArrivalAirport() + "(" + ex.getMessage() + ")", ex);
                    bookedTraveler.setFreeBaggageAllowance(BaggageAllowanceService.getDefaultBaggageAllowance());
                }

                try {
                    List<FreeBaggageAllowancePerLeg> listFreeBaggageAllowancePerLeg = new ArrayList<>();
                    FreeBaggageAllowancePerLeg freeBaggageAllowancePerLeg;
                    FreeBaggageAllowancePerLeg freeBaggageAllowancePerLegDos;
                    String segmentNumber = amSegments.get(0).getSegment().getSegmentNumber();

                    freeBaggageAllowancePerLeg = getBaggageAllowanceService().getFreeBagageAllowedPerLegNew(
                            origin,
                            destination,
                            false,
                            bookedTraveler.getBookingClasses().getCollection().get(0).getBookingCabin(),
                            recordLocator,
                            bookedTraveler.getTicketNumber(),
                            creationTicket
                    );

                    freeBaggageAllowancePerLeg.setLegCode(bookedLeg.getSegments().getLegCode());

                    freeBaggageAllowancePerLegDos = getBaggageAllowanceService().getFreeBagageAllowedPerLegArrivalNew(
                        origin,
                        destination,
                        false,
                        bookedTraveler.getBookingClasses().getCollection().get(0).getBookingCabin(),
                        recordLocator,
                        bookedTraveler.getTicketNumber(),
                        creationTicket
                    );
                    freeBaggageAllowancePerLegDos.setLegCode(bookedLeg.getSegments().getLegCode());

                    listFreeBaggageAllowancePerLeg.add(freeBaggageAllowancePerLeg);
                    if(2 == Integer.valueOf(segmentNumber)){
                        listFreeBaggageAllowancePerLeg.add(freeBaggageAllowancePerLegDos);
                    }

                    bookedTraveler.setFreeBaggageAllowancePerLegsArray(listFreeBaggageAllowancePerLeg);
                } catch (Exception ex) {
                    LOG.error("-2 Error.Free Bag Allowance |" + bookedSegment.getSegment().getDepartureAirport() + "-" + bookedSegment.getSegment().getArrivalAirport() + "(" + ex.getMessage() + ")", ex);
                    bookedTraveler.setFreeBaggageAllowancePerLegsArray( BaggageAllowanceService.getDefaultBaggageAllowancePerLegArray(bookedLeg.getSegments().getLegCode()) );
                }

            } else {
                LOG.info("-3 Error.Free Bag Allowance |" + bookedSegment.getSegment().getDepartureAirport() + "-" + bookedSegment.getSegment().getArrivalAirport() + "(Booking classes is empty or null.)");
                bookedTraveler.setFreeBaggageAllowance( BaggageAllowanceService.getNoneBaggageAllowance() );
                bookedTraveler.setFreeBaggageAllowancePerLegsArray( BaggageAllowanceService.getNoneBaggageAllowancePerLegArray(bookedLeg.getSegments().getLegCode()) );
            }

        } else {

            try {
                FreeBaggageAllowance freeBaggageAllowance = getBaggageAllowanceService().getFreeBagageAllowed(
                        origin,
                        destination,
                        bookedLeg.getMarketFlight().toString(),
                        amSegments.get(0).getSegment().getFareFamily(),
                        recordLocator,
                        bookedTraveler.getTicketNumber(),
                        creationTicket
                );

                bookedTraveler.setFreeBaggageAllowance(freeBaggageAllowance);
            } catch (Exception ex) {
                LOG.error("-4 Error.Free Bag Allowance |" + bookedSegment.getSegment().getDepartureAirport() + "-" + bookedSegment.getSegment().getArrivalAirport() + "(" + ex.getMessage() + ")", ex);
                bookedTraveler.setFreeBaggageAllowance( BaggageAllowanceService.getDefaultBaggageAllowance() );
            }

            try {
                List<FreeBaggageAllowancePerLeg> listFreeBaggageAllowancePerLeg = new ArrayList<>();
                FreeBaggageAllowancePerLeg freeBaggageAllowancePerLeg = null;
                FreeBaggageAllowancePerLeg freeBaggageAllowancePerLegDos = null;
                FreeBaggageAllowancePerLeg freeBaggageAllowancePerLegTres = null;
                String segmentNumber = amSegments.get(0).getSegment().getSegmentNumber();

                freeBaggageAllowancePerLeg = getBaggageAllowanceService().getFreeBagageAllowedPerLegOrigin(
                        origin,
                        destination,
                        bookedLeg.getMarketFlight().toString(),
                        amSegments.get(0).getSegment().getFareFamily(),
                        recordLocator,
                        bookedTraveler.getTicketNumber(),
                        creationTicket
                );

                freeBaggageAllowancePerLeg.setLegCode(bookedLeg.getSegments().getLegCode());

                if(2 == Integer.valueOf(segmentNumber)){
                    
                    freeBaggageAllowancePerLegDos = getBaggageAllowanceService().getFreeBagageAllowedPerLegArrival(
                        origin,
                        destination,
                        bookedLeg.getMarketFlight().toString(),
                        amSegments.get(0).getSegment().getFareFamily(),
                        recordLocator,
                        bookedTraveler.getTicketNumber(),
                        creationTicket
                    );
                    
                    freeBaggageAllowancePerLegDos.setLegCode(bookedLeg.getSegments().getLegCode());
                }

                if(3 == Integer.valueOf(segmentNumber)){

                    freeBaggageAllowancePerLegTres = getBaggageAllowanceService().getFreeBagageAllowedPerLeg(
                        origin,
                        destination,
                        bookedLeg.getMarketFlight().toString(),
                        amSegments.get(0).getSegment().getFareFamily(),
                        recordLocator,
                        bookedTraveler.getTicketNumber(),
                        creationTicket
                     );

                     freeBaggageAllowancePerLegTres.setLegCode(bookedLeg.getSegments().getLegCode());
                }

                if( 1 == Integer.valueOf(segmentNumber) ) {
                    listFreeBaggageAllowancePerLeg.add(freeBaggageAllowancePerLeg);
                }

                if ( 2 == Integer.valueOf(segmentNumber) ) {
                    listFreeBaggageAllowancePerLeg.add(freeBaggageAllowancePerLegDos);
                }

                if ( 3 == Integer.valueOf(segmentNumber) ) {
                    listFreeBaggageAllowancePerLeg.add(freeBaggageAllowancePerLegTres);
                }

                bookedTraveler.setFreeBaggageAllowancePerLegsArray( listFreeBaggageAllowancePerLeg );

            } catch (Exception ex) {
                LOG.error("-5 Error.Free Bag Allowance |" + bookedSegment.getSegment().getDepartureAirport() + "-" + bookedSegment.getSegment().getArrivalAirport() + "(" + ex.getMessage() + ")", ex);
                bookedTraveler.setFreeBaggageAllowancePerLegsArray( BaggageAllowanceService.getDefaultBaggageAllowancePerLegArray(bookedLeg.getSegments().getLegCode()) );
            }

        }
    }

    /*
     * This method will find out if the passenger has accepted an over-booking offer.
     * When a passenger accepts an over-booking offer, the passenger is added to the priority  list with an OVS priority code.
     */
    private String getPriorityOVSCode(PassengerDataResponseACS passengerDataResponse) {

        String priorityCode = null;

        //Get the priority code, if there is one
        for (PassengerItineraryACS itinerary : passengerDataResponse.getPassengerItineraryList().getPassengerItinerary()) {
            priorityCode = itinerary.getPriority();
            if ("OVS".equalsIgnoreCase(priorityCode)) {
                return priorityCode;
            }
        }

        return priorityCode;
    }

    /**
     * @param checkedBags
     * @param bookedTraveler
     */
    private void updateUsedPrePaidBags(int checkedBags, BookedTraveler bookedTraveler) {
        for (AbstractAncillary abstractAncillary : bookedTraveler.getAncillaries().getCollection()) {
            if (abstractAncillary instanceof PaidTravelerAncillary) {
                PaidTravelerAncillary paidTravelerAncillary = (PaidTravelerAncillary) abstractAncillary;
                if ("BG".equalsIgnoreCase(paidTravelerAncillary.getGroupCode())) {
                	if ( ! paidTravelerAncillary.getCommercialName().contains( "CARRY ON" ) ) {
	                    if (checkedBags > 0) {
	                        ((PaidTravelerAncillary) abstractAncillary).setUsed(true);
	                        --checkedBags;
	                    }
                	}
                }
            }
        }
    }

    /**
     * @param passengerDataResponse
     * @return
     */
    private String getBookingClassFromPassengerDataBySegment(PassengerDataResponseACS passengerDataResponse) {
        return passengerDataResponse.getBaggageRouteList().getBaggageRoute().get(0).getBookingClass();
    }

    /**
     * @param listACSPassengerDataRSACS
     * @param bookedTraveler
     * @param bookedSegment
     * @return
     */
    private GetPassengerDataRSACS getACSPassengerDataRSACSBySegment(
            List<GetPassengerDataRSACS> listACSPassengerDataRSACS,
            BookedTraveler bookedTraveler,
            BookedSegment bookedSegment
    ) {
        LOG.info("listACSPassengerDataRSACS {}",listACSPassengerDataRSACS);
        for (GetPassengerDataRSACS acsPassengerDataRSACS : listACSPassengerDataRSACS) {
            LOG.info("acsPassengerDataRSACS {} ", listACSPassengerDataRSACS);
            if (null != acsPassengerDataRSACS.getPassengerDataResponseList()
                    && ErrorOrSuccessCode.SUCCESS.toString().equalsIgnoreCase(acsPassengerDataRSACS.getResult().getStatus().toString())) {

                BaggageRouteACS baggageRouteACSTemp = acsPassengerDataRSACS.getPassengerDataResponseList().getPassengerDataResponse().get(0).getBaggageRouteList().getBaggageRoute().get(0);
                LOG.info("BaggageRouteACS: {}", baggageRouteACSTemp.getOrigin());
                LOG.info("BookedSegment: {}", bookedSegment.getSegment().getDepartureAirport());

                BaggageRouteACS baggageRouteACS = getBaggageRouteACSByPax(acsPassengerDataRSACS, bookedTraveler, bookedSegment);

                if (null != baggageRouteACS) {
                    boolean sameFlights = (FlightNumberUtil.compareFlightNumbers(baggageRouteACS.getFlight(), bookedSegment.getSegment().getOperatingFlightCode())
                            || FlightNumberUtil.compareFlightNumbers(baggageRouteACS.getFlight(), bookedSegment.getSegment().getMarketingFlightCode()));

                    if (baggageRouteACS.getOrigin().equalsIgnoreCase(bookedSegment.getSegment().getDepartureAirport())
                            && baggageRouteACS.getDestination().equalsIgnoreCase(bookedSegment.getSegment().getArrivalAirport())
                            && baggageRouteACS.getDepartureDate().equalsIgnoreCase(bookedSegment.getSegment().getDepartureDateTime().substring(0, 10))
                            && sameFlights) {

                        return acsPassengerDataRSACS;
                    }
                }
            }
        }

        return null;
    }

    private boolean containTicket(List<String> tkts, String ticket) {

        if (null == ticket || ticket.isEmpty() || null == tkts || tkts.isEmpty()) {
            return false;
        }

        if (tkts.contains(ticket)) {
            return true;
        }

        return false;
    }

    private boolean containTicket(Set<String> tickets, List<String> tkts) {

        if (null == tickets || tickets.isEmpty() || null == tkts || tkts.isEmpty()) {
            return false;
        }

        for (String tkt : tickets) {
            if (tkts.contains(tkt)) {
                return true;
            }
        }

        return false;
    }

    private List<String> getTickets(PassengerDataResponseACS passengerDataResponseACS) {
        List<String> tikectList = new ArrayList<>();

        if (null != passengerDataResponseACS.getVCRNumber()) {
            tikectList.add(passengerDataResponseACS.getVCRNumber().getValue());
        } else {
            if (null != passengerDataResponseACS.getPassengerItineraryList()) {
                for (PassengerItineraryACS passengerItineraryACS : passengerDataResponseACS.getPassengerItineraryList().getPassengerItinerary()) {
                    if (null != passengerItineraryACS.getVCRInfoList()) {
                        for (VCRInfoACS vCRInfoACS : passengerItineraryACS.getVCRInfoList().getVCRInfo()) {
                            if (null != vCRInfoACS.getVCRNumber()) {
                                tikectList.add(vCRInfoACS.getVCRNumber().getValue());
                            }
                        }
                    }
                }
            }
        }

        return tikectList;
    }

    private BaggageRouteACS getBaggageRouteACSByPax(
            GetPassengerDataRSACS acsPassengerDataRSACS,
            BookedTraveler bookedTraveler,
            BookedSegment bookedSegment
    ) {
        for (PassengerDataResponseACS passengerDataResponseACS : acsPassengerDataRSACS.getPassengerDataResponseList().getPassengerDataResponse()) {

            List<String> ticketList = getTickets(passengerDataResponseACS);

            boolean sameLastName = PNRLookUpServiceUtil.compareNames(bookedTraveler.getLastName(), passengerDataResponseACS.getLastName());
            boolean sameFirstName = PNRLookUpServiceUtil.compareNames(bookedTraveler.getFirstName(), passengerDataResponseACS.getFirstName());

            LOG.info("SameFirstName: {}", sameFirstName);
            LOG.info("SameLastName: {}", sameLastName);

            boolean sameTicket = containTicket(ticketList, bookedTraveler.getTicketNumber());
            sameTicket = (sameTicket || containTicket(bookedTraveler.getAllTicketNumbers(), ticketList));

            LOG.info("SameTicket: {}, {}, {}, {}", sameTicket, bookedTraveler.getTicketNumber(), bookedTraveler.getAllTicketNumbers(), ticketList);

            if (sameLastName
                    && sameFirstName
                    && sameTicket
            ) {
                LOG.info("Passenger found in data");

                for (BaggageRouteACS baggageRouteACS : passengerDataResponseACS.getBaggageRouteList().getBaggageRoute()) {

                    boolean sameFlights = (FlightNumberUtil.compareFlightNumbers(baggageRouteACS.getFlight(), bookedSegment.getSegment().getOperatingFlightCode())
                            || FlightNumberUtil.compareFlightNumbers(baggageRouteACS.getFlight(), bookedSegment.getSegment().getMarketingFlightCode()));

                    if (baggageRouteACS.getOrigin().equalsIgnoreCase(bookedSegment.getSegment().getDepartureAirport())
                            && baggageRouteACS.getDestination().equalsIgnoreCase(bookedSegment.getSegment().getArrivalAirport())
                            && baggageRouteACS.getDepartureDate().equalsIgnoreCase(bookedSegment.getSegment().getDepartureDateTime().substring(0, 10))
                            && sameFlights) {

                        return baggageRouteACS;
                    }
                }
            } else {
                LOG.info("Passenger not found in data");
            }
        }

        return null;
    }

    private PassengerDataResponseACS getPassengerDataResponseACS(
            GetPassengerDataRSACS acsPassengerDataRSACS,
            BookedTraveler bookedTraveler
    ) {

        for (PassengerDataResponseACS passengerDataResponseACS : acsPassengerDataRSACS.getPassengerDataResponseList().getPassengerDataResponse()) {

            List<String> ticketList = getTickets(passengerDataResponseACS);

            boolean sameLastName = PNRLookUpServiceUtil.compareNames(bookedTraveler.getLastName(), passengerDataResponseACS.getLastName());
            boolean sameFirstName = PNRLookUpServiceUtil.compareNames(bookedTraveler.getFirstName(), passengerDataResponseACS.getFirstName());

            LOG.info("SameFirstName: {}", sameFirstName);
            LOG.info("SameLastName: {}", sameLastName);

            boolean sameTicket = containTicket(ticketList, bookedTraveler.getTicketNumber());
            sameTicket = (sameTicket || containTicket(bookedTraveler.getAllTicketNumbers(), ticketList));

            LOG.info("SameTicket: {}, {}", sameTicket, bookedTraveler.getTicketNumber());

            if (sameLastName
                    && sameFirstName
                    && sameTicket) {
                return passengerDataResponseACS;
            }
        }
        return null;
    }

    private Integer getCheckedBags(PassengerDataResponseACS passengerDataResponse, BookedLeg bookedLeg) {

        String bagCount = "0";
        Segment firstSegment = bookedLeg.getSegments().getCollection().get(0).getSegment();
        try {

            for (PassengerItineraryACS passengerItineraryACS : passengerDataResponse.getPassengerItineraryList().getPassengerItinerary()) {

                if (passengerItineraryACS.getOrigin().equalsIgnoreCase(firstSegment.getDepartureAirport())
                        && firstSegment.getDepartureDateTime().startsWith(passengerItineraryACS.getDepartureDate())) {

                    if ("NB".equalsIgnoreCase(passengerItineraryACS.getBagCount())) {
                        bagCount = "0";
                    } else {
                        bagCount = passengerItineraryACS.getBagCount();
                    }

                    break;
                }
            }

            return Integer.parseInt(bagCount);

        } catch (Exception ex) {
            //ignore exception
            LOG.error(ex.getMessage(), ex);
        }

        return 0;
    }

    /**
     * @param origin
     * @param departureDate
     * @param passengerDataResponse
     * @return
     */
    private String getBoardingPassFlag(
            String origin,
            String departureDate,
            PassengerDataResponseACS passengerDataResponse
    ) {
        if (null == passengerDataResponse
                || null == passengerDataResponse.getPassengerItineraryList()
                || null == passengerDataResponse.getPassengerItineraryList().getPassengerItinerary()) {
            return "";
        }

        String boardingPassFlag = "";
        for (PassengerItineraryACS passengerItineraryACS : passengerDataResponse.getPassengerItineraryList().getPassengerItinerary()) {
            if (null != passengerItineraryACS
                    && null != passengerItineraryACS.getOrigin()
                    && passengerItineraryACS.getOrigin().equalsIgnoreCase(origin)
                    && null != passengerItineraryACS.getDepartureDate()
                    && passengerItineraryACS.getDepartureDate().equalsIgnoreCase(departureDate)
                    && null != passengerItineraryACS.getVCRInfoList()
                    && null != passengerItineraryACS.getVCRInfoList().getVCRInfo()
                    && null != passengerItineraryACS.getBoardingPassFlag()) {
                boardingPassFlag = passengerItineraryACS.getBoardingPassFlag();
                break;
            }
        }

        return boardingPassFlag;
    }

    /**
     * @param passenger
     * @param bookedSegment
     * @param messageNotEligibleChk
     * @return
     */
    private BookedTraveler getBookedTravelerBasicInfo(
            PassengerPNRB passenger,
            BookedSegment bookedSegment,
            String messageNotEligibleChk,
            String pos
    ) {
        BookedTraveler bookedTraveler = new BookedTraveler();

        bookedTraveler.setFirstName(passenger.getFirstName());
        if (null != passenger.getEmailAddress() && !passenger.getEmailAddress().isEmpty()) {
            bookedTraveler.setEmail(passenger.getEmailAddress().get(0).getAddress());
        }
        bookedTraveler.setLastName(passenger.getLastName());
        bookedTraveler.setDisplayName(CommonUtil.removePrefix(passenger.getFirstName()) + " " + passenger.getLastName());

        bookedTraveler.setFreeBaggageAllowance(BaggageAllowanceService.getNoneBaggageAllowance());

        BookingClassMap bookingClassMap = new BookingClassMap();
        bookedTraveler.setBookingClasses(bookingClassMap);

        //bookedTraveler.setPaxType(PaxType.ADULT);
        bookedTraveler.setTicketNumber("0000000000000");

        bookedTraveler.setCheckinStatus(false);
        bookedTraveler.setIsEligibleToCheckin(false);
        bookedTraveler.setIsSelectedToCheckin(false);

        if (null != messageNotEligibleChk) {
            LOG.error("ERROR_CHECKIN_INELIGIBLE_REASON: {} PassengerName: {} PassengerLastName: {}", messageNotEligibleChk, passenger.getFirstName(), passenger.getLastName());
            if (messageNotEligibleChk.contains("INITIALIZED")) {
                bookedTraveler.getIneligibleReasons().add(CheckinIneligibleReasons.NOTINITIALIZED.getCheckinIneligibleReason());
            } else if (messageNotEligibleChk.contains("CARRIER")) {
                bookedTraveler.getIneligibleReasons().add(CheckinIneligibleReasons.NOTOPERATEDAM.getCheckinIneligibleReason());
            } else if (messageNotEligibleChk.contains("NO_PNR_FOUND")) {
                bookedTraveler.getIneligibleReasons().add(CheckinIneligibleReasons.NO_PNR_FOUND_CK.getCheckinIneligibleReason());
            } else if (messageNotEligibleChk.contains("GROUND HANDLED") || messageNotEligibleChk.contains("GROUND")) {
                bookedTraveler.getIneligibleReasons().add(messageNotEligibleChk);
            } else if (messageNotEligibleChk.contains("CLOSED_NOT_OPERATED_BY_AM")) {
                bookedTraveler.getIneligibleReasons().add(messageNotEligibleChk);
            } else if (messageNotEligibleChk.contains("GROUP")) {
                bookedTraveler.getIneligibleReasons().add(CheckinIneligibleReasons.GROUP.getCheckinIneligibleReason());
            }
        }

        WindowTimeType windowTime;
        try {
            windowTime = getItineraryService().getWindowTime(bookedSegment.getSegment(), pos);
        } catch (Exception e) {
            windowTime = WindowTimeType.IN_CKWINDOW;
        }

        if (WindowTimeType.EARLY_CKIN.toString().equalsIgnoreCase(windowTime.toString())) {
            bookedTraveler.getIneligibleReasons().add(CheckinIneligibleReasons.EARLY.getCheckinIneligibleReason());
        } else if (WindowTimeType.LATE_CKIN.toString().equalsIgnoreCase(windowTime.toString())) {
            bookedTraveler.getIneligibleReasons().add(CheckinIneligibleReasons.LATE.getCheckinIneligibleReason());
        }

        return bookedTraveler;
    }

    /**
     * @param passengerDataResponse
     * @param bookedSegment
     * @param messageNotEligibleChk
     * @return
     */
    private BookedTraveler getBookedTravelerBasicInfo(
            PassengerDataResponseACS passengerDataResponse,
            BookedSegment bookedSegment,
            String messageNotEligibleChk,
            String pos
    ) {
        BookedTraveler bookedTraveler = new BookedTraveler();

        bookedTraveler.setFirstName(passengerDataResponse.getFirstName());
        bookedTraveler.setLastName(passengerDataResponse.getLastName());
        bookedTraveler.setDisplayName(CommonUtil.removePrefix(passengerDataResponse.getFirstName()) + " " + passengerDataResponse.getLastName());

        bookedTraveler.setFreeBaggageAllowance(BaggageAllowanceService.getNoneBaggageAllowance());

        BookingClassMap bookingClassMap = new BookingClassMap();
        bookedTraveler.setBookingClasses(bookingClassMap);

        //bookedTraveler.setPaxType(PaxType.ADULT);
        bookedTraveler.setTicketNumber("0000000000000");

        String boardingPassFlag = getBoardingPassFlag(
                bookedSegment.getSegment().getDepartureAirport(),
                bookedSegment.getSegment().getDepartureDateTime().substring(0, 10),
                passengerDataResponse
        );

        if (boardingPassFlag != null && boardingPassFlag.trim().length() > 0) {
            bookedTraveler.setCheckinStatus(true);
            bookedTraveler.setIsSelectedToCheckin(false);
        } else {
            bookedTraveler.setCheckinStatus(false);
        }

        bookedTraveler.setIsEligibleToCheckin(false);
        bookedTraveler.setIsSelectedToCheckin(false);

        if (null != messageNotEligibleChk) {
            if (messageNotEligibleChk.contains("INITIALIZED")) {
                bookedTraveler.getIneligibleReasons().add(CheckinIneligibleReasons.NOTINITIALIZED.getCheckinIneligibleReason());
            } else if (messageNotEligibleChk.contains("CARRIER")) {
                bookedTraveler.getIneligibleReasons().add(CheckinIneligibleReasons.NOTOPERATEDAM.getCheckinIneligibleReason());
            } else if (messageNotEligibleChk.contains("NO_PNR_FOUND")) {
                bookedTraveler.getIneligibleReasons().add(CheckinIneligibleReasons.NO_PNR_FOUND_CK.getCheckinIneligibleReason());
            } else if (messageNotEligibleChk.contains("GROUND HANDLED") || messageNotEligibleChk.contains("GROUND")) {
                bookedTraveler.getIneligibleReasons().add(messageNotEligibleChk);
            } else if (messageNotEligibleChk.contains("CLOSED_NOT_OPERATED_BY_AM")) {
                bookedTraveler.getIneligibleReasons().add(messageNotEligibleChk);
            } else if (messageNotEligibleChk.contains("GROUP")) {
                bookedTraveler.getIneligibleReasons().add(CheckinIneligibleReasons.GROUP.getCheckinIneligibleReason());
            }
        }

        WindowTimeType windowTime;
        try {
            windowTime = getItineraryService().getWindowTime(bookedSegment.getSegment(), pos);
        } catch (Exception e) {
            windowTime = WindowTimeType.IN_CKWINDOW;
        }

        if (WindowTimeType.EARLY_CKIN.toString().equalsIgnoreCase(windowTime.toString())) {
            bookedTraveler.getIneligibleReasons().add(CheckinIneligibleReasons.EARLY.getCheckinIneligibleReason());
        } else if (WindowTimeType.LATE_CKIN.toString().equalsIgnoreCase(windowTime.toString())) {
            bookedTraveler.getIneligibleReasons().add(CheckinIneligibleReasons.LATE.getCheckinIneligibleReason());
        }

        return bookedTraveler;
    }

    private void setEligibilityForCheckin(
            BookedTraveler bookedTraveler,
            List<OpenReservationElementType> specialServiceInfoList,
            BookedLeg bookedLeg,
            String pos
    ) throws Exception {

        // SSR criteria
        for (OpenReservationElementType specialServiceInfo : specialServiceInfoList) {
            ServiceRequestType service = specialServiceInfo.getServiceRequest();

            if (PNRLookUpServiceUtil.isSsrNonEligibleForCheckIn(service)) {
                for (NameAssociationTag personName : specialServiceInfo.getNameAssociation()) {
                    String fullName = bookedTraveler.getLastName() + "/" + bookedTraveler.getFirstName();

                    if (fullName.equalsIgnoreCase(personName.getLastName()+"/"+personName.getFirstName())) {
                        bookedTraveler.setIsEligibleToCheckin(false);
                        bookedTraveler.setIsSelectedToCheckin(false);

                        bookedTraveler.getIneligibleReasons().add(
                                CheckinIneligibleReasons.SSR.getCheckinIneligibleReason() + "-" + PNRLookUpServiceUtil.getSsrNonEligibleForCheckIn(service)
                        );

                        return;
                    }
                }
            }
        }

        BookedSegment bookedSegment = bookedLeg.getFirstOpenSegment();
        WindowTimeType windowTime = getItineraryService().getWindowTime(bookedSegment.getSegment(), pos);
        if (WindowTimeType.EARLY_CKIN.toString().equalsIgnoreCase(windowTime.toString())) {
            bookedTraveler.getIneligibleReasons().add(CheckinIneligibleReasons.EARLY.getCheckinIneligibleReason());
            bookedTraveler.setIsEligibleToCheckin(false);
            bookedTraveler.setIsSelectedToCheckin(false);

            return;
        }


        if (BookedLegFlightStatusType.CANCELLED.equals(bookedLeg.getFlightStatus())) {
            bookedTraveler.getIneligibleReasons().add(CheckinIneligibleReasons.FLIGHTCANCELED.getCheckinIneligibleReason());
            bookedTraveler.setIsEligibleToCheckin(false);
            bookedTraveler.setIsSelectedToCheckin(false);

            return;
        }

        if (!BookedLegCheckinStatusType.OPEN.equals(bookedLeg.getCheckinStatus())
                && (!BookedLegCheckinStatusType.FINAL.equals(bookedLeg.getCheckinStatus())
                || !bookedTraveler.isCheckinStatus())) {

            if (!bookedLeg.isStandByReservation() || !SystemVariablesUtil.isEarlyCheckinEnabled()) {
                bookedTraveler.getIneligibleReasons().add(bookedLeg.getCheckinStatus().toString());
                bookedTraveler.setIsEligibleToCheckin(false);
                bookedTraveler.setIsSelectedToCheckin(false);

                return;
            }
        }


        // If Flight is Canceled
        if (null != bookedLeg && BookedLegCheckinStatusType.CANCEL.equals(bookedLeg.getCheckinStatus())) {
            bookedTraveler.getIneligibleReasons().add(CheckinIneligibleReasons.FLIGHTCANCELED.getCheckinIneligibleReason());
            bookedTraveler.setIsEligibleToCheckin(false);
            bookedTraveler.setIsSelectedToCheckin(false);

            return;
        }

        // If VCerR does not exists
        if (null == bookedTraveler.getTicketNumber() || bookedTraveler.getTicketNumber().trim().isEmpty()) {
            if (!bookedTraveler.isCheckinStatus()) {
                bookedTraveler.getIneligibleReasons().add(CheckinIneligibleReasons.VCR.getCheckinIneligibleReason());
                bookedTraveler.setIsEligibleToCheckin(false);
                bookedTraveler.setIsSelectedToCheckin(false);

                return;
            }
        }

        //Infant without ETKT
        if (null != bookedTraveler.getInfant()
                && (null == bookedTraveler.getInfant().getTicketNumber()
                || bookedTraveler.getInfant().getTicketNumber().trim().isEmpty())) {
            bookedTraveler.setIsEligibleToCheckin(false);
            bookedTraveler.setIsSelectedToCheckin(false);
            bookedTraveler.getIneligibleReasons().add(CheckinIneligibleReasons.VCRI.getCheckinIneligibleReason());

            return;
        }

    }

    /**
     * @return
     */
    /*
    private PersonName getPerson(List<PersonName> passengerDetails, PassengerDataResponseACS passengerDataResponse) {

        for (PersonName passenger : passengerDetails) {
            try {
                if (passenger != null
                        && PNRLookUpServiceUtil.compareNames(passenger.getGivenName(), passengerDataResponse.getFirstName())
                        && PNRLookUpServiceUtil.compareNames(passenger.getSurname(), passengerDataResponse.getLastName())
                ) {
                    return passenger;
                }
            } catch (Exception e) {
                //Continue
            }
        }

        return null;
    }
     */

    private void setMissingRequiredInfo(
            BookedTraveler bookedTraveler,
            PassengerDataResponseACS passengerDataResponseACS,
            BookedLeg bookedLeg
    ) {

        RequiredInfoListACS reqInfoList = passengerDataResponseACS.getRequiredInfoSumList();

        boolean gender = false;

        if (reqInfoList != null) {

            List<RequiredInfoACS> reqInfo = reqInfoList.getRequiredInfo();

            for (RequiredInfoACS reqInfoItem : reqInfo) {

                switch (reqInfoItem.getCode()) {
                    case "ESTA":
                        break;

                    case "GENDER":
                        //If we found information related to Gender and Sabre returns misssing info, we will remove the missin message.
                        gender = true;
                        if (bookedTraveler.getGender() == null || bookedTraveler.getGender().getCode().isEmpty()) {
                            bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("GENDER"));
                        }
                        break;

                    case "DOCV":
                        bookedTraveler.getIneligibleReasons().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("DOCV"));
                        break;

                    case "DCVI/INF":
                        bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("DCVI/INF"));
                        break;

//                    case "PCTC":
//                        bookedTraveler.getIneligibleReasons().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("PCTC"));
//                        break;
                    case "DOCS":
                        bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("DOCS"));
                        break;

                    case "DOCS/INF":
                        bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("DOCS/INF"));
                        break;

                    case "TIM":
                        bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("TIM"));
                        break;

                    case "TIM/INF":
                        bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("TIM/INF"));
                        break;

                    default:
                        try {
                            bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason(reqInfoItem.getCode()));
                        } catch (Exception e) {
                            LOG.info("Ineligible Reason not mapped in AM API:" + reqInfoItem.getCode());
                        }
                }
            }

        }

        if (!gender && null == bookedTraveler.getGender()) {
            bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("GENDER"));
        }

        if (!bookedLeg.isDomestic()) {
            if (null == bookedTraveler.getDateOfBirth() || bookedTraveler.getDateOfBirth().isEmpty()) {
                bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("DOB"));
            }
        }

        if (null != bookedTraveler.getInfant()) {
            if (null == bookedTraveler.getInfant().getGender()) {
                bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("INFANT_GENDER"));
            }

            if (!bookedLeg.isDomestic()) {
                if (null == bookedTraveler.getInfant().getDateOfBirth() || bookedTraveler.getInfant().getDateOfBirth().isEmpty()) {
                    bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("INFANT_DOB"));
                }
            }

            if (!bookedLeg.isDomestic()) {
                if (null != bookedTraveler.getInfant().getDateOfBirth()) {
                    if (null == bookedTraveler.getInfant().getTravelDocument()) {
                        if (!bookedTraveler.getMissingCheckinRequiredFields().contains(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("DOCS/INF"))) {
                            bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("DOCS/INF"));
                        }
                    }
                }
            }
        }

        //ResidenceOfCountry for Connectign flights
        if (includeCountryOfResidence(bookedTraveler.getMissingCheckinRequiredFields(), bookedLeg)) {
            bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("DOCA/R"));
        }

        if (null == bookedTraveler.getEmail() || bookedTraveler.getEmail().isEmpty()) {
            bookedTraveler.getMissingCheckinRequiredFields().add("EMAIL");
        }

        if (null == bookedTraveler.getPhones() || bookedTraveler.getPhones().getCollection().isEmpty()) {
            bookedTraveler.getMissingCheckinRequiredFields().add("PHONE");
        }
    }

    private boolean includeCountryOfResidence(Set<String> missingCheckinRequiredFields, BookedLeg bookedLeg) {
        if (bookedLeg.getSegments().getLegType().equalsIgnoreCase(LegType.CONNECTING.toString())) {
            try {
                AirportWeather airportWeather = getAirportService().getAirportFromIata(getLatestArrivalAirportByLeg(bookedLeg));
                if (null != airportWeather) {
                    if (airportWeather.getAirport().getCountryCode().contains("US") && !missingCheckinRequiredFields.contains(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("DOCA/R"))) {
                        return true;
                    }
                }
            } catch (Exception e) {
                LOG.info("getCityNameByIATACode - ( " + e.getMessage() + " )");
            }
        }

        return false;
    }

    private String getLatestArrivalAirportByLeg(BookedLeg bookedLeg) {
        BookedSegment bookedSegment = ListsUtil.getLast(bookedLeg.getSegments().getCollection());
        return bookedSegment.getSegment().getArrivalAirport();
    }

    private void setACSInfo(BookedTraveler bookedTraveler, PassengerDataResponseACS passData, String tierLever) {

        LOG.info("setACSInfo()");

        if (passData == null) {
            //This should never happen
            return;
        }

        FreeTextInfoListACS freeTextInfoList = passData.getFreeTextInfoList();

        if (freeTextInfoList == null) {
            return;
        }

        List<FreeTextInfoACS> freeTextInfoACSList = freeTextInfoList.getFreeTextInfo();

        if (null != freeTextInfoACSList) {

            List<String> ticketList = getTickets(passData);
            boolean sameTicket = containTicket(ticketList, bookedTraveler.getTicketNumber());
            sameTicket = (sameTicket || containTicket(bookedTraveler.getAllTicketNumbers(), ticketList));

            for (FreeTextInfoACS freeTextInfo : freeTextInfoACSList) {
                if (sameTicket) {

                    LOG.info("freeTextInfo: {}", freeTextInfo);

                    if (freeTextInfo != null) {

                        String editCode = freeTextInfo.getEditCode();

                        LOG.info("editCode: {}, ", editCode);

                        // String lineID = freeTextInfo.getLineID();
                        TextListACS tlACS = freeTextInfo.getTextLine();
                        LOG.info("Text Line: {}, ", tlACS);

                        if (null != editCode && null != tlACS) {

                            List<String> tlTextList = tlACS.getText();

                            for (String tlStr : tlTextList) {

                                LOG.info("Text: {}, ", tlStr);

                                if ("DOCS".equalsIgnoreCase(editCode)) {

                                    CommonServiceUtil.parseDOCS(getCountryISODao(), bookedTraveler, tlStr);

                                } else if ("INF".equalsIgnoreCase(editCode)) {

                                    CommonServiceUtil.parseINF(bookedTraveler, tlStr);

                                } else if ("DHS".equalsIgnoreCase(editCode)) {

                                    if (tlStr.contains("SELECTEE")) {
                                        bookedTraveler.setSelectee(true);
                                    }

                                } else if ("FF".equalsIgnoreCase(editCode) || editCode.equalsIgnoreCase(tierLever)) {

                                    if (tlStr != null && tlStr.length() > 0) {
                                        if (tlStr.length() > 2) {
                                            bookedTraveler.setFrequentFlyerProgram(tlStr.substring(0, 2));
                                        }
                                        String[] listFF = tlStr.split(" ");
                                        if (listFF.length > 1) {
                                            bookedTraveler.setFrequentFlyerNumber(listFF[0].substring(2, listFF[0].length()));
                                        } else {
                                            bookedTraveler.setFrequentFlyerNumber(tlStr.substring(2, tlStr.length()));
                                        }
                                    }

                                } else if ("FQTV".equalsIgnoreCase(editCode)) {

                                    if (tlStr != null && tlStr.length() > 0) {
                                        if (tlStr.length() > 2) {
                                            bookedTraveler.setFrequentFlyerProgram(tlStr.substring(0, 2));
                                        }
                                        bookedTraveler.setFrequentFlyerNumber(tlStr.substring(2, tlStr.length()));
                                    }

                                } else if ("IFET".equalsIgnoreCase(editCode)) {

                                    //  1392106439577 C02 29JUL E MEXDFW OK
                                    if (tlStr == null || tlStr.length() < 13) {
                                        continue;
                                    }

                                    if (bookedTraveler.getInfant() == null) {
                                        bookedTraveler.setInfant(new Infant());
                                    }

                                    bookedTraveler.getInfant().setTicketNumber(tlStr.substring(0, 13));

                                } else if ("DOCA".equalsIgnoreCase(editCode)) {

                                    String[] countryOfResStr = new String[2];
                                    StringTokenizer st = new StringTokenizer(tlStr, "/");
                                    int i = 0;
                                    while (st.hasMoreTokens()) {
                                        countryOfResStr[i] = st.nextToken();
                                        i = i + 1;
                                        if (i == 2) {
                                            break;
                                        }
                                    }

                                    if (i == 2) {
                                        try {
                                            if (3 == countryOfResStr[1].length()) {
                                                countryOfResStr[1] = getCountryISODao().getCountryISO2ByIso3(countryOfResStr[1]);
                                            }
                                        } catch (Exception ex) {
                                            LOG.error(ex.getMessage(), ex);
                                        }

                                        bookedTraveler.setCountryOfResidence(countryOfResStr[1]);
                                    }
                                    if (i == 2) {
                                        try {
                                            if (3 == countryOfResStr[1].length()) {
                                                countryOfResStr[1] = getCountryISODao().getCountryISO2ByIso3(countryOfResStr[1]);
                                            }
                                        } catch (Exception ex) {
                                            LOG.error(ex.getMessage(), ex);
                                        }
                                        //We validate that we're setting Residence (R) instead of Documentation (D) to the countryOfResidence value
                                        if (countryOfResStr[0].equalsIgnoreCase("R")) {
                                            bookedTraveler.setCountryOfResidence(countryOfResStr[1]);
                                        }
                                    }

                                } else if ("ETO".equalsIgnoreCase(editCode)) {

                                    Set<String> ineligibleReasons = bookedTraveler.getIneligibleReasons();
                                    String reason = ValidateCheckinIneligibleReason.getCheckinIneligibleReason("ETO");
                                    ineligibleReasons.add(reason);
                                    bookedTraveler.setIneligibleReasons(ineligibleReasons);
                                    bookedTraveler.setIsEligibleToCheckin(false);
                                    bookedTraveler.setIsSelectedToCheckin(false);

                                } else if ("ESTA".equalsIgnoreCase(editCode)) {

                                    if ("VERIFY U.S. VISA".equalsIgnoreCase(tlStr)) {
                                        bookedTraveler.setIsEligibleToCheckin(false);
                                        bookedTraveler.setIsSelectedToCheckin(false);
                                        Set<String> ineligibleReasons = bookedTraveler.getIneligibleReasons();
                                        ineligibleReasons.add("VISA VERIFICATION NEEDED");
                                        bookedTraveler.setIneligibleReasons(ineligibleReasons);
                                    }

                                } else if ("DOCO".equalsIgnoreCase(editCode)) {
                                    //DOCO = VISA information
                                    CommonServiceUtil.parseDOCO(bookedTraveler, tlStr);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @param bookedTraveler
     * @param bookedSegment
     * @param passData
     */
    private void setSSRInfo(
            BookedTraveler bookedTraveler,
            BookedSegment bookedSegment,
            PassengerDataResponseACS passData
    ) {

        if (passData == null) {
            return;
        }

        SSRRemarks sSRRemarks = new SSRRemarks();
        List<String> ssrCodes = new ArrayList<>();

        PassengerItineraryListACS passengerItinList = passData.getPassengerItineraryList();
        List<PassengerItineraryACS> passengerItinLst = passengerItinList.getPassengerItinerary();

        for (PassengerItineraryACS passengerItin : passengerItinLst) {

            if (passengerItin.getOrigin().equalsIgnoreCase(bookedSegment.getSegment().getDepartureAirport())
                    && passengerItin.getDestination().equalsIgnoreCase(bookedSegment.getSegment().getArrivalAirport())) {

                SSRInfoListACS ssrInfoListACS = passengerItin.getSSRInfoList();
                if (ssrInfoListACS != null) {
                    List<String> brandedFares = ssrInfoListACS.getBrandedFare();
                    for (String brandedFare : brandedFares) {
                        if (!ssrCodes.contains(brandedFare)) {
                            ssrCodes.add(brandedFare);
                        }
                    }
                }
                //I will comment this for now.
                EditCodeListACS editCodeListACS = passengerItin.getEditCodeList();

                if (null != editCodeListACS) {
                    for (String editCode : editCodeListACS.getEditCode()) {
                        if (!ssrCodes.contains(editCode)) {
                            ssrCodes.add(editCode);
                        }
                    }
                }
            }
        }

        sSRRemarks.setSsrCodes(ssrCodes);
        bookedTraveler.setSsrRemarks(sSRRemarks);

        if (sSRRemarks.getSsrCodes().contains("F")) {
            bookedTraveler.setGender(GenderType.F);
            bookedTraveler.getIneligibleReasons().remove("GENDER");
        } else if (sSRRemarks.getSsrCodes().contains("M")) {
            bookedTraveler.setGender(GenderType.M);
            bookedTraveler.getIneligibleReasons().remove("GENDER");
        }
    }

    /**
     * @param bookedTraveler
     * @param passData
     * @param bookedLeg
     */
    private void setSeatInfo(BookedTraveler bookedTraveler, PassengerDataResponseACS passData, BookedLeg bookedLeg) {
        if (passData == null) {
            return;
        }

        List<AbstractSegmentChoice> collection = new ArrayList<>();

        PassengerItineraryListACS passengerItinList = passData.getPassengerItineraryList();
        List<PassengerItineraryACS> passengerItinLst = passengerItinList.getPassengerItinerary();

        for (PassengerItineraryACS passengerItin : passengerItinLst) {
            if (null != passengerItin) {
                String segmentCode = PNRLookUpServiceUtil.getSegmentCodeWithFlightNumber(passengerItin);

                BookedSegment bookedSegment = bookedLeg.getBookedSegmentWithFlightNumber(segmentCode);
                if (null != bookedSegment && null != passengerItin.getSeat()) {
                    segmentCode = bookedSegment.getSegment().getSegmentCode();

                    if (null != passengerItin.getAEDetailsList()
                            && null != passengerItin.getAEDetailsList().getAEDetails()
                            && !passengerItin.getAEDetailsList().getAEDetails().isEmpty()) {

                        boolean hasPremierError = false;
                        for (AEDetailsACS aeDetailsACS : passengerItin.getAEDetailsList().getAEDetails()) {
                            if ( //("AEROMEXICO PLUS SEATS".equalsIgnoreCase(aeDetailsACS.getDescription()) || "ASIENTO PAGADO PAYSEAT".equalsIgnoreCase(aeDetailsACS.getDescription())) &&
                                    AncillaryGroupType.SA.toString().equalsIgnoreCase(aeDetailsACS.getATPCOGroupCode())) {

                                hasPremierError = true;

                                if (ActionCodeType.HI.toString().equalsIgnoreCase(aeDetailsACS.getStatusCode()) //paid
                                        || ActionCodeType.HK.toString().equalsIgnoreCase(aeDetailsACS.getStatusCode()) //waive
                                ) {
                                    SeatChoice seatChoice = new SeatChoice();
                                    seatChoice.setCode(passengerItin.getSeat());
                                    seatChoice.setSeatCharacteristics(getSeatCharacteristics());

                                    PaidSegmentChoice paidSegmentChoice = new PaidSegmentChoice();
                                    paidSegmentChoice.setSeat(seatChoice);
                                    paidSegmentChoice.setActionCode(aeDetailsACS.getStatusCode());

                                    // Pattern
                                    // [A-Z]{3}_[A-Z]{3}_[A-Z0-9]{2}_[0-9]{4}-[0-9]{2}-[0-9]{2}_[0-9]{4}$
                                    paidSegmentChoice.setSegmentCode(segmentCode);
                                    try {
                                        PaidTravelerAncillary seatAncillary = getSeatAncillary(bookedTraveler, segmentCode);
                                        if (null != seatAncillary) {
                                            paidSegmentChoice.setAncillaryId(seatAncillary.getAncillaryId());
                                            paidSegmentChoice.setLinkedID(seatAncillary.getLinkedID());
                                            paidSegmentChoice.setEmd(seatAncillary.getEmd());
                                            if (null != paidSegmentChoice.getEmd()) {
                                                paidSegmentChoice.setPaidSeat(true);
                                                if (null != seatAncillary.getTotalPrice()) {
                                                    paidSegmentChoice.setSeatCost(seatAncillary.getTotalPrice());
                                                }

                                            } else {
                                                paidSegmentChoice.setPaidSeat(false);
                                            }
                                        }

                                        if (null != aeDetailsACS.getEMDTicketNumber()) {
                                            paidSegmentChoice.setEmd(aeDetailsACS.getEMDTicketNumber().getValue());
                                        }
                                    } catch (Exception e) {
                                        //If fails -> Continue
                                    }

                                    collection.add(paidSegmentChoice);
                                } else {//unpaid
                                    TravelerAncillary travelerAncillary = ReservationUtil.getSeatAncillaryBySegmentCode(
                                            segmentCode, bookedTraveler.getSeatAncillaries().getCollection()
                                    );

                                    SeatChoiceUpsell seatChoiceUpsell = new SeatChoiceUpsell();
                                    seatChoiceUpsell.setCode(passengerItin.getSeat());

                                    if (null != aeDetailsACS.getPriceDetails()) {

                                        PriceDetailsACS priceDetailsACS = aeDetailsACS.getPriceDetails();

                                        CurrencyValue currencyValue = new CurrencyValue();
                                        currencyValue.setBase(priceDetailsACS.getBasePrice().getValue());
                                        currencyValue.setCurrencyCode(priceDetailsACS.getTotalPrice().getCurrencyCode());
                                        currencyValue.setTotal(priceDetailsACS.getTotalPrice().getValue());

                                        BigDecimal taxes = BigDecimal.ZERO;
                                        if (null != priceDetailsACS.getTaxDetails() && null != priceDetailsACS.getTaxDetails().getTaxAmount()) {
                                            for (TaxDetailsACS.TaxAmount taxtAmount : priceDetailsACS.getTaxDetails().getTaxAmount()) {
                                                currencyValue.getTaxDetails().put(taxtAmount.getTaxCode(), taxtAmount.getValue());
                                                taxes = taxes.add(taxtAmount.getValue());
                                            }
                                        }

                                        currencyValue.setTotalTax(taxes);

                                        seatChoiceUpsell.setCurrency(currencyValue);
                                    }

                                    seatChoiceUpsell.setPoints(0);
                                    seatChoiceUpsell.setSeatCharacteristics(getSeatCharacteristics());

                                    SegmentChoice segmentChoice = new SegmentChoice();

                                    if (null != travelerAncillary) {
                                        segmentChoice.setAncillaryId(travelerAncillary.getAncillaryId());
                                        segmentChoice.setLinkedID(travelerAncillary.getLinkedID());
                                    }

                                    segmentChoice.setActionCode(aeDetailsACS.getStatusCode());
                                    segmentChoice.setNameNumber(bookedTraveler.getNameRefNumber());
                                    segmentChoice.setPartOfReservation(true);
                                    segmentChoice.setSeat(seatChoiceUpsell);
                                    segmentChoice.setSeatSelectionFee(null);
                                    segmentChoice.setSegmentCode(segmentCode);

                                    collection.add(segmentChoice);
                                }
                                //only one segment choice
                                break;
                            }
                        }

                        if (!hasPremierError) {
                            SeatChoice seatChoice = new SeatChoice();
                            seatChoice.setCode(passengerItin.getSeat());
                            seatChoice.setSeatCharacteristics(getSeatCharacteristics());

                            PaidSegmentChoice paidSegmentChoice = new PaidSegmentChoice();
                            paidSegmentChoice.setSeat(seatChoice);
                            //paidSegmentChoice.setActionCode(null);

                            // Pattern
                            // [A-Z]{3}_[A-Z]{3}_[A-Z0-9]{2}_[0-9]{4}-[0-9]{2}-[0-9]{2}_[0-9]{4}$
                            paidSegmentChoice.setSegmentCode(segmentCode);

                            collection.add(paidSegmentChoice);
                        }

                    } else {
                        SeatChoice seatChoice = new SeatChoice();
                        seatChoice.setCode(passengerItin.getSeat());
                        seatChoice.setSeatCharacteristics(getSeatCharacteristics());

                        PaidSegmentChoice paidSegmentChoice = new PaidSegmentChoice();
                        paidSegmentChoice.setSeat(seatChoice);
                        //paidSegmentChoice.setActionCode(null);

                        // Pattern
                        // [A-Z]{3}_[A-Z]{3}_[A-Z0-9]{2}_[0-9]{4}-[0-9]{2}-[0-9]{2}_[0-9]{4}$
                        paidSegmentChoice.setSegmentCode(segmentCode);

                        collection.add(paidSegmentChoice);
                    }
                }
            }
        }

        BookedSegmentChoiceCollection bookedSegmentChoiceCollection = new BookedSegmentChoiceCollection();
        bookedSegmentChoiceCollection.setCollection(collection);

        bookedTraveler.setSegmentChoices(bookedSegmentChoiceCollection);
    }

    private PaidTravelerAncillary getSeatAncillary(BookedTraveler bookedTraveler, String segmentCode) {
        BookedTravelerAncillaryCollection seatAncillariesList = bookedTraveler.getSeatAncillaries();
        for (AbstractAncillary abstractAncillary : seatAncillariesList.getCollection()) {
            if (abstractAncillary instanceof PaidTravelerAncillary) {
                PaidTravelerAncillary paidTravelerAncillary = (PaidTravelerAncillary) abstractAncillary;
                if (SegmentCodeUtil.compareSegmentCodeWithoutTime(segmentCode, paidTravelerAncillary.getSegmentCodeAux())) {
                    return paidTravelerAncillary;
                }
            }
        }
        return null;
    }

    private void addBaggageRouteToBaggageRouteList(
            BaggageRouteList baggageRouteList, String segmentCode,
            String airline, String arrivalDate, String arrivalTime,
            String bookingClass, String departureDate, String departureTime,
            String destination, String flight, String origin, String passengerId,
            short segmentId, String segmentStatus
    ) {

        BaggageRoute baggageRoute = new BaggageRoute();
        baggageRoute.setSegmentCode(segmentCode);
        baggageRoute.setAirline(airline);
        baggageRoute.setArrivalDate(arrivalDate);
        baggageRoute.setArrivalTime(arrivalTime);
        baggageRoute.setBookingClass(bookingClass);
        baggageRoute.setDepartureDate(departureDate);
        baggageRoute.setDepartureTime(departureTime);
        baggageRoute.setDestination(destination);
        baggageRoute.setFlight(flight);
        baggageRoute.setOrigin(origin);
        baggageRoute.setPassengerID(passengerId);
        baggageRoute.setSegmentID(segmentId);
        baggageRoute.setSegmentStatus(segmentStatus);
        baggageRouteList.add(baggageRoute);
    }

    private void callBenefitAndRedeemedInformation(
            BookedTraveler bookedTraveler,
            BookingClassMap bookingClassMap,
            BookedLeg bookedLeg,
            TierLevelType tierLevelType
    ) throws Exception {
        TierBenefitsRS tierBenefitsRS = getTierBenefitRSClubPremier(bookedTraveler);
        TierBenefitCobrandList tierBenefitCobrandList = getBenefitsInformationByCard(tierBenefitsRS); // Benefits Cobrand
        //Set Benefits
        TierBenefit tierBenefitOfferByCode = null;
        if (null != tierLevelType) {
            tierBenefitOfferByCode = getTierBenefitDao().getTierBenefitByCode(tierLevelType.getCode());
            LOG.info("Benefit Offer by Code: {}", tierBenefitOfferByCode.toString());
        }

        if (null != tierBenefitOfferByCode) {
            AirportWeather origin = getAirportService().getAirportFromIata(
                    bookedLeg.getFirstSegmentInLeg().getSegment().getDepartureAirport()
            );
            AirportWeather arrival = getAirportService().getAirportFromIata(
                    bookedLeg.getLatestSegmentInLeg().getSegment().getArrivalAirport()
            );

            BookedTravelerBenefit bookedTravelerBenefit;
            bookedTravelerBenefit = BenefitUtil.getTierBenefitCheckIn(
                    tierBenefitOfferByCode,
                    tierBenefitCobrandList,
                    origin,
                    arrival,
                    bookedLeg.getSegments().getLegCode(),
                    bookingClassMap
            );

            if (null == bookedTravelerBenefit) {
                bookedTraveler.setBookedTravellerBenefitCollection(null);
            } else {
                BookedTravellerBenefitCollection bookedTravellerBenefitCollection = new BookedTravellerBenefitCollection();
                bookedTravellerBenefitCollection.getCollection().add(bookedTravelerBenefit);
                bookedTraveler.setBookedTravellerBenefitCollection(bookedTravellerBenefitCollection);
            }
        }
    }

    private BenefitRedeemed getBenefitRedeemed(
            BookedTraveler bookedTraveler,
            RemarksPNRB remarkInfo,
            String legCode
    ) throws Exception {
        // read remark
        String codeBenefitRedeemed = PNRLookUpServiceUtil.getRemarkBenefit(
                remarkInfo,
                bookedTraveler.getFrequentFlyerNumber(),
                legCode
        );
        BenefitRedeemed benefitRedeemed = null;
        if (null != codeBenefitRedeemed) {
            // review if exit a free bag
            benefitRedeemed = PNRLookUpServiceUtil.setBenefitInfo(
                    bookedTraveler,
                    codeBenefitRedeemed,
                    legCode
            );
        }
        return benefitRedeemed;
    }

    private List<SeatCharacteristicsType> getSeatCharacteristics() {
        List<SeatCharacteristicsType> seatCharacteristics = new ArrayList<>();

        // TODO: Set here the logic to put the seat characteristics.
        //seatCharacteristics.add(SeatCharacteristicsType.A);
        return seatCharacteristics;
    }

    /**
     * Calculates the remaining days from today to {@code departureDateAsString}
     *
     * @param departureDateAsString
     * @return Integer the days between today and departure date.
     */
    private Integer calculateDaysBeforeDeparture(String departureDateAsString) {
        DateTime now = DateTime.now();
        Integer days = null;

        if (null != departureDateAsString) {
            try {
                DateTime departureDateTime = DateTime.parse(departureDateAsString);
                days = Days.daysBetween(now, departureDateTime).getDays();
            } catch (Exception e) {
                LOG.error("Error calculating days before departure {}", departureDateAsString);
                return days;
            }
        }
        return days;
    }

    public List<String> getTextProperties() {
        String text = "";
        
        List<String> listIatarestriction = null;
    	try (InputStream input = MobileBoardingPassService.class.getClassLoader().getResourceAsStream("restrictionCountry.properties")) {

            Properties prop = new Properties();

            if (input == null) {
            	LOG.error("Sorry, unable to find restrictionCountry.properties");
                return listIatarestriction;
            }
            
            prop.load(input);
            
            text = prop.getProperty("retriction.country");
            String[] textProp = text.split(",");
            listIatarestriction = new ArrayList<>();
            for(int i =0; i < textProp.length; i++){
                listIatarestriction.add(textProp[i]);
            }
            LOG.info("properties_text: " + text);
            
        } catch (IOException ex) {
        	LOG.error("Error PNRLookupService:getTextProperties ", ex.getMessage());
        }
    	return listIatarestriction;
    }

    public List<String> getTextPropertiesAttestation() {
        String text = "";
        
        List<String> listIatarestriction = null;
    	try (InputStream input = MobileBoardingPassService.class.getClassLoader().getResourceAsStream("restrictionCountry.properties")) {

            Properties prop = new Properties();

            if (input == null) {
            	LOG.error("Sorry, unable to find restrictionCountry.properties");
                return listIatarestriction;
            }
            
            prop.load(input);
            
            text = prop.getProperty("retriction.attestation.country");
            String[] textProp = text.split(",");
            listIatarestriction = new ArrayList<>();
            for(int i =0; i < textProp.length; i++){
                listIatarestriction.add(textProp[i]);
            }
            LOG.info("properties_attestation text: " + text);
            
        } catch (IOException ex) {
        	LOG.error("Error PNRLookupService:getTextProperties:attestation ", ex.getMessage());
        }
    	return listIatarestriction;
    }

    public static String convertXmlGregorianToString(XMLGregorianCalendar xc) 
    {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
 
        GregorianCalendar gCalendar = xc.toGregorianCalendar();
 
        //Converted to date object
        Date date = gCalendar.getTime();
 
        //Formatted to String value
        String dateString = df.format(date);
 
        return dateString;
    }

    public AMPassengerDataService getPassengerData() {
        return passengerDataService;
    }

    public void setPassengerData(AMPassengerDataService passengerDataService) {
        this.passengerDataService = passengerDataService;
    }

    /**
     * @return the ticketDocumentService
     */
    public TicketDocumentService getTicketDocumentService() {
        return ticketDocumentService;
    }

    /**
     * @param ticketDocumentService the ticketDocumentService to set
     */
    public void setTicketDocumentService(TicketDocumentService ticketDocumentService) {
        this.ticketDocumentService = ticketDocumentService;
    }

    public GetReservationService getGetReservationService() {
        return getReservationService;
    }

    public void setGetReservationService(GetReservationService getReservationService) {
        this.getReservationService = getReservationService;
    }

    public TierBenefitService getTierBenefitService() {
        return tierBenefitService;
    }

    public void setTierBenefitService(TierBenefitService tierBenefitService) {
        this.tierBenefitService = tierBenefitService;
    }

    public AMAncillariesPriceListService getAncillariesPriceService() {
        return ancillariesPriceService;
    }

    public void setAncillariesPriceService(AMAncillariesPriceListService ancillariesPriceService) {
        this.ancillariesPriceService = ancillariesPriceService;
    }

    public UpdatePNRCollectionDispatcher getUpdatePNRCollectionDispatcher() {
        return updatePNRCollectionDispatcher;
    }

    public void setUpdatePNRCollectionDispatcher(UpdatePNRCollectionDispatcher updatePNRCollectionDispatcher) {
        this.updatePNRCollectionDispatcher = updatePNRCollectionDispatcher;
    }

    public PnrCollectionService getPnrCollectionService() {
        return pnrCollectionService;
    }

    public void setPnrCollectionService(PnrCollectionService pnrCollectionService) {
        this.pnrCollectionService = pnrCollectionService;
    }

    public AEService getAeService() {
        return aeService;
    }

    public void setAeService(AEService aeService) {
        this.aeService = aeService;
    }

    public SeatsService getSeatsService() {
        return seatsService;
    }

    public void setSeatsService(SeatsService seatsService) {
        this.seatsService = seatsService;
    }

    public AncillariesWaiveService getAncillariesWaiveService() {
        return ancillariesWaiveService;
    }

    public void setAncillariesWaiveService(AncillariesWaiveService ancillariesWaiveService) {
        this.ancillariesWaiveService = ancillariesWaiveService;
    }

    public FlightDetailsService getFlightDetailsService() {
        return flightDetailsService;
    }

    public void setFlightDetailsService(FlightDetailsService flightDetailsService) {
        this.flightDetailsService = flightDetailsService;
    }

    public PriorityListService getPriorityListService() {
        return priorityListService;
    }

    public void setPriorityListService(PriorityListService priorityListService) {
        this.priorityListService = priorityListService;
    }

    public BaggageAllowanceService getBaggageAllowanceService() {
        return baggageAllowanceService;
    }

    public void setBaggageAllowanceService(BaggageAllowanceService baggageAllowanceService) {
        this.baggageAllowanceService = baggageAllowanceService;
    }

    public PriorityCodesDaoCache getPriorityCodesDaoCache() {
        return priorityCodesDaoCache;
    }

    public void setPriorityCodesDaoCache(PriorityCodesDaoCache priorityCodesDaoCache) {
        this.priorityCodesDaoCache = priorityCodesDaoCache;
    }

    public MarketFlightService getMarketFlightService() {
        return marketFlightService;
    }

    public void setMarketFlightService(MarketFlightService marketFlightService) {
        this.marketFlightService = marketFlightService;
    }

    public ITierBenefitDao getTierBenefitDao() {
        return tierBenefitDao;
    }

    public void setTierBenefitDao(ITierBenefitDao tierBenefitDao) {
        this.tierBenefitDao = tierBenefitDao;
    }

    public IOverBookingDao getOverBookingDao() {
        return overBookingDao;
    }

    public void setOverBookingDao(IOverBookingDao overBookingDao) {
        this.overBookingDao = overBookingDao;
    }

    public IOverBookingConfigDao getOverBookingConfigDao() {
        return overBookingConfigDao;
    }

    public void setOverBookingConfigDao(IOverBookingConfigDao overBookingConfigDao) {
        this.overBookingConfigDao = overBookingConfigDao;
    }

    public IKioskProfileDao getKioskProfileDao() {
        return kioskProfileDao;
    }

    public void setKioskProfileDao(IKioskProfileDao kioskProfileDao) {
        this.kioskProfileDao = kioskProfileDao;
    }

    public SearchReservationService getSearchReservationService() {
        return searchReservationService;
    }

    public void setSearchReservationService(SearchReservationService searchReservationService) {
        this.searchReservationService = searchReservationService;
    }

    public AirportService getAirportService() {
        return airportService;
    }

    public void setAirportService(AirportService airportService) {
        this.airportService = airportService;
    }


    public ItineraryService getItineraryService() {
        return itineraryService;
    }

    public void setItineraryService(ItineraryService itineraryService) {
        this.itineraryService = itineraryService;
    }

    public CheckInSetUpConfigFactoryWrapper getSetUpConfigFactory() {
        return setUpConfigFactory;
    }

    public void setSetUpConfigFactory(CheckInSetUpConfigFactoryWrapper setUpConfigFactory) {
        this.setUpConfigFactory = setUpConfigFactory;
    }

    public IAirportCodesDao getAirportsCodesDao() {
        return airportsCodesDao;
    }

    public void setAirportsCodesDao(IAirportCodesDao airportsCodesDao) {
        this.airportsCodesDao = airportsCodesDao;
    }

    public ICountryISODao getCountryISODao() {
        return countryISODao;
    }

    public void setCountryISODao(ICountryISODao countryISODao) {
        this.countryISODao = countryISODao;
    }

    public ReservationUtilService getReservationUtilService() {
        return reservationUtilService;
    }

    public void setReservationUtilService(ReservationUtilService reservationUtilService) {
        this.reservationUtilService = reservationUtilService;
    }

    public ReservationLookupService getReservationLookupService() {
        return reservationLookupService;
    }

    public void setReservationLookupService(ReservationLookupService reservationLookupService) {
        this.reservationLookupService = reservationLookupService;
    }

    public CorporateRecognitionService getCorporateRecognitionService() {
        return corporateRecognitionService;
    }

    public void setCorporateRecognitionService(CorporateRecognitionService corporateRecognitionService) {
        this.corporateRecognitionService = corporateRecognitionService;
    }

}
