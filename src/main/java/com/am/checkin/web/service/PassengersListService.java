package com.am.checkin.web.service;

import com.aeromexico.commons.model.PassengerListRS;
import com.aeromexico.commons.prototypes.JSONPrototype;
import com.aeromexico.commons.web.types.PriorityCodeType;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import com.aeromexico.sabre.api.acs.AMPassengerListService;
import com.aeromexico.sharedservices.clients.PassengerListClient;
import com.sabre.services.acs.bso.passengerlist.v3.ACSPassengerListRSACS;
import com.sabre.services.acs.bso.passengerlist.v3.PassengerInfoACS;

/**
 *
 * @author
 */
@Named
@ApplicationScoped

public class PassengersListService {

    @Inject
    private AMPassengerListService passengerListService;

    @Inject
    private PassengerListClient passengerListClient;

    public int getVolunteeredCount(String airlineCode, String flightNumber, String departureCity, String departureDate) throws Exception {

        String[] displayCodes = {"PALL"};
        ACSPassengerListRSACS passengerList = passengerListService.passengerList(airlineCode, flightNumber, departureCity, null, departureDate, displayCodes);

        int volunteeredCount = 0;
        if (passengerList != null && passengerList.getPassengerInfoList() != null && passengerList.getPassengerInfoList().getPassengerInfo() != null) {
            for (PassengerInfoACS passengerInfo : passengerList.getPassengerInfoList().getPassengerInfo()) {
                if (PriorityCodeType.OVS.getCode().equalsIgnoreCase(passengerInfo.getPriorityCode())) {
                    volunteeredCount++;
                }
            }
        }

        return volunteeredCount;
    }

    public int getTotalInList(String airlineCode, String flightNumber, String departureCity, String departureDate, String[] displayCodes) throws Exception {

        ACSPassengerListRSACS passengerList = passengerListService.passengerList(airlineCode, flightNumber, departureCity, null, departureDate, displayCodes);

        if (passengerList != null && passengerList.getPassengerInfoList() != null && passengerList.getPassengerInfoList().getPassengerInfo() != null) {
            return passengerList.getPassengerInfoList().getPassengerInfo().size();
        }

        return 0;
    }

    public ACSPassengerListRSACS getPassengerList(
            String airlineCode,
            String flightNumber,
            String departureCity,
            String arrivalCity,
            String departureDate,
            String[] displayCodes
    ) throws Exception {

        ACSPassengerListRSACS passengerList = passengerListService.passengerList(
                airlineCode,
                flightNumber,
                departureCity,
                arrivalCity,
                departureDate,
                displayCodes
        );

        return passengerList;
    }

    public PassengerListRS getNewPassengerList(String endpoint, MultivaluedMap<String, Object> queryParams) throws Exception {
        Response response = passengerListClient.get(endpoint,  queryParams);
        String json = response.readEntity(String.class);
        return JSONPrototype.convert(json, PassengerListRS.class);
    }


    public AMPassengerListService getPassengerListService() {
        return passengerListService;
    }

    public void setPassengerListService(AMPassengerListService passengerListService) {
        this.passengerListService = passengerListService;
    }

}
