package com.am.checkin.web.messages;

import com.aeromexico.commons.web.types.LanguageType;
import com.aeromexico.commons.web.types.ResourceBundleType;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

/**
 *
 * @author adrian
 */
@Named
@ApplicationScoped
public class ReceiptPectabMessageProvider {

    private ResourceBundle bundleEn;
    private ResourceBundle bundleEs;

    public ResourceBundle getBundle(LanguageType language) {
        if (null == language) {
            if (null == bundleEs) {
                Locale spanishLocale = new Locale("es", "MX");
                bundleEs = ResourceBundle.getBundle(ResourceBundleType.PECTAB_RECEIPT.toString(), spanishLocale);
            }
            return bundleEs;
        } else {
            switch (language) {
                case ES:
                    if (null == bundleEs) {
                        Locale spanishLocale = new Locale("es", "MX");
                        bundleEs = ResourceBundle.getBundle(ResourceBundleType.PECTAB_RECEIPT.toString(), spanishLocale);
                    }
                    return bundleEs;
                case EN:
                    if (null == bundleEn) {
                        Locale englishLocale = new Locale("en", "US");
                        bundleEn = ResourceBundle.getBundle(ResourceBundleType.PECTAB_RECEIPT.toString(), englishLocale);
                    }
                    return bundleEn;
                default:
                    if (null == bundleEs) {
                        Locale spanishLocale = new Locale("es", "MX");
                        bundleEs = ResourceBundle.getBundle(ResourceBundleType.PECTAB_RECEIPT.toString(), spanishLocale);
                    }
                    return bundleEs;
            }
        }
    }
}
