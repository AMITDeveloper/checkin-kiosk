package com.am.checkin.web.jobs;

import com.am.checkin.web.event.dispatcher.CleanLogCollectionDispatcher;
import com.am.checkin.web.event.model.CleanLogCollectionEvent;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.inject.Inject;

import com.am.checkin.web.v2.util.SystemVariablesUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adrian
 */
@Singleton
@Startup
public class CleanLogCollectionsJob {

    private static final Logger LOG = LoggerFactory.getLogger(CleanLogCollectionsJob.class);

    @Resource
    private TimerService timerService;

    @Inject
    private CleanLogCollectionDispatcher cleanLogCollectionDispatcher;

    @PostConstruct
    public void init() {
        //cancel all timers
        if (timerService != null && timerService.getTimers() != null) {
            for (Timer timer : timerService.getTimers()) {
                if (timer.getInfo().equals("CleanLogCollectionsTimer")) {
                    timer.cancel();
                }
            }
        }

        ScheduleExpression scheduleExpression = new ScheduleExpression();
        scheduleExpression.hour("2").minute("0").second("0");
        timerService.createCalendarTimer(scheduleExpression, new TimerConfig("CleanLogCollectionsTimer", false));

    }

    @Timeout
    public void execute(Timer timer) {
        try {
            LOG.info("Cleaning log collections ");
            LOG.info(SystemVariablesUtil.getPnrLogExpireTimeInDays()+" Days old.");

            //to remove collections older than getPnrLogExpireTimeInDays days, value obtained from standalone
            Date currentDate = new Date(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(SystemVariablesUtil.getPnrLogExpireTimeInDays()));

            CleanLogCollectionEvent cleanLogCollectionEvent = new CleanLogCollectionEvent(currentDate);
            cleanLogCollectionDispatcher.publish(cleanLogCollectionEvent);
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
        }
    }

}
