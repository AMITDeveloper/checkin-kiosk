package com.am.checkin.web.jobs;

import com.am.checkin.web.event.dispatcher.CleanPnrCollectionDispatcher;
import com.am.checkin.web.event.model.CleanPnrCollectionEvent;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adrian
 */
@Singleton
@Startup
public class CleanPnrCollections {

    private static final Logger logger = LoggerFactory.getLogger(CleanPnrCollections.class);

    @Resource
    private TimerService timerService;

    @Inject
    private CleanPnrCollectionDispatcher cleanPnrCollectionDispatcher;

    private final int EXPIRE_TIME_IN_MINUTES;

    public CleanPnrCollections() {
        this.EXPIRE_TIME_IN_MINUTES = 45;
    }

    @PostConstruct
    public void init() {
        //cancel all timers
        if (timerService != null && timerService.getTimers() != null) {
            for (Timer timer : timerService.getTimers()) {
                if (timer.getInfo().equals("CleanPnrCollectionTimer")) {
                    timer.cancel();
                }
            }
        }

        ScheduleExpression scheduleExpression = new ScheduleExpression();
        scheduleExpression.hour("*").minute("*/15").second("0");
        timerService.createCalendarTimer(scheduleExpression, new TimerConfig("CleanPnrCollectionTimer", false));

    }

    //@Schedule(second = "0", minute = "30", hour = "*", persistent = true)
    @Timeout
    public void execute(Timer timer) {
        try {
            logger.info("Cleaning pnr collection");

            //to remove collections older than EXPIRE_TIME_IN_MINUTES minutes
            Date currentDate = new Date(System.currentTimeMillis() - TimeUnit.MINUTES.toMillis(EXPIRE_TIME_IN_MINUTES));

            CleanPnrCollectionEvent cleanPnrCollectionEvent = new CleanPnrCollectionEvent(currentDate);
            cleanPnrCollectionDispatcher.publish(cleanPnrCollectionEvent);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

}
