package com.am.checkin.web.jobs;

import com.am.checkin.web.event.dispatcher.CleanSeatmapCollectionDispatcher;
import com.am.checkin.web.event.model.CleanSeatmapCollectionEvent;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adrian
 */
//This will disable the publish event
//@Singleton
//@Startup
public class CleanSeatMapCollections {

    private static final Logger logger = LoggerFactory.getLogger(CleanSeatMapCollections.class);

    @Resource
    private TimerService timerService;

    @Inject
    private CleanSeatmapCollectionDispatcher cleanSeatmapCollectionDispatcher;

    private final int EXPIRE_TIME_IN_MINUTES;

    public CleanSeatMapCollections() {
        this.EXPIRE_TIME_IN_MINUTES = 30;
    }

    @PostConstruct
    public void init() {
        //cancel all timers
        if (timerService != null && timerService.getTimers() != null) {
            for (Timer timer : timerService.getTimers()) {
                if (timer.getInfo().equals("CleanSeatMapCollectionTimer")) {
                    timer.cancel();
                }
            }
        }

        ScheduleExpression scheduleExpression = new ScheduleExpression();
        scheduleExpression.hour("*").minute("*/10").second("0");
        timerService.createCalendarTimer(scheduleExpression, new TimerConfig("CleanSeatMapCollectionTimer", false));

    }

    //@Schedule(second = "0", minute = "0", hour = "2", persistent = true)
    @Timeout
    public void execute(Timer timer) {
        try {
            logger.info("Cleaning seat map collection");

            //to remove collections older than EXPIRE_TIME_IN_MINUTES minutes
            Date currentDate = new Date(System.currentTimeMillis() - TimeUnit.MINUTES.toMillis(EXPIRE_TIME_IN_MINUTES));

            CleanSeatmapCollectionEvent cleanSeatmapCollectionEvent = new CleanSeatmapCollectionEvent(currentDate);
            cleanSeatmapCollectionDispatcher.publish(cleanSeatmapCollectionEvent);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

}
