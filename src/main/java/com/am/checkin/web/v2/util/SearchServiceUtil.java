package com.am.checkin.web.v2.util;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.rq.PnrRQ;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.types.PosType;
import com.aeromexico.sabre.api.tripsearch.model.TripSearchOption;
import com.aeromexico.sabre.api.tripsearch.model.TripSearchRequest;
import com.am.checkin.web.util.PNRLookUpServiceUtil;
import javax.ws.rs.core.Response;

/**
 *
 * @author adrianleal
 */
public class SearchServiceUtil {
    
    public static TripSearchRequest createTripSearchRequestByFF(PnrRQ pnrRQ) {
        TripSearchRequest tripSearchRequest = new TripSearchRequest();
        tripSearchRequest.setLastName(pnrRQ.getLastName());
        tripSearchRequest.setFfnumber(pnrRQ.getFfNumber());
        tripSearchRequest.setFfprogram(pnrRQ.getFfProgram());
        //for checkin is always FF
        tripSearchRequest.setTripSearchOption(TripSearchOption.FF_PROGRAM);
        return tripSearchRequest;
    }

    public static TripSearchRequest createTripSearchRequestByDestinationFlight(PnrRQ pnrRQ) {
        /*
        https://kui-rc.aeromexico.io/kiosk/api/v1/checkin/pnr?pos=kiosk&store=mx&lname=ARREDONDO&fname=MARISELA&from=MEX&to=GDL
         */
        TripSearchRequest tripSearchRequest = new TripSearchRequest();

        //Flight Number
        tripSearchRequest.setFlightNumber(pnrRQ.getFlightNumber());

        //FirstName
        tripSearchRequest.setFirstName(pnrRQ.getFirstName());

        //LastName
        tripSearchRequest.setLastName(pnrRQ.getLastName());

        //Destination/arrival city
        tripSearchRequest.setArrivalCity(pnrRQ.getArrivalCity());

        //Departure city
        tripSearchRequest.setDepartCity(pnrRQ.getDepartureCity());

        //Option of Search
        tripSearchRequest.setTripSearchOption(
                TripSearchOption.valueOf(pnrRQ.getTripSearchOption())
        );

        return tripSearchRequest;
    }
    
    /**
     * @param pnrRQ
     * @return
     * @throws Exception A private method to find out what type of a PNR lookup
     * request
     */
    public static PnrRQ validateRequest(PnrRQ pnrRQ) throws Exception {

        //Validating Last Name. Last name is not required for a KIOSK search request
        if (!PosType.KIOSK.toString().equalsIgnoreCase(pnrRQ.getPos())) {
            if (null == pnrRQ.getLastName() || pnrRQ.getLastName().trim().length() < 2) {
                throw new GenericException(
                        Response.Status.BAD_REQUEST,
                        ErrorType.INVALID_LAST_NAME,
                        ErrorType.INVALID_LAST_NAME.getFullDescription()
                );
            }
        }

        //Search by record locator or VCR
        if (pnrRQ.getRecordLocator() != null) {
            if (PNRLookUpServiceUtil.getPNR_PATTERN().matcher(pnrRQ.getRecordLocator().trim().toUpperCase()).matches()) {
                pnrRQ.setTripSearchOption(TripSearchOption.CONFIRMATION_CODE.toString());
                return pnrRQ;
            } else if (PNRLookUpServiceUtil.getPNR_TN_PATTERN().matcher(pnrRQ.getRecordLocator().trim().toUpperCase()).matches()) {
                pnrRQ.setTripSearchOption(TripSearchOption.CONFIRMATION_CODE.toString());
                return pnrRQ;
            } else if (PNRLookUpServiceUtil.getE_TICKET_PATTERN().matcher(pnrRQ.getRecordLocator()).matches()) {
                pnrRQ.setTripSearchOption(TripSearchOption.VCR_CODE.toString());
                return pnrRQ;
            } else {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.BAD_REQUEST, ErrorType.BAD_REQUEST.getFullDescription());
            }
        }

        // Frequent Flyer Program
        if (pnrRQ.getFfProgram() != null && pnrRQ.getFfNumber() != null && pnrRQ.getLastName() != null) {
            if (PNRLookUpServiceUtil.getFF_PROGRAM_PATTERN().matcher(pnrRQ.getFfProgram()).matches()) {
                pnrRQ.setTripSearchOption(TripSearchOption.FF_PROGRAM.toString());
                return pnrRQ;
            } else {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.FF_PROGRAM_NO_VALID, ErrorType.FF_PROGRAM_NO_VALID.getFullDescription());
            }
        }

        // Flight Number
        if (pnrRQ.getTripSearchOption() == null && pnrRQ.getFlightNumber() != null) {
            try {
                int flightNumber = Integer.parseInt(pnrRQ.getFlightNumber());
                if (flightNumber > 9999 || flightNumber < 1) {
                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.BAD_REQUEST, "Invalid flight number: " + pnrRQ.getFlightNumber());
                }
            } catch (NumberFormatException ex) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.BAD_REQUEST, "Invalid flight number: " + pnrRQ.getFlightNumber() + "[" + ex.getMessage() + "]");
            }

            //validate flight number
            if (pnrRQ.getLastName() == null || pnrRQ.getLastName().trim().isEmpty()) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.BAD_REQUEST, "Last name can't be null.");
            } else if (pnrRQ.getFirstName() == null || pnrRQ.getFirstName().trim().isEmpty()) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.BAD_REQUEST, "First name can't be null.");
            }

            pnrRQ.setTripSearchOption(TripSearchOption.FLIGHT_NUMBER.toString());
            return pnrRQ;
        }

        // Destination
        if (pnrRQ.getTripSearchOption() == null) {
            if (pnrRQ.getArrivalCity() != null && pnrRQ.getLastName() != null) {
                if (!PNRLookUpServiceUtil.getIATA_CODE_AIRPORT_PATTERN().matcher(pnrRQ.getArrivalCity()).matches()) {
                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.BAD_REQUEST, "Invalid arrival city code: [A-Z]{3}");
                }
                pnrRQ.setTripSearchOption(TripSearchOption.DESTINATION.toString());
                return pnrRQ;
            }
        }

        if (pnrRQ.getFirstName() != null && pnrRQ.getLastName() != null) {
            pnrRQ.setTripSearchOption(TripSearchOption.DESTINATION.toString());
            return pnrRQ;
        }

        throw new GenericException(
                Response.Status.BAD_REQUEST,
                ErrorType.INFORMATION_PROVIDED_NOT_ENOUGH_FOR_SEARCH,
                ErrorType.INFORMATION_PROVIDED_NOT_ENOUGH_FOR_SEARCH.getFullDescription()
        );
    }
}
