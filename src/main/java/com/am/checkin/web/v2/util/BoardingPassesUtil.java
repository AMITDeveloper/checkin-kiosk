package com.am.checkin.web.v2.util;

import com.aeromexico.cloudfiles.AMCloudFile;
import com.aeromexico.codes.ICodesGenerator;
import com.aeromexico.codes.impl.CodesGenerator;
import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.*;
import com.aeromexico.commons.model.rq.BoardingPassesRQ;
import com.aeromexico.commons.model.weather.DailyForecasts;
import com.aeromexico.commons.web.types.AirlineCodeType;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.types.PosType;
import com.aeromexico.commons.web.types.SeatSectionCodeType;
import com.aeromexico.commons.web.util.SegmentCodeUtil;
import com.aeromexico.commons.web.util.TimeUtil;
import com.aeromexico.sabre.api.models.Eligibility;
import com.aeromexico.sabre.api.models.PassengerFeatures;
import com.aeromexico.sabre.api.models.PassengerFlight;
import com.aeromexico.weather.service.WeatherService;
import com.am.checkin.web.util.PnrCollectionUtil;
import com.am.seamless.checkin.common.models.BoardingDocument;
import com.sabre.services.acs.bso.reprintbp.v3.ItineraryACS;
import com.sabre.services.acs.bso.reprintbp.v3.PassengerInfoACS;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BoardingPassesUtil {
    private static final Logger LOG = LoggerFactory.getLogger(BoardingPassesUtil.class);
    private static final ICodesGenerator CODES_GENERATOR = new CodesGenerator();
    private static final String HOUR_FORMAT = "HH:mm";

    /**
     * Get Seat from an abstract object
     *
     * @param abstractSegmentChoiceList
     * @return Seat Object
     */
    public static Seat getSeatFromCart(List<AbstractSegmentChoice> abstractSegmentChoiceList, String segment) {
        LOG.info("Making the information of seat passenger for segment: {}", segment);
        Seat seat = new Seat();
        for (AbstractSegmentChoice abstractSegmentChoice : abstractSegmentChoiceList) {
            if (abstractSegmentChoice.getSegmentCode().equals(segment)) {
                seat.setSeatCode(abstractSegmentChoice.getSeat().getCode());
                switch (abstractSegmentChoice.getSeat().getClass().getName()) {
                    case "SeatChoice":
                        SeatChoice seatChoise = (SeatChoice) abstractSegmentChoice.getSeat();
                        seat.setSeatCharacteristics(seatChoise.getSeatCharacteristics());
                        break;
                    case "SeatChoiceUpsell":
                        SeatChoiceUpsell seatChoiceUpsell = (SeatChoiceUpsell) abstractSegmentChoice.getSeat();
                        seat.setSeatCharacteristics(seatChoiceUpsell.getSeatCharacteristics());
                        break;
                    case "SeatChoiceUpgrade":
                        SeatChoiceUpgrade seatChoiceUpgrade = (SeatChoiceUpgrade) abstractSegmentChoice.getSeat();
                        seat.setSeatCharacteristics(seatChoiceUpgrade.getSeatCharacteristics());
                        break;
                    case "SeatChoiceFareUpsell":
                        SeatChoiceFareUpsell seatChoiceFareUpsell = (SeatChoiceFareUpsell) abstractSegmentChoice.getSeat();
                        seat.setSeatCharacteristics(seatChoiceFareUpsell.getSeatCharacteristics());
                        break;
                    case "SeatChoiceFareUpgrade":
                        SeatChoiceFareUpgrade seatChoiceFareUpgrade = (SeatChoiceFareUpgrade) abstractSegmentChoice.getSeat();
                        seat.setSeatCharacteristics(seatChoiceFareUpgrade.getSeatCharacteristics());
                        break;
                    case "SeatChoiceWaivedUpgrade":
                        SeatChoiceWaivedUpgrade seatChoiceWaivedUpgrade = (SeatChoiceWaivedUpgrade) abstractSegmentChoice.getSeat();
                        seat.setSeatCharacteristics(seatChoiceWaivedUpgrade.getSeatCharacteristics());
                        break;
                }
            }
        }
        return seat;
    }

    /**
     * Look for the section code
     *
     * @param cabinClass
     * @return boolean
     */
    public static boolean isFirstClassCabin(String cabinClass) {
        // ECONOMICA = W,V,R,N, R
        // CLASICA = T,Q, L, H, K, U
        // FLEXIBLE = M, B, Y
        // CONFORT = I, D
        // PREMIER = C, J
        return cabinClass.equalsIgnoreCase("I")
                || cabinClass.equalsIgnoreCase("D")
                || cabinClass.equalsIgnoreCase("C")
                || cabinClass.equalsIgnoreCase("J");
    }

    /**
     * @param boardingPassCollection
     */
    public static void sanatizeData(BoardingPassCollection boardingPassCollection) {
        if (null == boardingPassCollection) {
            return;
        }
        for (BoardingPass boardingPass : boardingPassCollection.getCollection()) {
            if (null != boardingPass && null != boardingPass.getPectabFormat()) {
                LOG.info("Re-print TravelDocData BEFORE: " + boardingPass.getPectabFormat());
                boardingPass.setPectabFormat(boardingPass.getPectabFormat().replaceAll("\u0026lt;", ">"));
                LOG.info("Re-print TravelDocData AFTER: " + boardingPass.getPectabFormat());
            }
        }
    }

    /**
     * Parse date format for the schema pattern
     *
     * @return
     */
    public static String formatBoardingTime(String boardingTime) {
        LOG.info("Parsing the boarding time field: " + boardingTime);
        if (null != boardingTime && !boardingTime.isEmpty()) {
            boardingTime = boardingTime.toUpperCase();
            if ("AM".contains(boardingTime) || "PM".contains(boardingTime)) {
                return boardingTime;
            } else {
                //Add AM or PM
                if (boardingTime.length() == 5) {
                    SimpleDateFormat twelveHours = new SimpleDateFormat("hh:mmaa");
                    SimpleDateFormat twentyFourHours = new SimpleDateFormat(HOUR_FORMAT);
                    try {
                        boardingTime = twelveHours.format(twentyFourHours.parse(boardingTime));
                    } catch (ParseException e) {
                        LOG.error("Error to parsing time: " + e);
                    }
                }
            }
        }
        return boardingTime;
    }

    /**
     * Parse date format for the schema pattern
     *
     * @param twelve
     * @return
     */
    public static String getTwentyFour(String twelve) {
        LOG.info("Parsing the boarding time field");
        String twenty = null;
        if (twelve != null && !twelve.isEmpty()) {
            if (twelve.contains("AM") || twelve.contains("PM")) {
                SimpleDateFormat twelveHours = new SimpleDateFormat("hh:mmaa");
                SimpleDateFormat twentyFourHours = new SimpleDateFormat(HOUR_FORMAT);
                try {
                    twenty = twentyFourHours.format(twelveHours.parse(twelve));
                } catch (ParseException e) {
                    LOG.error("Error to parsing time: " + e);
                }
            } else if (twelve.length() == 5) {
                return twelve;
            }
        }
        return twenty;
    }

    /**
     * Build legCode from segment documents list
     *
     * @param bookedTraveler
     * @return
     */
    public static String getLegCodeFromDocumentList(BookedTraveler bookedTraveler) {
        try {
            if (null != bookedTraveler.getSegmentDocumentsList() && !bookedTraveler.getSegmentDocumentsList().isEmpty()) {
                SegmentDocumentList segmentDocumentList = bookedTraveler.getSegmentDocumentsList();

                if (1 == segmentDocumentList.size()) {
                    return segmentDocumentList.get(0).getSegmentCode().substring(0, 7)
                            .replace("_", "-") + "_";
                } else {
                    String departureCity = segmentDocumentList.get(0).getSegmentCode().substring(0, 3);
                    String arrivalCity = segmentDocumentList.get(segmentDocumentList.size() - 1).getSegmentCode()
                            .split("_")[1];

                    return departureCity + "-" + arrivalCity + "_";
                }
            }
        } catch (Exception e) {
            throw e;
        }

        return "";
    }

    /**
     * @param bookedTraveler
     * @param bookedLeg
     * @param recordLocator
     */
    public static void setStandByBoardingPasses(
            BookedTraveler bookedTraveler,
            BookedLeg bookedLeg,
            String recordLocator
    ) {
        bookedTraveler.setSegmentDocumentsList(new SegmentDocumentList());

        List<BookedSegment> bookedSegmentList = bookedLeg.getSegments().getSegmentsOperatedBy(AirlineCodeType.AM);

        int itinerary = 0;
        for (BookedSegment bookedSegment : bookedSegmentList) {
            itinerary++;

            if (null != bookedSegment) {
                BaggageRoute baggageRoute;
                baggageRoute = PnrCollectionUtil.getBaggageRoute(
                        bookedTraveler.getBaggageRouteList(),
                        bookedSegment.getSegment().getSegmentCode()
                );

                short segmentId;

                if (null != baggageRoute) {
                    segmentId = baggageRoute.getSegmentID();
                } else {
                    segmentId = (short) itinerary;
                }

                String nameInPnr = getNameInPnr(bookedTraveler);
                String origin = null != bookedSegment.getSegment().getDepartureAirport()
                        ? bookedSegment.getSegment().getDepartureAirport() : "";
                String destination = bookedSegment.getSegment().getArrivalAirport();
                String operatingCarrier = null != bookedSegment.getSegment().getOperatingCarrier()
                        ? bookedSegment.getSegment().getOperatingCarrier() : "";
                String flightNumber = null != bookedSegment.getSegment().getOperatingFlightCode()
                        ? bookedSegment.getSegment().getOperatingFlightCode() : "";
                String classOfService = getClassOfService(bookedTraveler, bookedSegment.getSegment().getSegmentCode());
                String eTicket = null != bookedTraveler.getTicketNumber() ? bookedTraveler.getTicketNumber() : "";

                String boardingPass = getTravelDoc(
                        nameInPnr,
                        recordLocator,
                        origin,
                        destination,
                        operatingCarrier,
                        flightNumber,
                        classOfService,
                        eTicket
                );

                SegmentDocument segmentInfo = new SegmentDocument();
                try {
                    segmentInfo.setBarcode(boardingPass);
                    segmentInfo.setBarcodeImage(
                            CODES_GENERATOR.codesGenerator2D(boardingPass)
                    );
                    segmentInfo.setQrcodeImage(
                            CODES_GENERATOR.codesGeneratorQR(boardingPass)
                    );
                } catch (Exception ex) {
                    LOG.error(ex.getMessage());
                }

                segmentInfo.setSegmentId(segmentId);
                segmentInfo.setBoardingZone(""); //3
                segmentInfo.setGate(""); //A8
                segmentInfo.setCheckInNumber("");
                segmentInfo.setSegmentCode(bookedSegment.getSegment().getSegmentCode());

                segmentInfo.setCheckinStatus(true);

                segmentInfo.setTsaPreCheck(false);
                segmentInfo.setSkyPriority(false);
                bookedTraveler.getSegmentDocumentsList().addOrReplace(segmentInfo);
            }
        }
    }

    /**
     * @param nameInPnr
     * @param recordLocator
     * @param origin
     * @param destination
     * @param operatingCarrier
     * @param flightNumber
     * @param classOfService
     * @param ticketNumber
     * @return
     */
    private static String getTravelDoc(
            String nameInPnr,
            String recordLocator,
            String origin,
            String destination,
            String operatingCarrier,
            String flightNumber,
            String classOfService,
            String ticketNumber) {

        return "M1"
                + ((nameInPnr.length() <= 21) ? StringUtils.leftPad(nameInPnr, 21, " ") : nameInPnr.substring(0, 21))
                + recordLocator + " "
                + origin + destination + operatingCarrier + " "
                + StringUtils.leftPad(flightNumber, 4, "0")
                + " 000" + classOfService + "000 0000 000" + "&lt;" + "0000  0000BAM"
                + StringUtils.rightPad("", 40, " ")
                + "2A"
                + StringUtils.rightPad(ticketNumber, 13, " ") + " 0"
                + StringUtils.rightPad("", 26, " ")
                + "N";
    }

    private static String getDepartureDate(Segment segment) {
        try {
            return TimeUtil.getStrTime(
                    segment.getDepartureDateTime(),
                    "yyyy-MM-dd'T'HH:mm:ss",
                    "ddMMM"
            );
        } catch (ParseException ex) {
            LOG.error(ex.getMessage(), ex);
        }
        return "";
    }

    private static String getDepartureTime(Segment segment) {
        try {
            return TimeUtil.getStrTime(
                    segment.getDepartureDateTime(),
                    "yyyy-MM-dd'T'HH:mm:ss",
                    "HH:mm"
            );
        } catch (ParseException ex) {
            LOG.error(ex.getMessage(), ex);
        }
        return "";
    }

    private static String getClassOfService(BookedTraveler bookedTraveler, String segmentCode) {
        try {
            BookingClass bookingClass = bookedTraveler.getBookingClasses().getBookingClass(segmentCode);
            return bookingClass.getBookingClass();
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
        return "";
    }

    private static String getNameInPnr(BookedTraveler bookedTraveler) {
        try {
            return (bookedTraveler.getLastName() + "/" + bookedTraveler.getFirstName()).toUpperCase();
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
        return "";
    }

    public static boolean isNotSelectee(PassengerFeatures passengerElement) {
        if (null != passengerElement.getEligibilities() && !passengerElement.getEligibilities().isEmpty()) {
            for (Eligibility eligibility : passengerElement.getEligibilities()) {
                if (eligibility.getReasonCode().equalsIgnoreCase("BOARDING_PASS_INHIBITED_SELECTEE")) {
                    return false;
                }
            }
        }
        return true;
    }

    public static SegmentDocument createSegmentDocumentFromBoardingPass(
            String segmentCode,
            PassengerFlight passFlight,
            BoardingPassWeb boardingPass
    ) {
        SegmentDocument segmentDocument = new SegmentDocument();
        segmentDocument.setBarcode(passFlight.getBoardingPass().getCustomBoardingPass().getBarCode());
        segmentDocument.setBarcodeImage(boardingPass.getBarcodeImage());
        segmentDocument.setQrcodeImage(boardingPass.getQrCodeImage());
        segmentDocument.setTsaPreCheck(boardingPass.isTSAPre());
        segmentDocument.setSkyPriority(boardingPass.isSkyPriority());
        segmentDocument.setBoardingZone(boardingPass.getZone());
        segmentDocument.setSegmentCode(segmentCode);
        return segmentDocument;
    }

    public static SegmentChoice getSegmentChoice(BoardingDocument boardingDocument, String segmentCode) {

        if (null == boardingDocument.getSeatNumber()) {
            return null;
        }

        SegmentChoice segmentChoice = new SegmentChoice();
        segmentChoice.setSegmentCode(segmentCode);
        SeatChoice seatChoice = new SeatChoice();

        if (boardingDocument.getSeatNumber().contains("GATE")) {
            seatChoice.setCode("GATE");
        } else if (boardingDocument.getSeatNumber().contains("INF")) {
            seatChoice.setCode("INF");
        } else {
            seatChoice.setCode(boardingDocument.getSeatNumber());
        }

        segmentChoice.setSeat(seatChoice);

        return segmentChoice;
    }

    public static SegmentStatus extractSegmentStatusSabre(BoardingDocument boardingDocument, BookedSegment bookedSegment) {
        SegmentStatus segmentStatus = new SegmentStatus();

        if (null == boardingDocument.getBoardingGate()) {
            segmentStatus.setBoardingGate("TBD");
        } else {
            segmentStatus.setBoardingGate(boardingDocument.getBoardingGate());
        }

        String boardingTimeFinal = "--:--";
        if (null != boardingDocument.getBoardingLocalDateTime()) {
            boardingTimeFinal = boardingDocument.getBoardingLocalDateTime().substring(11, 16);
        }

        segmentStatus.setBoardingTime(boardingTimeFinal);
        bookedSegment.getSegment().setBoardingTime(boardingTimeFinal);

        segmentStatus.setBoardingTerminal(boardingDocument.getBoardingTerminalName());

        segmentStatus.setArrivalGate("");
        segmentStatus.setArrivalTerminal("");

        if (null != boardingDocument.getEstimatedArrivalLocalDateTime()) {
            segmentStatus.setEstimatedArrivalTime(boardingDocument.getEstimatedArrivalLocalDateTime().substring(0, 19));
        } else {
            segmentStatus.setEstimatedArrivalTime(bookedSegment.getSegment().getEstimatedArrivalTime());
        }

        if (null != boardingDocument.getEstimatedDepartureLocalDateTime()) {
            segmentStatus.setEstimatedDepartureTime(boardingDocument.getEstimatedDepartureLocalDateTime().substring(0, 19));
        } else {
            segmentStatus.setEstimatedDepartureTime(bookedSegment.getSegment().getEstimatedDepartureTime());
        }

        segmentStatus.setStatus(bookedSegment.getSegment().getFlightStatus().toString());

        segmentStatus.setSegment(bookedSegment.getSegment());

        return segmentStatus;
    }

    /**
     * This method is user for get segment from sabre response and is used in
     * Junit test
     *
     * @param passFlight
     * @param bookedLegCollection
     * @return
     */
    public static SegmentStatus extractSegmentStatusSabre(
            PassengerFlight passFlight,
            BookedLegCollection bookedLegCollection
    ) {
        SegmentStatus segmentStatus = new SegmentStatus();

        String defaultDepartureGate = "TBD";

        if (null != passFlight
                && null != passFlight.getFlight()
                && null != passFlight.getFlight().getDepartureGate()
                && StringUtils.isNotBlank(passFlight.getFlight().getDepartureGate())) {

            String departureGate = passFlight.getFlight().getDepartureGate();

            if (!"Not asigned".equalsIgnoreCase(departureGate)
                    && !"GATE".equalsIgnoreCase(departureGate)) {
                segmentStatus.setBoardingGate(departureGate);
            } else {
                segmentStatus.setBoardingGate(defaultDepartureGate);
            }

        } else {
            segmentStatus.setBoardingGate(defaultDepartureGate);
        }

        String boardingTimeFinal = "--:--";

        if (null != passFlight) {
            if (StringUtils.isNotBlank(
                    passFlight.getBoardingPass().getCustomBoardingPass().getBoardingPassDisplayData().getBoardingTime()
            )) {
                boardingTimeFinal = passFlight.getBoardingPass().getCustomBoardingPass().getBoardingPassDisplayData().getBoardingTime().substring(0, 4);
                boardingTimeFinal = boardingTimeFinal.substring(0, 2) + ":" + boardingTimeFinal.substring(2, 4);
            }
        }

        segmentStatus.setBoardingTime(boardingTimeFinal);

        segmentStatus.setArrivalGate("");
        segmentStatus.setArrivalTerminal("");
        segmentStatus.setBoardingTerminal("");

        if (null != passFlight) {
            segmentStatus.setEstimatedArrivalTime(passFlight.getFlight().getArrivalTime());
            segmentStatus.setEstimatedDepartureTime(passFlight.getFlight().getDepartureTime());
            segmentStatus.setStatus(passFlight.getFlight().getFlightStatus());

            Segment segment = extractSegmentSabre(passFlight, bookedLegCollection);
            segmentStatus.setSegment(segment);
        }

        return segmentStatus;
    }

    /**
     * Get segment status from Leg Collection
     *
     * @param legBooked
     * @param segmentCode
     * @return
     */
    public static SegmentStatus extractSegmentStatusSabre(BookedLeg legBooked, String segmentCode) {
        LOG.info("Adding segment information in the segment: {}", segmentCode);
        SegmentStatus segmentStatus = new SegmentStatus();
        try {
            BookedSegment bookedSegment = legBooked.getSegments().getBookedSegment(segmentCode);
            if (null == bookedSegment) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.CANNOT_GET_SEGMENT_STATUS_INFORMATION);
            }
            String defaultBoardingGate = "TBD";
            if (null != bookedSegment.getSegment().getBoardingGate()
                    && !StringUtils.isBlank(bookedSegment.getSegment().getBoardingGate())) {

                String boardingGate = bookedSegment.getSegment().getBoardingGate();
                if (!"Not assigned".equalsIgnoreCase(boardingGate) && !"GATE".equalsIgnoreCase(boardingGate)) {
                    segmentStatus.setBoardingGate(boardingGate);
                } else {
                    segmentStatus.setBoardingGate(defaultBoardingGate);
                }
            } else {
                segmentStatus.setBoardingGate(defaultBoardingGate);
            }

            segmentStatus.setBoardingTerminal(bookedSegment.getSegment().getBoardingTerminal());
            String boardingTimeFinal = null;
            if (StringUtils.isNotBlank(legBooked.getBoardingTime())) {
                boardingTimeFinal = BoardingPassesUtil.getTwentyFour(legBooked.getBoardingTime());
            }
            segmentStatus.setBoardingTime(boardingTimeFinal);
            segmentStatus.setEstimatedArrivalTime(bookedSegment.getSegment().getArrivalDateTime());
            segmentStatus.setEstimatedDepartureTime(legBooked.getEstimatedDepartureTime());
            segmentStatus.setSegment(bookedSegment.getSegment());
            segmentStatus.setStatus(legBooked.getFlightStatus().toString());
            segmentStatus.setArrivalGate("");
            segmentStatus.setArrivalTerminal("");
        } catch (Exception e) {
            LOG.error("Cannot get segment {} for the boarding passes", segmentCode, e);
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.CANNOT_GET_SEGMENT_STATUS_INFORMATION);
        }
        return segmentStatus;
    }

    /**
     * This method is user for get information in the response from Sabre
     * Digital Signature is used in JUnit test
     *
     * @param passFlight
     * @param bookedLegCollection
     * @return
     */
    public static Segment extractSegmentSabre(PassengerFlight passFlight, BookedLegCollection bookedLegCollection) {
        Segment segment = new Segment();

        segment.setLayoverToNextSegmentsInMinutes(0);

        segment.setArrivalAirport(passFlight.getFlight().getArrivalAirport());
        segment.setArrivalDateTime(passFlight.getFlight().getArrivalTime());
        segment.setDepartureAirport(passFlight.getFlight().getDepartureAirport());
        segment.setDepartureDateTime(passFlight.getFlight().getDepartureTime());
        segment.setOperatingCarrier(passFlight.getFlight().getOperatingAirlineCode());
        segment.setOperatingFlightCode(String.valueOf(passFlight.getFlight().getOperatingAirlineFlightNumber()));
        segment.setMarketingCarrier(passFlight.getFlight().getAirlineCode());
        segment.setMarketingFlightCode(String.valueOf(passFlight.getFlight().getFlightNumber()));
        segment.setIsPremierAvailable(false);
        segment.setFlightDurationInMinutes(0);

        String replaceTime = passFlight.getFlight().getDepartureTime().replace("T", "_").substring(0, 16);
        replaceTime = replaceTime.replace(":", "");
        String sb = passFlight.getFlight().getDepartureAirport() + "_"
                + passFlight.getFlight().getArrivalAirport() + "_"
                + passFlight.getFlight().getAirlineCode() + "_"
                + replaceTime;
        segment.setSegmentCode(sb);

        for (BookedLeg bookedLeg : bookedLegCollection.getCollection()) {
            for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {
                Segment segmentPnr = bookedSegment.getSegment();
                if (segmentPnr.getDepartureAirport().equals(passFlight.getFlight().getDepartureAirport())
                        && segmentPnr.getArrivalAirport().equals(passFlight.getFlight().getArrivalAirport())) {
                    segment.setFlightDurationInMinutes(segmentPnr.getFlightDurationInMinutes());
                    segment.setSegmentNumber(segmentPnr.getSegmentNumber());
                    segment.setSegmentStatus(segmentPnr.getSegmentStatus());
                    segment.setAircraftType(segmentPnr.getAircraftType());
                }
            }
        }
        return segment;
    }

    /**
     * This method is user for get seat from sabre response and is used in Junit
     * test
     *
     * @param passFlight
     * @return
     */
    public static Seat extractSeatSabre(PassengerFlight passFlight) {
        if (null == passFlight.getSeat()) {
            return null;
        }

        Seat seat = new Seat();
        seat.setSeatCode(passFlight.getSeat().getSeatCode());

        if (null != passFlight.getBoardingPass()
                && null != passFlight.getBoardingPass().getCustomBoardingPass()) {

            if (isFirstClassCabin(passFlight.getBoardingPass().getCustomBoardingPass().getBookingClass())) {
                seat.setSectionCode(SeatSectionCodeType.FIRST_CLASS);
            } else {
                seat.setSectionCode(SeatSectionCodeType.COACH);
            }

        } else {
            seat.setSectionCode(SeatSectionCodeType.COACH);
        }

        seat.setSeatCharacteristics(new ArrayList<>());
        return seat;
    }

    /**
     * @param seatCode
     * @param bookingClass
     * @return
     */
    public static Seat extractSeat(String seatCode, String bookingClass) {
        if (null == seatCode) {
            return null;
        }

        Seat seat = new Seat();
        seat.setSeatCode(seatCode);

        if (null != bookingClass) {
            if (isFirstClassCabin(bookingClass)) {
                seat.setSectionCode(SeatSectionCodeType.FIRST_CLASS);
            } else {
                seat.setSectionCode(SeatSectionCodeType.COACH);
            }
        } else {
            seat.setSectionCode(SeatSectionCodeType.COACH);
        }

        seat.setSeatCharacteristics(new ArrayList<>());
        return seat;
    }

    public static String containsSegmentCode(PassengerFlight passFlight, CartPNR cartPnr) {

        if (null != passFlight.getFlight()
                && null != passFlight.getBoardingPass()
                && null != passFlight.getBoardingPass().getCustomBoardingPass()) {
            try {

                String segmentCode = passFlight.getFlight().getDepartureAirport()
                        + "_" + passFlight.getFlight().getArrivalAirport()
                        + "_" + passFlight.getFlight().getOperatingAirlineCode()
                        + "_" + passFlight.getFlight().getDepartureTime().substring(0, 10)
                        + "_" + passFlight.getFlight().getDepartureTime().substring(11, 16).replace(":", "");

                for (BookedTraveler bookedTraveler : cartPnr.getTravelerInfo().getCollection()) {
                    for (BookingClass bookinClasses : bookedTraveler.getBookingClasses().getCollection()) {
                        if (SegmentCodeUtil.compareSegmentCodeWithoutTime(segmentCode, bookinClasses.getSegmentCode())) {
                            return segmentCode;
                        }
                    }
                }
            } catch (Exception ex) {
                LOG.error(ex.getMessage(), ex);
            }
        }

        return null;
    }

    public static void setPriorityBoarding(PNRCollection pnrCollection, String recordLocator) {
        try {
            PNR pnr = pnrCollection.getPNRByRecordLocator(recordLocator);

            for (CartPNR cartPNR : pnr.getCarts().getCollection()) {

                String cardId = haveCobranded(cartPNR);

                if (null == cardId) {
                    return;
                }

                for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
                    if (null != bookedTraveler.getSegmentDocumentsList()
                            && !bookedTraveler.getSegmentDocumentsList().isEmpty()) {

                        for (SegmentDocument segmentDocument : bookedTraveler.getSegmentDocumentsList()) {
                            if (null != segmentDocument) {
                                segmentDocument.setCardID(cardId);
                                segmentDocument.setPriorityBoarding(true);
                            }
                        }
                    }

                    if (null != bookedTraveler.getInfant() && null != bookedTraveler.getInfant().getSegmentDocumentsList()) {
                        for (SegmentDocument segmentDocument : bookedTraveler.getInfant().getSegmentDocumentsList()) {
                            if (null != segmentDocument) {
                                segmentDocument.setCardID(cardId);
                                segmentDocument.setPriorityBoarding(true);
                            }
                        }

                    }
                }
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
    }

    public static String haveCobranded(CartPNR cartPNR) {
        if (null == cartPNR
                || null == cartPNR.getTravelerInfo()
                || null == cartPNR.getTravelerInfo().getCollection()) {
            return null;
        }

        String cardId = null;
        int priority = 0;

        for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
            if (null != bookedTraveler.getBookedTravellerBenefitCollection()
                    && null != bookedTraveler.getBookedTravellerBenefitCollection().getCollection()
                    && !bookedTraveler.getBookedTravellerBenefitCollection().getCollection().isEmpty()) {

                for (BookedTravelerBenefit bookedTravelerBenefit : bookedTraveler.getBookedTravellerBenefitCollection().getCollection()) {

                    if (null != bookedTravelerBenefit.getBagBenefitCobrand()
                            && null != bookedTravelerBenefit.getBagBenefitCobrand().getFreeBagAllowanceByCardList()
                            && !bookedTravelerBenefit.getBagBenefitCobrand().getFreeBagAllowanceByCardList().isEmpty()) {

                        for (FreeBaggageAllowanceDetailByCard freeBaggageAllowanceDetailByCard : bookedTravelerBenefit.getBagBenefitCobrand().getFreeBagAllowanceByCardList()) {

                            String card = freeBaggageAllowanceDetailByCard.getCobrandCode();

                            if (null != card) {

                                switch (card) {
                                    case "AMXPLT":
                                        if (priority < 4) {
                                            //priority = 4;
                                            cardId = card;

                                            //is the highter priority, if one is found stop searching
                                            return cardId;
                                        }
                                        break;
                                    case "AMXGLD":
                                        if (priority < 3) {
                                            priority = 3;
                                            cardId = card;
                                        }
                                        break;
                                    case "SEIFNT":
                                        if (priority < 2) {
                                            priority = 2;
                                            cardId = card;
                                        }
                                        break;
                                    case "SEPLT":
                                        if (priority < 1) {
                                            priority = 1;
                                            cardId = card;
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
            }
        }
        return cardId;
    }

    public static void createItineraryFromBookedLeg(List<BookedLeg> legsByLegCodes, List<ItineraryACS> itineraryList) {
        for (BookedLeg legData : legsByLegCodes) {
            if (null != legData.getSegments().getCollection() && !legData.getSegments().getCollection().isEmpty()) {
                for (BookedSegment bookedSegment : legData.getSegments().getCollection()) {
                    ItineraryACS itinerary = new ItineraryACS();
                    itinerary.setAirline(bookedSegment.getSegment().getOperatingCarrier());
                    itinerary.setDepartureDate(bookedSegment.getSegment().getDepartureDateTime().substring(0, 10));
                    itinerary.setDestination(bookedSegment.getSegment().getArrivalAirport());
                    itinerary.setFlight(bookedSegment.getSegment().getOperatingFlightCode());
                    itinerary.setOrigin(bookedSegment.getSegment().getDepartureAirport());
                    itineraryList.add(itinerary);
                }
            }
        }
    }

    public static boolean needBoardingPassInfo(BookedTraveler bookedTraveler) {
        if (bookedTraveler.getSegmentDocumentsList() != null && bookedTraveler.getSegmentDocumentsList().size() > 0) {
            for (SegmentDocument segmentDocument : bookedTraveler.getSegmentDocumentsList()) {
                if (segmentDocument.getTravelDoc() == null) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Get passenger info from cart pnr
     *
     * @param cartPNR
     * @param boardingPassesRQ
     * @param segOriginal
     * @return
     */
    public static List<PassengerInfoACS> getListPassengerInfo(
            CartPNR cartPNR,
            BoardingPassesRQ boardingPassesRQ,
            String segOriginal
    ) {

        List<PassengerInfoACS> passengerInfoList = new ArrayList<>();

        for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {

            if (boardingPassesRQ.getPaxId().contains(bookedTraveler.getId())) {

                if (needBoardingPassInfo(bookedTraveler)) {
                    PassengerInfoACS passenger = new PassengerInfoACS();
                    passenger.setLastName(bookedTraveler.getLastName());
                    for (BookingClass booked : bookedTraveler.getBookingClasses().getCollection()) {
                        if (booked.getSegmentCode().substring(0, 3).equalsIgnoreCase(segOriginal)) {
                            passenger.setPassengerID(booked.getPassengerId());
                        }
                    }

                    if (null == passenger.getPassengerID() || passenger.getPassengerID().trim().isEmpty()) {
                        passenger.setPassengerID(bookedTraveler.getId());
                    }

                    passengerInfoList.add(passenger);
                }
            }
        }
        return passengerInfoList;
    }

    public static void checkErrorsPNR(BoardingPassesRQ boardingPassesRQ, PNR pnr) throws Exception {
        if (null == boardingPassesRQ.getLegCode() || boardingPassesRQ.getLegCode().trim().isEmpty()) {
            LOG.error("Cannot find the booked leg with the legcode specified: {}", boardingPassesRQ);
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.LEGCODE_REQUIRED_FOR_KIOSK);
        }

        if (null == pnr) {
            LOG.error("Invalid PNR value in the request, please verify: {}", boardingPassesRQ);
            throw new GenericException(Response.Status.NOT_FOUND, ErrorType.PNR_NOT_FOUND);
        }

        if (pnr.isTsaRequired() && !boardingPassesRQ.isOverrideTsa()) {
            LOG.error("Tsa validation required");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.TSA_VALIDATION_REQUIRED);
        }

        if (null == pnr.getCarts()
                || null == pnr.getCarts().getCollection()
                || pnr.getCarts().getCollection().isEmpty()) {
            LOG.error("The PNR doesn't have carts please validate PNR: {}", boardingPassesRQ);
            throw new GenericException(Response.Status.NOT_FOUND, ErrorType.CART_COLLECTION_OBJECT_EMPTY);
        }
    }

    /**
     * Create download link to rackspace files
     *
     * @param recordLocator
     * @param departureDate
     * @param legCode
     * @param identifier
     * @return
     */
    public static String createDownloadLinkRackspace(
            String recordLocator,
            String departureDate,
            String legCode,
            String identifier
    ) {

        if (SystemVariablesUtil.isCreateDownloadBoardingPassesLinkEnabled()) {
            try {
                AMCloudFile amCloudFile = new AMCloudFile();

                String downloadLink = amCloudFile.getPublishURL();
                //"http://a10093a1dcff20c33b04-0e7cdc201efa111f6c12c7e84417b41a.r62.cf1.rackcdn.com";
                amCloudFile.closeConnection();

                return downloadLink + "/" + recordLocator + "/" + "Aeromexico_" + recordLocator + "_"
                        + departureDate + "_"
                        + legCode
                        + identifier + ".pdf";
            } catch (Exception ex) {
                LOG.error(ex.getMessage(), ex);
                return "";
            }
        } else {
            return "";
        }
    }

    public static DailyForecasts getWeatherForService(String city, String pos, String language) {
        DailyForecasts weatherCityDest = null;

        if (PosType.KIOSK.toString().equalsIgnoreCase(pos)) {
            LOG.info("Trying to get the weather of destination");
            boolean isLanguageMX = false;
            Pattern pattern = Pattern.compile("ES_?.?");
            if (null != language) {
                Matcher matxh = pattern.matcher(language.toUpperCase());
                isLanguageMX = matxh.matches();
            }
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String formatArrival = sdf.format(new Date());

            try {
                weatherCityDest = WeatherService.getWeather(city, formatArrival, isLanguageMX);
            } catch (Exception e) {
                LOG.error("Cannot complete the weather service call, {}", e);
            }
        }

        return weatherCityDest;
    }
}
