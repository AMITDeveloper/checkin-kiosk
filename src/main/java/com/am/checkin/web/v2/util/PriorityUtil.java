package com.am.checkin.web.v2.util;

import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.FrequentFlyer;
import com.aeromexico.commons.web.types.TierPriority;
import com.aeromexico.sharedservices.util.RevenuePriorityCodeUtil;
import com.sabre.services.acs.bso.checkinpassenger.v3.PriorityClassificationInfoACS;

import java.util.Collections;
import java.util.List;

public class PriorityUtil {

//    public static PriorityClassificationInfoACS getPriorityClassificationInfo(
//            BookedTraveler bookedTraveler, String bookingClass
//    ) {
//        PriorityClassificationInfoACS priorityClassificationInfoACS = new PriorityClassificationInfoACS();
//        if (null != bookedTraveler.getCompanionLoyaltyCode() && null != bookedTraveler.getCompanionLoyaltyProgram()) {
//            FrequentFlyer frequentFlyer = bookedTraveler.getLoyaltyNumbers().get(0);
//
//            if (null != frequentFlyer
//                    && null != frequentFlyer.getTierPriority()) {
//
//                if (TierPriority.CLA.equals(frequentFlyer.getTierPriority())) {
//                    if (!"Y".equalsIgnoreCase(bookingClass)) {
//                        priorityClassificationInfoACS.setPriorityCode(bookedTraveler.getCompanionLoyaltyCode());
//                    } else {
//                        priorityClassificationInfoACS.setPriorityCode(frequentFlyer.getTierPriority().getPriorityCode());
//                    }
//                } else if (TierPriority.DLB.equals(frequentFlyer.getTierPriority())) {
//                    priorityClassificationInfoACS.setPriorityCode(bookedTraveler.getCompanionLoyaltyCode());
//                }
//            }
//        } else {
//            FrequentFlyer frequentFlyer = bookedTraveler.getLoyaltyNumbers().get(0);
//            if (null != frequentFlyer
//                    && null != frequentFlyer.getTierPriority()) {
//
//                if (TierPriority.CLA.equals(frequentFlyer.getTierPriority())) {
//                    if ("Y".equalsIgnoreCase(bookingClass)) {
//                        priorityClassificationInfoACS.setPriorityCode(frequentFlyer.getTierPriority().getPriorityCode());
//                    }
//                } else if (!TierPriority.DLB.equals(frequentFlyer.getTierPriority())) {
//                    priorityClassificationInfoACS.setPriorityCode(frequentFlyer.getTierPriority().getPriorityCode());
//                }
//            }
//        }
//        return priorityClassificationInfoACS;
//    }

    public static void setRevenuePriorityCode(List<BookedTraveler> bookedTravelerList) {
        //order based on passenger reference, first will be considered the principal
        Collections.sort(bookedTravelerList, (p1, p2) -> {
            if (null != p1.getNameRefNumber()
                    && null == p2.getNameRefNumber()) {
                return -1;
            } else if (null == p1.getNameRefNumber()
                    && null != p2.getNameRefNumber()) {
                return 1;
            } else if (null != p1.getNameRefNumber()
                    && null != p2.getNameRefNumber()) {
                Double nameRef1 = Double.valueOf(p1.getNameRefNumber());
                Double nameRef2 = Double.valueOf(p2.getNameRefNumber());

                return nameRef1.compareTo(nameRef2);
            } else {
                return 0;
            }
        });

        if (1 == bookedTravelerList.size()) {
            BookedTraveler titular = bookedTravelerList.get(0);

            if (null != titular.getTierLevel()
                    && !titular.getTierLevel().isEmpty()
                    && null != titular.getFrequentFlyerNumber()
                    && null != titular.getFrequentFlyerProgram()
                    && ("AM".equalsIgnoreCase(titular.getFrequentFlyerProgram())
                    || "DL".equalsIgnoreCase(titular.getFrequentFlyerProgram()))
            ) {
                titular.setCandidateForUpgradeList(true);
                titular.setTitular(true);

                String priorityCode = RevenuePriorityCodeUtil.getPriorityCode(titular.getTierLevel());
                titular.setPriorityCode(priorityCode);
            }
        } else if (2 == bookedTravelerList.size()) {
            BookedTraveler titular = bookedTravelerList.get(0);
            BookedTraveler companion = bookedTravelerList.get(1);

            if (null != titular.getTierLevel() && !titular.getTierLevel().isEmpty()
                    && null != titular.getFrequentFlyerNumber()
                    && null != titular.getFrequentFlyerProgram()
                    && ("AM".equalsIgnoreCase(titular.getFrequentFlyerProgram())
                    || "DL".equalsIgnoreCase(titular.getFrequentFlyerProgram()))
                    && null != companion.getTierLevel()
                    && !companion.getTierLevel().isEmpty()
                    && null != companion.getFrequentFlyerNumber()
                    && null != companion.getFrequentFlyerProgram()
                    && ("AM".equalsIgnoreCase(companion.getFrequentFlyerProgram())
                    || "DL".equalsIgnoreCase(companion.getFrequentFlyerProgram()))
            ) {

                String priorityCode = RevenuePriorityCodeUtil.getPriorityCode(titular.getTierLevel());

                if (null != priorityCode) {
                    titular.setPriorityCode(priorityCode);

                    String companionPriorityCode = RevenuePriorityCodeUtil.getCompanionPriorityCode(titular.getTierLevel());

                    if (null != companionPriorityCode) {
                        titular.setCandidateForUpgradeList(true);
                        titular.setTitular(true);

                        companion.setCandidateForUpgradeList(true);
                        companion.setCompanion(true);
                        companion.setPriorityCode(companionPriorityCode);
                    }
                }
            }
        }
    }
}
