package com.am.checkin.web.v2.model;


import java.util.ArrayList;
import java.util.Collection;

public class DigitalSignatureRqList extends ArrayList<DigitalSignatureRq> {

    /**
     * Serial version class
     */
    private static final long serialVersionUID = 1L;

    /**
     * Empty Constructor
     */
    public DigitalSignatureRqList() {

    }

    /**
     * Constructor with collection param
     *
     * @param c
     */
    public DigitalSignatureRqList(Collection<? extends DigitalSignatureRq> c) {
        super(c);
    }
}
