package com.am.checkin.web.v2.controller;

import com.am.checkin.web.v2.model.PassengerListUpdate;
import com.am.checkin.web.v2.services.AddPaxToPriorityListService;
import org.jboss.resteasy.annotations.Body;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/add-pax-to-priority-list")
public class AddPassengerToPriorityListController {

    @Inject
    private AddPaxToPriorityListService addPaxToPriorityListService;

    @POST
    @Path("/this")
    @Produces(MediaType.APPLICATION_JSON)
    public Response add(@Valid PassengerListUpdate passengerListUpdate) throws Exception {

        String response = addPaxToPriorityListService.addThis(passengerListUpdate);

        return Response.status(Response.Status.OK).entity(response).build();
    }
}
