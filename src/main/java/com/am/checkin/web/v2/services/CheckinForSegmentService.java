package com.am.checkin.web.v2.services;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.WarningCollection;
import com.aeromexico.commons.model.rq.PurchaseOrderRQ;
import com.aeromexico.sabre.api.acs.AMCheckInPassService;
import com.am.checkin.web.service.CheckInSetUpConfigFactoryWrapper;
import com.sabre.services.acs.bso.checkinpassenger.v3.ACSCheckInPassengerRSACS;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adrianleal
 */
@Named
@ApplicationScoped
public class CheckinForSegmentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CheckinForSegmentService.class);

    private static final String VERSION = "3.1.1";

    @Inject
    private AMCheckInPassService checkInPassService;

    @Inject
    @Named("CheckInSetUpConfigFactoryWrapper")
    private CheckInSetUpConfigFactoryWrapper setUpConfigFactory;

    @PostConstruct
    public void init() {
        if (null == setUpConfigFactory) {
            setUpConfigFactory = new CheckInSetUpConfigFactoryWrapper();
        }
    }

    public List<ACSCheckInPassengerRSACS> doCheckin(
            PurchaseOrderRQ purchaseOrderRQ,
            List<BookedTraveler> bookedTravelersEligibleForCheckin,
            Set<BookedTraveler> passengersSuccessfulCheckedIn,
            WarningCollection warningCollection,
            BookedSegment bookedSegment,
            boolean volunteerOffer,
            String recordLocator
    ) throws GenericException, Exception {

        if (null == passengersSuccessfulCheckedIn) {
            passengersSuccessfulCheckedIn = new HashSet<>();
        }

        String cartId = purchaseOrderRQ.getPurchaseOrder().getCartId();
        String pos = purchaseOrderRQ.getPos();
        String language = purchaseOrderRQ.getLanguage();
        String store = purchaseOrderRQ.getStore();

        List<ACSCheckInPassengerRSACS> acsCheckInPassengerRSACSList = new ArrayList<>();

        return acsCheckInPassengerRSACSList;
    }

}
