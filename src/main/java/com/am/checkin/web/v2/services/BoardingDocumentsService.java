package com.am.checkin.web.v2.services;

import com.aeromexico.codes.ICodesGenerator;
import com.aeromexico.codes.impl.CodesGenerator;
import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.loggingutils.LogUtil;
import com.aeromexico.commons.model.AbstractSegmentChoice;
import com.aeromexico.commons.model.BoardingPass;
import com.aeromexico.commons.model.BoardingPassCollection;
import com.aeromexico.commons.model.BoardingPassKiosk;
import com.aeromexico.commons.model.BoardingPassResponse;
import com.aeromexico.commons.model.BoardingPassWeb;
import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedLegCollection;
import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.BookingClass;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.Infant;
import com.aeromexico.commons.model.PNR;
import com.aeromexico.commons.model.PNRCollection;
import com.aeromexico.commons.model.PectabBoardingPass;
import com.aeromexico.commons.model.PectabBoardingPassList;
import com.aeromexico.commons.model.Seat;
import com.aeromexico.commons.model.SegmentDocument;
import com.aeromexico.commons.model.SegmentStatus;
import com.aeromexico.commons.model.WarningCollection;
import com.aeromexico.commons.model.rq.BoardingPassesRQ;
import com.aeromexico.commons.model.rq.PnrRQ;
import com.aeromexico.commons.model.weather.DailyForecasts;
import com.aeromexico.commons.web.types.AirlineCodeType;
import com.aeromexico.commons.web.types.DigitalSignatureStatusType;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.types.PassengerFareType;
import com.aeromexico.commons.web.types.PosType;
import com.aeromexico.commons.web.types.SeatSectionCodeType;
import com.aeromexico.commons.web.util.FilterPassengersUtil;
import com.aeromexico.commons.web.util.SegmentCodeUtil;
import com.aeromexico.dao.client.exception.MongoDisconnectException;
import com.aeromexico.dao.config.SetUpConfigFactory;
import com.aeromexico.dao.qualifiers.OverBookingConfigCache;
import com.aeromexico.dao.services.IOverBookingConfigDao;
import com.aeromexico.dao.services.IPNRLookupDao;
import com.aeromexico.sabre.api.acs.AMReprintBPService;
import com.aeromexico.sabre.api.models.dcci.Passenger;
import com.aeromexico.sabre.api.models.dcci.PassengerDetailsResponse;
import com.aeromexico.sabre.api.models.dcci.PassengerFlight;
import com.aeromexico.sabre.api.models.dcci.PassengerSegment;
import com.am.checkin.web.event.dispatcher.BoardingPassesDispatcher;
import com.am.checkin.web.service.AEService;
import com.am.checkin.web.service.CheckInSetUpConfigFactoryWrapper;
import com.am.checkin.web.service.MobileBoardingPassService;
import com.am.checkin.web.service.PNRLookupService;
import com.am.checkin.web.service.PassengersListService;
import com.am.checkin.web.service.PnrCollectionService;
import com.am.checkin.web.util.PnrCollectionUtil;
import com.am.checkin.web.util.PurchaseOrderUtil;
import com.am.checkin.web.v2.seamless.service.SeamlessBoardingDocumentService;
import com.am.checkin.web.v2.util.BoardingPassesUtil;
import com.am.checkin.web.v2.util.BoardingUtil;
import com.sabre.services.acs.bso.checkinpassenger.v3.ACSCheckInPassengerRSACS;
import com.sabre.services.acs.bso.reprintbp.v3.ACSReprintBPRSACS;
import com.sabre.services.acs.bso.reprintbp.v3.ItineraryACS;
import com.sabre.services.acs.bso.reprintbp.v3.PassengerInfoACS;
import com.sabre.services.stl.v3.PECTABDataListACS;
import com.sabre.services.stl.v3.PrintFormatACS;
import org.aeromexico.commons.exception.config.ReadProperties;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

//import com.am.checkin.web.service.ChangeContexService;

@Named
@ApplicationScoped
public class BoardingDocumentsService {
    private static final Logger LOG = LoggerFactory.getLogger(BoardingDocumentsService.class);

    private static final ICodesGenerator CODES_GENERATOR = new CodesGenerator();

    private static final int EMPLOYEE_INDEX = 1;

    private static final String SERVICE = "Boarding Documents Service";

    @Inject
    private MobileBoardingPassService mobileBoardingPassService;

    /**
     * Service for update collection on database
     */
    @Inject
    private PnrCollectionService updatePnrCollectionService;

    @Inject
    private DigitalSignatureService digitalSignatureService;

    @Inject
    private PassengersListService volunteeredListService;

    @Inject
    @OverBookingConfigCache
    private IOverBookingConfigDao overBookingConfigDao;

    /**
     * Dispatcher to email service for boarding pass
     */
    @Inject
    private BoardingPassesDispatcher mailDispatcher;

    /**
     * Service for do a sabreAPI call
     */
    @Inject
    private AMReprintBPService reprintService;


    /**
     * Dao for access to database
     */
    @Inject
    private IPNRLookupDao pnrLookupDao;

    @Inject
    private AEService aeService;

    /*
    @Inject
    private ChangeContexService changeContexService;
     */

    @Inject
    private PNRLookupService pnrLookupService;


    @Inject
    private SeamlessBoardingDocumentService seamlessBoardingDocumentService;

    @Inject
    private CountryValidationUtilService countryValidationUtilService;

    @Inject
    private AppleWalletUtilService appleWalletUtilService;

    @Inject
    @Named("CheckInSetUpConfigFactoryWrapper")
    private SetUpConfigFactory setUpConfigFactory;

    @Inject
    private ReadProperties prop;


    @PostConstruct
    public void init() {
        LOG.info("BoardingDocumentsService initialized.");
    }


    /**
     * Main service for boarding pass
     *
     * @param boardingPassesRQ
     * @return
     * @throws Exception
     */
    public BoardingPassCollection getBoardingPasses(BoardingPassesRQ boardingPassesRQ) throws Exception {
        PNRCollection pnrCollection = this.findInMongoPnrCollection(boardingPassesRQ.getRecordLocator(), boardingPassesRQ.getPos());

        if (null == pnrCollection) {

            //TODO: call a basic PNR to be able to call boarding passes
            try {
                PnrRQ pnrRQ = new PnrRQ();
                pnrRQ.setUserAuth("reservation=" + boardingPassesRQ.getRecordLocator() + ",name=Dummy");
                pnrRQ.setPos(boardingPassesRQ.getPos());
                pnrRQ.setStore(boardingPassesRQ.getStore());
                pnrRQ.setLanguage(boardingPassesRQ.getLanguage());
                pnrRQ.setSkipNamesValidation(true);

                pnrCollection = pnrLookupService.lookUpReservation(pnrRQ, new ArrayList<>());

                if (null == pnrCollection) {
                    if (PosType.WEB.toString().equalsIgnoreCase(boardingPassesRQ.getPos())) {
                        throw new GenericException(Response.Status.NOT_FOUND, ErrorType.EXPIRED_SESSION_WEB_CHECKIN);
                    } else {
                        throw new GenericException(Response.Status.NOT_FOUND, ErrorType.EXPIRED_SESSION);
                    }
                }
            } catch (MongoDisconnectException me) {
                if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(boardingPassesRQ.getPos())) {
                    throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getCode(),
                            prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getActionable(),
                            prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getMessage(),
                            Response.Status.INTERNAL_SERVER_ERROR);
                } else {
                    throw me;
                }
            } catch (Exception ex) {
                if (PosType.WEB.toString().equalsIgnoreCase(boardingPassesRQ.getPos())) {
                    throw new GenericException(Response.Status.NOT_FOUND, ErrorType.EXPIRED_SESSION_WEB_CHECKIN);
                } else {
                    throw new GenericException(Response.Status.NOT_FOUND, ErrorType.EXPIRED_SESSION);
                }
            }
        }

        if (null != pnrCollection.getStore() && !pnrCollection.getStore().trim().isEmpty()) {
            boardingPassesRQ.setStore(pnrCollection.getStore());
        }

        if (null != pnrCollection.getPos() && !pnrCollection.getPos().trim().isEmpty()) {
            boardingPassesRQ.setPos(pnrCollection.getPos());
        }

        String legCode = boardingPassesRQ.getLegCode();
        String recordLocator = boardingPassesRQ.getRecordLocator();

        PNR pnr = pnrCollection.getPNRByRecordLocator(recordLocator);
        if (null == pnr) {
            LOG.error(LogUtil.getLogError(SERVICE, null, recordLocator, "Pnr not found"));
            GenericException ex = new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION, "Pnr not found");
            ex.setWasLogged(true);
            throw ex;
        }

        CartPNR cartPNR = pnrCollection.getCartPNRByLegCode(legCode);

        if (null == cartPNR) {
            LOG.error(LogUtil.getLogError(SERVICE, "Cart not found", boardingPassesRQ.getRecordLocator(), null));
            GenericException ex = new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION, "Cart not found");
            ex.setWasLogged(true);
            throw ex;
        }

        BookedLeg bookedLeg = pnrCollection.getBookedLegByLegCode(legCode);

        if (null == bookedLeg) {
            LOG.error(LogUtil.getLogError(SERVICE, "BookedLeg not found: " + legCode, boardingPassesRQ.getRecordLocator(), cartPNR.getMeta().getCartId()));
            GenericException ex = new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION, "BookedLeg not found");
            ex.setWasLogged(true);
            throw ex;
        }

        BoardingPassCollection boardingPassCollection = null;

        if (cartPNR.isSeamlessCheckin()) {

            /*
            if (pnr.isTsaRequired() && !boardingPassesRQ.isOverrideTsa()) {
                LOG.error("Tsa validation required");
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.TSA_VALIDATION_REQUIRED);
            }
             */

            boardingPassCollection = seamlessBoardingDocumentService.getBoardingPassCollection(
                    boardingPassesRQ,
                    cartPNR,
                    bookedLeg,
                    pnr.isTsaRequired()
            );

            if (null != boardingPassCollection) {
                mailDispatcher.publish(pnrCollection);
            }

        } else {
            if (PosType.KIOSK.toString().equalsIgnoreCase(boardingPassesRQ.getPos())) {
                boardingPassCollection = getKioskBoardingPasses(boardingPassesRQ, pnrCollection);
            } else {
                BoardingPassResponse boardingPassResponse = getWebBoardingPasses(boardingPassesRQ, pnrCollection);
                boardingPassCollection = boardingPassResponse.getBoardingPassCollection();

                if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(boardingPassesRQ.getPos())) {
                    List<byte[]> imageFiles = mobileBoardingPassService.getMobileBoardingPass(pnrCollection);

                    for (int i = 0; i < boardingPassCollection.getCollection().size(); i++) {
                        if (null != imageFiles && imageFiles.size() > i) {
                            String imageBase64 = new CodesGenerator().convertImageToBase64(imageFiles.get(i));
                            BoardingPass boardingPass = boardingPassCollection.getCollection().get(i);
                            if (null != boardingPass) {
                                boardingPass.setMobileBoardingPass(imageBase64);
                            }
                        }
                    }
                }
            }
        }

        updatePnrCollectionService.createOrUpdatePnrCollectionAfterCheckin(
                pnrCollection,
                boardingPassesRQ.getRecordLocator(),
                cartPNR.getMeta().getCartId(),
                true
        );

        BoardingPassesUtil.sanatizeData(boardingPassCollection);

        return boardingPassCollection;
    }

    /**
     * This method is user for get boarding passes from sabre response and is
     * used in Junit test
     *
     * @param boardingPassesRQ
     * @param pnrCollection
     * @return BoardingPassResponse
     * @throws Exception
     */
    private BoardingPassResponse getWebBoardingPasses(
            BoardingPassesRQ boardingPassesRQ,
            PNRCollection pnrCollection
    ) throws Exception {
        LOG.info("getWebBoardingPasses PNR: {}", boardingPassesRQ.getRecordLocator());

        PNR pnr = pnrCollection.getPNRByRecordLocator(boardingPassesRQ.getRecordLocator());

        BoardingPassesUtil.checkErrorsPNR(boardingPassesRQ, pnr);

        BookedLeg bookedLeg = pnr.getBookedLegByLegCode(boardingPassesRQ.getLegCode().trim());
        if (null == bookedLeg) {
            LOG.error("Cannot find the booked leg with the specified legcode: {}", boardingPassesRQ);
            throw new GenericException(Response.Status.NOT_FOUND, ErrorType.LEGCODE_NOT_FOUND_IN_THE_PNR);
        }

        CartPNR cartPNR = pnr.getCartPNRByLegCode(boardingPassesRQ.getLegCode().trim());
        if (null == cartPNR) {
            LOG.error("Cannot find the cart with the specified legcode, {}", boardingPassesRQ);
            throw new GenericException(Response.Status.NOT_FOUND, ErrorType.CART_NOT_FOUND_IN_COLLECTION_WITH_LEGCODE);
        }

        List<BookedTraveler> alreadyCheckedInPassengers = FilterPassengersUtil.getAlreadyCheckedInPassengers(
                cartPNR.getTravelerInfo().getCollection()
        );

        List<List<BookedTraveler>> groupedPassengersByPassengerType = FilterPassengersUtil.groupPassengersByPassengerType(
                alreadyCheckedInPassengers
        );

        BoardingPassCollection boardingPassCollection;
        if (!alreadyCheckedInPassengers.isEmpty() && !groupedPassengersByPassengerType.get(EMPLOYEE_INDEX).isEmpty()) {

            boardingPassCollection = getEmployeeBoardingPass(
                    boardingPassesRQ,
                    pnr,
                    cartPNR,
                    bookedLeg
            );

        } else {

            boolean requireDigitalSignature = false;

            if (countryValidationUtilService.isUsaOrigin(bookedLeg)) {

                requireDigitalSignature = true;

                BoardingPassesUtil.setPriorityBoarding(pnrCollection, boardingPassesRQ.getRecordLocator());

                boardingPassCollection = getBoardingPassCollectionWithDigitalSignature(
                        boardingPassesRQ,
                        pnrCollection
                );

                if (null != boardingPassCollection) {
                    updateBoardingPassesWithDigitalSignature(pnrCollection, boardingPassCollection);
                    getMailDispatcher().publish(pnrCollection);
                    BoardingPassResponse boardingPassResponse = new BoardingPassResponse();
                    boardingPassResponse.setBoardingPassCollection(boardingPassCollection);
                    boardingPassResponse.setPnrCollection(pnrCollection);
                    return boardingPassResponse;
                }
            }

            boardingPassCollection = new BoardingPassCollection();

            for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {

                validateReprintInformation(
                        boardingPassesRQ.getPos(),
                        pnr,
                        cartPNR,
                        bookedTraveler,
                        pnrCollection.getWarnings()
                );

                List<BoardingPassWeb> boardingFilled = fillDataBoardingPassForWeb(
                        bookedTraveler,
                        bookedLeg,
                        boardingPassesRQ.getPos(),
                        pnr
                );

                if (boardingFilled != null && !boardingFilled.isEmpty()) {

                    if (requireDigitalSignature) {
                        for (BoardingPassWeb boardingPassWeb : boardingFilled) {
                            boardingPassWeb.setDigitalSignatureStatus(DigitalSignatureStatusType.MISSING);
                        }
                    }

                    boardingPassCollection.getCollection().addAll(boardingFilled);
                }

                String legCode = bookedLeg.getSegments().getLegCodeRackspace();

                /*
                try {
                    legCode = BoardingPassesUtil.getLegCodeFromDocumentList(bookedTraveler);
                } catch (Exception e) {
                    legCode = bookedLeg.getSegments().getLegCodeRackspace();
                }
                */

                String departureDate = bookedLeg.getSegments().getCollection().get(0).getSegment().getDepartureDateTime().substring(0, 10).replace("-", "_");

                String link = BoardingPassesUtil.createDownloadLinkRackspace(
                        boardingPassesRQ.getRecordLocator(),
                        departureDate,
                        legCode,
                        bookedTraveler.getFirstName().substring(0, 1)
                                + "_"
                                + bookedTraveler.getLastName()
                                + "_"
                                + bookedTraveler.getTicketNumber()
                );
                boardingPassCollection.addItineraryPdf(bookedTraveler.getTicketNumber(), link);
            }
        }

        try {
            LOG.info("Purchase: Trying to send email");
            updateBoardingPassesWithDigitalSignature(pnrCollection, boardingPassCollection);

            BoardingPassesUtil.setPriorityBoarding(pnrCollection, boardingPassesRQ.getRecordLocator());

            getMailDispatcher().publish(pnrCollection);
        } catch (Exception ex) {
            LOG.error("Error dispatching pnr collection please verify", ex);
        }

        if (null == boardingPassCollection.getCollection() || boardingPassCollection.getCollection().isEmpty()) {
            LOG.error("Error to get boarding pass information, some objects are not valid RECLOC : {}",boardingPassesRQ.getRecordLocator());
            LOG.error("RECLOC {} CartPNR BoardingDocumentsService Exception : {}",boardingPassesRQ.getRecordLocator(), cartPNR);
            String reasonsBp = null;
            String missingDataBp = null;
            if(cartPNR != null){
                Set<String> ineligbleReasons = null;
                ineligbleReasons = cartPNR.getTravelerInfo().getCollection().get(0).getIneligibleReasons();
                if(ineligbleReasons != null && !ineligbleReasons.isEmpty()){
                    for(String ireson: ineligbleReasons){
                        reasonsBp = ireson;
                        LOG.error("RECLOC_{}_BoardingDocumentsService_REASONBP:_{}_",boardingPassesRQ.getRecordLocator(), reasonsBp);
                    }	
                }

                Set<String> ineligbleMissingData = null;
                ineligbleMissingData = cartPNR.getTravelerInfo().getCollection().get(0).getMissingCheckinRequiredFields();
                if(ineligbleMissingData != null && !ineligbleMissingData.isEmpty()){
                    for(String iMissingField: ineligbleMissingData){
                        missingDataBp = iMissingField;
                        LOG.error("RECLOC_{}_BoardingDocumentsService_MISSINGDATABP:_{}_",boardingPassesRQ.getRecordLocator(), missingDataBp);
                    }	
                }
            }
            if(PosType.WEB.equals(boardingPassesRQ.getPos())){
                throw new GenericException(Response.Status.NOT_FOUND, ErrorType.BOARDING_PASSES_NOT_GENERATED_WEB);
            }else{
                throw new GenericException(Response.Status.NOT_FOUND, ErrorType.BOARDING_PASSES_NOT_GENERATED);
            }
        }

        LOG.info("Setting final data in boarding collection");
        PectabBoardingPassList pectabBoardingPassList = new PectabBoardingPassList();
        PectabBoardingPass pectabBoarding = new PectabBoardingPass();
        pectabBoarding.setValue("");
        pectabBoarding.setVersion("");
        pectabBoardingPassList.add(pectabBoarding);
        boardingPassCollection.setPnr(boardingPassesRQ.getRecordLocator());
        boardingPassCollection.setPectabBoardingPassList(pectabBoardingPassList);

        String iataCity;
        iataCity = boardingPassCollection.getCollection().get(0).getSegmentStatus().getSegment().getArrivalAirport();
        DailyForecasts wheatherCityDest;
        wheatherCityDest = BoardingPassesUtil.getWeatherForService(
                iataCity,
                boardingPassesRQ.getPos(),
                boardingPassesRQ.getLanguage()
        );

        boardingPassCollection.setDailyForecasts(wheatherCityDest);
        boardingPassCollection.setPos(boardingPassesRQ.getPos());

        //Reviewed  until the new mailing api is active
        //If there is just one pax the link for all is the same
        if (cartPNR.getTravelerInfo().getCollection().size() == 1) {
            boardingPassCollection.addItineraryPdf(
                    "ALL",
                    boardingPassCollection.getItineraryPdf().get(cartPNR.getTravelerInfo().getCollection().get(0).getTicketNumber())
            );
        } else {
            String legCode = bookedLeg.getSegments().getLegCodeRackspace();

            /*
            try {
                legCode = boardingPassCollection.getItineraryPdf().get(cartPNR.getTravelerInfo().getCollection().get(0).getTicketNumber()).split("_")[5] + "_";
            } catch (Exception e) {
                legCode = bookedLeg.getSegments().getLegCodeRackspace();
            }
            */

            String departureDate = bookedLeg.getSegments().getCollection().get(0).getSegment().getDepartureDateTime().substring(0, 10).replace("-", "_");

            String link = BoardingPassesUtil.createDownloadLinkRackspace(boardingPassesRQ.getRecordLocator(), departureDate, legCode, "ALL");
            boardingPassCollection.addItineraryPdf(
                    "ALL",
                    link
            );
        }

        LOG.info("pnrCollection after boarding passes: {}", pnrCollection.toString());

        BoardingPassResponse response = new BoardingPassResponse();
        response.setBoardingPassCollection(boardingPassCollection);
        response.setPnrCollection(pnrCollection);
        return response;

    }

    /**
     * @param boardingPassesRQ
     * @return
     * @throws Exception Sample URL service call from a Kiosk:
     *                   <p>
     *                   /checkIn/boarding-passes/RLMZJU?store=mx&pos=kiosk&language=
     *                   ES&legCode=MEX_AM_0301_2016-07-15&paxId=F35D69AD0001, F35D69AD0002
     */
    private BoardingPassCollection getKioskBoardingPasses(
            BoardingPassesRQ boardingPassesRQ,
            PNRCollection pnrCollection
    ) throws Exception {

        PNR pnr = pnrCollection.getPNRByRecordLocator(boardingPassesRQ.getRecordLocator());
        if (pnr == null) {
            // return, don't continue..
            LOG.error("Invalid PNR value in the request, please verify: {}", boardingPassesRQ);
            throw new GenericException(Response.Status.NOT_FOUND, ErrorType.PNR_NOT_FOUND);
        }

        if (pnr.isTsaRequired() && !boardingPassesRQ.isOverrideTsa()) {
            LOG.error("Tsa validation required");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.TSA_VALIDATION_REQUIRED);
        }

        if (pnr.getCarts() == null
                || pnr.getCarts().getCollection() == null
                || pnr.getCarts().getCollection().isEmpty()) {
            // return, don't continue..
            LOG.error("Invalid POS value in the request: {}", boardingPassesRQ);
            throw new GenericException(Response.Status.NOT_FOUND, ErrorType.CART_COLLECTION_OBJECT_EMPTY);
        }

        LOG.info("getKioskBoardingPasses PNR: {}", pnr);

        if (boardingPassesRQ.getLegCode() == null || boardingPassesRQ.getLegCode().trim().isEmpty()) {
            LOG.error("The legCode is required, request eupdate: {}", boardingPassesRQ);
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.LEGCODE_REQUIRED_FOR_KIOSK);
        }

        BookedLeg bookedLeg = pnr.getBookedLegByLegCode(boardingPassesRQ.getLegCode().trim());
        if (bookedLeg == null) {
            LOG.error("Cannot find the booked leg with the specified legcode, {}", boardingPassesRQ.getLegCode());
            throw new GenericException(Response.Status.NOT_FOUND, ErrorType.LEGCODE_NOT_FOUND_IN_THE_PNR);
        }

        CartPNR cartPNR = pnr.getCartPNRByLegCode(boardingPassesRQ.getLegCode().trim());
        if (boardingPassesRQ.getLegCode() != null && !boardingPassesRQ.getLegCode().trim().isEmpty()) {
            if (cartPNR == null) {
                LOG.error("Cannot find the cart with the specified legcode, {}", boardingPassesRQ);
                throw new GenericException(Response.Status.NOT_FOUND,
                        ErrorType.CART_NOT_FOUND_IN_COLLECTION_WITH_LEGCODE);
            }
        }

        // PAX ID is a required field for requests initiated from the Kiosk
        if (!validatePassengerIds(boardingPassesRQ, cartPNR)) {

            getReprintInformation(
                    pnr, boardingPassesRQ, cartPNR,
                    pnrCollection.getWarnings()
            );

            updatePnrCollectionService.createOrUpdatePnrCollectionAfterCheckin(
                    pnrCollection,
                    boardingPassesRQ.getRecordLocator(),
                    cartPNR.getMeta().getCartId(), true
            );
        }

        BoardingPassCollection boardingPassCollection = getKioskBoardingSelected(
                boardingPassesRQ, bookedLeg, cartPNR, pnr
        );

        boolean volunteerQualifier = false;

        try {
            LOG.info("Calling overbooking service");

            boolean atLeastOne = false;

            for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
                if (!bookedTraveler.isCheckinStatus()) {
                    bookedTraveler.setIsOverBookingEligible(false);
                }

                if (!atLeastOne && bookedTraveler.isIsOverBookingEligible()) {
                    atLeastOne = true;
                }

                BoardingPassKiosk boardingPass = boardingPassCollection.getBoardingPassByNameref(
                        bookedTraveler.getNameRefNumber()
                );

                if (null == boardingPass) {
                    boardingPass = new BoardingPassKiosk();
                    boardingPass.setTicketNumber(bookedTraveler.getTicketNumber());

                    if (null != bookedTraveler.getBookingClasses().getCollection()
                            && bookedTraveler.getBookingClasses().getCollection().size() > 0) {
                        boardingPass.setClassOfService(
                                bookedTraveler.getBookingClasses().getCollection().get(0).getBookingClass());
                    }

                    boardingPass.setFirstName(bookedTraveler.getFirstName());
                    boardingPass.setLastName(bookedTraveler.getLastName());
                    boardingPass.setNameRefNumber(bookedTraveler.getNameRefNumber());
                    boardingPass.setIsOverBookingEligible(bookedTraveler.isIsOverBookingEligible());
                    boardingPass.setIsSelectedToCheckin(false);

                    boardingPassCollection.getCollection().add(boardingPass);
                } else {
                    boardingPass.setIsOverBookingEligible(bookedTraveler.isIsOverBookingEligible());
                    boardingPass.setIsSelectedToCheckin(true);
                }
            }

            if (atLeastOne) {

                BookedSegment bookedSegment = bookedLeg.getSegments().getFirstSegmentOperatedBy(AirlineCodeType.AM);

                int volunteeres = volunteeredListService.getVolunteeredCount(
                        bookedSegment.getSegment().getOperatingCarrier(),
                        bookedSegment.getSegment().getOperatingFlightCode(),
                        bookedSegment.getSegment().getDepartureAirport(),
                        bookedSegment.getSegment().getDepartureDateTime()
                );

                int maxVolunteers = overBookingConfigDao.getMaxVolunteers(
                        bookedSegment.getSegment().getDepartureAirport(),
                        bookedSegment.getSegment().getOperatingFlightCode(),
                        bookedSegment.getSegment().getDepartureDateTime()
                );

                if (volunteeres < maxVolunteers) {
                    volunteerQualifier = true;
                } else {
                    if (PosType.KIOSK.toString().equalsIgnoreCase(boardingPassesRQ.getPos())) {
                        Iterator<BoardingPass> i = boardingPassCollection.getCollection().iterator();
                        int index = 0;
                        while (i.hasNext()) {
                            BoardingPass boardingPass = i.next();
                            if (boardingPass instanceof BoardingPassKiosk) {
                                BoardingPassKiosk boardingPassKiosk = (BoardingPassKiosk) boardingPass;
                                boardingPassKiosk.setIsOverBookingEligible(false);

                                boardingPassCollection.getCollection().set(index, boardingPassKiosk);
                            }
                            index++;
                        }

                        for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
                            bookedTraveler.setIsOverBookingEligible(false);
                        }
                    }
                }
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }

        boardingPassCollection.setMustOfferOverbooking(volunteerQualifier);
        return boardingPassCollection;
    }

    /**
     * @param boardingPassesRQ
     * @param cartPNR
     * @param pnr
     * @param bookedLeg
     * @return BoardingPassCollection
     * @throws Exception
     */
    private BoardingPassCollection getEmployeeBoardingPass(
            BoardingPassesRQ boardingPassesRQ,
            PNR pnr,
            CartPNR cartPNR,
            BookedLeg bookedLeg
    ) throws Exception {
        cartPNR.setTouched(true);
        pnr.setStandby(true);

        BoardingPassCollection boardingPassCollection = new BoardingPassCollection();
        for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {

            if (bookedTraveler.isCheckinStatus()
                    && PassengerFareType.E.getCode().equalsIgnoreCase(bookedTraveler.getPassengerType())) {

                bookedTraveler.setTouched(true);
                BoardingPassesUtil.setStandByBoardingPasses(bookedTraveler, bookedLeg, pnr.getPnr());

                for (AbstractSegmentChoice abstractSegmentChoice : bookedTraveler.getSegmentChoices().getCollection()) {
                    abstractSegmentChoice.getSeat().setCode("STB");
                }

                List<BoardingPassWeb> boardingFilled;
                boardingFilled = fillDataBoardingPassForWeb(
                        bookedTraveler,
                        bookedLeg,
                        boardingPassesRQ.getPos(),
                        pnr
                );

                if (boardingFilled != null && !boardingFilled.isEmpty()) {
                    boardingPassCollection.getCollection().addAll(boardingFilled);
                }

                String legCode = bookedLeg.getSegments().getLegCodeRackspace();

                /*
                try {
                    legCode = BoardingPassesUtil.getLegCodeFromDocumentList(bookedTraveler);
                } catch (Exception e) {
                    legCode = bookedLeg.getSegments().getLegCodeRackspace();
                }
                */

                String departureDate = bookedLeg.getSegments().getCollection().get(0).getSegment().getDepartureDateTime().substring(0, 10).replace("-", "_");

                String link = BoardingPassesUtil.createDownloadLinkRackspace(
                        boardingPassesRQ.getRecordLocator(),
                        departureDate,
                        legCode,
                        bookedTraveler.getFirstName().substring(0, 1)
                                + "_"
                                + bookedTraveler.getLastName()
                                + "_"
                                + bookedTraveler.getTicketNumber()
                );

                boardingPassCollection.addItineraryPdf(bookedTraveler.getTicketNumber(), link);
            }
        }
        return boardingPassCollection;
    }

    /**
     * This method allow fill the information in the boarding pass object
     *
     * @param bookedTraveler
     * @param pos
     * @param bookedLeg
     * @param pnr
     * @return
     * @throws Exception
     */
    protected List<BoardingPassWeb> fillDataBoardingPassForWeb(
            BookedTraveler bookedTraveler,
            BookedLeg bookedLeg,
            String pos,
            PNR pnr
    ) throws Exception {
        LOG.info("Fill information with booked traveler: {}", bookedTraveler.toString());
        List<BoardingPassWeb> boardingPassWebList = new ArrayList<>();
        for (SegmentDocument segmentDocument : bookedTraveler.getSegmentDocumentsList()) {
            BoardingPassWeb boardingPassWeb = this.callBoardingPassWeb(
                    segmentDocument,
                    bookedTraveler,
                    bookedLeg,
                    pos,
                    pnr
            );
            boardingPassWebList.add(boardingPassWeb);

            BoardingPassWeb boardingPassWebInfant = this.callBoardingPassWebInfant(
                    bookedTraveler,
                    segmentDocument,
                    boardingPassWeb.getSegmentStatus(),
                    pos,
                    pnr
            );
            if (null != boardingPassWebInfant) {
                boardingPassWebList.add(boardingPassWebInfant);
            }
        }
        return boardingPassWebList;
    }

    /**
     * Setting values of BoardingPassWeb object
     *
     * @param segmentDocument
     * @param bookedTraveler
     * @param bookedLeg
     * @param pos
     * @param pnr
     * @return BoardingPassWeb
     */
    private BoardingPassWeb callBoardingPassWeb(
            SegmentDocument segmentDocument,
            BookedTraveler bookedTraveler,
            BookedLeg bookedLeg,
            String pos,
            PNR pnr
    ) throws Exception {
        BoardingPassWeb boardingPassWeb = new BoardingPassWeb();
        if (!bookedTraveler.isSelectee()) {
            boardingPassWeb.setBarcodeImage(segmentDocument.getBarcodeImage());
            boardingPassWeb.setQrCodeImage(segmentDocument.getQrcodeImage());
        }
        boardingPassWeb.setTSAPre(segmentDocument.isTsaPreCheck());
        boardingPassWeb.setSkyPriority(segmentDocument.isSkyPriority());
        boardingPassWeb.setControlCode(segmentDocument.getCheckInNumber());
        boardingPassWeb.setZone(segmentDocument.getBoardingZone());

        if (null != bookedTraveler.getBookingClasses().getCollection()
                && !bookedTraveler.getBookingClasses().getCollection().isEmpty()) {

            String segmentCode = segmentDocument.getSegmentCode();
            BookingClass bookingClass = bookedTraveler.getBookingClasses().getBookingClass(segmentCode);
            if (null != bookingClass) {
                boardingPassWeb.setClassOfService(bookingClass.getBookingClass());
            } else {
                boardingPassWeb.setClassOfService(
                        bookedTraveler.getBookingClasses().getCollection().get(0).getBookingClass()
                );
            }
        }
        boardingPassWeb.setTicketNumber(bookedTraveler.getTicketNumber());
        boardingPassWeb.setNameRefNumber(bookedTraveler.getNameRefNumber());
        boardingPassWeb.setFirstName(bookedTraveler.getFirstName());
        boardingPassWeb.setLastName(bookedTraveler.getLastName());
        boardingPassWeb.setSelectee(bookedTraveler.isSelectee());
        boardingPassWeb.setFrequentFlyerNumber(bookedTraveler.getFrequentFlyerNumber());
        boardingPassWeb.setFrequentFlyerAirline(bookedTraveler.getFrequentFlyerProgram());

        List<AbstractSegmentChoice> abstractSegmentChoiceList = bookedTraveler.getSegmentChoices().getCollection();
        String segmentCode = segmentDocument.getSegmentCode();
        Seat seat = BoardingPassesUtil.getSeatFromCart(abstractSegmentChoiceList, segmentCode);
        if (BoardingPassesUtil.isFirstClassCabin(bookedTraveler.getBookingClasses().getCollection().get(0).getBookingCabin())) {
            seat.setSectionCode(SeatSectionCodeType.FIRST_CLASS);
        } else {
            seat.setSectionCode(SeatSectionCodeType.COACH);
        }
        boardingPassWeb.setSeat(seat);
        boardingPassWeb.setSegmentStatus(BoardingPassesUtil.extractSegmentStatusSabre(bookedLeg, segmentCode));

        LOG.info("Adding all parameters for url");
        boardingPassWeb.setAppleWalletPass(
                appleWalletUtilService.calculateAppleWallet(
                        boardingPassWeb.getSegmentStatus(),
                        segmentDocument.getBarcode(),
                        segmentDocument.getBoardingZone(),
                        bookedTraveler.getFirstName(),
                        bookedTraveler.getLastName(),
                        seat.getSeatCode(),
                        seat.getSectionCode(),
                        segmentDocument.isSkyPriority(),
                        segmentDocument.isTsaPreCheck(),
                        pos,
                        pnr.getPnr(),
                        bookedTraveler.getTicketNumber(),
                        bookedTraveler.getFrequentFlyerProgram(),
                        bookedTraveler.getFrequentFlyerNumber()
                )
        );
        return boardingPassWeb;
    }

    /**
     * Setting values of infant BoardingPassWeb object
     *
     * @param bookedTraveler
     * @param segmentDocument
     * @param segmentStatus
     * @param pos
     * @param pnr
     * @return
     */
    private BoardingPassWeb callBoardingPassWebInfant(
            BookedTraveler bookedTraveler,
            SegmentDocument segmentDocument,
            SegmentStatus segmentStatus,
            String pos,
            PNR pnr
    ) {
        BoardingPassWeb boardingPassWebInfant = null;
        if (null != bookedTraveler.getInfant()) {
            try {
                boardingPassWebInfant = this.fillDataInfantForWeb(bookedTraveler, segmentDocument.getSegmentCode());

                if (null != boardingPassWebInfant) {
                    boardingPassWebInfant.setSegmentStatus(segmentStatus);
                    boardingPassWebInfant.setAppleWalletPass(
                            appleWalletUtilService.calculateAppleWallet(
                                    segmentStatus,
                                    bookedTraveler.getInfant().getSegmentDocumentsList().getBySegmentCode(
                                            segmentDocument.getSegmentCode()
                                    ).getBarcode(),
                                    bookedTraveler.getInfant().getSegmentDocumentsList().getBySegmentCode(
                                            segmentDocument.getSegmentCode()
                                    ).getBoardingZone(),
                                    bookedTraveler.getInfant().getFirstName(),
                                    bookedTraveler.getInfant().getLastName(),
                                    boardingPassWebInfant.getSeat().getSeatCode(),
                                    boardingPassWebInfant.getSeat().getSectionCode(),
                                    segmentDocument.isSkyPriority(),
                                    segmentDocument.isTsaPreCheck(),
                                    pos,
                                    pnr.getPnr(),
                                    null,
                                    null,
                                    null
                            )
                    );
                }
            } catch (Exception e) {
                LOG.error(e.getMessage(), e);
                LOG.error("Cannot get the information for infant: {}", bookedTraveler.getInfant());
                throw new GenericException(Response.Status.NOT_FOUND, ErrorType.CANNOT_GET_INFANT_INFORMATION);
            }
        }
        return boardingPassWebInfant;
    }

    /**
     * This method allow to fill infant information from each booked traveler
     *
     * @param bookedTraveler
     * @param segmentCode
     * @return
     * @throws Exception
     */
    private BoardingPassWeb fillDataInfantForWeb(BookedTraveler bookedTraveler, String segmentCode) {
        LOG.info("Fill information of infant with booked traveler: {}", bookedTraveler);
        BoardingPassWeb boardingPassWeb = new BoardingPassWeb();

        boardingPassWeb.setBarcodeImage(bookedTraveler.getInfant().getSegmentDocumentsList().getBySegmentCode(segmentCode).getBarcodeImage());
        boardingPassWeb.setQrCodeImage(bookedTraveler.getInfant().getSegmentDocumentsList().getBySegmentCode(segmentCode).getQrcodeImage());
        boardingPassWeb.setTSAPre(bookedTraveler.getInfant().getSegmentDocumentsList().getBySegmentCode(segmentCode).isTsaPreCheck());
        boardingPassWeb.setSkyPriority(bookedTraveler.getInfant().getSegmentDocumentsList().getBySegmentCode(segmentCode).isSkyPriority());

        boardingPassWeb.setClassOfService(bookedTraveler.getBookingClasses().getBookingClass(segmentCode).getBookingClass());
        boardingPassWeb.setTicketNumber(bookedTraveler.getInfant().getTicketNumber());
        boardingPassWeb.setFirstName(bookedTraveler.getInfant().getFirstName());
        boardingPassWeb.setLastName(bookedTraveler.getInfant().getLastName());
        boardingPassWeb.setSelectee(bookedTraveler.isSelectee());
        boardingPassWeb.setFrequentFlyerAirline(bookedTraveler.getFrequentFlyerProgram());
        boardingPassWeb.setFrequentFlyerNumber(bookedTraveler.getFrequentFlyerNumber());
        boardingPassWeb.setControlCode(bookedTraveler.getSegmentDocumentsList().getBySegmentCode(segmentCode).getCheckInNumber());
        boardingPassWeb.setZone(bookedTraveler.getSegmentDocumentsList().getBySegmentCode(segmentCode).getBoardingZone());

        LOG.info("Get assigned seat and validating information for segment: {}", segmentCode);
        Seat seat = BoardingPassesUtil.getSeatFromCart(bookedTraveler.getSegmentChoices().getCollection(), segmentCode);
        if (BoardingPassesUtil.isFirstClassCabin(bookedTraveler.getBookingClasses().getCollection().get(0).getBookingCabin())) {
            seat.setSectionCode(SeatSectionCodeType.FIRST_CLASS);
        } else {
            seat.setSectionCode(SeatSectionCodeType.COACH);
        }
        seat.setSeatCode("INF");
        boardingPassWeb.setSeat(seat);
        return boardingPassWeb;
    }


    /**
     * @param recordLocator
     * @return
     */
    private PNRCollection findInMongoPnrCollection(String recordLocator, String pos) {
        try {
            return pnrLookupDao.readPNRCollectionByRecordLocator(recordLocator.toUpperCase());
        } catch (MongoDisconnectException me) {
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(pos)) {
                throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getCode(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getActionable(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getMessage(),
                        Response.Status.INTERNAL_SERVER_ERROR);
            } else {
                throw me;
            }
        } catch (Exception ex) {
            LOG.error("Failed to getPNR {}: {}", recordLocator, ex.getMessage());
            throw new GenericException(Response.Status.NOT_FOUND, ErrorType.PNR_COLLECTION_OBJECT_EMPTY);
        }
    }

    private BoardingPassCollection getBoardingPassCollectionWithDigitalSignature(
            BoardingPassesRQ boardingPassesRQ,
            PNRCollection pnrCollection
    ) throws Exception {

        PassengerDetailsResponse lookupResponse = digitalSignatureService.callDigitalSignatureSabre(
                boardingPassesRQ.getRecordLocator(),
                boardingPassesRQ.getStore()
        );
        BoardingPassCollection boardingPassesWeb;
        boardingPassesWeb = createWebBoardingPasses(
                lookupResponse,
                boardingPassesRQ.getLanguage(),
                boardingPassesRQ.getPos(),
                pnrCollection,
                boardingPassesRQ.getLegCode(),
                boardingPassesRQ.getRecordLocator()
        );

        return boardingPassesWeb;
    }

    /**
     * This method is user for get boarding pass collection from sabre response
     * and is used in Junit test
     *
     * @param lookupResponse
     * @param language
     * @param pos
     * @param pnrCollection
     * @param legCode
     * @param recordLocator
     * @return BoardingPassCollection
     * @throws java.lang.Exception
     */
    private BoardingPassCollection createWebBoardingPasses(
            PassengerDetailsResponse lookupResponse,
            String language,
            String pos,
            PNRCollection pnrCollection,
            String legCode,
            String recordLocator
    ) throws Exception {

        PNR pnr = pnrCollection.getPNRByRecordLocator(recordLocator);
        BookedLegCollection bookedLegCollection = pnr.getLegs();

        LOG.info("DIGITAL: " + lookupResponse);
        BoardingPassCollection boardingCollection = new BoardingPassCollection();
        List<BoardingPass> boardingPassList = new ArrayList<>();
        CartPNR cartPnr = null;

        try {
            cartPnr = pnr.getCartPNRByLegCode(legCode);
        } catch (Exception ex) {
            //TODO: throw an exception with a valid error and error code
            throw new Exception("Could not find cart for legcode");
        }

        String iataCity = "";

        for (Passenger passengerElement : lookupResponse.getReservation().getPassengers().getPassenger()) {
            String nameRef = passengerElement.getId().substring(1);
            BookedTraveler bTraveler = cartPnr.getBookedTravelerByNameref(nameRef);

            if (BoardingUtil.isNotSelectee(passengerElement)) {

                for (PassengerSegment passSegment : passengerElement.getPassengerSegments().getPassengerSegment()) {

                    for (PassengerFlight passFlight : passSegment.getPassengerFlight()) {

                        String segmentCode = BoardingUtil.containsSegmentCode(passFlight, cartPnr);

                        if (passFlight.getCheckedIn()
                                && null != segmentCode
                                && null != passFlight.getBoardingPass()) {

                            BoardingPassWeb boardingPass = new BoardingPassWeb();
                            boardingPass.setSegmentCode(segmentCode);

                            LOG.info("Fill information of passengers for pnr: {}", lookupResponse.getReservation().getRecordLocator());
                            CodesGenerator generator = new CodesGenerator();

                            boardingPass.setNameRefNumber(nameRef);

                            if (null != passFlight.getBoardingPass().getPersonName().getFirst()) {
                                boardingPass.setFirstName(passFlight.getBoardingPass().getPersonName().getFirst());
                            } else if (null != bTraveler) {
                                boardingPass.setFirstName(bTraveler.getFirstName());
                            }

                            if (null != passFlight.getBoardingPass().getPersonName().getLast()) {
                                boardingPass.setLastName(passFlight.getBoardingPass().getPersonName().getLast());
                            } else if (null != bTraveler) {
                                boardingPass.setLastName(bTraveler.getLastName());
                            }

                            if (null != passFlight.getBoardingPass().getLoyaltyAccount()) {
                                boardingPass.setFrequentFlyerAirline(passFlight.getBoardingPass().getLoyaltyAccount().getMemberAirline());
                                boardingPass.setFrequentFlyerNumber(passFlight.getBoardingPass().getLoyaltyAccount().getMemberId());
                            }

                            boolean hasDigitalSignature = false;

                            if (StringUtils.isNotBlank(passFlight.getBoardingPass().getBarCode())) {

                                boardingPass.setBarcode(passFlight.getBoardingPass().getBarCode());

                                //sample
                                //M1LOPEZ/EMILY         EBRHGOH MEXPVRAM 0142 079Y009A0026 162>5322RR9079BAM                                        2A139211019840500                          N^160MEQCIDxazxMO4rAt7uNhciy1WHZh12QhMgPS9g0NgWwtdc+lAiAkiNd3b9uLEHUEncjBeiDl0JTozKXyyOx97OAL+0y+dw==
                                String[] parts = passFlight.getBoardingPass().getBarCode().split("\\^");

                                if (null != parts && parts.length > 1) {
                                    hasDigitalSignature = true;
                                }

                                boardingPass.setBarcodeImage(
                                        generator.codesGenerator2D(
                                                passFlight.getBoardingPass().getBarCode()
                                        )
                                );
                                boardingPass.setQrCodeImage(
                                        generator.codesGeneratorQR(
                                                passFlight.getBoardingPass().getBarCode()
                                        )
                                );
                            } else {
                                LOG.error(
                                        "Error to get barcode from sabre service, response sabre {}",
                                        lookupResponse.getResults().get(0).getStatus().get(0).getMessage()
                                );
                                throw new GenericException(
                                        Response.Status.NOT_FOUND,
                                        ErrorType.UNABLE_TO_PROVIDE_BOARDING_PASS_BAR_CODE
                                );
                            }

                            boardingPass.setControlCode(passFlight.getBoardingPass().getCheckInSequenceNumber());
                            boardingPass.setClassOfService(passFlight.getBoardingPass().getFareInfo().getBookingClass());

                            boolean tsaValue = false;
                            if (null != passFlight.getBoardingPass().getDisplayData()) {
                                tsaValue = StringUtils.isNotBlank(passFlight.getBoardingPass().getDisplayData().getTsaPreCheckText());
                            }
                            boardingPass.setTSAPre(tsaValue);

                            boolean skyPriority = false;
                            if (null != passFlight.getBoardingPass().getSupplementaryData()
                                    && null != passFlight.getBoardingPass().getSupplementaryData().getSkyPriority()) {
                                skyPriority = passFlight.getBoardingPass().getSupplementaryData().getSkyPriority();
                            }
                            boardingPass.setSkyPriority(skyPriority);

                            if (StringUtils.isNotBlank(passFlight.getBoardingPass().getZone())) {
                                boardingPass.setZone(passFlight.getBoardingPass().getZone());
                            } else {
                                boardingPass.setZone("-");
                            }

                            boardingPass.setTicketNumber(passFlight.getBoardingPass().getTicketNumber().getNumber());
                            SegmentStatus segmentStatus = BoardingUtil.extractSegmentStatusSabre(passFlight, bookedLegCollection);
                            boardingPass.setSegmentStatus(segmentStatus);

                            Seat seat = BoardingUtil.extractSeatSabre(passFlight);
                            boardingPass.setSeat(seat);

                            if (hasDigitalSignature) {
                                boardingPass.setDigitalSignatureStatus(DigitalSignatureStatusType.OK);
                            } else {
                                LOG.error("MISSING DIGITAL SIGNATURE: {} {}", recordLocator, passFlight.getBoardingPass().getBarCode());
                                boardingPass.setDigitalSignatureStatus(DigitalSignatureStatusType.MISSING);
                            }

                            for (BookedTraveler bookedTraveler : cartPnr.getTravelerInfo().getCollection()) {
                                if (null != bookedTraveler.getNameRefNumber()
                                        && passengerElement.getId().contains(bookedTraveler.getNameRefNumber())) {

                                    if (bookedTraveler.isCheckinStatus()) {
                                        if (hasDigitalSignature) {
                                            bookedTraveler.setDigitalSignatureStatus(DigitalSignatureStatusType.OK);
                                        } else {
                                            bookedTraveler.setDigitalSignatureStatus(DigitalSignatureStatusType.MISSING);
                                        }

                                        //Reviewed  until the new mailing api is active
                                        String legCodeString;
                                        try {
                                            legCodeString = BoardingUtil.getLegCodeFromDocumentList(bookedTraveler);
                                        } catch (Exception e) {
                                            legCodeString = pnr.getBookedLegByLegCode(legCode).getSegments().getLegCodeRackspace();
                                        }

                                        String departureDate = pnr
                                                .getBookedLegByLegCode(legCode)
                                                .getSegments()
                                                .getCollection()
                                                .get(0)
                                                .getSegment()
                                                .getDepartureDateTime().substring(0, 10).replace("-", "_");

                                        String link = BoardingPassesUtil.createDownloadLinkRackspace(
                                                lookupResponse.getReservation().getRecordLocator(),
                                                departureDate,
                                                legCode,
                                                bookedTraveler.getFirstName().substring(0, 1)
                                                        + "_"
                                                        + bookedTraveler.getLastName()
                                                        + "_"
                                                        + bookedTraveler.getTicketNumber()
                                        );

                                        boardingCollection.addItineraryPdf(bookedTraveler.getTicketNumber(), link);

                                        boardingPass.setSelectee(bookedTraveler.isSelectee());

                                        if (bookedTraveler.getBookingClasses() != null && !bookedTraveler.getBookingClasses().getCollection().isEmpty()) {
                                            for (BookingClass bookingClass : bookedTraveler.getBookingClasses().getCollection()) {
                                                if (SegmentCodeUtil.compareSegmentCodeWithoutTime(bookingClass.getSegmentCode(), boardingPass.getSegmentStatus().getSegment().getSegmentCode())) {
                                                    bookedTraveler.getSegmentDocumentsList().addOrReplace(
                                                            BoardingUtil.createSegmentDocumentFromBoardingPass(
                                                                    segmentCode,
                                                                    passFlight,
                                                                    boardingPass)
                                                    );
                                                }
                                            }
                                        } else {
                                            bookedTraveler.getSegmentDocumentsList().addOrReplace(
                                                    BoardingUtil.createSegmentDocumentFromBoardingPass(
                                                            segmentCode,
                                                            passFlight,
                                                            boardingPass)
                                            );
                                        }
                                    } else {
                                        bookedTraveler.setDigitalSignatureStatus(DigitalSignatureStatusType.NOT_CHECKED_IN);
                                    }
                                    break;
                                }
                            }

                            if (null != passFlight.getBoardingPass().getFlightDetail()) {
                                iataCity = passFlight.getBoardingPass().getFlightDetail().getArrivalAirport();
                            }

                            //Reviewed  until the new mailing api is active
                            //Check if there is just one pax
                            if (cartPnr.getTravelerInfo().getCollection().size() == 1) {
                                boardingCollection.addItineraryPdf("ALL",
                                        boardingCollection.getItineraryPdf().get(cartPnr.getTravelerInfo()
                                                .getCollection().get(0).getTicketNumber()));
                            } else {
                                String legCodeString;
                                try {
                                    legCodeString = boardingCollection.getItineraryPdf().get(cartPnr.getTravelerInfo()
                                            .getCollection().get(0).getTicketNumber()).split("_")[5] + "_";
                                } catch (Exception e) {
                                    legCodeString = pnr.getBookedLegByLegCode(legCode).getSegments().getLegCodeRackspace();
                                }

                                String departureDate = pnr.getBookedLegByLegCode(legCode).getSegments().getCollection().get(0).getSegment()
                                        .getDepartureDateTime().substring(0, 10).replace("-", "_");

                                String link = BoardingPassesUtil.createDownloadLinkRackspace(
                                        lookupResponse.getReservation().getRecordLocator(),
                                        departureDate,
                                        legCodeString,
                                        "ALL"
                                );

                                boardingCollection.addItineraryPdf("ALL",
                                        link);
                            }
                            try {
                                String appleWallet = appleWalletUtilService.calculateAppleWallet(
                                        segmentStatus,
                                        passFlight.getBoardingPass().getBarCode(),
                                        boardingPass.getZone(),
                                        boardingPass.getFirstName(),
                                        boardingPass.getLastName(),
                                        boardingPass.getSeat().getSeatCode(),
                                        boardingPass.getSeat().getSectionCode(),
                                        boardingPass.isSkyPriority(),
                                        boardingPass.isTSAPre(),
                                        pos,
                                        pnr.getPnr(),
                                        boardingPass.getTicketNumber(),
                                        boardingPass.getFrequentFlyerAirline(),
                                        boardingPass.getFrequentFlyerNumber()
                                );

                                boardingPass.setAppleWalletPass(appleWallet);
                            } catch (Exception ex) {
                                LOG.error("Error trying to get applewallet string", ex);
                            }
                            boardingPassList.add(boardingPass);
                        }
                    }
                }
            }
            if(null != bTraveler && null != bTraveler.getInfant()){
                List<BoardingPass> boardingInfant = getInfantSabre(
                        passengerElement,
                        bTraveler.getInfant(),
                        bookedLegCollection,
                        pos,
                        pnr
                );
                boardingPassList.addAll(boardingInfant);
            }
        }

        if (boardingPassList.isEmpty()) {
            return null;
        } else {
            LOG.info("Setting final data for boarding collection");
            PectabBoardingPassList pectabBoardingPassList = new PectabBoardingPassList();
            PectabBoardingPass pectabBoarding = new PectabBoardingPass();
            pectabBoarding.setValue("");
            pectabBoarding.setVersion("");
            pectabBoardingPassList.add(pectabBoarding);
            boardingCollection.setPnr(lookupResponse.getReservation().getRecordLocator());
            boardingCollection.setPectabBoardingPassList(pectabBoardingPassList);
            boardingCollection.setDailyForecasts(BoardingPassesUtil.getWeatherForService("iataCity", pos, language));
            boardingCollection.setCollection(boardingPassList);
            boardingCollection.setPos(pos);
            return boardingCollection;
        }
    }

    /**
     * Method to get Infant information from Sabre. Additional changed adding
     * pos for apple wallet.
     *
     * @param infant              {@link com.aeromexico.sabre.api.models.Infant}
     * @param currentInfant       {@link Infant}
     * @param bookedLegCollection {@link BookedLegCollection}
     * @param pos                 String.
     * @return List<BoardingPass>
     */
    private List<BoardingPass> getInfantSabre(
            Passenger infant,
            Infant currentInfant,
            BookedLegCollection bookedLegCollection,
            String pos,
            PNR pnr
    ) {
        List<BoardingPass> boardingPassesInfant = new ArrayList<>();

        for (PassengerSegment passSegment : infant.getPassengerSegments().getPassengerSegment()) {
            for (PassengerFlight passFlight : passSegment.getPassengerFlight()) {
                if (passFlight.getCheckedIn()) {
                    BoardingPassWeb boardingPass = new BoardingPassWeb();
                    if (passFlight.getBoardingPass() != null) {
                        LOG.info("Fill information of infant passengers");
                        CodesGenerator generator = new CodesGenerator();

                        if (null != passFlight.getBoardingPass().getPersonName().getFirst()) {
                            boardingPass.setFirstName(passFlight.getBoardingPass().getPersonName().getFirst());
                        } else if (null != currentInfant) {
                            boardingPass.setFirstName(currentInfant.getFirstName());
                        }

                        if (null != passFlight.getBoardingPass().getPersonName().getLast()) {
                            boardingPass.setLastName(passFlight.getBoardingPass().getPersonName().getLast());
                        } else if (null != currentInfant) {
                            boardingPass.setLastName(currentInfant.getLastName());
                        }

                        boardingPass.setBarcodeImage(generator.codesGenerator2D(passFlight.getBoardingPass().getBarCode()));
                        boardingPass.setQrCodeImage(generator.codesGeneratorQR(passFlight.getBoardingPass().getBarCode()));
                        boardingPass.setControlCode(passFlight.getBoardingPass().getCheckInSequenceNumber());
                        boardingPass.setClassOfService(passFlight.getBoardingPass().getFareInfo().getBookingClass());
                        if (passFlight.getBoardingPass().getLoyaltyAccount() != null) {
                            boardingPass.setFrequentFlyerAirline(passFlight.getBoardingPass().getLoyaltyAccount().getMemberAirline());
                            boardingPass.setFrequentFlyerNumber(passFlight.getBoardingPass().getLoyaltyAccount().getMemberId());
                        }
                        boolean tsaValue = StringUtils.isNotBlank(passFlight.getBoardingPass().getDisplayData().getTsaPreCheckText());
                        boardingPass.setTSAPre(tsaValue);
                        boolean skyPriority = false;
                        if (null != passFlight.getBoardingPass().getSupplementaryData()
                                && null != passFlight.getBoardingPass().getSupplementaryData().getSkyPriority()) {
                            skyPriority = passFlight.getBoardingPass().getSupplementaryData().getSkyPriority();
                        }
                        boardingPass.setSkyPriority(skyPriority);
                        if (StringUtils.isNotBlank(passFlight.getBoardingPass().getZone())) {
                            boardingPass.setZone(passFlight.getBoardingPass().getZone());
                        } else {
                            boardingPass.setZone("-");
                        }
                        boardingPass.setTicketNumber(passFlight.getBoardingPass().getTicketNumber().getNumber());

                        SegmentStatus segmentStatus = BoardingUtil.extractSegmentStatusSabre(passFlight, bookedLegCollection);
                        boardingPass.setSegmentStatus(segmentStatus);
                        Seat seat = BoardingUtil.extractSeatSabre(passFlight);
                        boardingPass.setSeat(seat);

                        try {
                            String appleWallet = appleWalletUtilService.calculateAppleWallet(
                                    segmentStatus,
                                    passFlight.getBoardingPass().getBarCode(),
                                    boardingPass.getZone(),
                                    boardingPass.getFirstName(),
                                    boardingPass.getLastName(),
                                    boardingPass.getSeat().getSeatCode(),
                                    boardingPass.getSeat().getSectionCode(),
                                    boardingPass.isSkyPriority(),
                                    boardingPass.isTSAPre(),
                                    pos,
                                    pnr.getPnr(),
                                    boardingPass.getTicketNumber(),
                                    boardingPass.getFrequentFlyerAirline(),
                                    boardingPass.getFrequentFlyerNumber()
                            );

                            boardingPass.setAppleWalletPass(appleWallet);
                        } catch (Exception ex) {
                            LOG.error("Error trying to get applewallet string: " + ex.getMessage(), ex);
                        }
                    }
                    boardingPassesInfant.add(boardingPass);
                }
            }
        }
        return boardingPassesInfant;
    }

    private void updateBoardingPassesWithDigitalSignature(
            PNRCollection pnrCollection,
            BoardingPassCollection boardingPassCollection
    ) {
        for (PNR pnr : pnrCollection.getCollection()) {
            for (CartPNR cartPNR : pnr.getCarts().getCollection()) {
                for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
                    for (SegmentDocument segmentDocument : bookedTraveler.getSegmentDocumentsList()) {

                        BoardingPassWeb boardingPassWeb = boardingPassCollection.getBoardingPassByNamerefAndSegmentcode(
                                bookedTraveler.getNameRefNumber(),
                                segmentDocument.getSegmentCode()
                        );

                        if (null != boardingPassWeb) {
                            segmentDocument.setQrcodeImage(boardingPassWeb.getQrCodeImage());
                            segmentDocument.setBarcodeImage(boardingPassWeb.getBarcodeImage());
                        }
                    }
                }
            }
        }
    }

    private void validateReprintInformation(
            String pos, PNR pnr,
            CartPNR cartPnr,
            BookedTraveler bookedTraveler,
            WarningCollection warningCollection
    ) throws Exception {
        if (bookedTraveler == null) {
            LOG.error("Invalid booked Traveler in pnr {} the object is null", pnr);
            throw new GenericException(Response.Status.NOT_FOUND, ErrorType.TRAVELER_ID_NOT_MATCH);
        } else if (bookedTraveler.getSegmentDocumentsList() != null
                && !bookedTraveler.getSegmentDocumentsList().isEmpty()) {

            for (SegmentDocument segmentDocument : bookedTraveler.getSegmentDocumentsList()) {
                if (segmentDocument.getTravelDoc() == null || segmentDocument.getTravelDoc().trim().isEmpty()) {

                    getReprintInformationWeb(
                            pnr, pos, cartPnr, warningCollection
                    );
                }
            }

        } else {
            getReprintInformationWeb(
                    pnr, pos, cartPnr, warningCollection
            );
        }
    }

    /**
     * Service for boarding pass
     *
     * @param pnr
     * @param pos
     * @param cartPNR
     * @param warningCollection
     * @throws Exception
     */
    private void getReprintInformationWeb(
            PNR pnr,
            String pos,
            CartPNR cartPNR,
            WarningCollection warningCollection
    ) throws Exception {

        List<ACSReprintBPRSACS> reprintBPFinal = new ArrayList<>();
        String boardingPrinterCommand;
        boardingPrinterCommand = setUpConfigFactory.getInstance().getBoardingPassPrinterCommand();
        String boardingPrinter = setUpConfigFactory.getInstance().getBoardingLNIATA();

        List<ItineraryACS> itineraryList = getItineraryFromPNRWeb(pnr, cartPNR.getLegCode());
        for (ItineraryACS itinerary : itineraryList) {

            List<PassengerInfoACS> passengerInfoList = getListPassengerInfoWeb(cartPNR, itinerary.getOrigin());

            if (passengerInfoList.size() > 0) {
                List<ACSReprintBPRSACS> reprintBP = null;

                //if (true || AirlineCodeType.AM.getCode().equalsIgnoreCase(itinerary.getAirline())) {
                reprintBP = reprintService.reprintBP(
                        itinerary,
                        passengerInfoList,
                        PrintFormatACS.PECTAB,
                        boardingPrinter,
                        boardingPrinterCommand
                );
                    /*
                } else {
                    SabreSession sabreSession = null;
                    try {

                        sabreSession = aeService.getSession();

                        ContextChangeRQ contextChangeRQ = ChangeContexService.createContextChangeOverSignFirstStepRQ(SystemVariablesUtil.getEarlyCheckinUser());

                        ContextChangeRS contextChangeRS = changeContexService.changeContext(sabreSession, contextChangeRQ);

                        ChangeContexService.validateContextChangeResponse(contextChangeRS);

                        contextChangeRQ = ChangeContexService.createContextChangeOverSignSecondStepRQ(Constants.DUTY_CODE, SystemVariablesUtil.getEarlyCheckinUser(), SystemVariablesUtil.getEarlyCheckinPassword());

                        contextChangeRS = changeContexService.changeContext(sabreSession, contextChangeRQ);

                        ChangeContexService.validateContextChangeResponse(contextChangeRS);

                        reprintBP = reprintService.reprintBP(
                                sabreSession,
                                itinerary,
                                passengerInfoList,
                                PrintFormatACS.PECTAB,
                                boardingPrinter,
                                boardingPrinterCommand,
                                false
                        );

                        if (null != sabreSession) {
                            aeService.closeSession(sabreSession);
                        }
                    } catch (Exception ex) {
                        LOG.error(ex.getMessage(), ex);

                        if (null != sabreSession) {
                            aeService.closeSession(sabreSession);
                        }
                    }
                }
                */

                if (null != reprintBP) {
                    for (ACSReprintBPRSACS reprintOne : reprintBP) {
                        if (null != reprintOne.getPECTABDataList()) {
                            reprintBPFinal.add(reprintOne);
                        } else {
                            LOG.error("The segment {} is not valid for get boarding pass {}",
                                    itinerary.getOrigin() + "_" + itinerary.getDestination(),
                                    reprintOne.getResult().getStatus().toString());
                        }
                    }
                } else {
                    LOG.error("The segment {} is not valid for get boarding pass",
                            itinerary.getOrigin() + "_" + itinerary.getDestination());
                }
            }
        }

        if (reprintBPFinal.size() > 0) {
            // Add validation for errors on reprint service
            BookedLeg bookedLeg = pnr.getBookedLegByLegCode(cartPNR.getLegCode());
            updateCheckin(
                    pos, pnr.getPnr(),
                    cartPNR, bookedLeg,
                    reprintBPFinal,
                    warningCollection,
                    pnr.isTsaRequired()
            );
        }
    }

    /**
     * Get Itinerary from pnr collection
     *
     * @param pnr
     * @param legCode
     * @return
     * @throws Exception
     */
    private List<ItineraryACS> getItineraryFromPNRWeb(PNR pnr, String legCode) throws Exception {

        LOG.info("Getting the itinerary object from pnr: {}", pnr.getPnr());

        BookedLeg legByLegCode;
        List<BookedLeg> legsByLegCodes = new ArrayList<>();
        List<ItineraryACS> itineraryList = new ArrayList<>();

        if (null != pnr.getLegs().getCollection() && !pnr.getLegs().getCollection().isEmpty()) {

            legByLegCode = pnr.getBookedLegByLegCode(legCode);
            if (null != legByLegCode) {
                legsByLegCodes.add(legByLegCode);
            }

            BoardingPassesUtil.createItineraryFromBookedLeg(legsByLegCodes, itineraryList);
        }

        return itineraryList;
    }

    /**
     * Get passenger info from cart pnr
     *
     * @param cartPNR
     * @param segOriginal
     * @return
     */
    private List<PassengerInfoACS> getListPassengerInfoWeb(CartPNR cartPNR, String segOriginal) {

        List<PassengerInfoACS> passengerInfoList = new ArrayList<>();

        for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {

            if (BoardingPassesUtil.needBoardingPassInfo(bookedTraveler)) {
                PassengerInfoACS passenger = new PassengerInfoACS();
                passenger.setLastName(bookedTraveler.getLastName());
                for (BookingClass booked : bookedTraveler.getBookingClasses().getCollection()) {
                    if (booked.getSegmentCode().substring(0, 3).equalsIgnoreCase(segOriginal)) {
                        passenger.setPassengerID(booked.getPassengerId());
                    }
                }

                if (null == passenger.getPassengerID() || passenger.getPassengerID().trim().isEmpty()) {
                    passenger.setPassengerID(bookedTraveler.getId());
                }
                if (null == passenger.getPassengerID() || passenger.getPassengerID().trim().isEmpty()) {
                    if(null!= bookedTraveler.getPnrPassId()){
                        passenger.setPassengerID(bookedTraveler.getPnrPassId().substring(15,27));
                    }
                }
                passengerInfoList.add(passenger);
                // }
            }
        }
        LOG.info("getListPassengerInfo for segment: {} {}", segOriginal, passengerInfoList.size());
        return passengerInfoList;
    }

    /**
     * @param pos
     * @param recordLocator
     * @param cartPNR
     * @param bookedLeg
     * @param reprintBP
     * @param warningCollection
     * @param tsaRequired
     * @throws Exception
     */
    private void updateCheckin(
            String pos,
            String recordLocator,
            CartPNR cartPNR,
            BookedLeg bookedLeg,
            List<ACSReprintBPRSACS> reprintBP,
            WarningCollection warningCollection,
            boolean tsaRequired
    ) throws Exception {

        LOG.info("Updating the boarding pass information in cart collection");

        List<ACSCheckInPassengerRSACS> checkInPassengerRSACSList = new ArrayList();

        for (ACSReprintBPRSACS reprintTemp : reprintBP) {

            ACSCheckInPassengerRSACS ascTempData = new ACSCheckInPassengerRSACS();
            ascTempData.setFreeTextInfoList(reprintTemp.getFreeTextInfoList());
            ascTempData.setItineraryPassengerList(reprintTemp.getItineraryPassengerList());
            ascTempData.setPECTABDataList(reprintTemp.getPECTABDataList());

            checkInPassengerRSACSList.add(ascTempData);
        }

        if (PurchaseOrderUtil.searchInBoardingPasses("NOT VALID WITHOUT FLIGHT COUPON", checkInPassengerRSACSList)) {
            if (PurchaseOrderUtil.searchInAllBoardingPasses("NOT VALID WITHOUT FLIGHT COUPON", checkInPassengerRSACSList, warningCollection)) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.NOT_VALID_WITHOUT_FLIGHT_COUPON, PosType.getType(pos));
            }
        }

        List<ACSCheckInPassengerRSACS> acsCheckInPassengerRSACSList = new ArrayList<>();
        List<PectabBoardingPass> pectabDataList = new ArrayList<>();

        for (ACSReprintBPRSACS reprintTemp : reprintBP) {

            ACSCheckInPassengerRSACS ascTempData = new ACSCheckInPassengerRSACS();
            ascTempData.setFreeTextInfoList(reprintTemp.getFreeTextInfoList());
            ascTempData.setItineraryPassengerList(reprintTemp.getItineraryPassengerList());
            ascTempData.setPECTABDataList(reprintTemp.getPECTABDataList());

            if (null != reprintTemp.getPECTABDataList()) {
                if (null != reprintTemp.getPECTABDataList().getPECTABData()
                        && !reprintTemp.getPECTABDataList().getPECTABData().isEmpty()) {
                    for (PECTABDataListACS.PECTABData pectabData : reprintTemp.getPECTABDataList().getPECTABData()) {
                        PectabBoardingPass pectabDataLocal = new PectabBoardingPass();
                        pectabDataLocal.setValue(pectabData.getValue());
                        pectabDataLocal.setVersion(pectabData.getVersion());
                        pectabDataList.add(pectabDataLocal);
                    }
                    ascTempData.setResult(reprintTemp.getResult());
                    acsCheckInPassengerRSACSList.add(ascTempData);
                }
            }
        }
        cartPNR.getPectabBoardingPassList().addAll(pectabDataList);

        List<BookedTraveler> bookedTravelersEligibleForCheckin = cartPNR.getTravelerInfo().getCollection();
        Set<BookedTraveler> bookedTravelElegibles = new HashSet<>(bookedTravelersEligibleForCheckin);

        PnrCollectionUtil.updateAfterSuccessCheckin(
                bookedTravelElegibles,
                acsCheckInPassengerRSACSList,
                cartPNR.getTravelerInfo().getCollection(),
                bookedLeg,
                pos,
                recordLocator,
                tsaRequired,
                warningCollection
        );

    }

    private boolean validatePassengerIds(BoardingPassesRQ boardingPassesRQ, CartPNR cartPNR) throws Exception {

        if (boardingPassesRQ.getPaxId() == null || boardingPassesRQ.getPaxId().isEmpty()) {
            LOG.error("Passenger list is empty, please verify that is a valid list");
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PAX_LIST_REQUIRED_FOR_KIOSK);
        }

        for (String passengerId : boardingPassesRQ.getPaxId()) {

            BookedTraveler bookedTraveler = cartPNR.getBookedTravelerById(passengerId);

            if (bookedTraveler == null) {
                LOG.error("Invalid pax ID: {}", passengerId);
                throw new GenericException(Response.Status.NOT_FOUND, ErrorType.TRAVELER_ID_NOT_MATCH);
            } else if (bookedTraveler.getSegmentDocumentsList() != null
                    && !bookedTraveler.getSegmentDocumentsList().isEmpty()) {

                for (SegmentDocument segmentDocument : bookedTraveler.getSegmentDocumentsList()) {
                    if (segmentDocument.getTravelDoc() == null || StringUtils.isBlank(segmentDocument.getTravelDoc())) {
                        LOG.info("validatePassengerIds returning FALSE for PAX {}", passengerId);
                        return false;
                    }
                }

            } else {
                return false;
            }
        }

        return true;
    }

    /**
     * Service for boarding pass
     *
     * @param pnr
     * @param boardingPassesRQ
     * @param cartPNR
     * @param warningCollection
     * @throws Exception
     */
    private void getReprintInformation(
            PNR pnr,
            BoardingPassesRQ boardingPassesRQ,
            CartPNR cartPNR,
            WarningCollection warningCollection
    ) throws Exception {

        LOG.info("Finding information of the itinerary and the passengers");

        List<ACSReprintBPRSACS> reprintBPFinal = new ArrayList<>();
        String boardingPrinterCommand = setUpConfigFactory.getInstance().getBoardingPassPrinterCommand();
        String boardingPrinter = setUpConfigFactory.getInstance().getBoardingLNIATA();

        List<ItineraryACS> itineraryList = getItineraryFromPNR(pnr, boardingPassesRQ.getLegCode());
        for (ItineraryACS itinerary : itineraryList) {

            List<PassengerInfoACS> passengerInfoList = BoardingPassesUtil.getListPassengerInfo(
                    cartPNR, boardingPassesRQ,
                    itinerary.getOrigin()
            );

            if (passengerInfoList.size() > 0) {
                List<ACSReprintBPRSACS> reprintBP = reprintService.reprintBP(itinerary, passengerInfoList,
                        PrintFormatACS.PECTAB, boardingPrinter, boardingPrinterCommand);
                for (ACSReprintBPRSACS reprintOne : reprintBP) {
                    if (null != reprintOne.getPECTABDataList()) {
                        reprintBPFinal.add(reprintOne);
                    } else {
                        LOG.info("The segment {} is not valid for get boarding pass {}",
                                itinerary.getOrigin() + "_" + itinerary.getDestination(),
                                reprintOne.getResult().getStatus().toString());
                    }
                }
            }
        }

        if (reprintBPFinal.size() > 0) {
            // Add validation for errors on reprint service
            BookedLeg bookedLeg = pnr.getBookedLegByLegCode(cartPNR.getLegCode());
            updateCheckin(
                    boardingPassesRQ.getPos(),
                    boardingPassesRQ.getRecordLocator(),
                    cartPNR,
                    bookedLeg,
                    reprintBPFinal,
                    warningCollection,
                    pnr.isTsaRequired()
            );
        }
    }

    /**
     * Get Itinerary from pnr collection
     *
     * @param pnr
     * @param legCode
     * @return
     * @throws Exception
     */
    private List<ItineraryACS> getItineraryFromPNR(PNR pnr, String legCode) throws Exception {

        LOG.info("Getting the itinerary object from pnr: {}", pnr.getPnr());

        BookedLeg legByLegCode;
        List<BookedLeg> legsByLegCodes = new ArrayList<>();
        List<ItineraryACS> itineraryList = new ArrayList<>();

        if (null != pnr.getLegs().getCollection() && !pnr.getLegs().getCollection().isEmpty()) {

            if (StringUtils.isNotBlank(legCode)) {
                legByLegCode = pnr.getBookedLegByLegCode(legCode);
                legsByLegCodes.add(legByLegCode);
            } else {
                for (CartPNR cartLeg : pnr.getCarts().getCollection()) {
                    legCode = cartLeg.getLegCode();
                    legByLegCode = pnr.getBookedLegByLegCode(legCode);
                    if (null != legByLegCode) {
                        legsByLegCodes.add(legByLegCode);
                    }
                }
            }

            BoardingPassesUtil.createItineraryFromBookedLeg(legsByLegCodes, itineraryList);
        }

        return itineraryList;
    }

    /**
     * This method allow to get information from selected boarding passes
     *
     * @param boardingPassesRQ
     * @param bookedLeg
     * @param cartPNR
     * @param pnr
     * @return
     */
    protected BoardingPassCollection getKioskBoardingSelected(
            BoardingPassesRQ boardingPassesRQ,
            BookedLeg bookedLeg,
            CartPNR cartPNR,
            PNR pnr
    ) {

        BoardingPassCollection boardingPassCollection = new BoardingPassCollection();

        LOG.info("Getting information for each passenger in the request: {}", boardingPassesRQ.getPaxId());

        for (String passengerId : boardingPassesRQ.getPaxId()) {

            BookedTraveler bookedTraveler;
            try {
                bookedTraveler = cartPNR.getBookedTravelerById(passengerId);
            } catch (Exception e) {
                LOG.error("Error to find the booked traveler does not match id: {}", passengerId);
                throw new GenericException(Response.Status.NOT_FOUND, ErrorType.TRAVELER_ID_NOT_MATCH);
            }

            try {
                List<BoardingPassKiosk> boardingPassFill = fillDataBoardingPassForKiosks(
                        bookedTraveler, boardingPassesRQ.getPos(), bookedLeg, pnr
                );

                if (null != boardingPassFill) {
                    //Reviewed  until the new mailing api is active
                    String legCode = bookedLeg.getSegments().getLegCodeRackspace();

                    /*
                    try {
                        legCode = BoardingPassesUtil.getLegCodeFromDocumentList(bookedTraveler);
                    } catch (Exception e) {
                        legCode = bookedLeg.getSegments().getLegCodeRackspace();
                    }
                    */

                    String departureDate = bookedLeg.getSegments().getCollection().get(0).getSegment()
                            .getDepartureDateTime().substring(0, 10).replace("-", "_");

                    String link = BoardingPassesUtil.createDownloadLinkRackspace(
                            boardingPassesRQ.getRecordLocator(),
                            departureDate,
                            legCode,
                            bookedTraveler.getFirstName().substring(0, 1) + "_" + bookedTraveler.getLastName() + "_" + bookedTraveler.getTicketNumber()
                    );
                    boardingPassCollection.addItineraryPdf(
                            bookedTraveler.getTicketNumber(),
                            link
                    );
                    boardingPassCollection.getCollection().addAll(boardingPassFill);
                }
            } catch (MongoDisconnectException me) {
                if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(boardingPassesRQ.getPos())) {
                    throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getCode(),
                            prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getActionable(),
                            prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getMessage(),
                            Response.Status.INTERNAL_SERVER_ERROR);
                } else {
                    throw me;
                }
            } catch (Exception ex) {
                LOG.error("Error with pnr collection information read from database: {}", cartPNR.toString());
                throw new GenericException(Response.Status.NOT_FOUND, ErrorType.ERROR_INFORMATION_ON_COLLECTION);
            }
        }

        LOG.info("Return {} boarding passes in the collection", boardingPassCollection.getCollection().size());

        if (boardingPassCollection.getCollection() == null || boardingPassCollection.getCollection().isEmpty()) {
            LOG.error("Error to generate boarding passes collection empty");
            if(PosType.WEB.equals(boardingPassesRQ.getPos())){
                throw new GenericException(Response.Status.NOT_FOUND, ErrorType.BOARDING_PASSES_NOT_GENERATED_WEB);
            }else{
                throw new GenericException(Response.Status.NOT_FOUND, ErrorType.BOARDING_PASSES_NOT_GENERATED);
            }
        }

        PectabBoardingPassList smallPectabList = new PectabBoardingPassList();

        if (null != cartPNR.getPectabBoardingPassList() && !cartPNR.getPectabBoardingPassList().isEmpty()) {
            PectabBoardingPass pectabBoardingPass = cartPNR.getPectabBoardingPassList().get(0);
            smallPectabList.add(pectabBoardingPass);
        }

        boardingPassCollection.setPectabBoardingPassList(smallPectabList);
        boardingPassCollection.setPnr(boardingPassesRQ.getRecordLocator());

        String iataCity = bookedLeg.getSegments().getCollection().get(0).getSegment().getArrivalAirport();
        DailyForecasts wheatherCityDest = BoardingPassesUtil.getWeatherForService(
                iataCity, boardingPassesRQ.getPos(),
                boardingPassesRQ.getLanguage()
        );

        boardingPassCollection.setDailyForecasts(wheatherCityDest);
        boardingPassCollection.setPos(boardingPassesRQ.getPos());

        //Reviewed  until the new mailing api is active
        //If there is just one pax the link for all is the same
        if (cartPNR.getTravelerInfo().getCollection().size() == 1) {
            boardingPassCollection.addItineraryPdf(
                    "ALL",
                    boardingPassCollection.getItineraryPdf().get(
                            cartPNR.getTravelerInfo()
                                    .getCollection()
                                    .get(0)
                                    .getTicketNumber()
                    )
            );
        } else {
            String legCode = bookedLeg.getSegments().getLegCodeRackspace();

            /*
            try {
                legCode = boardingPassCollection
                        .getItineraryPdf()
                        .get(
                                cartPNR.getTravelerInfo()
                                        .getCollection()
                                        .get(0)
                                        .getTicketNumber()
                        ).split("_")[5] + "_";
            } catch (Exception e) {
                legCode = bookedLeg.getSegments().getLegCodeRackspace();
            }
            */

            String departureDate = bookedLeg.getSegments().getCollection().get(0).getSegment()
                    .getDepartureDateTime().substring(0, 10).replace("-", "_");

            String link = BoardingPassesUtil.createDownloadLinkRackspace(
                    boardingPassesRQ.getRecordLocator(),
                    departureDate,
                    legCode,
                    "ALL"
            );
            boardingPassCollection.addItineraryPdf(
                    "ALL",
                    link
            );
        }

        return boardingPassCollection;
    }

    /**
     * This method allow fill the information in the boarding pass object
     *
     * @param bookedTraveler
     * @param pos
     * @param bookedLeg
     * @param pnr
     * @return
     * @throws Exception
     */
    protected List<BoardingPassKiosk> fillDataBoardingPassForKiosks(
            BookedTraveler bookedTraveler,
            String pos,
            BookedLeg bookedLeg,
            PNR pnr
    ) throws Exception {

        LOG.info("Fill information with booked traveler: {}", bookedTraveler.toString());

        List<BoardingPassKiosk> boardingList = new ArrayList<>();

        BoardingPassKiosk boardingPassKiosk;

        for (SegmentDocument segmentDocument : bookedTraveler.getSegmentDocumentsList()) {

            String segmentCode = segmentDocument.getSegmentCode();

            boardingPassKiosk = new BoardingPassKiosk();
            boardingPassKiosk.setBarcode(segmentDocument.getBarcode());
            boardingPassKiosk.setPectabFormat(segmentDocument.getTravelDoc());
            boardingPassKiosk.setNameRefNumber(bookedTraveler.getNameRefNumber());

            boardingPassKiosk.setControlCode(segmentDocument.getCheckInNumber());
            boardingPassKiosk.setZone(segmentDocument.getBoardingZone());

            if (null != bookedTraveler.getBookingClasses().getCollection()
                    && !bookedTraveler.getBookingClasses().getCollection().isEmpty()) {
                boardingPassKiosk.setClassOfService(
                        bookedTraveler.getBookingClasses().getBookingClass(segmentCode).getBookingClass()
                );
            }

            boardingPassKiosk.setTicketNumber(bookedTraveler.getTicketNumber());
            boardingPassKiosk.setPassengerId(bookedTraveler.getId());
            boardingPassKiosk.setFirstName(bookedTraveler.getFirstName());
            boardingPassKiosk.setLastName(bookedTraveler.getLastName());
            boardingPassKiosk.setSelectee(bookedTraveler.isSelectee());

            List<AbstractSegmentChoice> collectionSegment = bookedTraveler.getSegmentChoices().getCollection();

            Seat seat = BoardingPassesUtil.getSeatFromCart(collectionSegment, segmentCode);

            if (BoardingPassesUtil.isFirstClassCabin(bookedTraveler.getBookingClasses().getCollection().get(0).getBookingCabin())) {
                seat.setSectionCode(SeatSectionCodeType.FIRST_CLASS);
            } else {
                seat.setSectionCode(SeatSectionCodeType.COACH);
            }

            boardingPassKiosk.setSeat(seat);
            SegmentStatus segmentStatus = null;

            try {
                segmentStatus = getSegmetStatus(bookedLeg, segmentCode);
            } catch (Exception e) {
                LOG.error(e.getMessage(), e);
                LOG.error("Cannot get segment {} for the boarding passes", segmentCode, e);
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.CANNOT_GET_SEGMENT_STATUS_INFORMATION);
            }

            boardingPassKiosk.setSegmentStatus(segmentStatus);

            if (PosType.WEB.toString().equalsIgnoreCase(pos) || PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(pos)) {

                LOG.info("Adding all parameters for url");

                String appleWallet = appleWalletUtilService.calculateAppleWallet(
                        segmentStatus,
                        segmentDocument.getBarcode(),
                        segmentDocument.getBoardingZone(),
                        bookedTraveler.getFirstName(),
                        bookedTraveler.getLastName(),
                        seat.getSeatCode(),
                        seat.getSectionCode(),
                        segmentDocument.isSkyPriority(),
                        segmentDocument.isTsaPreCheck(),
                        pos,
                        pnr.getPnr(),
                        bookedTraveler.getTicketNumber(),
                        bookedTraveler.getFrequentFlyerProgram(),
                        bookedTraveler.getFrequentFlyerNumber()
                );

                boardingPassKiosk.setAppleWalletPass(appleWallet);
            }

            boardingList.add(boardingPassKiosk);

            if (null != bookedTraveler.getInfant()) {
                try {
                    BoardingPassKiosk boardingInfant = fillDataInfantForKiosks(bookedTraveler, segmentCode);
                    if (null != boardingInfant) {
                        boardingInfant.setSegmentStatus(segmentStatus);
                        boardingList.add(boardingInfant);
                    }
                } catch (Exception e) {
                    LOG.error("Cannot get the information for infant: {}", bookedTraveler.getInfant());
                    throw new GenericException(Response.Status.NOT_FOUND, ErrorType.CANNOT_GET_INFANT_INFORMATION);
                }
            }
        }

        return boardingList;
    }

    /**
     * Get segment status from Leg Collection
     *
     * @param legBooked
     * @param segment
     * @return
     */
    protected SegmentStatus getSegmetStatus(BookedLeg legBooked, String segment) {
        LOG.info("Adding segment information in the segment: {}", segment);

        BookedSegment bookedSegment = legBooked.getSegments().getBookedSegment(segment);

        if (null == bookedSegment) {
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.CANNOT_GET_SEGMENT_STATUS_INFORMATION);
        }

        SegmentStatus segmentStatus = new SegmentStatus();
        String defaultBoardingGate = "TBD";

        if (null != bookedSegment.getSegment().getBoardingGate()
                && !StringUtils.isBlank(bookedSegment.getSegment().getBoardingGate())) {

            String boardingGate = bookedSegment.getSegment().getBoardingGate();

            if (!"Not asigned".equalsIgnoreCase(boardingGate)
                    && !"GATE".equalsIgnoreCase(boardingGate)) {
                segmentStatus.setBoardingGate(boardingGate);
            } else {
                segmentStatus.setBoardingGate(defaultBoardingGate);
            }
        } else {
            segmentStatus.setBoardingGate(defaultBoardingGate);
        }

        segmentStatus.setBoardingTerminal(bookedSegment.getSegment().getBoardingTerminal());
        String boardingTimeFinal = null;
        if (StringUtils.isNotBlank(legBooked.getBoardingTime())) {
            boardingTimeFinal = BoardingPassesUtil.getTwentyFour(legBooked.getBoardingTime());
        }
        segmentStatus.setBoardingTime(boardingTimeFinal);
        segmentStatus.setEstimatedArrivalTime(bookedSegment.getSegment().getArrivalDateTime());
        segmentStatus.setEstimatedDepartureTime(legBooked.getEstimatedDepartureTime());
        segmentStatus.setSegment(bookedSegment.getSegment());
        segmentStatus.setStatus(legBooked.getFlightStatus().toString());
        segmentStatus.setArrivalGate("");
        segmentStatus.setArrivalTerminal("");
        return segmentStatus;
    }

    /**
     * This method allow to fill infant information from each booked traveler
     *
     * @param bookedTraveler
     * @param segmentCode
     * @return
     * @throws Exception
     */
    private BoardingPassKiosk fillDataInfantForKiosks(BookedTraveler bookedTraveler, String segmentCode) {
        LOG.info("Fill information of infant with booked traveler: {}", bookedTraveler);

        BoardingPassKiosk boardingPassKiosk;
        boardingPassKiosk = new BoardingPassKiosk();
        boardingPassKiosk.setBarcode(bookedTraveler.getInfant().getSegmentDocumentsList().getBySegmentCode(segmentCode).getBarcode());
        boardingPassKiosk.setPectabFormat(bookedTraveler.getInfant().getSegmentDocumentsList().getBySegmentCode(segmentCode).getTravelDoc());

        boardingPassKiosk.setClassOfService(bookedTraveler.getBookingClasses().getBookingClass(segmentCode).getBookingClass());
        boardingPassKiosk.setTicketNumber(bookedTraveler.getInfant().getTicketNumber());
        boardingPassKiosk.setFirstName(bookedTraveler.getInfant().getFirstName());
        boardingPassKiosk.setLastName(bookedTraveler.getInfant().getLastName());
        boardingPassKiosk.setSelectee(bookedTraveler.isSelectee());
        boardingPassKiosk.setControlCode(bookedTraveler.getSegmentDocumentsList().getBySegmentCode(segmentCode).getCheckInNumber());
        boardingPassKiosk.setZone(bookedTraveler.getSegmentDocumentsList().getBySegmentCode(segmentCode).getBoardingZone());

        LOG.info("Get assigned seat and validating information for segment: {}", segmentCode);
        List<AbstractSegmentChoice> collectionSegment = bookedTraveler.getSegmentChoices().getCollection();
        Seat seat = BoardingPassesUtil.getSeatFromCart(collectionSegment, segmentCode);
        if (BoardingPassesUtil.isFirstClassCabin(bookedTraveler.getBookingClasses().getCollection().get(0).getBookingCabin())) {
            seat.setSectionCode(SeatSectionCodeType.FIRST_CLASS);
        } else {
            seat.setSectionCode(SeatSectionCodeType.COACH);
        }
        seat.setSeatCode("INF");
        boardingPassKiosk.setSeat(seat);
        return boardingPassKiosk;
    }

    public MobileBoardingPassService getMobileBoardingPassService() {
        return mobileBoardingPassService;
    }

    public void setMobileBoardingPassService(MobileBoardingPassService mobileBoardingPassService) {
        this.mobileBoardingPassService = mobileBoardingPassService;
    }

    public PnrCollectionService getUpdatePnrCollectionService() {
        return updatePnrCollectionService;
    }

    public void setUpdatePnrCollectionService(PnrCollectionService updatePnrCollectionService) {
        this.updatePnrCollectionService = updatePnrCollectionService;
    }

    public DigitalSignatureService getDigitalSignatureService() {
        return digitalSignatureService;
    }

    public void setDigitalSignatureService(DigitalSignatureService digitalSignatureService) {
        this.digitalSignatureService = digitalSignatureService;
    }

    public PassengersListService getVolunteeredListService() {
        return volunteeredListService;
    }

    public void setVolunteeredListService(PassengersListService volunteeredListService) {
        this.volunteeredListService = volunteeredListService;
    }

    public IOverBookingConfigDao getOverBookingConfigDao() {
        return overBookingConfigDao;
    }

    public void setOverBookingConfigDao(IOverBookingConfigDao overBookingConfigDao) {
        this.overBookingConfigDao = overBookingConfigDao;
    }

    public BoardingPassesDispatcher getMailDispatcher() {
        return mailDispatcher;
    }

    public void setMailDispatcher(BoardingPassesDispatcher mailDispatcher) {
        this.mailDispatcher = mailDispatcher;
    }

    public AMReprintBPService getReprintService() {
        return reprintService;
    }

    public void setReprintService(AMReprintBPService reprintService) {
        this.reprintService = reprintService;
    }

    public IPNRLookupDao getPnrLookupDao() {
        return pnrLookupDao;
    }

    public void setPnrLookupDao(IPNRLookupDao pnrLookupDao) {
        this.pnrLookupDao = pnrLookupDao;
    }

    /**
     * @return the setUpConfigFactory
     */
    public SetUpConfigFactory getSetUpConfigFactory() {
        if (null == setUpConfigFactory) {
            setUpConfigFactory = new CheckInSetUpConfigFactoryWrapper();
        }
        return setUpConfigFactory;
    }

    /**
     * @param setUpConfigFactory the setUpConfigFactory to set
     */
    public void setSetUpConfigFactory(SetUpConfigFactory setUpConfigFactory) {
        this.setUpConfigFactory = setUpConfigFactory;
    }

}
