package com.am.checkin.web.v2.util;

import com.aeromexico.commons.model.AbstractSeatMap;
import com.aeromexico.commons.model.SeatMap;
import com.aeromexico.commons.model.SeatmapCollection;
import com.aeromexico.commons.web.types.SeatmapStatusType;

import java.util.ArrayList;
import java.util.List;

public class SeatmapCollectionUtil {

    public static SeatmapCollection getSeatmapCollectionEmpty() {
        SeatmapCollection seatmapCollection;
        seatmapCollection = new SeatmapCollection();
        List<AbstractSeatMap> abstractSeatMapList = new ArrayList<>();
        SeatMap seatMap = new SeatMap();
        seatMap.setStatus(SeatmapStatusType.UNAVAILABLE);
        abstractSeatMapList.add(seatMap);
        seatmapCollection.setCollection(abstractSeatMapList);
        return seatmapCollection;
    }
}
