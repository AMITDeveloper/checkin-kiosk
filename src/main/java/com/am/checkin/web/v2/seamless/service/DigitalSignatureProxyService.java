package com.am.checkin.web.v2.seamless.service;


import com.aeromexico.commons.prototypes.JSONPrototype;
import com.am.checkin.web.v2.model.DigitalSignatureRq;
import com.am.checkin.web.v2.model.DigitalSignatureRqList;
import com.am.checkin.web.v2.model.DigitalSignatureRs;
import com.am.checkin.web.v2.model.DigitalSignatureRsList;
import com.am.checkin.web.v2.seamless.client.DigitalSignatureRestClient;
import com.am.checkin.web.v2.util.SystemVariablesUtil;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

@Named
@ApplicationScoped
public class DigitalSignatureProxyService {
    private static final Logger LOG = LoggerFactory.getLogger(DigitalSignatureProxyService.class);

    @Inject
    private DigitalSignatureRestClient digitalSignatureRestClient;

    private DigitalSignatureServicesInterface proxy = null;

    private DigitalSignatureServicesInterface getProxy() {
        if (null == proxy) {
            ResteasyWebTarget target = digitalSignatureRestClient.getRestEasyClient().target(UriBuilder.fromPath(SystemVariablesUtil.getDigitalSignatureUrl()));
            proxy = target.proxy(DigitalSignatureServicesInterface.class);
        }
        return proxy;
    }

    /**
     * @param digitalSignatureRq
     * @return
     * @throws Exception
     */
    public DigitalSignatureRs signBoardingpass(DigitalSignatureRq digitalSignatureRq) throws Exception {

        Response response = getProxy().signBoardingpass(digitalSignatureRq);

        LOG.info("DIGITAL_STATUS: {}", response.getStatus());
        LOG.info("DIGITAL_STATUS: {}", response.getStatusInfo());

        if (response.getStatus() >= 200 && response.getStatus() < 300) {
            String json = response.readEntity(String.class);
            LOG.info("DIGITAL_RESPONSE: {}", response);
            LOG.info("DIGITAL_JSON: {}", json);

            DigitalSignatureRs digitalSignatureRs = getResponse(json);

            return digitalSignatureRs;
        } else {
            LOG.error("DIGITAL_EXCEPTION_EMPTY");
            throw new Exception("");
        }
    }

    /**
     * @param digitalSignatureRqList
     * @return
     * @throws Exception
     */
    public DigitalSignatureRsList signBoardingpasses(DigitalSignatureRqList digitalSignatureRqList) throws Exception {

        Response response = getProxy().signBoardingpasses(digitalSignatureRqList);

        LOG.info("DIGITAL_STATUS: {}", response.getStatus());
        LOG.info("DIGITAL_STATUS: {}", response.getStatusInfo());

        if (response.getStatus() >= 200 && response.getStatus() < 300) {
            String json = response.readEntity(String.class);
            LOG.info("DIGITAL_RESPONSE: {}", response);
            LOG.info("DIGITAL_JSON: {}", json);

            DigitalSignatureRsList digitalSignatureRsList = getResponseList(json);

            return digitalSignatureRsList;
        } else {
            LOG.error("DIGITAL_EXCEPTION_EMPTY");
            throw new Exception("");
        }
    }


    public static DigitalSignatureRs getResponse(String jsonResponse) {
        return JSONPrototype.convert(jsonResponse, DigitalSignatureRs.class);
    }

    public static DigitalSignatureRsList getResponseList(String jsonResponse) {
        return JSONPrototype.convert(jsonResponse, DigitalSignatureRsList.class);
    }
}
