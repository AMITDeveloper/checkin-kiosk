package com.am.checkin.web.v2.services;

import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.CheckInStatus;
import com.aeromexico.commons.model.PriorityBySegment;
import com.aeromexico.commons.web.types.PosType;
import com.aeromexico.commons.web.util.FilterPassengersUtil;
import com.aeromexico.commons.web.util.FlightNumberUtil;
import com.aeromexico.sabre.api.acs.AMAddToPriorityListService;
import com.am.checkin.web.service.CheckInSetUpConfigFactoryWrapper;
import com.am.checkin.web.v2.model.PassengerListUpdate;
import com.am.checkin.web.v2.model.UpgradeKnowledge;
import com.am.checkin.web.v2.util.CheckinUtil;
import com.sabre.services.acs.bso.addtoprioritylist.v3.ACSAddToPriorityListRQACS;
import com.sabre.services.acs.bso.addtoprioritylist.v3.ACSAddToPriorityListRSACS;
import com.sabre.services.acs.bso.addtoprioritylist.v3.ItineraryACS;
import com.sabre.services.acs.bso.addtoprioritylist.v3.PassengerInfoACS;
import com.sabre.services.acs.bso.addtoprioritylist.v3.PassengerInfoListACS;
import com.sabre.services.acs.bso.addtoprioritylist.v3.PriorityClassificationInfoACS;
import com.sabre.services.stl.v3.ErrorOrSuccessCode;
import com.sabre.services.stl.v3.FreeTextInfoACS;
import com.sabre.services.stl.v3.ItineraryPassengerACS;
import com.sabre.services.stl.v3.PassengerDetailACS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;
import java.util.Map;

/**
 * @author adrianleal
 */
@Named
@ApplicationScoped
public class AddPaxToPriorityListService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AddPaxToPriorityListService.class);

    @Inject
    private AMAddToPriorityListService addToPriorityListService;

    @Inject
    private UpgradeEligibilityRuleService upgradeEligibilityRuleService;

    @Inject
    @Named("CheckInSetUpConfigFactoryWrapper")
    private CheckInSetUpConfigFactoryWrapper setUpConfigFactory;

    @PostConstruct
    public void init() {
        if (null == getSetUpConfigFactory()) {
            setSetUpConfigFactory(new CheckInSetUpConfigFactoryWrapper());
        }
    }

    /**
     *
     * @param passengerListUpdate
     * @return
     * @throws Exception
     *
     * Used to add specific passengers to the priorityList
     */
    public String addThis(PassengerListUpdate passengerListUpdate) throws Exception {

        try {
            if (null == passengerListUpdate.getItinerary()
                    || null == passengerListUpdate.getPassengers()
                    || passengerListUpdate.getPassengers().isEmpty()) {
                return "BAD_REQUEST";
            }

            ACSAddToPriorityListRQACS addToPriorityListRQACS = new ACSAddToPriorityListRQACS();

            ItineraryACS itineraryACS = new ItineraryACS();

            itineraryACS.setAirline(passengerListUpdate.getItinerary().getAirlineCode());
            itineraryACS.setFlight(passengerListUpdate.getItinerary().getFlightNumber());
            itineraryACS.setBookingClass(passengerListUpdate.getItinerary().getBookingClass().toUpperCase());
            itineraryACS.setDepartureDate(passengerListUpdate.getItinerary().getDepartureDate());
            itineraryACS.setOrigin(passengerListUpdate.getItinerary().getOrigin().toUpperCase());
            itineraryACS.setDestination(passengerListUpdate.getItinerary().getDestination().toUpperCase());
            addToPriorityListRQACS.setItinerary(itineraryACS);

            PassengerInfoListACS passengerInfoList = new PassengerInfoListACS();

            for (PassengerListUpdate.BookedTraveler passenger : passengerListUpdate.getPassengers()) {
                PassengerInfoACS passengerInfo = new PassengerInfoACS();
                passengerInfo.setLastName(passenger.getLastname());

                passengerInfo.setPassengerID(passenger.getPassengerId());

                if (null != passenger.getPriorityCode()) {
                    PriorityClassificationInfoACS priorityClassificationInfo = new PriorityClassificationInfoACS();
                    priorityClassificationInfo.setPriorityCode(passenger.getPriorityCode());

                    if (null != passenger.getUpgradeCode() && !passenger.getUpgradeCode().trim().isEmpty()) {
                        priorityClassificationInfo.setUpgradePriorityCode(passenger.getUpgradeCode());
                    }

                    passengerInfo.setPriorityClassificationInfo(priorityClassificationInfo);

                    passengerInfo.getNOBAGS();
                    passengerInfoList.getPassengerInfo().add(passengerInfo);
                }
            }

            if (null != passengerInfoList.getPassengerInfo() && !passengerInfoList.getPassengerInfo().isEmpty()) {

                addToPriorityListRQACS.setPassengerInfoList(passengerInfoList);

                if (null == passengerListUpdate.getClient() || passengerListUpdate.getClient().trim().isEmpty()) {
                    addToPriorityListRQACS.setClient("WEB");
                } else {
                    addToPriorityListRQACS.setClient(passengerListUpdate.getClient().toUpperCase());
                }

                String boardingPrinterCommand = getSetUpConfigFactory().getInstance().getBoardingPassPrinterCommand();
                String boardingPrinter = getSetUpConfigFactory().getInstance().getBoardingLNIATA();

                ACSAddToPriorityListRSACS addToPriorityListRSACS = getAddToPriorityListService().addToPriorityLis(
                        addToPriorityListRQACS, boardingPrinter, boardingPrinterCommand
                );

                //TODO: validate response

                return "OK";
            }

            return "BAD_REQUEST";
        } catch (Exception ex) {
            return "ERROR";
        }
    }

    public void addTierPassengersToPriorityList(
            List<BookedTraveler> bookedTravelerList,
            List<BookedSegment> checkInSegments,
            String pos
    ) {
        //Add revenue pax to priority list
        for (BookedTraveler bookedTraveler : bookedTravelerList) {
            bookedTraveler.getPriorityBySegmentCollection().getCollection().clear();
        }

        if (null == checkInSegments || checkInSegments.isEmpty()) {
            return;
        }

        for (BookedSegment checkInSegment : checkInSegments) {
            try {
                //If titular is already checked-in from other session
                boolean titularIsOnUpgradeList = false;
                for (BookedTraveler bookedTraveler : bookedTravelerList) {
                    if (bookedTraveler.isTitular()) {
                        CheckInStatus checkInStatus = bookedTraveler.getCheckinStatusBySegment().getCheckInStatus(
                                checkInSegment.getSegment().getSegmentCode()
                        );
                        if (null != checkInStatus && checkInStatus.isCheckinStatus() && checkInStatus.isOnUpgradeList()) {
                            titularIsOnUpgradeList = true;
                        }
                        break;
                    }
                }

                boolean titularApplyForUpgrade = false;

                List<BookedTraveler> checkedInPassengersNotListed = FilterPassengersUtil.getAlreadyCheckedInPassengersNotListed(
                        bookedTravelerList,
                        checkInSegment.getSegment().getSegmentCode()
                );

                Map<String, UpgradeKnowledge> upgradeFactsMap = UpgradeEligibilityRuleService.getFacts(
                        checkedInPassengersNotListed,
                        checkInSegment
                );

                if (!upgradeFactsMap.isEmpty()) {

                    getUpgradeEligibilityRuleService().evaluateRules(upgradeFactsMap);


                    for (BookedTraveler bookedTraveler : checkedInPassengersNotListed) {
                        UpgradeKnowledge upgradeKnowledge = upgradeFactsMap.get(bookedTraveler.getNameRefNumber());

                        if ((null != upgradeKnowledge
                                && null != upgradeKnowledge.getResult()
                                && bookedTraveler.isTitular()
                                && upgradeKnowledge.getResult().isApplyForUpgrade())
                                || (bookedTraveler.isCompanion() && (titularIsOnUpgradeList || titularApplyForUpgrade))) { //ignore the rule if titular is already on list

                            if (!titularIsOnUpgradeList && bookedTraveler.isTitular()) {
                                titularApplyForUpgrade = true;
                            }

                            PriorityBySegment priorityBySegment = new PriorityBySegment();
                            priorityBySegment.setSegmentCode(checkInSegment.getSegment().getSegmentCode());
                            priorityBySegment.setPriorityCode(upgradeKnowledge.getFact().getPriorityCode());
                            priorityBySegment.setApplyForUpgrade(true);

                            bookedTraveler.getPriorityBySegmentCollection().getCollection().add(priorityBySegment);
                        }
                    }
                } else {
                    LOGGER.info("UpgradeFactsMap is empty");
                }

                List<BookedTraveler> checkedInPassengersApplyingForUpgrades = FilterPassengersUtil.getRevenuePassengersApplyingForUpgrade(
                        checkedInPassengersNotListed,
                        checkInSegment.getSegment().getSegmentCode()
                );

                if (null != checkedInPassengersApplyingForUpgrades && !checkedInPassengersApplyingForUpgrades.isEmpty()) {
                    ACSAddToPriorityListRSACS addToPriorityListRS = addPassengerToPriorityList(
                            checkInSegment,
                            checkedInPassengersApplyingForUpgrades,
                            checkInSegment.getSegment().getSegmentCode(),
                            pos
                    );

                    if (null != addToPriorityListRS) {
                        for (BookedTraveler bookedTraveler : checkedInPassengersApplyingForUpgrades) {
                            boolean wasAdded = wasAddedToPriorityList(bookedTraveler, checkInSegment, addToPriorityListRS);
                            CheckInStatus checkInStatus = bookedTraveler.getCheckinStatusBySegment().getCheckInStatus(
                                    checkInSegment.getSegment().getSegmentCode()
                            );
                            if (null != checkInStatus) {
                                checkInStatus.setOnUpgradeList(wasAdded);
                            }
                            bookedTraveler.setOnUpgradeList(bookedTraveler.isOnUpgradeList() || wasAdded);
                        }
                    }
                } else {
                    LOGGER.info("CheckedInPassengersApplyingForUpgrades is empty");
                }
            } catch (Exception ex) {
                LOGGER.error(ex.getMessage(), ex);
            }
        }
    }

    public boolean wasAddedToPriorityList(
            BookedTraveler bookedTraveler,
            BookedSegment bookedSegment,
            ACSAddToPriorityListRSACS addToPriorityListRS
    ) {
        if (ErrorOrSuccessCode.SUCCESS.equals(addToPriorityListRS.getResult().getStatus())
                && null != addToPriorityListRS.getItineraryPassengerList()
                && null != addToPriorityListRS.getItineraryPassengerList().getItineraryPassenger()
                && !addToPriorityListRS.getItineraryPassengerList().getItineraryPassenger().isEmpty()) {

            for (ItineraryPassengerACS itineraryPassengerACS : addToPriorityListRS.getItineraryPassengerList().getItineraryPassenger()) {

                if (itineraryPassengerACS.getItineraryDetail().getOrigin().equalsIgnoreCase(bookedSegment.getSegment().getDepartureAirport())
                        && FlightNumberUtil.compareFlightNumbers(itineraryPassengerACS.getItineraryDetail().getFlight(), bookedSegment.getSegment().getOperatingFlightCode())
                        && itineraryPassengerACS.getItineraryDetail().getDepartureDate().equalsIgnoreCase(bookedSegment.getSegment().getDepartureDateTime().substring(0, 10))
                        && itineraryPassengerACS.getItineraryDetail().getAirline().equalsIgnoreCase(bookedSegment.getSegment().getOperatingCarrier())
                ) {

                    if (null != itineraryPassengerACS.getPassengerDetailList()
                            && null != itineraryPassengerACS.getPassengerDetailList().getPassengerDetail()) {
                        for (PassengerDetailACS passengerDetailACS : itineraryPassengerACS.getPassengerDetailList().getPassengerDetail()) {
                            if (passengerDetailACS.getPassengerID().equalsIgnoreCase(bookedTraveler.getId())) {

                                if (null != passengerDetailACS.getFreeTextInfoList()
                                        && null != passengerDetailACS.getFreeTextInfoList().getFreeTextInfo()) {
                                    for (FreeTextInfoACS freeTextInfoACS : passengerDetailACS.getFreeTextInfoList().getFreeTextInfo()) {
                                        if (null != freeTextInfoACS.getTextLine() && null != freeTextInfoACS.getTextLine().getText()) {
                                            for (String text : freeTextInfoACS.getTextLine().getText()) {
                                                if ("!PASSENGER ON PRIORITY LIST".equalsIgnoreCase(text)) {
                                                    return true;
                                                } else if ("!PASSENGER ALREADY ON PRIORITY LIST".equalsIgnoreCase(text)) {
                                                    return true;
                                                }
                                            }
                                        }
                                    }
                                }

                                return false;
                            }
                        }
                    }

                    return false;
                }
            }
        }

        return false;
    }

    /**
     * @param bookedSegment
     * @param bookedTravelerList
     * @param segmentCode
     * @param pos
     * @return
     * @throws Exception
     */
    private ACSAddToPriorityListRSACS addPassengerToPriorityList(
            BookedSegment bookedSegment,
            List<BookedTraveler> bookedTravelerList,
            String segmentCode,
            String pos
    ) throws Exception {

        ACSAddToPriorityListRQACS addToPriorityListRQACS = getAcsAddToPriorityListRQ(
                bookedSegment,
                bookedTravelerList,
                segmentCode,
                pos
        );

        String boardingPrinterCommand = getSetUpConfigFactory().getInstance().getBoardingPassPrinterCommand();
        String boardingPrinter = getSetUpConfigFactory().getInstance().getBoardingLNIATA();

        ACSAddToPriorityListRSACS addToPriorityListRSACS = getAddToPriorityListService().addToPriorityLis(
                addToPriorityListRQACS, boardingPrinter, boardingPrinterCommand
        );

        //TODO: validate response

        return addToPriorityListRSACS;
    }

    public ACSAddToPriorityListRSACS addPassengerToPriorityListCheckIn(
            BookedSegment bookedSegment,
            List<BookedTraveler> bookedTravelerList,
            String segmentCode,
            String pos
    ) throws Exception {

        ACSAddToPriorityListRQACS addToPriorityListRQACS = getAcsAddToPriorityListRQCheckInService(
                bookedSegment,
                bookedTravelerList,
                segmentCode,
                pos
        );

        String boardingPrinterCommand = getSetUpConfigFactory().getInstance().getBoardingPassPrinterCommand();
        String boardingPrinter = getSetUpConfigFactory().getInstance().getBoardingLNIATA();

        ACSAddToPriorityListRSACS addToPriorityListRSACS = getAddToPriorityListService().addToPriorityLis(
                addToPriorityListRQACS, boardingPrinter, boardingPrinterCommand
        );

        //TODO: validate response

        return addToPriorityListRSACS;
    }

    /**
     * @param bookedSegment
     * @param bookedTravelerList
     * @param segmentCode
     * @param pos
     * @return
     */
    private ACSAddToPriorityListRQACS getAcsAddToPriorityListRQ(
            BookedSegment bookedSegment,
            List<BookedTraveler> bookedTravelerList,
            String segmentCode,
            String pos
    ) {

        ACSAddToPriorityListRQACS addToPriorityListRQACS = new ACSAddToPriorityListRQACS();

        ItineraryACS itineraryACS = CheckinUtil.getItineraryListFromSameSegments(bookedSegment);
        addToPriorityListRQACS.setItinerary(itineraryACS);

        PassengerInfoListACS passengerInfoList = CheckinUtil.getPassengerInfoList(
                bookedTravelerList,
                segmentCode
        );
        addToPriorityListRQACS.setPassengerInfoList(passengerInfoList);

        if (PosType.KIOSK.toString().equalsIgnoreCase(pos)) {
            addToPriorityListRQACS.setClient("KIOSK");
        } else if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(pos)) {
            addToPriorityListRQACS.setClient("MOBILE");
        } else {
            addToPriorityListRQACS.setClient("WEB");
        }

        return addToPriorityListRQACS;
    }

    /**
     * @param bookedSegment
     * @param bookedTravelerList
     * @param segmentCode
     * @param pos
     * @return
     */
    private ACSAddToPriorityListRQACS getAcsAddToPriorityListRQCheckInService(
            BookedSegment bookedSegment,
            List<BookedTraveler> bookedTravelerList,
            String segmentCode,
            String pos
    ) {

        ACSAddToPriorityListRQACS addToPriorityListRQACS = new ACSAddToPriorityListRQACS();

        ItineraryACS itineraryACS = CheckinUtil.getItineraryListFromSameSegments(bookedSegment);
        addToPriorityListRQACS.setItinerary(itineraryACS);

        PassengerInfoListACS passengerInfoList = CheckinUtil.getPassengerInfoListCheckInService(
                bookedTravelerList,
                segmentCode
        );
        addToPriorityListRQACS.setPassengerInfoList(passengerInfoList);

        if (PosType.KIOSK.toString().equalsIgnoreCase(pos)) {
            addToPriorityListRQACS.setClient("KIOSK");
        } else if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(pos)) {
            addToPriorityListRQACS.setClient("MOBILE");
        } else {
            addToPriorityListRQACS.setClient("WEB");
        }

        return addToPriorityListRQACS;
    }

    public AMAddToPriorityListService getAddToPriorityListService() {
        return addToPriorityListService;
    }

    public void setAddToPriorityListService(AMAddToPriorityListService addToPriorityListService) {
        this.addToPriorityListService = addToPriorityListService;
    }

    public UpgradeEligibilityRuleService getUpgradeEligibilityRuleService() {
        return upgradeEligibilityRuleService;
    }

    public void setUpgradeEligibilityRuleService(UpgradeEligibilityRuleService upgradeEligibilityRuleService) {
        this.upgradeEligibilityRuleService = upgradeEligibilityRuleService;
    }

    public CheckInSetUpConfigFactoryWrapper getSetUpConfigFactory() {
        return setUpConfigFactory;
    }

    public void setSetUpConfigFactory(CheckInSetUpConfigFactoryWrapper setUpConfigFactory) {
        this.setUpConfigFactory = setUpConfigFactory;
    }
}
