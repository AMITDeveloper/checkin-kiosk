package com.am.checkin.web.v2.util;

import com.aeromexico.commons.model.AbstractAncillary;
import com.aeromexico.commons.model.AncillaryOfferCollection;
import com.aeromexico.commons.model.BaggageAncillaryOffer;

import java.util.ArrayList;
import java.util.List;

public class AncillaryCollectionUtil {

    public static AncillaryOfferCollection getAncillaryOfferCollectionEmpty() {
        AncillaryOfferCollection ancillaryOfferCollection;
        ancillaryOfferCollection = new AncillaryOfferCollection();

        List<AbstractAncillary> listAbstractAncillary = new ArrayList<>();
        BaggageAncillaryOffer baggageAncillaryOffer = new BaggageAncillaryOffer();
        listAbstractAncillary.add(baggageAncillaryOffer);
        ancillaryOfferCollection.setCollection(listAbstractAncillary);
        return ancillaryOfferCollection;
    }
}
