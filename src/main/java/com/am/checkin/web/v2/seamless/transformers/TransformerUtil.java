package com.am.checkin.web.v2.seamless.transformers;

import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.Infant;
import com.aeromexico.commons.model.Mandate;
import com.aeromexico.commons.model.TicketNumber;
import com.aeromexico.commons.model.TravelerDocument;
import com.aeromexico.commons.model.VisaInfo;
import com.aeromexico.commons.web.types.GenderType;
import com.aeromexico.commons.web.util.UuidUtil;
import com.aeromexico.dao.util.CommonUtil;
import com.am.checkin.web.util.PNRLookUpServiceUtil;
import com.am.seamless.checkin.common.models.DatedOperatingLeg;
import com.am.seamless.checkin.common.models.DatedOperatingSegment;
import com.am.seamless.checkin.common.models.IdentityDocument;
import com.am.seamless.checkin.common.models.Passenger;
import com.am.seamless.checkin.common.models.SupplementaryDocument;
import com.am.seamless.checkin.common.models.Ticket;
import com.am.seamless.checkin.common.models.TravelParty;
import com.am.seamless.checkin.common.models.Trip;
import com.am.seamless.checkin.common.models.types.DataSourceCodeType;
import com.am.seamless.checkin.common.models.types.GenderCodeType;
import com.am.seamless.checkin.common.models.types.IdentityDocumentCodeType;
import com.am.seamless.checkin.common.models.types.SupplementaryDocumentCodeType;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class TransformerUtil {

    public static Mandate getMandate(String id, List<Mandate> mandateList) {
        if (null == id) {
            return null;
        }

        for (Mandate mandate : mandateList) {
            if (UuidUtil.compare(id, mandate.getId())) {
                return mandate;
            }
        }

        return null;
    }

    public static Trip getTrip(BookedLeg bookedLeg, List<Trip> trips) {

        for (Trip trip : trips) {
            for (DatedOperatingSegment segment : trip.getSegments()) {
                for (DatedOperatingLeg leg : segment.getLegs()) {
                    String segmentCode = getSegmentCode(segment, leg);

                    BookedSegment bookedSegment = bookedLeg.getBookedSegment(segmentCode);

                    if (null != bookedSegment) {
                        return trip;
                    }
                }
            }
        }

        return null;
    }

    public static Trip getTrip(String segmentId, String legId, List<Trip> trips) {

        for (Trip trip : trips) {
            for (DatedOperatingSegment segment : trip.getSegments()) {
                if (UuidUtil.compare(segment.getId(), segmentId)) {
                    for (DatedOperatingLeg leg : segment.getLegs()) {
                        if (UuidUtil.compare(leg.getId(), legId)) {
                            return trip;
                        }
                    }
                }
            }
        }

        return null;
    }

    public static String getSegmentCode(DatedOperatingSegment datedOperatingSegment, DatedOperatingLeg datedOperatingLeg) {

        StringBuilder sb = new StringBuilder();

        sb.append(datedOperatingLeg.getTransportDeparture().getStation().getCode());
        sb.append("_");
        sb.append(datedOperatingLeg.getTransportArrival().getStation().getCode());
        sb.append("_");
        sb.append(datedOperatingSegment.getOperatingCarrier().getCode());
        sb.append("_");
        sb.append(datedOperatingLeg.getTransportDeparture().getScheduledDepartureLocalDateTime(), 0, 10);
        sb.append("_");
        sb.append(datedOperatingLeg.getTransportDeparture().getScheduledDepartureLocalDateTime(), 11, 13);
        sb.append(datedOperatingLeg.getTransportDeparture().getScheduledDepartureLocalDateTime(), 14, 16);

        return sb.toString();

    }

    public static Optional<IdentityDocument> getPassport(List<IdentityDocument> documentList) {

        Stream<IdentityDocument> identityDocumentStream = documentList.stream()
                .filter(identityDocument -> IdentityDocumentCodeType.PASSPORT.equals(identityDocument.getType())
                );

        return identityDocumentStream.findFirst();

    }

    public static Optional<SupplementaryDocument> getVisa(List<SupplementaryDocument> documentList) {

        Stream<SupplementaryDocument> supplementaryDocumentStream = documentList.stream()
                .filter(supplementaryDocument -> SupplementaryDocumentCodeType.VISA.equals(supplementaryDocument.getSupplementaryDocumentType())
                );

        return supplementaryDocumentStream.findFirst();
    }

    public static VisaInfo getVisaInfo(SupplementaryDocument supplementaryDocument) {
        VisaInfo visaInfo = new VisaInfo();
        if (null != supplementaryDocument.getIssuingCountry()) {
            visaInfo.setApplicableCountryCode(supplementaryDocument.getIssuingCountry().getCode());
        }
        visaInfo.setBirthPlace(null);
        visaInfo.setExpirationDate(supplementaryDocument.getExpirationDate());
        visaInfo.setIssueCity(null);
        visaInfo.setIssueDate(null);
        visaInfo.setNumber(supplementaryDocument.getNumber());
        visaInfo.setType(null);
        return visaInfo;
    }

    public static TravelerDocument getTravelerDocument(IdentityDocument identityDocument) {
        TravelerDocument travelerDocument = new TravelerDocument();

        travelerDocument.setExpirationDate(identityDocument.getExpirationDate());
        if (null != identityDocument.getIssuingCountry()) {
            travelerDocument.setIssuingCountry(identityDocument.getIssuingCountry().getCode());
        }
        if (DataSourceCodeType.MACHINE_READABLE_ZONE.equals(identityDocument.getDataSourceType())) {
            travelerDocument.setPassportScanned(true);
        }
        if (null != identityDocument.getNationality()) {
            travelerDocument.setNationality(identityDocument.getNationality().getCode());
        }
        travelerDocument.setDocumentNumber(identityDocument.getNumber());
        travelerDocument.setIssueDate(identityDocument.getIssueDate());
        return travelerDocument;
    }

    public static GenderType getGender(GenderCodeType genderCodeType, boolean infant) {
        if (null == genderCodeType) {
            return null;
        }

        switch (genderCodeType) {
            case FEMALE:
                if (infant) {
                    return GenderType.FI;
                } else {
                    return GenderType.F;
                }
            case MALE:
                if (infant) {
                    return GenderType.MI;
                } else {
                    return GenderType.M;
                }
            default:
                return GenderType.DEFAULT;
        }
    }

    public static GenderCodeType getGender(GenderType genderType) {

        if (null == genderType) {
            return null;
        }

        switch (genderType) {
            case F:
            case FI:
                return GenderCodeType.FEMALE;
            case M:
            case MI:
                return GenderCodeType.MALE;
            case C:
            case DEFAULT:
            default:
                return GenderCodeType.UNSPECIFIED;
        }
    }

    public static Optional<Passenger> getPassenger(BookedTraveler bookedTraveler, TravelParty travelParty) {

        Stream<Passenger> passengerSeamless = travelParty.getSelectedPassengers().stream()
                .filter(selectedPassenger ->
                        //selectedPassenger.getPassengerType().name().equals(bookedTraveler.getPaxType().getDescription()) &&
                        (PNRLookUpServiceUtil.compareNames(CommonUtil.removePrefix(selectedPassenger.getGivenNames()), CommonUtil.removePrefix(bookedTraveler.getFirstName()))
                                && PNRLookUpServiceUtil.compareNames(CommonUtil.removePrefix(selectedPassenger.getSurname()), CommonUtil.removePrefix(bookedTraveler.getLastName())))
                                || containsTicket(bookedTraveler, selectedPassenger)
                );

        return passengerSeamless.findFirst();
    }

    public static Optional<Passenger> getPassenger(Infant bookedTraveler, TravelParty travelParty) {

        Stream<Passenger> passengerSeamless = travelParty.getSelectedPassengers().stream()
                .filter(selectedPassenger ->
                        //selectedPassenger.getPassengerType().name().equals(bookedTraveler.getPaxType().getDescription()) &&
                        (PNRLookUpServiceUtil.compareNames(CommonUtil.removePrefix(selectedPassenger.getGivenNames()), CommonUtil.removePrefix(bookedTraveler.getFirstName()))
                                && PNRLookUpServiceUtil.compareNames(CommonUtil.removePrefix(selectedPassenger.getSurname()), CommonUtil.removePrefix(bookedTraveler.getLastName())))
                                || containsTicket(bookedTraveler, selectedPassenger)
                );

        return passengerSeamless.findFirst();


    }

    public static boolean containsTicket(BookedTraveler bookedTraveler, Passenger passenger) {

        for (Ticket ticket : passenger.getTickets()) {
            for (TicketNumber ticketNumber : bookedTraveler.getTicketNumbers()) {
                if (ticketNumber.getNumber().equalsIgnoreCase(ticket.getNumber())) {
                    return true;
                }
            }
        }

        if (null != bookedTraveler.getTicketNumber()) {
            for (Ticket ticket : passenger.getTickets()) {
                if (ticket.getNumber().equalsIgnoreCase(bookedTraveler.getTicketNumber())) {
                    return true;
                }
            }
        }

        return false;
    }

    public static boolean containsTicket(Infant bookedTraveler, Passenger passenger) {

        for (Ticket ticket : passenger.getTickets()) {
            for (TicketNumber ticketNumber : bookedTraveler.getTicketNumbers()) {
                if (ticketNumber.getNumber().equalsIgnoreCase(ticket.getNumber())) {
                    return true;
                }
            }
        }

        if (null != bookedTraveler.getTicketNumber()) {
            for (Ticket ticket : passenger.getTickets()) {
                if (ticket.getNumber().equalsIgnoreCase(bookedTraveler.getTicketNumber())) {
                    return true;
                }
            }
        }

        return false;
    }
}
