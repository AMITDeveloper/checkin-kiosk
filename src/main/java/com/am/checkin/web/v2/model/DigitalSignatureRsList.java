package com.am.checkin.web.v2.model;


import java.util.ArrayList;
import java.util.Collection;

public class DigitalSignatureRsList extends ArrayList<DigitalSignatureRs> {

    /**
     * Serial version class
     */
    private static final long serialVersionUID = 1L;

    /**
     * Empty Constructor
     */
    public DigitalSignatureRsList() {

    }

    /**
     * Constructor with collection param
     *
     * @param c
     */
    public DigitalSignatureRsList(Collection<? extends DigitalSignatureRs> c) {
        super(c);
    }
}
