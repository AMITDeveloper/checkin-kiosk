package com.am.checkin.web.v2.controller;

import com.aeromexico.commons.model.PNRCollection;
import com.aeromexico.commons.model.rq.PnrRQ;
import com.aeromexico.dao.util.CommonUtil;
import com.aeromexico.sabre.api.session.common.ServiceCall;
import com.am.checkin.web.controller.PNRLookupController;
import com.am.checkin.web.v2.services.ReservationLookupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("/pnr/v2")
public class TravelPartyController {
    private static final Logger LOG = LoggerFactory.getLogger(PNRLookupController.class);

    @Inject
    private ReservationLookupService reservationLookupService;

    @GET
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPNR(@Valid @BeanParam PnrRQ pnrRQ, @HeaderParam("X-AM-User-Auth") String userAuth) throws Exception {
        
        LOG.info("SEAMLES_ENPOINT {} PNR: {} ", "/pnr/v2", pnrRQ.getRecordLocator());

        pnrRQ.setUserAuth(userAuth);

        pnrRQ.setUriStr(CommonUtil.getLink(pnrRQ.getUri()));

        try {
            List<ServiceCall> serviceCallList = new ArrayList<>();
            PNRCollection pnrCollection;

            //Process the request by calling the pnrLookup service.
            pnrCollection = reservationLookupService.lookUpReservation(pnrRQ, serviceCallList);

            LOG.info("PNRLookupController.getPNR RES: " + pnrCollection);

            //Build the response object
            GenericEntity<PNRCollection> genericEntity = new GenericEntity<>(pnrCollection, PNRCollection.class);

            //Send back the response
            return Response.status(Response.Status.OK).entity(genericEntity).build();

        } catch (Exception ex) {
            throw ex;
        }
    }

}
