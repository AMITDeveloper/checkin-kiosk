package com.am.checkin.web.v2.seamless.service;

import com.am.checkin.web.v2.model.DigitalSignatureRq;
import com.am.checkin.web.v2.model.DigitalSignatureRqList;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/digitalsignature")
public interface DigitalSignatureServicesInterface {

    @POST
    @Path("/signboardingpass")
    @Consumes({ MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response signBoardingpass(
            DigitalSignatureRq digitalSignatureRq
    ) throws Exception;

    @POST
    @Path("/signboardingpasses")
    @Consumes({ MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response signBoardingpasses(
            DigitalSignatureRqList digitalSignatureRqList
    ) throws Exception;

}
