package com.am.checkin.web.v2.services;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.AuthorizationPaymentConfig;
import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedLegCollection;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.PNR;
import com.aeromexico.commons.model.PNRCollection;
import com.aeromexico.commons.model.rq.PnrRQ;
import com.aeromexico.commons.myb.model.ManageStatus;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.types.PosType;
import com.aeromexico.commons.web.util.Constants;
import com.aeromexico.commons.web.util.ErrorCodeDescriptions;
import com.aeromexico.commons.web.util.TimeUtil;
import com.aeromexico.dao.services.MarketFlightService;
import com.aeromexico.dao.util.CommonUtil;
import com.aeromexico.sabre.api.pnr.AMGetReservationService;
import com.aeromexico.sabre.api.session.common.ServiceCall;
import com.aeromexico.sabre.api.tripsearch.model.TripSearchOption;
import com.aeromexico.sharedservices.services.TicketDocumentService;
import com.aeromexico.sharedservices.util.BrandedFareRulesUtil;
import com.aeromexico.sharedservices.util.PnrUtil;
import com.aeromexico.sharedservices.util.TravelerItineraryUtil;
import com.am.checkin.web.service.CheckInSetUpConfigFactoryWrapper;
import com.am.checkin.web.service.PnrCollectionService;
import com.am.checkin.web.util.PNRLookUpServiceUtil;
import com.am.checkin.web.util.PnrCollectionUtil;
import com.am.checkin.web.util.TicketDocumentUtil;
import com.am.checkin.web.v2.seamless.service.SeamlessTravelPartyService;
import com.am.checkin.web.v2.util.SearchServiceUtil;
import com.am.checkin.web.v2.util.SystemVariablesUtil;
import com.google.gson.Gson;
import com.sabre.ns.ticketing.dc.GetTicketingDocumentRS;
import com.sabre.webservices.pnrbuilder.getreservation.ErrorsPNRB;
import com.sabre.webservices.pnrbuilder.getreservation.GetReservationRS;
import com.sabre.webservices.pnrbuilder.getreservation.PassengerPNRB;
import com.sabre.webservices.pnrbuilder.updatereservation.ReservationUpdateItemType;
import org.aeromexico.commons.exception.config.ReadProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author adrian
 */
@Named
@ApplicationScoped
public class ReservationLookupService {

    private static final Logger LOG = LoggerFactory.getLogger(ReservationLookupService.class);

//    @Inject
//    private GetReservationService getReservationService;

    @Inject
    private AMGetReservationService amGetReservationService;

    @Inject
    private TicketDocumentService ticketDocumentService;

    @Inject
    private SearchReservationService searchReservationService;

    @Inject
    private ItineraryService itineraryService;

    @Inject
    private SeamlessTravelPartyService seamlessTravelPartyService;

    @Inject
    private PnrCollectionService pnrCollectionService;

    @Inject
    private MarketFlightService marketFlightService;

    @Inject
    @Named("CheckInSetUpConfigFactoryWrapper")
    private CheckInSetUpConfigFactoryWrapper setUpConfigFactory;

    @Inject
    private ReadProperties prop;

    @PostConstruct
    public void init() {
        if (null == getSetUpConfigFactory()) {
            setSetUpConfigFactory(new CheckInSetUpConfigFactoryWrapper());
        }
    }

    public GetReservationRS getReservationRS(PnrRQ pnrRQ, List<ServiceCall> serviceCallList) throws Exception {
        GetReservationRS getReservationRS = null;
        try {
            getReservationRS = getReservation(pnrRQ.getRecordLocator(), pnrRQ.getCartId(),"Simple", getSubjectAreas(), pnrRQ.getPos());
        } catch (Exception e) {
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(pnrRQ.getPos())) {
                LOG.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_22110301,"ReservationLookupService.java:108", e.getMessage());
                throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getCode(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getActionable(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getMessage(),
                        Response.Status.INTERNAL_SERVER_ERROR);
            } else {
                LOG.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_12110301,"ReservationLookupService.java:114", e.getMessage());
                throw e;
            }
        }

        if (null != getReservationRS.getErrors()) {
            String errorCode ="";
            for (ErrorsPNRB.Error error : getReservationRS.getErrors().getError()) {
                errorCode = error.getCode();
                if ("700102".equalsIgnoreCase(error.getCode())) {
                    if (null != pnrRQ.getPos() && PosType.WEB.toString().equalsIgnoreCase(pnrRQ.getPos())) {
                        throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.RESERVATION_NOT_FOUND_WEB_CHECKIN, CommonUtil.getMessageByCode(Constants.CODE_046), error.getMessage());
                    } else {
                        throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.RECORD_LOCATOR_NOT_FOUND_APP, CommonUtil.getMessageByCode(Constants.CODE_046), error.getMessage());
                    }
                }
                if ("100123".equalsIgnoreCase(error.getCode())) {
                    String recordLocator = getSearchReservationService().searchByAgencyRecordlocator(
                            pnrRQ, serviceCallList
                    );
                    pnrRQ.setRecordLocator(
                            recordLocator
                    );
                    try {
                        getReservationRS = getReservation(pnrRQ.getRecordLocator(), pnrRQ.getCartId(),"Simple", getSubjectAreas(), pnrRQ.getPos());
                    } catch (Exception e) {
                        LOG.error(e.getMessage(), e);
                        throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.INTERNAL_ERROR, ErrorCodeDescriptions.INTERNAL_SABRE_ERROR);
                    }
                    if (getReservationRS == null) {
                        throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNKNOWN_CODE, "Get reservation response is empty :"+errorCode+" "+pnrRQ.getRecordLocator());
                    }

                    if (null != getReservationRS.getErrors()) {
                        for (ErrorsPNRB.Error errors : getReservationRS.getErrors().getError()) {
                            errorCode = errors.getCode();
                            if ("700102".equalsIgnoreCase(errors.getCode())) {
                                if (null != pnrRQ.getPos() && PosType.WEB.toString().equalsIgnoreCase(pnrRQ.getPos())) {
                                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.RESERVATION_NOT_FOUND_WEB_CHECKIN, CommonUtil.getMessageByCode(Constants.CODE_046), error.getMessage());
                                } else {
                                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.RECORD_LOCATOR_NOT_FOUND_APP, CommonUtil.getMessageByCode(Constants.CODE_046), error.getMessage());
                                }
                            }
                            if ("100123".equalsIgnoreCase(errors.getCode())) {
                                if (null != pnrRQ.getPos() && PosType.WEB.toString().equalsIgnoreCase(pnrRQ.getPos())) {
                                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.RESERVATION_NOT_FOUND_WEB_CHECKIN, CommonUtil.getMessageByCode(Constants.CODE_046), error.getMessage());
                                } else {
                                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.RECORD_LOCATOR_NOT_FOUND_APP, CommonUtil.getMessageByCode(Constants.CODE_046), error.getMessage());
                                }
                            }
                        }
                    }
                }
            }
            if(null == getReservationRS.getReservation()){
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNKNOWN_CODE, "Get Reservation response is empty : "+errorCode+" "+pnrRQ.getRecordLocator());
            }
        }

        if (getReservationRS == null) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.UNKNOWN_CODE, "Get Reservation response is empty : " +pnrRQ.getRecordLocator());
        }

        if (!pnrRQ.isSkipNamesValidation() && (PosType.WEB.toString().equalsIgnoreCase(pnrRQ.getPos())
                || PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(pnrRQ.getPos()))) {
            if (!isLastNameMatchingInReservation(getReservationRS, pnrRQ.getLastName())) {
                throw new GenericException(
                        Response.Status.BAD_REQUEST,
                        ErrorType.NAME_NOT_FOUND_IN_PNR_CHECKIN,
                        ErrorType.NAME_NOT_FOUND_IN_PNR_CHECKIN.getFullDescription()
                );
            }
        }

        TravelerItineraryUtil.filterTravelItinerarySegments(getReservationRS);

        if (!TravelerItineraryUtil.validateTravelItinerarySegments(getReservationRS)) {
            if("WEB".equalsIgnoreCase(pnrRQ.getPos())){
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.INVALID_ITINERARY_WEB, "NO AVAILABLE SEGMENTS IN TRAVEL ITINERARY");
            } else{
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.INVALID_ITINERARY, "NO AVAILABLE SEGMENTS IN TRAVEL ITINERARY");
            }
        }

        return getReservationRS;

    }

    private boolean isLastNameMatchingInReservation(GetReservationRS getReservationRS, String lastName) {

        for (PassengerPNRB personName : getReservationRS.getReservation().getPassengerReservation().getPassengers().getPassenger()) {

            if (personName.getLastName() != null) {

                //Earlier validation should have validated that the lastName provided in the request is NOT null
                //If the given last name is less than three letters, do an exact match
                if (lastName.length() < 3) {
                    if (lastName.equalsIgnoreCase(personName.getLastName())) {
                        return true;
                    }
                } else if (personName.getLastName().toUpperCase().startsWith(lastName.toUpperCase().substring(0, 3))) {
                    return true;
                }
            }

        }

        return false;
    }

    public List<String> getSubjectAreas() {
        List<String> subjectAreas = new ArrayList<>();
        subjectAreas.add("HEADER");
        subjectAreas.add("ACTIVE");
        subjectAreas.add("HISTORICAL");
        subjectAreas.add("AIR_CABIN");
        subjectAreas.add("PRICING_INFORMATION");
        subjectAreas.add("PASSENGERDETAILS");
        subjectAreas.add("NAME");
        subjectAreas.add("TICKETING");
        subjectAreas.add("VCR");
        subjectAreas.add("AFAX");
        return subjectAreas;
    }

    public GetReservationRS getReservation(String recordLocator, String cartId, String viewName, List<String> subjectAreas, String pos) throws Exception {
        String msg = "";
        try {
            GetReservationRS reservationRS = amGetReservationService.getReservation(recordLocator, "STATELESS", viewName, subjectAreas);
            LOG.info(recordLocator, cartId, "Get Reservation Response", new Gson().toJson(reservationRS));

            return reservationRS;
        } catch (GenericException ex) {
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.INTERNAL_ERROR, ErrorCodeDescriptions.INTERNAL_SABRE_ERROR);
        } catch (Exception ex) {
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(pos)) {
                LOG.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_22110301,"ReservationLookupService.java:246", ex.getMessage());
                throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getCode(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getActionable(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getMessage(),
                        Response.Status.INTERNAL_SERVER_ERROR);
            } else {
                LOG.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_12110301,"ReservationLookupService.java:252", ex.getMessage());
                throw ex;
            }
        }
    }

    /**
     * @param pnrCollection This method is used to save the PNR collection
     *                      MongoDB in an synchronous mode
     */
    private void savePnrCollection(PNRCollection pnrCollection) {
        try {
            getPnrCollectionService().savePnrCollection(pnrCollection);
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
    }

    /**
     * @param pnrRQ
     * @return
     * @throws Exception
     * @throws ParseException
     */
    public PNR searchByPNR(
            PnrRQ pnrRQ
    ) throws Exception {

        List<ReservationUpdateItemType> reservationUpdateItemTypeList = new ArrayList<>();
        List<ReservationUpdateItemType> reservationUpdateSeatItemTypeList = new ArrayList<>();
        List<String> errorMessagesCaughtList = new ArrayList<>();

        GetReservationRS reservationRS = getReservation(
                pnrRQ.getRecordLocator(),
                pnrRQ.getCartId(),
                "Simple",
                getSubjectAreas(),
                pnrRQ.getPos()
        );

        GetTicketingDocumentRS ticketingDocumentRS = getTicketDocumentService().getTicketDocByRecordLocator(
                pnrRQ.getRecordLocator(),
                pnrRQ.getCartId()
        );


        PNR pnr = new PNR();
        pnr.setPnr(pnrRQ.getRecordLocator());
        pnr.setBoardingPassId(pnrRQ.getRecordLocator());

        //set reservation creationDate
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            String creationDate = sdf.format(
                    reservationRS.getReservation().getBookingDetails().getCreationTimestamp().toGregorianCalendar().getTime()
            );
            pnr.setCreationDate(creationDate);
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }

        boolean isAmTicketingProvider = TicketDocumentUtil.isAmTicketingProvider(ticketingDocumentRS);

        BookedLegCollection bookedLegCollection = getItineraryService().getBookedLegCollection(reservationRS, isAmTicketingProvider, pnrRQ);
        pnr.setLegs(bookedLegCollection);

        PnrUtil.setCouponNumber(bookedLegCollection, ticketingDocumentRS);

        BrandedFareRulesUtil.setRegion(pnr, getMarketFlightService());

        BrandedFareRulesUtil.setFareBasisCode(pnr, ticketingDocumentRS);

        BrandedFareRulesUtil.setSeatSelectionType(pnr);

        BrandedFareRulesUtil.setSeatChangeAllowance(pnr);

        Set<String> formOfPayments;
        formOfPayments = ItineraryService.getFormOfPayment(ticketingDocumentRS);
        pnr.setFormOfPayment(formOfPayments);

        if (SystemVariablesUtil.isTsaRequired()) {
            itineraryService.setTsaValidation(pnr, formOfPayments, pnrRQ);
        }

        itineraryService.setManageStatus(pnr, pnrRQ.getPos());

        for (BookedLeg seamlessLeg : bookedLegCollection.getCollection()) {

            CartPNR seamlessCart = getSeamlessTravelPartyService().getCarts(
                    pnrRQ,
                    seamlessLeg,
                    reservationRS,
                    ticketingDocumentRS,
                    reservationUpdateItemTypeList,
                    reservationUpdateSeatItemTypeList
            );

            pnr.getCarts().getCollection().add(seamlessCart);
        }


        return pnr;
    }

    /**
     * @param pnrRQ
     * @return
     * @throws Exception
     */
    private List<PNR> searchByFF(
            PnrRQ pnrRQ
    ) throws Exception {

        List<String> recordLocatorList = getSearchReservationService().searchByFF(pnrRQ);

        List<PNR> pnrList = new ArrayList<>();

        int maxReservations = 5;

        if (null != recordLocatorList && !recordLocatorList.isEmpty()) {

            for (String recordLocator : recordLocatorList) {

                PnrRQ pnrRQNewSearch = new PnrRQ();
                pnrRQNewSearch.setRecordLocator(recordLocator);
                pnrRQNewSearch.setLastName(pnrRQ.getLastName());
                pnrRQNewSearch.setStore(pnrRQ.getStore());
                pnrRQNewSearch.setPos(pnrRQ.getPos());
                pnrRQNewSearch.setStore(pnrRQ.getStore());
                pnrRQNewSearch.setLanguage(pnrRQ.getLanguage());
                pnrRQNewSearch.setTripSearchOption(TripSearchOption.CONFIRMATION_CODE.toString());

                try {
                    PNR pnr = searchByPNR(
                            pnrRQNewSearch
                    );

                    if (null != pnr) {
                        if (ManageStatus.OPEN.equals(pnr.getCheckinStatus())
                                || ManageStatus.OPEN.equals(pnr.getManageStatus())) {
                            pnrList.add(pnr);

                            if (pnrList.size() >= maxReservations) {
                                break;
                            }
                        }
                    }
                } catch (GenericException ex) {
                    LOG.error(ex.getMessage(), ex);
                } catch (Exception ex) {
                    LOG.error(ex.getMessage(), ex);
                }
            }
        }

        if (!pnrList.isEmpty()) {
            pnrList.sort((first, second) -> {
                try {
                    long firstDeparture;
                    firstDeparture = TimeUtil.getTime(
                            first.getLegs().getCollection().get(0).getFirstSegmentInLeg().getSegment().getDepartureDateTime()
                    );

                    long secondDeparture;
                    secondDeparture = TimeUtil.getTime(
                            second.getLegs().getCollection().get(0).getFirstSegmentInLeg().getSegment().getDepartureDateTime()
                    );
                    return (int) (firstDeparture - secondDeparture);
                } catch (Exception ex) {
                    return 1;
                }
            });
        }

        return pnrList;
    }

    /**
     * @param pnrRQ
     * @param serviceCallList
     * @return
     * @throws Exception This is the entry point to all type of reservations
     *                   lookup.
     */
    public PNRCollection lookUpReservation(PnrRQ pnrRQ, List<ServiceCall> serviceCallList) throws Exception {
        PNRCollection pnrCollection = new PNRCollection();
        pnrRQ = SearchServiceUtil.validateRequest(pnrRQ);

        PNR pnr;
        String recordLocator;

        switch (pnrRQ.getTripSearchOption()) {
            case "CONFIRMATION_CODE":
                pnr = searchByPNR(
                        pnrRQ
                );

                pnrCollection.getCollection().add(pnr);
                break;
            case "FF_PROGRAM":
                List<PNR> pnrList = searchByFF(
                        pnrRQ
                );

                pnrCollection.getCollection().addAll(pnrList);
                break;
            case "FLIGHT_NUMBER":
            case "DESTINATION":

                recordLocator = getSearchReservationService().searchByDestinationFlight(
                        pnrRQ
                );

                if (null != recordLocator && !recordLocator.trim().isEmpty()) {
                    pnrRQ.setRecordLocator(recordLocator);
                    pnr = searchByPNR(
                            pnrRQ
                    );
                    pnrCollection.getCollection().add(pnr);
                }
                break;
            case "VCR_CODE":

                recordLocator = getSearchReservationService().searchByVCR(
                        pnrRQ.getRecordLocator(),
                        pnrRQ.getPos()
                );

                if (null != recordLocator && !recordLocator.trim().isEmpty()) {
                    pnrRQ.setRecordLocator(recordLocator);
                    pnr = searchByPNR(
                            pnrRQ
                    );
                    pnrCollection.getCollection().add(pnr);
                }

                break;
            default:
                break;
        }

        if (null != pnrCollection && !pnrCollection.getCollection().isEmpty()) {
            pnrCollection.setPos(pnrRQ.getPos());
            pnrCollection.setStore(pnrRQ.getStore());

            if (null != pnrCollection.getCollection()
                    && !pnrCollection.getCollection().isEmpty()) {

                AuthorizationPaymentConfig authorizationPaymentConfig;
                authorizationPaymentConfig = getSetUpConfigFactory().getInstance().getAuthorizationPaymentConfig(
                        pnrRQ.getStore(),
                        pnrRQ.getPos()
                );
                String currencyCode = authorizationPaymentConfig.getCurrency();

                PnrCollectionUtil.setTotal(
                        pnrCollection,
                        currencyCode,
                        pnrRQ.getStore(),
                        pnrRQ.getPos()
                );

                //If there are no errors creating PnrCollection we build message info.
                pnrRQ.setMessage(PNRLookUpServiceUtil.buildMessageToBeLogged(pnrCollection));

                try {
                    savePnrCollection(pnrCollection);
                } catch (Exception ex) {
                    LOG.error(ex.getMessage(), ex);
                }
            }
        }

        return pnrCollection;

    }

    public MarketFlightService getMarketFlightService() {
        return marketFlightService;
    }

    public void setMarketFlightService(MarketFlightService marketFlightService) {
        this.marketFlightService = marketFlightService;
    }

    public TicketDocumentService getTicketDocumentService() {
        return ticketDocumentService;
    }

    public void setTicketDocumentService(TicketDocumentService ticketDocumentService) {
        this.ticketDocumentService = ticketDocumentService;
    }

    public SearchReservationService getSearchReservationService() {
        return searchReservationService;
    }

    public void setSearchReservationService(SearchReservationService searchReservationService) {
        this.searchReservationService = searchReservationService;
    }

    public ItineraryService getItineraryService() {
        return itineraryService;
    }

    public void setItineraryService(ItineraryService itineraryService) {
        this.itineraryService = itineraryService;
    }

    public SeamlessTravelPartyService getSeamlessTravelPartyService() {
        return seamlessTravelPartyService;
    }

    public void setSeamlessTravelPartyService(SeamlessTravelPartyService seamlessTravelPartyService) {
        this.seamlessTravelPartyService = seamlessTravelPartyService;
    }

    public PnrCollectionService getPnrCollectionService() {
        return pnrCollectionService;
    }

    public void setPnrCollectionService(PnrCollectionService pnrCollectionService) {
        this.pnrCollectionService = pnrCollectionService;
    }

    public CheckInSetUpConfigFactoryWrapper getSetUpConfigFactory() {
        return setUpConfigFactory;
    }

    public void setSetUpConfigFactory(CheckInSetUpConfigFactoryWrapper setUpConfigFactory) {
        this.setUpConfigFactory = setUpConfigFactory;
    }

    public ReadProperties getProp() {
        return prop;
    }

    public void setProp(ReadProperties prop) {
        this.prop = prop;
    }
}
