package com.am.checkin.web.v2.model;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UpgradeKnowledge implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Gson GSON = new GsonBuilder().disableHtmlEscaping()
            .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
            // .setPrettyPrinting()
            .serializeNulls()
            // .setExclusionStrategies(new ExcludeNullWarning())
            .excludeFieldsWithoutExposeAnnotation()
            // .registerTypeAdapter(WarningCollection.class, new
            // WarningCollectionAdapter())
            // .registerTypeAdapter(CheckinConfirmation.class, new
            // CheckinConfirmationJsonAdapter())
            .create();

    @SerializedName("nameRef")
    @Expose
    private String nameRef;

    @SerializedName("passengerId")
    @Expose
    private String passengerId;

    @SerializedName("fact")
    @Expose
    private Fact fact = new Fact();

    @SerializedName("result")
    @Expose
    private Result result = new Result();

    public String getNameRef() {
        return nameRef;
    }

    public void setNameRef(String nameRef) {
        this.nameRef = nameRef;
    }

    public String getPassengerId() {
        return passengerId;
    }

    public void setPassengerId(String passengerId) {
        this.passengerId = passengerId;
    }

    public Fact getFact() {
        if (null == fact) {
            fact = new Fact();
        }
        return fact;
    }

    public void setFact(Fact fact) {
        this.fact = fact;
    }

    public Result getResult() {
        if (null == result) {
            result = new Result();
        }
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public class Fact implements Serializable {

        private static final long serialVersionUID = 1L;

        @SerializedName("tierLevel")
        @Expose
        private String tierLevel;

        @SerializedName("classOfService")
        @Expose
        private String classOfService;

        @SerializedName("origin")
        @Expose
        private String origin;

        @SerializedName("destination")
        @Expose
        private String destination;

        @SerializedName("priorityCode")
        @Expose
        private String priorityCode;

        public String getTierLevel() {
            return tierLevel;
        }

        public void setTierLevel(String tierLevel) {
            this.tierLevel = tierLevel;
        }

        public String getClassOfService() {
            return classOfService;
        }

        public void setClassOfService(String classOfService) {
            this.classOfService = classOfService;
        }

        public String getOrigin() {
            return origin;
        }

        public void setOrigin(String origin) {
            this.origin = origin;
        }

        public String getDestination() {
            return destination;
        }

        public void setDestination(String destination) {
            this.destination = destination;
        }

        public String getPriorityCode() {
            return priorityCode;
        }

        public void setPriorityCode(String priorityCode) {
            this.priorityCode = priorityCode;
        }

        @Override
        public String toString() {
            return GSON.toJson(this);
        }
    }

    public class Result implements Serializable {

        private static final long serialVersionUID = 1L;

        @SerializedName("applyForUpgrade")
        @Expose
        private boolean applyForUpgrade;

        public boolean isApplyForUpgrade() {
            return applyForUpgrade;
        }

        public void setApplyForUpgrade(boolean applyForUpgrade) {
            this.applyForUpgrade = applyForUpgrade;
        }

        @Override
        public String toString() {
            return GSON.toJson(this);
        }
    }

    @Override
    public String toString() {
        return GSON.toJson(this);
    }
}
