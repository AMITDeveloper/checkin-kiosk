package com.am.checkin.web.v2.controller;

import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.SelectPassengersRq;
import com.am.checkin.web.v2.model.SelectRq;
import com.am.checkin.web.v2.services.PassengersSelectionService;
import com.am.checkin.web.v2.util.SeamlessErrorMapper;
import com.am.checkin.web.v2.util.TravelPartyErrorUtil;
import com.am.seamless.checkin.common.models.errorhandling.SeamlessError;
import com.am.seamless.checkin.common.models.exception.SeamlessException;
import com.am.seamless.checkin.common.models.types.ErrorType;
import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/selectpassengers")
public class SelectPassengerController {

    private static final Logger LOG = LoggerFactory.getLogger(SelectPassengerController.class);

    @Inject
    private PassengersSelectionService passengersSelectionService;

    @PUT
    @Path("/{cartId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response putShoppingCart(
            @Valid @NotNull SelectRq selectPassengersRq,
            @PathParam("cartId")
            @NotNull(message = "{cartId.notnull}")
            @NotEmpty(message = "{cartId.notempty}")
                    String cartId,
            @QueryParam("store")
            @NotNull(message = "{store.notnull}")
            @NotEmpty(message = "{store.notempty}")
                    String store,
            @QueryParam("pos")
            @NotNull(message = "{pos.notnull}")
            @NotEmpty(message = "{pos.notempty}")
                    String pos,
            @QueryParam("language")
                    String language
    ) throws Exception {
        
        try {

            LOG.info("SEAMLES_ENPOINT {}", "/selectpassengers");

            if (null != selectPassengersRq.getError() && !selectPassengersRq.getError().isEmpty()) {

                ErrorType errorType;

                SeamlessErrorMapper.Error error = SeamlessErrorMapper.Error.fromWebCode(selectPassengersRq.getError());

                if (null == error) {
                    try {
                        error = SeamlessErrorMapper.Error.fromCode(selectPassengersRq.getError());
                    } catch (IllegalArgumentException ex) {
                        //
                    }
                }

                if (null != error) {
                    errorType = ErrorType.getType(error.getErrorCode());
                    if (null == errorType) {
                        errorType = ErrorType.SDS_TRAVEL_PARTY_INTERNAL_ERROR;
                    }
                } else {
                    errorType = ErrorType.SDS_TRAVEL_PARTY_INTERNAL_ERROR;
                }

                SeamlessError seamlessError = TravelPartyErrorUtil.getSeamlessError(errorType, null);

                throw new SeamlessException(seamlessError.toString(), 500);
            }


            CartPNR cartPNR = passengersSelectionService.selectPassengers(selectPassengersRq, cartId, store, pos, language);

            return Response.status(Response.Status.OK).entity(cartPNR).build();
        } catch (Throwable ex) {
            LOG.error(ex.getMessage(), ex);
            throw ex;
        }
    }
}
