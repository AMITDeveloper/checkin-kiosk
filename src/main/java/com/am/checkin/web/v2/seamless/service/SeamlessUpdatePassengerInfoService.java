package com.am.checkin.web.v2.seamless.service;

import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.CartPNRUpdate;
import com.aeromexico.commons.model.rq.ShoppingCartRQ;
import com.aeromexico.commons.web.util.ValidateCheckinIneligibleReason;
import com.am.checkin.web.v2.seamless.transformers.TransformFromDS;
import com.am.checkin.web.v2.seamless.transformers.TransformToDS;
import com.am.checkin.web.v2.seamless.transformers.TransformerUtil;
import com.am.checkin.web.v2.services.ReservationUtilService;
import com.am.checkin.web.v2.util.SystemVariablesUtil;
import com.am.seamless.checkin.common.models.Passenger;
import com.am.seamless.checkin.common.models.TravelParty;
import com.am.seamless.checkin.common.models.request.PassengerInfoRq;
import com.am.seamless.checkin.common.models.types.AdvancePassengerInformationRequirementCodeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Optional;
import java.util.Set;

@Named
@ApplicationScoped
public class SeamlessUpdatePassengerInfoService {

    private static final Logger LOG = LoggerFactory.getLogger(SeamlessUpdatePassengerInfoService.class);

    @Inject
    private ReservationUtilService reservationUtilService;

    @Inject
    private SeamlessCheckinProxyService seamlessCheckinProxyService;

    public void updatePassengerInfoInfo(ShoppingCartRQ shoppingCartRQ, CartPNR cartPNR, CartPNRUpdate cartPNRUpdate, BookedLeg bookedLeg) throws Exception {

        String travelPartyId = cartPNR.getMeta().getTravelPartyId();
        String transactionId = cartPNR.getMeta().getTransactionId();

        //change country codes to three letters as digital spine needs
        for (BookedTraveler bookedTraveler : cartPNRUpdate.getTravelerInfo().getCollection()) {
            reservationUtilService.changeCountryCodesToIso3(bookedTraveler);
        }

        for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
            reservationUtilService.changeCountryCodesToIso3(bookedTraveler);
        }

        PassengerInfoRq passengerInfoRq = TransformToDS.getPassengerInfoRq(shoppingCartRQ, cartPNR, bookedLeg, cartPNRUpdate);

        if (null != passengerInfoRq.getPassengerInformation().getSelectedPassengers()
                && !passengerInfoRq.getPassengerInformation().getSelectedPassengers().isEmpty()) {

            TravelParty travelPartyUpdated = seamlessCheckinProxyService.updatePassengerInfo(passengerInfoRq, travelPartyId, transactionId);
            LOG.info("Travel Party: {}", travelPartyUpdated.toString());

            mapTravelParty(cartPNR, travelPartyUpdated);

            //change country codes to two letters as am api needs
            for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
                reservationUtilService.changeCountryCodesToIso2(bookedTraveler);
            }

            for (BookedTraveler bookedTraveler : cartPNRUpdate.getTravelerInfo().getCollection()) {
                reservationUtilService.changeCountryCodesToIso2(bookedTraveler);
            }
        }
    }

    public void mapTravelParty(CartPNR cartPNR, TravelParty travelPartyUpdated) {

        TransformFromDS.setNotifications(travelPartyUpdated, cartPNR);
        TransformFromDS.setMandates(travelPartyUpdated, cartPNR);
        TransformFromDS.setLinks(travelPartyUpdated, cartPNR);

        for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
            Optional<Passenger> optAdt = TransformerUtil.getPassenger(bookedTraveler, travelPartyUpdated);

            Passenger infant = null;
            Passenger passenger = null;

            if (optAdt.isPresent()) {
                passenger = optAdt.get();
            }

            if (null != bookedTraveler.getInfant()) {

                if (null != passenger.getInfant()) {
                    infant = passenger.getInfant();
                } else {
                    Optional<Passenger> optInf = TransformerUtil.getPassenger(bookedTraveler.getInfant(), travelPartyUpdated);

                    if (optInf.isPresent()) {
                        infant = optInf.get();
                    }
                }
            }

            TransformFromDS.resetMissings(bookedTraveler);

            //map all infant
            if (null != infant) {
                bookedTraveler.getInfant().setId(infant.getId());
                bookedTraveler.getInfant().setDateOfBirth(infant.getBirthDate());

                Set<AdvancePassengerInformationRequirementCodeType> advancePassengerInformationRequirementList = TransformFromDS.setAdvancePassengerInformationRequirements(bookedTraveler, infant, true);
                boolean passportRequired = TransformFromDS.setPrimaryDocumentsRequirement(bookedTraveler, infant, true);
                boolean visaRequired = TransformFromDS.setSecondaryDocumentsRequirement(bookedTraveler, infant, true);

                TransformFromDS.setGender(bookedTraveler, infant, true);
                TransformFromDS.setDob(bookedTraveler, infant, true);
                TransformFromDS.setDocuments(bookedTraveler, infant, true);
                TransformFromDS.setDestinationAddress(bookedTraveler, infant, true);
                TransformFromDS.setCountryOfResidence(bookedTraveler, infant, true);
                TransformFromDS.setOnwardTravelDate(bookedTraveler, infant, true);

                if (SystemVariablesUtil.isForceVisaForSeamlessEnabled()
                        && null != bookedTraveler.getInfant().getTravelDocument()
                        && ("MX".equalsIgnoreCase(bookedTraveler.getInfant().getTravelDocument().getIssuingCountry())
                        || "MEX".equalsIgnoreCase(bookedTraveler.getInfant().getTravelDocument().getIssuingCountry()))
                        && null != advancePassengerInformationRequirementList
                        && !advancePassengerInformationRequirementList.isEmpty()
                        && advancePassengerInformationRequirementList.contains(AdvancePassengerInformationRequirementCodeType.DESTINATION_ADDRESS)
                        && !visaRequired
                        && !passportRequired) {

                    bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("INFANT_VISA"));
                } else if (SystemVariablesUtil.isForceVisaForSeamlessEnabled()) {
                    LOG.info("Condition not satisfied for infant");
                }
            }

            //map all adult/child
            if (null != passenger) {
                bookedTraveler.setId(passenger.getId());
                bookedTraveler.setDateOfBirth(passenger.getBirthDate());

                Set<AdvancePassengerInformationRequirementCodeType> advancePassengerInformationRequirementList = TransformFromDS.setAdvancePassengerInformationRequirements(bookedTraveler, passenger, false);
                boolean passportRequired = TransformFromDS.setPrimaryDocumentsRequirement(bookedTraveler, passenger, false);
                boolean visaRequired = TransformFromDS.setSecondaryDocumentsRequirement(bookedTraveler, passenger, false);

                TransformFromDS.setGender(bookedTraveler, passenger, false);
                TransformFromDS.setDob(bookedTraveler, passenger, false);
                TransformFromDS.setDocuments(bookedTraveler, passenger, false);
                TransformFromDS.setDestinationAddress(bookedTraveler, passenger, false);
                TransformFromDS.setCountryOfResidence(bookedTraveler, passenger, false);
                TransformFromDS.setOnwardTravelDate(bookedTraveler, passenger, false);

                if (SystemVariablesUtil.isForceVisaForSeamlessEnabled()
                        && null != bookedTraveler.getTravelDocument()
                        && ("MX".equalsIgnoreCase(bookedTraveler.getTravelDocument().getIssuingCountry())
                        || "MEX".equalsIgnoreCase(bookedTraveler.getTravelDocument().getIssuingCountry()))
                        && null != advancePassengerInformationRequirementList
                        && !advancePassengerInformationRequirementList.isEmpty()
                        && advancePassengerInformationRequirementList.contains(AdvancePassengerInformationRequirementCodeType.DESTINATION_ADDRESS)
                        && !visaRequired
                        && !passportRequired) {

                    bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("VISA"));
                } else if (SystemVariablesUtil.isForceVisaForSeamlessEnabled()) {
                    LOG.info("Condition not satisfied for adult");
                }
            }
        }
    }

}
