package com.am.checkin.web.v2.services;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.PNR;
import com.aeromexico.commons.model.PNRCollection;
import com.aeromexico.commons.model.SelectPassengersRq;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.types.PosType;
import com.aeromexico.commons.web.util.Constants;
import com.aeromexico.commons.web.util.FilterPassengersUtil;
import com.aeromexico.dao.client.exception.MongoDisconnectException;
import com.aeromexico.dao.services.IPNRLookupDao;
import com.aeromexico.dao.util.CommonUtil;
import com.am.checkin.web.service.PnrCollectionService;
import com.am.checkin.web.v2.seamless.service.SeamlessPassengerSelectionService;
import org.aeromexico.commons.exception.config.ReadProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@Named
@ApplicationScoped
public class PassengersSelectionService {

    private static final Logger LOG = LoggerFactory.getLogger(PassengersSelectionService.class);

    @Inject
    private PnrCollectionService updatePnrCollectionService;

    @Inject
    private IPNRLookupDao pnrLookupDao;

    @Inject
    private SeamlessPassengerSelectionService seamlessPassengerSelectionService;

    @Inject
    private ReadProperties prop;

    @Inject
    private CovidRestrictionStorageService covidRestrictionStorageService;


    public CartPNR selectPassengers(
            SelectPassengersRq selectPassengersRq,
            String cartId,
            String store,
            String pos,
            String language
    ) throws Exception {
        LOG.info("**put Select Passengers: {}", selectPassengersRq);

        // First read the pnrCollection from the database
        PNRCollection pnrCollection = null;
        try {
            pnrCollection = pnrLookupDao.readPNRCollectionByCartId(cartId);
        } catch (MongoDisconnectException me) {
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(pos)) {
                LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
                throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getCode(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getActionable(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getMessage(),
                        Response.Status.INTERNAL_SERVER_ERROR);
            } else {
                LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
                throw me;
            }
        }

        // If the pnrCollection is null, throw an expired session exception
        if (null == pnrCollection || null == pnrCollection.getCollection() || pnrCollection.getCollection().isEmpty()) {
            LOG.error(CommonUtil.getMessageByCode(Constants.CODE_004) + "pnrCollection is null "
                    + cartId);
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.EXPIRED_SESSION,
                    CommonUtil.getMessageByCode(Constants.CODE_004));
        }

        //use the first value sent on pnrLookUp
        if (null != pnrCollection.getStore() && !pnrCollection.getStore().trim().isEmpty()) {
            store = pnrCollection.getStore();
        }

        //use the first value sent on pnrLookUp
        if (null != pnrCollection.getPos() && !pnrCollection.getPos().trim().isEmpty()) {
            pos = pnrCollection.getPos();
        }

        PNR pnr = pnrCollection.getPNRByCartId(cartId);

        if (null == pnr) {
            LOG.error(CommonUtil
                    .getMessageByCode(ErrorType.PNR_OBJECT_EMPTY.getFullDescription() + cartId));
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PNR_OBJECT_EMPTY,
                    ErrorType.PNR_OBJECT_EMPTY.getFullDescription());
        }

        CartPNR cartPNR = pnrCollection.getCartPNRByCartId(cartId);

        if (null == cartPNR) {
            LOG.error(CommonUtil.getMessageByCode(
                    ErrorType.CART_COLLECTION_OBJECT_EMPTY.getFullDescription() + cartId));
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.CART_COLLECTION_OBJECT_EMPTY,
                    ErrorType.CART_COLLECTION_OBJECT_EMPTY.getErrorCode());
        }

        BookedLeg bookedLeg = pnr.getLegs().getBookedLegByLegCode(cartPNR.getLegCode());

        if (null == bookedLeg) {
            LOG.error(CommonUtil.getMessageByCode(
                    ErrorType.LEG_NOT_FOUND_IN_COLLECTION.getFullDescription() + cartId));
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.LEG_NOT_FOUND_IN_COLLECTION,
                    ErrorType.LEG_NOT_FOUND_IN_COLLECTION.getErrorCode());
        }

        selectPassengers(selectPassengersRq, cartPNR);

        covidRestrictionStorageService.saveCovidRestrictionForm(pnr.getPnr(), cartPNR, bookedLeg);

        updatePnrCollectionService.updatePnrCollection(pnrCollection, false);

        return cartPNR;
    }

    public void selectPassengers(
            SelectPassengersRq selectPassengersRq,
            CartPNR cartPNR
    ) throws Exception {
        List<SelectPassengersRq.BookedTravelerUpdate> bookedTravelerUpdateList = selectPassengersRq.getTravelerInfo().getCollection();

        cartPNR.getMeta().setPassengersSelected(true);
        markPassengersAsSelected(cartPNR, bookedTravelerUpdateList);

        boolean selectionChanged = removeNonSelectedPassengers(cartPNR);

        if (!selectionChanged && cartPNR.isSeamlessCheckin()) {
            seamlessPassengerSelectionService.selectPassengers(cartPNR);
        }
    }

    public boolean removeNonSelectedPassengers(CartPNR cartPNR) {
        Iterator<BookedTraveler> it = cartPNR.getTravelerInfo().getCollection().iterator();

        boolean selectionChanged = true;

        //exclude all passengers non selected for checkin
        while (it.hasNext()) {
            BookedTraveler bookedTraveler = (BookedTraveler) it.next();

            selectionChanged &= bookedTraveler.isSelectedToCheckin();

            if (!bookedTraveler.isSelectedToCheckin()) {
                it.remove();
            }
        }

        return selectionChanged;
    }

    public void markPassengersAsSelected(
            CartPNR cartPNR,
            List<SelectPassengersRq.BookedTravelerUpdate> bookedTravelerUpdateList
    ) throws Exception {
        //unselect all
        for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
            bookedTraveler.setIsSelectedToCheckin(false);
        }

        Boolean selectCheckedIn = null;

        //mark passengers as selected
        for (SelectPassengersRq.BookedTravelerUpdate bookedTravelerUpdate : bookedTravelerUpdateList) {

            if (bookedTravelerUpdate.isSelectedToCheckin()) {

                LOG.info("**BookedTravelerUpdate: {}", bookedTravelerUpdate);

                // Find bookedTraveler in DB
                Optional<BookedTraveler> bookedTravelerOptional = FilterPassengersUtil.getBookedTravelerById(
                        cartPNR.getTravelerInfo().getCollection(),
                        bookedTravelerUpdate.getId()
                );

                if (!bookedTravelerOptional.isPresent()) {
                    LOG.error(ErrorType.TRAVELER_ID_NOT_MATCH.getFullDescription() + " PASSENGER ID : "
                            + bookedTravelerUpdate.getId());
                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.TRAVELER_ID_NOT_MATCH,
                            ErrorType.TRAVELER_ID_NOT_MATCH.getFullDescription() + bookedTravelerUpdate.getId());
                }

                BookedTraveler bookedTraveler = bookedTravelerOptional.get();

                LOG.info("**bookedTraveler: {}", bookedTraveler);

                if (null == selectCheckedIn) {
                    selectCheckedIn = bookedTraveler.isCheckinStatus();
                }

                //to mark only not checked in or only already checked in
                if (selectCheckedIn.equals(Boolean.valueOf(bookedTraveler.isCheckinStatus()))) {
                    bookedTraveler.setIsSelectedToCheckin(true);

                    bookedTraveler.setCovidRestrictionConfirmed(bookedTravelerUpdate.isCovidRestrictionConfirmed());
                }
            }
        }
    }
}
