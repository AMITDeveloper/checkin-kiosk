package com.am.checkin.web.v2.seamless.service;

import com.aeromexico.commons.model.AuthorizationPaymentConfig;
import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.BookedTravelerAncillaryCollection;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.Infant;
import com.aeromexico.commons.model.POS;
import com.aeromexico.commons.model.SSRRemarks;
import com.aeromexico.commons.model.rq.PnrRQ;
import com.aeromexico.commons.web.types.BookedLegCheckinStatusType;
import com.aeromexico.commons.web.types.BookedLegFlightStatusType;
import com.aeromexico.commons.web.types.CheckinIneligibleReasons;
import com.aeromexico.commons.web.types.PaxType;
import com.aeromexico.commons.web.types.WindowTimeType;
import com.aeromexico.commons.web.util.ListsUtil;
import com.aeromexico.commons.web.util.UuidUtil;
import com.aeromexico.commons.web.util.ValidateCheckinIneligibleReason;
import com.aeromexico.dao.util.CommonUtil;
import com.aeromexico.sharedservices.services.BaggageAllowanceService;
import com.aeromexico.sharedservices.util.PnrUtil;
import com.am.checkin.web.service.CheckInSetUpConfigFactoryWrapper;
import com.am.checkin.web.util.PNRLookUpServiceUtil;
import com.am.checkin.web.v2.seamless.model.TravelPartyWrapper;
import com.am.checkin.web.v2.seamless.transformers.TransformFromDS;
import com.am.checkin.web.v2.seamless.transformers.TransformToDS;
import com.am.checkin.web.v2.seamless.transformers.TransformerUtil;
import com.am.checkin.web.v2.services.ItineraryService;
import com.am.checkin.web.v2.services.ReservationUtilService;
import com.am.checkin.web.v2.util.PassengerAncillaryUtil;
import com.am.checkin.web.v2.util.SystemVariablesUtil;
import com.am.checkin.web.v2.util.TravelPartyErrorUtil;
import com.am.seamless.checkin.common.models.HandlingRestriction;
import com.am.seamless.checkin.common.models.Passenger;
import com.am.seamless.checkin.common.models.TravelParty;
import com.am.seamless.checkin.common.models.Trip;
import com.am.seamless.checkin.common.models.request.TravelPartyRq;
import com.am.seamless.checkin.common.models.types.AdvancePassengerInformationRequirementCodeType;
import com.am.seamless.checkin.common.models.types.HandlingRestrictionCodeType;
import com.google.gson.Gson;
import com.sabre.ns.ticketing.dc.GetTicketingDocumentRS;
import com.sabre.services.res.or.getreservation.NameAssociationTag;
import com.sabre.services.res.or.getreservation.OpenReservationElementType;
import com.sabre.services.res.or.getreservation.ServiceRequestType;
import com.sabre.webservices.pnrbuilder.getreservation.FrequentFlyerPNRB;
import com.sabre.webservices.pnrbuilder.getreservation.GetReservationRS;
import com.sabre.webservices.pnrbuilder.getreservation.PassengerPNRB;
import com.sabre.webservices.pnrbuilder.getreservation.PhoneNumbersPNRB;
import com.sabre.webservices.pnrbuilder.getreservation.RemarksPNRB;
import com.sabre.webservices.pnrbuilder.getreservation.WheelchairRequestPNRB;
import com.sabre.webservices.pnrbuilder.updatereservation.ReservationUpdateItemType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Named
@ApplicationScoped
public class SeamlessTravelPartyService {

    private static final Logger LOG = LoggerFactory.getLogger(SeamlessTravelPartyService.class);

    @Inject
    @Named("CheckInSetUpConfigFactoryWrapper")
    private CheckInSetUpConfigFactoryWrapper setUpConfigFactory;

    @Inject
    private ItineraryService itineraryService;

    @Inject
    private ReservationUtilService reservationUtilService;

    @Inject
    private SeamlessCheckinProxyService seamlessCheckinProxyService;

    private static final long CART_EXPIRATION_SECONDS = 1800;

    private CartPNR initializeCart(PnrRQ pnrRQ) {
        CartPNR cartPNR = new CartPNR();
        cartPNR.getMeta().setCartId(CommonUtil.createCartId());
        cartPNR.getMeta().setCartExpirationTimeInSeconds(CART_EXPIRATION_SECONDS);

        POS pos = new POS();
        pos.setChannel(pnrRQ.getPos());
        pos.setStore(pnrRQ.getStore());
        pos.setLanguage(pnrRQ.getLanguage());

        cartPNR.getMeta().setPos(pos);

        return cartPNR;
    }

    public CartPNR getCarts(
            PnrRQ pnrRQ,
            BookedLeg bookedLeg,
            GetReservationRS getReservationRS,
            GetTicketingDocumentRS ticketingDocumentRS,
            List<ReservationUpdateItemType> reservationUpdateItemTypeList,
            List<ReservationUpdateItemType> reservationUpdateSeatItemTypeList
    ) throws Exception {
        CartPNR cartPNR = initializeCart(pnrRQ);
        cartPNR.setLegCode(bookedLeg.getSegments().getLegCode());
        cartPNR.setSeamlessCheckin(true);

        setBasicInformationFromReservation(
                pnrRQ,
                cartPNR,
                bookedLeg,
                getReservationRS,
                ticketingDocumentRS,
                reservationUpdateItemTypeList,
                reservationUpdateSeatItemTypeList
        );

        TravelPartyRq travelPartyRq = TransformToDS.getTravelPartyRq(pnrRQ, cartPNR, bookedLeg);
        String operatingHandler = bookedLeg.getOperatingHandler();
        TravelPartyWrapper travelPartyWrapper = seamlessCheckinProxyService.getTravelParty(travelPartyRq, operatingHandler);
        LOG.info("Travel Party Wrapper: {}", travelPartyWrapper.toString());

        mapTravelParty(cartPNR, bookedLeg, travelPartyWrapper.getTravelParty(), travelPartyWrapper.getTransactionId());

        return cartPNR;
    }

    private void setBasicInformationFromReservation(
            PnrRQ pnrRQ,
            CartPNR cartPNR,
            BookedLeg bookedLeg,
            GetReservationRS getReservationRS,
            GetTicketingDocumentRS ticketingDocumentRS,
            List<ReservationUpdateItemType> reservationUpdateItemTypeList,
            List<ReservationUpdateItemType> reservationUpdateSeatItemTypeList
    ) throws Exception {

        AuthorizationPaymentConfig authorizationPaymentConfig = getSetUpConfigFactory().getInstance().getAuthorizationPaymentConfig(pnrRQ.getStore(), pnrRQ.getPos());
        String currencyCode = authorizationPaymentConfig.getCurrency();

        List<FrequentFlyerPNRB> listCustLoyalty;
        listCustLoyalty = PNRLookUpServiceUtil.getCustLoyalty(getReservationRS);

        PhoneNumbersPNRB phoneNumbers;
        phoneNumbers = getReservationRS.getReservation().getPhoneNumbers();

        List<PassengerPNRB> passengerDetails = PNRLookUpServiceUtil.getPassengerDetails(getReservationRS);

        RemarksPNRB remarkInfo ;
        remarkInfo = getReservationRS.getReservation().getRemarks();

        List<OpenReservationElementType> specialServiceInfo;
        specialServiceInfo = PNRLookUpServiceUtil.getSSRInformation(getReservationRS);

        String[] corporateResults;
        corporateResults = PNRLookUpServiceUtil.getCorporateNumbers(getReservationRS);

        List<Infant> infantList = new ArrayList<>();

        for (PassengerPNRB passengerPNRB : getReservationRS.getReservation().getPassengerReservation().getPassengers().getPassenger()) {
            if ("INF".equalsIgnoreCase(passengerPNRB.getReferenceNumber())
                    || com.sabre.webservices.pnrbuilder.getreservation.PassengerTypePNRB.I.equals(passengerPNRB.getNameType())) {

                Infant infant = new Infant();
                infant.setNameRefNumber(passengerPNRB.getNameId());

                infant.setFirstName(passengerPNRB.getFirstName());
                infant.setLastName(passengerPNRB.getLastName());


                infantList.add(infant);
            } else {

                BookedTraveler bookedTraveler = new BookedTraveler();
                bookedTraveler.setNameRefNumber(passengerPNRB.getNameId());

                PaxType paxType = PNRLookUpServiceUtil.getPaxType(bookedTraveler.getNameRefNumber(), passengerDetails);
                bookedTraveler.setPaxType((paxType == null) ? PaxType.ADULT : paxType);

                bookedTraveler.setFirstName(passengerPNRB.getFirstName());
                bookedTraveler.setLastName(passengerPNRB.getLastName());
                bookedTraveler.setDisplayName(
                        CommonUtil.removePrefix(passengerPNRB.getFirstName()) + " " + passengerPNRB.getLastName()
                );

                if (null != passengerPNRB.isWithInfant() && passengerPNRB.isWithInfant().booleanValue()) {
                    bookedTraveler.setInfant(new Infant());
                }

                PnrUtil.setAllTicketNumbers(bookedTraveler, getReservationRS);

                PNRLookUpServiceUtil.addFrequentFlyerIfNotExistsInPaxData(bookedTraveler, listCustLoyalty);

                if (bookedTraveler.isEligibleToCheckin()) {
                    bookedTraveler.setIsSelectedToCheckin(true);
                }

                String departureCity = (ListsUtil.getFirst(bookedLeg.getSegments().getCollection())).getSegment().getDepartureAirport();
                String arrivalCity = (ListsUtil.getLast(bookedLeg.getSegments().getCollection())).getSegment().getArrivalAirport();
                String pnrPassId = pnrRQ.getRecordLocator() + "_" + departureCity + "_" + arrivalCity + "_" + bookedTraveler.getNameRefNumber();
                bookedTraveler.setPnrPassId(pnrPassId);

                bookedTraveler.setSavedEmails(
                        PNRLookUpServiceUtil.getEmailsPerPassenger(
                                bookedTraveler.getNameRefNumber(),
                                passengerDetails
                        )
                );

                bookedTraveler.getPhones().setCollection(
                        PNRLookUpServiceUtil.getPhonesPerPassenger(
                                bookedTraveler.getNameRefNumber(),
                                phoneNumbers
                        )
                );

                bookedTraveler.setShowEmailCapture(
                        PNRLookUpServiceUtil.showCaptureEmailRemark(
                                bookedTraveler.getNameRefNumber(),
                                remarkInfo
                        )
                );

                bookedTraveler.setCorporateFrequentFlyerNumber(corporateResults[0]);
                bookedTraveler.setCorporateFrequentFlyerProgram(corporateResults[1]);

                if (null != passengerPNRB.getAncillaryServices() && null != passengerPNRB.getAncillaryServices().getAncillaryService()) {

                    List<com.sabre.webservices.pnrbuilder.getreservation.AncillaryServicesPNRB> ancillariesListForThisLeg = PNRLookUpServiceUtil.getAncillariesForLeg(
                            passengerPNRB.getAncillaryServices().getAncillaryService(),
                            bookedLeg
                    );

                    LOG.info("ancillariesListForThisLeg {}", ancillariesListForThisLeg);

                    BookedTravelerAncillaryCollection bookedTravelerAncillaryCollection = PassengerAncillaryUtil.getAncillariesFromGetReservation(
                            ancillariesListForThisLeg,
                            bookedTraveler,
                            reservationUpdateItemTypeList,
                            bookedLeg.getSegments().getLegCode()
                    );

                    LOG.info("bookedTravelerAncillaryCollection: {}", new Gson().toJson(bookedTravelerAncillaryCollection));

                    bookedTraveler.setAncillaries(
                            bookedTravelerAncillaryCollection
                    );

                    BookedTravelerAncillaryCollection seatBookedTravelerAncillaryCollection = PassengerAncillaryUtil.getSeatAncillariesFromGetReservation(
                            ancillariesListForThisLeg,
                            bookedTraveler,
                            reservationUpdateSeatItemTypeList,
                            bookedLeg.getSegments().getLegCode(),
                            currencyCode
                    );

                    LOG.info("seatBookedTravelerAncillaryCollection: {}", bookedTravelerAncillaryCollection);

                    bookedTraveler.setSeatAncillaries(
                            seatBookedTravelerAncillaryCollection
                    );
                }

                //this method set eligibility on traveler
                setEligibilityForCheckin(
                        bookedTraveler,
                        specialServiceInfo,
                        bookedLeg,
                        pnrRQ.getPos()
                );

                setSsr(bookedTraveler, passengerPNRB);

                setBaggageAllowance(bookedTraveler, bookedLeg);

                PNRLookUpServiceUtil.addExtraBaggageForInfant(bookedLeg, bookedTraveler);

                // Require passport to be entered or re-entered (adult and infant) for all international flights even if it was already entered.
                PNRLookUpServiceUtil.setRequiredTravelDocs(bookedTraveler, bookedLeg);

                cartPNR.getTravelerInfo().getCollection().add(bookedTraveler);
            }
        }

        //sort passengers to be ordered by nameRef number
        Collections.sort(cartPNR.getTravelerInfo().getCollection(), (BookedTraveler p1, BookedTraveler p2) -> {
            if (null != p1.getNameRefNumber() && null != p2.getNameRefNumber()) {
                return p1.getNameRefNumber().compareTo(p2.getNameRefNumber());
            } else if (null == p1.getNameRefNumber() && null == p2.getNameRefNumber()) {
                return 0;
            } else if (null == p1.getNameRefNumber()) {
                return 1;
            } else {// if (null == p2.getNameRefNumber()) {
                return -1;
            }
        });

        LOG.info("cartPNR: {}", cartPNR);
        LOG.info("infantList: {}", infantList);

        int infanIndex = 0;
        for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
            if (null != bookedTraveler.getInfant()) {
                if (infanIndex < infantList.size()) {
                    bookedTraveler.setInfant(infantList.get(infanIndex));

                    infanIndex++;
                } else { // no more infants
                    bookedTraveler.setInfant(null);
                }
            }
        }

        PnrUtil.setTicketNumbersFromDocumentService(cartPNR, bookedLeg, ticketingDocumentRS);
        PnrUtil.setTicketNumbers(cartPNR, bookedLeg, ticketingDocumentRS);

        for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
            LOG.info(bookedTraveler.toString());
            validateMissingTickets(bookedTraveler);
        }
    }

    public static void setSsr(BookedTraveler bookedTraveler, PassengerPNRB passengerPNRB) {
        if (null != passengerPNRB.getSpecialRequests()) {
            if (null != passengerPNRB.getSpecialRequests().getWheelchairRequest()) {
                for (WheelchairRequestPNRB wheelchairRequestPNRB : passengerPNRB.getSpecialRequests().getWheelchairRequest()) {
                    if (null != wheelchairRequestPNRB.getWheelchairCode()) {
                        if (null == bookedTraveler.getSsrRemarks()) {
                            bookedTraveler.setSsrRemarks(new SSRRemarks());
                        }

                        bookedTraveler.getSsrRemarks().getSsrCodes().add(wheelchairRequestPNRB.getWheelchairCode().value());

                        break;
                    }
                }
            }
        }
    }

    public void mapTravelParty(CartPNR cartPNR, BookedLeg bookedLeg, TravelParty travelParty, String transactionId) {

        //set cartId
        cartPNR.getMeta().setCartId(travelParty.getId());
        cartPNR.getMeta().setTransactionId(transactionId);
        cartPNR.getMeta().setTravelPartyId(travelParty.getId());

        TransformFromDS.setNotifications(travelParty, cartPNR);
        TransformFromDS.setMandates(travelParty, cartPNR);
        TransformFromDS.setLinks(travelParty, cartPNR);

        for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {

            boolean hasSelecteeError = TravelPartyErrorUtil.hasSelecteeError(travelParty);

            LOG.info("firstname {}", bookedTraveler.getFirstName());
            LOG.info("lastname {}", bookedTraveler.getLastName());
            LOG.info("passengertype {}", bookedTraveler.getPaxType());
            LOG.info("tickets {}", bookedTraveler.getTicketNumbers());
            LOG.info("ticket {}", bookedTraveler.getTicketNumber());

            Optional<Passenger> optAdt = TransformerUtil.getPassenger(bookedTraveler, travelParty);

            Passenger infant = null;
            Passenger passenger = null;

            if (optAdt.isPresent()) {
                passenger = optAdt.get();

                if (null != bookedTraveler.getInfant()) {

                    if (null != passenger.getInfant()) {
                        infant = passenger.getInfant();
                    } else {
                        Optional<Passenger> optInf = TransformerUtil.getPassenger(bookedTraveler.getInfant(), travelParty);

                        if (optInf.isPresent()) {
                            infant = optInf.get();
                        }
                    }
                }
            }

            TransformFromDS.resetMissings(bookedTraveler);

            boolean visaRequiredForAdult = false;

            //map all adult/child
            if (null != passenger) {
                bookedTraveler.setId(passenger.getId());
                bookedTraveler.setFirstName(passenger.getGivenNames());
                bookedTraveler.setLastName(passenger.getSurname());
                bookedTraveler.setDateOfBirth(passenger.getBirthDate());

                HandlingRestriction handlingRestriction = new HandlingRestriction();
                handlingRestriction.setType(HandlingRestrictionCodeType.CHECKIN);

                if (hasSelecteeError
                        && null != passenger.getHandlingRestrictions()
                        && !passenger.getHandlingRestrictions().isEmpty()
                        && passenger.getHandlingRestrictions().contains(handlingRestriction)
                ) {
                    bookedTraveler.setIsEligibleToCheckin(false);
                    bookedTraveler.setIsSelectedToCheckin(false);
                    bookedTraveler.getIneligibleReasons().add(CheckinIneligibleReasons.HANDLING_RESTRICTION_CHECKIN.name());
                    bookedTraveler.setSelectee(true);
                }

                TransformFromDS.setCheckInStatus(bookedTraveler, bookedLeg, passenger, travelParty.getTrips());

                if (!bookedTraveler.isCheckinStatus()) {
                    Set<AdvancePassengerInformationRequirementCodeType> advancePassengerInformationRequirementList = TransformFromDS.setAdvancePassengerInformationRequirements(bookedTraveler, passenger, false);
                    boolean passportRequired = TransformFromDS.setPrimaryDocumentsRequirement(bookedTraveler, passenger, false);
                    visaRequiredForAdult = TransformFromDS.setSecondaryDocumentsRequirement(bookedTraveler, passenger, false);

                    if (SystemVariablesUtil.isForceVisaForSeamlessEnabled()
                            && null != bookedTraveler.getTravelDocument()
                            && ("MX".equalsIgnoreCase(bookedTraveler.getTravelDocument().getIssuingCountry())
                            || "MEX".equalsIgnoreCase(bookedTraveler.getTravelDocument().getIssuingCountry()))
                            && null != advancePassengerInformationRequirementList
                            && !advancePassengerInformationRequirementList.isEmpty()
                            && advancePassengerInformationRequirementList.contains(AdvancePassengerInformationRequirementCodeType.DESTINATION_ADDRESS)
                            && !visaRequiredForAdult
                            && !passportRequired) {

                        visaRequiredForAdult = true;

                        bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("VISA"));
                    }
                }

                TransformFromDS.setGender(bookedTraveler, passenger, false);
                TransformFromDS.setDob(bookedTraveler, passenger, false);
                TransformFromDS.setDocuments(bookedTraveler, passenger, false);
                TransformFromDS.setDestinationAddress(bookedTraveler, passenger, false);
                TransformFromDS.setCountryOfResidence(bookedTraveler, passenger, false);
                TransformFromDS.setOnwardTravelDate(bookedTraveler, passenger, false);
                TransformFromDS.mapSeats(bookedTraveler, passenger, bookedLeg, travelParty.getTrips());
            } else {
                LOG.info("Adult is not present");
                bookedTraveler.setId(UuidUtil.getId());

                bookedTraveler.setIsEligibleToCheckin(false);
                bookedTraveler.setIsSelectedToCheckin(false);
                bookedTraveler.getIneligibleReasons().add(CheckinIneligibleReasons.PASSENGER_NOT_FOUND_IN_SDS.name());
            }


            //map all infant
            if (null != bookedTraveler.getInfant()) {
                if (null != infant) {
                    bookedTraveler.getInfant().setId(infant.getId());
                    bookedTraveler.getInfant().setFirstName(infant.getGivenNames());
                    bookedTraveler.getInfant().setLastName(infant.getSurname());
                    bookedTraveler.getInfant().setDateOfBirth(infant.getBirthDate());

                    HandlingRestriction handlingRestriction = new HandlingRestriction();
                    handlingRestriction.setType(HandlingRestrictionCodeType.CHECKIN);

                    if (hasSelecteeError
                            && null != passenger.getHandlingRestrictions()
                            && !passenger.getHandlingRestrictions().isEmpty()
                            && passenger.getHandlingRestrictions().contains(handlingRestriction)
                    ) {
                        bookedTraveler.setIsEligibleToCheckin(false);
                        bookedTraveler.setIsSelectedToCheckin(false);
                        bookedTraveler.getIneligibleReasons().add(CheckinIneligibleReasons.INFANT_HANDLING_RESTRICTION_CHECKIN.name());
                        bookedTraveler.setSelectee(true);
                    }

                    TransformFromDS.setCheckInStatus(bookedTraveler.getInfant(), bookedLeg, infant, travelParty.getTrips());

                    if (!bookedTraveler.getInfant().isCheckinStatus()) {
                        Set<AdvancePassengerInformationRequirementCodeType> advancePassengerInformationRequirementList = TransformFromDS.setAdvancePassengerInformationRequirements(bookedTraveler, infant, true);
                        boolean passportRequired = TransformFromDS.setPrimaryDocumentsRequirement(bookedTraveler, infant, true);
                        boolean visaRequired = TransformFromDS.setSecondaryDocumentsRequirement(bookedTraveler, infant, true);

                        if (SystemVariablesUtil.isForceVisaForSeamlessEnabled()
                                && null != bookedTraveler.getInfant().getTravelDocument()
                                && ("MX".equalsIgnoreCase(bookedTraveler.getInfant().getTravelDocument().getIssuingCountry())
                                || "MEX".equalsIgnoreCase(bookedTraveler.getInfant().getTravelDocument().getIssuingCountry()))

                                && ((null != advancePassengerInformationRequirementList
                                && !advancePassengerInformationRequirementList.isEmpty()
                                && advancePassengerInformationRequirementList.contains(AdvancePassengerInformationRequirementCodeType.DESTINATION_ADDRESS)
                                && !visaRequired
                                && !passportRequired) || (visaRequiredForAdult && !visaRequired && !passportRequired))) {

                            bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("INFANT_VISA"));
                        }
                    }

                    TransformFromDS.setGender(bookedTraveler, infant, true);
                    TransformFromDS.setDob(bookedTraveler, infant, true);
                    TransformFromDS.setDocuments(bookedTraveler, infant, true);
                    TransformFromDS.setDestinationAddress(bookedTraveler, infant, true);
                    TransformFromDS.setCountryOfResidence(bookedTraveler, infant, true);
                    TransformFromDS.setOnwardTravelDate(bookedTraveler, infant, true);
                } else {
                    LOG.info("Infant is not present");

                    bookedTraveler.getInfant().setId(UuidUtil.getId());

                    bookedTraveler.setIsEligibleToCheckin(false);
                    bookedTraveler.setIsSelectedToCheckin(false);
                    bookedTraveler.getIneligibleReasons().add(CheckinIneligibleReasons.INFANT_PASSENGER_NOT_FOUND_IN_SDS.name());
                }
            }

            Trip trip = TransformerUtil.getTrip(bookedLeg, travelParty.getTrips());

            if (null != trip) {
                bookedLeg.getSegments().setTripId(trip.getId());
            }

            setMissingInfo(bookedLeg, bookedTraveler);

            //Validate all country codes Information.
            try {
                getReservationUtilService().changeCountryCodesToIso2(bookedTraveler);
            } catch (Exception ex) {
                LOG.error(ex.getMessage(), ex);
            }

        }
    }

    public void setMissingInfo(BookedLeg bookedLeg, BookedTraveler bookedTraveler) {
        if (!bookedTraveler.isCheckinStatus()) {
            //Double check
            if (null == bookedTraveler.getGender()) {
                bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("GENDER"));
            }

            if (!bookedLeg.isDomestic()) {
                if (null == bookedTraveler.getDateOfBirth() || bookedTraveler.getDateOfBirth().isEmpty()) {
                    bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("DOB"));
                }

                bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("DOCS"));
            }
        }

        if (null != bookedTraveler.getInfant() && !bookedTraveler.getInfant().isCheckinStatus()) {

            if (null == bookedTraveler.getInfant().getGender()) {
                bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("INFANT_GENDER"));
            }

            if (!bookedLeg.isDomestic()) {
                if (null == bookedTraveler.getInfant().getDateOfBirth() || bookedTraveler.getInfant().getDateOfBirth().isEmpty()) {
                    bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("INFANT_DOB"));
                }

                bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("DOCS/INF"));
            }
        }
    }

    private void setBaggageAllowance(
            BookedTraveler bookedTraveler,
            BookedLeg bookedLeg
    ) {

        //set bags
        bookedTraveler.setFreeBaggageAllowance(
                BaggageAllowanceService.getNoneBaggageAllowance()
        );
        bookedTraveler.setFreeBaggageAllowancePerLegsArray(
                BaggageAllowanceService.getNoneBaggageAllowancePerLegArray(bookedLeg.getSegments().getLegCode())
        );


    }

    private void setEligibilityForCheckin(
            BookedTraveler bookedTraveler,
            List<OpenReservationElementType> specialServiceInfoList,
            BookedLeg bookedLeg,
            String pos
    ) throws Exception {

        // SSR criteria
        for (OpenReservationElementType specialServiceInfo : specialServiceInfoList) {
            ServiceRequestType service = specialServiceInfo.getServiceRequest();

            if (PNRLookUpServiceUtil.isSsrNonEligibleForCheckIn(service)) {
                for (NameAssociationTag personName : specialServiceInfo.getNameAssociation()) {
                    String fullName = bookedTraveler.getLastName() + "/" + bookedTraveler.getFirstName();

                    if (fullName.equalsIgnoreCase(personName.getLastName()+"/"+personName.getFirstName())) {
                        bookedTraveler.setIsEligibleToCheckin(false);
                        bookedTraveler.setIsSelectedToCheckin(false);

                        bookedTraveler.getIneligibleReasons().add(
                                CheckinIneligibleReasons.SSR.getCheckinIneligibleReason() + "-" + PNRLookUpServiceUtil.getSsrNonEligibleForCheckIn(service)
                        );

                        return;
                    }
                }
            }
        }

        BookedSegment bookedSegment = bookedLeg.getFirstOpenSegment();
        WindowTimeType windowTime = getItineraryService().getWindowTime(bookedSegment.getSegment(), pos);
        if (WindowTimeType.EARLY_CKIN.toString().equalsIgnoreCase(windowTime.toString())) {
            bookedTraveler.getIneligibleReasons().add(CheckinIneligibleReasons.EARLY.getCheckinIneligibleReason());
            bookedTraveler.setIsEligibleToCheckin(false);
            bookedTraveler.setIsSelectedToCheckin(false);

            return;
        }


        if (BookedLegFlightStatusType.CANCELLED.equals(bookedLeg.getFlightStatus())) {
            bookedTraveler.getIneligibleReasons().add(CheckinIneligibleReasons.FLIGHTCANCELED.getCheckinIneligibleReason());
            bookedTraveler.setIsEligibleToCheckin(false);
            bookedTraveler.setIsSelectedToCheckin(false);

            return;
        }

        if (!BookedLegCheckinStatusType.OPEN.equals(bookedLeg.getCheckinStatus())
                && (!BookedLegCheckinStatusType.FINAL.equals(bookedLeg.getCheckinStatus())
                || !bookedTraveler.isCheckinStatus())) {

            if (!bookedLeg.isStandByReservation() || !SystemVariablesUtil.isEarlyCheckinEnabled()) {
                bookedTraveler.getIneligibleReasons().add(bookedLeg.getCheckinStatus().toString());
                bookedTraveler.setIsEligibleToCheckin(false);
                bookedTraveler.setIsSelectedToCheckin(false);

                return;
            }
        }

        // If Flight is Canceled
        if (null != bookedLeg && BookedLegCheckinStatusType.CANCEL.equals(bookedLeg.getCheckinStatus())) {
            bookedTraveler.getIneligibleReasons().add(CheckinIneligibleReasons.FLIGHTCANCELED.getCheckinIneligibleReason());
            bookedTraveler.setIsEligibleToCheckin(false);
            bookedTraveler.setIsSelectedToCheckin(false);

            return;
        }

    }

    private void validateMissingTickets(BookedTraveler bookedTraveler) {
        // If VCerR does not exists
        if (null == bookedTraveler.getTicketNumber() || bookedTraveler.getTicketNumber().trim().isEmpty()) {
            if (!bookedTraveler.isCheckinStatus()) {
                bookedTraveler.getIneligibleReasons().add(CheckinIneligibleReasons.VCR.getCheckinIneligibleReason());
                bookedTraveler.setIsEligibleToCheckin(false);
                bookedTraveler.setIsSelectedToCheckin(false);

                return;
            }
        }

        //Infant without ETKT
        if (null != bookedTraveler.getInfant()
                && (null == bookedTraveler.getInfant().getTicketNumber()
                || bookedTraveler.getInfant().getTicketNumber().trim().isEmpty())) {
            bookedTraveler.setIsEligibleToCheckin(false);
            bookedTraveler.setIsSelectedToCheckin(false);
            bookedTraveler.getIneligibleReasons().add(CheckinIneligibleReasons.VCRI.getCheckinIneligibleReason());

            return;
        }
    }

    public CheckInSetUpConfigFactoryWrapper getSetUpConfigFactory() {
        return setUpConfigFactory;
    }

    public void setSetUpConfigFactory(CheckInSetUpConfigFactoryWrapper setUpConfigFactory) {
        this.setUpConfigFactory = setUpConfigFactory;
    }

    public ItineraryService getItineraryService() {
        return itineraryService;
    }

    public void setItineraryService(ItineraryService itineraryService) {
        this.itineraryService = itineraryService;
    }

    public ReservationUtilService getReservationUtilService() {
        return reservationUtilService;
    }

    public void setReservationUtilService(ReservationUtilService reservationUtilService) {
        this.reservationUtilService = reservationUtilService;
    }
}
