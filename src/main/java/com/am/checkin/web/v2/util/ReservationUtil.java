package com.am.checkin.web.v2.util;

import com.aeromexico.commons.model.AbstractAncillary;
import com.aeromexico.commons.model.AbstractSegmentChoice;
import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.BookedTravelerAncillaryCollection;
import com.aeromexico.commons.model.CabinCapacity;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.CartPNRCollection;
import com.aeromexico.commons.model.PNR;
import com.aeromexico.commons.model.PaidSegmentChoice;
import com.aeromexico.commons.model.PaidTravelerAncillary;
import com.aeromexico.commons.model.Segment;
import com.aeromexico.commons.model.SegmentChoice;
import com.aeromexico.commons.model.TravelerAncillary;
import com.aeromexico.commons.web.types.ActionCodeType;
import com.aeromexico.commons.web.types.AirlineCodeType;
import com.aeromexico.commons.web.types.AncillaryGroupType;
import com.aeromexico.commons.web.types.AncillaryPostBookingType;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.util.SegmentCodeUtil;
import com.aeromexico.commons.web.util.TimeUtil;
import com.aeromexico.sharedservices.util.AncillaryUtil;
import com.aeromexico.sharedservices.util.BrandedFareRulesUtil;
import com.am.checkin.web.util.AEUtil;
import com.am.checkin.web.util.PNRLookUpServiceUtil;
import com.google.gson.Gson;
import com.sabre.services.res.or.getreservation.OpenReservationElementType;
import com.sabre.webservices.pnrbuilder.getreservation.AncillaryServicesPNRB;
import com.sabre.webservices.pnrbuilder.getreservation.GetReservationRS;
import com.sabre.webservices.pnrbuilder.getreservation.PassengerPNRB;
import com.sabre.webservices.pnrbuilder.getreservation.SegmentOrTravelPortionType;
import com.sabre.webservices.pnrbuilder.updatereservation.ReservationUpdateItemType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class ReservationUtil {
    private static final Logger LOG = LoggerFactory.getLogger(ReservationUtil.class);

    private static List<AncillaryServicesPNRB> getAncillariesForThisPassengerAndLeg(
            String nameRef,
            BookedLeg bookedLeg,
            GetReservationRS getReservationRS
    ) {
        if (null == nameRef
                || null == bookedLeg
                || bookedLeg.getSegments().getCollection().isEmpty()
                || null == getReservationRS
                || null == getReservationRS.getReservation()
                || null == getReservationRS.getReservation().getPassengerReservation()
                || null == getReservationRS.getReservation().getPassengerReservation().getPassengers()
                || null == getReservationRS.getReservation().getPassengerReservation().getPassengers().getPassenger()
        ) {
            return null;
        }

        for (PassengerPNRB passengerPNRB : getReservationRS.getReservation().getPassengerReservation().getPassengers().getPassenger()) {
            if (nameRef.equalsIgnoreCase(passengerPNRB.getNameId())) {
                if (null != passengerPNRB.getAncillaryServices() && null != passengerPNRB.getAncillaryServices().getAncillaryService()) {

                    List<AncillaryServicesPNRB> ancillaryServicesPNRBList = new ArrayList<>();

                    for (AncillaryServicesPNRB ancillaryServicesPNRB : passengerPNRB.getAncillaryServices().getAncillaryService()) {
                        if (null == ancillaryServicesPNRB.getSegment() && null == ancillaryServicesPNRB.getTravelPortions()) {
                            ancillaryServicesPNRBList.add(ancillaryServicesPNRB);
                        } else {
                            SegmentOrTravelPortionType segmentOrTravelPortionType = null;

                            if (null != ancillaryServicesPNRB.getSegment()) {
                                segmentOrTravelPortionType = ancillaryServicesPNRB.getSegment();
                            } else if (null != ancillaryServicesPNRB.getTravelPortions()
                                    && null != ancillaryServicesPNRB.getTravelPortions().getTravelPortion()
                                    && !ancillaryServicesPNRB.getTravelPortions().getTravelPortion().isEmpty()) {
                                segmentOrTravelPortionType = ancillaryServicesPNRB.getTravelPortions().getTravelPortion().get(0);
                            }

                            if (null != segmentOrTravelPortionType && (
                                    PNRLookUpServiceUtil.getLegCode(segmentOrTravelPortionType).equalsIgnoreCase(bookedLeg.getSegments().getLegCode())
                                            || isForThisLeg(bookedLeg.getSegments().getCollection(), segmentOrTravelPortionType))
                            ) {
                                ancillaryServicesPNRBList.add(ancillaryServicesPNRB);
                            }
                        }
                    }

                    if (!ancillaryServicesPNRBList.isEmpty()) {
                        return ancillaryServicesPNRBList;
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            }
        }

        return null;
    }

    /**
     * @param bookedTraveler
     * @param bookedLeg
     * @param getReservationRS
     * @param reservationUpdateItemTypeList
     * @return
     * @throws Exception
     */
    public static BookedTravelerAncillaryCollection getAncillaries(
            BookedTraveler bookedTraveler,
            BookedLeg bookedLeg,
            GetReservationRS getReservationRS,
            List<ReservationUpdateItemType> reservationUpdateItemTypeList
    ) throws Exception {

        BookedTravelerAncillaryCollection bookedTravelerAncillaryCollection = new BookedTravelerAncillaryCollection();

        List<AncillaryServicesPNRB> ancillariesListForThisPassenger = getAncillariesForThisPassengerAndLeg(
                bookedTraveler.getNameRefNumber(), bookedLeg, getReservationRS
        );

        if (null == ancillariesListForThisPassenger || ancillariesListForThisPassenger.isEmpty()) {
            return bookedTravelerAncillaryCollection;
        }

        List<AbstractAncillary> bookedTravelerAncillaryList = new ArrayList<>();

        LOG.info("ancillariesListForThisPassenger: " + new Gson().toJson(ancillariesListForThisPassenger));

        for (AncillaryServicesPNRB ancillaryPassenger : ancillariesListForThisPassenger) {
            if (null != ancillaryPassenger
                    && !AncillaryGroupType.SA.toString().equalsIgnoreCase(ancillaryPassenger.getGroupCode())
                    && !AncillaryPostBookingType._0B5.getRficSubCode().equalsIgnoreCase(ancillaryPassenger.getRficSubcode())) {

                // PAID ANCILLARIES
                if (ActionCodeType.HK.toString().equalsIgnoreCase(ancillaryPassenger.getActionCode())
                        || ActionCodeType.HI.toString().equalsIgnoreCase(ancillaryPassenger.getActionCode())) {

                    AncillaryPostBookingType ancillaryType = AncillaryPostBookingType.getType(ancillaryPassenger.getRficSubcode());

                    if (null != ancillaryType && !"Y".equalsIgnoreCase(ancillaryPassenger.getEMDConsummedAtIssuance())) {

                        AncillaryPostBookingType newRficSubcode = AncillaryUtil.convertNewCodeToOldCode(ancillaryPassenger.getRficSubcode());

                        if (null != newRficSubcode) {

                            PaidTravelerAncillary paidTravelerAncillary = new PaidTravelerAncillary();
                            if (null != ancillaryPassenger.getSegment()) {
                                paidTravelerAncillary.setLegCode(
                                        createLegCode(ancillaryPassenger.getSegment())
                                );
                            } else if (null != ancillaryPassenger.getTravelPortions()
                                    && null != ancillaryPassenger.getTravelPortions().getTravelPortion()
                                    && !ancillaryPassenger.getTravelPortions().getTravelPortion().isEmpty()) {
                                paidTravelerAncillary.setLegCode(
                                        createLegCode(ancillaryPassenger.getTravelPortions().getTravelPortion().get(0))
                                );
                            } else {
                                paidTravelerAncillary.setLegCode(bookedLeg.getSegments().getLegCode());
                            }

                            paidTravelerAncillary.setType(newRficSubcode.getRficSubCode());
                            paidTravelerAncillary.setRficSubcode(newRficSubcode.getRficSubCode());

                            paidTravelerAncillary.setRealRficSubcode(ancillaryPassenger.getRficSubcode());
                            paidTravelerAncillary.setGroupCode(ancillaryPassenger.getGroupCode());

                            paidTravelerAncillary.setQuantity(
                                    Integer.parseInt(ancillaryPassenger.getNumberOfItems())
                            );

                            if (null != ancillaryPassenger.getEMDNumber()) {
                                paidTravelerAncillary.setEmd(ancillaryPassenger.getEMDNumber());
                            } else if (null != ancillaryPassenger.getSegment()
                                    && null != ancillaryPassenger.getSegment().getEMDNumber()) {
                                paidTravelerAncillary.setEmd(ancillaryPassenger.getSegment().getEMDNumber());
                            }

                            if (null != ancillaryPassenger.getEMDCoupon()) {
                                paidTravelerAncillary.setEmdCoupon(ancillaryPassenger.getEMDCoupon());
                            } else if (null != ancillaryPassenger.getSegment()
                                    && null != ancillaryPassenger.getSegment().getEMDCoupon()) {
                                paidTravelerAncillary.setEmdCoupon(ancillaryPassenger.getSegment().getEMDCoupon());
                            }

                            if (ActionCodeType.HK.toString().equalsIgnoreCase(ancillaryPassenger.getActionCode())) {
                                paidTravelerAncillary.setRedeemed(true);
                            } else if (ActionCodeType.HI.toString().equalsIgnoreCase(ancillaryPassenger.getActionCode())) {
                                paidTravelerAncillary.setRedeemed(false);
                            }

                            paidTravelerAncillary.setEmdType(ancillaryPassenger.getEMDType());
                            paidTravelerAncillary.setEmdConsummedAtIssuance(ancillaryPassenger.getEMDConsummedAtIssuance());

                            paidTravelerAncillary.setIsPartOfReservation(true);
                            paidTravelerAncillary.setAncillaryId(ancillaryPassenger.getId());
                            paidTravelerAncillary.setNameNumber(bookedTraveler.getNameRefNumber());
                            paidTravelerAncillary.setActionCode(ancillaryPassenger.getActionCode());
                            paidTravelerAncillary.setCommercialName(ancillaryPassenger.getCommercialName());

                            bookedTravelerAncillaryList.add(paidTravelerAncillary);
                        }
                    }
                } else {
                    //delete this AE
                    AEUtil.addDeleteItem(ancillaryPassenger.getId(), bookedTraveler, reservationUpdateItemTypeList);
                }
            }
        }

        bookedTravelerAncillaryCollection.setCollection(bookedTravelerAncillaryList);

        return bookedTravelerAncillaryCollection;
    }

    private static String createLegCode(
            SegmentOrTravelPortionType segment
    ) {
        if (segment.getAirlineCode() == null) {
            return null;
        }
        return segment.getBoardPoint()
                + "_"
                + segment.getAirlineCode()
                + "_"
                + segment.getFlightNumber()
                + "_"
                + TimeUtil.getTime(segment.getDepartureDate(), "yyyy-MM-dd");
    }

    /**
     * @param bookedSegmentList
     * @param segment
     * @return
     */
    private static boolean isForThisLeg(List<BookedSegment> bookedSegmentList, SegmentOrTravelPortionType segment) {

        if (null == bookedSegmentList || bookedSegmentList.isEmpty() || null == segment) {
            return false;
        }

        for (BookedSegment bookedSegment : bookedSegmentList) {
            try {
                Segment segmentLocal = bookedSegment.getSegment();

                if (segmentLocal.getOperatingCarrier().equalsIgnoreCase(segment.getAirlineCode())
                        && segmentLocal.getDepartureAirport().equalsIgnoreCase(segment.getBoardPoint())
                        && segmentLocal.getArrivalAirport().equalsIgnoreCase(segment.getOffPoint())) {
                    return true;
                }
            } catch (Exception ex) {
                //
            }

        }

        return false;
    }

    public static void setChildrenValidation(PNR pnr) {

        if (null != pnr.getCarts() && null != pnr.getCarts().getCollection()) {
            for (CartPNR cartPNR : pnr.getCarts().getCollection()) {

                BookedLeg bookedLeg = pnr.getBookedLegByCartId(cartPNR.getMeta().getCartId());

                BookedSegment bookedSegment = bookedLeg.getFirstOpenSegment();

                if (!cartPNR.isSeamlessCheckin()
                        || (null != bookedSegment && AirlineCodeType.AM.getCode().equalsIgnoreCase(bookedSegment.getSegment().getOperatingCarrier()))) {

                    if (BrandedFareRulesUtil.isBasicEconomy(bookedLeg, pnr.getCreationDate())
                            && BrandedFareRulesUtil.areThereChildrenPaxs(pnr)) {

                        for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
                            //throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.INFANT_OR_CHILDREN_IN_PNR_NOT_VALID_FOR_BASIC_ECONOMY, ErrorType.INFANT_OR_CHILDREN_IN_PNR_NOT_VALID_FOR_BASIC_ECONOMY.getFullDescription());

                            bookedTraveler.setIsEligibleToCheckin(false);

                            bookedTraveler.getIneligibleReasons().add(
                                    ErrorType.INFANT_OR_CHILDREN_IN_PNR_NOT_VALID_FOR_BASIC_ECONOMY.getSearchableCode()
                            );
                        }
                    }
                }
            }
        }
    }

    public static boolean isFlightOverBooked(Segment segment) {

        if (segment == null) {
            return false;
        }

        for (CabinCapacity capacity : segment.getCapacity()) {
            if (capacity.getCabin().equalsIgnoreCase("main")) {

                double percent = ((double) (capacity.getBooked() - capacity.getAuthorized()) / capacity.getAuthorized()) * 100.0;
                LOG.info("Flight {} is {} over booked.", segment.getMarketingFlightCode(), percent);

                if (percent > 3.0) {
                    return true;
                } else {
                    return false;
                }
            }
        }

        return false;
    }

    /**
     * @param pnr
     */
    public static void updateSegmentsFlagUpgradeAvaiable(PNR pnr) {
        for (CartPNR cartPNR : pnr.getCarts().getCollection()) {
            for (BookedLeg bookedLeg : pnr.getLegs().getCollection()) {
                if (cartPNR.getLegCode().equalsIgnoreCase(bookedLeg.getSegments().getLegCode())) {
                    for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {
                        if (null != cartPNR.getUpsellOffers()) {
                            bookedSegment.getSegment().setIsPremierAvailable(true);
                        }
                    }
                }
            }
        }
    }

    /**
     * @param pnr
     * @throws Exception
     */
    public static void updateSegmentCodesForSeats(PNR pnr) throws Exception {
        CartPNRCollection carts = pnr.getCarts();

        for (CartPNR carPNR : carts.getCollection()) {
            BookedLeg bookedLeg = pnr.getBookedLegByLegCode(carPNR.getLegCode());
            for (BookedTraveler bookedTraveler : carPNR.getTravelerInfo().getCollection()) {

                for (AbstractSegmentChoice abstractSegmentChoice : bookedTraveler.getSegmentChoices().getCollection()) {
                    if (abstractSegmentChoice instanceof SegmentChoice) {
                        SegmentChoice segmentChoice = (SegmentChoice) abstractSegmentChoice;
                        Segment segment = getSegmentFromSegmentCodeSeat(segmentChoice.getSegmentCode(), bookedLeg);
                        if (null != segment) {
                            segmentChoice.setSegmentCode(segment.getSegmentCode());
                            int index = getSeatAncillaryIndexBySegmentCode(segment.getSegmentCode(), bookedTraveler.getSeatAncillaries().getCollection());

                            if (0 <= index) {
                                TravelerAncillary travelerAncillary = (TravelerAncillary) bookedTraveler.getSeatAncillaries().getCollection().get(index);
                                travelerAncillary.setSegmentCodeAux(segment.getSegmentCode());
                                bookedTraveler.getSeatAncillaries().getCollection().set(index, travelerAncillary);
                            }
                        }

                    } else if (abstractSegmentChoice instanceof PaidSegmentChoice) {
                        PaidSegmentChoice paidSegmentChoice = (PaidSegmentChoice) abstractSegmentChoice;
                        Segment segment = getSegmentFromSegmentCodeSeat(paidSegmentChoice.getSegmentCode(), bookedLeg);
                        if (null != segment) {
                            paidSegmentChoice.setSegmentCode(segment.getSegmentCode());
                        }
                    }
                }
            }
        }
    }

    /**
     * This method exclude the time part for compare
     *
     * @param segmentCode
     * @param bookedLeg
     * @return
     */
    private static Segment getSegmentFromSegmentCodeSeat(String segmentCode, BookedLeg bookedLeg) {
        for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {
            try {
                String[] parts = bookedSegment.getSegment().getSegmentCode().split("_");
                String[] partsSeats = segmentCode.split("_");

                if (parts[0].equalsIgnoreCase(partsSeats[0]) && parts[1].equalsIgnoreCase(partsSeats[1])
                        && parts[2].equalsIgnoreCase(partsSeats[2]) && parts[3].equalsIgnoreCase(partsSeats[3])) {
                    return bookedSegment.getSegment();
                }

            } catch (Exception ignored) {

            }
        }

        return null;
    }

    /**
     * @param segmentCode
     * @param abstractAncillaryList
     * @return
     */
    public static int getSeatAncillaryIndexBySegmentCode(
            String segmentCode,
            List<AbstractAncillary> abstractAncillaryList
    ) {

        int index = 0;
        for (AbstractAncillary abstractAncillary : abstractAncillaryList) {
            if (abstractAncillary instanceof TravelerAncillary) {
                TravelerAncillary travelerAncillary = (TravelerAncillary) abstractAncillary;
                if (("SEAT ASSIGNMENT".equalsIgnoreCase(travelerAncillary.getAncillary().getCommercialName())
                        || "ASIENTO PAGADO PAIDSEAT".equalsIgnoreCase(travelerAncillary.getAncillary().getCommercialName())
                        || "ASIENTO PAGADO PAID SEAT".equalsIgnoreCase(travelerAncillary.getAncillary().getCommercialName())
                        || "SEAT SELECTION".equalsIgnoreCase(travelerAncillary.getAncillary().getCommercialName())
                        || AncillaryPostBookingType._0B5.getRficSubCode().equalsIgnoreCase(travelerAncillary.getAncillary().getRficSubcode()))
                        && SegmentCodeUtil.compareSegmentCodeWithoutTime(segmentCode, travelerAncillary.getSegmentCodeAux())) {

                    return index;
                }
            }
            index++;
        }

        return -1;
    }

    /**
     * @param segmentCode
     * @param abstractAncillaryList
     * @return
     */
    public static TravelerAncillary getSeatAncillaryBySegmentCode(
            String segmentCode,
            List<AbstractAncillary> abstractAncillaryList
    ) {

        for (AbstractAncillary abstractAncillary : abstractAncillaryList) {
            if (abstractAncillary instanceof TravelerAncillary) {
                TravelerAncillary travelerAncillary = (TravelerAncillary) abstractAncillary;
                if (("SEAT ASSIGNMENT".equalsIgnoreCase(travelerAncillary.getAncillary().getCommercialName())
                        || "ASIENTO PAGADO PAIDSEAT".equalsIgnoreCase(travelerAncillary.getAncillary().getCommercialName())
                        || "ASIENTO PAGADO PAID SEAT".equalsIgnoreCase(travelerAncillary.getAncillary().getCommercialName())
                        || "SEAT SELECTION".equalsIgnoreCase(travelerAncillary.getAncillary().getCommercialName())
                        || AncillaryPostBookingType._0B5.getRficSubCode().equalsIgnoreCase(travelerAncillary.getAncillary().getRficSubcode()))
                        && SegmentCodeUtil.compareSegmentCodeWithoutTime(segmentCode, travelerAncillary.getSegmentCodeAux())) {

                    return travelerAncillary;
                }
            }

        }

        return null;
    }

    public static void checkExistsBookedCar(PNR pnr, GetReservationRS getReservationRS) {
        List<OpenReservationElementType> openReservationElement = getReservationRS.getReservation().getOpenReservationElements().getOpenReservationElement();

        for (OpenReservationElementType specialServiceInfo : openReservationElement) {
            if (specialServiceInfo.getServiceRequest() != null && !specialServiceInfo.getServiceRequest().getFreeText().isEmpty()
                    && specialServiceInfo.getServiceRequest().getFreeText().contains("CAR RENTAL")) {
                pnr.setBookedCar(true);
                break;
            }
        }
    }
}
