package com.am.checkin.web.v2.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DigitalSignatureRq implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Gson GSON = new GsonBuilder()
            .serializeNulls()
            .excludeFieldsWithoutExposeAnnotation()
            .create();

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("barcodeData")
    @Expose
    private String barcodeData;

    public String getBarcodeData() {
        return barcodeData;
    }

    public void setBarcodeData(String barcodeData) {
        this.barcodeData = barcodeData;
    }

    @Override
    public String toString() {
        return GSON.toJson(this);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
