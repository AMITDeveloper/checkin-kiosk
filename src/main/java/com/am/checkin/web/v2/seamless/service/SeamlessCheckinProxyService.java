package com.am.checkin.web.v2.seamless.service;


import com.aeromexico.commons.prototypes.JSONPrototype;
import com.am.checkin.web.v2.seamless.model.TravelPartyWrapper;
import com.am.checkin.web.v2.seamless.client.SeamlessCheckInRestClient;
import com.am.checkin.web.v2.util.SystemVariablesUtil;
import com.am.checkin.web.v2.util.TravelPartyErrorUtil;
import com.am.seamless.checkin.common.models.TravelParty;
import com.am.seamless.checkin.common.models.errorhandling.SeamlessError;
import com.am.seamless.checkin.common.models.exception.SeamlessException;
import com.am.seamless.checkin.common.models.request.BoardingDocumentRq;
import com.am.seamless.checkin.common.models.request.PassengerInfoRq;
import com.am.seamless.checkin.common.models.request.PassengerSelectionRq;
import com.am.seamless.checkin.common.models.request.TravelPartyRq;
import com.am.seamless.checkin.common.models.request.UpdateMandatesRq;

import com.am.seamless.checkin.common.models.types.ErrorType;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.SocketTimeoutException;

@Named
@ApplicationScoped
public class SeamlessCheckinProxyService {
    private static final Logger LOG = LoggerFactory.getLogger(SeamlessCheckinProxyService.class);

    @Inject
    private SeamlessCheckInRestClient seamlessCheckInRestClient;

    private SeamlessCheckinServicesInterface proxy = null;

    private SeamlessCheckinServicesInterface getProxy() {
        if (null == proxy) {
            ResteasyWebTarget target = seamlessCheckInRestClient.getRestEasyClient().target(UriBuilder.fromPath(SystemVariablesUtil.getSeamlessCheckinUrl()));
            proxy = target.proxy(SeamlessCheckinServicesInterface.class);
        }
        return proxy;
    }

    /**
     * @param travelPartyRq
     * @param operatingHandler
     * @return
     */
    public TravelPartyWrapper getTravelParty(TravelPartyRq travelPartyRq, String operatingHandler) throws Exception {

        try {

            LOG.info("SEAMLESS_RQ_BP: {} OH: {}", travelPartyRq, operatingHandler);

            Response response = getProxy().getTravelParty(travelPartyRq, operatingHandler);

            LOG.info("SEAMLESS_STATUS: {}", response.getStatus());
            LOG.info("SEAMLESS_STATUS: {}", response.getStatusInfo());

            if (response.getStatus() >= 200 && response.getStatus() < 300) {
                String transactionId = response.getHeaderString("transactionid");

                String json = response.readEntity(String.class);
                LOG.info("SEAMLESS_RESPONSE: {}", response);
                LOG.info("SEAMLESS_JSON: {}", json);

                TravelParty travelPartyUpdated = getResponse(json);

                TravelPartyErrorUtil.validateTravelPartyResponse(travelPartyUpdated, response.getStatus());

                TravelPartyWrapper travelPartyWrapper = new TravelPartyWrapper(transactionId, travelPartyUpdated);

                return travelPartyWrapper;
            } else {

                LOG.error("SEAMLESS_ERROR: calling travel party, reponse not ok");

                throw new SeamlessException(response.readEntity(String.class), response.getStatus());
            }
        } catch (SocketTimeoutException ex) {

            SeamlessError seamlessError = TravelPartyErrorUtil.getSeamlessError(ErrorType.SDS_TRAVEL_PARTY_TIME_OUT, null);

            throw new SeamlessException(seamlessError.toString(), 500);
        }
    }

    /**
     * @param passengerSelectionRq
     * @param travelPartyId
     * @param transactionId
     * @return
     * @throws Exception
     */
    public TravelParty selectPassengers(PassengerSelectionRq passengerSelectionRq, String travelPartyId, String transactionId) throws Exception {

        try {
            LOG.info("SEAMLESS_RQ_BP: {} TPI: {} TRI: {}", passengerSelectionRq, travelPartyId, transactionId);

            Response response = getProxy().selectPassengers(passengerSelectionRq, travelPartyId, transactionId);

            LOG.info("SEAMLESS_STATUS: {}", response.getStatus());
            LOG.info("SEAMLESS_STATUS: {}", response.getStatusInfo());

            if (response.getStatus() >= 200 && response.getStatus() < 300) {
                String json = response.readEntity(String.class);
                LOG.info("SEAMLESS_RESPONSE: {}", response);
                LOG.info("SEAMLESS_JSON: {}", json);

                TravelParty travelPartyUpdated = getResponse(json);

                TravelPartyErrorUtil.validateTravelPartyResponse(travelPartyUpdated, response.getStatus());

                return travelPartyUpdated;
            } else {
                throw new SeamlessException(response.readEntity(String.class), response.getStatus());
            }
        } catch (SocketTimeoutException ex) {

            SeamlessError seamlessError = TravelPartyErrorUtil.getSeamlessError(ErrorType.SDS_TRAVEL_PARTY_TIME_OUT, null);

            throw new SeamlessException(seamlessError.toString(), 500);
        }
    }

    /**
     * @param passengerInfoRq
     * @param travelPartyId
     * @param transactionId
     * @return
     * @throws Exception
     */
    public TravelParty updatePassengerInfo(PassengerInfoRq passengerInfoRq, String travelPartyId, String transactionId) throws Exception {

        try {
            LOG.info("SEAMLESS_RQ_BP: {} TPI: {} TRI: {}", passengerInfoRq, travelPartyId, transactionId);

            Response response = getProxy().updatePassengerInfo(passengerInfoRq, travelPartyId, transactionId);

            LOG.info("SEAMLESS_STATUS: {}", response.getStatus());
            LOG.info("SEAMLESS_STATUS: {}", response.getStatusInfo());

            if (response.getStatus() >= 200 && response.getStatus() < 300) {
                String json = response.readEntity(String.class);
                LOG.info("SEAMLESS_RESPONSE: {}", response);
                LOG.info("SEAMLESS_JSON: {}", json);

                TravelParty travelPartyUpdated = getResponse(json);

                TravelPartyErrorUtil.validateTravelPartyResponse(travelPartyUpdated, response.getStatus());

                return travelPartyUpdated;
            } else {
                throw new SeamlessException(response.readEntity(String.class), response.getStatus());
            }
        } catch (SocketTimeoutException ex) {

            SeamlessError seamlessError = TravelPartyErrorUtil.getSeamlessError(ErrorType.SDS_TRAVEL_PARTY_TIME_OUT, null);

            throw new SeamlessException(seamlessError.toString(), 500);
        }
    }

    public TravelParty updateMandates(
            UpdateMandatesRq updateMandatesRq,
            String travelPartyId,
            String transactionId
    ) throws Exception {

        try {
            LOG.info("SEAMLESS_RQ_BP: {} TPI: {} TRI: {}", updateMandatesRq, travelPartyId, transactionId);

            Response response = getProxy().updateMandates(updateMandatesRq, travelPartyId, transactionId);

            LOG.info("SEAMLESS_STATUS: {}", response.getStatus());
            LOG.info("SEAMLESS_STATUS: {}", response.getStatusInfo());

            if (response.getStatus() >= 200 && response.getStatus() < 300) {
                String json = response.readEntity(String.class);
                LOG.info("SEAMLESS_RESPONSE: {}", response);
                LOG.info("SEAMLESS_JSON: {}", json);

                TravelParty travelPartyUpdated = getResponse(json);

                TravelPartyErrorUtil.validateTravelPartyResponse(travelPartyUpdated, response.getStatus());

                return travelPartyUpdated;
            } else {
                throw new SeamlessException(response.readEntity(String.class), response.getStatus());
            }
        } catch (SocketTimeoutException ex) {

            SeamlessError seamlessError = TravelPartyErrorUtil.getSeamlessError(ErrorType.SDS_TRAVEL_PARTY_TIME_OUT, null);

            throw new SeamlessException(seamlessError.toString(), 500);
        }
    }

    public TravelParty boardingDocuments(
            BoardingDocumentRq boardingDocumentRq,
            String issuingCarrier,
            String travelPartyId,
            String transactionId
    ) throws Exception {

        try {
            LOG.info("SEAMLESS_RQ_BP: {} IC: {} TPI: {} TRI: {}", boardingDocumentRq, issuingCarrier, travelPartyId, transactionId);

            Response response = getProxy().boardingDocuments(boardingDocumentRq, issuingCarrier, travelPartyId, transactionId);

            LOG.info("SEAMLESS_STATUS: {}", response.getStatus());
            LOG.info("SEAMLESS_STATUS: {}", response.getStatusInfo());

            if (response.getStatus() >= 200 && response.getStatus() < 300) {
                String json = response.readEntity(String.class);
                LOG.info("SEAMLESS_RESPONSE: {}", response);
                LOG.info("SEAMLESS_JSON: {}", json);

                TravelParty travelPartyUpdated = getResponse(json);

                TravelPartyErrorUtil.validateTravelPartyResponse(travelPartyUpdated, response.getStatus());

                return travelPartyUpdated;
            } else {
                throw new SeamlessException(response.readEntity(String.class), response.getStatus());
            }
        } catch (SocketTimeoutException ex) {

            SeamlessError seamlessError = TravelPartyErrorUtil.getSeamlessError(ErrorType.SDS_TRAVEL_PARTY_TIME_OUT, null);

            throw new SeamlessException(seamlessError.toString(), 500);
        }
    }

    public static TravelParty getResponse(String jsonResponse) {
        return JSONPrototype.convert(jsonResponse, TravelParty.class);
    }
}
