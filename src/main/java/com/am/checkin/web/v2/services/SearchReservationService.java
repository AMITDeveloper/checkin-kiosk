package com.am.checkin.web.v2.services;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.rq.PnrRQ;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.types.PosType;
import com.aeromexico.commons.web.util.Constants;
import com.aeromexico.dao.util.CommonUtil;
import com.aeromexico.sabre.api.session.common.ServiceCall;
import com.aeromexico.sabre.api.tripsearch.AMTripSearchService;
import com.aeromexico.sabre.api.tripsearch.model.TripSearchOption;
import com.aeromexico.sabre.api.tripsearch.model.TripSearchRequest;
import com.aeromexico.sharedservices.services.TicketDocumentService;
import com.am.checkin.web.v2.util.SearchReservationUtil;
import com.am.checkin.web.v2.util.SearchServiceUtil;
import com.sabre.ns.ticketing.dc.GetTicketingDocumentRS;
import com.sabre.webservices.triprecord.Reservations;
import com.sabre.webservices.triprecord.TripSearchRS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

/**
 * @author adrianleal
 */
@Named
@ApplicationScoped
public class SearchReservationService {
    private static final Logger LOG = LoggerFactory.getLogger(SearchReservationService.class);

    @Inject
    private TicketDocumentService ticketDocumentService;

    @Inject
    private AMTripSearchService tripSearchService;

//    @Inject
//    private AMTripSearchServiceTVL tripSearchServiceTvl;

    /**
     * @param pnrRQ
     * @return
     * @throws Exception
     */
    public List<String> searchByFF(
            PnrRQ pnrRQ
    ) throws GenericException, Exception {

        LOG.info("searchByFF: {}", pnrRQ.toString());

        List<String> pnrList = new ArrayList<>();

        TripSearchRequest tripSearchRequest = SearchServiceUtil.createTripSearchRequestByFF(pnrRQ);

        TripSearchRS tripSearchRS = getTripSearchService().searchTrip(
                tripSearchRequest, false, new ArrayList<>()
        );

        SearchReservationUtil.validateTripSearchResponse(tripSearchRS);
        SearchReservationUtil.validateTripSearchResponseResultsByFF(tripSearchRS, pnrRQ.getPos());

        for (Reservations reservations : tripSearchRS.getReservationsList().getReservations()) {
            for (Reservations.Reservation reservation : reservations.getReservation()) {
                pnrList.add(reservation.getLocator());
            }
        }

        return pnrList;
    }


    /**
     * This method does a search by Flight Number or Destiantion Airport
     *
     * @param pnrRQ
     * @return
     * @throws Exception
     */
    public String searchByDestinationFlight(
            PnrRQ pnrRQ
    ) throws Exception {

        LOG.info("searchByDestinationFlight: {}", pnrRQ.toString());

        TripSearchRequest tripSearchRequest = SearchServiceUtil.createTripSearchRequestByDestinationFlight(pnrRQ);

        TripSearchRS tripSearchRS = getTripSearchService().searchTrip(
                tripSearchRequest, false, new ArrayList<>()
        );

        SearchReservationUtil.validateTripSearchResponse(tripSearchRS);

        if (null == tripSearchRS.getReservationsList().getTotalResults()
                || tripSearchRS.getReservationsList().getTotalResults() <= 0
        ) {
            tripSearchRequest.setFinalDestination(tripSearchRequest.getArrivalCity());
            tripSearchRequest.setArrivalCity(null);

            tripSearchRS = getTripSearchService().searchTrip(
                    tripSearchRequest, false, new ArrayList<>()
            );

            SearchReservationUtil.validateTripSearchResponse(tripSearchRS);
        }

        SearchReservationUtil.validateTripSearchResponseResultsByDestinationFlight(tripSearchRS, pnrRQ.getPos());

        if (null != tripSearchRS.getReservationsList() && null != tripSearchRS.getReservationsList().getReservations()) {
            for (Reservations reservations : tripSearchRS.getReservationsList().getReservations()) {
                for (Reservations.Reservation reservation : reservations.getReservation()) {
                    return reservation.getLocator();
                }
            }
        }

        return null;
    }

    /**
     * @param vcr
     * @return
     * @throws Exception
     */
    public String searchByVCR(
            String vcr,
            String pos
    ) throws Exception {

        GetTicketingDocumentRS getTicketingDocumentRS;
        getTicketingDocumentRS = getTicketDocumentService().getTicketDocByVCR(
                vcr,
                null
        );

        //TODO: validate response

        /*
        <TT:GetTicketingDocumentRS Version="3.3.0" xmlns:TT="http://www.sabre.com/ns/Ticketing/DC">
           <STL:STL_Header.RS messageID="TKTVLC750-25488-1691081104-1463098009812-46551-tktdoc" timeStamp="2016-05-12T19:06:49" xmlns:STL="http://services.sabre.com/STL/v01">
              <OrchestrationID seq="1" xmlns="http://services.sabre.com/STL/v01">TKTVLC750-25488-1691081104-1463098009812-46551-tktdoc</OrchestrationID>
              <DiagnosticData xmlns="http://services.sabre.com/STL/v01">Not found</DiagnosticData>
              <STL:Results>
                 <Success xmlns="http://services.sabre.com/STL/v01">
                    <System>T2</System>
                    <Source>DocumentServices</Source>
                 </Success>
              </STL:Results>
           </STL:STL_Header.RS>
        </TT:GetTicketingDocumentRS>
         */
        if (null == getTicketingDocumentRS || !"Found".equalsIgnoreCase(getTicketingDocumentRS.getSTLHeaderRS().getDiagnosticData())) {
            if (PosType.WEB.toString().equalsIgnoreCase(pos)) {
                throw new GenericException(
                        Response.Status.BAD_REQUEST,
                        ErrorType.RESERVATION_NOT_FOUND_WEB_CHECKIN_VCR,
                        CommonUtil.getMessageByCode(Constants.CODE_045)
                );
            } else {
            	throw new GenericException(
                    Response.Status.BAD_REQUEST,
                    ErrorType.TICKET_WAS_NOT_FOUND_APP
            	);
            	//throw new Exception("eTICKET WAS NOT FOUND: " + getTicketingDocumentRS.getSTLHeaderRS().getDiagnosticData());
            }
        }

        String pnr = getTicketingDocumentRS.getAbbreviated().get(0).getTicketingDocument().getDetails().getReservation().getSabre().getValue();

        /*
        <STL:STL_Header.RS messageID="TKTVLC750-25488-1691081104-1463098441949-46779-tktdoc" timeStamp="2016-05-12T19:14:01" xmlns:STL="http://services.sabre.com/STL/v01">
            <OrchestrationID seq="1" xmlns="http://services.sabre.com/STL/v01">TKTVLC750-25488-1691081104-1463098441949-46779-tktdoc</OrchestrationID>
            <DiagnosticData xmlns="http://services.sabre.com/STL/v01">Found</DiagnosticData>
            <STL:Results>
               <Success xmlns="http://services.sabre.com/STL/v01">
                  <System>T2</System>
                  <Source>DocumentServices</Source>
               </Success>
            </STL:Results>
        </STL:STL_Header.RS>
         */
        return pnr;

    }

    //Change service TripSearchTVL to TripSearch4.4
    public String searchByAgencyRecordlocator(PnrRQ pnrRQ, List<ServiceCall> serviceCallList) throws Exception {
        String recordLocator = pnrRQ.getRecordLocator();
        TripSearchRequest tripSearchRequest;
        tripSearchRequest= new TripSearchRequest();

        tripSearchRequest.setPnr(recordLocator);

        tripSearchRequest.setTripSearchOption(TripSearchOption.RESEARCHPNR);
        TripSearchRS tripSearchRS = tripSearchService.searchTrip(tripSearchRequest, false, serviceCallList);

        if (tripSearchRS == null) {
            throw new GenericException(Response.Status.INTERNAL_SERVER_ERROR, ErrorType.INTERNAL_ERROR);
        }

       if (tripSearchRS.getErrors() != null) {
            String errorMessage = null;
            for (com.sabre.webservices.triprecord.BaseTripResponse.Errors.Error error : tripSearchRS.getErrors().getError()) {
                errorMessage += error.getErrorMessage() + ". ";
            }
            LOG.error(errorMessage);
            if (null != pnrRQ.getPos() && PosType.WEB.toString().equalsIgnoreCase(pnrRQ.getPos())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.RESERVATION_NOT_FOUND_WEB_CHECKIN, CommonUtil.getMessageByCode(Constants.CODE_045));
            } else {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.RESERVATION_NOT_FOUND, CommonUtil.getMessageByCode(Constants.CODE_045));
           }

        }

        if (tripSearchRS.getReservationsList() != null && tripSearchRS.getReservationsList().getNumberResults() == 0) {
            if (null != pnrRQ.getPos() && PosType.WEB.toString().equalsIgnoreCase(pnrRQ.getPos())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.RESERVATION_NOT_FOUND_WEB_CHECKIN, CommonUtil.getMessageByCode(Constants.CODE_046));
            } else {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.RECORD_LOCATOR_NOT_FOUND_APP, CommonUtil.getMessageByCode(Constants.CODE_046));
            }

        } else if (tripSearchRS.getReservationsList().getNumberResults() > 1) {
            if (null != pnrRQ.getPos() && PosType.WEB.toString().equalsIgnoreCase(pnrRQ.getPos())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.RESERVATION_NOT_FOUND_WEB_CHECKIN, CommonUtil.getMessageByCode(Constants.CODE_046));
            } else {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.RECORD_LOCATOR_NOT_FOUND_APP, CommonUtil.getMessageByCode(Constants.CODE_046));
            }

        }

        try {
            return tripSearchRS.getReservationsList().getReservations().get(0).getReservation().get(0).getLocator();
        } catch (Exception e) {
            if (null != pnrRQ.getPos() && PosType.WEB.toString().equalsIgnoreCase(pnrRQ.getPos())) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.RESERVATION_NOT_FOUND_WEB_CHECKIN, CommonUtil.getMessageByCode(Constants.CODE_046));
            } else {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.RESERVATION_NOT_FOUND, CommonUtil.getMessageByCode(Constants.CODE_046));
            }

        }
    }

    /**
     * @return the ticketDocumentService
     */
    public TicketDocumentService getTicketDocumentService() {
        return ticketDocumentService;
    }

    /**
     * @param ticketDocumentService the ticketDocumentService to set
     */
    public void setTicketDocumentService(TicketDocumentService ticketDocumentService) {
        this.ticketDocumentService = ticketDocumentService;
    }

    public AMTripSearchService getTripSearchService() {
        return tripSearchService;
    }

    public void setTripSearchService(AMTripSearchService tripSearchService) {
        this.tripSearchService = tripSearchService;
    }

//    public AMTripSearchServiceTVL getTripSearchServiceTvl() {
//        return tripSearchServiceTvl;
//    }
//
//    public void setTripSearchServiceTvl(AMTripSearchServiceTVL tripSearchServiceTvl) {
//        this.tripSearchServiceTvl = tripSearchServiceTvl;
//    }
}
