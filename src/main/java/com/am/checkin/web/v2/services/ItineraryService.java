package com.am.checkin.web.v2.services;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedLegCollection;
import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.BookedSegmentCollection;
import com.aeromexico.commons.model.GroundHandling;
import com.aeromexico.commons.model.RestrictedCheckin;
import com.aeromexico.commons.model.PNR;
import com.aeromexico.commons.model.Segment;
import com.aeromexico.commons.model.Stop;
import com.aeromexico.commons.model.rq.PnrRQ;
import com.aeromexico.commons.model.weather.AirportWeather;
import com.aeromexico.commons.myb.model.ManageStatus;
import com.aeromexico.commons.web.types.AirlineCodeType;
import com.aeromexico.commons.web.types.BookedLegCheckinStatusType;
import com.aeromexico.commons.web.types.BookedLegFlightStatusType;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.types.FOPType;
import com.aeromexico.commons.web.types.LegType;
import com.aeromexico.commons.web.types.PosType;
import com.aeromexico.commons.web.types.WindowTimeType;
import com.aeromexico.commons.web.util.FlightNumberUtil;
import com.aeromexico.commons.web.util.ListsUtil;
import com.aeromexico.commons.web.util.TimeUtil;
import com.aeromexico.dao.util.CommonUtil;
import com.aeromexico.service.AmenitiesServiceCheckInMyb;
import com.aeromexico.sharedservices.util.PnrUtil;
import com.aeromexico.sharedservices.util.TravelerItineraryUtil;
import com.aeromexico.timezone.service.TimeZoneService;
import com.am.checkin.web.service.CommonServiceUtil;
import com.am.checkin.web.service.FlightDetailsService;
import com.am.checkin.web.util.PNRLookUpServiceUtil;
import com.am.checkin.web.v2.util.SystemVariablesUtil;
import com.sabre.ns.ticketing.dc.GetTicketingDocumentRS;
import com.sabre.ns.ticketing.dc.TicketingDocumentInfoAbbreviated;
import com.sabre.ns.ticketing.dc.TicketingDocumentPayment;
import com.sabre.services.acs.bso.flightdetail.v3.ACSFlightDetailRSACS;
import com.sabre.services.acs.bso.flightdetail.v3.ItineraryDetailsACS;
import com.sabre.services.res.or.getreservation.ProductType;
import com.sabre.webservices.pnrbuilder.getreservation.GetReservationRS;
import com.sabre.webservices.pnrbuilder.getreservation.SegmentTypePNRB;
import org.aeromexico.commons.exception.config.ReadProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Named
@ApplicationScoped
public class ItineraryService {

    private static final Logger LOG = LoggerFactory.getLogger(ItineraryService.class);

    @Inject
    private AirportService airportService;

    @Inject
    private AmenitiesServiceCheckInMyb amenitiesService;

    @Inject
    private FlightDetailsService flightDetailsService;

    @Inject
    private ReadProperties prop;

    private static final String DATE_TIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ss";

    /**
     *
     * @param getReservationRS
     * @param isAmTicketingProvider
     * @param pnrRQ
     * @return
     * @throws Exception
     */
    public BookedLegCollection getBookedLegCollection(
            GetReservationRS getReservationRS,
            boolean isAmTicketingProvider,
            PnrRQ pnrRQ
    ) throws Exception {
        BookedLegCollection bookedLegCollection = new BookedLegCollection();

        //To get missing information from getReservation
        List<SegmentTypePNRB.Segment> segmentList = getReservationRS.getReservation().getPassengerReservation().getSegments().getSegment()
                .stream().filter(i -> null != i.getAir() && null != i.getAir().getActionCode() && TravelerItineraryUtil.getFLIGHT_STATUS_OK_LIST().contains(i.getAir().getActionCode()))
                .collect(Collectors.toList());

        for (SegmentTypePNRB.Segment segmentReservation : segmentList) {

            SegmentTypePNRB.Segment.Air air = segmentReservation.getAir();
            ProductType productType = segmentReservation.getProduct();

            boolean start = false;
            int sequence = 0;
            LOG.info("Inbound connection : {}",air.isInboundConnection());
            LOG.info("Outbound connection : {}", air.isOutboundConnection());

            if (null == air.getMarriageGrp()
                    || "0".equalsIgnoreCase(air.getMarriageGrp().getGroup())
                    || "0".equalsIgnoreCase(air.getMarriageGrp().getSequence())
                    || "1".equalsIgnoreCase(air.getMarriageGrp().getSequence())) {
                start = true;
                if (null != air.getMarriageGrp()
                        && null != air.getMarriageGrp().getSequence()) {
                    sequence = Integer.valueOf(air.getMarriageGrp().getSequence());
                }
            }
            if (!air.isInboundConnection()&& air.isOutboundConnection()) {
                start = true;
                sequence = air.getSequence();
            }
            if (air.isInboundConnection() && air.isOutboundConnection()){
                start = false;
                sequence = air.getSequence();
            }
            if (air.isInboundConnection() && !air.isOutboundConnection()){
                start = false;
                sequence = air.getSequence();
            }
            LOG.info("Start : {}",start);
            LOG.info("Sequence : {}", sequence);

            BookedLeg bookedLeg;

            if (start
                    || null == bookedLegCollection.getCollection()
                    || bookedLegCollection.getCollection().isEmpty()) {

                // Create a new Leg only if does not exists
                bookedLeg = new BookedLeg();

                bookedLegCollection.getCollection().add(bookedLeg);
            } else {
                // Need to add this segment to the previous Leg
                bookedLeg = bookedLegCollection.getCollection().get(bookedLegCollection.getCollection().size() - 1);
            }

            Segment segment = new Segment();

            segment.setCodeShare(air.isCodeShare());
            segment.setPassFlight(air.isIsPast());

            segment.setBookingClass(air.getClassOfService());
            segment.setCabin(air.getCabin().getCode());

            if (null != productType
                    && null != productType.getProductDetails()
                    && null != productType.getProductDetails().getAir()
                    && null != productType.getProductDetails().getAir().getEquipmentType()) {
                segment.setAircraftType(productType.getProductDetails().getAir().getEquipmentType());
            } else {
                segment.setAircraftType(air.getEquipmentType());
            }

            segment.setMarketingCarrier(air.getMarketingAirlineCode());
            segment.setMarketingFlightCode(air.getMarketingFlightNumber());

            segment.setDepartureAirport(air.getDepartureAirport());
            segment.setArrivalAirport(air.getArrivalAirport());

            try {
                segment.setDomestic(
                        airportService.isDomesticFlight(
                                segment.getDepartureAirport(),
                                segment.getArrivalAirport()
                        )
                );
            } catch (Exception e) {
                // Continue with the code.
                segment.setDomestic(true);
            }

            com.aeromexico.timezone.service.TimeZone timeZone;
            timeZone = this.getTimeZone(air.getDepartureAirport());

            if (null != timeZone) {
                segment.setTimeZoneName(timeZone.getTimeZoneName());
                segment.setTimeZoneId(timeZone.getTimeZoneId());
                segment.setDstOffset(timeZone.getDstOffset());
                segment.setRawOffset(timeZone.getRawOffset());
            }

            timeZone = getTimeZone(air.getArrivalAirport());

            if (null != timeZone) {
                segment.setArrivalTimeZoneName(timeZone.getTimeZoneName());
                segment.setArrivalTimeZoneId(timeZone.getTimeZoneId());
                segment.setArrivalDstOffset(timeZone.getDstOffset());
                segment.setArrivalRawOffset(timeZone.getRawOffset());
            }

            segment.setDepartureDateTime(
                    air.getDepartureDateTime()
            );

            segment.setArrivalDateTime(
                    air.getArrivalDateTime()
            );

            if (null == air.getOperatingAirlineCode()
                    || air.getOperatingAirlineCode().isEmpty()
                    || "*".equalsIgnoreCase(air.getOperatingAirlineCode())
                    || "-".equalsIgnoreCase(air.getOperatingAirlineCode())) {
                if (null != air.getOperatingAirlineShortName()
                        && air.getOperatingAirlineShortName().toUpperCase().contains("AEROMEXICO")) {
                    segment.setOperatingCarrier(AirlineCodeType.AM.getCode());
                } else if (null != air.getMarketingAirlineCode()) {
                    segment.setOperatingCarrier(air.getMarketingAirlineCode());
                } else { // default is AM
                    segment.setOperatingCarrier(AirlineCodeType.AM.getCode());
                }
            } else {
                segment.setOperatingCarrier(air.getOperatingAirlineCode());
            }

            if (null == air.getOperatingFlightNumber()
                    || air.getOperatingFlightNumber().isEmpty()) {
                if (null == air.getMarketingFlightNumber()
                        || air.getMarketingFlightNumber().isEmpty()) {
                    segment.setOperatingFlightCode(air.getFlightNumber());
                } else {
                    segment.setOperatingFlightCode(air.getMarketingFlightNumber());
                }
            } else {
                segment.setOperatingFlightCode(air.getOperatingFlightNumber());
            }

            StringBuilder sb = new StringBuilder();
            sb.append(segment.getDepartureAirport());
            sb.append("_");
            sb.append(segment.getArrivalAirport());
            sb.append("_");
            sb.append(segment.getOperatingCarrier());
            sb.append("_");
            sb.append(CommonUtil.getFormattedDateTime(segment.getDepartureDateTime()));

            segment.setSegmentCode(
                    sb.toString()
            );

            segment.setFlightDurationInMinutes(CommonUtil.getElapsedTimeMins(air.getElapsedTime()));
            segment.setLayoverToNextSegmentsInMinutes(0);

            try {
                amenitiesService.setSegmentAmenities(segment);
            } catch (Exception ex) {
                // LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), ex.fillInStackTrace());
            }

            //Flight Details Service.
            ACSFlightDetailRSACS flightDetailRS;
            try {
                flightDetailRS = flightDetailsService.getFlightDetails(
                        segment.getOperatingCarrier(),
                        segment.getOperatingFlightCode(),
                        segment.getDepartureAirport(),
                        segment.getDepartureDateTime().substring(0, 10),
                        pnrRQ.getPos()
                );
            } catch (Exception e) {
                //LOG.error(e.getMessage(), e);
                flightDetailRS = null;
            }
            updateTimeInfoInSegment(segment, flightDetailRS, pnrRQ);
            //Add Capacity
            PNRLookUpServiceUtil.addCapacityInfo(segment, flightDetailRS);
            if (null != productType
                    && null != productType.getProductDetails()
                    && null != productType.getProductDetails().getAir()
                    && null != productType.getProductDetails().getAir().getDepartureTerminalCode()) {
                segment.setBoardingTerminal(productType.getProductDetails().getAir().getDepartureTerminalCode());
            } else {
                segment.setBoardingTerminal(null == air.getDepartureTerminalCode() ? "" : air.getDepartureTerminalCode());
            }

            segment.setIsPremierAvailable(false);
            segment.setSegmentNumber(String.valueOf(air.getSequence().intValue()));

            segment.setSegmentStatus(air.getActionCode());

            if (null != air.getHiddenStop() && !air.getHiddenStop().isEmpty()) {
                for (SegmentTypePNRB.Segment.Air.HiddenStop hiddenStop : air.getHiddenStop()) {
                    Stop stop = new Stop();
                    stop.setAirport(hiddenStop.getAirport());
                    stop.setAircraftType(hiddenStop.getEquipmentType());

                    String arrivalDateTime = CommonUtil.getFormattedFullDateTime(hiddenStop.getArrivalDateTime());
                    String departureDateTime = CommonUtil.getFormattedFullDateTime(hiddenStop.getDepartureDateTime());

                    stop.setArrivalDateTime(arrivalDateTime);
                    stop.setDepartureDateTime(departureDateTime);

                    Integer stopMinutes = getLayoverToNextSegmentsInMinutes(
                            arrivalDateTime,
                            departureDateTime
                    );

                    stop.setStopDurationInMinutes(stopMinutes);

                    timeZone = getTimeZone(air.getDepartureAirport());

                    if (null != timeZone) {
                        stop.setTimeZoneId(timeZone.getTimeZoneId());
                        stop.setTimeZoneName(timeZone.getTimeZoneName());
                        stop.setArrivalDstOffset(timeZone.getDstOffset());
                        stop.setArrivalRawOffset(timeZone.getRawOffset());
                    }

                    segment.getStops().add(stop);
                }
            }

            BookedSegment bookedSegment = new BookedSegment();
            bookedSegment.setSegment(segment);

            if ("MM".equalsIgnoreCase(segment.getSegmentStatus())) {
                bookedLeg.setStandByReservation(true);
            }

            bookedLeg.getSegments().getCollection().add(bookedSegment);

            if (sequence > 1 && (null != air.isIsPast() || !air.isIsPast())) {
                //Calculate Lay Over Time Next Itinerary
                //Get Previous Itinerary.

                if (bookedLeg.getSegments().getCollection().size() >= 2) {
                    String arrivalDateTime = bookedLeg.getSegments().getCollection().get(
                            bookedLeg.getSegments().getCollection().size() - 2
                    ).getSegment().getArrivalDateTime();

                    int minutesLayOverToNextFlight = getLayoverToNextSegmentsInMinutes(
                            arrivalDateTime,
                            segment.getDepartureDateTime()
                    );

                    bookedLeg.getSegments().getCollection().get(
                            bookedLeg.getSegments().getCollection().size() - 2
                    ).getSegment().setLayoverToNextSegmentsInMinutes(minutesLayOverToNextFlight);
                }
            }
        }

        if (null == bookedLegCollection.getCollection() || bookedLegCollection.getCollection().isEmpty()) {
            if (PosType.WEB.toString().equalsIgnoreCase(pnrRQ.getPos())) {
                throw new GenericException(
                        Response.Status.BAD_REQUEST,
                        ErrorType.INVALID_ITINERARY_WEB,
                        "NO AVAILABLE SEGMENTS IN RESERVATION"
                );
            } else {
                throw new GenericException(
                        Response.Status.BAD_REQUEST,
                        ErrorType.INVALID_ITINERARY,
                        "NO AVAILABLE SEGMENTS IN RESERVATION"
                );
            }
        }

        for (BookedLeg bookedLeg : bookedLegCollection.getCollection()) {

            //Update Domestic Leg Flag
            updateDomesticLeg(bookedLeg);

            //Update Boarding Information & checkIn Status
            updateLegBoardingInfoAndCheckinStatus(bookedLeg, isAmTicketingProvider, pnrRQ.getPos());

            //Update leg type
            if (bookedLeg.getSegments().getCollection().size() == 1
                    && bookedLeg.getSegments().getCollection().get(0).getSegment().getStops().isEmpty()) {
                bookedLeg.getSegments().setLegType(LegType.NONSTOP.toString());
            } else {
                bookedLeg.getSegments().setLegType(LegType.CONNECTING.toString());
            }

            //Total Flight Duration
            bookedLeg.getSegments().setTotalFlightTimeInMinutes(
                    getTotalFlightTimeInMinutes(bookedLeg)
            );

            //Leg Code using first Itinerary.
            bookedLeg.getSegments().setLegCode(
                    PNRLookUpServiceUtil.getLegCode(
                            bookedLeg.getSegments().getCollection().get(0).getSegment()
                    )
            );
        }

        PnrUtil.setItineraryPartType(bookedLegCollection);

        return bookedLegCollection;
    }

    private com.aeromexico.timezone.service.TimeZone getTimeZone(String iataAirport) throws Exception {
        try {
            AirportWeather airportWeather = airportService.getAirportFromIata(iataAirport);
            com.aeromexico.timezone.service.TimeZone timeZone = TimeZoneService.getTimeZone(
                    airportWeather.getAirport().getCode(),
                    airportWeather.getAirport().getLatitude(),
                    airportWeather.getAirport().getLongitude()
            );
            return timeZone;
        } catch (Exception ex) {
            LOG.error("PNRLooKUp - getLocalDateTimeByAirport Failed to lookup airport details {}: {}", iataAirport, ex.getMessage());
        }

        return null;
    }

    private void updateTimeInfoInSegment(
            Segment segment,
            ACSFlightDetailRSACS flightDetailRS,
            PnrRQ pnrRQ
    ) throws Exception {

        try {
            if (null != flightDetailRS) {
                ItineraryDetailsACS itineraryDetails;
                itineraryDetails = flightDetailRS.getItineraryResponseList().getItineraryInfoResponse().get(0);
                //Setting boarding gate Message as default.
                segment.setBoardingGate(null == itineraryDetails.getDepartureGate() ? "TBD" : itineraryDetails.getDepartureGate());

                segment.setBoardingTime(
                        CommonUtil.getBoardingTimeString(
                                itineraryDetails.getBoardingTime().getValue(),
                                pnrRQ.getPos()
                        )
                );

                if (segment.getDepartureAirport().equalsIgnoreCase(itineraryDetails.getOrigin())
                        && (FlightNumberUtil.compareFlightNumbers(segment.getOperatingFlightCode(), itineraryDetails.getFlight())
                        || FlightNumberUtil.compareFlightNumbers(segment.getMarketingFlightCode(), itineraryDetails.getFlight()))) {

                    segment.setEstimatedDepartureTime(
                            CommonUtil.getFormattedDateTimeString(
                                    itineraryDetails.getEstimatedDepartureDate(),
                                    itineraryDetails.getEstimatedDepartureTime()
                            )
                    );
                    segment.setEstimatedArrivalTime(
                            CommonUtil.getFormattedDateTimeString(
                                    itineraryDetails.getEstimatedArrivalDate(),
                                    itineraryDetails.getEstimatedArrivalTime()
                            )
                    );
                    segment.setScheduledDepartureTime(
                            CommonUtil.getFormattedDateTimeString(
                                    itineraryDetails.getScheduledDepartureDate(),
                                    itineraryDetails.getScheduledDepartureTime()
                            )
                    );
                    segment.setScheduledArrivalTime(
                            CommonUtil.getFormattedDateTimeString(
                                    itineraryDetails.getScheduledArrivalDate(),
                                    itineraryDetails.getScheduledArrivalTime()
                            )
                    );


                    segment.setFlightStatus(
                            getFlightStatus(itineraryDetails, itineraryDetails.getFlightStatus())
                    );

                    segment.setCheckinStatus(
                            getCheckinStatus(itineraryDetails, itineraryDetails.getFlightStatus())
                    );

                }

                WindowTimeType windowType;
                try {
                    windowType = getWindowTime(segment, pnrRQ.getPos());
                } catch (Exception ex) {
                    throw new GenericException(Response.Status.INTERNAL_SERVER_ERROR, ErrorType.INTERNAL_ERROR, "PNRLookUp.updateTimeInfoInSegment() - getWindowTime " + ex.getMessage());
                }

                if (BookedLegFlightStatusType.ON_TIME.equals(segment.getFlightStatus())) {
                    segment.setFlightStatus(getFlightStatusByWindowTime(windowType));
                }
            } else {

                WindowTimeType windowType;
                try {
                    windowType = getWindowTime(segment, pnrRQ.getPos());
                } catch (Exception ex) {
                    throw new GenericException(Response.Status.INTERNAL_SERVER_ERROR, ErrorType.INTERNAL_ERROR, "PNRLookUp.updateTimeInfoInSegment() - getWindowTime " + ex.getMessage());
                }

                //Calculating if the flight is already flown.
                segment.setFlightStatus(getFlightStatusByWindowTime(windowType));
                //When occur an errror calling Flight Details.

                segment.setBoardingGate("TBD");
            }
        } catch (Exception ex2) {
            //If an error Occurs.
            segment.setBoardingGate("-");
            segment.setFlightStatus(BookedLegFlightStatusType.FLOWN);
            segment.setBoardingGate("-");

            LOG.error("PNRLookUp.updateTimeInfoInSegment()" + ex2.getMessage(), ex2);
        }
    }

    /**
     * @param arrivalDateTime
     * @param departureDateTime
     * @return IN "2016-06-04T20:29:00" OUT duration in minutes
     */
    private Integer getLayoverToNextSegmentsInMinutes(String arrivalDateTime, String departureDateTime) {
        try {
            Date dateArrivalDateTime = CommonServiceUtil.parseSabreDateTime(arrivalDateTime);
            Date dateDepartureDateTime = CommonServiceUtil.parseSabreDateTime(departureDateTime);

            long diff = dateDepartureDateTime.getTime() - dateArrivalDateTime.getTime();
            long diffMinutes = diff / (60 * 1000);

            Integer result = Integer.parseInt(String.valueOf(diffMinutes));
            return result;
        } catch (Exception e) {
            return 0;
        }
    }

    private int getTotalFlightTimeInMinutes(BookedLeg bookedLeg) {
        int totalFlightTimeInMinutes = 0;
        for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {
            if (null == bookedSegment.getSegment().getFlightDurationInMinutes()) {
                bookedSegment.getSegment().setFlightDurationInMinutes(0);
            }

            totalFlightTimeInMinutes += bookedSegment.getSegment().getFlightDurationInMinutes();

            if (null == bookedSegment.getSegment().getLayoverToNextSegmentsInMinutes()) {
                bookedSegment.getSegment().setLayoverToNextSegmentsInMinutes(0);
            }

            totalFlightTimeInMinutes += bookedSegment.getSegment().getLayoverToNextSegmentsInMinutes();
        }
        return totalFlightTimeInMinutes;
    }

    /**
     * @param bookedLeg
     */
    private void updateDomesticLeg(BookedLeg bookedLeg) {
        boolean isDomesticLeg;
        isDomesticLeg = isLegDomestic(bookedLeg.getSegments());
        bookedLeg.setDomestic(isDomesticLeg);
    }

    private boolean isLegDomestic(BookedSegmentCollection bookedSegmentCollection) {
        for (BookedSegment bookedSegment : bookedSegmentCollection.getCollection()) {
            if (!bookedSegment.getSegment().isDomestic()) {
                return false;
            }
        }
        return true;
    }

    /**
     *
     * @param bookedLeg
     * @param isAmTicketingProvider
     * @param pos
     * @throws Exception
     */
    private void updateLegBoardingInfoAndCheckinStatus(
            BookedLeg bookedLeg,
            boolean isAmTicketingProvider,
            String pos
    ) throws Exception {

        Segment firstEligibleSegment = getFirstEligibleSegment(bookedLeg, pos);

        if (null != firstEligibleSegment) {

            //Updating info at Leg Level (only for kiosk)
            bookedLeg.setBoardingGate(firstEligibleSegment.getBoardingGate());
            bookedLeg.setBoardingTerminal(firstEligibleSegment.getBoardingTerminal());
            bookedLeg.setBoardingTime(firstEligibleSegment.getBoardingTime());
            bookedLeg.setEstimatedDepartureTime(firstEligibleSegment.getEstimatedDepartureTime());
            bookedLeg.setScheduledDepartureTime(firstEligibleSegment.getScheduledDepartureTime());

            bookedLeg.setFlightStatus(firstEligibleSegment.getFlightStatus());
            //Setting CheckIn status for Leg
            setCheckInStatusFromSegment(bookedLeg, firstEligibleSegment, isAmTicketingProvider, pos);

            //Get LookUpTime
            bookedLeg.setLookUpTime(
                    getLookUpWindowTime(
                            firstEligibleSegment.getDepartureAirport(),
                            firstEligibleSegment.getDepartureDateTime(),
                            pos
                    )
            );

            try {
                Date currentTime = airportService.getLocalDateTimeByAirport(
                        firstEligibleSegment.getDepartureAirport(),
                        pos
                );

                Integer remainingTime = null;
                if (null != firstEligibleSegment.getBoardingTime()) {
                    if (null != firstEligibleSegment.getScheduledDepartureTime()) {
                        remainingTime = CommonUtil.getDiffTimeInMinutes(
                                currentTime,
                                firstEligibleSegment.getBoardingTime(),
                                firstEligibleSegment.getScheduledDepartureTime().substring(0, 10),
                                "yyyy-MM-dd HH:mm"
                        );
                    } else if (null != firstEligibleSegment.getDepartureDateTime()) {
                        remainingTime = CommonUtil.getDiffTimeInMinutes(
                                currentTime,
                                firstEligibleSegment.getBoardingTime(),
                                firstEligibleSegment.getDepartureDateTime().substring(0, 10),
                                "yyyy-MM-dd HH:mm"
                        );
                    }

                    if (null != remainingTime) {
                        bookedLeg.setRemainingTimeToBoard(remainingTime);
                    }
                }
            } catch (Exception e) {
                LOG.error(e.getMessage(), e);
                bookedLeg.setRemainingTimeToBoard(0);
            }
        }
    }

    private BookedLegFlightStatusType getFlightStatus(ItineraryDetailsACS itinerary, String flightStatusIt) {

        if ("CANCEL".equalsIgnoreCase(flightStatusIt)) {
            return BookedLegFlightStatusType.CANCELLED;
        } else if ("PDC".equalsIgnoreCase(flightStatusIt)) { //post departure complete
            return BookedLegFlightStatusType.FLOWN;
        } else if ("FINAL".equalsIgnoreCase(flightStatusIt) || "CLOSE".equalsIgnoreCase(flightStatusIt)) {
            return BookedLegFlightStatusType.CLOSED;
        } else if (CommonUtil.getCalendarV2(itinerary.getEstimatedDepartureDate() + " " + itinerary.getEstimatedDepartureTime()).after(CommonUtil.getCalendarV2(itinerary.getScheduledDepartureDate() + " " + itinerary.getScheduledDepartureTime()))) {
            return BookedLegFlightStatusType.DELAYED;
        } else {
            return BookedLegFlightStatusType.ON_TIME;
        }
    }

    private BookedLegCheckinStatusType getCheckinStatus(ItineraryDetailsACS itinerary, String flightStatusIt) {

        if ("CANCEL".equalsIgnoreCase(flightStatusIt)) {
            return BookedLegCheckinStatusType.CANCEL;
        } else if ("PDC".equalsIgnoreCase(flightStatusIt)) { //post departure complete
            return BookedLegCheckinStatusType.CLOSED;
        } else if ("FINAL".equalsIgnoreCase(flightStatusIt)) {
            return BookedLegCheckinStatusType.FINAL;
        } else if ("CLOSE".equalsIgnoreCase(flightStatusIt)) {
            return BookedLegCheckinStatusType.CLOSED;
        } else if (CommonUtil.getCalendarV2(itinerary.getEstimatedDepartureDate() + " " + itinerary.getEstimatedDepartureTime()).after(CommonUtil.getCalendarV2(itinerary.getScheduledDepartureDate() + " " + itinerary.getScheduledDepartureTime()))) {
            return BookedLegCheckinStatusType.OPEN;
        } else {
            return BookedLegCheckinStatusType.OPEN;
        }
    }

    private BookedLegFlightStatusType getFlightStatusByWindowTime(WindowTimeType windowType) {
        if (WindowTimeType.EARLY_CKIN.toString().equalsIgnoreCase(windowType.toString())) {
            return BookedLegFlightStatusType.ON_TIME;
        } else if (WindowTimeType.IN_CKWINDOW.toString().equalsIgnoreCase(windowType.toString())) {
            return BookedLegFlightStatusType.ON_TIME;
        } else if (WindowTimeType.LATE_CKIN.toString().equalsIgnoreCase(windowType.toString())) {
            return BookedLegFlightStatusType.FLOWN;
        } else {
            return BookedLegFlightStatusType.FLOWN;
        }
    }

    /**
     * @param segment
     * @return
     * @throws ParseException
     * @throws Exception
     */
    public WindowTimeType getWindowTime(
            Segment segment,
            String pos
    ) throws ParseException, Exception {
        String origin = segment.getDepartureAirport();
        String arrival = segment.getArrivalAirport();

        if (segment.getEstimatedDepartureTime() != null && !segment.getEstimatedDepartureTime().trim().isEmpty()) {
            return getWindowTime(origin, arrival, segment.getEstimatedDepartureTime(), segment.isDomestic(), pos);
        }

        return getWindowTime(origin, arrival, segment.getDepartureDateTime(), segment.isDomestic(), pos);
    }

    /**
     * @param origin
     * @param arrival
     * @param departureDateTime
     * @param isDomestic
     * @return
     * @throws Exception
     */
    private WindowTimeType getWindowTime(
            String origin,
            String arrival,
            String departureDateTime,
            boolean isDomestic,
            String pos
    ) throws Exception {

        int windowTimeInMinutes;
        if (isDomestic) {
            windowTimeInMinutes = SystemVariablesUtil.getDomesticCheckinWindowTimeInHrs() * 60;
        } else {
            windowTimeInMinutes = SystemVariablesUtil.getInternationalCheckinWindowTimeInHrs() * 60;
        }

        SimpleDateFormat parseFormat = new SimpleDateFormat(DATE_TIME_PATTERN);
        Date scheduledDepartureDate = parseFormat.parse(
                CommonUtil.getFormattedFullDateTime(departureDateTime)
        );
        Date currentTime = airportService.getLocalDateTimeByAirport(origin, pos);

        LOG.info("LocalDateTimeByAirport: {}", origin);
        LOG.info("LocalDateTimeByAirport: {}", currentTime);

        long diff = scheduledDepartureDate.getTime() - currentTime.getTime();
        long diffMinutes = diff / (60 * 1000);

        LOG.info("diffMinutes: {}", diffMinutes);
        LOG.info("windowTimeInMinutes: {}", windowTimeInMinutes);

        //Return 0, when is in check in window
        if (diffMinutes > 0 && diffMinutes <= windowTimeInMinutes) {
            return WindowTimeType.IN_CKWINDOW;
        }

        //Return 1, when is to early for check in
        if (diffMinutes > windowTimeInMinutes) {
            return WindowTimeType.EARLY_CKIN;
        }

        //Return -1, when is to late for check in
        if (diffMinutes < windowTimeInMinutes) {
            return WindowTimeType.LATE_CKIN;
        }

        return WindowTimeType.IN_CKWINDOW;
    }

    public Segment getFirstEligibleSegment(BookedLeg bookedLeg, String pos) {

        //Are All flown Flights - return latest segment.
        if (PNRLookUpServiceUtil.areAllFlownFlightsFromLeg(bookedLeg)) {
            return bookedLeg.getLatestSegmentInLeg().getSegment();
        }

        //Are All closed Flights - return latest segment.
        if (PNRLookUpServiceUtil.areAllClosedFlightsFromLeg(bookedLeg)) {
            return bookedLeg.getLatestSegmentInLeg().getSegment();
        }

        //Are All future Flights
        if (areAllFutureFlightsFromLeg(bookedLeg, pos)) {
            return bookedLeg.getFirstSegmentInLeg().getSegment();
        }

        for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {
            if (BookedLegFlightStatusType.ON_TIME.equals(bookedSegment.getSegment().getFlightStatus())
                    || BookedLegFlightStatusType.DELAYED.equals(bookedSegment.getSegment().getFlightStatus())) {
                return bookedSegment.getSegment();
            }
        }

        //this should not happen
        return bookedLeg.getFirstSegmentInLeg().getSegment();
    }

    public boolean areAllFutureFlightsFromLeg(BookedLeg bookedLeg, String pos) {
        for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {
            try {
                WindowTimeType windowTimeType = getWindowTime(bookedSegment.getSegment(), pos);

                if (!WindowTimeType.EARLY_CKIN.equals(windowTimeType)) {
                    return false;
                }
            } catch (Exception ex) {
                return false;
            }
        }

        return true;
    }

    public void setManageStatus(PNR pnr, String pos) {
        boolean areThereFutureFLights = false;
        boolean areThereOpenFLights = false;

        if (null != pnr.getLegs() && null != pnr.getLegs().getCollection()) {
            for (BookedLeg bookedLeg : pnr.getLegs().getCollection()) {
                if (null != bookedLeg.getCheckinStatus()) {
                    switch (bookedLeg.getCheckinStatus()) {
                        case OPEN:
                        case OPENCI:
                        case FINAL:
                        case BOARDING:
                            areThereOpenFLights = true;
                            break;
                        case CLOSED_DOMESTIC_WINDOW:
                        case CLOSED_INTERNATIONAL_WINDOW:
                            areThereFutureFLights = true;
                            bookedLeg.setManageStatus(ManageStatus.OPEN);
                            break;
                        case CLOSED_NOT_OPERATED_BY_AM:
                        case CLOSED_GROUND_HANDLING_BY_DL:
                        case CLOSED_GROUND_HANDLING_BY_AF:
                        case CLOSED_GROUND_HANDLING_BY_KL:
                        case CLOSED_GROUND_HANDLING_BY_LF:
                        case CLOSED_GROUND_HANDLING_BY_LH:
                        case CLOSED_GROUND_HANDLING_BY_ATO:

                            try {
                                Segment segmentAvailable = getFirstEligibleSegment(bookedLeg,pos);
                                BookedLegCheckinStatusType bookedLegCheckinStatusType;
                                bookedLegCheckinStatusType = getCheckInStatusFromWindowTime(
                                        segmentAvailable,
                                        pos
                                );

                                if (BookedLegCheckinStatusType.CLOSED_DOMESTIC_WINDOW.equals(bookedLegCheckinStatusType)
                                        || BookedLegCheckinStatusType.CLOSED_INTERNATIONAL_WINDOW.equals(bookedLegCheckinStatusType)) {

                                    bookedLeg.setManageStatus(ManageStatus.OPEN);
                                    areThereFutureFLights = true;
                                }

                            } catch (Exception ex) {
                                LOG.error(ex.getMessage(), ex);
                            }

                            break;
                        default:
                            break;
                    }
                }
            }

            if (!areThereOpenFLights && areThereFutureFLights) {
                pnr.setManageStatus(ManageStatus.OPEN);
            } else if (areThereOpenFLights) {
                pnr.setCheckinStatus(ManageStatus.OPEN);
            }
        }
    }

    /**
     *
     * @param bookedLeg
     * @param firstEligibleSegment
     * @param isAmTicketingProvider
     * @throws Exception
     */
    private void setCheckInStatusFromSegment(
            BookedLeg bookedLeg,
            Segment firstEligibleSegment,
            boolean isAmTicketingProvider, // for future
            String pos
    ) throws Exception {

        BookedLegCheckinStatusType bookedLegCheckinStatusType = null;
        BookedLegCheckinStatusType originalBookedLegCheckinStatusType = null;

        if (AirlineCodeType.AM.toString().equalsIgnoreCase(firstEligibleSegment.getOperatingCarrier())) { //operated by AM

            String groundHandled = validateGroundHandledByDepartureAirport(firstEligibleSegment.getDepartureAirport());

            if (groundHandled.contains("GROUND")) {
                bookedLeg.setGroundHandled(true);
                bookedLeg.setGroundedReservation(true);
            }

            if (bookedLeg.isGroundHandled()) { //ground handled by other airline

                bookedLegCheckinStatusType = getGroundHandledStatus(groundHandled);
                originalBookedLegCheckinStatusType = bookedLegCheckinStatusType;

                if (bookedLeg.isStandByReservation()) { //staff standby reservation passenger, allow then to be added to priority list (normal flow)
                    if (SystemVariablesUtil.isEarlyCheckinEnabled()) {
                        long timeToDepartureInMinutes;

                        if (null != firstEligibleSegment.getEstimatedDepartureTime()
                                && !firstEligibleSegment.getEstimatedDepartureTime().trim().isEmpty()) {
                            timeToDepartureInMinutes = airportService.getTimeToDepartureInMinutes(
                                    firstEligibleSegment.getDepartureAirport(),
                                    firstEligibleSegment.getEstimatedDepartureTime(),
                                    pos
                            );
                        } else {
                            timeToDepartureInMinutes = airportService.getTimeToDepartureInMinutes(
                                    firstEligibleSegment.getDepartureAirport(),
                                    firstEligibleSegment.getDepartureDateTime(),
                                    pos
                            );
                        }

                        if (timeToDepartureInMinutes <= (SystemVariablesUtil.getEarlyCheckinWindowTimeLimitInHrs() * 60)) {
                            bookedLeg.setClosedForEarlyChk(true);
                        } else {
                            bookedLegCheckinStatusType = getCheckInStatusFromWindowTime(firstEligibleSegment, pos);
                            originalBookedLegCheckinStatusType = bookedLegCheckinStatusType;
                        }
                    }
                } else { //confirmed ground handled reservations, seamless checkin for allowed airlines
                    if (SystemVariablesUtil.isSeamlessCheckinGhEnabled()
                            && (SystemVariablesUtil.isSeamlessCheckinForNotAMMarketedEnabled()                                      // and seamless checkin enabled for not am marketed
                            || AirlineCodeType.AM.getCode().equalsIgnoreCase(firstEligibleSegment.getMarketingCarrier()))) {                                               // if seamless checkin is enabled

                        if (BookedLegCheckinStatusType.CLOSED_GROUND_HANDLING_BY_DL.equals(bookedLegCheckinStatusType)) { // only DL for now
                            bookedLeg.setSeamlessCheckin(true);
                            bookedLeg.setOperatingHandler(AirlineCodeType.DL.getCode());
                            bookedLeg.setGroundHandled(false);
                            bookedLeg.setGroundedReservation(false);
                            bookedLegCheckinStatusType = getCheckInStatusFromWindowTime(firstEligibleSegment, pos);
                        }
//                        else if (BookedLegCheckinStatusType.CLOSED_GROUND_HANDLING_BY_KL.equals(bookedLegCheckinStatusType)) { // for KL
//                            bookedLeg.setSeamlessCheckin(true);
//                            bookedLeg.setOperatingHandler(AirlineCodeType.KL.getCode());
//                            bookedLeg.setGroundHandled(false);
//                            bookedLeg.setGroundedReservation(false);
//                            bookedLegCheckinStatusType = getCheckInStatusFromWindowTime(firstEligibleSegment, pos);
//                        } else if (BookedLegCheckinStatusType.CLOSED_GROUND_HANDLING_BY_AF.equals(bookedLegCheckinStatusType)) { // for AF
//                            bookedLeg.setSeamlessCheckin(true);
//                            bookedLeg.setOperatingHandler(AirlineCodeType.AF.getCode());
//                            bookedLeg.setGroundHandled(false);
//                            bookedLeg.setGroundedReservation(false);
//                            bookedLegCheckinStatusType = getCheckInStatusFromWindowTime(firstEligibleSegment, pos);
//                        }
                        //else { // bookedLegCheckinStatusType already set, nothing to do
                        //}
                    }
                    //else { // bookedLegCheckinStatusType already set, nothing to do
                    //}
                }
            } else { //ground handled by AM
                bookedLegCheckinStatusType = getCheckInStatusFromWindowTime(firstEligibleSegment, pos);
                originalBookedLegCheckinStatusType = bookedLegCheckinStatusType;
            }
        } else { //operated by other airline
            bookedLegCheckinStatusType = BookedLegCheckinStatusType.CLOSED_NOT_OPERATED_BY_AM;
            originalBookedLegCheckinStatusType = bookedLegCheckinStatusType;

            if (SystemVariablesUtil.isSeamlessCheckinOpEnabled()                                                            // if seamless checkin is enabled
                    && (SystemVariablesUtil.isSeamlessCheckinForNotAMMarketedEnabled()                                      // and seamless checkin enabled for not am marketed
                    || AirlineCodeType.AM.getCode().equalsIgnoreCase(firstEligibleSegment.getMarketingCarrier()))) {        // or if if marketed by AM

                if (AirlineCodeType.DL.getCode().equalsIgnoreCase(firstEligibleSegment.getOperatingCarrier())) {            //  only for DL flights now
                    bookedLeg.setSeamlessCheckin(true);
                    bookedLeg.setOperatingHandler(AirlineCodeType.DL.getCode());
                    bookedLegCheckinStatusType = getCheckInStatusFromWindowTime(firstEligibleSegment, pos);
                }
//                else if (AirlineCodeType.KL.getCode().equalsIgnoreCase(firstEligibleSegment.getOperatingCarrier())) {    // for KL flights
//                    bookedLeg.setSeamlessCheckin(true);
//                    bookedLeg.setOperatingHandler(AirlineCodeType.KL.getCode());
//                    bookedLegCheckinStatusType = getCheckInStatusFromWindowTime(firstEligibleSegment, pos);
//                } else if (AirlineCodeType.AF.getCode().equalsIgnoreCase(firstEligibleSegment.getOperatingCarrier())) {    // for AF flights
//                    bookedLeg.setSeamlessCheckin(true);
//                    bookedLeg.setOperatingHandler(AirlineCodeType.AF.getCode());
//                    bookedLegCheckinStatusType = getCheckInStatusFromWindowTime(firstEligibleSegment, pos);
//                }
                else {// verify that is not close
                    try {
                        BookedLegCheckinStatusType checkinStatusType = getCheckInStatusFromWindowTime(firstEligibleSegment, pos);

                        if (checkinStatusType.equals(BookedLegCheckinStatusType.CLOSED_DOMESTIC_WINDOW)
                                || checkinStatusType.equals(BookedLegCheckinStatusType.CLOSED_INTERNATIONAL_WINDOW)) {
                            bookedLegCheckinStatusType = checkinStatusType;
                            //originalBookedLegCheckinStatusType = checkinStatusType;
                        }
                    } catch(Exception ex) {
                        //
                    }
                }
            }
            else {// verify that is not close
                try {
                    BookedLegCheckinStatusType checkinStatusType = getCheckInStatusFromWindowTime(firstEligibleSegment, pos);

                    if (checkinStatusType.equals(BookedLegCheckinStatusType.CLOSED_DOMESTIC_WINDOW)
                            || checkinStatusType.equals(BookedLegCheckinStatusType.CLOSED_INTERNATIONAL_WINDOW)) {
                        bookedLegCheckinStatusType = checkinStatusType;
                        //originalBookedLegCheckinStatusType = checkinStatusType;
                    }
                } catch(Exception ex) {
                    //
                }
            }
        }

        bookedLeg.setCheckinStatus(bookedLegCheckinStatusType);
        bookedLeg.setOriginalCheckinStatus(originalBookedLegCheckinStatusType);
    }

    private BookedLegCheckinStatusType getGroundHandledStatus(String groundHandled) {
        BookedLegCheckinStatusType bookedLegCheckinStatusType = null;

        if (groundHandled.contains("DL")) {
            bookedLegCheckinStatusType = BookedLegCheckinStatusType.CLOSED_GROUND_HANDLING_BY_DL;
        } else if (groundHandled.contains("AF")) {
            bookedLegCheckinStatusType = BookedLegCheckinStatusType.CLOSED_GROUND_HANDLING_BY_AF;
        } else if (groundHandled.contains("KL")) {
            bookedLegCheckinStatusType = BookedLegCheckinStatusType.CLOSED_GROUND_HANDLING_BY_KL;
        } else if (groundHandled.contains("LF")) {
            bookedLegCheckinStatusType = BookedLegCheckinStatusType.CLOSED_GROUND_HANDLING_BY_LF;
        } else if (groundHandled.contains("LH")) {
            bookedLegCheckinStatusType = BookedLegCheckinStatusType.CLOSED_GROUND_HANDLING_BY_LH;
        } else if (groundHandled.contains("ATO")) {
            bookedLegCheckinStatusType = BookedLegCheckinStatusType.CLOSED_GROUND_HANDLING_BY_ATO;
        } else {
            bookedLegCheckinStatusType = BookedLegCheckinStatusType.CLOSED;
        }

        return bookedLegCheckinStatusType;
    }

    public BookedLegCheckinStatusType getCheckInStatusFromWindowTime(
            Segment segment, String pos
    ) throws Exception {
        WindowTimeType windowTime = getWindowTime(segment, pos);

        if (WindowTimeType.EARLY_CKIN.equals(windowTime)) {
            if (segment.isDomestic()) {
                return BookedLegCheckinStatusType.CLOSED_DOMESTIC_WINDOW;
            } else {
                return BookedLegCheckinStatusType.CLOSED_INTERNATIONAL_WINDOW;
            }
        } else if (BookedLegFlightStatusType.FLOWN.equals(segment.getFlightStatus())) {
            return BookedLegCheckinStatusType.CLOSED;
        } else if (BookedLegFlightStatusType.ON_TIME.equals(segment.getFlightStatus())
                || BookedLegFlightStatusType.DELAYED.equals(segment.getFlightStatus())) {
            return BookedLegCheckinStatusType.OPEN;
        } else if (BookedLegCheckinStatusType.FINAL.equals(segment.getCheckinStatus())) {
            return BookedLegCheckinStatusType.FINAL;
        } else {
            return BookedLegCheckinStatusType.CLOSED;
        }
    }

    /**
     * Method used to validate Ground Handled by Third Party Airlines
     */
    private String validateGroundHandledByDepartureAirport(String departAirport) {
        try {
            GroundHandling groundHandling = airportService.getGroundHandledAirport(departAirport);
            if (null != groundHandling) {
                return "GROUND_" + groundHandling.getGroundHandling();
            }
        } catch (Exception ex) {
            LOG.error(
                    "PNRLookUpService - validateGroundHandledByDepartureAirport (), Error retrieving GroundHandling - "
                            + departAirport
                            + "error: "
                            + ex.getMessage()
            );
        }
        return "";
    }

    public String validateCheckinRestrictedSanitaryReasons(String departure, String arrival) {
        try {
            RestrictedCheckin restrictedCheckinDep = airportService.getRestrictedCheckinAirport(departure);
            RestrictedCheckin restrictedCheckinArr = airportService.getRestrictedCheckinAirport(arrival);
            if (null != restrictedCheckinDep || null != restrictedCheckinArr) {
                String restrictedCheckinReason = "";
                if (null != restrictedCheckinDep) {
                    restrictedCheckinReason = restrictedCheckinDep.getReasonRestriction();
                }
                if (null != restrictedCheckinArr) {
                    restrictedCheckinReason = restrictedCheckinArr.getReasonRestriction();
                }
                return "CHECKIN_NOT_ALLOWED_" + restrictedCheckinReason;
            }
        } catch (Exception ex) {
            LOG.info(
                    "PNRLookUpService - validateGroundHandledByDepartureAirport (), Error retrieving RestrictedCheckin - "
                            + departure + " " + arrival
                            + "error: "
                            + ex.getMessage()
            );
        }
        return "";
    }

    private long getLookUpWindowTime(String origin, String departureDateTime, String pos) throws Exception {
        long looKUpTimeHours = 0;

        SimpleDateFormat parseFormat = new SimpleDateFormat(DATE_TIME_PATTERN);
        Date scheduledDepartureDate = parseFormat.parse(CommonUtil.getFormattedFullDateTime(departureDateTime));
        Date currentTime = airportService.getLocalDateTimeByAirport(origin, pos);

        long differenceInMinutes = (scheduledDepartureDate.getTime() - currentTime.getTime()) / (60 * 1000);

        if (differenceInMinutes > 0) {
            looKUpTimeHours = (differenceInMinutes / 60);
        }
        return Math.round(looKUpTimeHours);
    }

    public static Set<String> getFormOfPayment(GetTicketingDocumentRS ticketingDocumentRS) {
        Set<String> fops = new HashSet<>();

        try {
            if (null != ticketingDocumentRS && null != ticketingDocumentRS.getAbbreviated()) {
                for (TicketingDocumentInfoAbbreviated ticketingDocumentInfoAbbreviated : ticketingDocumentRS.getAbbreviated()) {
                    for (TicketingDocumentPayment ticketingDocumentPaymentAbbreviated : ticketingDocumentInfoAbbreviated.getTicketingDocument().getPayment()) {
                        if (null != ticketingDocumentPaymentAbbreviated.getType()) {
                            fops.add(ticketingDocumentPaymentAbbreviated.getType().toUpperCase());
                        }
                    }
                }
            }
        } catch (Exception ex) {
            //
        }

        return fops;
    }

    /**
     * @param pnr
     * @param formOfPayments
     * @param pnrRQ
     */
    public void setTsaValidation(PNR pnr, Set<String> formOfPayments, PnrRQ pnrRQ) {
        try {

            if (PosType.KIOSK.toString().equalsIgnoreCase(pnrRQ.getPos())
                    && pnrRQ.isOverrideTsa()) {
                return;
            }

            //2018-04-05T15:35:00
            ZonedDateTime creationDate = TimeUtil.getZonedDateTime(pnr.getCreationDate(), "GMT", DATE_TIME_PATTERN);

            boolean isCash = null != formOfPayments && !formOfPayments.isEmpty() && formOfPayments.contains(FOPType.CA.toString());

            boolean noRoundTrip = pnr.getLegs().isOneWay() || pnr.getLegs().isMultiCity();

            for (BookedLeg bookedLeg : pnr.getLegs().getBookedLegsOpenForCheckin()) {
                BookedSegment firstBookedSegment = ListsUtil.getFirst(bookedLeg.getSegments().getCollection());

                String zoneId;
                if (null == firstBookedSegment.getSegment().getTimeZoneId()) {
                    zoneId = "GMT";
                } else {
                    zoneId = firstBookedSegment.getSegment().getTimeZoneId();
                }

                ZonedDateTime departure = TimeUtil.getZonedDateTime(firstBookedSegment.getSegment().getDepartureDateTime(), zoneId);
                //ZonedDateTime _24HourBeforeDeparture = departure.plus(Period.ofDays(1));

                long hours = ChronoUnit.HOURS.between(creationDate, departure);

                boolean wasCreated24orLessBeforeDeparture = hours >= 0L && hours <= 24L;

                boolean isTsaCountry = false;

                for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {
                    try {
                        AirportWeather airport = airportService.getAirportFromIata(
                                bookedSegment.getSegment().getArrivalAirport()
                        );

                        isTsaCountry = null != airport
                                && null != airport.getAirport()
                                && ("US".equalsIgnoreCase(airport.getAirport().getCountry())
                        );

                        if (isTsaCountry) {
                            break;
                        }
                    } catch (Exception ex) {
                        //
                    }
                }

                if (isTsaCountry
                        && (noRoundTrip || wasCreated24orLessBeforeDeparture || isCash)) {
                    pnr.setTsaRequired(true);
                    break;
                }
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
    }

    public void setAmenitiesServiceCheckInMyb( AmenitiesServiceCheckInMyb amenitiesService ) {
    	this.amenitiesService = amenitiesService;
    }

    public AmenitiesServiceCheckInMyb getAmenitiesServiceCheckInMyb() {
    	return this.amenitiesService;
    }

    public void setAirportService(AirportService airportService) {
    	this.airportService = airportService;
    }

    public AirportService getAirportService() {
    	return this.airportService;
    }
}
