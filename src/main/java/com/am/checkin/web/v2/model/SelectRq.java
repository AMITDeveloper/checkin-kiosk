package com.am.checkin.web.v2.model;

import com.aeromexico.commons.model.SelectPassengersRq;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SelectRq extends SelectPassengersRq {

    @SerializedName("error")
    @Expose
    private String error;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
