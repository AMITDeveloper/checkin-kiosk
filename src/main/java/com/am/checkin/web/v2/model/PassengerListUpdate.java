package com.am.checkin.web.v2.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

public class PassengerListUpdate implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Gson GSON = new GsonBuilder()
            .serializeNulls()
            .excludeFieldsWithoutExposeAnnotation()
            .create();

    @SerializedName("itinerary")
    @Expose
    @NotNull(message = "{passengerListUpdate.itinerary.notnull}")
    private Itinerary itinerary;

    @SerializedName("passengers")
    @Expose
    @NotNull(message = "{passengerListUpdate.passengers.notnull}")
    @NotEmpty(message = "{passengerListUpdate.passengers.notempty}")
    private List<BookedTraveler> passengers;

    @SerializedName("client")
    @Expose
    private String client;

    public Itinerary getItinerary() {
        return itinerary;
    }

    public void setItinerary(Itinerary itinerary) {
        this.itinerary = itinerary;
    }

    public List<BookedTraveler> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<BookedTraveler> passengers) {
        this.passengers = passengers;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public static class Itinerary implements Serializable {

        private static final long serialVersionUID = 1L;

        @SerializedName("airlineCode")
        @Expose
        @NotNull(message = "{itinerary.airlineCode.notnull}")
        @NotEmpty(message = "{itinerary.airlineCode.notempty}")
        private String airlineCode;

        @SerializedName("flightNumber")
        @Expose
        @NotNull(message = "{itinerary.flightNumber.notnull}")
        @NotEmpty(message = "{itinerary.flightNumber.notempty}")
        private String flightNumber;

        @SerializedName("bookingClass")
        @Expose
        @NotNull(message = "{itinerary.bookingClass.notnull}")
        @NotEmpty(message = "{itinerary.bookingClass.notempty}")
        private String bookingClass;

        @SerializedName("departureDate")
        @Expose
        @NotNull(message = "{itinerary.departureDate.notnull}")
        @NotEmpty(message = "{itinerary.departureDate.notempty}")
        private String departureDate;

        @SerializedName("origin")
        @Expose
        @NotNull(message = "{itinerary.origin.notnull}")
        @NotEmpty(message = "{itinerary.origin.notempty}")
        private String origin;

        @SerializedName("destination")
        @Expose
        @NotNull(message = "{itinerary.destination.notnull}")
        @NotEmpty(message = "{itinerary.destination.notempty}")
        private String destination;

        public String getAirlineCode() {
            return airlineCode;
        }

        public void setAirlineCode(String airlineCode) {
            this.airlineCode = airlineCode;
        }

        public String getFlightNumber() {
            return flightNumber;
        }

        public void setFlightNumber(String flightNumber) {
            this.flightNumber = flightNumber;
        }

        public String getBookingClass() {
            return bookingClass;
        }

        public void setBookingClass(String bookingClass) {
            this.bookingClass = bookingClass;
        }

        public String getDepartureDate() {
            return departureDate;
        }

        public void setDepartureDate(String departureDate) {
            this.departureDate = departureDate;
        }

        public String getOrigin() {
            return origin;
        }

        public void setOrigin(String origin) {
            this.origin = origin;
        }

        public String getDestination() {
            return destination;
        }

        public void setDestination(String destination) {
            this.destination = destination;
        }

        @Override
        public String toString() {
            return GSON.toJson(this);
        }
    }

    public static class BookedTraveler implements Serializable {

        private static final long serialVersionUID = 1L;

        @SerializedName("lastname")
        @Expose
        @NotNull(message = "{passenger.lastname.notnull}")
        @NotEmpty(message = "{passenger.lastname.notempty}")
        private String lastname;

        @SerializedName("passengerId")
        @Expose
        @NotNull(message = "{passenger.passengerId.notnull}")
        @NotEmpty(message = "{passenger.passengerId.notempty}")
        private String passengerId;

        @SerializedName("priorityCode")
        @Expose
        @NotNull(message = "{passenger.priorityCode.notnull}")
        @NotEmpty(message = "{passenger.priorityCode.notempty}")
        private String priorityCode;

        @SerializedName("upgradeCode")
        @Expose
        private String upgradeCode;

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getPassengerId() {
            return passengerId;
        }

        public void setPassengerId(String passengerId) {
            this.passengerId = passengerId;
        }

        public String getPriorityCode() {
            return priorityCode;
        }

        public void setPriorityCode(String priorityCode) {
            this.priorityCode = priorityCode;
        }

        public String getUpgradeCode() {
            return upgradeCode;
        }

        public void setUpgradeCode(String upgradeCode) {
            this.upgradeCode = upgradeCode;
        }

        @Override
        public String toString() {
            return GSON.toJson(this);
        }
    }

    @Override
    public String toString() {
        return GSON.toJson(this);
    }
}
