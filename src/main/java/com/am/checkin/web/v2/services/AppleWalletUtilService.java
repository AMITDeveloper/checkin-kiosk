package com.am.checkin.web.v2.services;

import com.aeromexico.commons.model.SegmentStatus;
import com.aeromexico.commons.model.weather.AirportWeather;
import com.aeromexico.commons.web.types.PosType;
import com.aeromexico.commons.web.types.SeatSectionCodeType;
import com.aeromexico.dao.qualifiers.AirportsCache;
import com.aeromexico.dao.services.IAirportCodesDao;
import com.aeromexico.timezone.service.TimeZoneService;
import com.am.checkin.web.v2.util.BoardingPassesUtil;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.TimeZone;

@Named
@ApplicationScoped
public class AppleWalletUtilService {

    private static final Logger LOG = LoggerFactory.getLogger(AppleWalletUtilService.class);

    /**
     * Dao for get city name for apple-wallet
     */
    @Inject
    @AirportsCache
    private IAirportCodesDao airportCodesDao;

    /**
     * New method to calculate apple wallet for APP or WEB.
     *
     * @param segmentStatus       {@link SegmentStatus}
     * @param barCode             String.
     * @param zone                String.
     * @param firstName           String.
     * @param lastName            String.
     * @param seatNumber          String.
     * @param seatSectionCodeType {@link SeatSectionCodeType}
     * @param skyPriority         boolean.
     * @param tsaPre              boolean.
     * @return String. The apple wallet URL.
     * @throws IOException
     */
    public String calculateAppleWallet(
            SegmentStatus segmentStatus,
            String barCode,
            String zone,
            String firstName,
            String lastName,
            String seatNumber,
            SeatSectionCodeType seatSectionCodeType,
            boolean skyPriority,
            boolean tsaPre,
            String pos,
            String recordLocator,
            String ticketNumber,
            String ffProgram,
            String ffNumber
    ) throws IOException {
        final String ENCODE_UTF_8 = "UTF-8";

        String contextRoot = "/api/v1/";
        if (pos.equalsIgnoreCase(PosType.CHECKIN_MOBILE.toString())) {
            contextRoot += "app/";
        }

        if (barCode != null) {
            barCode = barCode.replace("&lt;", ">");
        }

        StringBuilder sb = new StringBuilder(contextRoot + "checkin/apple-wallet/boarding-pass?");

        StringBuilder aux = new StringBuilder().append(firstName);
        aux.append(" ");
        aux.append(lastName);
        sb.append("passenger=");
        sb.append(URLEncoder.encode(aux.toString(), ENCODE_UTF_8));

        aux = new StringBuilder().append(segmentStatus.getSegment().getOperatingCarrier());
        aux.append(" ");
        aux.append(segmentStatus.getSegment().getOperatingFlightCode());
        sb.append("&flight=");
        sb.append(URLEncoder.encode(aux.toString(), ENCODE_UTF_8));

        sb.append("&departure-airport=");
        sb.append(segmentStatus.getSegment().getDepartureAirport());
        sb.append("&departure-city=");

        try {
            String cityName = airportCodesDao.getAirportWeatherCityNameByIata(
                    segmentStatus.getSegment().getDepartureAirport()
            );
            sb.append(URLEncoder.encode(cityName, ENCODE_UTF_8));
        } catch (Exception ex) {
            LOG.info("Cannot get information of city name from database", ex);
        }

        try {
            AirportWeather airportWeather = airportCodesDao.getAirportWeatherByIata(
                    segmentStatus.getSegment().getDepartureAirport()
            );
            sb.append("&latitude=");
            sb.append(airportWeather.getAirport().getLatitude());
            sb.append("&longitude=");
            sb.append(airportWeather.getAirport().getLongitude());
        } catch (Exception ex) {
            LOG.info("Cannot get city location", ex);
        }

        if (segmentStatus.getSegment().getDepartureDateTime().contains("T")) {
            try {
                String departureDateTime = segmentStatus.getSegment().getDepartureDateTime().substring(0,19);
                LocalDateTime dateTime = LocalDateTime.parse(
                        departureDateTime,
                        DateTimeFormatter.ISO_LOCAL_DATE_TIME
                );
                sb.append("&departure-date=");
                sb.append(URLEncoder.encode(
                        dateTime.format(DateTimeFormatter.ofPattern("MMM dd, yyyy")),
                        ENCODE_UTF_8)
                );
                sb.append("&departure-time=");
                sb.append(URLEncoder.encode(
                        dateTime.format(DateTimeFormatter.ofPattern("hh:mma")),
                        ENCODE_UTF_8)
                );
            } catch (Exception e) {
                LOG.error("Error getting information from departure date and time: ", e);
                sb.append("&departure-date=");
                sb.append("&departure-time=");
            }
        }

        if (segmentStatus.getSegment().getArrivalDateTime().contains("T")) {
            try {
                String arrivalDateTime = segmentStatus.getSegment().getArrivalDateTime().substring(0,19);
                LocalDateTime dateTime = LocalDateTime.parse(
                        arrivalDateTime,
                        DateTimeFormatter.ISO_LOCAL_DATE_TIME
                );
                sb.append("&arrival-date=");
                sb.append(URLEncoder.encode(
                        dateTime.format(DateTimeFormatter.ofPattern("MMM dd, yyyy")),
                        ENCODE_UTF_8)
                );
                sb.append("&arrival-time=");
                sb.append(URLEncoder.encode(
                        dateTime.format(DateTimeFormatter.ofPattern("hh:mma")),
                        ENCODE_UTF_8)
                );
            } catch (Exception e) {
                LOG.error("Error getting information from arrival date and time: ", e);
                sb.append("&arrival-date=");
                sb.append("&arrival-time=");
            }
        }

        sb.append("&boarding-time=");
        String boardingTime = BoardingPassesUtil.formatBoardingTime(segmentStatus.getBoardingTime());
        if (null != boardingTime) {
            sb.append(URLEncoder.encode(boardingTime, ENCODE_UTF_8));
        }

        sb.append("&skyPriority=");
        sb.append(URLEncoder.encode(Boolean.toString(skyPriority), ENCODE_UTF_8));

        sb.append("&tsaPre=");
        sb.append(URLEncoder.encode(Boolean.toString(tsaPre), ENCODE_UTF_8));

        sb.append("&boarding-group=");
        sb.append(zone);

        sb.append("&departure-terminal=");
        sb.append(segmentStatus.getBoardingTerminal());
        sb.append("&departure-gate=");
        sb.append(segmentStatus.getBoardingGate());

        sb.append("&arrival-city=");
        try {
            String cityName = airportCodesDao.getAirportWeatherCityNameByIata(
                    segmentStatus.getSegment().getArrivalAirport()
            );
            sb.append(URLEncoder.encode(cityName, ENCODE_UTF_8));
        } catch (Exception ex) {
            LOG.info("Cannot get information of city name from database", ex);
        }
        sb.append("&arrival-airport=");
        sb.append(segmentStatus.getSegment().getArrivalAirport());

        sb.append("&service-class=");
        if (null != seatSectionCodeType) {
            switch (seatSectionCodeType) {
                case FIRST_CLASS:
                    sb.append("BUSSINESS");
                    break;
                default:
                    sb.append("COACH");
                    break;
            }
        } else {
            sb.append("COACH");
        }

        if (null != seatNumber) {
            sb.append("&seat-assignment=");
            sb.append(seatNumber.trim());
        }

        if (null != recordLocator) {
            sb.append("&pnr=");
            sb.append(recordLocator);
        }

        sb.append("&ticketNumber=");
        sb.append(ticketNumber);

        if (null != ffProgram && null != ffNumber) {
            sb.append("&clubPremier=");
            sb.append(ffProgram + ffNumber);
        }

        sb.append("&operatingCarrier=");
        sb.append(URLEncoder.encode(segmentStatus.getSegment().getOperatingCarrier(), ENCODE_UTF_8));

        sb.append("&barcodeData=");
        sb.append(URLEncoder.encode(barCode, ENCODE_UTF_8));

        sb.append("&relevantDate=");
        try {
            sb.append(URLEncoder.encode(
                    this.getRelevantDate(
                            segmentStatus.getSegment().getDepartureAirport(),
                            segmentStatus.getSegment().getDepartureDateTime()),
                    ENCODE_UTF_8)
            );
        } catch (Exception e) {
            LOG.error("Error creating relevant date: " + e.getMessage());
        }

        return sb.toString();
    }

    /**
     * @param origin
     * @param departureDateTime
     * @return
     * @throws Exception
     */
    private String getRelevantDate(String origin, String departureDateTime) throws Exception {
        LOG.info("+getRelevantDate({}, {})", origin, departureDateTime);
        //Get timeZone for the departure city
        AirportWeather airportWeather = airportCodesDao.getAirportWeatherByIata(origin);
        com.aeromexico.timezone.service.TimeZone timeZone = TimeZoneService.getTimeZone(
                airportWeather.getAirport().getCode(),
                airportWeather.getAirport().getLatitude(),
                airportWeather.getAirport().getLongitude()
        );
        LOG.info("TimeZone: {}", new Gson().toJson(timeZone));

        //Departure date/time based on local time
        //"2017-10-27T15:25:00"
        SimpleDateFormat localTime = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        localTime.setTimeZone(TimeZone.getTimeZone(timeZone.getTimeZoneId()));

        Calendar localCalendar = Calendar.getInstance();
        localCalendar.setTimeZone(TimeZone.getTimeZone(timeZone.getTimeZoneId()));
        localCalendar.setTime(localTime.parse(departureDateTime));

        //Convert departure date/time from local time zone to UTC/GMT
        //"2017-10-27T15:25-05:00"
        SimpleDateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmX");
        utcFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

        String result = utcFormat.format(localCalendar.getTime());
        LOG.info("-getRelevantDate({})", result);

        return result;
    }

    public IAirportCodesDao getAirportCodesDao() {
        return airportCodesDao;
    }

    public void setAirportCodesDao(IAirportCodesDao airportCodesDao) {
        this.airportCodesDao = airportCodesDao;
    }
}
