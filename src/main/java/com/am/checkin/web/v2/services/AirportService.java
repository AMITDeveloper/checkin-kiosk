package com.am.checkin.web.v2.services;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.GroundHandling;
import com.aeromexico.commons.model.RestrictedCheckin;
import com.aeromexico.commons.model.weather.AirportWeather;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.types.PosType;
import com.aeromexico.commons.web.util.Constants;
import com.aeromexico.dao.client.exception.MongoDisconnectException;
import com.aeromexico.dao.qualifiers.AirportsCache;
import com.aeromexico.dao.qualifiers.GroundHandlingCache;
import com.aeromexico.dao.qualifiers.RestrictedCheckinCache;
import com.aeromexico.dao.services.IAirportCodesDao;
import com.aeromexico.dao.services.IGroundHandlingDao;
import com.aeromexico.dao.services.IRestrictedCheckinDao;
import com.aeromexico.dao.util.CommonUtil;
import com.aeromexico.timezone.service.TimeZoneService;

import org.aeromexico.commons.exception.config.ReadProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

@Named
@ApplicationScoped
public class AirportService {
    private static final Logger LOG = LoggerFactory.getLogger(AirportService.class);

    @Inject
    @AirportsCache
    private IAirportCodesDao airportsCodesDao;

    @Inject
    @GroundHandlingCache
    private IGroundHandlingDao groundHandlingDao;

    @Inject
    @RestrictedCheckinCache
    private IRestrictedCheckinDao restrictedCheckinDao;

    @Inject
    private ReadProperties prop;

    public boolean isDomesticFlight(String origin, String destination) throws Exception {

        List<String> catAirportsList = airportsCodesDao.getMexicoAirportsList();

        if (null == catAirportsList) {
            throw new GenericException(Response.Status.INTERNAL_SERVER_ERROR, ErrorType.INTERNAL_ERROR, "PNRLookUpService - isDomesticFlight() - aiportsWeather coll unable to load.");
        }

        return catAirportsList.contains(origin) && catAirportsList.contains(destination);
    }

    /**
     * Input: "MEX_GDL_AM_2016-10-04_0625"
     *
     * @param segmentCode
     * @return
     */
    public boolean isDomesticFlight(String segmentCode) throws Exception {
        String[] parts = segmentCode.split("_");

        return isDomesticFlight(parts[0], parts[1]);
    }

    /**
     * @param iata
     * @return
     */
    public AirportWeather getAirportFromIata(String iata) throws Exception {
        AirportWeather airportWeather = null;
        try {
            airportWeather = airportsCodesDao.getAirportWeatherByIata(iata);
        } catch (Exception e) {
            LOG.info("getCityNameByIATACode - " + iata + "( " + e.getMessage() + " )");
            throw e;
        }
        return airportWeather;
    }

    public Date getLocalDateTimeByAirport(String iataAirport, String pos) throws Exception {
        try {
            AirportWeather airportWeather = getAirportFromIata(iataAirport);
            com.aeromexico.timezone.service.TimeZone timeZone = TimeZoneService.getTimeZone(
                    airportWeather.getAirport().getCode(),
                    airportWeather.getAirport().getLatitude(),
                    airportWeather.getAirport().getLongitude()
            );
            return getLocalDateTime(timeZone);
        }catch (MongoDisconnectException me){
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(pos)) {
                LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).toString(), me.fillInStackTrace());
                throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getCode(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getActionable(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110101).getMessage(),
                        Response.Status.INTERNAL_SERVER_ERROR);
            } else {
                LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110101).toString(), me.fillInStackTrace());
                throw me;
            }
        } catch (Exception ex) {
            LOG.error("PNRLooKUp - getLocalDateTimeByAirport Failed to lookup airport details {" + iataAirport + "}: {}", iataAirport, ex.getMessage());
            throw ex;
        }
    }

    /**
     * @param timeZone
     * @return
     */
    private Date getLocalDateTime(com.aeromexico.timezone.service.TimeZone timeZone) {
        try {
            SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
            dateFormatGmt.setTimeZone(java.util.TimeZone.getTimeZone("GMT"));
            SimpleDateFormat dateFormatLocal = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
            SimpleDateFormat dateFormatYear = new SimpleDateFormat("yyyy");

            Date dateZulu = dateFormatLocal.parse(dateFormatGmt.format(new Date()));
            int year = Integer.parseInt(dateFormatYear.format(dateZulu));

            Calendar calendarZulu = new GregorianCalendar(year, dateZulu.getMonth(), dateZulu.getDate(), dateZulu.getHours(), dateZulu.getMinutes());

            int rawOffSet = Integer.parseInt(String.valueOf(timeZone.getRawOffset())) / 60;
            int dstOffSet = Integer.parseInt(String.valueOf(timeZone.getDstOffset())) / 60;
            calendarZulu.add(Calendar.MINUTE, rawOffSet + dstOffSet);
            return calendarZulu.getTime();
        } catch (NumberFormatException | ParseException ex) {
            //If fails return localMachine Time to avoid fail.
            return Date.from(timeZone.getLocalTime().toInstant());
        }
    }

    /**
     * @param origin
     * @param departureDateTime
     * @return
     * @throws ParseException
     * @throws Exception
     */
    public long getTimeToDepartureInMinutes(String origin, String departureDateTime, String pos) throws Exception {

        SimpleDateFormat parseFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date scheduledDepartureDate = parseFormat.parse(
                CommonUtil.getFormattedFullDateTime(departureDateTime)
        );
        Date currentTime = getLocalDateTimeByAirport(origin, pos);

        long diff = scheduledDepartureDate.getTime() - currentTime.getTime();
        long diffMinutes = diff / (60 * 1000);

        LOG.info("diffMinutes: " + diffMinutes);

        return diffMinutes;
    }

    public GroundHandling getGroundHandledAirport(String departAirport) throws Exception {
        return groundHandlingDao.getGroundHandlingByIata(departAirport);
    }

    public RestrictedCheckin getRestrictedCheckinAirport (String iata) throws Exception{
        return restrictedCheckinDao.getRestrictedCheckinByIata(iata);
    }

    public void setAirportCodesDao( IAirportCodesDao airportsCodesDao ) {
    	this.airportsCodesDao = airportsCodesDao;
    }

    public IAirportCodesDao getAirportCodesDao() {
    	return this.airportsCodesDao;
    }

}
