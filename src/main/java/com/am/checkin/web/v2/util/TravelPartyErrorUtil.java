package com.am.checkin.web.v2.util;

import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.web.types.ActionableType;
import com.am.seamless.checkin.common.models.TravelParty;
import com.am.seamless.checkin.common.models.errorhandling.Exception;
import com.am.seamless.checkin.common.models.errorhandling.SeamlessError;
import com.am.seamless.checkin.common.models.exception.SeamlessException;
import com.am.seamless.checkin.common.models.types.ErrorType;
import com.am.seamless.checkin.common.models.types.SeverityCodeType;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class TravelPartyErrorUtil {

    private static DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");

    public static boolean hasSelecteeError(TravelParty travelParty) {

        if (null != travelParty.getExceptions() && !travelParty.getExceptions().isEmpty()) {
            for (Exception exception : travelParty.getExceptions()) {
                if (SeverityCodeType.ERROR.equals(exception.getType())
                        || SeverityCodeType.ERROR_WARING.equals(exception.getType())
                        || SeverityCodeType.WARNING_ERROR.equals(exception.getType())
                        && "Check In Restricted due to Special Handling Required".equalsIgnoreCase(exception.getDescription())
                        && "ID Verification Rqd - Selectee".equalsIgnoreCase(exception.getTechnicalSupplement())) {

                    return true;
                }
            }
        }

        return false;
    }

    public static void validateTravelPartyResponse(TravelParty travelParty, int status) throws java.lang.Exception {

        SeamlessError seamlessError = null;

        if (null != travelParty.getExceptions() && !travelParty.getExceptions().isEmpty()) {
            for (Exception exception : travelParty.getExceptions()) {
                if ((SeverityCodeType.ERROR.equals(exception.getType())
                        || SeverityCodeType.ERROR_WARING.equals(exception.getType())
                        || SeverityCodeType.WARNING_ERROR.equals(exception.getType()))
                        && (!"Check In Restricted due to Special Handling Required".equalsIgnoreCase(exception.getDescription())
                        || !"ID Verification Rqd - Selectee".equalsIgnoreCase(exception.getTechnicalSupplement()))
                        //"Document Validity"
                        //"Admission and Transit Restriction"
                ) {

                    seamlessError = getSeamlessError(exception);


                    break;
                } else if (SeverityCodeType.WARNING.equals(exception.getType())) {
                    if ("Ticket Not Valid For Check In".equalsIgnoreCase(exception.getDescription())
                            && ("Ticket Information not Found".equalsIgnoreCase(exception.getTechnicalSupplement())
                            || "Invalid Coupon Status".equalsIgnoreCase(exception.getTechnicalSupplement())
                    )) {

                        seamlessError = getSeamlessError(exception);

                        break;

                    } else if ("After Checkin End Date Time".equalsIgnoreCase(exception.getDescription())
                        //&& "Origin Flight Departed".equalsIgnoreCase(exception.getTechnicalSupplement())
                    ) {
                        seamlessError = getSeamlessError(exception);

                        break;
                    } else if ("Before Checkin Start Date Time".equalsIgnoreCase(exception.getDescription())
                        //&& "Attempted Check-in Too Early".equalsIgnoreCase(exception.getTechnicalSupplement())
                    ) {
                        seamlessError = getSeamlessError(exception);

                        break;
                    }
                }
            }
        }

        if (null != seamlessError) {
            throw new SeamlessException(seamlessError.toString(), status);
        }
    }

    public static SeamlessError getSeamlessError(Exception exception) {
        SeamlessError seamlessError = new SeamlessError();

        ErrorType errorType = ErrorType.fromCode(exception.getCode());

        if (null != errorType) {
            seamlessError.setActionable(errorType.getActionable());
            seamlessError.setErrorCode(errorType.getErrorCode());
            seamlessError.setErrorMessage(errorType.getMessage());
            seamlessError.setDescription(exception.getTechnicalSupplement());
            seamlessError.setTimestamp(LocalDateTime.now().format(dateTimeFormatter));

        } else {
            seamlessError.setActionable(ActionableType.ABORT.toString());
            seamlessError.setErrorCode(exception.getCode());
            seamlessError.setErrorMessage(exception.getDescription());
            seamlessError.setDescription(exception.getTechnicalSupplement());
            seamlessError.setTimestamp(LocalDateTime.now().format(dateTimeFormatter));
        }

        return seamlessError;
    }

    public static SeamlessError getSeamlessError(ErrorType errorType, String description) {
        SeamlessError seamlessError = new SeamlessError();

        seamlessError.setActionable(errorType.getActionable());
        seamlessError.setErrorCode(errorType.getErrorCode());
        seamlessError.setErrorMessage(errorType.getMessage());
        if (null != description) {
            seamlessError.setDescription(description);
        } else {
            seamlessError.setDescription(errorType.getDescription());
        }
        seamlessError.setTimestamp(LocalDateTime.now().format(dateTimeFormatter));


        return seamlessError;
    }
}
