package com.am.checkin.web.v2.util;

import com.aeromexico.codes.ICodesGenerator;
import com.aeromexico.codes.impl.CodesGenerator;
import com.aeromexico.commons.model.*;
import com.aeromexico.commons.model.rq.BoardingPassesRQ;
import com.aeromexico.commons.web.types.AirlineCodeType;
import com.aeromexico.commons.web.types.SeatSectionCodeType;
import com.aeromexico.commons.web.util.SegmentCodeUtil;
import com.aeromexico.commons.web.util.TimeUtil;
import com.aeromexico.sabre.api.models.dcci.Eligibility;
import com.aeromexico.sabre.api.models.dcci.Passenger;
import com.aeromexico.sabre.api.models.dcci.PassengerFlight;
import com.aeromexico.sabre.api.models.dcci.Reason;
import com.am.checkin.web.util.PnrCollectionUtil;
import com.sabre.services.acs.bso.reprintbp.v3.ItineraryACS;
import com.sabre.services.acs.bso.reprintbp.v3.PassengerInfoACS;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class BoardingUtil {
    private static final Logger LOG = LoggerFactory.getLogger(BoardingUtil.class);
    private static final ICodesGenerator CODES_GENERATOR = new CodesGenerator();

    /**
     * Get Seat from an abstract object
     *
     * @param abstractSegmentChoiceList
     * @return Seat Object
     */
    public static Seat getSeatFromCart(List<AbstractSegmentChoice> abstractSegmentChoiceList, String segment) {
        LOG.info("Making the information of seat passenger for segment: {}", segment);
        Seat seat = new Seat();
        for (AbstractSegmentChoice abstractSegmentChoice : abstractSegmentChoiceList) {
            if (abstractSegmentChoice.getSegmentCode().equals(segment)) {
                seat.setSeatCode(abstractSegmentChoice.getSeat().getCode());
                switch (abstractSegmentChoice.getSeat().getClass().getName()){
                    case "SeatChoice":
                        SeatChoice seatChoise = (SeatChoice) abstractSegmentChoice.getSeat();
                        seat.setSeatCharacteristics(seatChoise.getSeatCharacteristics());
                        break;
                    case "SeatChoiceUpsell":
                        SeatChoiceUpsell seatChoiceUpsell = (SeatChoiceUpsell) abstractSegmentChoice.getSeat();
                        seat.setSeatCharacteristics(seatChoiceUpsell.getSeatCharacteristics());
                        break;
                    case "SeatChoiceUpgrade":
                        SeatChoiceUpgrade seatChoiceUpgrade = (SeatChoiceUpgrade) abstractSegmentChoice.getSeat();
                        seat.setSeatCharacteristics(seatChoiceUpgrade.getSeatCharacteristics());
                        break;
                    case "SeatChoiceFareUpsell":
                        SeatChoiceFareUpsell seatChoiceFareUpsell = (SeatChoiceFareUpsell) abstractSegmentChoice.getSeat();
                        seat.setSeatCharacteristics(seatChoiceFareUpsell.getSeatCharacteristics());
                        break;
                    case "SeatChoiceFareUpgrade":
                        SeatChoiceFareUpgrade seatChoiceFareUpgrade = (SeatChoiceFareUpgrade) abstractSegmentChoice.getSeat();
                        seat.setSeatCharacteristics(seatChoiceFareUpgrade.getSeatCharacteristics());
                        break;
                    case "SeatChoiceWaivedUpgrade":
                        SeatChoiceWaivedUpgrade seatChoiceWaivedUpgrade = (SeatChoiceWaivedUpgrade) abstractSegmentChoice.getSeat();
                        seat.setSeatCharacteristics(seatChoiceWaivedUpgrade.getSeatCharacteristics());
                        break;
                }
            }
        }
        return seat;
    }

    /**
     * Look for the section code
     *
     * @param cabinClass
     * @return boolean
     */
    public static boolean isFirstClassCabin(String cabinClass) {
        // ECONOMICA = W,V,R,N, R
        // CLASICA = T,Q, L, H, K, U
        // FLEXIBLE = M, B, Y
        // CONFORT = I, D
        // PREMIER = C, J
        return cabinClass.equalsIgnoreCase("I")
                || cabinClass.equalsIgnoreCase("D")
                || cabinClass.equalsIgnoreCase("C")
                || cabinClass.equalsIgnoreCase("J");
    }

    /**
     *
     * @param boardingPassCollection
     */
    public static void sanatizeData(BoardingPassCollection boardingPassCollection) {
        if (null == boardingPassCollection) {
            return;
        }
        for (BoardingPass boardingPass : boardingPassCollection.getCollection()) {
            if (null != boardingPass && null != boardingPass.getPectabFormat()) {
                LOG.info("Re-print TravelDocData BEFORE: " + boardingPass.getPectabFormat());
                boardingPass.setPectabFormat(boardingPass.getPectabFormat().replaceAll("\u0026lt;", ">"));
                LOG.info("Re-print TravelDocData AFTER: " + boardingPass.getPectabFormat());
            }
        }
    }

    /**
     * Parse date format for the schema pattern
     *
     * @return
     */
    public static String formatBoardingTime(String boardingTime) {
        LOG.info("Parsing the boarding time field: " + boardingTime);
        if (null != boardingTime && !boardingTime.isEmpty()) {
            boardingTime = boardingTime.toUpperCase();
            if ("AM".contains(boardingTime) || "PM".contains(boardingTime)) {
                return boardingTime;
            } else {
                //Add AM or PM
                if (boardingTime.length() == 5) {
                    SimpleDateFormat twelveHours = new SimpleDateFormat("hh:mmaa");
                    SimpleDateFormat twentyFourHours = new SimpleDateFormat("HH:mm");
                    try {
                        boardingTime = twelveHours.format(twentyFourHours.parse(boardingTime));
                    } catch (ParseException e) {
                        LOG.error("Error to parsing time: " + e);
                    }
                }
            }
        }
        return boardingTime;
    }

    /**
     * Parse date format for the schema pattern
     *
     * @param twelve
     * @return
     */
    public static String getTwentyFour(String twelve) {
        LOG.info("Parsing the boarding time field");
        String twenty = null;
        if (twelve != null && !twelve.isEmpty()) {
            if (twelve.contains("AM") || twelve.contains("PM")) {
                SimpleDateFormat twelveHours = new SimpleDateFormat("hh:mmaa");
                SimpleDateFormat twentyFourHours = new SimpleDateFormat("HH:mm");
                try {
                    twenty = twentyFourHours.format(twelveHours.parse(twelve));
                } catch (ParseException e) {
                    LOG.error("Error to parsing time: " + e);
                }
            } else if (twelve.length() == 5) {
                return twelve;
            }
        }
        return twenty;
    }

    /**
     * Build legCode from segment documents list
     *
     * @param bookedTraveler
     * @return
     */
    public static String getLegCodeFromDocumentList(BookedTraveler bookedTraveler) {
        try {
            if (null != bookedTraveler.getSegmentDocumentsList() && !bookedTraveler.getSegmentDocumentsList().isEmpty()) {
                SegmentDocumentList segmentDocumentList = bookedTraveler.getSegmentDocumentsList();

                if (1 == segmentDocumentList.size()) {
                    return segmentDocumentList.get(0).getSegmentCode().substring(0, 7)
                            .replace("_", "-") + "_";
                } else {
                    String departureCity = segmentDocumentList.get(0).getSegmentCode().substring(0, 3);
                    String arrivalCity = segmentDocumentList.get(segmentDocumentList.size() - 1).getSegmentCode()
                            .split("_")[1];

                    return departureCity + "-" + arrivalCity + "_";
                }
            }
        } catch (Exception e) {
            throw e;
        }

        return "";
    }

    /**
     *
     * @param bookedTraveler
     * @param bookedLeg
     * @param recordLocator
     */
    public static void setStandByBoardingPasses(
            BookedTraveler bookedTraveler,
            BookedLeg bookedLeg,
            String recordLocator
    ) {
        bookedTraveler.setSegmentDocumentsList(new SegmentDocumentList());

        List<BookedSegment> bookedSegmentList = bookedLeg.getSegments().getSegmentsOperatedBy(AirlineCodeType.AM);

        int itinerary = 0;
        for (BookedSegment bookedSegment : bookedSegmentList) {
            itinerary++;

            if (null != bookedSegment) {
                BaggageRoute baggageRoute;
                baggageRoute = PnrCollectionUtil.getBaggageRoute(
                        bookedTraveler.getBaggageRouteList(),
                        bookedSegment.getSegment().getSegmentCode()
                );

                short segmentId;

                if (null != baggageRoute) {
                    segmentId = baggageRoute.getSegmentID();
                } else {
                    segmentId = (short) itinerary;
                }

                String nameInPnr = getNameInPnr(bookedTraveler);
                String origin = null != bookedSegment.getSegment().getDepartureAirport()
                        ? bookedSegment.getSegment().getDepartureAirport() : "";
                String destination = bookedSegment.getSegment().getArrivalAirport();
                String operatingCarrier = null != bookedSegment.getSegment().getOperatingCarrier()
                        ? bookedSegment.getSegment().getOperatingCarrier() : "";
                String flightNumber = null != bookedSegment.getSegment().getOperatingFlightCode()
                        ? bookedSegment.getSegment().getOperatingFlightCode() : "";
                String classOfService = getClassOfService(bookedTraveler, bookedSegment.getSegment().getSegmentCode());
                String eTicket = null != bookedTraveler.getTicketNumber() ? bookedTraveler.getTicketNumber() : "";

                String boardingPass = getTravelDoc(
                        nameInPnr,
                        recordLocator,
                        origin,
                        destination,
                        operatingCarrier,
                        flightNumber,
                        classOfService,
                        eTicket
                );

                SegmentDocument segmentInfo = new SegmentDocument();
                try {
                    segmentInfo.setBarcode(boardingPass);
                    segmentInfo.setBarcodeImage(
                            CODES_GENERATOR.codesGenerator2D(boardingPass)
                    );
                    segmentInfo.setQrcodeImage(
                            CODES_GENERATOR.codesGeneratorQR(boardingPass)
                    );
                } catch (Exception ex) {
                    LOG.error(ex.getMessage());
                }

                segmentInfo.setSegmentId(segmentId);
                segmentInfo.setBoardingZone(""); //3
                segmentInfo.setGate(""); //A8
                segmentInfo.setCheckInNumber("");
                segmentInfo.setSegmentCode(bookedSegment.getSegment().getSegmentCode());

                segmentInfo.setCheckinStatus(true);

                segmentInfo.setTsaPreCheck(false);
                segmentInfo.setSkyPriority(false);
                bookedTraveler.getSegmentDocumentsList().addOrReplace(segmentInfo);
            }
        }
    }

    /**
     *
     * @param nameInPnr
     * @param recordLocator
     * @param origin
     * @param destination
     * @param operatingCarrier
     * @param flightNumber
     * @param classOfService
     * @param ticketNumber
     * @return
     */
    private static String getTravelDoc(
            String nameInPnr,
            String recordLocator,
            String origin,
            String destination,
            String operatingCarrier,
            String flightNumber,
            String classOfService,
            String ticketNumber) {

        return "M1"
                + ((nameInPnr.length() <= 21) ? StringUtils.leftPad(nameInPnr, 21, " ") : nameInPnr.substring(0, 21))
                + recordLocator + " "
                + origin + destination + operatingCarrier + " "
                + StringUtils.leftPad(flightNumber, 4, "0")
                + " 000" + classOfService + "000 0000 000" + "&lt;" + "0000  0000BAM"
                + StringUtils.rightPad("", 40, " ")
                + "2A"
                + StringUtils.rightPad(ticketNumber, 13, " ") + " 0"
                + StringUtils.rightPad("", 26, " ")
                + "N";
    }

    private static String getDepartureDate(Segment segment) {
        try {
            return TimeUtil.getStrTime(
                    segment.getDepartureDateTime(),
                    "yyyy-MM-dd'T'HH:mm:ss",
                    "ddMMM"
            );
        } catch (ParseException ex) {
            LOG.error(ex.getMessage(), ex);
        }
        return "";
    }

    private static String getDepartureTime(Segment segment) {
        try {
            return TimeUtil.getStrTime(
                    segment.getDepartureDateTime(),
                    "yyyy-MM-dd'T'HH:mm:ss",
                    "HH:mm"
            );
        } catch (ParseException ex) {
            LOG.error(ex.getMessage(), ex);
        }
        return "";
    }

    private static String getClassOfService(BookedTraveler bookedTraveler, String segmentCode) {
        try {
            BookingClass bookingClass = bookedTraveler.getBookingClasses().getBookingClass(segmentCode);
            return bookingClass.getBookingClass();
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
        return "";
    }

    private static String getNameInPnr(BookedTraveler bookedTraveler) {
        try {
            return (bookedTraveler.getLastName() + "/" + bookedTraveler.getFirstName()).toUpperCase();
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
        return "";
    }

    public static boolean isNotSelectee(Passenger passengerElement) {
        if (passengerElement.getEligibilities() != null && !passengerElement.getEligibilities().getEligibility().isEmpty()) {
            for (Eligibility elegibilities : passengerElement.getEligibilities().getEligibility()) {
                for(Reason reason : elegibilities.getReason()){
                    if (reason.getMessage().equalsIgnoreCase("BOARDING_PASS_INHIBITED_SELECTEE")) {
                        return false;
                    }
                }

            }
        }
        return true;
    }

    public static SegmentDocument createSegmentDocumentFromBoardingPass(
            String segmentCode,
            PassengerFlight passFlight,
            BoardingPassWeb boardingPass
    ) {
        SegmentDocument segmentDocument = new SegmentDocument();
        segmentDocument.setBarcode(passFlight.getBoardingPass().getBarCode());
        segmentDocument.setBarcodeImage(boardingPass.getBarcodeImage());
        segmentDocument.setQrcodeImage(boardingPass.getQrCodeImage());
        segmentDocument.setTsaPreCheck(boardingPass.isTSAPre());
        segmentDocument.setSkyPriority(boardingPass.isSkyPriority());
        segmentDocument.setBoardingZone(boardingPass.getZone());
        segmentDocument.setSegmentCode(segmentCode);
        return segmentDocument;
    }

    /**
     * This method is user for get segment from sabre response and is used in
     * Junit test
     *
     * @param passFlight
     * @param bookedLegCollection
     * @return
     */
    public static SegmentStatus extractSegmentStatusSabre(
            PassengerFlight passFlight,
            BookedLegCollection bookedLegCollection
    ) {
        SegmentStatus segmentStatus = new SegmentStatus();

        String defaultDepartureGate = "TBD";

        if (null != passFlight
                && null != passFlight.getBoardingPass().getFlightDetail()
                && null != passFlight.getBoardingPass().getFlightDetail().getDepartureGate()
                && StringUtils.isNotBlank( passFlight.getBoardingPass().getFlightDetail().getDepartureGate())) {

            String departureGate = passFlight.getBoardingPass().getFlightDetail().getDepartureGate();

            if (!"Not asigned".equalsIgnoreCase(departureGate)
                    && !"GATE".equalsIgnoreCase(departureGate)) {
                segmentStatus.setBoardingGate(departureGate);
            } else {
                segmentStatus.setBoardingGate(defaultDepartureGate);
            }

        } else {
            segmentStatus.setBoardingGate(defaultDepartureGate);
        }

        String boardingTimeFinal = "--:--";

        if (null != passFlight) {
            if (StringUtils.isNotBlank(
                    passFlight.getBoardingPass().getDisplayData().getBoardingTime()
            )) {
                boardingTimeFinal = passFlight.getBoardingPass().getDisplayData().getBoardingTime().substring(0, 4);
                boardingTimeFinal = boardingTimeFinal.substring(0, 2) + ":" + boardingTimeFinal.substring(2, 4);
            }
        }

        segmentStatus.setBoardingTime(boardingTimeFinal);

        segmentStatus.setArrivalGate("");
        segmentStatus.setArrivalTerminal("");
        segmentStatus.setBoardingTerminal("");

        if (null != passFlight) {
        String departureTime = passFlight.getBoardingPass().getFlightDetail().getDepartureTime();
        String arrivalTime = passFlight.getBoardingPass().getFlightDetail().getArrivalTime();

            segmentStatus.setEstimatedArrivalTime(arrivalTime.substring(0,arrivalTime.length()-6));
            segmentStatus.setEstimatedDepartureTime(departureTime.substring(0,departureTime.length()-6));
            segmentStatus.setStatus(passFlight.getBoardingPass().getFlightDetail().getDepartureFlightScheduleStatus());

            Segment segment = extractSegmentSabre(passFlight, bookedLegCollection);
            segmentStatus.setSegment(segment);
        }

        return segmentStatus;
    }

    /**
     * This method is user for get information in the response from Sabre
     * Digital Signature is used in JUnit test
     *
     * @param passFlight
     * @param bookedLegCollection
     * @return
     */
    public static Segment extractSegmentSabre(PassengerFlight passFlight, BookedLegCollection bookedLegCollection) {
        Segment segment = new Segment();

        segment.setLayoverToNextSegmentsInMinutes(0);

        String departureTime = passFlight.getBoardingPass().getFlightDetail().getDepartureTime();
        String arrivalTime = passFlight.getBoardingPass().getFlightDetail().getArrivalTime();

        String boardingTimeFinal = "--:--";

        if (null != passFlight) {
            if (StringUtils.isNotBlank(
                    passFlight.getBoardingPass().getDisplayData().getBoardingTime()
            )) {
                boardingTimeFinal = passFlight.getBoardingPass().getDisplayData().getBoardingTime().substring(0, 4);
                boardingTimeFinal = boardingTimeFinal.substring(0, 2) + ":" + boardingTimeFinal.substring(2, 4);
            }
        }

        segment.setArrivalAirport(passFlight.getBoardingPass().getFlightDetail().getArrivalAirport());
        segment.setArrivalDateTime(arrivalTime.substring(0,arrivalTime.length()-6));
        segment.setDepartureAirport(passFlight.getBoardingPass().getFlightDetail().getDepartureAirport());
        segment.setDepartureDateTime(departureTime.substring(0,departureTime.length()-6));
        segment.setOperatingCarrier(passFlight.getBoardingPass().getFlightDetail().getOperatingAirline());
        segment.setOperatingFlightCode(String.valueOf(passFlight.getBoardingPass().getFlightDetail().getOperatingFlightNumber()));
        segment.setMarketingCarrier(passFlight.getBoardingPass().getFlightDetail().getAirline());
        segment.setMarketingFlightCode(String.valueOf(passFlight.getBoardingPass().getFlightDetail().getFlightNumber()));
        segment.setBoardingTime(boardingTimeFinal);
        segment.setIsPremierAvailable(false);
        segment.setFlightDurationInMinutes(0);

        String replaceTime = passFlight.getBoardingPass().getFlightDetail().getDepartureTime().replace("T", "_").substring(0, 16);
        replaceTime = replaceTime.replace(":", "");
        String sb = passFlight.getBoardingPass().getFlightDetail().getDepartureAirport() + "_"
                + passFlight.getBoardingPass().getFlightDetail().getArrivalAirport() + "_"
                + passFlight.getBoardingPass().getFlightDetail().getAirline() + "_"
                + replaceTime;
        segment.setSegmentCode(sb);

        for (BookedLeg bookedLeg : bookedLegCollection.getCollection()) {
            for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {
                Segment segmentPnr = bookedSegment.getSegment();
                if (segmentPnr.getDepartureAirport().equals(passFlight.getBoardingPass().getFlightDetail().getDepartureAirport())
                        && segmentPnr.getArrivalAirport().equals(passFlight.getBoardingPass().getFlightDetail().getArrivalAirport())) {
                    segment.setFlightDurationInMinutes(segmentPnr.getFlightDurationInMinutes());
                    segment.setSegmentNumber(segmentPnr.getSegmentNumber());
                    segment.setSegmentStatus(segmentPnr.getSegmentStatus());
                    segment.setAircraftType(segmentPnr.getAircraftType());
                }
            }
        }
        return segment;
    }

    /**
     * This method is user for get seat from sabre response and is used in Junit
     * test
     *
     * @param passFlight
     * @return
     */
    public static Seat extractSeatSabre(PassengerFlight passFlight) {
        if (null == passFlight.getSeat()) {
            return null;
        }

        Seat seat = new Seat();
        seat.setSeatCode(passFlight.getSeat().getValue());

        if (null != passFlight.getBoardingPass()) {

            if (isFirstClassCabin(passFlight.getBoardingPass().getFareInfo().getBookingClass())) {
                seat.setSectionCode(SeatSectionCodeType.FIRST_CLASS);
            } else {
                seat.setSectionCode(SeatSectionCodeType.COACH);
            }

        } else {
            seat.setSectionCode(SeatSectionCodeType.COACH);
        }

        seat.setSeatCharacteristics(new ArrayList<>());
        return seat;
    }

    public static String containsSegmentCode(PassengerFlight passFlight, CartPNR cartPnr) {

        if ( null != passFlight.getBoardingPass()
                && null != passFlight.getBoardingPass().getFlightDetail()) {
            try {

                String segmentCode = passFlight.getBoardingPass().getFlightDetail().getDepartureAirport()
                        + "_" + passFlight.getBoardingPass().getFlightDetail().getArrivalAirport()
                        + "_" + passFlight.getBoardingPass().getFlightDetail().getOperatingAirline()
                        + "_" + passFlight.getBoardingPass().getFlightDetail().getDepartureTime().substring(0, 10)
                        + "_" + passFlight.getBoardingPass().getFlightDetail().getDepartureTime().substring(11, 16).replace(":", "");

                for (BookedTraveler bookedTraveler : cartPnr.getTravelerInfo().getCollection()) {
                    for (BookingClass bookinClasses : bookedTraveler.getBookingClasses().getCollection()) {
                        if (SegmentCodeUtil.compareSegmentCodeWithoutTime(segmentCode, bookinClasses.getSegmentCode())) {
                            return segmentCode;
                        }
                    }
                }
            } catch (Exception ex) {
                LOG.error(ex.getMessage(), ex);
            }
        }

        return null;
    }

    public static void setPriorityBoarding(PNRCollection pnrCollection, String recordLocator) {
        try {
            PNR pnr = pnrCollection.getPNRByRecordLocator(recordLocator);

            for (CartPNR cartPNR : pnr.getCarts().getCollection()) {

                String cardId = haveCobranded(cartPNR);

                if (null == cardId) {
                    return;
                }

                for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
                    if (null != bookedTraveler.getSegmentDocumentsList()
                            && !bookedTraveler.getSegmentDocumentsList().isEmpty()) {

                        for (SegmentDocument segmentDocument : bookedTraveler.getSegmentDocumentsList()) {
                            if (null != segmentDocument) {
                                segmentDocument.setCardID(cardId);
                                segmentDocument.setPriorityBoarding(true);
                            }
                        }
                    }

                    if (null != bookedTraveler.getInfant() && null != bookedTraveler.getInfant().getSegmentDocumentsList()) {
                        for (SegmentDocument segmentDocument : bookedTraveler.getInfant().getSegmentDocumentsList()) {
                            if (null != segmentDocument) {
                                segmentDocument.setCardID(cardId);
                                segmentDocument.setPriorityBoarding(true);
                            }
                        }

                    }
                }
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
    }

    public static String haveCobranded(CartPNR cartPNR) {
        if (null == cartPNR
                || null == cartPNR.getTravelerInfo()
                || null == cartPNR.getTravelerInfo().getCollection()) {
            return null;
        }

        String cardId = null;
        int priority = 0;

        for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
            if (null != bookedTraveler.getBookedTravellerBenefitCollection()
                    && null != bookedTraveler.getBookedTravellerBenefitCollection().getCollection()
                    && !bookedTraveler.getBookedTravellerBenefitCollection().getCollection().isEmpty()) {

                for (BookedTravelerBenefit bookedTravelerBenefit : bookedTraveler.getBookedTravellerBenefitCollection().getCollection()) {

                    if (null != bookedTravelerBenefit.getBagBenefitCobrand()
                            && null != bookedTravelerBenefit.getBagBenefitCobrand().getFreeBagAllowanceByCardList()
                            && !bookedTravelerBenefit.getBagBenefitCobrand().getFreeBagAllowanceByCardList().isEmpty()) {

                        for (FreeBaggageAllowanceDetailByCard freeBaggageAllowanceDetailByCard : bookedTravelerBenefit.getBagBenefitCobrand().getFreeBagAllowanceByCardList()) {

                            String card = freeBaggageAllowanceDetailByCard.getCobrandCode();

                            if (null != card) {

                                switch (card) {
                                    case "AMXPLT":
                                        if (priority < 4) {
                                            //priority = 4;
                                            cardId = card;

                                            //is the highter priority, if one is found stop searching
                                            return cardId;
                                        }
                                        break;
                                    case "AMXGLD":
                                        if (priority < 3) {
                                            priority = 3;
                                            cardId = card;
                                        }
                                        break;
                                    case "SEIFNT":
                                        if (priority < 2) {
                                            priority = 2;
                                            cardId = card;
                                        }
                                        break;
                                    case "SEPLT":
                                        if (priority < 1) {
                                            priority = 1;
                                            cardId = card;
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
            }
        }
        return cardId;
    }

    public static void createItineraryFromBookedLeg(List<BookedLeg> legsByLegCodes, List<ItineraryACS> itineraryList) {
        for (BookedLeg legData : legsByLegCodes) {
            if (null != legData.getSegments().getCollection() && !legData.getSegments().getCollection().isEmpty()) {
                for (BookedSegment bookedSegment : legData.getSegments().getCollection()) {
                    ItineraryACS itinerary = new ItineraryACS();
                    itinerary.setAirline(bookedSegment.getSegment().getOperatingCarrier());
                    itinerary.setDepartureDate(bookedSegment.getSegment().getDepartureDateTime().substring(0, 10));
                    itinerary.setDestination(bookedSegment.getSegment().getArrivalAirport());
                    itinerary.setFlight(bookedSegment.getSegment().getOperatingFlightCode());
                    itinerary.setOrigin(bookedSegment.getSegment().getDepartureAirport());
                    itineraryList.add(itinerary);
                }
            }
        }
    }

    public static boolean needBoardingPassInfo(BookedTraveler bookedTraveler) {
        if (bookedTraveler.getSegmentDocumentsList() != null && bookedTraveler.getSegmentDocumentsList().size() > 0) {
            for (SegmentDocument segmentDocument : bookedTraveler.getSegmentDocumentsList()) {
                if (segmentDocument.getTravelDoc() == null) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Get passenger info from cart pnr
     *
     * @param cartPNR
     * @param boardingPassesRQ
     * @param segOriginal
     * @return
     */
    public static List<PassengerInfoACS> getListPassengerInfo(
            CartPNR cartPNR,
            BoardingPassesRQ boardingPassesRQ,
            String segOriginal
    ) {

        List<PassengerInfoACS> passengerInfoList = new ArrayList<>();

        for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {

            if (boardingPassesRQ.getPaxId().contains(bookedTraveler.getId())) {

                if (needBoardingPassInfo(bookedTraveler)) {
                    PassengerInfoACS passenger = new PassengerInfoACS();
                    passenger.setLastName(bookedTraveler.getLastName());
                    for (BookingClass booked : bookedTraveler.getBookingClasses().getCollection()) {
                        if (booked.getSegmentCode().substring(0, 3).equalsIgnoreCase(segOriginal)) {
                            passenger.setPassengerID(booked.getPassengerId());
                        }
                    }

                    if (null == passenger.getPassengerID() || passenger.getPassengerID().trim().isEmpty()) {
                        passenger.setPassengerID(bookedTraveler.getId());
                    }

                    passengerInfoList.add(passenger);
                }
            }
        }
        return passengerInfoList;
    }
}
