package com.am.checkin.web.v2.seamless.transformers;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.CartPNRUpdate;
import com.aeromexico.commons.model.Mandate;
import com.aeromexico.commons.model.PurchaseOrder;
import com.aeromexico.commons.model.rq.PnrRQ;
import com.aeromexico.commons.model.rq.ShoppingCartRQ;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.util.FilterPassengersUtil;
import com.am.seamless.checkin.common.models.Country;
import com.am.seamless.checkin.common.models.CountrySubDivision;
import com.am.seamless.checkin.common.models.IdentityDocument;
import com.am.seamless.checkin.common.models.PassengerInformation;
import com.am.seamless.checkin.common.models.PassengerSelection;
import com.am.seamless.checkin.common.models.PostalAddress;
import com.am.seamless.checkin.common.models.SupplementaryDocument;
import com.am.seamless.checkin.common.models.request.BoardingDocumentRq;
import com.am.seamless.checkin.common.models.request.PassengerInfoRq;
import com.am.seamless.checkin.common.models.request.PassengerSelectionRq;
import com.am.seamless.checkin.common.models.request.TravelPartyRq;
import com.am.seamless.checkin.common.models.request.UpdateMandatesRq;
import com.am.seamless.checkin.common.models.types.DataSourceCodeType;
import com.am.seamless.checkin.common.models.types.GenderCodeType;
import com.am.seamless.checkin.common.models.types.IdentityDocumentCodeType;
import com.am.seamless.checkin.common.models.types.SupplementaryDocumentCodeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TransformToDS {

    private static final Logger LOG = LoggerFactory.getLogger(TransformToDS.class);

    public static TravelPartyRq getTravelPartyRq(PnrRQ pnrRQ, CartPNR cartPNR, BookedLeg bookedLeg) {
        List<String> tickets = new ArrayList<>();
        for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
            if (null != bookedTraveler.getTicketNumber()) {
                tickets.add(bookedTraveler.getTicketNumber());
            } else if (null != bookedTraveler.getTicketNumbers() && !bookedTraveler.getTicketNumbers().isEmpty()) {
                tickets.add(bookedTraveler.getTicketNumbers().iterator().next().getNumber());
            } else if (null != bookedTraveler.getAllTicketNumbers() && !bookedTraveler.getAllTicketNumbers().isEmpty()) {
                tickets.add(bookedTraveler.getAllTicketNumbers().iterator().next());
            }
        }

        String departureCity = bookedLeg.getSegments().getCollection().get(0).getSegment().getDepartureAirport();
        TravelPartyRq travelPartyRq = new TravelPartyRq(pnrRQ.getRecordLocator(), tickets, departureCity);

        return travelPartyRq;
    }

    public static BoardingDocumentRq getBoardingDocumentRq(CartPNR cartPNR, BookedLeg bookedLeg) {

        BoardingDocumentRq boardingDocumentRq = new BoardingDocumentRq();

        boardingDocumentRq.setTravelParty(new BoardingDocumentRq.TravelParty());

        //TODO: filter out not eligible for checkin passengers

        for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {

            if (bookedTraveler.isSelectedToCheckin()) {
                PassengerSelection.SelectedPassenger selectedPassenger = new PassengerSelection.SelectedPassenger();

                selectedPassenger.setId(bookedTraveler.getId());
                boardingDocumentRq.getTravelParty().getSelectedPassengers().add(selectedPassenger);
            }
        }

        for (Mandate mandate : cartPNR.getMandates().getCollection()) {
            PassengerInformation.Mandate man = new PassengerInformation.Mandate();
            man.setId(mandate.getId());
            man.setConfirmed(mandate.isConfirmed());

            boardingDocumentRq.getTravelParty().getMandates().add(man);
        }

        PassengerInformation.Trip trip = new PassengerInformation.Trip();
        trip.setId(bookedLeg.getSegments().getTripId());
        boardingDocumentRq.getTravelParty().getTrips().add(trip);

        return boardingDocumentRq;
    }

    public static BoardingDocumentRq getBoardingDocumentRq(CartPNR cartPNR, BookedLeg bookedLeg, PurchaseOrder purchaseOrder) {

        BoardingDocumentRq boardingDocumentRq = new BoardingDocumentRq();

        boardingDocumentRq.setTravelParty(new BoardingDocumentRq.TravelParty());

        //TODO: filter out not eligible for checkin passengers

        for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {

            if (bookedTraveler.isSelectedToCheckin()) {
                PassengerSelection.SelectedPassenger selectedPassenger = new PassengerSelection.SelectedPassenger();

                selectedPassenger.setId(bookedTraveler.getId());
                boardingDocumentRq.getTravelParty().getSelectedPassengers().add(selectedPassenger);
            }
        }

        for (Mandate mandate : purchaseOrder.getMandates().getCollection()) {

            PassengerInformation.Mandate man = new PassengerInformation.Mandate();
            man.setId(mandate.getId());
            man.setConfirmed(mandate.isConfirmed());

            boardingDocumentRq.getTravelParty().getMandates().add(man);
        }

        PassengerInformation.Trip trip = new PassengerInformation.Trip();
        trip.setId(bookedLeg.getSegments().getTripId());
        boardingDocumentRq.getTravelParty().getTrips().add(trip);


        return boardingDocumentRq;
    }

    public static UpdateMandatesRq getUpdateMandatesRq(CartPNR cartPNR) {

        UpdateMandatesRq mandatesRq = new UpdateMandatesRq();

        com.am.seamless.checkin.common.models.request.UpdateMandatesRq.PassengerInformation passengerInformation;
        passengerInformation = new com.am.seamless.checkin.common.models.request.UpdateMandatesRq.PassengerInformation();

        List<UpdateMandatesRq.Mandate> mandates = new ArrayList<>();

        for (Mandate mandate : cartPNR.getMandates().getCollection()) {
            UpdateMandatesRq.Mandate mandateSmlss = new UpdateMandatesRq.Mandate();

            mandateSmlss.setConfirmed(mandate.isConfirmed());
            mandateSmlss.setId(mandate.getId());
            mandates.add(mandateSmlss);
        }

        passengerInformation.setMandates(mandates);
        mandatesRq.setPassengerInformation(passengerInformation);
        return mandatesRq;

    }

    public static PassengerSelectionRq getPassengerSelectionRq(CartPNR cartPNR) {
        List<PassengerSelection.SelectedPassenger> selectedPassengers = new ArrayList<>();
        for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
            if (bookedTraveler.isSelectedToCheckin()) {
                PassengerSelection.SelectedPassenger selectedPassenger = new PassengerSelection.SelectedPassenger();

                selectedPassenger.setId(bookedTraveler.getId());
                selectedPassengers.add(selectedPassenger);
            }
        }

        PassengerSelectionRq passengerSelectionRq = new PassengerSelectionRq();

        PassengerSelection passengerSelection = new PassengerSelection();
        passengerSelection.setSelectedPassengers(selectedPassengers);

        passengerSelectionRq.setPassengerSelection(passengerSelection);

        return passengerSelectionRq;
    }

    public static PassengerInfoRq getPassengerInfoRq(
            ShoppingCartRQ shoppingCartRQ,
            CartPNR cartPNR,
            BookedLeg bookedLeg,
            CartPNRUpdate cartPNRUpdate
    ) throws Exception {

        PassengerInfoRq passengerInfoRq = new PassengerInfoRq();
        PassengerInformation passengerInformation = new PassengerInformation();
        passengerInfoRq.setPassengerInformation(passengerInformation);

        PassengerInformation.Trip trip = new PassengerInformation.Trip();
        trip.setId(bookedLeg.getSegments().getTripId());
        passengerInformation.getTrips().add(trip);

        List<BookedTraveler> selectedBookedTravelersList = cartPNR.getTravelerInfo().getCollection();
        List<BookedTraveler> selectedBookedTravelersUpdateList = cartPNRUpdate.getTravelerInfo().getCollection();

        for (BookedTraveler bookedTravelerUpdate : selectedBookedTravelersUpdateList) {

            Optional<BookedTraveler> bookedTravelerOptional = FilterPassengersUtil.getBookedTravelerById(
                    selectedBookedTravelersList,
                    bookedTravelerUpdate.getId()
            );

            LOG.info("**bookedTravelerUpdate: {}", bookedTravelerUpdate);

            if (!bookedTravelerOptional.isPresent()) {
                LOG.error(ErrorType.TRAVELER_ID_NOT_MATCH.getFullDescription() + " PASSENGER ID : "
                        + bookedTravelerUpdate.getId() + " - PNR: " + shoppingCartRQ.getRecordLocator());
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.TRAVELER_ID_NOT_MATCH,
                        ErrorType.TRAVELER_ID_NOT_MATCH.getFullDescription() + bookedTravelerUpdate.getId() + " - PNR: "
                                + shoppingCartRQ.getRecordLocator());
            }

            BookedTraveler bookedTraveler = bookedTravelerOptional.get();

            if (!bookedTraveler.isSelectedToCheckin()) {
                LOG.error(ErrorType.PASSENGER_NOT_SELECTED_FOR_CHECKIN.getFullDescription() + " PASSENGER ID : "
                        + bookedTravelerUpdate.getId() + " - PNR: " + shoppingCartRQ.getRecordLocator());
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.PASSENGER_NOT_SELECTED_FOR_CHECKIN,
                        ErrorType.PASSENGER_NOT_SELECTED_FOR_CHECKIN.getFullDescription() + bookedTravelerUpdate.getId() + " - PNR: "
                                + shoppingCartRQ.getRecordLocator());
            }

            if (!bookedTraveler.isCheckinStatus()) {

                PassengerInformation.PassengerUpdate passengerUpdate = new PassengerInformation.PassengerUpdate();

                passengerUpdate.setId(bookedTraveler.getId());

                if (null != bookedTraveler.getInfant()) {
                    PassengerInformation.PassengerUpdate infantUpdate = new PassengerInformation.PassengerUpdate();
                    infantUpdate.setId(bookedTraveler.getInfant().getId());

                    PassengerInformation.AdvancePassengerInformation advancePassengerInformation = new PassengerInformation.AdvancePassengerInformation();
                    infantUpdate.setAdvancePassengerInformation(advancePassengerInformation);

                    passengerUpdate.setInfant(infantUpdate);
                }

                PassengerInformation.AdvancePassengerInformation advancePassengerInformation = new PassengerInformation.AdvancePassengerInformation();
                passengerUpdate.setAdvancePassengerInformation(advancePassengerInformation);

                boolean thereWereChanges = updateGender(bookedTraveler, bookedTravelerUpdate, passengerUpdate);

                thereWereChanges = updateDateOfBirth(bookedTraveler, bookedTravelerUpdate, passengerUpdate) || thereWereChanges;

                thereWereChanges = updateDestinationAddress(bookedTraveler, bookedTravelerUpdate, passengerUpdate) || thereWereChanges;

                thereWereChanges = updateTravelDocument(bookedTraveler, bookedTravelerUpdate, passengerUpdate) || thereWereChanges;

                if (null != bookedTravelerUpdate.getTimaticInfo()
                        && null != bookedTravelerUpdate.getTimaticInfo().getReturnDate()
                        && null != bookedTravelerUpdate.getTimaticInfo().getResidencyCountry()) {
                    thereWereChanges = updateDataFromTimaticInfo(bookedTraveler, bookedTravelerUpdate, passengerUpdate) || thereWereChanges;
                } else {
                    thereWereChanges = updateCountryOfResidence(bookedTraveler, bookedTravelerUpdate, passengerUpdate) || thereWereChanges;
                    thereWereChanges = updateOnwardTravelDate(bookedTraveler, bookedTravelerUpdate, passengerUpdate) || thereWereChanges;
                }

                if (thereWereChanges) {
                    passengerInformation.getSelectedPassengers().add(passengerUpdate);
                }

            }
        }

        return passengerInfoRq;

    }


    private static boolean updateGender(BookedTraveler currentBookedTraveler, BookedTraveler updatedBookedTraveler, PassengerInformation.PassengerUpdate passengerUpdate) {
        boolean thereWereChanges = false;

        if (null != updatedBookedTraveler.getGender()) {
            if (!updatedBookedTraveler.getGender().equals(currentBookedTraveler.getGender())) {
                passengerUpdate.setUpdateGender(true);
                passengerUpdate.setGender(TransformerUtil.getGender(updatedBookedTraveler.getGender()));
                thereWereChanges = true;
            }
        }

        if (null != currentBookedTraveler.getInfant() && null != updatedBookedTraveler.getInfant() && null != passengerUpdate.getInfant()) {
            if (null != updatedBookedTraveler.getInfant().getGender()) {
                if (!updatedBookedTraveler.getInfant().getGender().equals(currentBookedTraveler.getInfant().getGender())) {
                    passengerUpdate.getInfant().setUpdateGender(true);
                    passengerUpdate.getInfant().setGender(TransformerUtil.getGender(updatedBookedTraveler.getInfant().getGender()));
                    thereWereChanges = true;
                }
            }
        }

        return thereWereChanges;
    }

    private static boolean updateDateOfBirth(BookedTraveler currentBookedTraveler, BookedTraveler updatedBookedTraveler, PassengerInformation.PassengerUpdate passengerUpdate) {
        boolean thereWereChanges = false;

        if (null != updatedBookedTraveler.getDateOfBirth()) {
            if (!updatedBookedTraveler.getDateOfBirth().equals(currentBookedTraveler.getDateOfBirth())) {
                passengerUpdate.setUpdateBirthDate(true);
                passengerUpdate.setBirthDate(updatedBookedTraveler.getDateOfBirth());
                thereWereChanges = true;
            }
        }

        if (null != currentBookedTraveler.getInfant() && null != updatedBookedTraveler.getInfant() && null != passengerUpdate.getInfant()) {
            if (null != updatedBookedTraveler.getInfant().getDateOfBirth()) {
                if (!updatedBookedTraveler.getInfant().getDateOfBirth().equals(currentBookedTraveler.getInfant().getDateOfBirth())) {
                    passengerUpdate.getInfant().setUpdateBirthDate(true);
                    passengerUpdate.getInfant().setBirthDate(updatedBookedTraveler.getInfant().getDateOfBirth());
                    thereWereChanges = true;
                }
            }
        }

        return thereWereChanges;
    }

    private static boolean updateCountryOfResidence(BookedTraveler currentBookedTraveler, BookedTraveler updatedBookedTraveler, PassengerInformation.PassengerUpdate passengerUpdate) {
        boolean thereWereChanges = false;

        if (null != updatedBookedTraveler.getCountryOfResidence()) {
            if (!updatedBookedTraveler.getCountryOfResidence().equalsIgnoreCase(currentBookedTraveler.getCountryOfResidence())) {
                Country country = new Country();
                country.setCode(updatedBookedTraveler.getCountryOfResidence());
                passengerUpdate.getAdvancePassengerInformation().setUpdateCountryOfResidence(true);
                passengerUpdate.getAdvancePassengerInformation().setCountryOfResidence(country);
                thereWereChanges = true;
            }
        }

        if (null != currentBookedTraveler.getInfant() && null != updatedBookedTraveler.getInfant() && null != passengerUpdate.getInfant()) {
            if (null != updatedBookedTraveler.getInfant().getCountryOfResidence()) {
                if (!updatedBookedTraveler.getInfant().getCountryOfResidence().equals(currentBookedTraveler.getInfant().getCountryOfResidence())) {
                    Country country = new Country();
                    country.setCode(updatedBookedTraveler.getInfant().getCountryOfResidence());
                    passengerUpdate.getInfant().getAdvancePassengerInformation().setUpdateCountryOfResidence(true);
                    passengerUpdate.getInfant().getAdvancePassengerInformation().setCountryOfResidence(country);
                    thereWereChanges = true;
                }
            }
        }

        return thereWereChanges;
    }

    private static boolean updateTravelDocument(BookedTraveler currentBookedTraveler, BookedTraveler updatedBookedTraveler, PassengerInformation.PassengerUpdate passengerUpdate) {
        boolean thereWereChanges = false;

        if (null != updatedBookedTraveler.getTravelDocument() || null != currentBookedTraveler.getTravelDocument()) {

            if ((null != updatedBookedTraveler.getTravelDocument()
                    && !updatedBookedTraveler.getTravelDocument().equals(currentBookedTraveler.getTravelDocument()))
                    || (null != updatedBookedTraveler.getVisaInfo()
                    && !updatedBookedTraveler.getVisaInfo().equals(currentBookedTraveler.getVisaInfo()))
            ) {

                thereWereChanges = true;

                IdentityDocument identityDocument = new IdentityDocument();

                if (null != updatedBookedTraveler.getGender()) {
                    GenderCodeType genderCodeType = TransformerUtil.getGender(updatedBookedTraveler.getGender());
                    identityDocument.setGender(genderCodeType);
                } else if (null != currentBookedTraveler.getGender()) {
                    GenderCodeType genderCodeType = TransformerUtil.getGender(currentBookedTraveler.getGender());
                    identityDocument.setGender(genderCodeType);
                }

                if (null != updatedBookedTraveler.getDateOfBirth()) {
                    identityDocument.setBirthDate(updatedBookedTraveler.getDateOfBirth());
                } else if (null != currentBookedTraveler.getDateOfBirth()) {
                    identityDocument.setBirthDate(currentBookedTraveler.getDateOfBirth());
                }

                identityDocument.setSurname(currentBookedTraveler.getLastName());
                identityDocument.setGivenNames(currentBookedTraveler.getFirstName());

                if (null != updatedBookedTraveler.getTravelDocument()) {
                    identityDocument.setExpirationDate(updatedBookedTraveler.getTravelDocument().getExpirationDate());
                    identityDocument.setIssueDate(updatedBookedTraveler.getTravelDocument().getIssueDate());

                    Country country = new Country();
                    country.setCode(updatedBookedTraveler.getTravelDocument().getIssuingCountry());
                    identityDocument.setIssuingCountry(country);

                    Country nationality = new Country();
                    nationality.setCode(updatedBookedTraveler.getTravelDocument().getNationality());
                    identityDocument.setNationality(nationality);

                    identityDocument.setNumber(updatedBookedTraveler.getTravelDocument().getDocumentNumber());

                    identityDocument.setType(IdentityDocumentCodeType.PASSPORT);

                    if (updatedBookedTraveler.getTravelDocument().isPassportScanned()) {
                        identityDocument.setDataSourceType(DataSourceCodeType.MACHINE_READABLE_ZONE);
                    } else {
                        identityDocument.setDataSourceType(DataSourceCodeType.MANUAL);
                    }
                } else if (null != currentBookedTraveler.getTravelDocument()) {
                    identityDocument.setExpirationDate(currentBookedTraveler.getTravelDocument().getExpirationDate());
                    identityDocument.setIssueDate(currentBookedTraveler.getTravelDocument().getIssueDate());

                    Country country = new Country();
                    country.setCode(currentBookedTraveler.getTravelDocument().getIssuingCountry());
                    identityDocument.setIssuingCountry(country);

                    Country nationality = new Country();
                    nationality.setCode(currentBookedTraveler.getTravelDocument().getNationality());
                    identityDocument.setNationality(nationality);

                    identityDocument.setNumber(currentBookedTraveler.getTravelDocument().getDocumentNumber());

                    identityDocument.setType(IdentityDocumentCodeType.PASSPORT);

                    if (currentBookedTraveler.getTravelDocument().isPassportScanned()) {
                        identityDocument.setDataSourceType(DataSourceCodeType.MACHINE_READABLE_ZONE);
                    } else {
                        identityDocument.setDataSourceType(DataSourceCodeType.MANUAL);
                    }
                }

                if (null != updatedBookedTraveler.getVisaInfo()) {
                    thereWereChanges = true;

                    SupplementaryDocument supplementaryDocument = new SupplementaryDocument();
                    supplementaryDocument.setDataSourceType(DataSourceCodeType.MANUAL);
                    supplementaryDocument.setExpirationDate(updatedBookedTraveler.getVisaInfo().getExpirationDate());
                    Country issuingCountry = new Country();
                    issuingCountry.setCode(updatedBookedTraveler.getVisaInfo().getApplicableCountryCode());
                    supplementaryDocument.setIssuingCountry(issuingCountry);
                    supplementaryDocument.setNumber(updatedBookedTraveler.getVisaInfo().getNumber());
                    supplementaryDocument.setSupplementaryDocumentType(SupplementaryDocumentCodeType.VISA);
                    identityDocument.getSupplementaryDocuments().add(supplementaryDocument);
                }

                passengerUpdate.getAdvancePassengerInformation().setUpdateTravelDocuments(true);
                passengerUpdate.getAdvancePassengerInformation().getTravelDocuments().add(identityDocument);
            }
        }

        if (null != currentBookedTraveler.getInfant() && null != updatedBookedTraveler.getInfant() && null != passengerUpdate.getInfant()) {
            if ((null != updatedBookedTraveler.getInfant().getTravelDocument()
                    && !updatedBookedTraveler.getInfant().getTravelDocument().equals(currentBookedTraveler.getInfant().getTravelDocument()))
                    || (null != updatedBookedTraveler.getInfant().getVisaInfo()
                    && !updatedBookedTraveler.getInfant().getVisaInfo().equals(currentBookedTraveler.getInfant().getVisaInfo()))
            ) {

                thereWereChanges = true;

                IdentityDocument identityDocument = new IdentityDocument();

                if (null != updatedBookedTraveler.getInfant().getGender()) {
                    GenderCodeType genderCodeType = TransformerUtil.getGender(updatedBookedTraveler.getInfant().getGender());
                    identityDocument.setGender(genderCodeType);
                } else if (null != currentBookedTraveler.getInfant().getGender()) {
                    GenderCodeType genderCodeType = TransformerUtil.getGender(currentBookedTraveler.getInfant().getGender());
                    identityDocument.setGender(genderCodeType);
                }

                if (null != updatedBookedTraveler.getInfant().getDateOfBirth()) {
                    identityDocument.setBirthDate(updatedBookedTraveler.getInfant().getDateOfBirth());
                } else if (null != currentBookedTraveler.getInfant().getDateOfBirth()) {
                    identityDocument.setBirthDate(currentBookedTraveler.getInfant().getDateOfBirth());
                }

                identityDocument.setSurname(currentBookedTraveler.getInfant().getLastName());
                identityDocument.setGivenNames(currentBookedTraveler.getInfant().getFirstName());

                if (null != updatedBookedTraveler.getInfant().getTravelDocument()) {
                    identityDocument.setExpirationDate(updatedBookedTraveler.getInfant().getTravelDocument().getExpirationDate());
                    identityDocument.setIssueDate(updatedBookedTraveler.getInfant().getTravelDocument().getIssueDate());

                    Country country = new Country();
                    country.setCode(updatedBookedTraveler.getInfant().getTravelDocument().getIssuingCountry());
                    identityDocument.setIssuingCountry(country);

                    Country nationality = new Country();
                    nationality.setCode(updatedBookedTraveler.getInfant().getTravelDocument().getNationality());
                    identityDocument.setNationality(nationality);

                    identityDocument.setNumber(updatedBookedTraveler.getInfant().getTravelDocument().getDocumentNumber());

                    identityDocument.setType(IdentityDocumentCodeType.PASSPORT);

                    if (updatedBookedTraveler.getInfant().getTravelDocument().isPassportScanned()) {
                        identityDocument.setDataSourceType(DataSourceCodeType.MACHINE_READABLE_ZONE);
                    } else {
                        identityDocument.setDataSourceType(DataSourceCodeType.MANUAL);
                    }
                } else if (null != currentBookedTraveler.getInfant().getTravelDocument()) {
                    identityDocument.setExpirationDate(currentBookedTraveler.getInfant().getTravelDocument().getExpirationDate());
                    identityDocument.setIssueDate(currentBookedTraveler.getInfant().getTravelDocument().getIssueDate());

                    Country country = new Country();
                    country.setCode(currentBookedTraveler.getInfant().getTravelDocument().getIssuingCountry());
                    identityDocument.setIssuingCountry(country);

                    Country nationality = new Country();
                    nationality.setCode(currentBookedTraveler.getInfant().getTravelDocument().getNationality());
                    identityDocument.setNationality(nationality);

                    identityDocument.setNumber(currentBookedTraveler.getInfant().getTravelDocument().getDocumentNumber());

                    identityDocument.setType(IdentityDocumentCodeType.PASSPORT);

                    if (currentBookedTraveler.getInfant().getTravelDocument().isPassportScanned()) {
                        identityDocument.setDataSourceType(DataSourceCodeType.MACHINE_READABLE_ZONE);
                    } else {
                        identityDocument.setDataSourceType(DataSourceCodeType.MANUAL);
                    }
                }

                if (null != updatedBookedTraveler.getInfant().getVisaInfo()) {

                    thereWereChanges = true;

                    SupplementaryDocument supplementaryDocument = new SupplementaryDocument();
                    supplementaryDocument.setDataSourceType(DataSourceCodeType.MANUAL);
                    supplementaryDocument.setExpirationDate(updatedBookedTraveler.getInfant().getVisaInfo().getExpirationDate());
                    Country issuingCountry = new Country();
                    issuingCountry.setCode(updatedBookedTraveler.getInfant().getVisaInfo().getApplicableCountryCode());
                    supplementaryDocument.setIssuingCountry(issuingCountry);
                    supplementaryDocument.setNumber(updatedBookedTraveler.getInfant().getVisaInfo().getNumber());
                    supplementaryDocument.setSupplementaryDocumentType(SupplementaryDocumentCodeType.VISA);
                    identityDocument.getSupplementaryDocuments().add(supplementaryDocument);
                }

                passengerUpdate.getInfant().getAdvancePassengerInformation().setUpdateTravelDocuments(true);
                passengerUpdate.getInfant().getAdvancePassengerInformation().getTravelDocuments().add(identityDocument);
            }
        }

        return thereWereChanges;
    }

    private static boolean updateDestinationAddress(
            BookedTraveler currentBookedTraveler,
            BookedTraveler updatedBookedTraveler,
            PassengerInformation.PassengerUpdate passengerUpdate
    ) {
        boolean thereWereChanges = false;

        if (null != updatedBookedTraveler.getDestinationAddress()) {
            if (!updatedBookedTraveler.getDestinationAddress().equals(currentBookedTraveler.getDestinationAddress())) {

                PostalAddress postalAddress = new PostalAddress();
                postalAddress.setCityName(updatedBookedTraveler.getDestinationAddress().getCity());
                Country country = new Country();
                country.setCode(updatedBookedTraveler.getDestinationAddress().getCountry());
                postalAddress.setCountry(country);
                CountrySubDivision countrySubDivision = new CountrySubDivision();
                countrySubDivision.setCode(updatedBookedTraveler.getDestinationAddress().getState());
                postalAddress.setCountrySubDivision(countrySubDivision);
                postalAddress.setPostalCode(updatedBookedTraveler.getDestinationAddress().getZipCode());

                StringBuilder sb = new StringBuilder();
                sb.append(updatedBookedTraveler.getDestinationAddress().getAddressOne());
                if (null != updatedBookedTraveler.getDestinationAddress().getAddressTwo()) {
                    sb.append(" ");
                    sb.append(updatedBookedTraveler.getDestinationAddress().getAddressTwo());
                }

                postalAddress.setStreet(sb.toString());

                passengerUpdate.getAdvancePassengerInformation().setUpdateDestinationAddresses(true);
                passengerUpdate.getAdvancePassengerInformation().getDestinationAddresses().add(postalAddress);

                thereWereChanges = true;
            }
        }

        if (null != currentBookedTraveler.getInfant() && null != updatedBookedTraveler.getInfant() && null != passengerUpdate.getInfant()) {
            if (null != updatedBookedTraveler.getInfant().getDestinationAddress()) {
                if (!updatedBookedTraveler.getInfant().getDestinationAddress().equals(currentBookedTraveler.getInfant().getDestinationAddress())) {

                    PostalAddress postalAddress = new PostalAddress();
                    postalAddress.setCityName(updatedBookedTraveler.getInfant().getDestinationAddress().getCity());
                    Country country = new Country();
                    country.setCode(updatedBookedTraveler.getInfant().getDestinationAddress().getCountry());
                    postalAddress.setCountry(country);
                    CountrySubDivision countrySubDivision = new CountrySubDivision();
                    countrySubDivision.setCode(updatedBookedTraveler.getInfant().getDestinationAddress().getState());
                    postalAddress.setCountrySubDivision(countrySubDivision);
                    postalAddress.setPostalCode(updatedBookedTraveler.getInfant().getDestinationAddress().getZipCode());

                    StringBuilder sb = new StringBuilder();
                    sb.append(updatedBookedTraveler.getInfant().getDestinationAddress().getAddressOne());
                    if (null != updatedBookedTraveler.getInfant().getDestinationAddress().getAddressTwo()) {
                        sb.append(" ");
                        sb.append(updatedBookedTraveler.getInfant().getDestinationAddress().getAddressTwo());
                    }

                    postalAddress.setStreet(sb.toString());

                    passengerUpdate.getInfant().getAdvancePassengerInformation().setUpdateDestinationAddresses(true);
                    passengerUpdate.getInfant().getAdvancePassengerInformation().getDestinationAddresses().add(postalAddress);

                    thereWereChanges = true;
                }
            }
        }

        return thereWereChanges;
    }

    private static boolean updateOnwardTravelDate(BookedTraveler currentBookedTraveler, BookedTraveler updatedBookedTraveler, PassengerInformation.PassengerUpdate passengerUpdate) {
        boolean thereWereChanges = false;

        if (null != updatedBookedTraveler.getOnwardTravelDate()) {
            if (!updatedBookedTraveler.getOnwardTravelDate().equalsIgnoreCase(currentBookedTraveler.getOnwardTravelDate())) {
                passengerUpdate.getAdvancePassengerInformation().setUpdateOnwardTravelDate(true);
                passengerUpdate.getAdvancePassengerInformation().setOnwardTravelDate(updatedBookedTraveler.getOnwardTravelDate());

                thereWereChanges = true;
            }
        }

        if (null != currentBookedTraveler.getInfant() && null != updatedBookedTraveler.getInfant() && null != passengerUpdate.getInfant()) {
            if (null != updatedBookedTraveler.getInfant().getOnwardTravelDate()) {
                if (!updatedBookedTraveler.getInfant().getOnwardTravelDate().equalsIgnoreCase(currentBookedTraveler.getInfant().getOnwardTravelDate())) {
                    passengerUpdate.getInfant().getAdvancePassengerInformation().setUpdateOnwardTravelDate(true);
                    passengerUpdate.getInfant().getAdvancePassengerInformation().setOnwardTravelDate(updatedBookedTraveler.getInfant().getOnwardTravelDate());

                    thereWereChanges = true;
                }
            }
        }

        return thereWereChanges;
    }

    private static boolean updateDataFromTimaticInfo(BookedTraveler currentBookedTraveler, BookedTraveler updatedBookedTraveler, PassengerInformation.PassengerUpdate passengerUpdate) {

        boolean thereWereChanges = false;

        if (null != updatedBookedTraveler.getTimaticInfo()) {
            if (!updatedBookedTraveler.getTimaticInfo().equals(currentBookedTraveler.getTimaticInfo())) {

                passengerUpdate.getAdvancePassengerInformation().setUpdateOnwardTravelDate(true);
                passengerUpdate.getAdvancePassengerInformation().setOnwardTravelDate(updatedBookedTraveler.getTimaticInfo().getReturnDate());

                Country country = new Country();
                country.setCode(updatedBookedTraveler.getTimaticInfo().getResidencyCountry());
                passengerUpdate.getAdvancePassengerInformation().setUpdateCountryOfResidence(true);
                passengerUpdate.getAdvancePassengerInformation().setCountryOfResidence(country);

                thereWereChanges = true;
            }
        }

        if (null != currentBookedTraveler.getInfant() && null != updatedBookedTraveler.getInfant() && null != passengerUpdate.getInfant()) {
            if (null != updatedBookedTraveler.getInfant().getTimaticInfo()) {
                if (!updatedBookedTraveler.getInfant().getTimaticInfo().equals(currentBookedTraveler.getInfant().getTimaticInfo())) {

                    passengerUpdate.getInfant().getAdvancePassengerInformation().setUpdateOnwardTravelDate(true);
                    passengerUpdate.getInfant().getAdvancePassengerInformation().setOnwardTravelDate(updatedBookedTraveler.getInfant().getTimaticInfo().getReturnDate());

                    Country country = new Country();
                    country.setCode(updatedBookedTraveler.getInfant().getTimaticInfo().getResidencyCountry());
                    passengerUpdate.getInfant().getAdvancePassengerInformation().setUpdateCountryOfResidence(true);
                    passengerUpdate.getInfant().getAdvancePassengerInformation().setCountryOfResidence(country);

                    thereWereChanges = true;
                }
            }
        }

        return thereWereChanges;
    }
}
