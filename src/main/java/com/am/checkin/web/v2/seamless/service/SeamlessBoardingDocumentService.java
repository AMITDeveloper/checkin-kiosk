package com.am.checkin.web.v2.seamless.service;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.BoardingPass;
import com.aeromexico.commons.model.BoardingPassCollection;
import com.aeromexico.commons.model.BoardingPassWeb;
import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.Mandate;
import com.aeromexico.commons.model.PectabBoardingPass;
import com.aeromexico.commons.model.PectabBoardingPassList;
import com.aeromexico.commons.model.Seat;
import com.aeromexico.commons.model.SegmentDocument;
import com.aeromexico.commons.model.SelectPassengersRq;
import com.aeromexico.commons.model.rq.BoardingPassesRQ;
import com.aeromexico.commons.web.types.DigitalSignatureStatusType;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.util.ListsUtil;
import com.aeromexico.dao.config.SetUpConfigFactory;
import com.am.checkin.web.v2.seamless.model.LinkConstant;
import com.am.checkin.web.v2.services.AppleWalletUtilService;
import com.am.checkin.web.v2.services.PassengersSelectionService;
import com.am.checkin.web.v2.util.BoardingPassesUtil;
import com.am.checkin.web.v2.util.SystemVariablesUtil;
import com.am.checkin.web.v2.util.TravelPartyErrorUtil;
import com.am.seamless.checkin.common.models.exception.SeamlessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Named
@ApplicationScoped
public class SeamlessBoardingDocumentService {

    private static final Logger LOG = LoggerFactory.getLogger(SeamlessBoardingDocumentService.class);

    @Inject
    private SeamlessCheckinService seamlessCheckinService;

    @Inject
    private PassengersSelectionService passengersSelectionService;

    @Inject
    private AppleWalletUtilService appleWalletUtilService;

    @Inject
    @Named("CheckInSetUpConfigFactoryWrapper")
    private SetUpConfigFactory setUpConfigFactory;

    @PostConstruct
    public void init() {
        LOG.info("SeamlessBoardingDocumentService initialized.");
    }

    public BoardingPassCollection getBoardingPassCollection(
            BoardingPassesRQ boardingPassesRQ,
            CartPNR cartPNR,
            BookedLeg bookedLeg,
            boolean tsaRequired
    ) throws Exception {

        if (!areThereCheckedInPassengers(cartPNR)) {
            //throw error
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.NO_PASSENGERS_CHECKED_IN,
                    ErrorType.NO_PASSENGERS_CHECKED_IN.getErrorCode());
        }

        if (SystemVariablesUtil.isValidateSeamlessBoardingDocumentsLinkOnReprintEnabled()    //validate or not the link
                && null == cartPNR.getLinks().get(LinkConstant.BOARDING_DOCUMENTS)) {
            //throw error
            throw new SeamlessException(
                    TravelPartyErrorUtil.getSeamlessError(
                            com.am.seamless.checkin.common.models.types.ErrorType.NO_BOARDINGPASS_LINK_CHECKIN_NOT_ALLOWED,
                            null
                    ).toString(),
                    500
            );
        }

        if (areThereNotCheckedInPassengers(cartPNR)) {
            //call selection only for already checked in passengers
            SelectPassengersRq selectPassengersRq = new SelectPassengersRq();

            for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
                if (bookedTraveler.isCheckinStatus()) {
                    SelectPassengersRq.BookedTravelerUpdate bookedTravelerUpdate = new SelectPassengersRq.BookedTravelerUpdate();
                    bookedTravelerUpdate.setId(bookedTraveler.getId());
                    bookedTravelerUpdate.setSelectedToCheckin(true);
                    selectPassengersRq.getTravelerInfo().getCollection().add(bookedTravelerUpdate);
                }
            }

            passengersSelectionService.selectPassengers(selectPassengersRq, cartPNR);

        }

        if (!validateBoardingPasses(cartPNR)) {
            for (Mandate mandate : cartPNR.getMandates().getCollection()) {
                mandate.setConfirmed(true);
            }

            //call boarding documents and acceptance
            seamlessCheckinService.reprint(cartPNR, bookedLeg, tsaRequired, boardingPassesRQ.getPos());
        }

        LOG.info("cartPNR: {}", cartPNR.toString());

        BoardingPassCollection boardingPassCollection = new BoardingPassCollection();
        List<BoardingPass> boardingPassList = new ArrayList<>();

        for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {

            if (bookedTraveler.isCheckinStatus() && !bookedTraveler.isSelectee()) {

                for (SegmentDocument segmentDocument : bookedTraveler.getSegmentDocumentsList()) {

                    BookedSegment bookedSegment = bookedLeg.getBookedSegment(segmentDocument.getSegmentCode());

                    if (null != bookedSegment) {
                        BoardingPassWeb boardingPassWeb = new BoardingPassWeb();
                        boardingPassWeb.setSegmentCode(bookedSegment.getSegment().getSegmentCode());
                        boardingPassWeb.setClassOfService(bookedSegment.getSegment().getBookingClass());
                        segmentDocument.setBookingClass(bookedSegment.getSegment().getBookingClass());
                        boardingPassWeb.setNameRefNumber(bookedTraveler.getNameRefNumber());
                        boardingPassWeb.setFirstName(bookedTraveler.getFirstName());
                        boardingPassWeb.setLastName(bookedTraveler.getLastName());
                        boardingPassWeb.setFrequentFlyerAirline(bookedTraveler.getFrequentFlyerProgram());
                        boardingPassWeb.setFrequentFlyerNumber(bookedTraveler.getFrequentFlyerNumber());

                        boardingPassWeb.setBarcode(segmentDocument.getBarcode());
                        boardingPassWeb.setBarcodeImage(segmentDocument.getBarcodeImage());
                        boardingPassWeb.setQrCodeImage(segmentDocument.getQrcodeImage());

                        boardingPassWeb.setDigitalSignatureStatus(segmentDocument.getDigitalSignatureStatus());

                        if (null == bookedTraveler.getDigitalSignatureStatus()
                                || DigitalSignatureStatusType.OK.equals(bookedTraveler.getDigitalSignatureStatus())) {

                            if (DigitalSignatureStatusType.NOT_REQUIRED.equals(segmentDocument.getDigitalSignatureStatus())) {
                                bookedTraveler.setDigitalSignatureStatus(DigitalSignatureStatusType.OK);
                            } else {
                                bookedTraveler.setDigitalSignatureStatus(segmentDocument.getDigitalSignatureStatus());
                            }

                        } else if (DigitalSignatureStatusType.NOT_REQUIRED.equals(bookedTraveler.getDigitalSignatureStatus())
                                && DigitalSignatureStatusType.MISSING.equals(segmentDocument.getDigitalSignatureStatus())) {

                            bookedTraveler.setDigitalSignatureStatus(DigitalSignatureStatusType.MISSING);
                        }

                        boardingPassWeb.setControlCode(segmentDocument.getCheckInNumber());
                        boardingPassWeb.setClassOfService(boardingPassWeb.getClassOfService());
                        boardingPassWeb.setSelectee((bookedTraveler.isSelectee() || null != segmentDocument.getSelecteeText()));
                        boardingPassWeb.setTSAPre(segmentDocument.isTsaPreCheck());
                        boardingPassWeb.setSkyPriority(segmentDocument.isSkyPriority());
                        boardingPassWeb.setZone(segmentDocument.getBoardingZone());
                        boardingPassWeb.setTicketNumber(segmentDocument.getTicketNumber());
                        boardingPassWeb.setSegmentStatus(segmentDocument.getSegmentStatus());
                        Seat seat = BoardingPassesUtil.extractSeat(segmentDocument.getSeat(), boardingPassWeb.getClassOfService());
                        boardingPassWeb.setSeat(seat);

                        String link = BoardingPassesUtil.createDownloadLinkRackspace(
                                boardingPassesRQ.getRecordLocator(),
                                bookedSegment.getSegment().getDepartureDateTime().substring(0, 10).replace("-", "_"),
                                bookedLeg.getSegments().getLegCodeRackspace(),
                                bookedTraveler.getFirstName().substring(0, 1)
                                        + "_"
                                        + bookedTraveler.getLastName()
                                        + "_"
                                        + bookedTraveler.getTicketNumber()
                        );
                        boardingPassCollection.addItineraryPdf(bookedTraveler.getTicketNumber(), link);

                        try {
                            String appleWallet = appleWalletUtilService.calculateAppleWallet(
                                    segmentDocument.getSegmentStatus(),
                                    segmentDocument.getBarcode(),
                                    boardingPassWeb.getZone(),
                                    boardingPassWeb.getFirstName(),
                                    boardingPassWeb.getLastName(),
                                    (null != boardingPassWeb.getSeat()) ? boardingPassWeb.getSeat().getSeatCode() : null,
                                    (null != boardingPassWeb.getSeat()) ? boardingPassWeb.getSeat().getSectionCode() : null,
                                    boardingPassWeb.isSkyPriority(),
                                    boardingPassWeb.isTSAPre(),
                                    boardingPassesRQ.getPos(),
                                    boardingPassesRQ.getRecordLocator(),
                                    boardingPassWeb.getTicketNumber(),
                                    boardingPassWeb.getFrequentFlyerAirline(),
                                    boardingPassWeb.getFrequentFlyerNumber()
                            );

                            boardingPassWeb.setAppleWalletPass(appleWallet);

                            boardingPassList.add(boardingPassWeb);
                        } catch (Exception ex) {
                            LOG.error("Error trying to get applewallet string", ex);
                        }
                    } else {
                        LOG.error("Segment not found");
                    }
                }


                if (null != bookedTraveler.getInfant() && bookedTraveler.getInfant().isCheckinStatus()) {
                    for (SegmentDocument segmentDocument : bookedTraveler.getInfant().getSegmentDocumentsList()) {

                        BookedSegment bookedSegment = bookedLeg.getBookedSegment(segmentDocument.getSegmentCode());

                        if (null != bookedSegment) {
                            BoardingPassWeb boardingPassWeb = new BoardingPassWeb();
                            boardingPassWeb.setSegmentCode(bookedSegment.getSegment().getSegmentCode());
                            boardingPassWeb.setClassOfService(bookedSegment.getSegment().getBookingClass());
                            segmentDocument.setBookingClass(bookedSegment.getSegment().getBookingClass());
                            boardingPassWeb.setNameRefNumber(bookedTraveler.getInfant().getNameRefNumber());
                            boardingPassWeb.setFirstName(bookedTraveler.getInfant().getFirstName());
                            boardingPassWeb.setLastName(bookedTraveler.getInfant().getLastName());
                            boardingPassWeb.setFrequentFlyerAirline(null);
                            boardingPassWeb.setFrequentFlyerNumber(null);

                            boardingPassWeb.setBarcode(segmentDocument.getBarcode());
                            boardingPassWeb.setBarcodeImage(segmentDocument.getBarcodeImage());
                            boardingPassWeb.setQrCodeImage(segmentDocument.getQrcodeImage());

                            boardingPassWeb.setDigitalSignatureStatus(segmentDocument.getDigitalSignatureStatus());

                            if (null == bookedTraveler.getDigitalSignatureStatus()
                                    || DigitalSignatureStatusType.OK.equals(bookedTraveler.getDigitalSignatureStatus())) {

                                if (DigitalSignatureStatusType.NOT_REQUIRED.equals(segmentDocument.getDigitalSignatureStatus())) {
                                    bookedTraveler.setDigitalSignatureStatus(DigitalSignatureStatusType.OK);
                                } else {
                                    bookedTraveler.setDigitalSignatureStatus(segmentDocument.getDigitalSignatureStatus());
                                }

                            } else if (DigitalSignatureStatusType.NOT_REQUIRED.equals(bookedTraveler.getDigitalSignatureStatus())
                                    && DigitalSignatureStatusType.MISSING.equals(segmentDocument.getDigitalSignatureStatus())) {

                                bookedTraveler.setDigitalSignatureStatus(DigitalSignatureStatusType.MISSING);
                            }

                            boardingPassWeb.setControlCode(segmentDocument.getCheckInNumber());
                            boardingPassWeb.setClassOfService(boardingPassWeb.getClassOfService());
                            boardingPassWeb.setSelectee((bookedTraveler.isSelectee() || null != segmentDocument.getSelecteeText()));
                            boardingPassWeb.setTSAPre(segmentDocument.isTsaPreCheck());
                            boardingPassWeb.setSkyPriority(segmentDocument.isSkyPriority());
                            boardingPassWeb.setZone(segmentDocument.getBoardingZone());
                            boardingPassWeb.setTicketNumber(segmentDocument.getTicketNumber());
                            boardingPassWeb.setSegmentStatus(segmentDocument.getSegmentStatus());
                            Seat seat = BoardingPassesUtil.extractSeat(segmentDocument.getSeat(), boardingPassWeb.getClassOfService());
                            boardingPassWeb.setSeat(seat);

                            String link = BoardingPassesUtil.createDownloadLinkRackspace(
                                    boardingPassesRQ.getRecordLocator(),
                                    bookedSegment.getSegment().getDepartureDateTime().substring(0, 10).replace("-", "_"),
                                    bookedLeg.getSegments().getLegCodeRackspace(),
                                    bookedTraveler.getInfant().getFirstName().substring(0, 1)
                                            + "_"
                                            + bookedTraveler.getInfant().getLastName()
                                            + "_"
                                            + bookedTraveler.getInfant().getTicketNumber()
                            );
                            boardingPassCollection.addItineraryPdf(bookedTraveler.getInfant().getTicketNumber(), link);

                            try {
                                String appleWallet = appleWalletUtilService.calculateAppleWallet(
                                        segmentDocument.getSegmentStatus(),
                                        segmentDocument.getBarcode(),
                                        boardingPassWeb.getZone(),
                                        boardingPassWeb.getFirstName(),
                                        boardingPassWeb.getLastName(),
                                        (null != boardingPassWeb.getSeat()) ? boardingPassWeb.getSeat().getSeatCode() : null,
                                        (null != boardingPassWeb.getSeat()) ? boardingPassWeb.getSeat().getSectionCode() : null,
                                        boardingPassWeb.isSkyPriority(),
                                        boardingPassWeb.isTSAPre(),
                                        boardingPassesRQ.getPos(),
                                        boardingPassesRQ.getRecordLocator(),
                                        boardingPassWeb.getTicketNumber(),
                                        boardingPassWeb.getFrequentFlyerAirline(),
                                        boardingPassWeb.getFrequentFlyerNumber()
                                );

                                boardingPassWeb.setAppleWalletPass(appleWallet);

                                boardingPassList.add(boardingPassWeb);
                            } catch (Exception ex) {
                                LOG.error("Error trying to get applewallet string", ex);
                            }
                        } else {
                            LOG.error("Segment not found");
                        }
                    }
                }
            } else {
                bookedTraveler.setDigitalSignatureStatus(DigitalSignatureStatusType.NOT_CHECKED_IN);
            }
        }


        if (cartPNR.getTravelerInfo().getCollection().size() == 1) {
            boardingPassCollection.addItineraryPdf(
                    "ALL",
                    boardingPassCollection.getItineraryPdf().get(cartPNR.getTravelerInfo().getCollection().get(0).getTicketNumber())
            );
        } else {
            String link = BoardingPassesUtil.createDownloadLinkRackspace(
                    boardingPassesRQ.getRecordLocator(),
                    bookedLeg.getSegments().getCollection().get(0).getSegment().getDepartureDateTime().substring(0, 10).replace("-", "_"),
                    bookedLeg.getSegments().getLegCodeRackspace(),
                    "ALL"
            );
            boardingPassCollection.addItineraryPdf("ALL", link);
        }


        if (boardingPassList.isEmpty()) {
            return null;
        } else {
            LOG.info("Setting final data for boarding collection");

            BookedSegment lastBookedSegment = ListsUtil.getLast(bookedLeg.getSegments().getCollection());

            PectabBoardingPassList pectabBoardingPassList = new PectabBoardingPassList();
            PectabBoardingPass pectabBoarding = new PectabBoardingPass();
            pectabBoarding.setValue("");
            pectabBoarding.setVersion("");
            pectabBoardingPassList.add(pectabBoarding);
            boardingPassCollection.setPectabBoardingPassList(pectabBoardingPassList);

            boardingPassCollection.setPnr(boardingPassesRQ.getRecordLocator());
            boardingPassCollection.setPos(boardingPassesRQ.getPos());

            boardingPassCollection.setDailyForecasts(
                    BoardingPassesUtil.getWeatherForService(
                            lastBookedSegment.getSegment().getArrivalAirport(),
                            boardingPassesRQ.getPos(),
                            boardingPassesRQ.getLanguage()
                    )
            );
            boardingPassCollection.setCollection(boardingPassList);

            return boardingPassCollection;
        }
    }

    public static boolean areThereCheckedInPassengers(CartPNR cartPNR) {
        for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
            if (bookedTraveler.isCheckinStatus()) {
                return true;
            }
        }

        return false;
    }

    public static boolean validateBoardingPasses(CartPNR cartPNR) {
        boolean validate = true;

        for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
            if (!bookedTraveler.isCheckinStatus()
                    || null == bookedTraveler.getSegmentDocumentsList()
                    || bookedTraveler.getSegmentDocumentsList().isEmpty()) {
                validate = false;
            }

            if (bookedTraveler.isCheckinStatus()) {
                bookedTraveler.setIsSelectedToCheckin(true);
            }
        }

        return validate;
    }

    public static boolean areThereNotCheckedInPassengers(CartPNR cartPNR) {
        for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
            if (!bookedTraveler.isCheckinStatus()) {
                return true;
            }
        }

        return false;
    }
}
