package com.am.checkin.web.v2.util;

import com.aeromexico.commons.model.ExtraInfo;
import com.aeromexico.commons.model.Incident;
import com.aeromexico.commons.web.types.ActionableType;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.types.PosType;
import com.am.seamless.checkin.common.models.errorhandling.SeamlessError;
import com.am.seamless.checkin.common.models.exception.SdsError;
import com.am.seamless.checkin.common.models.exception.SeamlessException;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SeamlessIncidentUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(SeamlessIncidentUtil.class);

    private static final Gson GSON = new Gson();

    public static Incident getSeamlessIncident(SeamlessException exception, String posType) {
        Incident incident = new Incident();
        ExtraInfo extraInfo = new ExtraInfo();
        try {
            SeamlessError seamlessError = GSON.fromJson(exception.getMessage(), SeamlessError.class);

            if (!seamlessError.getExceptions().isEmpty()) {
                SdsError sdsError = seamlessError.getExceptions().get(0);

                incident.setActionable(ErrorType.INTERNAL_ERROR.getActionable());

                if (PosType.WEB.getCode().equalsIgnoreCase(posType)) {
                    incident.setCode(SeamlessErrorMapper.getWebError(sdsError.getCode()));
                } else if (PosType.KIOSK.getCode().equalsIgnoreCase(posType)) {
                    incident.setCode(SeamlessErrorMapper.getKioskError(sdsError.getCode()));
                } else if (PosType.CHECKIN_MOBILE.getCode().equalsIgnoreCase(posType)) {
                    incident.setCode(SeamlessErrorMapper.getMobileError(sdsError.getCode()));
                } else {
                    incident.setCode(sdsError.getCode());
                }

                if (null == incident.getCode() || incident.getCode().isEmpty()) {
                    incident.setCode(sdsError.getCode());
                }

                incident.setMsg(sdsError.getDescription());
                extraInfo.setExtra(sdsError.getTechnicalSupplement());

            } else if (null != seamlessError.getErrorCode()) {

                try {
                    incident.setActionable(ActionableType.valueOf(seamlessError.getActionable()));
                } catch (Exception ex) {
                    incident.setActionable(ActionableType.ABORT);
                }

                if (PosType.WEB.getCode().equalsIgnoreCase(posType)) {
                    incident.setCode(SeamlessErrorMapper.getWebError(seamlessError.getErrorCode()));
                } else if (PosType.KIOSK.getCode().equalsIgnoreCase(posType)) {
                    incident.setCode(SeamlessErrorMapper.getKioskError(seamlessError.getErrorCode()));
                } else if (PosType.CHECKIN_MOBILE.getCode().equalsIgnoreCase(posType)) {
                    incident.setCode(SeamlessErrorMapper.getMobileError(seamlessError.getErrorCode()));
                } else {
                    incident.setCode(seamlessError.getErrorCode());
                }

                if (null == incident.getCode() || incident.getCode().isEmpty()) {
                    incident.setCode(seamlessError.getErrorCode());
                }

                incident.setMsg(seamlessError.getErrorMessage());

                extraInfo.setExtra(seamlessError.getDescription());

            } else {
                incident.setActionable(ErrorType.INTERNAL_ERROR.getActionable());
                incident.setCode(ErrorType.INTERNAL_ERROR.getErrorCode());
                incident.setMsg(exception.getMessage());
            }

            String debug = "";
            try {
                if (null != exception.getStackTrace() && exception.getStackTrace().length > 0) {
                    debug = exception.getStackTrace()[0].toString();
                } else {
                    debug = "";
                }
            } catch (Exception e) {
                debug = "";
            }

            extraInfo.setDebugging_stuff_here(debug);
            incident.setExtraInfo(extraInfo);

        } catch (Exception ex) {

            incident.setActionable(ErrorType.INTERNAL_ERROR.getActionable());
            incident.setCode(ErrorType.INTERNAL_ERROR.getErrorCode());
            incident.setMsg(exception.getMessage());

            extraInfo.setExtra(ex.getMessage());

            String debug = "";
            try {
                if (null != exception.getStackTrace() && exception.getStackTrace().length > 0) {
                    debug = exception.getStackTrace()[0].toString();
                } else {
                    debug = "";
                }
            } catch (Exception e) {
                debug = "";
            }

            extraInfo.setDebugging_stuff_here(debug);
            incident.setExtraInfo(extraInfo);


            LOGGER.error("Incident_generalException: " + incident.toString());
            LOGGER.error("Incident_generalException: " + ex.getMessage(), ex);

        }

        return incident;
    }
}
