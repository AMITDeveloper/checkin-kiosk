package com.am.checkin.web.v2.util;

import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.BookingClass;
import com.aeromexico.commons.model.PriorityBySegment;
import com.aeromexico.commons.web.util.ListsUtil;
import com.aeromexico.dao.util.CommonUtil;
import com.sabre.services.acs.bso.addtoprioritylist.v3.ItineraryACS;
import com.sabre.services.acs.bso.addtoprioritylist.v3.PassengerInfoACS;
import com.sabre.services.acs.bso.addtoprioritylist.v3.PassengerInfoListACS;
import com.sabre.services.acs.bso.addtoprioritylist.v3.PriorityClassificationInfoACS;

import java.util.List;

public class CheckinUtil {

    /**
     *
     * @param bookedSegmentListWithSameFlight
     * @return
     */
    public static ItineraryACS getItineraryListFromSameSegments(
            List<BookedSegment> bookedSegmentListWithSameFlight
    ) {
        BookedSegment firstSegment = ListsUtil.getFirst(bookedSegmentListWithSameFlight);
        BookedSegment lastSegment = ListsUtil.getLast(bookedSegmentListWithSameFlight);

        ItineraryACS itineraryACS = getItineraryListFromSameSegments(firstSegment);
        itineraryACS.setDestination(lastSegment.getSegment().getArrivalAirport());

        return itineraryACS;
    }

    /**
     *
     * @param bookedSegment
     * @return
     */
    public static ItineraryACS getItineraryListFromSameSegments(
            BookedSegment bookedSegment
    ) {
        ItineraryACS itineraryACS = new ItineraryACS();

        itineraryACS.setAirline(bookedSegment.getSegment().getOperatingCarrier());
        itineraryACS.setFlight(bookedSegment.getSegment().getOperatingFlightCode());
        itineraryACS.setBookingClass(bookedSegment.getSegment().getBookingClass());
        itineraryACS.setDepartureDate(CommonUtil.getOnlyDatePart(bookedSegment.getSegment().getDepartureDateTime()));
        itineraryACS.setOrigin(bookedSegment.getSegment().getDepartureAirport());
        itineraryACS.setDestination(bookedSegment.getSegment().getArrivalAirport());

        return itineraryACS;
    }

    /**
     *
     * @param bookedTravelerList
     * @param segmentCode
     * @return
     */
    public static PassengerInfoListACS getPassengerInfoList(
            List<BookedTraveler> bookedTravelerList,
            String segmentCode
    ){
        PassengerInfoListACS passengerInfoList = new PassengerInfoListACS();

        for (BookedTraveler bookedTraveler: bookedTravelerList) {
            PriorityBySegment priorityBySegment = bookedTraveler.getPriorityBySegmentCollection().getPrioritySegment(segmentCode);
            BookingClass bookingClass = bookedTraveler.getBookingClasses().getBookingClass(segmentCode);

            if (null != bookingClass
                    && null != bookingClass.getPassengerId()
                    && null != priorityBySegment
                    && priorityBySegment.isApplyForUpgrade()) {

                PassengerInfoACS passengerInfo = new PassengerInfoACS();
                passengerInfo.setLastName(bookedTraveler.getLastName());
                passengerInfo.setPassengerID(bookingClass.getPassengerId());
                if(null != bookedTraveler.getPriorityCode()) {
                    PriorityClassificationInfoACS priorityClassificationInfo = new PriorityClassificationInfoACS();
                    priorityClassificationInfo.setPriorityCode(bookedTraveler.getPriorityCode());
                    passengerInfo.setPriorityClassificationInfo(priorityClassificationInfo);
                }
                passengerInfo.getNOBAGS();
                passengerInfoList.getPassengerInfo().add(passengerInfo);
            }
        }

        return passengerInfoList;
    }

    /**
     *
     * @param bookedTravelerList
     * @param segmentCode
     * @return
     */
    public static PassengerInfoListACS getPassengerInfoListCheckInService(
            List<BookedTraveler> bookedTravelerList,
            String segmentCode
    ){
        PassengerInfoListACS passengerInfoList = new PassengerInfoListACS();

        for (BookedTraveler bookedTraveler: bookedTravelerList) {
            BookingClass bookingClass = bookedTraveler.getBookingClasses().getBookingClass(segmentCode);

            PassengerInfoACS passengerInfo = new PassengerInfoACS();
            passengerInfo.setLastName(bookedTraveler.getLastName());
            passengerInfo.setPassengerID(bookingClass.getPassengerId());
            if(null != bookedTraveler.getPriorityCode()) {
                PriorityClassificationInfoACS priorityClassificationInfo = new PriorityClassificationInfoACS();
                priorityClassificationInfo.setPriorityCode(bookedTraveler.getPriorityCode());
                passengerInfo.setPriorityClassificationInfo(priorityClassificationInfo);
            }else{
                PriorityClassificationInfoACS priorityClassificationInfo = new PriorityClassificationInfoACS();
                priorityClassificationInfo.setPriorityCode("OVS");
                passengerInfo.setPriorityClassificationInfo(priorityClassificationInfo);
            }
            passengerInfo.getNOBAGS();
            passengerInfoList.getPassengerInfo().add(passengerInfo);
            
        }

        return passengerInfoList;
    }
}
