package com.am.checkin.web.v2.services;

import com.aeromexico.commons.model.FlightStatusCollection;
import com.aeromexico.commons.model.SystemPropertiesFactory;
import com.aeromexico.commons.model.rq.FlightStatusRQ;
import com.aeromexico.commons.prototypes.JSONPrototype;
import com.aeromexico.sharedservices.clients.FlightStatusApiClient;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Armando Arroyo
 */
@Named
@ApplicationScoped
public class FlightStatusService {
    
    private static final Logger log = LoggerFactory.getLogger(FlightStatusService.class);
    
    @Inject
    private FlightStatusApiClient flightStatusApiClient;
    
    private static final String SCHEDULE_SERVICE_NAME = "/schedules";
    private static final String FLIGHT_SERVICE_NAME = "/flight-stats";


    /**
     * 
     * @param flightStatusRQ
     * @return
     * @throws Exception 
     */
    public FlightStatusCollection callFlightStatus(FlightStatusRQ flightStatusRQ) throws Exception {
        if (null == flightStatusRQ) {
            throw new Exception("Flight Status Request is null");
        }
        log.info("FlightStatusRQ: {}", flightStatusRQ.toString());
        //Do we have the flight number in the request?
        if (null != flightStatusRQ.getFlight() && !flightStatusRQ.getFlight().trim().isEmpty()) {
            return getStatusForFlight(flightStatusRQ);
        } else if (null != flightStatusRQ.getOrigin() && null != flightStatusRQ.getDestination()) {
            return getStatusByOriginAndDestination(flightStatusRQ);
        } else {
            log.error("Invalid FlightStatusService request: " + flightStatusRQ);
        }

        return new FlightStatusCollection();
    }
    
    
    private FlightStatusCollection getStatusForFlight(FlightStatusRQ flightStatusRQ) throws Exception {
        MultivaluedMap<String, Object> queryParams = new MultivaluedHashMap<>();
        queryParams.add("flight", flightStatusRQ.getFlight());
        queryParams.add("airlineCode", "AM");
        queryParams.add("departureDate", flightStatusRQ.getDate());
                               
        Response response = flightStatusApiClient.get(FLIGHT_SERVICE_NAME, queryParams);        
        String json = response.readEntity(String.class);
        log.info("response: {}", response);
        log.info("json: {}", json);
        return JSONPrototype.convert(json, FlightStatusCollection.class);                
    }
    
    private FlightStatusCollection getStatusByOriginAndDestination(FlightStatusRQ flightStatusRQ) throws Exception {
        MultivaluedMap<String, Object> queryParams = new MultivaluedHashMap<>();
        queryParams.add("origin", flightStatusRQ.getOrigin());
        queryParams.add("destination", flightStatusRQ.getDestination());
        queryParams.add("departureDate", flightStatusRQ.getDate());
                               
        Response response = flightStatusApiClient.get(SCHEDULE_SERVICE_NAME, queryParams);        
        String json = response.readEntity(String.class);
        log.info("response: {}", response);
        log.info("json: {}", json);
        
        return JSONPrototype.convert(json, FlightStatusCollection.class);
    }       
    
}
