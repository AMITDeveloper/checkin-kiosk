package com.am.checkin.web.v2.services;

import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adrianleal
 */
public class CheckingBridgeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CheckingBridgeService.class);

    @Inject
    private CheckinForSegmentService checkinForSegmentService;
}
