package com.am.checkin.web.v2.controller;

import com.aeromexico.sharedservices.util.AESEncryption;
import com.am.checkin.web.v2.model.DigitalSignatureRq;
import com.am.checkin.web.v2.model.DigitalSignatureRs;
import com.am.checkin.web.v2.model.EarlyEncryptModelRQ;
import com.am.checkin.web.v2.services.DigitalSignatureService;
import com.am.checkin.web.v2.util.SystemVariablesUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.BeanParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("admin")
public class AdminController {

    private static final Logger log = LoggerFactory.getLogger(AdminController.class);

    @Inject
    private DigitalSignatureService digitalSignatureService;

    @POST
    @Path("/early/encrypt")
    @Produces(MediaType.APPLICATION_JSON)
    public Response encrypt(@Valid @BeanParam EarlyEncryptModelRQ encryptModelRQ) throws Exception {

        String pass = "";
        try {
            pass = AESEncryption.encrypt(encryptModelRQ.getRequest(), SystemVariablesUtil.getEarlyCheckinSeed());
        } catch (Exception e) {
            log.error("Encrypt: " + e.getMessage(), e);
            throw e;
        }
        return Response.status(Response.Status.OK).entity(pass).build();
    }

    @POST
    @Path("/early/decrypt")
    @Produces(MediaType.APPLICATION_JSON)
    public Response decrypt(@Valid @BeanParam EarlyEncryptModelRQ encryptModelRQ) throws Exception {

        String pass = "";
        try {
            pass = AESEncryption.decrypt(encryptModelRQ.getRequest(), SystemVariablesUtil.getEarlyCheckinSeed());
        } catch (Exception e) {
            log.error("Decrypt: " + e.getMessage(), e);
            throw e;
        }
        return Response.status(Response.Status.OK).entity(pass).build();
    }

    @POST
    @Path("/signBoardingpass")
    @Produces(MediaType.APPLICATION_JSON)
    public Response signBoardingpass(@Valid DigitalSignatureRq digitalSignatureRq) throws Exception {

        DigitalSignatureRs digitalSignatureRs = digitalSignatureService.signBoardingPassSync(digitalSignatureRq);

        return Response.status(Response.Status.OK).entity(digitalSignatureRs).build();
    }
}
