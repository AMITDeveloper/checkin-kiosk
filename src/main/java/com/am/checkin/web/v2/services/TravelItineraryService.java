package com.am.checkin.web.v2.services;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.rq.PnrRQ;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.types.PosType;
import com.aeromexico.commons.web.util.Constants;
import com.aeromexico.commons.web.util.ErrorCodeDescriptions;
import com.aeromexico.dao.util.CommonUtil;
import com.aeromexico.sabre.api.session.common.ServiceCall;
import com.aeromexico.sabre.api.session.exception.SabreEmptyResponseException;
import com.aeromexico.sabre.api.session.exception.SabreLayerUnavailableException;
import com.aeromexico.sabre.api.travelitinerary.AMTravelItineraryService;
import com.aeromexico.sharedservices.util.TravelerItineraryUtil;
import com.sabre.services.res.tir.travelitinerary.TravelItineraryReadRS;
import com.sabre.services.stl_header.v120.CompletionCodes;
import org.aeromexico.commons.exception.config.ReadProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;
import java.util.List;

@Named
@ApplicationScoped
public class TravelItineraryService {

    private static final Logger LOG = LoggerFactory.getLogger(TravelItineraryService.class);

    @Inject
    private AMTravelItineraryService amTravelItineraryService;

    @Inject
    private ReadProperties prop;

    @Inject
    private SearchReservationService searchReservationService;

    /**
     * @param pnrRQ
     * @param serviceCallList
     * @return
     * @throws Exception
     */
    public TravelItineraryReadRS getTravelItineraryReadRS(PnrRQ pnrRQ, List<ServiceCall> serviceCallList) throws Exception {

        TravelItineraryReadRS travelItineraryReadRS = null;

        try {
            travelItineraryReadRS = getAmTravelItineraryService().TravelItinerary(pnrRQ.getRecordLocator(), serviceCallList);
        } catch (SabreEmptyResponseException ser) {
            LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110302).toString(), ser.fillInStackTrace());
            throw ser;
        } catch (SabreLayerUnavailableException se) {
            if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(pnrRQ.getPos())) {
                LOG.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_22110301,"TravelItineraryService.java:59", se.getMessage());
                throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getCode(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getActionable(),
                        prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getMessage(),
                        Response.Status.INTERNAL_SERVER_ERROR);
            } else {
                LOG.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_12110301,"TravelItineraryService.java:65", se.getMessage());
                throw se;
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.INTERNAL_ERROR, ErrorCodeDescriptions.INTERNAL_SABRE_ERROR);
        }

        if (travelItineraryReadRS == null) {
            throw new GenericException(getProp().getMessagesError(Constants.CODE_12110301).getCode(),
                    getProp().getMessagesError(Constants.CODE_12110301).getActionable(),
                    getProp().getMessagesError(Constants.CODE_12110301).getMessage(),
                    Response.Status.INTERNAL_SERVER_ERROR);
        }

        //PNR was NOT found when we get a status of "NotProcessed"
        if (CompletionCodes.NOT_PROCESSED.toString().equalsIgnoreCase(travelItineraryReadRS.getApplicationResults().getStatus().toString())) {

            String recordLocator = getSearchReservationService().searchByAgencyRecordlocator(
                    pnrRQ, serviceCallList
            );

            pnrRQ.setRecordLocator(
                    recordLocator
            );

            try {
                travelItineraryReadRS = getAmTravelItineraryService().TravelItinerary(pnrRQ.getRecordLocator(), serviceCallList);
            } catch (SabreEmptyResponseException ser) {
                LOG.error(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_12110302).toString(), ser.fillInStackTrace());
                throw ser;
            } catch (SabreLayerUnavailableException se) {
                if (PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(pnrRQ.getPos())) {
                    LOG.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_22110301,"TravelItineraryService.java:98", se.getMessage());
                    throw new GenericException(prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getCode(),
                            prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getActionable(),
                            prop.getMessagesError(com.aeromexico.commons.web.util.Constants.CODE_22110301).getMessage(),
                            Response.Status.INTERNAL_SERVER_ERROR);
                } else {
                    LOG.error("ERROR_CODE = {} | COMPONENTE = {} | ERROR = {}",com.aeromexico.commons.web.util.Constants.CODE_12110301,"TravelItineraryService.java:104", se.getMessage());
                    throw se;
                }
            } catch (Exception e) {
                if (null != pnrRQ.getPos() && PosType.WEB.toString().equalsIgnoreCase(pnrRQ.getPos())) {
                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.RESERVATION_NOT_FOUND_WEB, CommonUtil.getMessageByCode(Constants.CODE_051));
                } else {
                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.RESERVATION_NOT_FOUND, CommonUtil.getMessageByCode(Constants.CODE_051));
                }
            }

            if (travelItineraryReadRS == null) {
//                throw new GenericException(Response.Status.INTERNAL_SERVER_ERROR, ErrorType.INTERNAL_ERROR, ErrorCodeDescriptions.INTERNAL_SABRE_ERROR);
                throw new GenericException(getProp().getMessagesError(Constants.CODE_12110301).getCode(),
                        getProp().getMessagesError(Constants.CODE_12110301).getActionable(),
                        getProp().getMessagesError(Constants.CODE_12110301).getMessage(),
                        Response.Status.INTERNAL_SERVER_ERROR);
            }

            if (CompletionCodes.NOT_PROCESSED.toString().equalsIgnoreCase(travelItineraryReadRS.getApplicationResults().getStatus().toString())) {
                if (null != pnrRQ.getPos() && PosType.WEB.toString().equalsIgnoreCase(pnrRQ.getPos())) {
                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.RESERVATION_NOT_FOUND_WEB, CommonUtil.getMessageByCode(Constants.CODE_046));
                } else {
                    throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.RESERVATION_NOT_FOUND, CommonUtil.getMessageByCode(Constants.CODE_046));
                }

            }
        }

        if (null == travelItineraryReadRS.getApplicationResults() || travelItineraryReadRS.getApplicationResults().getStatus() != CompletionCodes.COMPLETE) {
            throw new GenericException(getProp().getMessagesError(Constants.CODE_12110301).getCode(),
                    getProp().getMessagesError(Constants.CODE_12110301).getActionable(),
                    getProp().getMessagesError(Constants.CODE_12110301).getMessage(),
                    Response.Status.INTERNAL_SERVER_ERROR);
        }

        if (!pnrRQ.isSkipNamesValidation() && (PosType.WEB.toString().equalsIgnoreCase(pnrRQ.getPos())
                || PosType.CHECKIN_MOBILE.toString().equalsIgnoreCase(pnrRQ.getPos()))) {
            if (!isLastNameMatchingInReservation(travelItineraryReadRS, pnrRQ.getLastName())) {
                throw new GenericException(
                        Response.Status.BAD_REQUEST,
                        ErrorType.NAME_NOT_FOUND_IN_PNR_CHECKIN,
                        ErrorType.NAME_NOT_FOUND_IN_PNR_CHECKIN.getFullDescription()
                );
            }
        }

//        TravelerItineraryUtil.filterTravelItinerarySegments(travelItineraryReadRS);

//        if (!TravelerItineraryUtil.validateTravelItinerarySegments(travelItineraryReadRS)) {
//            throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.INVALID_ITINERARY, "NO AVAILABLE SEGMENTS IN TRAVEL ITINERARY");
//        }

        return travelItineraryReadRS;

    }

    private boolean isLastNameMatchingInReservation(TravelItineraryReadRS travelItineraryReadRS, String lastName) {

        for (TravelItineraryReadRS.TravelItinerary.CustomerInfo.PersonName personName : travelItineraryReadRS.getTravelItinerary().getCustomerInfo().getPersonName()) {

            if (personName.getSurname() != null) {

                //Earlier validation should have validated that the lastName provided in the request is NOT null
                //If the given last name is less than three letters, do an exact match
                if (lastName.length() < 3) {
                    if (lastName.equalsIgnoreCase(personName.getSurname())) {
                        return true;
                    }
                } else if (personName.getSurname().toUpperCase().startsWith(lastName.toUpperCase().substring(0, 3))) {
                    return true;
                }
            }

        }

        return false;
    }

    public AMTravelItineraryService getAmTravelItineraryService() {
        return amTravelItineraryService;
    }

    public void setAmTravelItineraryService(AMTravelItineraryService amTravelItineraryService) {
        this.amTravelItineraryService = amTravelItineraryService;
    }

    public ReadProperties getProp() {
        return prop;
    }

    public void setProp(ReadProperties prop) {
        this.prop = prop;
    }

    public SearchReservationService getSearchReservationService() {
        return searchReservationService;
    }

    public void setSearchReservationService(SearchReservationService searchReservationService) {
        this.searchReservationService = searchReservationService;
    }
}
