package com.am.checkin.web.v2.config;

import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.KieModule;
import org.kie.api.runtime.KieContainer;
import org.kie.internal.io.ResourceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DroolsConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger(DroolsConfig.class);

    private static final String RULES_PATH = "rules/";

    private static KieContainer instance;

    private DroolsConfig() {
    }

    private static KieServices getKieServices() {
        return KieServices.Factory.get();
    }

    private static String[] getRuleFiles() {
        final String[] files = {
                "UPGRADE_ELIGIBILITY_AM_CLASSIC_RULE.drl",
                "UPGRADE_ELIGIBILITY_AM_ORO_RULE.drl",
                "UPGRADE_ELIGIBILITY_AM_PLATINO_RULE.drl",
                "UPGRADE_ELIGIBILITY_AM_TITANIO_RULE.drl",
                "UPGRADE_ELIGIBILITY_DL_DIAMANTE_RULE.drl",
                "UPGRADE_ELIGIBILITY_DL_ORO_RULE.drl",
                "UPGRADE_ELIGIBILITY_DL_PLATA_RULE.drl",
                "UPGRADE_ELIGIBILITY_DL_PLATINO_RULE.drl",
                "UPGRADE_ELIGIBILITY_NO_TIER_RULE.drl"
        };
        return files;
    }

    private static KieFileSystem kieFileSystem() {
        KieFileSystem kieFileSystem = getKieServices().newKieFileSystem();

        for (String filename : getRuleFiles()) {

            LOGGER.info(filename);

            kieFileSystem.write(ResourceFactory.newClassPathResource(RULES_PATH + filename, "UTF-8"));
        }

        return kieFileSystem;
    }

    private static KieContainer kieContainer() {
        KieServices kieServices = getKieServices();

        KieBuilder kieBuilder = kieServices.newKieBuilder(kieFileSystem());
        kieBuilder.buildAll();
        KieModule kieModule = kieBuilder.getKieModule();

        return kieServices.newKieContainer(kieModule.getReleaseId());
    }

    public static KieContainer getInstance() {
        if (null == instance) {
            instance = kieContainer();
        }

        return instance;
    }
}
