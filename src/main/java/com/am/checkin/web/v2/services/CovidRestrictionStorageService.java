package com.am.checkin.web.v2.services;

import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.CovidRestrictionForm;
import com.aeromexico.commons.web.util.Constants;
import com.aeromexico.dao.event.DocumentEvent;
import com.aeromexico.dao.services.IPNRLookupDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Named
@ApplicationScoped
public class CovidRestrictionStorageService {

    private static final Logger LOG = LoggerFactory.getLogger(CovidRestrictionStorageService.class);

    @Inject
    private IPNRLookupDao pnrLookupDao;

    public void saveCovidRestrictionForm(String pnr, CartPNR cartPNR, BookedLeg bookedLeg) {

        try {

            if (cartPNR.isCovidRestrictionFormAlreadySent()) {
                return;
            }

            Date now = new Date();

            List<BookedTraveler> bookedTravelerConfirmedList = cartPNR.getTravelerInfo().getCollection()
                    .stream().filter(i -> i.isSelectedToCheckin() && i.isCovidRestrictionConfirmed() && !i.isCheckinStatus())
                    .collect(Collectors.toList());

            boolean formWasSent = false;

            for (BookedTraveler bookedTraveler : bookedTravelerConfirmedList) {
                CovidRestrictionForm covidRestrictionForm = new CovidRestrictionForm();
                covidRestrictionForm.setPnr(pnr);
                covidRestrictionForm.setCartId(cartPNR.getMeta().getCartId());
                covidRestrictionForm.setStrApplicationDate(now.toString());
                covidRestrictionForm.setApplicationDate(now);
                covidRestrictionForm.setFirstName(bookedTraveler.getFirstName());
                covidRestrictionForm.setLastName(bookedTraveler.getLastName());
                covidRestrictionForm.setFormConfirmed(bookedTraveler.isCovidRestrictionConfirmed());
                covidRestrictionForm.setPassengerId(bookedTraveler.getId());
                covidRestrictionForm.setPassengerNameRef(bookedTraveler.getNameRefNumber());
                covidRestrictionForm.setPaxType(bookedTraveler.getPaxType());

                List<String> originDestinationParts = new ArrayList<>();

                for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {
                    String part = bookedSegment.getSegment().getDepartureAirport() + "-" + bookedSegment.getSegment().getArrivalAirport();
                    originDestinationParts.add(part);
                }

                covidRestrictionForm.setOriginDestinationParts(originDestinationParts);

                DocumentEvent documentEvent = new DocumentEvent(covidRestrictionForm.toString(), null, null, Constants.COVID_RESTRICTION_FORM_COLL);

                int code = pnrLookupDao.saveDocument(
                        documentEvent.getDocument(), documentEvent.getCollection()
                );

                formWasSent = true;
            }

            if (formWasSent) {
                cartPNR.setCovidRestrictionFormAlreadySent(true);
            }

        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }

    }
}
