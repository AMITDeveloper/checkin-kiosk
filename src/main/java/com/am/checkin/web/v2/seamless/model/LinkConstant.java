package com.am.checkin.web.v2.seamless.model;

public class LinkConstant {
    public static final String PASSENGER_SELECTION = "passengerSelection";
    public static final String PASSENGER_INFORMATION = "passengerInformation";
    public static final String BOARDING_DOCUMENTS = "boardingDocuments";
}
