package com.am.checkin.web.v2.seamless.service;

import com.aeromexico.commons.model.CartPNR;
import com.am.checkin.web.v2.seamless.transformers.TransformFromDS;
import com.am.checkin.web.v2.seamless.transformers.TransformToDS;
import com.am.seamless.checkin.common.models.TravelParty;
import com.am.seamless.checkin.common.models.request.PassengerSelectionRq;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@ApplicationScoped
public class SeamlessPassengerSelectionService {

    private static final Logger LOG = LoggerFactory.getLogger(SeamlessPassengerSelectionService.class);

    @Inject
    private SeamlessCheckinProxyService seamlessCheckinProxyService;

    public TravelParty selectPassengers(CartPNR cartPNR) throws Exception {

        String travelPartyId = cartPNR.getMeta().getTravelPartyId();
        String transactionId = cartPNR.getMeta().getTransactionId();
        PassengerSelectionRq passengerSelectionRq = TransformToDS.getPassengerSelectionRq(cartPNR);
        TravelParty travelPartyUpdated = seamlessCheckinProxyService.selectPassengers(passengerSelectionRq, travelPartyId, transactionId);
        LOG.info("Travel Party: {}", travelPartyUpdated.toString());

        TransformFromDS.setNotifications(travelPartyUpdated, cartPNR);
        TransformFromDS.setMandates(travelPartyUpdated, cartPNR);
        TransformFromDS.setLinks(travelPartyUpdated, cartPNR);

        return travelPartyUpdated;

    }
}
