package com.am.checkin.web.v2.util;

import com.aeromexico.commons.model.*;
import com.aeromexico.commons.model.utils.CurrencyUtil;
import com.aeromexico.commons.web.types.ActionCodeType;
import com.aeromexico.commons.web.types.AncillaryGroupType;
import com.aeromexico.commons.web.types.AncillaryPostBookingType;
import com.aeromexico.commons.web.types.PosType;
import com.aeromexico.commons.web.util.TimeUtil;
import com.aeromexico.sharedservices.util.AncillaryUtil;
import com.am.checkin.web.util.AEUtil;
import com.am.checkin.web.util.PNRLookUpServiceUtil;
import com.google.gson.Gson;
import com.sabre.webservices.pnrbuilder.getreservation.*;
import com.sabre.webservices.pnrbuilder.updatereservation.ReservationUpdateItemType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class PassengerAncillaryUtil {

    private static final Logger LOG = LoggerFactory.getLogger(PassengerAncillaryUtil.class);

    /**
     * @param travelItineraryReadRS
     * @return
     */
//    public static List<TravelItineraryReadRS.TravelItinerary.ItineraryInfo.ReservationItems.Item.Ancillaries.AncillaryService> getAncillaryList(TravelItineraryReadRS travelItineraryReadRS) {
//
//        List<TravelItineraryReadRS.TravelItinerary.ItineraryInfo.ReservationItems.Item.Ancillaries.AncillaryService> ancillaryList = new ArrayList<>();
//
//        TravelItineraryReadRS.TravelItinerary travelItinerary = travelItineraryReadRS.getTravelItinerary();
//        if (travelItinerary != null) {
//            TravelItineraryReadRS.TravelItinerary.ItineraryInfo itineraryInfo = travelItinerary.getItineraryInfo();
//            if (itineraryInfo != null) {
//                TravelItineraryReadRS.TravelItinerary.ItineraryInfo.ReservationItems reservationItems = itineraryInfo.getReservationItems();
//                if (reservationItems != null) {
//                    List<TravelItineraryReadRS.TravelItinerary.ItineraryInfo.ReservationItems.Item> itemList = reservationItems.getItem();
//                    for (TravelItineraryReadRS.TravelItinerary.ItineraryInfo.ReservationItems.Item item : itemList) {
//                        TravelItineraryReadRS.TravelItinerary.ItineraryInfo.ReservationItems.Item.Ancillaries ancillaries = item.getAncillaries();
//                        if (ancillaries != null) {
//                            TravelItineraryReadRS.TravelItinerary.ItineraryInfo.ReservationItems.Item.Ancillaries.AncillaryService ancillaryDetails = ancillaries.getAncillaryService();
//                            if (ancillaryDetails != null) {
//                                ancillaryList.add(ancillaryDetails);
//                            }
//                        }
//                    }
//                }
//            }
//        }
//        return ancillaryList;
//    }

    /**
     * @param getReservationRS
     * @return
     */
    public static List<AncillaryServicesPNRB> getAncillaryList(GetReservationRS getReservationRS) {

        List<AncillaryServicesPNRB> ancillaryList = new ArrayList<>();

        ReservationPNRB reservationPNRB =  getReservationRS.getReservation();

        if (reservationPNRB != null) {
            for (PassengerPNRB passengers : reservationPNRB.getPassengerReservation().getPassengers().getPassenger()) {
                if(passengers.getAncillaryServices() != null) {
                    for (AncillaryServicesPNRB ancillaryServices : passengers.getAncillaryServices().getAncillaryService()) {
                        if (ancillaryServices != null) {
                            ancillaryList.add(ancillaryServices);
                        }
                    }
                }
            }
        }
        return ancillaryList;
    }/**
     * @param getReservationRS
     * @return
     */
    public static List<AncillaryServicesPNRB> getAncillaryListForPassengerAndLeg(GetReservationRS getReservationRS, BookedLeg bookedLeg, String passengerNameNumber) {

    	String legCode = bookedLeg.getSegments().getLegCode();

        List<AncillaryServicesPNRB> ancillariesListForThisPassenger = new ArrayList<>();

        ReservationPNRB reservationPNRB =  getReservationRS.getReservation();

        if (reservationPNRB != null) {
            for (PassengerPNRB passengers : reservationPNRB.getPassengerReservation().getPassengers().getPassenger()) {
                if (passengers.getAncillaryServices() != null) {
                    if(passengerNameNumber.equalsIgnoreCase(passengers.getNameId())) {
                        for (AncillaryServicesPNRB ancillaryServices : passengers.getAncillaryServices().getAncillaryService()) {
                            boolean banderaUno = (null == ancillaryServices.getSegment()
                            || PNRLookUpServiceUtil.getLegCode(ancillaryServices.getSegment()).equalsIgnoreCase(bookedLeg.getSegments().getLegCode())
                            || PNRLookUpServiceUtil.isForThisLeg(bookedLeg.getSegments().getCollection(), ancillaryServices.getSegment()));
                            boolean baderaTres = PNRLookUpServiceUtil.isForThisLegCarryOn(bookedLeg.getSegments().getCollection(), ancillaryServices);
                            if (ancillaryServices != null && banderaUno) {
                                        if(ancillaryServices.getRficSubcode().equalsIgnoreCase("0CZ")){
                                            if(baderaTres){
                                                ancillaryServices.setRficSubcode("0MJ");
                                                ancillariesListForThisPassenger.add(ancillaryServices);
                                            }
                                        }else if (ancillaryServices.getRficSubcode().equalsIgnoreCase("0MJ")){
                                            if(baderaTres){
                                                ancillariesListForThisPassenger.add(ancillaryServices);
                                            }
                                        }else{
                                            ancillariesListForThisPassenger.add(ancillaryServices);
                                        }
                            }
                        }
                    }
                }
            }
        }
        return ancillariesListForThisPassenger;
    }

    /**
     *
     * @param ancillariesListForThisPassenger
     * @param bookedTraveler
     * @param reservationUpdateItemTypeList
     * @param legCode
     * @return
     */
    public static BookedTravelerAncillaryCollection getAncillariesFromGetReservation(
            List<com.sabre.webservices.pnrbuilder.getreservation.AncillaryServicesPNRB> ancillariesListForThisPassenger,
            BookedTraveler bookedTraveler,
            List<ReservationUpdateItemType> reservationUpdateItemTypeList,
            String legCode
    ) {

        BookedTravelerAncillaryCollection bookedTravelerAncillaryCollection = new BookedTravelerAncillaryCollection();
        List<AbstractAncillary> bookedTravelerAncillaryList = new ArrayList<>();

        LOG.info("ancillariesListForThisPassenger: " + new Gson().toJson(ancillariesListForThisPassenger));

        for (AncillaryServicesPNRB ancillaryPassenger : ancillariesListForThisPassenger) {
            if (null != ancillaryPassenger
                    && !AncillaryGroupType.SA.toString().equalsIgnoreCase(ancillaryPassenger.getGroupCode())
                    && !AncillaryPostBookingType._0B5.getRficSubCode().equalsIgnoreCase(ancillaryPassenger.getRficSubcode())) {

                // PAID ANCILLARIES
                if (ActionCodeType.HK.toString().equalsIgnoreCase(ancillaryPassenger.getActionCode())
                        || ActionCodeType.HI.toString().equalsIgnoreCase(ancillaryPassenger.getActionCode())) {

                    AncillaryPostBookingType ancillaryType = AncillaryPostBookingType.getType(ancillaryPassenger.getRficSubcode());

                    if (null != ancillaryType && ("1".equalsIgnoreCase(ancillaryPassenger.getEMDType())
                            || !"Y".equalsIgnoreCase(ancillaryPassenger.getEMDConsummedAtIssuance()))) {

                        AncillaryPostBookingType newRficSubcode = AncillaryUtil.convertNewCodeToOldCode(ancillaryPassenger.getRficSubcode());

                        if (null != newRficSubcode) {

                            PaidTravelerAncillary paidTravelerAncillary = new PaidTravelerAncillary();
                            if (null != ancillaryPassenger.getSegment()) {
                                paidTravelerAncillary.setLegCode(
                                        PNRLookUpServiceUtil.getLegCode(ancillaryPassenger.getSegment())
                                );
                            } else if (null != ancillaryPassenger.getTravelPortions()
                                    && null != ancillaryPassenger.getTravelPortions().getTravelPortion()
                                    && !ancillaryPassenger.getTravelPortions().getTravelPortion().isEmpty()) {
                                paidTravelerAncillary.setLegCode(
                                        createLegCode(ancillaryPassenger.getTravelPortions().getTravelPortion().get(0))
                                );
                            } else {
                                paidTravelerAncillary.setLegCode(legCode);
                            }

                            paidTravelerAncillary.setType(newRficSubcode.getRficSubCode());
                            paidTravelerAncillary.setRficSubcode(newRficSubcode.getRficSubCode());

                            paidTravelerAncillary.setRealRficSubcode(ancillaryPassenger.getRficSubcode());
                            paidTravelerAncillary.setGroupCode(ancillaryPassenger.getGroupCode());

                            paidTravelerAncillary.setQuantity(
                                    Integer.parseInt(ancillaryPassenger.getNumberOfItems())
                            );

                            if (null != ancillaryPassenger.getEMDNumber()) {
                                paidTravelerAncillary.setEmd(ancillaryPassenger.getEMDNumber());
                            } else if (null != ancillaryPassenger.getSegment()
                                    && null != ancillaryPassenger.getSegment().getEMDNumber()) {
                                paidTravelerAncillary.setEmd(ancillaryPassenger.getSegment().getEMDNumber());
                            }

                            if (null != ancillaryPassenger.getEMDCoupon()) {
                                paidTravelerAncillary.setEmdCoupon(ancillaryPassenger.getEMDCoupon());
                            } else if (null != ancillaryPassenger.getSegment()
                                    && null != ancillaryPassenger.getSegment().getEMDCoupon()) {
                                paidTravelerAncillary.setEmdCoupon(ancillaryPassenger.getSegment().getEMDCoupon());
                            }

                            if (ActionCodeType.HK.toString().equalsIgnoreCase(ancillaryPassenger.getActionCode())) {
                                paidTravelerAncillary.setRedeemed(true);
                            } else if (ActionCodeType.HI.toString().equalsIgnoreCase(ancillaryPassenger.getActionCode())) {
                                paidTravelerAncillary.setRedeemed(false);
                            }

                            paidTravelerAncillary.setEmdType(ancillaryPassenger.getEMDType());
                            paidTravelerAncillary.setEmdConsummedAtIssuance(ancillaryPassenger.getEMDConsummedAtIssuance());

                            paidTravelerAncillary.setIsPartOfReservation(true);
                            paidTravelerAncillary.setAncillaryId(ancillaryPassenger.getId());
                            paidTravelerAncillary.setNameNumber(bookedTraveler.getNameRefNumber());
                            paidTravelerAncillary.setActionCode(ancillaryPassenger.getActionCode());
                            paidTravelerAncillary.setCommercialName(ancillaryPassenger.getCommercialName());

                            bookedTravelerAncillaryList.add(paidTravelerAncillary);
                        }
                    }
                } else {
                    //delete this AE
                    AEUtil.addDeleteItem(ancillaryPassenger.getId(), bookedTraveler, reservationUpdateItemTypeList);
                }
            }
        }

        bookedTravelerAncillaryCollection.setCollection(bookedTravelerAncillaryList);

        return bookedTravelerAncillaryCollection;
    }

    /**
     *
     * @param ancillariesListForThisPassenger
     * @param bookedTraveler
     * @param reservationUpdateSeatItemTypeList
     * @param legCode
     * @param currencyCode
     * @return
     */
    public static BookedTravelerAncillaryCollection getSeatAncillariesFromGetReservation(
            List<com.sabre.webservices.pnrbuilder.getreservation.AncillaryServicesPNRB> ancillariesListForThisPassenger,
            BookedTraveler bookedTraveler,
            List<ReservationUpdateItemType> reservationUpdateSeatItemTypeList,
            String legCode,
            String currencyCode
    ) {

        BookedTravelerAncillaryCollection bookedTravelerAncillaryCollection = new BookedTravelerAncillaryCollection();
        List<AbstractAncillary> bookedTravelerAncillaryList = new ArrayList<>();

        LOG.info("ancillariesListForThisPassenger: " + new Gson().toJson(ancillariesListForThisPassenger));

        for (AncillaryServicesPNRB ancillaryPassenger : ancillariesListForThisPassenger) {
            if (null != ancillaryPassenger
                    && AncillaryGroupType.SA.toString().equalsIgnoreCase(ancillaryPassenger.getGroupCode())) {
                // PAID ANCILLARIES

                if (ActionCodeType.HK.toString().equalsIgnoreCase(ancillaryPassenger.getActionCode())
                        || ActionCodeType.HI.toString().equalsIgnoreCase(ancillaryPassenger.getActionCode())) {

                    PaidTravelerAncillary paidTravelerAncillary = new PaidTravelerAncillary();
                    if (null != ancillaryPassenger.getSegment()) {
                        paidTravelerAncillary.setLegCode(
                                PNRLookUpServiceUtil.getLegCode(ancillaryPassenger.getSegment())
                        );

                        if (null != ancillaryPassenger.getSegment()
                                && ("SEAT ASSIGNMENT".equalsIgnoreCase(ancillaryPassenger.getCommercialName())
                                || "ASIENTO PAGADO PAIDSEAT".equalsIgnoreCase(ancillaryPassenger.getCommercialName())
                                || "ASIENTO PAGADO PAID SEAT".equalsIgnoreCase(ancillaryPassenger.getCommercialName())
                                || "SEAT SELECTION".equalsIgnoreCase(ancillaryPassenger.getCommercialName())
                                || AncillaryPostBookingType._0B5.getRficSubCode().equalsIgnoreCase(ancillaryPassenger.getRficSubcode()))) {

                            paidTravelerAncillary.setSegmentCodeAux(
                                    PNRLookUpServiceUtil.getSegmentCode(ancillaryPassenger.getSegment())
                            );
                            paidTravelerAncillary.setLinkedID(UUID.randomUUID().toString());
                        }
                    } else {
                        paidTravelerAncillary.setLegCode(legCode);
                    }
                    paidTravelerAncillary.setType(ancillaryPassenger.getRficSubcode());
                    paidTravelerAncillary.setGroupCode(ancillaryPassenger.getGroupCode());
                    // ancillary
                    paidTravelerAncillary.setQuantity(1);

                    if (null != ancillaryPassenger.getEMDNumber()) {
                        paidTravelerAncillary.setEmd(ancillaryPassenger.getEMDNumber());
                    } else if (null != ancillaryPassenger.getSegment()
                            && null != ancillaryPassenger.getSegment().getEMDNumber()) {
                        paidTravelerAncillary.setEmd(ancillaryPassenger.getSegment().getEMDNumber());
                    }

                    if (null != ancillaryPassenger.getEMDCoupon()) {
                        paidTravelerAncillary.setEmdCoupon(ancillaryPassenger.getEMDCoupon());
                    } else if (null != ancillaryPassenger.getSegment()
                            && null != ancillaryPassenger.getSegment().getEMDCoupon()) {
                        paidTravelerAncillary.setEmdCoupon(ancillaryPassenger.getSegment().getEMDCoupon());
                    }

                    paidTravelerAncillary.setEmdType(ancillaryPassenger.getEMDType());
                    paidTravelerAncillary.setEmdConsummedAtIssuance(ancillaryPassenger.getEMDConsummedAtIssuance());

                    paidTravelerAncillary.setIsPartOfReservation(true);
                    paidTravelerAncillary.setAncillaryId(ancillaryPassenger.getId());
                    paidTravelerAncillary.setNameNumber(bookedTraveler.getNameRefNumber());
                    paidTravelerAncillary.setActionCode(ancillaryPassenger.getActionCode());
                    paidTravelerAncillary.setCommercialName(ancillaryPassenger.getCommercialName());
                    paidTravelerAncillary.setSeatCode(ancillaryPassenger.getPdcSeat());
                    CurrencyValue currency = new CurrencyValue();
                    currency.setBase(ancillaryPassenger.getTotalOriginalBasePrice().getPrice());
                    Map<String, BigDecimal> taxDetails = new HashMap<>();
                    if (null != ancillaryPassenger.getTaxes()) {

                        for (AncillaryTaxPNRB tax : ancillaryPassenger.getTaxes().getTax()) {
                            String taxCode = tax.getTaxCode();
                            BigDecimal taxes = tax.getTaxAmount();

                            BigDecimal currentTaxTotal = taxDetails.get(taxCode);
                            if (null != currentTaxTotal) {
                                taxDetails.put(taxCode, currentTaxTotal.add(taxes));
                            } else {
                                taxDetails.put(taxCode, taxes);
                            }
                        }
                    }
                    currency.setTaxDetails(taxDetails);
                    currency.setCurrencyCode(ancillaryPassenger.getTotalOriginalBasePrice().getCurrency());
                    currency.setTotal(CurrencyUtil.getAmount(ancillaryPassenger.getTTLPrice().getPrice(), currency.getCurrencyCode()));
                    paidTravelerAncillary.setTotalPrice(currency);

                    bookedTravelerAncillaryList.add(paidTravelerAncillary);
                } else {
                    // UNPAID ANCILLARIES
                    Ancillary ancillary = new Ancillary();
                    ancillary.setCommercialName(ancillaryPassenger.getCommercialName());
                    ancillary.setEmdConsummedAtIssuance(ancillaryPassenger.getEMDConsummedAtIssuance());
                    ancillary.setEmdType(ancillaryPassenger.getEMDType());
                    ancillary.setEmdCoupon(ancillaryPassenger.getEMDCoupon());

                    if (null != ancillaryPassenger.getEMDCoupon()) {
                        ancillary.setEmdCoupon(ancillaryPassenger.getEMDCoupon());
                    } else if (null != ancillaryPassenger.getSegment()
                            && null != ancillaryPassenger.getSegment().getEMDCoupon()) {
                        ancillary.setEmdCoupon(ancillaryPassenger.getSegment().getEMDCoupon());
                    }

                    ancillary.setFeeApplicationIndicator(ancillaryPassenger.getFeeApplicationIndicator());
                    ancillary.setOwningCarrierCode(ancillaryPassenger.getOwningCarrierCode());
                    ancillary.setSegmentIndicator(ancillaryPassenger.getSegmentIndicator());
                    ancillary.setGroupCode(ancillaryPassenger.getGroupCode());
                    ancillary.setPoints(0);
                    ancillary.setRficCode(ancillaryPassenger.getRficCode());
                    ancillary.setRficSubcode(ancillaryPassenger.getRficSubcode());
                    ancillary.setRealRficSubcode(ancillaryPassenger.getRficSubcode());
                    ancillary.setSsrCode(ancillaryPassenger.getSSRCode());
                    ancillary.setType(ancillaryPassenger.getRficSubcode());
                    ancillary.setVendor(ancillaryPassenger.getVendor());
                    ancillary.setCommisionIndicator(ancillaryPassenger.getCommisionIndicator());
                    ancillary.setInterlineIndicator(ancillaryPassenger.getInterlineIndicator());
                    ancillary.setRefundIndicator(ancillaryPassenger.getRefundIndicator());
                    ancillary.setBookingIndicator(ancillaryPassenger.getBookingIndicator());

                    CurrencyValue currency = getCurrencyValueFromAncillaryPassenger(currencyCode, ancillaryPassenger);
                    ancillary.setCurrency(currency);

                    TravelerAncillary travelerAncillary = new TravelerAncillary();
                    if (null != ancillaryPassenger.getSegment()) {
                        travelerAncillary.setLegCode(
                                PNRLookUpServiceUtil.getLegCode(ancillaryPassenger.getSegment())
                        );

                        if (null != ancillaryPassenger.getSegment()
                                && ("SEAT ASSIGNMENT".equalsIgnoreCase(ancillaryPassenger.getCommercialName())
                                || "ASIENTO PAGADO PAIDSEAT".equalsIgnoreCase(ancillaryPassenger.getCommercialName())
                                || "ASIENTO PAGADO PAID SEAT".equalsIgnoreCase(ancillaryPassenger.getCommercialName())
                                || "SEAT SELECTION".equalsIgnoreCase(ancillaryPassenger.getCommercialName())
                                || AncillaryPostBookingType._0B5.getRficSubCode().equalsIgnoreCase(ancillaryPassenger.getRficSubcode()))) {

                            travelerAncillary.setSegmentCodeAux(
                                    PNRLookUpServiceUtil.getSegmentCode(ancillaryPassenger.getSegment())
                            );
                            travelerAncillary.setLinkedID(UUID.randomUUID().toString());
                        }
                    } else {
                        travelerAncillary.setLegCode(legCode);
                    }
                    travelerAncillary.setSeatCode(ancillaryPassenger.getPdcSeat());
                    travelerAncillary.setAncillary(ancillary);
                    travelerAncillary.setAncillaryId(ancillaryPassenger.getId());
                    travelerAncillary.setNameNumber(bookedTraveler.getNameRefNumber());
                    travelerAncillary.setQuantity(Integer.parseInt(ancillaryPassenger.getNumberOfItems()));
                    travelerAncillary.setIsPartOfReservation(true);
                    travelerAncillary.setActionCode(ancillaryPassenger.getActionCode());

                    bookedTravelerAncillaryList.add(travelerAncillary);

                    //for delete this AE later
                    AEUtil.addDeleteItem(ancillaryPassenger.getId(), bookedTraveler, reservationUpdateSeatItemTypeList);
                }
            }
        }

        bookedTravelerAncillaryCollection.setCollection(bookedTravelerAncillaryList);

        return bookedTravelerAncillaryCollection;
    }


    /**
     *
     * @param ancillariesListForThisPassenger
     * @param bookedTraveler
     * @param reservationUpdateItemTypeList
     * @param legCode
     * @return
     */
    public static BookedTravelerAncillaryCollection getAncillaries(
            List<AncillaryServicesPNRB> ancillariesListForThisPassenger,
            BookedTraveler bookedTraveler,
            List<ReservationUpdateItemType> reservationUpdateItemTypeList,
            String legCode,
            String pos
    ) {

        BookedTravelerAncillaryCollection bookedTravelerAncillaryCollection = new BookedTravelerAncillaryCollection();
        List<AbstractAncillary> bookedTravelerAncillaryList = new ArrayList<>();

        LOG.info("ancillariesListForThisPassenger: " + new Gson().toJson(ancillariesListForThisPassenger));

        for (AncillaryServicesPNRB ancillaryPassenger : ancillariesListForThisPassenger) {
            if (null != ancillaryPassenger
                    && !AncillaryGroupType.SA.toString().equalsIgnoreCase(ancillaryPassenger.getGroupCode())
                    && !AncillaryPostBookingType._0B5.getRficSubCode().equalsIgnoreCase(ancillaryPassenger.getRficSubcode())) {

                // PAID ANCILLARIES
                if (ActionCodeType.HK.toString().equalsIgnoreCase(ancillaryPassenger.getActionCode())
                        || ActionCodeType.HI.toString().equalsIgnoreCase(ancillaryPassenger.getActionCode())) {

                    AncillaryPostBookingType ancillaryType = AncillaryPostBookingType.getType(ancillaryPassenger.getRficSubcode());

                    if (null != ancillaryType && !"Y".equalsIgnoreCase(ancillaryPassenger.getEMDConsummedAtIssuance())) {

                        AncillaryPostBookingType newRficSubcode = AncillaryUtil.convertNewCodeToOldCode(ancillaryPassenger.getRficSubcode());

                        if (null != newRficSubcode) {

                            PaidTravelerAncillary paidTravelerAncillary = new PaidTravelerAncillary();
                            paidTravelerAncillary.setLegCode(legCode);
                            paidTravelerAncillary.setType(newRficSubcode.getRficSubCode());
                            paidTravelerAncillary.setRficSubcode(newRficSubcode.getRficSubCode());

                            paidTravelerAncillary.setRealRficSubcode(ancillaryPassenger.getRficSubcode());
                            paidTravelerAncillary.setGroupCode(ancillaryPassenger.getGroupCode());

                            paidTravelerAncillary.setQuantity( Integer.parseInt(ancillaryPassenger.getNumberOfItems()) );

                            if (null != ancillaryPassenger.getEMDNumber()) {
                                paidTravelerAncillary.setEmd(ancillaryPassenger.getEMDNumber());
                            } else if (null != ancillaryPassenger.getSegment()
                                    && null != ancillaryPassenger.getSegment().getEMDNumber()) {
                                paidTravelerAncillary.setEmd(ancillaryPassenger.getSegment().getEMDNumber());
                            }

                            if (null != ancillaryPassenger.getEMDCoupon()) {
                                paidTravelerAncillary.setEmdCoupon(ancillaryPassenger.getEMDCoupon());
                            } else if (null != ancillaryPassenger.getSegment()
                                    && null != ancillaryPassenger.getSegment().getEMDCoupon()) {
                                paidTravelerAncillary.setEmdCoupon(ancillaryPassenger.getSegment().getEMDCoupon());
                            }

                            if (ActionCodeType.HK.toString().equalsIgnoreCase(ancillaryPassenger.getActionCode())) {
                                paidTravelerAncillary.setRedeemed(true);
                            } else if (ActionCodeType.HI.toString().equalsIgnoreCase(ancillaryPassenger.getActionCode())) {
                                paidTravelerAncillary.setRedeemed(false);
                            }

                            paidTravelerAncillary.setEmdType(ancillaryPassenger.getEMDType());
                            paidTravelerAncillary.setEmdConsummedAtIssuance(ancillaryPassenger.getEMDConsummedAtIssuance());

                            paidTravelerAncillary.setIsPartOfReservation(true);
                            paidTravelerAncillary.setAncillaryId(ancillaryPassenger.getId());
                            paidTravelerAncillary.setNameNumber(bookedTraveler.getNameRefNumber());
                            paidTravelerAncillary.setActionCode(ancillaryPassenger.getActionCode());
                            paidTravelerAncillary.setCommercialName(ancillaryPassenger.getCommercialName());

                            if ( PosType.KIOSK.toString().equalsIgnoreCase(pos) ) {
	                            if ( ! isThisSecondLeg( bookedTravelerAncillaryList, paidTravelerAncillary ) ) {
	                            	bookedTravelerAncillaryList.add(paidTravelerAncillary);
	                            }
                            } else {
                            	bookedTravelerAncillaryList.add(paidTravelerAncillary);
                            }

                        }
                    }
                } else {
                    //delete this AE
                    AEUtil.addDeleteItem(ancillaryPassenger.getId(), bookedTraveler, reservationUpdateItemTypeList);
                }
            }
        }

        bookedTravelerAncillaryCollection.setCollection(bookedTravelerAncillaryList);

        return bookedTravelerAncillaryCollection;
    }

    /**
		"type": "0IA",
		"rficSubcode": "0IA",
		"groupCode": "BG",
		"commercialName": "1ST BAG UP TO 50LB 25KG",

     * @param bookedTravelerAncillaryList
     * @param paidTravelerAncillaryToAdd
     * @return
     */
    static private boolean isThisSecondLeg(List<AbstractAncillary> bookedTravelerAncillaryList, PaidTravelerAncillary paidTravelerAncillaryToAdd ) {

    	for ( AbstractAncillary abstractAncillary: bookedTravelerAncillaryList) {
    		PaidTravelerAncillary paidTravelerAncillary = (PaidTravelerAncillary)abstractAncillary;
    		if ( paidTravelerAncillary.getType().equals( paidTravelerAncillaryToAdd.getType() ) && 
    				paidTravelerAncillary.getRficSubcode().equals( paidTravelerAncillaryToAdd.getRficSubcode() ) &&
    				paidTravelerAncillary.getGroupCode().equals( paidTravelerAncillaryToAdd.getGroupCode() ) && 
    				paidTravelerAncillary.getCommercialName().equals( paidTravelerAncillaryToAdd.getCommercialName() )) {
    			return true;
    			
    		}
    	}

    	return false;
    }

    
    /**
     *
     * @param ancillariesListForThisPassenger
     * @param bookedTraveler
     * @param reservationUpdateSeatItemTypeList
     * @param legCode
     * @param currencyCode
     * @return
     */
    public static BookedTravelerAncillaryCollection getSeatAncillaries(
            List<AncillaryServicesPNRB> ancillariesListForThisPassenger,
            BookedTraveler bookedTraveler,
            List<ReservationUpdateItemType> reservationUpdateSeatItemTypeList,
            String legCode,
            String currencyCode
    ) {

        BookedTravelerAncillaryCollection bookedTravelerAncillaryCollection = new BookedTravelerAncillaryCollection();
        List<AbstractAncillary> bookedTravelerAncillaryList = new ArrayList<>();

        LOG.info("ancillariesListForThisPassenger: " + new Gson().toJson(ancillariesListForThisPassenger));

        for (AncillaryServicesPNRB ancillaryPassenger : ancillariesListForThisPassenger) {
            if (null != ancillaryPassenger
                    && AncillaryGroupType.SA.toString().equalsIgnoreCase(ancillaryPassenger.getGroupCode())) {
                // PAID ANCILLARIES

                if (ActionCodeType.HK.toString().equalsIgnoreCase(ancillaryPassenger.getActionCode())
                        || ActionCodeType.HI.toString().equalsIgnoreCase(ancillaryPassenger.getActionCode())) {

                    PaidTravelerAncillary paidTravelerAncillary = new PaidTravelerAncillary();
                    if (null != ancillaryPassenger.getSegment()) {
                        paidTravelerAncillary.setLegCode(
                                PNRLookUpServiceUtil.getLegCode(ancillaryPassenger.getSegment())
                        );

                        if (null != ancillaryPassenger.getSegment()
                                && ("SEAT ASSIGNMENT".equalsIgnoreCase(ancillaryPassenger.getCommercialName())
                                || "ASIENTO PAGADO PAIDSEAT".equalsIgnoreCase(ancillaryPassenger.getCommercialName())
                                || "ASIENTO PAGADO PAID SEAT".equalsIgnoreCase(ancillaryPassenger.getCommercialName())
                                || "SEAT SELECTION".equalsIgnoreCase(ancillaryPassenger.getCommercialName())
                                || AncillaryPostBookingType._0B5.getRficSubCode().equalsIgnoreCase(ancillaryPassenger.getRficSubcode()))) {

                            paidTravelerAncillary.setSegmentCodeAux(
                                    PNRLookUpServiceUtil.getSegmentCode(ancillaryPassenger.getSegment())
                            );
                            paidTravelerAncillary.setLinkedID(UUID.randomUUID().toString());
                        }
                    } else {
                        paidTravelerAncillary.setLegCode(legCode);
                    }
                    paidTravelerAncillary.setType(ancillaryPassenger.getRficSubcode());
                    paidTravelerAncillary.setGroupCode(ancillaryPassenger.getGroupCode());
                    // ancillary
                    paidTravelerAncillary.setQuantity(1);

                    if (null != ancillaryPassenger.getEMDNumber()) {
                        paidTravelerAncillary.setEmd(ancillaryPassenger.getEMDNumber());
                    } else if (null != ancillaryPassenger.getSegment()
                            && null != ancillaryPassenger.getSegment().getEMDNumber()) {
                        paidTravelerAncillary.setEmd(ancillaryPassenger.getSegment().getEMDNumber());
                    }

                    if (null != ancillaryPassenger.getEMDCoupon()) {
                        paidTravelerAncillary.setEmdCoupon(ancillaryPassenger.getEMDCoupon());
                    } else if (null != ancillaryPassenger.getSegment()
                            && null != ancillaryPassenger.getSegment().getEMDCoupon()) {
                        paidTravelerAncillary.setEmdCoupon(ancillaryPassenger.getSegment().getEMDCoupon());
                    }

                    paidTravelerAncillary.setEmdType(ancillaryPassenger.getEMDType());
                    paidTravelerAncillary.setEmdConsummedAtIssuance(ancillaryPassenger.getEMDConsummedAtIssuance());

                    paidTravelerAncillary.setIsPartOfReservation(true);
                    paidTravelerAncillary.setAncillaryId(ancillaryPassenger.getId());
                    paidTravelerAncillary.setNameNumber(bookedTraveler.getNameRefNumber());
                    paidTravelerAncillary.setActionCode(ancillaryPassenger.getActionCode());
                    paidTravelerAncillary.setCommercialName(ancillaryPassenger.getCommercialName());
                    paidTravelerAncillary.setSeatCode(ancillaryPassenger.getPdcSeat());
                    CurrencyValue currency = new CurrencyValue();
                    currency.setBase(ancillaryPassenger.getTotalOriginalBasePrice().getPrice());
                    Map<String, BigDecimal> taxDetails = new HashMap<>();
                    if (null != ancillaryPassenger.getTaxes()) {
                        for (AncillaryTaxPNRB tax : ancillaryPassenger.getTaxes().getTax()) {
                            String taxCode = tax.getTaxCode();
                            BigDecimal taxes = tax.getTaxAmount();

                            BigDecimal currentTaxTotal = taxDetails.get(taxCode);
                            if (null != currentTaxTotal) {
                                taxDetails.put(taxCode, currentTaxTotal.add(taxes));
                            } else {
                                taxDetails.put(taxCode, taxes);
                            }
                        }
                    }
                    currency.setTaxDetails(taxDetails);
                    currency.setCurrencyCode(ancillaryPassenger.getTotalOriginalBasePrice().getCurrency());
                    currency.setTotal(CurrencyUtil.getAmount(ancillaryPassenger.getTTLPrice().getPrice(), currency.getCurrencyCode()));
                    paidTravelerAncillary.setTotalPrice(currency);

                    bookedTravelerAncillaryList.add(paidTravelerAncillary);
                } else {
                    // UNPAID ANCILLARIES
                    Ancillary ancillary = new Ancillary();
                    ancillary.setCommercialName(ancillaryPassenger.getCommercialName());
                    ancillary.setEmdConsummedAtIssuance(ancillaryPassenger.getEMDConsummedAtIssuance());
                    ancillary.setEmdType(ancillaryPassenger.getEMDType());
                    ancillary.setEmdCoupon(ancillaryPassenger.getEMDCoupon());

                    if (null != ancillaryPassenger.getEMDCoupon()) {
                        ancillary.setEmdCoupon(ancillaryPassenger.getEMDCoupon());
                    } else if (null != ancillaryPassenger.getSegment()
                            && null != ancillaryPassenger.getSegment().getEMDCoupon()) {
                        ancillary.setEmdCoupon(ancillaryPassenger.getSegment().getEMDCoupon());
                    }

                    ancillary.setFeeApplicationIndicator(ancillaryPassenger.getFeeApplicationIndicator());
                    ancillary.setOwningCarrierCode(ancillaryPassenger.getOwningCarrierCode());
                    ancillary.setSegmentIndicator(ancillaryPassenger.getSegmentIndicator());
                    ancillary.setGroupCode(ancillaryPassenger.getGroupCode());
                    ancillary.setPoints(0);
                    ancillary.setRficCode(ancillaryPassenger.getRficCode());
                    ancillary.setRficSubcode(ancillaryPassenger.getRficSubcode());
                    ancillary.setRealRficSubcode(ancillaryPassenger.getRficSubcode());
                    ancillary.setSsrCode(ancillaryPassenger.getSSRCode());
                    ancillary.setType(ancillaryPassenger.getRficSubcode());
                    ancillary.setVendor(ancillaryPassenger.getVendor());
                    ancillary.setCommisionIndicator(ancillaryPassenger.getCommisionIndicator());
                    ancillary.setInterlineIndicator(ancillaryPassenger.getInterlineIndicator());
                    ancillary.setRefundIndicator(ancillaryPassenger.getRefundIndicator());
                    ancillary.setBookingIndicator(ancillaryPassenger.getBookingIndicator());

                    CurrencyValue currency = getCurrencyValueFromAncillaryPassenger(currencyCode, ancillaryPassenger);
                    ancillary.setCurrency(currency);

                    TravelerAncillary travelerAncillary = new TravelerAncillary();
                    if (null != ancillaryPassenger.getSegment()) {
                        travelerAncillary.setLegCode(
                                PNRLookUpServiceUtil.getLegCode(ancillaryPassenger.getSegment())
                        );

                        if (null != ancillaryPassenger.getSegment()
                                && ("SEAT ASSIGNMENT".equalsIgnoreCase(ancillaryPassenger.getCommercialName())
                                || "ASIENTO PAGADO PAIDSEAT".equalsIgnoreCase(ancillaryPassenger.getCommercialName())
                                || "ASIENTO PAGADO PAID SEAT".equalsIgnoreCase(ancillaryPassenger.getCommercialName())
                                || "SEAT SELECTION".equalsIgnoreCase(ancillaryPassenger.getCommercialName())
                                || AncillaryPostBookingType._0B5.getRficSubCode().equalsIgnoreCase(ancillaryPassenger.getRficSubcode()))) {

                            travelerAncillary.setSegmentCodeAux(
                                    PNRLookUpServiceUtil.getSegmentCode(ancillaryPassenger.getSegment())
                            );
                            travelerAncillary.setLinkedID(UUID.randomUUID().toString());
                        }
                    } else {
                        travelerAncillary.setLegCode(legCode);
                    }
                    travelerAncillary.setSeatCode(ancillaryPassenger.getPdcSeat());
                    travelerAncillary.setAncillary(ancillary);
                    travelerAncillary.setAncillaryId(ancillaryPassenger.getId());
                    travelerAncillary.setNameNumber(bookedTraveler.getNameRefNumber());
                    travelerAncillary.setQuantity(Integer.parseInt(ancillaryPassenger.getNumberOfItems()));
                    travelerAncillary.setIsPartOfReservation(true);
                    travelerAncillary.setActionCode(ancillaryPassenger.getActionCode());

                    bookedTravelerAncillaryList.add(travelerAncillary);

                    //for delete this AE later
                    AEUtil.addDeleteItem(ancillaryPassenger.getId(), bookedTraveler, reservationUpdateSeatItemTypeList);
                }
            }
        }

        bookedTravelerAncillaryCollection.setCollection(bookedTravelerAncillaryList);

        return bookedTravelerAncillaryCollection;
    }

    /**
     * @param currencyCode
     * @param ancillaryPassenger
     * @return
     */
//    private static CurrencyValue getCurrencyValueFromAncillaryPassenger(
//            String currencyCode,
//            TravelItineraryReadRS.TravelItinerary.ItineraryInfo.ReservationItems.Item.Ancillaries.AncillaryService ancillaryPassenger
//    ) {
//
//        CurrencyValue currency;
//        if (null == ancillaryPassenger.getTotalTTLPrice()
//                || null == ancillaryPassenger.getTotalTTLPrice().getCurrency()) {
//            currency = CurrencyUtil.getCurrencyValue(currencyCode);
//        } else {
//
//            currency = new CurrencyValue();
//            currency.setBase(ancillaryPassenger.getTotalOriginalBasePrice().getPrice());
//            if (null != ancillaryPassenger.getTotalTaxes()
//                    && null != ancillaryPassenger.getTotalTaxes().getTax()
//                    && !ancillaryPassenger.getTotalTaxes().getTax().isEmpty()) {
//
//                List<TravelItineraryReadRS.TravelItinerary.ItineraryInfo.ReservationItems.Item.Ancillaries.AncillaryService.TotalTaxes.Tax> taxes;
//                taxes = ancillaryPassenger.getTotalTaxes().getTax();
//                BigDecimal totalTaxes = BigDecimal.ZERO;
//                for (TravelItineraryReadRS.TravelItinerary.ItineraryInfo.ReservationItems.Item.Ancillaries.AncillaryService.TotalTaxes.Tax tax : taxes) {
//                    totalTaxes = totalTaxes.add(tax.getTaxAmount());
//
//                    currency.addTax(tax.getTaxCode(), tax.getTaxAmount());
//                }
//                currency.setTotalTax(totalTaxes);
//            } else {
//                currency.setTotalTax(BigDecimal.ZERO);
//                Map<String, BigDecimal> taxDetails = new HashMap<>();
//                currency.setTaxDetails(taxDetails);
//            }
//            currency.setTotal(ancillaryPassenger.getTotalTTLPrice().getPrice());
//            currency.setCurrencyCode(ancillaryPassenger.getTotalTTLPrice().getCurrency());
//        }
//        return currency;
//    }

    /**
     * @param currencyCode
     * @param ancillaryPassenger
     * @return
     */
    private static CurrencyValue getCurrencyValueFromAncillaryPassenger(
            String currencyCode,
            AncillaryServicesPNRB ancillaryPassenger
    ) {

        CurrencyValue currency;
        if (null == ancillaryPassenger.getTotalTTLPrice()
                || null == ancillaryPassenger.getTotalTTLPrice().getCurrency()) {
            currency = CurrencyUtil.getCurrencyValue(currencyCode);
        } else {

            currency = new CurrencyValue();
            currency.setBase(ancillaryPassenger.getTotalOriginalBasePrice().getPrice());
            if (null != ancillaryPassenger.getTotalTaxes()
                    && null != ancillaryPassenger.getTotalTaxes().getTax()
                    && !ancillaryPassenger.getTotalTaxes().getTax().isEmpty()) {

                List<AncillaryTaxPNRB> taxes;
                taxes = ancillaryPassenger.getTotalTaxes().getTax();
                BigDecimal totalTaxes = BigDecimal.ZERO;
                for (AncillaryTaxPNRB tax : taxes) {
                    totalTaxes = totalTaxes.add(tax.getTaxAmount());

                    currency.addTax(tax.getTaxCode(), tax.getTaxAmount());
                }
                currency.setTotalTax(totalTaxes);
            } else {
                currency.setTotalTax(BigDecimal.ZERO);
                Map<String, BigDecimal> taxDetails = new HashMap<>();
                currency.setTaxDetails(taxDetails);
            }
            currency.setTotal(ancillaryPassenger.getTotalTTLPrice().getPrice());
            currency.setCurrencyCode(ancillaryPassenger.getTotalTTLPrice().getCurrency());
        }
        return currency;
    }

//    private static String createLegCode(
//            TravelItineraryReadRS.TravelItinerary.ItineraryInfo.ReservationItems.Item.Ancillaries.AncillaryService.TravelPortions.TravelPortion segment
//    ) {
//        if (segment.getAirlineCode() == null) {
//            return null;
//        }
//        return segment.getBoardPoint()
//                + "_"
//                + segment.getAirlineCode()
//                + "_"
//                + segment.getFlightNumber()
//                + "_"
//                + TimeUtil.getTime(segment.getDepartureDate(), "yyyy-MM-dd");
//    }

    private static String createLegCode(
            com.sabre.webservices.pnrbuilder.getreservation.SegmentOrTravelPortionType segment
    ) {
        if (segment.getAirlineCode() == null) {
            return null;
        }
        return segment.getBoardPoint()
                + "_"
                + segment.getAirlineCode()
                + "_"
                + segment.getFlightNumber()
                + "_"
                + TimeUtil.getTime(segment.getDepartureDate(), "yyyy-MM-dd");
    }
}
