package com.am.checkin.web.v2.seamless.service;

import com.am.seamless.checkin.common.models.request.BoardingDocumentRq;
import com.am.seamless.checkin.common.models.request.PassengerInfoRq;
import com.am.seamless.checkin.common.models.request.PassengerSelectionRq;
import com.am.seamless.checkin.common.models.request.TravelPartyRq;
import com.am.seamless.checkin.common.models.request.UpdateMandatesRq;


import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/travelparties")
public interface SeamlessCheckinServicesInterface {

    @POST
    @Path("")
    @Consumes({ MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response getTravelParty(
            TravelPartyRq travelPartyRq,
            @QueryParam("operatingHandler") String operatingHandler
    ) throws Exception;

    @POST
    @Path("/{travelpartyid}/passengerSelection")
    @Consumes({ MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response selectPassengers(
            PassengerSelectionRq passengerSelectionRq,
            @PathParam("travelpartyid") String travelpartyid,
            @HeaderParam("transactionid") String transactionId
    ) throws Exception;

    @POST
    @Path("/{travelpartyid}/passengerInfo")
    @Consumes({ MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response updatePassengerInfo(
            PassengerInfoRq passengerInfoRq,
            @PathParam("travelpartyid") String travelpartyid,
            @HeaderParam("transactionid") String transactionId
    ) throws Exception;

    @POST
    @Path("/{travelpartyid}/mandates")
    @Consumes({ MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response updateMandates(
            UpdateMandatesRq updateMandatesRq,
            @PathParam("travelpartyid") String travelPartyId,
            @HeaderParam("transactionid") String transactionId
    ) throws Exception;

    @POST
    @Path("/{travelpartyid}/boardingDocuments")
    @Consumes({ MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response boardingDocuments(
            BoardingDocumentRq boardingDocumentRq,
            @QueryParam("issuingCarrier") String issuingCarrier,
            @PathParam("travelpartyid") String travelPartyId,
            @HeaderParam("transactionid") String transactionId
    ) throws Exception;

}
