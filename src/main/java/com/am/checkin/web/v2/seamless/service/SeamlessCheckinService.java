package com.am.checkin.web.v2.seamless.service;

import com.aeromexico.codes.ICodesGenerator;
import com.aeromexico.codes.impl.CodesGenerator;
import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.CheckinConfirmation;
import com.aeromexico.commons.model.Mandate;
import com.aeromexico.commons.model.MandatesCollection;
import com.aeromexico.commons.model.PurchaseOrder;
import com.aeromexico.commons.model.SegmentDocument;
import com.aeromexico.commons.model.rq.PurchaseOrderRQ;
import com.aeromexico.commons.web.types.DigitalSignatureStatusType;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.util.TimeUtil;
import com.am.checkin.web.v2.model.DigitalSignatureRq;
import com.am.checkin.web.v2.model.DigitalSignatureRqList;
import com.am.checkin.web.v2.model.DigitalSignatureRs;
import com.am.checkin.web.v2.seamless.model.LinkConstant;
import com.am.checkin.web.v2.seamless.transformers.TransformFromDS;
import com.am.checkin.web.v2.seamless.transformers.TransformToDS;
import com.am.checkin.web.v2.seamless.transformers.TransformerUtil;
import com.am.checkin.web.v2.services.CountryValidationUtilService;
import com.am.checkin.web.v2.services.DigitalSignatureService;
import com.am.checkin.web.v2.util.SystemVariablesUtil;
import com.am.checkin.web.v2.util.TravelPartyErrorUtil;
import com.am.seamless.checkin.common.models.Passenger;
import com.am.seamless.checkin.common.models.TravelParty;
import com.am.seamless.checkin.common.models.exception.SeamlessException;
import com.am.seamless.checkin.common.models.request.BoardingDocumentRq;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Named
@ApplicationScoped
public class SeamlessCheckinService {

    private static final Logger LOG = LoggerFactory.getLogger(SeamlessCheckinService.class);

    private static final ICodesGenerator CODES_GENERATOR = new CodesGenerator();

    @Inject
    private SeamlessCheckinProxyService seamlessCheckinProxyService;

    @Inject
    private DigitalSignatureService digitalSignatureService;

    @Inject
    private CountryValidationUtilService countryValidationUtilService;

    public void reprint(CartPNR cartPNR, BookedLeg bookedLeg, boolean tsaRequired, String pos) throws Exception {
        BoardingDocumentRq boardingDocumentRq = TransformToDS.getBoardingDocumentRq(cartPNR, bookedLeg);

        String travelPartyId = cartPNR.getMeta().getTravelPartyId();
        String transactionId = cartPNR.getMeta().getTransactionId();
        String issuingCarrier = "AM";

        TravelParty travelPartyUpdated = seamlessCheckinProxyService.boardingDocuments(boardingDocumentRq, issuingCarrier, travelPartyId, transactionId);
        LOG.info("Travel Party: {}", travelPartyUpdated.toString());

        updateAfterSuccessCheckin(cartPNR, bookedLeg, travelPartyUpdated, tsaRequired, pos);

        signDocuments(cartPNR, bookedLeg);
    }

    public CheckinConfirmation doCheckin(PurchaseOrderRQ purchaseOrderRQ, CartPNR cartPNR, BookedLeg bookedLeg, boolean tsaRequired) throws Exception {

        if (SystemVariablesUtil.isValidateSeamlessBoardingDocumentsLinkOnFirstAttemptEnabled()    //validate or not the link
                && null == cartPNR.getLinks().get(LinkConstant.BOARDING_DOCUMENTS)) {

            throw new SeamlessException(
                    TravelPartyErrorUtil.getSeamlessError(
                            com.am.seamless.checkin.common.models.types.ErrorType.NO_BOARDINGPASS_LINK_CHECKIN_NOT_ALLOWED,
                            null
                    ).toString(),
                    500
            );
        }

        PurchaseOrder purchaseOrder = purchaseOrderRQ.getPurchaseOrder();

        //validate acceptance of mandates
        if (SystemVariablesUtil.isValidateSeamlessCheckinMandatesEnabled()) {
            validateMandates(cartPNR, purchaseOrder);
        } else { //override mandates
            purchaseOrder.setMandates(new MandatesCollection());
            for (Mandate mandate : cartPNR.getMandates().getCollection()) {
                Mandate man = new Mandate();
                man.setConfirmed(true);
                man.setId(mandate.getId());
                purchaseOrder.getMandates().getCollection().add(man);
            }
        }

        BoardingDocumentRq boardingDocumentRq = TransformToDS.getBoardingDocumentRq(cartPNR, bookedLeg, purchaseOrder);

        String travelPartyId = cartPNR.getMeta().getTravelPartyId();
        String transactionId = cartPNR.getMeta().getTransactionId();
        String issuingCarrier = "AM";

        TravelParty travelPartyUpdated = seamlessCheckinProxyService.boardingDocuments(boardingDocumentRq, issuingCarrier, travelPartyId, transactionId);
        LOG.info("Travel Party: {}", travelPartyUpdated.toString());

        updateAfterSuccessCheckin(cartPNR, bookedLeg, travelPartyUpdated, tsaRequired, purchaseOrderRQ.getPos());

        signDocuments(cartPNR, bookedLeg);

        CheckinConfirmation checkinConfirmation = getCheckinConfirmation(cartPNR, bookedLeg, purchaseOrderRQ);

        return checkinConfirmation;
    }

    public void signDocuments(CartPNR cartPNR, BookedLeg bookedLeg) {

        DigitalSignatureRqList digitalSignatureRqList = new DigitalSignatureRqList();

        //check for digital signature requirement, add to request
        for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
            if (null != bookedTraveler.getSegmentDocumentsList()
                    && !bookedTraveler.getSegmentDocumentsList().isEmpty()) {
                for (SegmentDocument segmentDocument : bookedTraveler.getSegmentDocumentsList()) {

                    BookedSegment bookedSegment = bookedLeg.getBookedSegment(segmentDocument.getSegmentCode());

                    if (null != bookedSegment) {
                        if (SystemVariablesUtil.isSignAllSeamlessCheckinBoardingPassesEnabled()
                                || countryValidationUtilService.isUsaOrigin(bookedSegment)) {
                            DigitalSignatureRq digitalSignatureRq = new DigitalSignatureRq();
                            digitalSignatureRq.setId(segmentDocument.getId());
                            digitalSignatureRq.setBarcodeData(segmentDocument.getBarcode());

                            digitalSignatureRqList.add(digitalSignatureRq);
                        } else {
                            segmentDocument.setDigitalSignatureStatus(DigitalSignatureStatusType.NOT_REQUIRED);
                        }
                    }
                }
            }

            if (null != bookedTraveler.getInfant()) {
                if (null != bookedTraveler.getInfant().getSegmentDocumentsList()
                        && !bookedTraveler.getInfant().getSegmentDocumentsList().isEmpty()) {
                    for (SegmentDocument segmentDocument : bookedTraveler.getInfant().getSegmentDocumentsList()) {

                        BookedSegment bookedSegment = bookedLeg.getBookedSegment(segmentDocument.getSegmentCode());

                        if (null != bookedSegment) {
                            if (SystemVariablesUtil.isSignAllSeamlessCheckinBoardingPassesEnabled()
                                    || countryValidationUtilService.isUsaOrigin(bookedSegment)) {

                                DigitalSignatureRq digitalSignatureRq = new DigitalSignatureRq();
                                digitalSignatureRq.setId(segmentDocument.getId());
                                digitalSignatureRq.setBarcodeData(segmentDocument.getBarcode());

                                digitalSignatureRqList.add(digitalSignatureRq);
                            } else {
                                segmentDocument.setDigitalSignatureStatus(DigitalSignatureStatusType.NOT_REQUIRED);
                            }
                        }
                    }
                }
            }
        }

        //call digital signature asynchronously
        LOG.info("Start digital signature call");
        List<DigitalSignatureRs> digitalSignatureRsList = digitalSignatureService.callDigitalSignatureAsync(digitalSignatureRqList);
        LOG.info("End digital signature call");

        //convert responses to map to easy set
        //key = id, value = DigitalSignatureRs
        Map<String, DigitalSignatureRs> digitalSignatureRsMap = digitalSignatureRsList.stream()
                .filter(r -> r != null)
                .collect(
                        Collectors.toMap(r -> r.getId(), r -> r)
                );

        //map responses
        for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
            if (null != bookedTraveler.getSegmentDocumentsList()
                    && !bookedTraveler.getSegmentDocumentsList().isEmpty()) {
                for (SegmentDocument segmentDocument : bookedTraveler.getSegmentDocumentsList()) {
                    setDigitalSignature(segmentDocument, digitalSignatureRsMap);
                    setImages(segmentDocument);
                }
            }

            if (null != bookedTraveler.getInfant()) {
                if (null != bookedTraveler.getInfant().getSegmentDocumentsList()
                        && !bookedTraveler.getInfant().getSegmentDocumentsList().isEmpty()) {
                    for (SegmentDocument segmentDocument : bookedTraveler.getInfant().getSegmentDocumentsList()) {
                        setDigitalSignature(segmentDocument, digitalSignatureRsMap);
                        setImages(segmentDocument);
                    }
                }
            }
        }
    }

    public void setImages(SegmentDocument segmentDocument) {
        segmentDocument.setBarcodeImage(
                CODES_GENERATOR.codesGenerator2D(segmentDocument.getBarcode())
        );

        segmentDocument.setQrcodeImage(
                CODES_GENERATOR.codesGeneratorQR(segmentDocument.getBarcode())
        );
    }

    public void setDigitalSignature(SegmentDocument segmentDocument, Map<String, DigitalSignatureRs> digitalSignatureRsMap) {
        if (SystemVariablesUtil.isSignAllSeamlessCheckinBoardingPassesEnabled()
                || !DigitalSignatureStatusType.NOT_REQUIRED.equals(segmentDocument.getDigitalSignatureStatus())) {

            DigitalSignatureRs digitalSignatureRs = digitalSignatureRsMap.get(segmentDocument.getId());

            if (null != digitalSignatureRs && digitalSignatureRs.isSuccess()) {
                segmentDocument.setBarcode(digitalSignatureRs.getData());
                segmentDocument.setDigitalSignatureStatus(DigitalSignatureStatusType.OK);
            } else if (!DigitalSignatureStatusType.NOT_REQUIRED.equals(segmentDocument.getDigitalSignatureStatus())) {
                segmentDocument.setDigitalSignatureStatus(DigitalSignatureStatusType.MISSING);
            }
        }
    }

    public static void updateAfterSuccessCheckin(
            CartPNR cartPNR,
            BookedLeg bookedLeg,
            TravelParty travelPartyUpdated,
            boolean tsaRequired,
            String pos
    ) {

        TransformFromDS.setNotifications(travelPartyUpdated, cartPNR);
        TransformFromDS.setMandates(travelPartyUpdated, cartPNR);
        TransformFromDS.setLinks(travelPartyUpdated, cartPNR);

        for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
            Optional<Passenger> optAdt = TransformerUtil.getPassenger(bookedTraveler, travelPartyUpdated);

            Passenger infant = null;
            Passenger passenger = null;

            if (optAdt.isPresent()) {
                passenger = optAdt.get();
            }

            if (null != bookedTraveler.getInfant()) {
                if (null != passenger.getInfant()) {
                    infant = passenger.getInfant();
                } else {
                    Optional<Passenger> optInf = TransformerUtil.getPassenger(bookedTraveler.getInfant(), travelPartyUpdated);

                    if (optInf.isPresent()) {
                        infant = optInf.get();
                    }
                }
            }

            if (null != infant) {
                TransformFromDS.setCheckInStatus(bookedTraveler.getInfant(), bookedLeg, infant, travelPartyUpdated.getTrips());

                TransformFromDS.setBoardingDocumentsForInfant(
                        bookedTraveler,
                        bookedLeg,
                        infant,
                        travelPartyUpdated.getTrips(),
                        cartPNR.getWarnings(),
                        tsaRequired,
                        cartPNR.getMeta().getCartId(),
                        pos
                );
            }

            if (null != passenger) {
                TransformFromDS.setCheckInStatus(bookedTraveler, bookedLeg, passenger, travelPartyUpdated.getTrips());
                TransformFromDS.mapSeats(bookedTraveler, passenger, bookedLeg, travelPartyUpdated.getTrips());

                TransformFromDS.setBoardingDocuments(
                        bookedTraveler,
                        bookedLeg,
                        passenger,
                        travelPartyUpdated.getTrips(),
                        cartPNR.getWarnings(),
                        tsaRequired,
                        cartPNR.getMeta().getCartId(),
                        pos
                );
            }
        }
    }

    public static CheckinConfirmation getCheckinConfirmation(CartPNR cartPNR, BookedLeg bookedLeg, PurchaseOrderRQ purchaseOrderRQ) {
        CheckinConfirmation checkinConfirmation = new CheckinConfirmation();
        checkinConfirmation.setTransactionId("");
        checkinConfirmation.setItineraryPdf("");
        checkinConfirmation.setPosCode(purchaseOrderRQ.getPos());
        checkinConfirmation.setBoardingPassId(purchaseOrderRQ.getRecordLocator());

        checkinConfirmation.getCheckedInLegs().getCollection().add(bookedLeg);
        checkinConfirmation.getCheckedInCarts().getCollection().add(cartPNR.getCartPNRConfirmation());

        checkinConfirmation.setTransactionTime(TimeUtil.getStrTime(purchaseOrderRQ.getTransactionTime()));

        checkinConfirmation.setLegCode(cartPNR.getLegCode());

        return checkinConfirmation;
    }

    public static void validateMandates(CartPNR cartPNR, PurchaseOrder purchaseOrder) throws Exception {

        //validate confirmed mandates
        if (null != purchaseOrder.getMandates() && null != purchaseOrder.getMandates().getCollection() && !purchaseOrder.getMandates().getCollection().isEmpty()) {
            for (Mandate mandate : cartPNR.getMandates().getCollection()) {
                Mandate updatedMandate = TransformerUtil.getMandate(mandate.getId(), purchaseOrder.getMandates().getCollection());

                if (null == updatedMandate || !updatedMandate.isConfirmed()) {
                    GenericException ex = new GenericException(Response.Status.BAD_REQUEST, ErrorType.ACCEPT_MANDATES, "Mandate " + mandate.getId() + " need to be accepted");
                    throw ex;
                }
            }
        } else {
            for (Mandate mandate : cartPNR.getMandates().getCollection()) {
                if (!mandate.isConfirmed()) {
                    GenericException ex = new GenericException(Response.Status.BAD_REQUEST, ErrorType.ACCEPT_MANDATES, "Mandate " + mandate.getId() + " need to be accepted");
                    throw ex;
                }
            }
        }

    }
}
