package com.am.checkin.web.v2.services;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.sabre.api.models.dcci.PassengerDetailsResponse;
import com.aeromexico.sabre.api.restclient.DigitalSignatureSabreClient;
import com.am.checkin.web.v2.model.DigitalSignatureRq;
import com.am.checkin.web.v2.model.DigitalSignatureRqList;
import com.am.checkin.web.v2.model.DigitalSignatureRs;
import com.am.checkin.web.v2.model.DigitalSignatureRsList;
import com.am.checkin.web.v2.seamless.service.DigitalSignatureProxyService;
import com.google.gson.Gson;
import com.sabre.services.acs.bso.flightdetail.v3.ACSFlightDetailRSACS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;



@Named
@ApplicationScoped
public class DigitalSignatureService {
    private static final Logger LOG = LoggerFactory.getLogger(DigitalSignatureService.class);

    /**
     * Rest Client for Sabre services
     */
    @Inject
    private DigitalSignatureSabreClient digitalSignatureSabreClient;

    /**
     * Rest Client for digital signature provider
     */
    @Inject
    private DigitalSignatureProxyService digitalSignatureProxyService;

    public DigitalSignatureRs signBoardingPassSync(DigitalSignatureRq digitalSignatureRq) throws Exception {
        DigitalSignatureRs digitalSignatureRs = digitalSignatureProxyService.signBoardingpass(digitalSignatureRq);

        return digitalSignatureRs;
    }

    public CompletableFuture<DigitalSignatureRs> signBoardingPassAsync(DigitalSignatureRq digitalSignatureRq) {
        CompletableFuture<DigitalSignatureRs> future = CompletableFuture.supplyAsync(() -> {
            try {

                final DigitalSignatureRs digitalSignatureRs = digitalSignatureProxyService.signBoardingpass(digitalSignatureRq);

                return digitalSignatureRs;
            } catch (Exception ex) {
                throw new IllegalStateException(ex);
            }
        }).handle((res, ex) -> { //using generic handle
            if (ex != null) {
                LOG.error(ex.getMessage(), ex);
                return null;
            }

            return res;
        });

        return future;
    }

    public List<DigitalSignatureRs> callDigitalSignatureAsync(DigitalSignatureRqList digitalSignatureRqList) {

        List<CompletableFuture<DigitalSignatureRs>> futures = digitalSignatureRqList.stream()
                .map(digitalSignatureRq -> signBoardingPassAsync(digitalSignatureRq))
                .collect(Collectors.toList());

        List<DigitalSignatureRs> digitalSignatureRsList = futures.stream()
                .map(CompletableFuture::join)
                .collect(Collectors.toList());

        //remove null responses
        boolean removed = digitalSignatureRsList.removeAll(Collections.singleton(null));

        if (removed) {
            // Second try
        }

        return digitalSignatureRsList;
    }



    public PassengerDetailsResponse callDigitalSignatureSabre(String pnr, String store) throws Exception {
        int retryAttempt = 1;
        int maxRetries = 2;
        boolean valid = false;
        PassengerDetailsResponse lookupResponse = null;

        while (!valid) {
            try {
                String sabreResponse = digitalSignatureSabreClient.doGet(pnr, store);
                if (null == sabreResponse) {
                    throw new Exception("Error digital signature get null");
                }
                valid = true;
                LOG.info("Sabre Digital Signature response: {}", sabreResponse);
                lookupResponse = new Gson().fromJson(sabreResponse, PassengerDetailsResponse.class);
                LOG.info("PnrLookupResponse: {}", lookupResponse);

            } catch (Exception ex) {
                LOG.error("Error en la llamada: {} a digitalSignature. Total de intentos {}", retryAttempt, maxRetries);
                LOG.error(ex.getMessage(), ex);
                if (maxRetries > retryAttempt) {
                    retryAttempt++;
                } else {
                    LOG.error("Error getting digital signature from internal call");
                    throw new GenericException(Response.Status.NOT_FOUND, ErrorType.DIGITAL_SIGNATURE_REQUIRED);
                }
            }
        }
        return lookupResponse;
    }

    public PassengerDetailsResponse callDigitalSignatureSabreErrorForTesting(
            String pnr, String store
    ) throws Exception {
        LOG.error("Error getting digital signature from internal call");
        throw new GenericException(Response.Status.NOT_FOUND, ErrorType.DIGITAL_SIGNATURE_REQUIRED);
    }

    public DigitalSignatureSabreClient getDigitalSignatureSabreClient() {
        return digitalSignatureSabreClient;
    }

    public void setDigitalSignatureSabreClient(DigitalSignatureSabreClient digitalSignatureSabreClient) {
        this.digitalSignatureSabreClient = digitalSignatureSabreClient;
    }

}
