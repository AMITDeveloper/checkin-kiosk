package com.am.checkin.web.v2.services;

import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.BookingClass;
import com.aeromexico.commons.model.PriorityBySegment;
import com.am.checkin.web.v2.config.DroolsConfig;
import com.am.checkin.web.v2.model.UpgradeKnowledge;
import org.drools.core.base.RuleNameStartsWithAgendaFilter;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Named
@ApplicationScoped
public class UpgradeEligibilityRuleService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpgradeEligibilityRuleService.class);

    private KieContainer container;

    @PostConstruct
    public void init() {
        container = DroolsConfig.getInstance();
    }

    public void evaluateRules(Map<String, UpgradeKnowledge> upgradeKnowledgeList) {
        KieSession kieSession = container.newKieSession();

        for (Map.Entry<String, UpgradeKnowledge> stringUpgradeKnowledgeEntry : upgradeKnowledgeList.entrySet()) {
            kieSession.insert(stringUpgradeKnowledgeEntry.getValue());
        }

        kieSession.fireAllRules(new RuleNameStartsWithAgendaFilter("UPGRADE-ELIGIBILITY"));
        kieSession.dispose();
    }

    public static Map<String, UpgradeKnowledge> getFacts(
            List<BookedTraveler> bookedTravelerList,
            BookedSegment bookedSegment
    ) {
        Map<String, UpgradeKnowledge> upgradeKnowledgeMap = new HashMap<>();

        for (BookedTraveler bookedTraveler : bookedTravelerList) {
            if (bookedTraveler.isCandidateForUpgradeList()) {

                UpgradeKnowledge upgradeKnowledge = new UpgradeKnowledge();

                upgradeKnowledge.setNameRef(bookedTraveler.getNameRefNumber());

                BookingClass bookingClass = bookedTraveler.getItineraryBookingClasses().getBookingClass(
                        bookedSegment.getSegment().getSegmentCode()
                );

                if (null != bookingClass) {
                    upgradeKnowledge.setPassengerId(bookingClass.getPassengerId());
                    upgradeKnowledge.getFact().setClassOfService(bookingClass.getBookingClass());
                }

                upgradeKnowledge.getFact().setTierLevel(bookedTraveler.getTierLevel());
                upgradeKnowledge.getFact().setPriorityCode(bookedTraveler.getPriorityCode());

                if (null != bookedSegment) {
                    LOGGER.info("itinerary: " + bookedSegment.toString());
                    upgradeKnowledge.getFact().setOrigin(bookedSegment.getSegment().getDepartureAirport());
                    upgradeKnowledge.getFact().setDestination(bookedSegment.getSegment().getArrivalAirport());
                }

                upgradeKnowledgeMap.put(bookedTraveler.getNameRefNumber(), upgradeKnowledge);
            }
        }

        return upgradeKnowledgeMap;
    }
}
