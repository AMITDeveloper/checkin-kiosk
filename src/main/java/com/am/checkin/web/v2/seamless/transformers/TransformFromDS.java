package com.am.checkin.web.v2.seamless.transformers;

import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.CheckInStatus;
import com.aeromexico.commons.model.DestinationAddress;
import com.aeromexico.commons.model.FrequentFlyer;
import com.aeromexico.commons.model.HateoasLink;
import com.aeromexico.commons.model.Infant;
import com.aeromexico.commons.model.Mandate;
import com.aeromexico.commons.model.Notification;
import com.aeromexico.commons.model.SeatChoice;
import com.aeromexico.commons.model.SegmentChoice;
import com.aeromexico.commons.model.SegmentDocument;
import com.aeromexico.commons.model.SegmentStatus;
import com.aeromexico.commons.model.TravelerDocument;
import com.aeromexico.commons.model.VisaInfo;
import com.aeromexico.commons.model.Warning;
import com.aeromexico.commons.model.WarningCollection;
import com.aeromexico.commons.web.types.CheckinIneligibleReasons;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.types.PosType;
import com.aeromexico.commons.web.util.SegmentCodeUtil;
import com.aeromexico.commons.web.util.ValidateCheckinIneligibleReason;
import com.am.checkin.web.util.PnrCollectionUtil;
import com.am.checkin.web.v2.model.PassengerListUpdate;
import com.am.checkin.web.v2.seamless.model.LinkConstant;
import com.am.checkin.web.v2.util.BoardingPassesUtil;
import com.am.seamless.checkin.common.models.AdvancePassengerInformationRequirement;
import com.am.seamless.checkin.common.models.BoardingDocument;
import com.am.seamless.checkin.common.models.DatedOperatingLeg;
import com.am.seamless.checkin.common.models.DatedOperatingSegment;
import com.am.seamless.checkin.common.models.DocumentCombinationChoice;
import com.am.seamless.checkin.common.models.IdentityDocument;
import com.am.seamless.checkin.common.models.Link;
import com.am.seamless.checkin.common.models.LoyaltyProgramAccount;
import com.am.seamless.checkin.common.models.Passenger;
import com.am.seamless.checkin.common.models.PassengerLeg;
import com.am.seamless.checkin.common.models.PassengerSegment;
import com.am.seamless.checkin.common.models.PostalAddress;
import com.am.seamless.checkin.common.models.SeatAssignment;
import com.am.seamless.checkin.common.models.SupplementaryDocument;
import com.am.seamless.checkin.common.models.TravelParty;
import com.am.seamless.checkin.common.models.Trip;
import com.am.seamless.checkin.common.models.types.AdvancePassengerInformationRequirementCodeType;
import com.am.seamless.checkin.common.models.types.CheckinStatus;
import com.am.seamless.checkin.common.utilities.UuidUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class TransformFromDS {

    private static final Logger LOG = LoggerFactory.getLogger(TransformFromDS.class);

    public static void setLoyalty(BookedTraveler bookedTraveler, Passenger passenger) {

        if (null == bookedTraveler.getLoyaltyNumbers() || bookedTraveler.getLoyaltyNumbers().isEmpty()) {

            if (null != passenger.getLoyaltyPrograms() && !passenger.getLoyaltyPrograms().isEmpty()) {
                for (LoyaltyProgramAccount loyaltyProgram : passenger.getLoyaltyPrograms()) {
                    FrequentFlyer frequentFlyer = new FrequentFlyer();


                    frequentFlyer.setNumber(loyaltyProgram.getNumber());
                    frequentFlyer.setSupplierCode(null);
                    frequentFlyer.setTierLevelNumber(null);
                    frequentFlyer.setTierPriority(null);
                    frequentFlyer.setTierTag(null);

                    bookedTraveler.getLoyaltyNumbers().add(frequentFlyer);
                }
            }

        }

        if (null == bookedTraveler.getTierLevelNumber() || bookedTraveler.getTierLevelNumber().isEmpty()) {
            if (null != passenger.getLoyaltyPrograms() && !passenger.getLoyaltyPrograms().isEmpty()) {
                LoyaltyProgramAccount loyaltyProgram = passenger.getLoyaltyPrograms().get(0);

                bookedTraveler.setTierLevel(null);
                bookedTraveler.setTierLevelNumber(null);
                bookedTraveler.setTierTag(null);
                bookedTraveler.setFrequentFlyerProgram(null);
                bookedTraveler.setFrequentFlyerNumber(loyaltyProgram.getNumber());

            }
        }
    }

    public static void setOnwardTravelDate(BookedTraveler bookedTraveler, Passenger passenger, boolean infant) {
        if (null != passenger.getAdvancePassengerInformation()
                && null != passenger.getAdvancePassengerInformation().getOnwardTravelDate()) {
            if (infant) {
                bookedTraveler.getInfant().setOnwardTravelDate(passenger.getAdvancePassengerInformation().getOnwardTravelDate());
            } else {
                bookedTraveler.setOnwardTravelDate(passenger.getAdvancePassengerInformation().getOnwardTravelDate());
            }
        }
    }

    public static void setDestinationAddress(BookedTraveler bookedTraveler, Passenger passenger, boolean infant) {
        if (null != passenger.getAdvancePassengerInformation()
                && null != passenger.getAdvancePassengerInformation().getDestinationAddresses()
                && !passenger.getAdvancePassengerInformation().getDestinationAddresses().isEmpty()) {

            PostalAddress postalAddress = passenger.getAdvancePassengerInformation().getDestinationAddresses().get(0);

            DestinationAddress destinationAddress = new DestinationAddress();
            destinationAddress.setCountry(postalAddress.getCountry().getCode());
            destinationAddress.setAddressOne(postalAddress.getStreet());
            destinationAddress.setAddressTwo(null);
            destinationAddress.setCity(postalAddress.getCityName());
            destinationAddress.setState(postalAddress.getCountrySubDivision().getCode());
            destinationAddress.setZipCode(postalAddress.getStreet());

            if (infant) {
                bookedTraveler.getInfant().setDestinationAddress(destinationAddress);
            } else {
                bookedTraveler.setDestinationAddress(destinationAddress);
            }
        }
    }

    public static void mapSeats(BookedTraveler bookedTraveler, Passenger passenger, BookedLeg bookedLeg, List<Trip> trips) {

        if (null != passenger.getPassengerSegments()
                && !passenger.getPassengerSegments().isEmpty()) {

            for (PassengerSegment passengerSegment : passenger.getPassengerSegments()) {

                String segmentId = passengerSegment.getDatedOperatingSegmentId();

                for (PassengerLeg passengerLeg : passengerSegment.getPassengerLegs()) {
                    String legId = passengerLeg.getDatedOperatingLegId();

                    for (Trip trip : trips) {
                        DatedOperatingSegment datedOperatingSegment = trip.getSegment(segmentId);
                        DatedOperatingLeg datedOperatingLeg = trip.getLeg(segmentId, legId);

                        if (null != datedOperatingSegment && null != datedOperatingLeg) {
                            String segmentCode = TransformerUtil.getSegmentCode(datedOperatingSegment, datedOperatingLeg);

                            if (null != passengerLeg.getSeatAssignment() && !passengerLeg.getSeatAssignment().isEmpty()) {
                                for (SeatAssignment seatAssignment : passengerLeg.getSeatAssignment()) {

                                    SegmentChoice segmentChoice = new SegmentChoice();
                                    segmentChoice.setSegmentCode(segmentCode);
                                    SeatChoice seatChoice = new SeatChoice();

                                    if ("GATE".equalsIgnoreCase(seatAssignment.getSeatStatus())) {
                                        seatChoice.setCode("GATE");
                                    } else if (null != seatAssignment.getSeat()) {
                                        seatChoice.setCode(seatAssignment.getSeat().getNumber());
                                    }

                                    if (null != seatChoice.getCode()) {
                                        segmentChoice.setSeat(seatChoice);

                                        bookedTraveler.getSegmentChoices().removeBySegmentCode(segmentCode);

                                        bookedTraveler.getSegmentChoices().getCollection().add(segmentChoice);

                                        //map seat for infant
                                        if (null != bookedTraveler.getInfant()) {
                                            segmentChoice = new SegmentChoice();
                                            segmentChoice.setSegmentCode(segmentCode);
                                            seatChoice = new SeatChoice();

                                            seatChoice.setCode("INF");

                                            segmentChoice.setSeat(seatChoice);

                                            bookedTraveler.getInfant().getSegmentChoices().removeBySegmentCode(segmentCode);

                                            bookedTraveler.getInfant().getSegmentChoices().getCollection().add(segmentChoice);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static void setCountryOfResidence(BookedTraveler bookedTraveler, Passenger passenger, boolean infant) {

        if (null != passenger.getAdvancePassengerInformation()
                && null != passenger.getAdvancePassengerInformation().getCountryOfResidence()) {

            if (infant) {
                bookedTraveler.getInfant().setCountryOfResidence(passenger.getAdvancePassengerInformation().getCountryOfResidence().getCode());
            } else {
                bookedTraveler.setCountryOfResidence(passenger.getAdvancePassengerInformation().getCountryOfResidence().getCode());
            }
        }
    }

    public static void setDocuments(BookedTraveler bookedTraveler, Passenger passenger, boolean infant) {

        if (null != passenger.getAdvancePassengerInformation()
                && null != passenger.getAdvancePassengerInformation().getTravelDocuments()
                && !passenger.getAdvancePassengerInformation().getTravelDocuments().isEmpty()) {

            Optional<IdentityDocument> identityDocumentOptional = TransformerUtil.getPassport(
                    passenger.getAdvancePassengerInformation().getTravelDocuments()
            );

            if (identityDocumentOptional.isPresent()) {
                IdentityDocument identityDocument = identityDocumentOptional.get();

                if (infant) {
                    if (null != bookedTraveler.getInfant()) {
                        TravelerDocument travelerDocument = TransformerUtil.getTravelerDocument(identityDocument);

                        bookedTraveler.getInfant().setTravelDocument(travelerDocument);
                    }
                } else {
                    TravelerDocument travelerDocument = TransformerUtil.getTravelerDocument(identityDocument);

                    bookedTraveler.setTravelDocument(travelerDocument);
                }

                if (null != identityDocument.getSupplementaryDocuments()) {

                    Optional<SupplementaryDocument> supplementaryDocumentOptional = TransformerUtil.getVisa(
                            identityDocument.getSupplementaryDocuments()
                    );

                    if (supplementaryDocumentOptional.isPresent()) {
                        SupplementaryDocument supplementaryDocument = supplementaryDocumentOptional.get();

                        if (infant) {
                            VisaInfo visaInfo = TransformerUtil.getVisaInfo(supplementaryDocument);

                            bookedTraveler.getInfant().setVisaInfo(visaInfo);
                        } else {
                            VisaInfo visaInfo = TransformerUtil.getVisaInfo(supplementaryDocument);

                            bookedTraveler.setVisaInfo(visaInfo);
                        }
                    }
                }
            }
        }
    }

    public static void setDob(BookedTraveler bookedTraveler, Passenger passenger, boolean infant) {
        if (infant) {
            if (null != passenger.getBirthDate()) {
                bookedTraveler.getInfant().setDateOfBirth(passenger.getBirthDate());
            }
        } else {
            if (null != passenger.getBirthDate()) {
                bookedTraveler.setDateOfBirth(passenger.getBirthDate());
            }
        }
    }

    public static void setGender(BookedTraveler bookedTraveler, Passenger passenger, boolean infant) {
        if (infant) {
            if (null != passenger.getGender()) {
                bookedTraveler.getInfant().setGender(TransformerUtil.getGender(passenger.getGender(), infant));
            }
        } else {
            if (null != passenger.getGender()) {
                bookedTraveler.setGender(TransformerUtil.getGender(passenger.getGender(), infant));
            }
        }
    }

    public static void setBoardingDocuments(
            BookedTraveler bookedTraveler,
            BookedLeg bookedLeg,
            Passenger passenger,
            List<Trip> trips,
            WarningCollection warningCollection,
            boolean tsaRequired,
            String cartId,
            String pos
    ) {

        if (null != passenger.getBoardingPasses()) {

            for (BoardingDocument boardingDocument : passenger.getBoardingPasses()) {

                String segmentId = boardingDocument.getDatedOperatingSegmentId();
                String legId = boardingDocument.getDatedOperatingLegId();

                String segmentCode = null;
                if (null != segmentId && null != legId) {

                    Trip trip = TransformerUtil.getTrip(segmentId, legId, trips);

                    if (null != trip) {
                        DatedOperatingSegment datedOperatingSegment = trip.getSegment(segmentId);
                        DatedOperatingLeg datedOperatingLeg = datedOperatingSegment.getLeg(legId);


                        if (null != datedOperatingSegment && null != datedOperatingLeg) {
                            segmentCode = TransformerUtil.getSegmentCode(datedOperatingSegment, datedOperatingLeg);
                        }
                    }
                } else {
                    LOG.error("Leg or segment not found to map boarding pass.");
                }

                if (null == segmentCode) {
                    segmentCode = boardingDocument.getDepartureStationCode()
                            + "_"
                            + boardingDocument.getArrivalStationCode()
                            + "_"
                            + boardingDocument.getOperatingCarrierCode()
                            + "_"
                            + boardingDocument.getEstimatedDepartureLocalDateTime().substring(0, 10);
                }

                if (null != segmentCode) {

                    BookedSegment bookedSegment = bookedLeg.getBookedSegment(segmentCode);

                    if (null != bookedSegment) {

                        SegmentChoice segmentChoice = BoardingPassesUtil.getSegmentChoice(boardingDocument, segmentCode);

                        if (null != segmentChoice) {
                            bookedTraveler.getSegmentChoices().removeBySegmentCode(segmentCode);
                            bookedTraveler.getSegmentChoices().getCollection().add(segmentChoice);
                        }

                        SegmentDocument segmentDocument = new SegmentDocument();

                        segmentDocument.setId(UuidUtil.getId());
                        segmentDocument.setSegmentCode(segmentCode);
                        segmentDocument.setCardID(cartId);

                        if (null != boardingDocument.getSeatNumber()) {
                            if (boardingDocument.getSeatNumber().toUpperCase().contains("GATE")) {
                                segmentDocument.setSeat("GATE");
                            } else if (boardingDocument.getSeatNumber().toUpperCase().contains("INF")) {
                                segmentDocument.setSeat("INF");
                            } else {
                                segmentDocument.setSeat(boardingDocument.getSeatNumber());
                            }
                        } else {
                            segmentDocument.setSeat("GATE");
                        }

                        segmentDocument.setTicketNumber(boardingDocument.getTicketNumber());
                        segmentDocument.setBookingClass(null);
                        segmentDocument.setSelecteeText(boardingDocument.getSelecteeText());
                        segmentDocument.setSeamlessCheckin(true);

                        CheckInStatus checkInStatus = bookedTraveler.getCheckinStatusBySegment().getCheckInStatus(segmentCode);
                        if (null != checkInStatus && checkInStatus.isCheckinStatus()) {
                            segmentDocument.setCheckinStatus(true);
                        }

                        SegmentStatus segmentStatus = BoardingPassesUtil.extractSegmentStatusSabre(boardingDocument, bookedSegment);
                        segmentDocument.setSegmentStatus(segmentStatus);

                        segmentDocument.setSegmentId(Short.valueOf(bookedSegment.getSegment().getSegmentNumber()));
                        segmentDocument.setCheckInNumber(boardingDocument.getCheckinSequenceNumber());

                        segmentDocument.setBoardingZone(boardingDocument.getBoardingZone());
                        segmentDocument.setGate(boardingDocument.getBoardingGate());
                        segmentDocument.setGroupZone(null);
                        segmentDocument.setTerminal(boardingDocument.getBoardingTerminalName());

                        segmentDocument.setTsaPreCheck(boardingDocument.isTsaPrecheckedIndicator());
                        segmentDocument.setSkyPriority(boardingDocument.getSkyPriorityIndicator());
                        segmentDocument.setPriorityBoarding(false);

                        String barcodeString = boardingDocument.getBarcodeString();

                        if (null != barcodeString && !barcodeString.trim().isEmpty()) {
                            segmentDocument.setBarcode(barcodeString);
                        }

                        if (tsaRequired) {
                            bookedTraveler.getSegmentDocumentsListTSA().addOrReplace(segmentDocument);

                            if (PosType.KIOSK.toString().equalsIgnoreCase(pos)) {
                                Warning warning = new Warning(
                                        ErrorType.TSA_VALIDATION_REQUIRED.getErrorCode(),
                                        ErrorType.TSA_VALIDATION_REQUIRED.getFullDescription()
                                );
                                warningCollection.getCollection().add(warning);
                            }
                        } else if (null != barcodeString && !barcodeString.trim().isEmpty()) {
                            bookedTraveler.getSegmentDocumentsList().addOrReplace(segmentDocument);
                        }
                    } else {
                        LOG.error("BookedSegemnt not found by segmentcode {} to map boarding pass.", segmentCode);
                    }
                } else {
                    LOG.error("Unable to generate segmentcode to map boarding pass.");
                }
            }
        } else {
            if (null != passenger.getHandlingRestrictions() && !passenger.getHandlingRestrictions().isEmpty()) {
                bookedTraveler.getIneligibleReasons().add(CheckinIneligibleReasons.HANDLING_RESTRICTION_CHECKIN.name());
            }
        }
    }

    public static void setBoardingDocumentsForInfant(
            BookedTraveler bookedTraveler,
            BookedLeg bookedLeg,
            Passenger passenger,
            List<Trip> trips,
            WarningCollection warningCollection,
            boolean tsaRequired,
            String cartId,
            String pos
    ) {

        if (null != passenger.getBoardingPasses()) {
            for (BoardingDocument boardingDocument : passenger.getBoardingPasses()) {
                String segmentId = boardingDocument.getDatedOperatingSegmentId();
                String legId = boardingDocument.getDatedOperatingLegId();

                String segmentCode = null;
                if (null != segmentId && null != legId) {

                    Trip trip = TransformerUtil.getTrip(segmentId, legId, trips);

                    if (null != trip) {
                        DatedOperatingSegment datedOperatingSegment = trip.getSegment(segmentId);
                        DatedOperatingLeg datedOperatingLeg = datedOperatingSegment.getLeg(legId);


                        if (null != datedOperatingSegment && null != datedOperatingLeg) {
                            segmentCode = TransformerUtil.getSegmentCode(datedOperatingSegment, datedOperatingLeg);
                        }
                    }
                } else {
                    LOG.error("Leg or segment not found to map boarding pass.");
                }

                if (null == segmentCode) {
                    segmentCode = boardingDocument.getDepartureStationCode()
                            + "_"
                            + boardingDocument.getArrivalStationCode()
                            + "_"
                            + boardingDocument.getOperatingCarrierCode()
                            + "_"
                            + boardingDocument.getEstimatedDepartureLocalDateTime().substring(0, 10);
                }

                if (null != segmentCode) {
                    BookedSegment bookedSegment = bookedLeg.getBookedSegment(segmentCode);

                    if (null != bookedSegment) {

                        SegmentChoice segmentChoice = BoardingPassesUtil.getSegmentChoice(boardingDocument, segmentCode);

                        if (null != segmentChoice) {
                            bookedTraveler.getInfant().getSegmentChoices().removeBySegmentCode(segmentCode);
                            bookedTraveler.getInfant().getSegmentChoices().getCollection().add(segmentChoice);
                        }

                        SegmentDocument segmentDocument = new SegmentDocument();

                        segmentDocument.setId(UuidUtil.getId());
                        segmentDocument.setSegmentCode(segmentCode);
                        segmentDocument.setCardID(cartId);

                        if (null != boardingDocument.getSeatNumber()) {
                            if (boardingDocument.getSeatNumber().toUpperCase().contains("GATE")) {
                                segmentDocument.setSeat("GATE");
                            } else if (boardingDocument.getSeatNumber().toUpperCase().contains("INF")) {
                                segmentDocument.setSeat("INF");
                            } else {
                                segmentDocument.setSeat(boardingDocument.getSeatNumber());
                            }
                        } else {
                            segmentDocument.setSeat("GATE");
                        }

                        segmentDocument.setTicketNumber(boardingDocument.getTicketNumber());
                        segmentDocument.setBookingClass(null);
                        segmentDocument.setSelecteeText(boardingDocument.getSelecteeText());
                        segmentDocument.setSeamlessCheckin(true);

                        CheckInStatus checkInStatus = bookedTraveler.getInfant().getCheckinStatusBySegment().getCheckInStatus(segmentCode);
                        if (null != checkInStatus && checkInStatus.isCheckinStatus()) {
                            segmentDocument.setCheckinStatus(true);
                        }

                        SegmentStatus segmentStatus = BoardingPassesUtil.extractSegmentStatusSabre(boardingDocument, bookedSegment);
                        segmentDocument.setSegmentStatus(segmentStatus);

                        segmentDocument.setSegmentId(Short.valueOf(bookedSegment.getSegment().getSegmentNumber()));
                        segmentDocument.setCheckInNumber(boardingDocument.getCheckinSequenceNumber());

                        segmentDocument.setBoardingZone(boardingDocument.getBoardingZone());
                        segmentDocument.setGate(boardingDocument.getBoardingGate());
                        segmentDocument.setGroupZone(null);
                        segmentDocument.setTerminal(boardingDocument.getBoardingTerminalName());

                        segmentDocument.setTsaPreCheck(boardingDocument.isTsaPrecheckedIndicator());
                        segmentDocument.setSkyPriority(boardingDocument.getSkyPriorityIndicator());
                        segmentDocument.setPriorityBoarding(false);

                        String barcodeString = boardingDocument.getBarcodeString();

                        if (null != barcodeString && !barcodeString.trim().isEmpty()) {
                            segmentDocument.setBarcode(barcodeString);
                        }

                        if (tsaRequired) {
                            bookedTraveler.getInfant().getSegmentDocumentsListTSA().addOrReplace(segmentDocument);

                            if (PosType.KIOSK.toString().equalsIgnoreCase(pos)) {
                                Warning warning = new Warning(
                                        ErrorType.TSA_VALIDATION_REQUIRED.getErrorCode(),
                                        ErrorType.TSA_VALIDATION_REQUIRED.getFullDescription()
                                );
                                warningCollection.getCollection().add(warning);
                            }
                        } else if (null != barcodeString && !barcodeString.trim().isEmpty()) {
                            bookedTraveler.getInfant().getSegmentDocumentsList().addOrReplace(segmentDocument);
                        }
                    }
                }
            }
        } else {
            if (null != passenger.getHandlingRestrictions() && !passenger.getHandlingRestrictions().isEmpty()) {
                bookedTraveler.getIneligibleReasons().add(CheckinIneligibleReasons.INFANT_HANDLING_RESTRICTION_CHECKIN.name());
            }
        }
    }

    public static void setCheckInStatus(BookedTraveler bookedTraveler, BookedLeg bookedLeg, Passenger passenger, List<Trip> trips) {

        String firstSegmentCode = bookedLeg.getSegments().getCollection().get(0).getSegment().getSegmentCode();

        for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {
            PnrCollectionUtil.setCheckInStatusBySegment(bookedTraveler, bookedSegment.getSegment().getSegmentCode(), false);
        }

        for (PassengerSegment passengerSegment : passenger.getPassengerSegments()) {
            String segmentId = passengerSegment.getDatedOperatingSegmentId();

            if (CheckinStatus.CHECKED_IN.equals(passengerSegment.getCheckinStatus())) {

                for (PassengerLeg passengerLeg : passengerSegment.getPassengerLegs()) {
                    String legId = passengerLeg.getDatedOperatingLegId();

                    Trip trip = TransformerUtil.getTrip(segmentId, legId, trips);

                    if (null != trip) {
                        DatedOperatingSegment datedOperatingSegment = trip.getSegment(segmentId);
                        DatedOperatingLeg datedOperatingLeg = datedOperatingSegment.getLeg(legId);

                        if (null != datedOperatingSegment && null != datedOperatingLeg) {
                            String segmentCode = TransformerUtil.getSegmentCode(datedOperatingSegment, datedOperatingLeg);

                            if (SegmentCodeUtil.compareSegmentCodeWithoutTime(firstSegmentCode, segmentCode)) {
                                bookedTraveler.setCheckinStatus(true);
                                bookedTraveler.setIsSelectedToCheckin(false);
                            }

                            CheckInStatus checkInStatus = bookedTraveler.getCheckinStatusBySegment().getCheckInStatus(segmentCode);
                            if (null != checkInStatus) {
                                checkInStatus.setCheckinStatus(true);
                            }
                        }
                    }
                }
            }
        }
    }

    public static void setCheckInStatus(Infant bookedTraveler, BookedLeg bookedLeg, Passenger passenger, List<Trip> trips) {

        String firstSegmentCode = bookedLeg.getSegments().getCollection().get(0).getSegment().getSegmentCode();

        for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {
            boolean checkInStatus = false;
            PnrCollectionUtil.setCheckInStatusBySegment(bookedTraveler, bookedSegment.getSegment().getSegmentCode(), checkInStatus);
        }

        for (PassengerSegment passengerSegment : passenger.getPassengerSegments()) {
            String segmentId = passengerSegment.getDatedOperatingSegmentId();

            if (CheckinStatus.CHECKED_IN.equals(passengerSegment.getCheckinStatus())) {

                for (PassengerLeg passengerLeg : passengerSegment.getPassengerLegs()) {
                    String legId = passengerLeg.getDatedOperatingLegId();

                    for (Trip trip : trips) {
                        DatedOperatingSegment datedOperatingSegment = trip.getSegment(segmentId);
                        DatedOperatingLeg datedOperatingLeg = trip.getLeg(segmentId, legId);

                        if (null != datedOperatingSegment && null != datedOperatingLeg) {
                            String segmentCode = TransformerUtil.getSegmentCode(datedOperatingSegment, datedOperatingLeg);

                            if (SegmentCodeUtil.compareSegmentCodeWithoutTime(firstSegmentCode, segmentCode)) {
                                bookedTraveler.setCheckinStatus(true);
                            }

                            CheckInStatus checkInStatus = bookedTraveler.getCheckinStatusBySegment().getCheckInStatus(segmentCode);
                            if (null != checkInStatus) {
                                checkInStatus.setCheckinStatus(true);
                            }
                        }
                    }
                }
            }
        }
    }

    public static boolean setPrimaryDocumentsRequirement(BookedTraveler bookedTraveler, Passenger passenger, boolean infant) {

        if (null != passenger.getAdvancePassengerInformation()) {
            if (null != passenger.getAdvancePassengerInformation().getRequiredPrimaryDocuments()) {
                if (passenger.getAdvancePassengerInformation().getRequiredPrimaryDocuments().isMandatory()) {
                    switch (passenger.getAdvancePassengerInformation().getRequiredPrimaryDocuments().getIdentityDocumentType()) {
                        case PASSPORT:
                            if (infant) {
                                bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("DOCS/INF"));
                            } else {
                                bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("DOCS"));
                            }

                            return true;
                        case NEXUS_CARD:
                        case NATIONAL_ID:
                        case PERMANENT_RESIDENT_CARD:
                            //TOOD: how to map that?
                            break;
                    }
                }
            }
        }

        return false;

    }

    public static boolean setSecondaryDocumentsRequirement(BookedTraveler bookedTraveler, Passenger passenger, boolean infant) {
        if (null != passenger.getAdvancePassengerInformation()
                && null != passenger.getAdvancePassengerInformation().getRequiredSecondaryDocuments()) {
            for (DocumentCombinationChoice requiredSecondaryDocument : passenger.getAdvancePassengerInformation().getRequiredSecondaryDocuments()) {
                if (requiredSecondaryDocument.isMandatory()) {
                    switch (requiredSecondaryDocument.getSupplementaryDocumentType()) {
                        case VISA:
                            if (infant) {
                                bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("INFANT_VISA"));
                            } else {
                                bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("VISA"));
                            }

                            return true;
                    }
                }
            }
        }

        return false;
    }

    public static void resetMissings(BookedTraveler bookedTraveler) {
        bookedTraveler.getMissingCheckinRequiredFields().remove(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("INFANT_GENDER"));
        bookedTraveler.getMissingCheckinRequiredFields().remove(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("INFANT_DOB"));
        bookedTraveler.getMissingCheckinRequiredFields().remove(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("INFANT_NATIONALITY"));
        bookedTraveler.getMissingCheckinRequiredFields().remove(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("DOCA/R/INF"));
        bookedTraveler.getMissingCheckinRequiredFields().remove(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("DOCA/D/INF"));
        bookedTraveler.getMissingCheckinRequiredFields().remove(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("DOCS/INF"));
        bookedTraveler.getMissingCheckinRequiredFields().remove(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("INFANT_VISA"));
        bookedTraveler.getMissingCheckinRequiredFields().remove(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("INFANT_ONWARD_TRAVEL_DATE"));
        //bookedTraveler.getMissingCheckinRequiredFields().remove(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("INFANT_TIMATIC_STATUS_REQUIRED"));

        bookedTraveler.getMissingCheckinRequiredFields().remove(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("GENDER"));
        bookedTraveler.getMissingCheckinRequiredFields().remove(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("DOB"));
        bookedTraveler.getMissingCheckinRequiredFields().remove(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("NATIONALITY"));
        bookedTraveler.getMissingCheckinRequiredFields().remove(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("DOCA/R"));
        bookedTraveler.getMissingCheckinRequiredFields().remove(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("DOCA/D"));
        bookedTraveler.getMissingCheckinRequiredFields().remove(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("DOCS"));
        bookedTraveler.getMissingCheckinRequiredFields().remove(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("VISA"));
        bookedTraveler.getMissingCheckinRequiredFields().remove(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("ONWARD_TRAVEL_DATE"));
        //bookedTraveler.getMissingCheckinRequiredFields().remove(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("TIMATIC_STATUS_REQUIRED"));
    }

    public static Set<AdvancePassengerInformationRequirementCodeType> setAdvancePassengerInformationRequirements(BookedTraveler bookedTraveler, Passenger passenger, boolean infant) {

        Set<AdvancePassengerInformationRequirementCodeType> advancePassengerInformationRequirementCodeTypes = new HashSet<>();

        if (null != passenger.getAdvancePassengerInformation()
                && null != passenger.getAdvancePassengerInformation().getAdvancePassengerInformationRequirements()) {
            for (AdvancePassengerInformationRequirement advancePassengerInformationRequirement : passenger.getAdvancePassengerInformation().getAdvancePassengerInformationRequirements()) {
                if (advancePassengerInformationRequirement.isRequired()) {
                    switch (advancePassengerInformationRequirement.getRequirementType()) {
                        case GENDER:
                            if (infant) {
                                bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("INFANT_GENDER"));
                            } else {
                                bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("GENDER"));
                            }

                            advancePassengerInformationRequirementCodeTypes.add(AdvancePassengerInformationRequirementCodeType.GENDER);

                            break;
                        case DATE_OF_BIRTH:
                            if (infant) {
                                bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("INFANT_DOB"));
                            } else {
                                bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("DOB"));
                            }

                            advancePassengerInformationRequirementCodeTypes.add(AdvancePassengerInformationRequirementCodeType.DATE_OF_BIRTH);

                            break;
                        case NATIONALITY:
                            if (infant) {
                                bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("INFANT_NATIONALITY"));
                            } else {
                                bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("NATIONALITY"));
                            }

                            advancePassengerInformationRequirementCodeTypes.add(AdvancePassengerInformationRequirementCodeType.NATIONALITY);

                            break;
                        case COUNTRY_OF_RESIDENCE:
                            if (infant) {
                                bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("DOCA/R/INF"));
                            } else {
                                bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("DOCA/R"));
                            }

                            advancePassengerInformationRequirementCodeTypes.add(AdvancePassengerInformationRequirementCodeType.COUNTRY_OF_RESIDENCE);

                            break;
                        case DESTINATION_ADDRESS:
                            if (infant) {
                                bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("DOCA/D/INF"));
                            } else {
                                bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("DOCA/D"));
                            }

                            advancePassengerInformationRequirementCodeTypes.add(AdvancePassengerInformationRequirementCodeType.DESTINATION_ADDRESS);

                            break;
                        case ONWARD_TRAVEL_DATE:
                            if (infant) {
                                bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("INFANT_ONWARD_TRAVEL_DATE"));
                                //bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("INFANT_TIMATIC_STATUS_REQUIRED"));
                            } else {
                                bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("ONWARD_TRAVEL_DATE"));
                                //bookedTraveler.getMissingCheckinRequiredFields().add(ValidateCheckinIneligibleReason.getCheckinIneligibleReason("TIMATIC_STATUS_REQUIRED"));
                            }

                            advancePassengerInformationRequirementCodeTypes.add(AdvancePassengerInformationRequirementCodeType.ONWARD_TRAVEL_DATE);
                            break;
                    }
                }
            }
        }

        return advancePassengerInformationRequirementCodeTypes;
    }

    public static void setNotifications(TravelParty travelParty, CartPNR cartPNR) {

        cartPNR.getNotifications().getCollection().clear();

        if (null != travelParty.getNotifications()) {
            for (com.am.seamless.checkin.common.models.Notification notification : travelParty.getNotifications()) {
                Notification notificationCartPNR = new Notification();
                notificationCartPNR.setCode(notification.getCode());
                notificationCartPNR.setDescription(notification.getDescription());
                if (null != notification.getType()) {
                    notificationCartPNR.setType(notification.getType().name());
                }
                cartPNR.getNotifications().getCollection().add(notificationCartPNR);
            }
        }
    }

    public static void setMandates(TravelParty travelParty, CartPNR cartPNR) {

        cartPNR.getMandates().getCollection().clear();

        if (null != travelParty.getMandates()) {
            for (com.am.seamless.checkin.common.models.Mandate mandate : travelParty.getMandates()) {
                Mandate mandateCartPNR = new Mandate();
                mandateCartPNR.setCode(mandate.getCode());
                mandateCartPNR.setConfirmed(mandate.isConfirmed());
                mandateCartPNR.setDescription(mandate.getDescription());
                mandateCartPNR.setGroupType(mandate.getGroupType().name());
                mandateCartPNR.setId(mandate.getId());
                cartPNR.getMandates().getCollection().add(mandateCartPNR);
            }
        }
    }

    public static void setLinks(TravelParty travelParty, CartPNR cartPNR) {
        cartPNR.getLinks().clear();

        cartPNR.setCheckInAllowed(false);

        if (null != travelParty.getLinks() && !travelParty.getLinks().isEmpty()) {
            for (Map.Entry<String, Link> stringLinkEntry : travelParty.getLinks().entrySet()) {
                HateoasLink hateoasLink = new HateoasLink();
                hateoasLink.setHref(stringLinkEntry.getValue().getHref());

                cartPNR.getLinks().put(stringLinkEntry.getKey(), hateoasLink);

                if (LinkConstant.BOARDING_DOCUMENTS.equalsIgnoreCase(stringLinkEntry.getKey())) {
                    cartPNR.setCheckInAllowed(true);
                }
            }
        }
    }
}
