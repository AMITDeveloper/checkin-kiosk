package com.am.checkin.web.v2.util;

import com.aeromexico.sharedservices.util.AESEncryption;
import org.apache.commons.codec.DecoderException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * @author adrianleal
 */
public class SystemVariablesUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(SystemVariablesUtil.class);

    private static double standByPaxLimitPercentage;
    private static final double DEFAULT_STAND_BY_PAX_LIMIT_PERCENTAGE = 0.0;

    private static boolean tsaRequired;
    private static final boolean DEFAULT_TSA_REQUIRED = true;

    private static boolean amTierBenefitsTurnOff;
    private static final boolean DEFAULT_AM_TIER_BENEFITS_TURN_OFF = false;

    private static boolean dlTierBenefitsTurnOff;
    private static final boolean DEFAULT_DL_TIER_BENEFITS_TURN_OFF = false;

    private static int domesticCheckinWindowTimeInHrs;
    private static final int DEFAULT_DOMESTIC_CHK_WINDOW_TIME_IN_HRS = 48;

    private static int internationalCheckinWindowTimeInHrs;
    private static final int DEFAULT_INTERNATIONAL_CHK_WINDOW_TIME_IN_HRS = 24;

    private static boolean timaticEnabled;
    private static final boolean DEFAULT_TIMATIC_ENABLED = true;

    private static boolean addRevenuePaxToPriorityListEnabled;
    private static final boolean DEFAULT_ADD_REVENUE_PAX_TO_PRIORITY_LIST_ENABLED = false;

    private static boolean checkChildrenOnBasicEnabled;
    private static final boolean DEFAULT_CHECK_CHILDREN_ON_BASIC_ENABLED = true;

    private static String mobileBoardingPassTemplateName;
    private static final String DEFAULT_MOBILE_BOARDING_PASS_TEMPLATE_NAME = "PNGBoardingPass.vm";

    private static String mobileBoardingPassTmpPath;
    private static final String DEFAULT_MOBILE_BOARDING_PASS_TMP_PATH = "/opt/am/tmp/";

    private static boolean fareLogixSeatmapEnabled;
    private static final boolean DEFAULT_FARELOGIX_SEATMAP_ENABLED = false;

    private static boolean fareLogixUpgradeEnabled;
    private static final boolean DEFAULT_FARELOGIX_UPGRADE_ENABLED = false;

    private static boolean robotPassengerListEnabled;
    private static final boolean DEFAULT_ROBOT_PASSENGER_LIST_ENABLED = false;

    private static boolean bigQueryEnabled;
    private static final boolean DEFAULT_BIG_QUERY_ENABLED = true;

    private static boolean createBoardingPassesOnPurchaseEnabled;
    private static final boolean DEFAULT_CREATE_BOARDING_PASSES_ON_PURCHASE_ENABLED = true;

    private static boolean createDownloadBoardingPassesLinkEnabled;
    private static final boolean DEFAULT_CREATE_DOWNLOAD_BOARDING_PASSES_LINK_ENABLED = true;

    private static boolean validateSeamlessCheckinMandatesEnabled;
    private static final boolean DEFAULT_VALIDATE_SEAMLESS_CHECKIN_MANDATES_ENABLED = false;

    private static boolean signAllSeamlessCheckinBoardingPassesEnabled;
    private static final boolean DEFAULT_SIGN_ALL_SEAMLESS_CHECKIN_BOARDING_PASSES_ENABLED = false;

    private static boolean seamlessCheckinGhEnabled;
    private static final boolean DEFAULT_SEAMLESS_CHECKIN_GH_ENABLED = false;

    private static boolean seamlessCheckinOpEnabled;
    private static final boolean DEFAULT_SEAMLESS_CHECKIN_OP_ENABLED = false;

    private static boolean forceVisaForSeamlessEnabled;
    private static final boolean DEFAULT_FORCE_VISA_FOR_SEAMLESS_ENABLED = false;

    private static boolean validateSeamlessBoardingDocumentsLinkOnReprintEnabled;
    private static final boolean DEFAULT_VALIDATE_SEAMLESS_BOARDING_DOCUMENTS_LINK_ON_REPRINT_ENABLED = true;

    private static boolean validateSeamlessBoardingDocumentsLinkOnFirstAttemptEnabled;
    private static final boolean DEFAULT_VALIDATE_SEAMLESS_BOARDING_DOCUMENTS_LINK_ON_FIRST_ATTEMPT_ENABLED = true;

    private static boolean seamlessCheckinForNotAMMarketedEnabled;
    private static final boolean DEFAULT_SEAMLESS_CHECKIN_FOR_NOT_AM_MARKETED_ENABLED = false;

    private  static String seamlessCheckinUrl;
    private static final String DEFAULT_SEAMLESS_CHECKIN_URL = "http://localhost:8088";

    private  static String seamlessCheckinUser;
    private static final String DEFAULT_SEAMLESS_CHECKIN_USER = "adrian";

    private  static String seamlessCheckinPass;
    private static final String DEFAULT_SEAMLESS_CHECKIN_PASS = "adrian";

    private static int seamlessCheckinTimeout;
    private static final int DEFAULT_SEAMLESS_CHECKIN_TIMEOUT = 25;

    private  static String digitalSignatureUrl;
    private static final String DEFAULT_DIGITAL_SIGNATURE_URL = "http://localhost:8084";

    private  static String digitalSignatureUser;
    private static final String DEFAULT_DIGITAL_SIGNATURE_USER = "adrian";

    private  static String digitalSignaturePass;
    private static final String DEFAULT_DIGITAL_SIGNATURE_PASS = "adrian";

    private static int digitalSignatureTimeout;
    private static final int DEFAULT_DIGITAL_SIGNATURE_TIMEOUT = 25;

    private static String threeDsRedirectPath;
    private static final String DEFAULT_3_DS_REDIRECT_PATH = "http://localhost:4444";

    private static String threeDsDefaultReturnPath;
    private static String threeDsCancelReturnPath;
    private static String threeDsApprovedReturnPath;
    private static String threeDsDeclinedReturnPath;
    private static String threeDsErrorReturnPath;
    private static String threeDsExpiredReturnPath;
    private static String threeDsPendingReturnPath;
    private static final String THREE_DS_DEFAULT_RETURN_PATH = "http://localhost:8080/checkIn/Payment/RedirectPayment?status=ok";

    private static boolean threeDsWebEnabled;
    private static final boolean DEFAULT_THREE_DS_WEB_ENABLED = false;

    private static boolean threeDsKioskEnabled;
    private static final boolean DEFAULT_THREE_DS_KIOSK_ENABLED = false;

    private static boolean threeDsAppEnabled;
    private static final boolean DEFAULT_THREE_DS_APP_ENABLED = false;

    private static boolean earlyCheckinEnabled;
    private static final boolean DEFAULT_EARLY_CHECKIN_ENABLED = false;

    private static boolean loggingEnabled;
    private static final boolean DEFAULT_LOGGING_ENABLED = true;

    private static int earlyCheckinWindowTimeLimitInHrs;
    private static final int DEFAULT_EARLY_CHK_WINDOW_TIME_LIMIT_IN_HRS = 4;

    private static int pnrLogExpireTimeInDays;
    private static final int DEFAULT_PNR_LOGS_EXPIRE_TIME_IN_DAYS= 90;

    private static String earlyCheckinUser;
    private static String earlyCheckinPassword;
    private static String earlyCheckinSeed;

    private  static String chubbAgentName;
    private static final String DEFAULT_CHUBB_AGENT_NAME = "AMX";

    private  static String chubbAgentCode;
    private static final String DEFAULT_CHUBB_AGENT_CODE = "AMX";

    private  static String chubbAgentInternalCode;
    private static final String DEFAULT_CHUBB_AGENT_INTERNAL_CODE = "amxwci";

    static {

        try {
            String threeds = System.getProperty("three_ds_web_enabled");

            if (null == threeds || threeds.isEmpty()) {
                threeDsWebEnabled = DEFAULT_THREE_DS_WEB_ENABLED;
            } else {
                threeDsWebEnabled = Boolean.valueOf(threeds);
            }
        } catch (Exception ex) {
            threeDsWebEnabled = DEFAULT_THREE_DS_WEB_ENABLED;
        }

        try {
            String threeds = System.getProperty("three_ds_kiosk_enabled");

            if (null == threeds || threeds.isEmpty()) {
                threeDsKioskEnabled = DEFAULT_THREE_DS_KIOSK_ENABLED;
            } else {
                threeDsKioskEnabled = Boolean.valueOf(threeds);
            }
        } catch (Exception ex) {
            threeDsKioskEnabled = DEFAULT_THREE_DS_KIOSK_ENABLED;
        }

        try {
            String threeds = System.getProperty("three_ds_app_enabled");

            if (null == threeds || threeds.isEmpty()) {
                threeDsAppEnabled = DEFAULT_THREE_DS_APP_ENABLED;
            } else {
                threeDsAppEnabled = Boolean.valueOf(threeds);
            }
        } catch (Exception ex) {
            threeDsAppEnabled = DEFAULT_THREE_DS_APP_ENABLED;
        }

        try {
            String url = System.getProperty("3_ds_default_return_path");

            if (null == url || url.isEmpty()) {
                threeDsDefaultReturnPath = THREE_DS_DEFAULT_RETURN_PATH;
            } else {
                threeDsDefaultReturnPath = url;
            }
        } catch (Exception ex) {
            threeDsDefaultReturnPath = THREE_DS_DEFAULT_RETURN_PATH;
        }

        try {
            String url = System.getProperty("3_ds_cancel_return_path");

            if (null == url || url.isEmpty()) {
                threeDsCancelReturnPath = THREE_DS_DEFAULT_RETURN_PATH;
            } else {
                threeDsCancelReturnPath = url;
            }
        } catch (Exception ex) {
            threeDsCancelReturnPath = THREE_DS_DEFAULT_RETURN_PATH;
        }

        try {
            String url = System.getProperty("3_ds_approved_return_path");

            if (null == url || url.isEmpty()) {
                threeDsApprovedReturnPath = THREE_DS_DEFAULT_RETURN_PATH;
            } else {
                threeDsApprovedReturnPath = url;
            }
        } catch (Exception ex) {
            threeDsApprovedReturnPath = THREE_DS_DEFAULT_RETURN_PATH;
        }

        try {
            String url = System.getProperty("3_ds_declined_return_path");

            if (null == url || url.isEmpty()) {
                threeDsDeclinedReturnPath = THREE_DS_DEFAULT_RETURN_PATH;
            } else {
                threeDsDeclinedReturnPath = url;
            }
        } catch (Exception ex) {
            threeDsDeclinedReturnPath = THREE_DS_DEFAULT_RETURN_PATH;
        }

        try {
            String url = System.getProperty("3_ds_error_return_path");

            if (null == url || url.isEmpty()) {
                threeDsErrorReturnPath = THREE_DS_DEFAULT_RETURN_PATH;
            } else {
                threeDsErrorReturnPath = url;
            }
        } catch (Exception ex) {
            threeDsErrorReturnPath = THREE_DS_DEFAULT_RETURN_PATH;
        }

        try {
            String url = System.getProperty("3_ds_expired_return_path");

            if (null == url || url.isEmpty()) {
                threeDsExpiredReturnPath = THREE_DS_DEFAULT_RETURN_PATH;
            } else {
                threeDsExpiredReturnPath = url;
            }
        } catch (Exception ex) {
            threeDsExpiredReturnPath = THREE_DS_DEFAULT_RETURN_PATH;
        }

        try {
            String url = System.getProperty("3_ds_pending_return_path");

            if (null == url || url.isEmpty()) {
                threeDsPendingReturnPath = THREE_DS_DEFAULT_RETURN_PATH;
            } else {
                threeDsPendingReturnPath = url;
            }
        } catch (Exception ex) {
            threeDsPendingReturnPath = THREE_DS_DEFAULT_RETURN_PATH;
        }

        try {
            String bigquery = System.getProperty("bigquery_enabled");

            if (null == bigquery || bigquery.isEmpty()) {
                bigQueryEnabled = DEFAULT_BIG_QUERY_ENABLED;
            } else {
                bigQueryEnabled = Boolean.valueOf(bigquery);
            }
        } catch (Exception ex) {
            bigQueryEnabled = DEFAULT_BIG_QUERY_ENABLED;
        }

        try {
            String createBP = System.getProperty("create_boarding_passes_on_purchase_enabled");

            if (null == createBP || createBP.isEmpty()) {
                createBoardingPassesOnPurchaseEnabled = DEFAULT_CREATE_BOARDING_PASSES_ON_PURCHASE_ENABLED;
            } else {
                createBoardingPassesOnPurchaseEnabled = Boolean.valueOf(createBP);
            }
        } catch (Exception ex) {
            createBoardingPassesOnPurchaseEnabled = DEFAULT_CREATE_BOARDING_PASSES_ON_PURCHASE_ENABLED;
        }

        try {
            String createDL = System.getProperty("create_download_boarding_passes_link_enabled");

            if (null == createDL || createDL.isEmpty()) {
                createDownloadBoardingPassesLinkEnabled = DEFAULT_CREATE_DOWNLOAD_BOARDING_PASSES_LINK_ENABLED;
            } else {
                createDownloadBoardingPassesLinkEnabled = Boolean.valueOf(createDL);
            }
        } catch (Exception ex) {
            createDownloadBoardingPassesLinkEnabled = DEFAULT_CREATE_DOWNLOAD_BOARDING_PASSES_LINK_ENABLED;
        }

        try {
            String farelx = System.getProperty("farelogix_seatmap_enabled");

            if (null == farelx || farelx.isEmpty()) {
                fareLogixSeatmapEnabled = DEFAULT_FARELOGIX_SEATMAP_ENABLED;
            } else {
                fareLogixSeatmapEnabled = Boolean.valueOf(farelx);
            }

        } catch (Exception ex) {
            fareLogixSeatmapEnabled = DEFAULT_FARELOGIX_SEATMAP_ENABLED;
        }

        try {
            String farelx = System.getProperty("farelogix_upgrade_enabled");

            if (null == farelx || farelx.isEmpty()) {
                fareLogixUpgradeEnabled = DEFAULT_FARELOGIX_UPGRADE_ENABLED;
            } else {
                fareLogixUpgradeEnabled = Boolean.valueOf(farelx);
            }

        } catch (Exception ex) {
            fareLogixUpgradeEnabled = DEFAULT_FARELOGIX_UPGRADE_ENABLED;
        }

        try {
            String name = System.getProperty("mobile_boardingpass_template_name");

            if (null == name || name.isEmpty()) {
                mobileBoardingPassTemplateName = DEFAULT_MOBILE_BOARDING_PASS_TEMPLATE_NAME;
            } else {
                mobileBoardingPassTemplateName = name;
            }
        } catch (Exception ex) {
            mobileBoardingPassTemplateName = DEFAULT_MOBILE_BOARDING_PASS_TEMPLATE_NAME;
        }

        try {
            String path = System.getProperty("mobile_boardingpass_tmp_path");

            if (null == path || path.isEmpty()) {
                mobileBoardingPassTmpPath = DEFAULT_MOBILE_BOARDING_PASS_TMP_PATH;
            } else {
                mobileBoardingPassTmpPath = path;
            }
        } catch (Exception ex) {
            mobileBoardingPassTmpPath = DEFAULT_MOBILE_BOARDING_PASS_TMP_PATH;
        }

        try {
            String tsa = System.getProperty("tsa_validation");

            if (null == tsa || tsa.isEmpty()) {
                SystemVariablesUtil.tsaRequired = DEFAULT_TSA_REQUIRED;
            } else {
                SystemVariablesUtil.tsaRequired = Boolean.valueOf(tsa);
            }
        } catch (Exception e) {
            tsaRequired = DEFAULT_TSA_REQUIRED;
        }

        try {
            double value = Double.valueOf(System.getProperty("stand_by_pax_limit_percentage"));

            if (value <= 0.0) {
                standByPaxLimitPercentage = DEFAULT_STAND_BY_PAX_LIMIT_PERCENTAGE;
            } else {
                standByPaxLimitPercentage = value;
            }
        } catch (NumberFormatException ex) {
            standByPaxLimitPercentage = DEFAULT_STAND_BY_PAX_LIMIT_PERCENTAGE;
        } catch (Exception e) {
            standByPaxLimitPercentage = DEFAULT_STAND_BY_PAX_LIMIT_PERCENTAGE;
        }

        try {
            String turnOff = System.getProperty("turn_off_am_tier_benefits_on_chk");

            if (null == turnOff || turnOff.isEmpty()) {
                amTierBenefitsTurnOff = DEFAULT_AM_TIER_BENEFITS_TURN_OFF;
            } else {
                amTierBenefitsTurnOff = Boolean.valueOf(turnOff);
            }
        } catch (Exception e) {
            amTierBenefitsTurnOff = DEFAULT_AM_TIER_BENEFITS_TURN_OFF;
        }
        try {
            String turnOff = System.getProperty("turn_off_dl_tier_benefits_on_chk");

            if (null == turnOff || turnOff.isEmpty()) {
                dlTierBenefitsTurnOff = DEFAULT_DL_TIER_BENEFITS_TURN_OFF;
            } else {
                dlTierBenefitsTurnOff = Boolean.valueOf(turnOff);
            }
        } catch (Exception e) {
            dlTierBenefitsTurnOff = DEFAULT_DL_TIER_BENEFITS_TURN_OFF;
        }

        try {
            int value = Integer.valueOf(System.getProperty("domestic_chk_window_time_in_hrs"));

            if (value <= 0) {
                domesticCheckinWindowTimeInHrs = DEFAULT_DOMESTIC_CHK_WINDOW_TIME_IN_HRS;
            } else {
                domesticCheckinWindowTimeInHrs = value;
            }
        } catch (NumberFormatException ex) {
            domesticCheckinWindowTimeInHrs = DEFAULT_DOMESTIC_CHK_WINDOW_TIME_IN_HRS;
        } catch (Exception e) {
            domesticCheckinWindowTimeInHrs = DEFAULT_DOMESTIC_CHK_WINDOW_TIME_IN_HRS;
        }

        try {
            int value = Integer.valueOf(System.getProperty("international_chk_window_time_in_hrs"));

            if (value <= 0) {
                internationalCheckinWindowTimeInHrs = DEFAULT_INTERNATIONAL_CHK_WINDOW_TIME_IN_HRS;
            } else {
                internationalCheckinWindowTimeInHrs = value;
            }
        } catch (NumberFormatException ex) {
            internationalCheckinWindowTimeInHrs = DEFAULT_INTERNATIONAL_CHK_WINDOW_TIME_IN_HRS;
        } catch (Exception e) {
            internationalCheckinWindowTimeInHrs = DEFAULT_INTERNATIONAL_CHK_WINDOW_TIME_IN_HRS;
        }

        try {
            String timatic = System.getProperty("timatic");

            if (null == timatic || timatic.isEmpty()) {
                timaticEnabled = DEFAULT_TIMATIC_ENABLED;
            } else {
                timaticEnabled = Boolean.valueOf(timatic);
            }
        } catch (Exception ex) {
            timaticEnabled = DEFAULT_TIMATIC_ENABLED;
        }

        try {
            String addRevenue2Priority = System.getProperty("add_revenue_pax_to_priority_list");

            if (null == addRevenue2Priority || addRevenue2Priority.isEmpty()) {
                addRevenuePaxToPriorityListEnabled = DEFAULT_ADD_REVENUE_PAX_TO_PRIORITY_LIST_ENABLED;
            } else {
                addRevenuePaxToPriorityListEnabled = Boolean.valueOf(addRevenue2Priority);
            }
        } catch (Exception ex) {
            addRevenuePaxToPriorityListEnabled = DEFAULT_ADD_REVENUE_PAX_TO_PRIORITY_LIST_ENABLED;
        }

        try {
            String validateMandates = System.getProperty("validate.seamless.checkin.mandates.enabled");

            if (null == validateMandates || validateMandates.isEmpty()) {
                setValidateSeamlessCheckinMandatesEnabled(DEFAULT_VALIDATE_SEAMLESS_CHECKIN_MANDATES_ENABLED);
            } else {
                setValidateSeamlessCheckinMandatesEnabled(Boolean.valueOf(validateMandates));
            }
        } catch (Exception ex) {
            setValidateSeamlessCheckinMandatesEnabled(DEFAULT_VALIDATE_SEAMLESS_CHECKIN_MANDATES_ENABLED);
        }

        try {
            String signAll = System.getProperty("sign.all.seamless.checkin.boarding.passes.enabled");

            if (null == signAll || signAll.isEmpty()) {
                setSignAllSeamlessCheckinBoardingPassesEnabled(DEFAULT_SIGN_ALL_SEAMLESS_CHECKIN_BOARDING_PASSES_ENABLED);
            } else {
                setSignAllSeamlessCheckinBoardingPassesEnabled(Boolean.valueOf(signAll));
            }
        } catch (Exception ex) {
            setSignAllSeamlessCheckinBoardingPassesEnabled(DEFAULT_SIGN_ALL_SEAMLESS_CHECKIN_BOARDING_PASSES_ENABLED);
        }

        try {
            String seamlessCheckin = System.getProperty("seamless.checkin.gh.enabled");

            if (null == seamlessCheckin || seamlessCheckin.isEmpty()) {
                setSeamlessCheckinGhEnabled(DEFAULT_SEAMLESS_CHECKIN_GH_ENABLED);
            } else {
                setSeamlessCheckinGhEnabled(Boolean.valueOf(seamlessCheckin));
            }
        } catch (Exception ex) {
            setSeamlessCheckinGhEnabled(DEFAULT_SEAMLESS_CHECKIN_GH_ENABLED);
        }

        try {
            String seamlessCheckin = System.getProperty("seamless.checkin.op.enabled");

            if (null == seamlessCheckin || seamlessCheckin.isEmpty()) {
                setSeamlessCheckinOpEnabled(DEFAULT_SEAMLESS_CHECKIN_OP_ENABLED);
            } else {
                setSeamlessCheckinOpEnabled(Boolean.valueOf(seamlessCheckin));
            }
        } catch (Exception ex) {
            setSeamlessCheckinOpEnabled(DEFAULT_SEAMLESS_CHECKIN_OP_ENABLED);
        }

        try {
            String forceVisa = System.getProperty("seamless.checkin.force.visa.enabled");

            if (null == forceVisa || forceVisa.isEmpty()) {
                setForceVisaForSeamlessEnabled(DEFAULT_FORCE_VISA_FOR_SEAMLESS_ENABLED);
            } else {
                setForceVisaForSeamlessEnabled(Boolean.valueOf(forceVisa));
            }
        } catch (Exception ex) {
            setForceVisaForSeamlessEnabled(DEFAULT_FORCE_VISA_FOR_SEAMLESS_ENABLED);
        }

        try {
            String validateLink = System.getProperty("seamless.checkin.validate.boarding.documents.link.on.reprint.enabled");

            if (null == validateLink || validateLink.isEmpty()) {
                setValidateSeamlessBoardingDocumentsLinkOnReprintEnabled(DEFAULT_VALIDATE_SEAMLESS_BOARDING_DOCUMENTS_LINK_ON_REPRINT_ENABLED);
            } else {
                setValidateSeamlessBoardingDocumentsLinkOnReprintEnabled(Boolean.valueOf(validateLink));
            }
        } catch (Exception ex) {
            setValidateSeamlessBoardingDocumentsLinkOnReprintEnabled(DEFAULT_VALIDATE_SEAMLESS_BOARDING_DOCUMENTS_LINK_ON_REPRINT_ENABLED);
        }

        try {
            String validateLink = System.getProperty("seamless.checkin.validate.boarding.documents.link.on.first.attempt.enabled");

            if (null == validateLink || validateLink.isEmpty()) {
                setValidateSeamlessBoardingDocumentsLinkOnFirstAttemptEnabled(DEFAULT_VALIDATE_SEAMLESS_BOARDING_DOCUMENTS_LINK_ON_FIRST_ATTEMPT_ENABLED);
            } else {
                setValidateSeamlessBoardingDocumentsLinkOnFirstAttemptEnabled(Boolean.valueOf(validateLink));
            }
        } catch (Exception ex) {
            setValidateSeamlessBoardingDocumentsLinkOnFirstAttemptEnabled(DEFAULT_VALIDATE_SEAMLESS_BOARDING_DOCUMENTS_LINK_ON_FIRST_ATTEMPT_ENABLED);
        }

        try {
            String notAMMarketed = System.getProperty("seamless.checkin.for.not.am.marketed.enabled");

            if (null == notAMMarketed || notAMMarketed.isEmpty()) {
                setSeamlessCheckinForNotAMMarketedEnabled(DEFAULT_SEAMLESS_CHECKIN_FOR_NOT_AM_MARKETED_ENABLED);
            } else {
                setSeamlessCheckinForNotAMMarketedEnabled(Boolean.valueOf(notAMMarketed));
            }
        } catch (Exception ex) {
            setSeamlessCheckinForNotAMMarketedEnabled(DEFAULT_SEAMLESS_CHECKIN_FOR_NOT_AM_MARKETED_ENABLED);
        }

        try {
            String seamlessUrl = System.getProperty("seamless.checkin.api.url");

            if (null == seamlessUrl || seamlessUrl.isEmpty()) {
                setSeamlessCheckinUrl(DEFAULT_SEAMLESS_CHECKIN_URL);
            } else {
                setSeamlessCheckinUrl(seamlessUrl);
            }
        } catch (Exception ex) {
            setSeamlessCheckinUrl(DEFAULT_SEAMLESS_CHECKIN_URL);
        }

        try {
            String user = System.getProperty("seamless.checkin.api.user");

            if (null != user  && !user.trim().isEmpty()) {
                setSeamlessCheckinUser(user);
            } else {
                setSeamlessCheckinUser(DEFAULT_SEAMLESS_CHECKIN_USER);
            }
        } catch (NumberFormatException ex) {
            setSeamlessCheckinUser(DEFAULT_SEAMLESS_CHECKIN_USER);
        }

        try {
            String pass = System.getProperty("seamless.checkin.api.password");

            if (null != pass && !pass.trim().isEmpty()) {
                setSeamlessCheckinPass(pass);
            } else {
                setSeamlessCheckinPass(DEFAULT_SEAMLESS_CHECKIN_PASS);
            }
        } catch (NumberFormatException ex) {
            setSeamlessCheckinPass(DEFAULT_SEAMLESS_CHECKIN_PASS);
        }

        try {
            int value = Integer.valueOf(System.getProperty("seamless.checkin.api.timeout"));

            if (value <= 0) {
                setSeamlessCheckinTimeout(DEFAULT_SEAMLESS_CHECKIN_TIMEOUT);
            } else {
                setSeamlessCheckinTimeout(value);
            }
        } catch (NumberFormatException ex) {
            setSeamlessCheckinTimeout(DEFAULT_SEAMLESS_CHECKIN_TIMEOUT);
        } catch (Exception e) {
            setSeamlessCheckinTimeout(DEFAULT_SEAMLESS_CHECKIN_TIMEOUT);
        }

        try {
            String dsUrl = System.getProperty("digital.signature.api.url");

            if (null == dsUrl || dsUrl.isEmpty()) {
                setDigitalSignatureUrl(DEFAULT_DIGITAL_SIGNATURE_URL);
            } else {
                setDigitalSignatureUrl(dsUrl);
            }
        } catch (Exception ex) {
            setDigitalSignatureUrl(DEFAULT_DIGITAL_SIGNATURE_URL);
        }

        try {
            String user = System.getProperty("digital.signature.api.user");

            if (null != user  && !user.trim().isEmpty()) {
                setDigitalSignatureUser(user);
            } else {
                setDigitalSignatureUser(DEFAULT_DIGITAL_SIGNATURE_USER);
            }
        } catch (NumberFormatException ex) {
            setDigitalSignatureUser(DEFAULT_DIGITAL_SIGNATURE_USER);
        }

        try {
            String pass = System.getProperty("digital.signature.api.password");

            if (null != pass && !pass.trim().isEmpty()) {
                setDigitalSignaturePass(pass);
            } else {
                setDigitalSignaturePass(DEFAULT_DIGITAL_SIGNATURE_PASS);
            }
        } catch (NumberFormatException ex) {
            setDigitalSignaturePass(DEFAULT_DIGITAL_SIGNATURE_PASS);
        }

        try {
            int value = Integer.valueOf(System.getProperty("digital.signature.api.timeout"));

            if (value <= 0) {
                setDigitalSignatureTimeout(DEFAULT_DIGITAL_SIGNATURE_TIMEOUT);
            } else {
                setDigitalSignatureTimeout(value);
            }
        } catch (NumberFormatException ex) {
            setDigitalSignatureTimeout(DEFAULT_DIGITAL_SIGNATURE_TIMEOUT);
        } catch (Exception e) {
            setDigitalSignatureTimeout(DEFAULT_DIGITAL_SIGNATURE_TIMEOUT);
        }

        try {
            String earlyCheckin = System.getProperty("early_checkin");

            if (null == earlyCheckin || earlyCheckin.isEmpty()) {
                earlyCheckinEnabled = DEFAULT_EARLY_CHECKIN_ENABLED;
            } else {
                earlyCheckinEnabled = Boolean.valueOf(earlyCheckin);
            }
        } catch (Exception ex) {
            earlyCheckinEnabled = DEFAULT_EARLY_CHECKIN_ENABLED;
        }

        try {
            String loggingEnabled = System.getProperty("logging_enabled");

            if (null == loggingEnabled || loggingEnabled.isEmpty()) {
                SystemVariablesUtil.loggingEnabled = DEFAULT_LOGGING_ENABLED;
            } else {
                SystemVariablesUtil.loggingEnabled = Boolean.valueOf(loggingEnabled);
            }
        } catch (Exception ex) {
            loggingEnabled = DEFAULT_LOGGING_ENABLED;
        }

        try {
            int value = Integer.valueOf(System.getProperty("early_chk_window_time_limit_in_hrs"));

            if (value < 0) {
                earlyCheckinWindowTimeLimitInHrs = DEFAULT_EARLY_CHK_WINDOW_TIME_LIMIT_IN_HRS;
            } else {
                earlyCheckinWindowTimeLimitInHrs = value;
            }
        } catch (NumberFormatException ex) {
            earlyCheckinWindowTimeLimitInHrs = DEFAULT_EARLY_CHK_WINDOW_TIME_LIMIT_IN_HRS;
        } catch (Exception e) {
            earlyCheckinWindowTimeLimitInHrs = DEFAULT_EARLY_CHK_WINDOW_TIME_LIMIT_IN_HRS;
        }

        try {
            String earlyS = System.getProperty("early_chk_s");

            if (null == earlyS || earlyS.isEmpty()) {
                earlyCheckinSeed = null;
            } else {
                earlyCheckinSeed = earlyS;
            }
        } catch (Exception ex) {
            earlyCheckinSeed = null;
        }

        try {
            String earlyU = System.getProperty("early_chk_u");

            if (null == earlyU || earlyU.isEmpty()) {
                earlyCheckinUser = null;
            } else {
                earlyCheckinUser = earlyU;
            }
        } catch (Exception ex) {
            earlyCheckinUser = null;
        }

        try {
            String earlyP = System.getProperty("early_chk_p");

            if (null == earlyP || earlyP.isEmpty()) {
                earlyCheckinPassword = null;
            } else {
                earlyCheckinPassword = AESEncryption.decrypt(earlyP, getEarlyCheckinSeed());
            }
        } catch (UnsupportedEncodingException | InvalidAlgorithmParameterException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException | DecoderException ex) {
            earlyCheckinPassword = null;
        }


        try {
            String checkInfant = System.getProperty("check_infant_on_basic_economy");

            if (null == checkInfant || checkInfant.isEmpty()) {
                checkChildrenOnBasicEnabled = DEFAULT_CHECK_CHILDREN_ON_BASIC_ENABLED;
            } else {
                checkChildrenOnBasicEnabled = Boolean.valueOf(checkInfant);
            }
        } catch (Exception ex) {
            checkChildrenOnBasicEnabled = DEFAULT_CHECK_CHILDREN_ON_BASIC_ENABLED;
        }

        try {
            String robotPassengerList = System.getProperty("robot_passengerlist_enabled");

            if (null == robotPassengerList || robotPassengerList.isEmpty()) {
                robotPassengerListEnabled = DEFAULT_ROBOT_PASSENGER_LIST_ENABLED;
            } else {
                robotPassengerListEnabled = Boolean.valueOf(robotPassengerList);
            }

        } catch (Exception ex) {
            robotPassengerListEnabled = DEFAULT_ROBOT_PASSENGER_LIST_ENABLED;
        }

        try {
            String path = System.getProperty("three_ds_redirect_path");

            if (null == path || path.isEmpty()) {
                threeDsRedirectPath = DEFAULT_3_DS_REDIRECT_PATH;
            } else {
                threeDsRedirectPath = path;
            }
        } catch (Exception ex) {
            threeDsRedirectPath = DEFAULT_3_DS_REDIRECT_PATH;
        }

        try {
            int value = Integer.valueOf(System.getProperty("pnr_lookup_log_expire_time_days"));

            if (value <= 0 ) {
                pnrLogExpireTimeInDays = DEFAULT_PNR_LOGS_EXPIRE_TIME_IN_DAYS;
            } else {
                pnrLogExpireTimeInDays = value;
            }
        } catch (Exception ex) {
            pnrLogExpireTimeInDays = DEFAULT_PNR_LOGS_EXPIRE_TIME_IN_DAYS;
        }

        try {
            String agentName = System.getProperty("chubb.service.chk.agent.name");

            if (null == agentName || agentName.isEmpty()) {
                setChubbAgentName(DEFAULT_CHUBB_AGENT_NAME);
            } else {
                setChubbAgentName(agentName);
            }
        } catch (Exception ex) {
            setChubbAgentName(DEFAULT_CHUBB_AGENT_NAME);
        }

        try {
            String agentCode = System.getProperty("chubb.service.chk.agent.code");

            if (null == agentCode || agentCode.isEmpty()) {
                setChubbAgentCode(DEFAULT_CHUBB_AGENT_CODE);
            } else {
                setChubbAgentCode(agentCode);
            }
        } catch (Exception ex) {
            setChubbAgentCode(DEFAULT_CHUBB_AGENT_CODE);
        }

        try {
            String agentInternalCode = System.getProperty("chubb.service.chk.agent.internalcode");

            if (null == agentInternalCode || agentInternalCode.isEmpty()) {
                setChubbAgentInternalCode(DEFAULT_CHUBB_AGENT_INTERNAL_CODE);
            } else {
                setChubbAgentInternalCode(agentInternalCode);
            }
        } catch (Exception ex) {
            setChubbAgentInternalCode(DEFAULT_CHUBB_AGENT_INTERNAL_CODE);
        }


        LOGGER.info("standByPaxLimitPercentage: {}", standByPaxLimitPercentage);
        LOGGER.info("tsaRequired: {}", tsaRequired);
        LOGGER.info("amTierBenefitsTurnOff: {}", amTierBenefitsTurnOff);
        LOGGER.info("dlTierBenefitsTurnOff: {}", dlTierBenefitsTurnOff);
        LOGGER.info("domesticCheckinWindowTimeInHrs: {}", domesticCheckinWindowTimeInHrs);
        LOGGER.info("internationalCheckinWindowTimeInHrs: {}", internationalCheckinWindowTimeInHrs);
        LOGGER.info("earlyCheckinWindowTimeLimitInHrs: {}", earlyCheckinWindowTimeLimitInHrs);
        LOGGER.info("addRevenuePaxToPriorityListEnabled: {}", addRevenuePaxToPriorityListEnabled);
        LOGGER.info("earlyCheckinEnabled: {}", earlyCheckinEnabled);
        LOGGER.info("checkChildrenOnBasicEnabled: {}", checkChildrenOnBasicEnabled);
        LOGGER.info("mobileBoardingPassTemplateName: {}", mobileBoardingPassTemplateName);
        LOGGER.info("mobileBoardingPassTmpPath: {}", mobileBoardingPassTmpPath);
        LOGGER.info("fareLogixSeatmapEnabled: {}", fareLogixSeatmapEnabled);
        LOGGER.info("robotPassengerListEnabled: {}", robotPassengerListEnabled);
        LOGGER.info("threeDsRedirectPath: {}", getThreeDsRedirectPath());

    }

    /**
     * @return the timaticEnabled
     */
    public static boolean isTimaticEnabled() {
        return timaticEnabled;
    }

    /**
     * @return the addRevenuePaxToPriorityListEnabled
     */
    public static boolean isAddRevenuePaxToPriorityListEnabled() {
        return addRevenuePaxToPriorityListEnabled;
    }

    /**
     * @return the earlyCheckinEnabled
     */
    public static boolean isEarlyCheckinEnabled() {
        return earlyCheckinEnabled;
    }

    public static boolean isLoggingEnabled() {
        return loggingEnabled;
    }

    /**
     * @return the earlyCheckinUser
     */
    public static String getEarlyCheckinUser() {
        return earlyCheckinUser;
    }

    /**
     * @return the earlyCheckinPassword
     */
    public static String getEarlyCheckinPassword() {
        return earlyCheckinPassword;
    }

    /**
     * @return the earlyCheckinSeed
     */
    public static String getEarlyCheckinSeed() {
        return earlyCheckinSeed;
    }

    /**
     * @return the standByPaxLimitPercentage
     */
    public static double getStandByPaxLimitPercentage() {
        return standByPaxLimitPercentage;
    }

    /**
     * @return the tsaRequired
     */
    public static boolean isTsaRequired() {
        return tsaRequired;
    }

    /**
     * @return the amTierBenefitsTurnOff
     */
    public static boolean isAmTierBenefitsTurnOff() {
        return amTierBenefitsTurnOff;
    }

    /**
     * @return the dlTierBenefitsTurnOff
     */
    public static boolean isDlTierBenefitsTurnOff() {
        return dlTierBenefitsTurnOff;
    }

    /**
     * @return the domesticCheckinWindowTimeInHrs
     */
    public static int getDomesticCheckinWindowTimeInHrs() {
        return domesticCheckinWindowTimeInHrs;
    }

    /**
     * @return the internationalCheckinWindowTimeInHrs
     */
    public static int getInternationalCheckinWindowTimeInHrs() {
        return internationalCheckinWindowTimeInHrs;
    }

    /**
     * @return the earlyCheckinWindowTimeLimitInHrs
     */
    public static int getEarlyCheckinWindowTimeLimitInHrs() {
        return earlyCheckinWindowTimeLimitInHrs;
    }



    /**
     * @return the checkChildrenOnBasicEnabled
     */
    public static boolean isCheckChildrenOnBasicEnabled() {
        return checkChildrenOnBasicEnabled;
    }

    /**
     * @return the mobileBoardingPassTemplateName
     */
    public static String getMobileBoardingPassTemplateName() {
        return mobileBoardingPassTemplateName;
    }

    /**
     * @return the mobileBoardingPassTmpPath
     */
    public static String getMobileBoardingPassTmpPath() {
        return mobileBoardingPassTmpPath;
    }

    public static boolean isFareLogixSeatmapEnabled() {
        return fareLogixSeatmapEnabled;
    }

    public static boolean isRobotPassengerListEnabled() {
        return robotPassengerListEnabled;
    }

    public static boolean isFareLogixUpgradeEnabled() {
        return fareLogixUpgradeEnabled;
    }

    public static boolean isBigQueryEnabled() {
        return bigQueryEnabled;
    }

    public static boolean isCreateBoardingPassesOnPurchaseEnabled() {
        return createBoardingPassesOnPurchaseEnabled;
    }

    public static boolean isCreateDownloadBoardingPassesLinkEnabled() {
        return createDownloadBoardingPassesLinkEnabled;
    }

    public static boolean isSeamlessCheckinGhEnabled() {
        return seamlessCheckinGhEnabled;
    }

    public static String getThreeDsRedirectPath() {
        return threeDsRedirectPath;
    }

    public static String getThreeDsDefaultReturnPath() {
        return threeDsDefaultReturnPath;
    }

    public static void setThreeDsDefaultReturnPath(String threeDsDefaultReturnPath) {
        SystemVariablesUtil.threeDsDefaultReturnPath = threeDsDefaultReturnPath;
    }

    public static String getThreeDsCancelReturnPath() {
        return threeDsCancelReturnPath;
    }

    public static void setThreeDsCancelReturnPath(String threeDsCancelReturnPath) {
        SystemVariablesUtil.threeDsCancelReturnPath = threeDsCancelReturnPath;
    }

    public static String getThreeDsApprovedReturnPath() {
        return threeDsApprovedReturnPath;
    }

    public static void setThreeDsApprovedReturnPath(String threeDsApprovedReturnPath) {
        SystemVariablesUtil.threeDsApprovedReturnPath = threeDsApprovedReturnPath;
    }

    public static String getThreeDsDeclinedReturnPath() {
        return threeDsDeclinedReturnPath;
    }

    public static void setThreeDsDeclinedReturnPath(String threeDsDeclinedReturnPath) {
        SystemVariablesUtil.threeDsDeclinedReturnPath = threeDsDeclinedReturnPath;
    }

    public static String getThreeDsExpiredReturnPath() {
        return threeDsExpiredReturnPath;
    }

    public static void setThreeDsExpiredReturnPath(String threeDsExpiredReturnPath) {
        SystemVariablesUtil.threeDsExpiredReturnPath = threeDsExpiredReturnPath;
    }

    public static String getThreeDsPendingReturnPath() {
        return threeDsPendingReturnPath;
    }

    public static void setThreeDsPendingReturnPath(String threeDsPendingReturnPath) {
        SystemVariablesUtil.threeDsPendingReturnPath = threeDsPendingReturnPath;
    }

    public static String getThreeDsErrorReturnPath() {
        return threeDsErrorReturnPath;
    }

    public static void setThreeDsErrorReturnPath(String threeDsErrorReturnPath) {
        SystemVariablesUtil.threeDsErrorReturnPath = threeDsErrorReturnPath;
    }

    public static boolean isThreeDsWebEnabled() {
        return threeDsWebEnabled;
    }

    public static boolean isThreeDsKioskEnabled() {
        return threeDsKioskEnabled;
    }

    public static boolean isThreeDsAppEnabled() {
        return threeDsAppEnabled;
    }

    public static int getPnrLogExpireTimeInDays() {
        return pnrLogExpireTimeInDays;
    }

    public static void setPnrLogExpireTimeInDays(int pnrLogExpireTimeInDays) {
        SystemVariablesUtil.pnrLogExpireTimeInDays = pnrLogExpireTimeInDays;
    }

    public static String getSeamlessCheckinUrl() {
        return seamlessCheckinUrl;
    }

    public static void setSeamlessCheckinUrl(String seamlessCheckinUrl) {
        SystemVariablesUtil.seamlessCheckinUrl = seamlessCheckinUrl;
    }

    public static boolean isSeamlessCheckinOpEnabled() {
        return seamlessCheckinOpEnabled;
    }

    public static void setSeamlessCheckinOpEnabled(boolean seamlessCheckinOpEnabled) {
        SystemVariablesUtil.seamlessCheckinOpEnabled = seamlessCheckinOpEnabled;
    }

    public static String getSeamlessCheckinUser() {
        return seamlessCheckinUser;
    }

    public static void setSeamlessCheckinUser(String seamlessCheckinUser) {
        SystemVariablesUtil.seamlessCheckinUser = seamlessCheckinUser;
    }

    public static String getSeamlessCheckinPass() {
        return seamlessCheckinPass;
    }

    public static void setSeamlessCheckinPass(String seamlessCheckinPass) {
        SystemVariablesUtil.seamlessCheckinPass = seamlessCheckinPass;
    }

    public static String getDigitalSignatureUrl() {
        return digitalSignatureUrl;
    }

    public static void setDigitalSignatureUrl(String digitalSignatureUrl) {
        SystemVariablesUtil.digitalSignatureUrl = digitalSignatureUrl;
    }

    public static String getDigitalSignatureUser() {
        return digitalSignatureUser;
    }

    public static void setDigitalSignatureUser(String digitalSignatureUser) {
        SystemVariablesUtil.digitalSignatureUser = digitalSignatureUser;
    }

    public static String getDigitalSignaturePass() {
        return digitalSignaturePass;
    }

    public static void setDigitalSignaturePass(String digitalSignaturePass) {
        SystemVariablesUtil.digitalSignaturePass = digitalSignaturePass;
    }

    public static boolean isValidateSeamlessCheckinMandatesEnabled() {
        return validateSeamlessCheckinMandatesEnabled;
    }

    public static void setValidateSeamlessCheckinMandatesEnabled(boolean validateSeamlessCheckinMandatesEnabled) {
        SystemVariablesUtil.validateSeamlessCheckinMandatesEnabled = validateSeamlessCheckinMandatesEnabled;
    }

    public static boolean isSignAllSeamlessCheckinBoardingPassesEnabled() {
        return signAllSeamlessCheckinBoardingPassesEnabled;
    }

    public static void setSignAllSeamlessCheckinBoardingPassesEnabled(boolean signAllSeamlessCheckinBoardingPassesEnabled) {
        SystemVariablesUtil.signAllSeamlessCheckinBoardingPassesEnabled = signAllSeamlessCheckinBoardingPassesEnabled;
    }

    public static boolean isForceVisaForSeamlessEnabled() {
        return forceVisaForSeamlessEnabled;
    }

    public static void setForceVisaForSeamlessEnabled(boolean forceVisaForSeamlessEnabled) {
        SystemVariablesUtil.forceVisaForSeamlessEnabled = forceVisaForSeamlessEnabled;
    }

    public static void setSeamlessCheckinGhEnabled(boolean seamlessCheckinGhEnabled) {
        SystemVariablesUtil.seamlessCheckinGhEnabled = seamlessCheckinGhEnabled;
    }

    public static int getSeamlessCheckinTimeout() {
        return seamlessCheckinTimeout;
    }

    public static void setSeamlessCheckinTimeout(int seamlessCheckinTimeout) {
        SystemVariablesUtil.seamlessCheckinTimeout = seamlessCheckinTimeout;
    }

    public static int getDigitalSignatureTimeout() {
        return digitalSignatureTimeout;
    }

    public static void setDigitalSignatureTimeout(int digitalSignatureTimeout) {
        SystemVariablesUtil.digitalSignatureTimeout = digitalSignatureTimeout;
    }

    public static boolean isSeamlessCheckinForNotAMMarketedEnabled() {
        return seamlessCheckinForNotAMMarketedEnabled;
    }

    public static void setSeamlessCheckinForNotAMMarketedEnabled(boolean seamlessCheckinForNotAMMarketedEnabled) {
        SystemVariablesUtil.seamlessCheckinForNotAMMarketedEnabled = seamlessCheckinForNotAMMarketedEnabled;
    }

    public static boolean isValidateSeamlessBoardingDocumentsLinkOnReprintEnabled() {
        return validateSeamlessBoardingDocumentsLinkOnReprintEnabled;
    }

    public static void setValidateSeamlessBoardingDocumentsLinkOnReprintEnabled(boolean validateSeamlessBoardingDocumentsLinkOnReprintEnabled) {
        SystemVariablesUtil.validateSeamlessBoardingDocumentsLinkOnReprintEnabled = validateSeamlessBoardingDocumentsLinkOnReprintEnabled;
    }

    public static boolean isValidateSeamlessBoardingDocumentsLinkOnFirstAttemptEnabled() {
        return validateSeamlessBoardingDocumentsLinkOnFirstAttemptEnabled;
    }

    public static void setValidateSeamlessBoardingDocumentsLinkOnFirstAttemptEnabled(boolean validateSeamlessBoardingDocumentsLinkOnFirstAttemptEnabled) {
        SystemVariablesUtil.validateSeamlessBoardingDocumentsLinkOnFirstAttemptEnabled = validateSeamlessBoardingDocumentsLinkOnFirstAttemptEnabled;
    }

    public static String getChubbAgentName() {
        return chubbAgentName;
    }

    public static void setChubbAgentName(String chubbAgentName) {
        SystemVariablesUtil.chubbAgentName = chubbAgentName;
    }

    public static String getChubbAgentCode() {
        return chubbAgentCode;
    }

    public static void setChubbAgentCode(String chubbAgentCode) {
        SystemVariablesUtil.chubbAgentCode = chubbAgentCode;
    }

    public static String getChubbAgentInternalCode() {
        return chubbAgentInternalCode;
    }

    public static void setChubbAgentInternalCode(String chubbAgentInternalCode) {
        SystemVariablesUtil.chubbAgentInternalCode = chubbAgentInternalCode;
    }
}
