package com.am.checkin.web.v2.services;

import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.CountryISO;
import com.aeromexico.commons.model.KioskProfiles;
import com.aeromexico.commons.model.OverBooking;
import com.aeromexico.commons.model.OverBookingConfigFlight;
import com.aeromexico.commons.model.OverBookingOffer;
import com.aeromexico.commons.model.PNR;
import com.aeromexico.commons.model.Segment;
import com.aeromexico.commons.model.corporate.CorporateRecognition;
import com.aeromexico.commons.model.rq.PnrRQ;
import com.aeromexico.commons.model.weather.AirportWeather;
import com.aeromexico.dao.qualifiers.CountryISOCache;
import com.aeromexico.dao.qualifiers.KioskProfileCache;
import com.aeromexico.dao.qualifiers.OverBookingCache;
import com.aeromexico.dao.qualifiers.OverBookingConfigCache;
import com.aeromexico.dao.services.ICountryISODao;
import com.aeromexico.dao.services.IKioskProfileDao;
import com.aeromexico.dao.services.IOverBookingConfigDao;
import com.aeromexico.dao.services.IOverBookingDao;
import com.am.checkin.web.util.PNRLookUpServiceUtil;
import com.am.checkin.web.v2.util.ReservationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Named
@ApplicationScoped
public class ReservationUtilService {

    private static final Logger LOG = LoggerFactory.getLogger(ReservationUtilService.class);

    @Inject
    @CountryISOCache
    private ICountryISODao countryISODao;

    @Inject
    @OverBookingCache
    private IOverBookingDao overBookingDao;

    @Inject
    @OverBookingConfigCache
    private IOverBookingConfigDao overBookingConfigDao;

    @Inject
    @KioskProfileCache
    private IKioskProfileDao kioskProfileDao;

    @Inject
    private AirportService airportService;

    public boolean isKioskOnTsaList(String kioskId) {
        try {
            //call cybersource
            KioskProfiles kioskProfiles = getKioskProfileDao().findProfile(kioskId);
            if (null == kioskProfiles) {
                return false;
            }
            String value = kioskProfiles.getAttributeValue("tsaRequired");
            if (null == value) {
                return false;
            }

            return Boolean.valueOf(value);
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);

            return false;
        }
    }


    /**
     * @param pnr
     * @param pnrRQ
     */
    public void addVolunteerOffer(PNR pnr, PnrRQ pnrRQ) {

        LOG.info("Checking volunteer eligibility for {}", pnr.getPnr());

        //Booking class is Economy
        //At most 2 passenger without INF or CHILD
        //The next segment is the final (no connection) segment.
        try {

            pnr.setVolunteerOffer(pnrRQ.isVolunteerOffer());

            String cartId = null;
            if (null != pnr.getCarts().getCollection() && pnr.getCarts().getCollection().size() > 0) {
                cartId = pnr.getCarts().getCollection().get(0).getMeta().getCartId();
            }

            CartPNR cartPNR = pnr.getCartPNRByCartId(cartId);
            BookedLeg bookedLeg = pnr.getBookedLegByCartId(cartId);
            if (null != cartPNR) {
                if (pnrRQ.isVolunteerOffer() && PNRLookUpServiceUtil.isEligibleForVolunteerOffer(cartPNR, bookedLeg)) {

                    if (getOverBookingConfigDao() != null && getOverBookingConfigDao().getOverBookingConfig() != null
                            && getOverBookingConfigDao().getOverBookingConfig().getOverBookingConfigFlightList() != null && getOverBookingConfigDao().getOverBookingConfig().getOverBookingConfigFlightList().size() > 0) {

                        LOG.info("A specific flight list defined for over booking: " + getOverBookingConfigDao().getOverBookingConfig().toString());
                        Segment segment = bookedLeg.getFirstOpenSegment().getSegment();
                        String flight = segment.getOperatingFlightCode();

                        if (isFlightInOverBookingList(flight) && ReservationUtil.isFlightOverBooked(segment)) {
                            doOverBookingOffer(pnr, bookedLeg);
                        } else {
                            //Update all passenger with false in the flag.
                            PNRLookUpServiceUtil.turnOffFlagPerPassngerOverBooking(cartPNR);
                        }

                    } else {
                        LOG.info("No specific flight list defined for over booking offer.");
                        if (ReservationUtil.isFlightOverBooked(bookedLeg.getFirstOpenSegment().getSegment())) {
                            doOverBookingOffer(pnr, bookedLeg);
                        } else {
                            //Update all passenger with false in the flag.
                            PNRLookUpServiceUtil.turnOffFlagPerPassngerOverBooking(cartPNR);
                        }
                    }

                } else {
                    //Update all passenger with false in the flag.
                    PNRLookUpServiceUtil.turnOffFlagPerPassngerOverBooking(cartPNR);
                }
            }
        } catch (Exception ex) {
            //Continue
            LOG.error("Volunteer eligibility failed: " + ex.getMessage(), ex);
        }
    }

    public boolean isFlightInOverBookingList(String flight) {

        //This should never happen. Just in case.
        if (flight == null) {
            return false;
        }

        try {
            if (getOverBookingConfigDao() == null
                    || getOverBookingConfigDao().getOverBookingConfig() == null
                    || getOverBookingConfigDao().getOverBookingConfig().getOverBookingConfigFlightList() == null
                    || getOverBookingConfigDao().getOverBookingConfig().getOverBookingConfigFlightList().isEmpty()) {
                return false;
            }
        } catch (Exception e) {
            //Ignore exception.
            return false;
        }

        if (flight.trim().length() == 3) {
            flight = "0" + flight;
        }

        try {

            for (OverBookingConfigFlight overBookingConfigFlight : getOverBookingConfigDao().getOverBookingConfig().getOverBookingConfigFlightList()) {
                if (flight.equalsIgnoreCase(overBookingConfigFlight.getFlight())) {
                    return true;
                }
            }

            return false;

        } catch (Exception e) {
            //Ignore exception
            return false;
        }
    }

    public void doOverBookingOffer(PNR pnr, BookedLeg bookedLeg) throws Exception {

        //DO offer
        List<OverBookingOffer> overBookingOfferList = getOverBookingDao().getAllOverBooking();
        AirportWeather originAirport = getAirportService().getAirportFromIata(bookedLeg.getFirstOpenSegment().getSegment().getDepartureAirport());
        AirportWeather arrivalAirport = getAirportService().getAirportFromIata(bookedLeg.getLatestSegmentInLeg().getSegment().getArrivalAirport());

        OverBooking overBooking = PNRLookUpServiceUtil.getOverBookingOffer(originAirport, arrivalAirport, overBookingOfferList);
        if (null != overBooking) {
            pnr.setOverBooking(overBooking);
        } else {
            LOG.info("OverBooking offer info is null");
        }
    }

    public CountryISO getIsoCountryByIso2(String iso2Country) throws Exception {

        if (null == iso2Country || iso2Country.isEmpty()) {
            return null;
        }

        return getCountryISODao().getCountryISOByIso2(iso2Country);
    }

    public CountryISO getIsoCountryByIso3(String iso3Country) throws Exception {

        if (null == iso3Country || iso3Country.isEmpty()) {
            return null;
        }

        return getCountryISODao().getCountryISOByIso3(iso3Country);
    }

    public String getIso3Country(String iso2Country) throws Exception {

        if (null == iso2Country || iso2Country.isEmpty()) {
            return null;
        }

        return getCountryISODao().getCountryISO3ByIso2(iso2Country);
    }

    public String getIso2Country(String iso3Country) throws Exception {

        if (null == iso3Country || iso3Country.isEmpty()) {
            return null;
        }

        return getCountryISODao().getCountryISO2ByIso3(iso3Country);
    }

    /**
     * @param bookedTraveler
     * @throws Exception
     */
    public void changeCountryCodesToIso2(BookedTraveler bookedTraveler) throws Exception {
        String countryCode;
        //Country Of Residence
        if (null != bookedTraveler.getCountryOfResidence()
                && bookedTraveler.getCountryOfResidence().length() == 3) {
            countryCode = getCountryISODao().getCountryISO2ByIso3(bookedTraveler.getCountryOfResidence());
            if (null != countryCode) {
                bookedTraveler.setCountryOfResidence(countryCode);
            }
        }

        if (null != bookedTraveler.getTravelDocument()) {
            if (null != bookedTraveler.getTravelDocument().getIssuingCountry()
                    && bookedTraveler.getTravelDocument().getIssuingCountry().length() == 3) {
                countryCode = getCountryISODao().getCountryISO2ByIso3(bookedTraveler.getTravelDocument().getIssuingCountry());
                if (null != countryCode) {
                    bookedTraveler.getTravelDocument().setIssuingCountry(countryCode);
                }
            }

            if (null != bookedTraveler.getTravelDocument().getNationality()
                    && bookedTraveler.getTravelDocument().getNationality().length() == 3) {
                countryCode = getCountryISODao().getCountryISO2ByIso3(bookedTraveler.getTravelDocument().getNationality());
                if (null != countryCode) {
                    bookedTraveler.getTravelDocument().setNationality(countryCode);
                }
            }
        }

        if (null != bookedTraveler.getVisaInfo()) {
            if (null != bookedTraveler.getVisaInfo().getApplicableCountryCode()
                    && bookedTraveler.getVisaInfo().getApplicableCountryCode().length() == 3) {
                countryCode = getCountryISODao().getCountryISO2ByIso3(bookedTraveler.getVisaInfo().getApplicableCountryCode());
                if (null != countryCode) {
                    bookedTraveler.getVisaInfo().setApplicableCountryCode(countryCode);
                }
            }
        }

        if (null != bookedTraveler.getTimaticInfo()) {
            if (null != bookedTraveler.getTimaticInfo().getResidencyCountry()
                    && bookedTraveler.getTimaticInfo().getResidencyCountry().length() == 3) {
                countryCode = getCountryISODao().getCountryISO2ByIso3(bookedTraveler.getTimaticInfo().getResidencyCountry());
                if (null != countryCode) {
                    bookedTraveler.getTimaticInfo().setResidencyCountry(countryCode);
                }
            }
        }

        if (null != bookedTraveler.getDestinationAddress()) {
            if (null != bookedTraveler.getDestinationAddress().getCountry()
                    && bookedTraveler.getDestinationAddress().getCountry().length() == 3) {
                countryCode = getCountryISODao().getCountryISO2ByIso3(bookedTraveler.getDestinationAddress().getCountry());
                if (null != countryCode) {
                    bookedTraveler.getDestinationAddress().setCountry(countryCode);
                }
            }
        }

        if (null != bookedTraveler.getInfant()) {
            //Country Of Residence
            if (null != bookedTraveler.getInfant().getCountryOfResidence()
                    && bookedTraveler.getInfant().getCountryOfResidence().length() == 3) {
                countryCode = getCountryISODao().getCountryISO2ByIso3(bookedTraveler.getInfant().getCountryOfResidence());
                if (null != countryCode) {
                    bookedTraveler.getInfant().setCountryOfResidence(countryCode);
                }
            }

            if (null != bookedTraveler.getInfant().getTravelDocument()) {
                if (null != bookedTraveler.getInfant().getTravelDocument().getIssuingCountry()
                        && bookedTraveler.getInfant().getTravelDocument().getIssuingCountry().length() == 3) {
                    countryCode = getCountryISODao().getCountryISO2ByIso3(bookedTraveler.getInfant().getTravelDocument().getIssuingCountry());
                    if (null != countryCode) {
                        bookedTraveler.getInfant().getTravelDocument().setIssuingCountry(countryCode);
                    }
                }

                if (null != bookedTraveler.getInfant().getTravelDocument().getNationality()
                        && bookedTraveler.getInfant().getTravelDocument().getNationality().length() == 3) {
                    countryCode = getCountryISODao().getCountryISO2ByIso3(bookedTraveler.getInfant().getTravelDocument().getNationality());
                    if (null != countryCode) {
                        bookedTraveler.getInfant().getTravelDocument().setNationality(countryCode);
                    }
                }
            }

            if (null != bookedTraveler.getInfant().getVisaInfo()) {
                if (null != bookedTraveler.getInfant().getVisaInfo().getApplicableCountryCode()
                        && bookedTraveler.getInfant().getVisaInfo().getApplicableCountryCode().length() == 3) {
                    countryCode = getCountryISODao().getCountryISO2ByIso3(bookedTraveler.getInfant().getVisaInfo().getApplicableCountryCode());
                    if (null != countryCode) {
                        bookedTraveler.getInfant().getVisaInfo().setApplicableCountryCode(countryCode);
                    }
                }
            }

            if (null != bookedTraveler.getInfant().getTimaticInfo()) {
                if (null != bookedTraveler.getInfant().getTimaticInfo().getResidencyCountry()
                        && bookedTraveler.getInfant().getTimaticInfo().getResidencyCountry().length() == 3) {
                    countryCode = getCountryISODao().getCountryISO2ByIso3(bookedTraveler.getInfant().getTimaticInfo().getResidencyCountry());
                    if (null != countryCode) {
                        bookedTraveler.getInfant().getTimaticInfo().setResidencyCountry(countryCode);
                    }
                }
            }

            if (null != bookedTraveler.getInfant().getDestinationAddress()) {
                if (null != bookedTraveler.getInfant().getDestinationAddress().getCountry()
                        && bookedTraveler.getInfant().getDestinationAddress().getCountry().length() == 3) {
                    countryCode = getCountryISODao().getCountryISO2ByIso3(bookedTraveler.getInfant().getDestinationAddress().getCountry());
                    if (null != countryCode) {
                        bookedTraveler.getInfant().getDestinationAddress().setCountry(countryCode);
                    }
                }
            }
        }
    }

    /**
     * @param bookedTraveler
     * @throws Exception
     */
    public void changeCountryCodesToIso3(BookedTraveler bookedTraveler) throws Exception {
        String countryCode;
        //Country Of Residence
        if (null != bookedTraveler.getCountryOfResidence()
                && bookedTraveler.getCountryOfResidence().length() == 2) {
            countryCode = getCountryISODao().getCountryISO3ByIso2(bookedTraveler.getCountryOfResidence());
            if (null != countryCode) {
                bookedTraveler.setCountryOfResidence(countryCode);
            }
        }

        if (null != bookedTraveler.getTravelDocument()) {
            if (null != bookedTraveler.getTravelDocument().getIssuingCountry()
                    && bookedTraveler.getTravelDocument().getIssuingCountry().length() == 2) {
                countryCode = getCountryISODao().getCountryISO3ByIso2(bookedTraveler.getTravelDocument().getIssuingCountry());
                if (null != countryCode) {
                    bookedTraveler.getTravelDocument().setIssuingCountry(countryCode);
                }
            }

            if (null != bookedTraveler.getTravelDocument().getNationality()
                    && bookedTraveler.getTravelDocument().getNationality().length() == 2) {
                countryCode = getCountryISODao().getCountryISO3ByIso2(bookedTraveler.getTravelDocument().getNationality());
                if (null != countryCode) {
                    bookedTraveler.getTravelDocument().setNationality(countryCode);
                }
            }
        }

        if (null != bookedTraveler.getVisaInfo()) {
            if (null != bookedTraveler.getVisaInfo().getApplicableCountryCode()
                    && bookedTraveler.getVisaInfo().getApplicableCountryCode().length() == 2) {
                countryCode = getCountryISODao().getCountryISO3ByIso2(bookedTraveler.getVisaInfo().getApplicableCountryCode());
                if (null != countryCode) {
                    bookedTraveler.getVisaInfo().setApplicableCountryCode(countryCode);
                }
            }
        }

        if (null != bookedTraveler.getTimaticInfo()) {
            if (null != bookedTraveler.getTimaticInfo().getResidencyCountry()
                    && bookedTraveler.getTimaticInfo().getResidencyCountry().length() == 2) {
                countryCode = getCountryISODao().getCountryISO3ByIso2(bookedTraveler.getTimaticInfo().getResidencyCountry());
                if (null != countryCode) {
                    bookedTraveler.getTimaticInfo().setResidencyCountry(countryCode);
                }
            }
        }

        if (null != bookedTraveler.getDestinationAddress()) {
            if (null != bookedTraveler.getDestinationAddress().getCountry()
                    && bookedTraveler.getDestinationAddress().getCountry().length() == 2) {
                countryCode = getCountryISODao().getCountryISO3ByIso2(bookedTraveler.getDestinationAddress().getCountry());
                if (null != countryCode) {
                    bookedTraveler.getDestinationAddress().setCountry(countryCode);
                }
            }
        }

        if (null != bookedTraveler.getInfant()) {
            //Country Of Residence
            if (null != bookedTraveler.getInfant().getCountryOfResidence()
                    && bookedTraveler.getInfant().getCountryOfResidence().length() == 2) {
                countryCode = getCountryISODao().getCountryISO3ByIso2(bookedTraveler.getInfant().getCountryOfResidence());
                if (null != countryCode) {
                    bookedTraveler.getInfant().setCountryOfResidence(countryCode);
                }
            }

            if (null != bookedTraveler.getInfant().getTravelDocument()) {
                if (null != bookedTraveler.getInfant().getTravelDocument().getIssuingCountry()
                        && bookedTraveler.getInfant().getTravelDocument().getIssuingCountry().length() == 2) {
                    countryCode = getCountryISODao().getCountryISO3ByIso2(bookedTraveler.getInfant().getTravelDocument().getIssuingCountry());
                    if (null != countryCode) {
                        bookedTraveler.getInfant().getTravelDocument().setIssuingCountry(countryCode);
                    }
                }

                if (null != bookedTraveler.getInfant().getTravelDocument().getNationality()
                        && bookedTraveler.getInfant().getTravelDocument().getNationality().length() == 2) {
                    countryCode = getCountryISODao().getCountryISO3ByIso2(bookedTraveler.getInfant().getTravelDocument().getNationality());
                    if (null != countryCode) {
                        bookedTraveler.getInfant().getTravelDocument().setNationality(countryCode);
                    }
                }
            }

            if (null != bookedTraveler.getInfant().getVisaInfo()) {
                if (null != bookedTraveler.getInfant().getVisaInfo().getApplicableCountryCode()
                        && bookedTraveler.getInfant().getVisaInfo().getApplicableCountryCode().length() == 2) {
                    countryCode = getCountryISODao().getCountryISO3ByIso2(bookedTraveler.getInfant().getVisaInfo().getApplicableCountryCode());
                    if (null != countryCode) {
                        bookedTraveler.getInfant().getVisaInfo().setApplicableCountryCode(countryCode);
                    }
                }
            }

            if (null != bookedTraveler.getInfant().getTimaticInfo()) {
                if (null != bookedTraveler.getInfant().getTimaticInfo().getResidencyCountry()
                        && bookedTraveler.getInfant().getTimaticInfo().getResidencyCountry().length() == 2) {
                    countryCode = getCountryISODao().getCountryISO3ByIso2(bookedTraveler.getInfant().getTimaticInfo().getResidencyCountry());
                    if (null != countryCode) {
                        bookedTraveler.getInfant().getTimaticInfo().setResidencyCountry(countryCode);
                    }
                }
            }

            if (null != bookedTraveler.getInfant().getDestinationAddress()) {
                if (null != bookedTraveler.getInfant().getDestinationAddress().getCountry()
                        && bookedTraveler.getInfant().getDestinationAddress().getCountry().length() == 2) {
                    countryCode = getCountryISODao().getCountryISO3ByIso2(bookedTraveler.getInfant().getDestinationAddress().getCountry());
                    if (null != countryCode) {
                        bookedTraveler.getInfant().getDestinationAddress().setCountry(countryCode);
                    }
                }
            }
        }
    }


    public ICountryISODao getCountryISODao() {
        return countryISODao;
    }

    public void setCountryISODao(ICountryISODao countryISODao) {
        this.countryISODao = countryISODao;
    }

    public IOverBookingDao getOverBookingDao() {
        return overBookingDao;
    }

    public void setOverBookingDao(IOverBookingDao overBookingDao) {
        this.overBookingDao = overBookingDao;
    }

    public IOverBookingConfigDao getOverBookingConfigDao() {
        return overBookingConfigDao;
    }

    public void setOverBookingConfigDao(IOverBookingConfigDao overBookingConfigDao) {
        this.overBookingConfigDao = overBookingConfigDao;
    }

    public IKioskProfileDao getKioskProfileDao() {
        return kioskProfileDao;
    }

    public void setKioskProfileDao(IKioskProfileDao kioskProfileDao) {
        this.kioskProfileDao = kioskProfileDao;
    }

    public AirportService getAirportService() {
        return airportService;
    }

    public void setAirportService(AirportService airportService) {
        this.airportService = airportService;
    }

}
