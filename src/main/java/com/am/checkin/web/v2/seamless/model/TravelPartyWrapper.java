package com.am.checkin.web.v2.seamless.model;

import com.am.seamless.checkin.common.models.TravelParty;
import com.google.gson.Gson;

import java.io.Serializable;

public class TravelPartyWrapper  implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Gson GSON = new Gson();

    private String transactionId;

    private TravelParty travelParty;

    public TravelPartyWrapper() {
    }

    public TravelPartyWrapper(String transactionId, TravelParty travelParty) {
        this.transactionId = transactionId;
        this.travelParty = travelParty;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public TravelParty getTravelParty() {
        return travelParty;
    }

    public void setTravelParty(TravelParty travelParty) {
        this.travelParty = travelParty;
    }

    public String toString() {
        return GSON.toJson(this);
    }
}
