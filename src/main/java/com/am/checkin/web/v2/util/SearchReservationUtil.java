package com.am.checkin.web.v2.util;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.web.types.ErrorType;
import com.aeromexico.commons.web.types.PosType;
import com.aeromexico.commons.web.util.Constants;
import com.aeromexico.dao.util.CommonUtil;
import com.sabre.webservices.triprecord.TripSearchRS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;

public class SearchReservationUtil {

    private static final Logger LOG = LoggerFactory.getLogger(SearchReservationUtil.class);

    public static void validateTripSearchResponse(TripSearchRS tripSearchRS) throws GenericException {

        if (null == tripSearchRS) {
            throw new GenericException(
                    Response.Status.BAD_REQUEST,
                    ErrorType.INTERNAL_PROVIDER_ERROR,
                    ErrorType.INTERNAL_PROVIDER_ERROR.getFullDescription() + " tripSearch response is null"
            );
        }

        String errors = "";
        if (null != tripSearchRS.getErrors() && null != tripSearchRS.getErrors().getError()) {
            StringBuilder errorMessage = new StringBuilder();

            for (com.sabre.webservices.triprecord.BaseTripResponse.Errors.Error error : tripSearchRS.getErrors().getError()) {
                errorMessage.append(error.getErrorMessage()).append(", ");
            }

            errors = errorMessage.toString();

            LOG.error(errors);
        }

        if (!"Success".equalsIgnoreCase(tripSearchRS.getSuccess()) || !errors.isEmpty()) {
            throw new GenericException(
                    Response.Status.BAD_REQUEST,
                    ErrorType.INTERNAL_PROVIDER_ERROR,
                    ErrorType.INTERNAL_PROVIDER_ERROR.getFullDescription() + errors
            );
        }
    }

    public static void validateTripSearchResponseResultsByFF(TripSearchRS tripSearchRS, String pos) throws GenericException {
        if (null == tripSearchRS.getReservationsList().getTotalResults()
                || tripSearchRS.getReservationsList().getTotalResults() <= 0) {
            LOG.debug("getTripSearchRS: 0 PNRs found.");
            if(PosType.WEB.equals(pos)){
                throw new GenericException(
                        Response.Status.BAD_REQUEST,
                        ErrorType.RESRVATIONS_NOT_FOUND_BY_FF_WEB,
                        ErrorType.RESRVATIONS_NOT_FOUND_BY_FF_WEB.getFullDescription()
                );
            }else{
                throw new GenericException(
                        Response.Status.BAD_REQUEST,
                        ErrorType.RESRVATIONS_NOT_FOUND_BY_FF,
                        ErrorType.RESRVATIONS_NOT_FOUND_BY_FF.getFullDescription()
                );
            }
        }
    }

    public static void validateTripSearchResponseResultsByDestinationFlight(TripSearchRS tripSearchRS, String pos) throws GenericException {
        if (null == tripSearchRS.getReservationsList().getTotalResults()
                || tripSearchRS.getReservationsList().getTotalResults() <= 0
        ) {
            LOG.info("getTripSearchRS: 0 PNRs found.");

            if (PosType.WEB.toString().equalsIgnoreCase(pos)) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.RESERVATION_NOT_FOUND_WEB, CommonUtil.getMessageByCode(Constants.CODE_050));
            } else {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.RESERVATION_NOT_FOUND, CommonUtil.getMessageByCode(Constants.CODE_050));
            }
        } else if (tripSearchRS.getReservationsList().getTotalResults() > 1){
            LOG.info("getTripSearchRS: 0 PNRs found.");

            if (PosType.WEB.toString().equalsIgnoreCase(pos)) {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.RESERVATION_NOT_FOUND_WEB, CommonUtil.getMessageByCode(Constants.CODE_050));
            } else {
                throw new GenericException(Response.Status.BAD_REQUEST, ErrorType.RESERVATION_NOT_FOUND, CommonUtil.getMessageByCode(Constants.CODE_050));
            }
        }
    }
}
