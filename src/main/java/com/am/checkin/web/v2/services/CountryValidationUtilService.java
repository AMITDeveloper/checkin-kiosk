package com.am.checkin.web.v2.services;

import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.dao.client.exception.MongoDisconnectException;
import com.aeromexico.dao.config.SetUpConfigFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Named
@ApplicationScoped
public class CountryValidationUtilService {

    private static final Logger LOG = LoggerFactory.getLogger(CountryValidationUtilService.class);

    public static List<String> USA_AIRPORTS;

    @Inject
    @Named("CheckInSetUpConfigFactoryWrapper")
    private SetUpConfigFactory setUpConfigFactory;

    public void setUsaAirports() {
        try {
            if (null == USA_AIRPORTS || USA_AIRPORTS.isEmpty()) {
                String listResult = setUpConfigFactory.getInstance().getUsaAirports();
                String[] airports = listResult.replace(" ", "").split(",");
                for (int i = 0; i < airports.length; i++) {
                    airports[i] = airports[i].toUpperCase();
                }
                USA_AIRPORTS = new ArrayList<>(Arrays.asList(airports));
            }
        } catch (MongoDisconnectException e) {
            LOG.error(e.getMessage(), e);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
    }

    public boolean isUsaOrigin(BookedLeg bookedLeg) {

        setUsaAirports();

        if (null != USA_AIRPORTS && !USA_AIRPORTS.isEmpty()) {
            for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {
                if (USA_AIRPORTS.contains(bookedSegment.getSegment().getDepartureAirport())) {
                    return true;
                }
            }

        } else {
            LOG.info("Usa airports is empty");
        }

        return false;
    }

    public boolean isUsaOrigin(BookedSegment bookedSegment) {

        setUsaAirports();

        if (null != USA_AIRPORTS && !USA_AIRPORTS.isEmpty()) {
            if (USA_AIRPORTS.contains(bookedSegment.getSegment().getDepartureAirport())) {
                return true;
            }
        } else {
            LOG.info("Usa airports is empty");
        }

        return false;
    }
}
