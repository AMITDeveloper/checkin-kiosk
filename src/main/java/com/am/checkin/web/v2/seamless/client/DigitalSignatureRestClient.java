package com.am.checkin.web.v2.seamless.client;

import com.am.checkin.web.v2.util.SystemVariablesUtil;
import org.apache.http.config.SocketConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.jboss.resteasy.client.jaxrs.BasicAuthentication;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.engines.ApacheHttpClient4Engine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.util.concurrent.TimeUnit;

@Named("DigitalSignatureRestClient")
@ApplicationScoped
public class DigitalSignatureRestClient {

    private static final Logger LOG = LoggerFactory.getLogger(DigitalSignatureRestClient.class);

    private ResteasyClient client = null;


    @PostConstruct
    public void init() {
    }

    public ResteasyClient getRestEasyClient() {

        if (null == client) {
            PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
            SocketConfig socketConfig = SocketConfig.custom().setSoTimeout(SystemVariablesUtil.getDigitalSignatureTimeout()).build();
            CloseableHttpClient httpClient = HttpClients
                    .custom()
                    //.setDefaultSocketConfig(socketConfig)
                    .setConnectionManager(cm)
                    .build();
            cm.setMaxTotal(200); // Increase max total connection to 200
            cm.setDefaultMaxPerRoute(20); // Increase default max connection per route to 20

            ApacheHttpClient4Engine engine = new ApacheHttpClient4Engine(httpClient);


            client = new ResteasyClientBuilder().httpEngine(engine)
                    .establishConnectionTimeout(SystemVariablesUtil.getDigitalSignatureTimeout(), TimeUnit.SECONDS)
                    .socketTimeout(SystemVariablesUtil.getDigitalSignatureTimeout(), TimeUnit.SECONDS).build()
                    .register(new BasicAuthentication(SystemVariablesUtil.getDigitalSignatureUser(), SystemVariablesUtil.getDigitalSignaturePass()));

        }

        return client;
    }
}
