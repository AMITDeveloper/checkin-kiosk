package com.am.checkin.web.v2.seamless.service;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeromexico.commons.model.CartPNR;
import com.am.checkin.web.v2.seamless.transformers.TransformToDS;
import com.am.seamless.checkin.common.models.TravelParty;
import com.am.seamless.checkin.common.models.request.UpdateMandatesRq;

@Named
@ApplicationScoped
public class SeamlessUpdateMandatesService {

	private static final Logger LOG = LoggerFactory.getLogger(SeamlessUpdateMandatesService.class);
	
	@Inject
    private SeamlessCheckinProxyService seamlessCheckinProxyService;

	public void sendMandates(CartPNR cartPNR) throws Exception {

        String transactionId = cartPNR.getMeta().getTransactionId();
        String travelPartyId = cartPNR.getMeta().getTravelPartyId();
		UpdateMandatesRq updateMandatesRq = TransformToDS.getUpdateMandatesRq(cartPNR);

		//call service
		TravelParty travelParty = this.seamlessCheckinProxyService.updateMandates(
				updateMandatesRq,
				travelPartyId,
				transactionId
		);
		LOG.info("Response: " + travelParty.toString());
		//build answer to return ??
	}
}
