package com.am.checkin.web.v2.model;

import org.hibernate.validator.constraints.NotEmpty;
import org.jboss.resteasy.annotations.Body;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class EarlyEncryptModelRQ {
    @Body
    @Valid
    @NotNull(message = "{body.request.notnull}")
    @NotEmpty
    private String request;

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }
}
