package com.am.checkin.web.sabreFacades;

import com.am.checkin.web.service.CheckInSetUpConfigFactoryWrapper;
import com.am.checkin.web.service.SabreRESTClientCheckIn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

public abstract class SabreDCServiceFacadeCheckIn {
    protected static final Logger log = LoggerFactory.getLogger(SabreDCServiceFacadeCheckIn.class);

    public static final String PRODUCTS_AIR_FLIGHT_SCHEDULES_ENDPOINT = "/products/air/flight/schedules";
    public static final String PRODUCTS_AIR_FLIGHT_STATUS_ENDPOINT = "/products/air/flight/status";

    @Inject
    protected SabreRESTClientCheckIn sabreRESTClientCheckIn;

    @Inject
    @Named("CheckInSetUpConfigFactoryWrapper")
    private CheckInSetUpConfigFactoryWrapper setUpConfigFactory;

    @PostConstruct
    public void init() {
        if (null == setUpConfigFactory) {
            setUpConfigFactory = new CheckInSetUpConfigFactoryWrapper();
        }
    }

}
