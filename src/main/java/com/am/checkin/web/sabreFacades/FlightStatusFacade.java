package com.am.checkin.web.sabreFacades;

import com.aeromexico.commons.flightStatus.model.FlightScheduleResult;
import com.aeromexico.commons.flightStatus.model.FlightStatusResult;
import com.aeromexico.commons.flightStatus.model.FlightStatusSearch;
import com.am.checkin.web.service.CheckInSetUpConfigFactoryWrapper;
import com.google.gson.Gson;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named
@ApplicationScoped
public class FlightStatusFacade extends SabreDCServiceFacadeCheckIn {
    
    private static final Logger log = LoggerFactory.getLogger(FlightStatusFacade.class);

    @Inject
    @Named("CheckInSetUpConfigFactoryWrapper")
    private CheckInSetUpConfigFactoryWrapper setUpConfigFactory;

    @PostConstruct
    public void init() {
        if (null == setUpConfigFactory) {
            setUpConfigFactory = new CheckInSetUpConfigFactoryWrapper();
        }
    }

    public FlightStatusResult getSabreFlightStatus(FlightStatusSearch flightStatusSearch, String storeParameter) throws Exception {
        FlightStatusResult flightStatusResult = null;

        Map<String, String> response = sabreRESTClientCheckIn.doGetRequest(
                "status",
                PRODUCTS_AIR_FLIGHT_STATUS_ENDPOINT,                
                flightStatusSearch,
                storeParameter);
        
        if (null != response && 200 == Integer.valueOf(response.get("status"))) {            
            flightStatusResult = new Gson().fromJson(
                    response.get("response"),
                    FlightStatusResult.class
            );
        }
        
        return flightStatusResult;
    }
    
    public FlightScheduleResult getSabreSchedules(FlightStatusSearch flightStatusSearch, String storeParameter) throws Exception {
        FlightScheduleResult flightScheduleResult = null;

        Map<String, String> response = sabreRESTClientCheckIn.doGetRequest(
                "schedules",
                PRODUCTS_AIR_FLIGHT_SCHEDULES_ENDPOINT,                
                flightStatusSearch,
                storeParameter);
        
        if (null != response && 200 == Integer.valueOf(response.get("status"))) {            
            try {
                flightScheduleResult = new Gson().fromJson(response.get("response"), FlightScheduleResult.class);
            } catch (Exception e){
                log.info(e.getMessage());
            }
        }
        
        return flightScheduleResult;
    }

}
