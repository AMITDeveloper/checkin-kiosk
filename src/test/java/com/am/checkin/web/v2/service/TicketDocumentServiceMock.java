package com.am.checkin.web.v2.service;

import java.io.InputStream;

import com.aeromexico.sharedservices.services.TicketDocumentService;
import com.am.checkin.web.util.CommonTestUtil;
import com.sabre.ns.ticketing.dc.GetTicketingDocumentRS;

public class TicketDocumentServiceMock extends TicketDocumentService {
	
    @Override
    public GetTicketingDocumentRS getTicketDocByRecordLocator(String recordLocator, String cartId) throws Exception {

    	InputStream ticketingDocumentIS = getClass().getResourceAsStream( recordLocator + "_Ticket.xml");
    	GetTicketingDocumentRS getTicketingDocumentRS = CommonTestUtil.getGetTicketingDocumentRS(ticketingDocumentIS);

        return getTicketingDocumentRS;
    }

}
