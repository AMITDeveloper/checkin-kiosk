package com.am.checkin.web.v2.service;

import java.io.InputStream;
import java.util.List;

import com.aeromexico.commons.model.rq.PnrRQ;
import com.aeromexico.sabre.api.session.common.ServiceCall;
import com.am.checkin.web.util.CommonTestUtil;
import com.am.checkin.web.v2.services.ReservationLookupService;
import com.sabre.webservices.pnrbuilder.getreservation.GetReservationRS;

public class ReservationLookupServiceMock extends ReservationLookupService {

	@Override
	public GetReservationRS getReservationRS(PnrRQ pnrRQ, List<ServiceCall> serviceCallList) throws Exception {

		InputStream travelItineraryIS = getClass().getResourceAsStream(pnrRQ.getRecordLocator() + "_GetReservationRS.xml");
		GetReservationRS getReservationRS = CommonTestUtil.getReservationRS(travelItineraryIS);

        return getReservationRS;
	}
}
