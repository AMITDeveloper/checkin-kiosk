package com.am.checkin.web.v2.service;

import com.am.checkin.web.v2.model.UpgradeKnowledge;
import com.am.checkin.web.v2.services.UpgradeEligibilityRuleService;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class UpgradeRuleExecutorTest {
    private static UpgradeEligibilityRuleService upgradeEligibilityRuleService;

    @BeforeClass
    public static void setup() {
        upgradeEligibilityRuleService = new UpgradeEligibilityRuleService();
        upgradeEligibilityRuleService.init();
    }


    @Test
    public void executeRules() {

        Map<String, UpgradeKnowledge> upgradeKnowledgeMap = new HashMap<>();

        UpgradeKnowledge upgradeKnowledge;

        //Apply for upgrade
        upgradeKnowledge = new UpgradeKnowledge();
        upgradeKnowledge.setNameRef("01.01");
        upgradeKnowledge.setPassengerId("FC7053340901");
        upgradeKnowledge.getFact().setPriorityCode("PSV");
        upgradeKnowledge.getFact().setTierLevel("GLD");
        upgradeKnowledge.getFact().setClassOfService("K");
        upgradeKnowledge.getFact().setDestination("MEX");
        upgradeKnowledge.getFact().setOrigin("HMO");
        upgradeKnowledgeMap.put("01.01", upgradeKnowledge);

        //Not apply for upgrade because of the class of service
        upgradeKnowledge = new UpgradeKnowledge();
        upgradeKnowledge.setNameRef("02.01");
        upgradeKnowledge.setPassengerId("FC7053340902");
        upgradeKnowledge.getFact().setPriorityCode("PSV");
        upgradeKnowledge.getFact().setTierLevel("GLD");
        upgradeKnowledge.getFact().setClassOfService("G");
        upgradeKnowledge.getFact().setDestination("MEX");
        upgradeKnowledge.getFact().setOrigin("HMO");
        upgradeKnowledgeMap.put("02.01", upgradeKnowledge);

        //Apply for upgrade
        upgradeKnowledge = new UpgradeKnowledge();
        upgradeKnowledge.setNameRef("03.01");
        upgradeKnowledge.setPassengerId("FC7053340903");
        upgradeKnowledge.getFact().setPriorityCode("PSV");
        upgradeKnowledge.getFact().setTierLevel("GLD");
        upgradeKnowledge.getFact().setClassOfService("U");
        upgradeKnowledge.getFact().setDestination("MEX");
        upgradeKnowledge.getFact().setOrigin("HMO");
        upgradeKnowledgeMap.put("03.01", upgradeKnowledge);

        //Not apply for upgrade because of the departure cuty
        upgradeKnowledge = new UpgradeKnowledge();
        upgradeKnowledge.setNameRef("04.01");
        upgradeKnowledge.setPassengerId("FC7053340904");
        upgradeKnowledge.getFact().setPriorityCode("PSV");
        upgradeKnowledge.getFact().setTierLevel("DLP");
        upgradeKnowledge.getFact().setClassOfService("U");
        upgradeKnowledge.getFact().setDestination("LHR");
        upgradeKnowledge.getFact().setOrigin("MEX");
        upgradeKnowledgeMap.put("04.01", upgradeKnowledge);

        upgradeEligibilityRuleService.evaluateRules(upgradeKnowledgeMap);

        Assert.assertTrue(upgradeKnowledgeMap.get("01.01").getResult().isApplyForUpgrade());
        Assert.assertFalse(upgradeKnowledgeMap.get("02.01").getResult().isApplyForUpgrade());
        Assert.assertTrue(upgradeKnowledgeMap.get("03.01").getResult().isApplyForUpgrade());
        Assert.assertFalse(upgradeKnowledgeMap.get("04.01").getResult().isApplyForUpgrade());
    }
}
