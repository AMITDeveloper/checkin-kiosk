package com.am.checkin.web.model;

import com.aeromexico.commons.model.CheckinConfirmation;
import com.aeromexico.commons.typeadapters.CheckinConfirmationJsonAdapter;
import com.am.checkin.web.util.CommonTestUtil;
import com.google.gson.stream.JsonReader;
import org.junit.Test;

import java.io.InputStream;
import java.io.StringReader;

import static org.junit.Assert.*;

public class CheckinConfirmationJsonAdapterTest {

    @Test
    public void testDeserialiceCheckinConfirmationJson() {
        CheckinConfirmationJsonAdapter checkinConfirmationJsonAdapter = new CheckinConfirmationJsonAdapter();

        try {
            InputStream in = getClass().getResourceAsStream("checkinConfirmation.json");
            String jsonString = CommonTestUtil.fileToString(in);
            assertNotNull(jsonString);

            JsonReader reader = new JsonReader(new StringReader(jsonString));
            CheckinConfirmation checkinConfirmation = checkinConfirmationJsonAdapter.read(reader);

            assertEquals("EYEGEE", checkinConfirmation.getBoardingPassId());
            //assertEquals("", checkinConfirmation.getCheckedInCarts().getCollection().get(0).getLegCode());
            assertEquals("", checkinConfirmation.getPosCode());
            //assertEquals("", checkinConfirmation.getCheckedInLegs().getCollection().get(0).getEstimatedDepartureTime());

        } catch (Exception ex) {
            ex.printStackTrace();
            fail(ex.getMessage());
        }
    }
}
