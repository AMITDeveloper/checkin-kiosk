package com.am.checkin.web.model.util;

import com.aeromexico.commons.typeadapters.CollectionAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializer;
import org.junit.Before;
import org.junit.BeforeClass;

import java.util.Collection;

/**
 *
 * @author adrian
 */
public abstract class CheckPojoStructureTest {

    public static Gson gson = null;

    private static void createGson() {
        if (null == gson) {
            gson = new GsonBuilder().registerTypeAdapter(Double.class, (JsonSerializer<Double>) (src, typeOfSrc, context) -> {
                if (src == src.longValue()) {
                    return new JsonPrimitive(src.longValue());
                }
                return new JsonPrimitive(src);
            })
                    .serializeNulls()
                    .registerTypeHierarchyAdapter(Collection.class, new CollectionAdapter())
                    .create();
        }
    }

    @BeforeClass
    public static void beforeClass() {
        createGson();
    }

    @Before
    public void setup() {
    }
}
