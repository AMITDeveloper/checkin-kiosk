
package com.am.checkin.web.model.rq;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import com.aeromexico.commons.model.rq.PnrRQ;
import org.junit.Test;

public class PnrRQTest {

	/**
	 * 
	 */
	@Test
	public void testUserAuth1() {
		PnrRQ pnrRQ = new PnrRQ();

		String userAuth = "PNR name=\"Gomez\",reservation=\"ABC123\"";

		try {
			pnrRQ.setUserAuth(userAuth);
		} catch (Exception e) {
			fail();
			e.printStackTrace();
		}

		assertNotNull(pnrRQ.getUserAuth());

		assertTrue( "Gomez".equalsIgnoreCase( pnrRQ.getLastName() ) );
		assertTrue( "ABC123".equalsIgnoreCase( pnrRQ.getRecordLocator() ) );
	}

	@Test
	public void testUserAuth2() {
		PnrRQ pnrRQ = new PnrRQ();

		String userAuth = "PNR name=\"Gomez\",ffp=\"AM\",ffp_number=\"123456789\"";

		try {
			pnrRQ.setUserAuth(userAuth);
		} catch (Exception e) {
			fail();
			e.printStackTrace();
		}

		assertNotNull(pnrRQ.getUserAuth());

		assertTrue( "Gomez".equalsIgnoreCase( pnrRQ.getLastName() ) );
		assertTrue( "AM".equalsIgnoreCase( pnrRQ.getFfProgram() ) );
		assertTrue( "123456789".equalsIgnoreCase( pnrRQ.getFfNumber() ) );
	}

	@Test
	public void testUserAuth3() {
		PnrRQ pnrRQ = new PnrRQ();

		String userAuth = "reservation=MWXLET ,name=MAHMUD  ";

		try {
			pnrRQ.setUserAuth(userAuth);
		} catch (Exception e) {
			fail();
			e.printStackTrace();
		}

		assertNotNull(pnrRQ.getUserAuth());

		assertTrue( "MAHMUD".equalsIgnoreCase( pnrRQ.getLastName() ) );
		assertTrue( "MWXLET".equalsIgnoreCase( pnrRQ.getRecordLocator() ) );
	}

	@Test
	public void testUserAuth4() {
		PnrRQ pnrRQ = new PnrRQ();

		String userAuth = "PNR name=\"wilde\",reservation=\"KCJTMV\"";

		try {
			pnrRQ.setUserAuth(userAuth);
		} catch (Exception e) {
			fail();
			e.printStackTrace();
		}

		assertNotNull(pnrRQ.getUserAuth());

		System.out.println( "Last Name: " + pnrRQ.getLastName() );
		System.out.println( "Record Locator: " + pnrRQ.getRecordLocator() );

		assertTrue( "wilde".equalsIgnoreCase( pnrRQ.getLastName() ) );
		assertTrue( "KCJTMV".equalsIgnoreCase( pnrRQ.getRecordLocator() ) );
	}

	@Test
	public void testUserAuth5() {
		PnrRQ pnrRQ = new PnrRQ();

		String userAuth = "PNR name=\"ABIMERHIMILLET\",ffp=\"AM\",ffp_number=\"265635003\"";
		try {
			pnrRQ.setUserAuth(userAuth);
		} catch (Exception e) {
			fail();
			e.printStackTrace();
		}

		assertNotNull(pnrRQ.getUserAuth());

		assertTrue( "ABIMERHIMILLET".equalsIgnoreCase( pnrRQ.getLastName() ) );
		assertTrue( "AM".equalsIgnoreCase( pnrRQ.getFfProgram() ) );
		assertTrue( "265635003".equalsIgnoreCase( pnrRQ.getFfNumber() ) );
	}

}
