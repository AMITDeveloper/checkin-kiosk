package com.am.checkin.web.model;

import com.aeromexico.commons.model.Metadata;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.Test;

public class MetadataTest {

    public static final String BUILD_VERSION = "1.0.0";

    @Test
    public void testRootAndMetadata() {

        Metadata metadata = new Metadata(BUILD_VERSION, "NA");

        metadata.addEndpoint("pnr", "/pnr?store={store}&pos={pos}", true);
        metadata.addEndpoint("shoppingCart", "/carts/{cartId}?store={store}&pos={pos}&language={language}", true);
        metadata.addEndpoint("ancillaries", "/ancillaries?store={storeCode}&coupon={couponCode}&cartId={cartId}&pos={posCode}", true);
        metadata.addEndpoint("formsOfPayment", "/payment-methods?store={store}&cartId={cartId}&pos={pos}", true);
        metadata.addEndpoint("purchaseOrder", "/po?store={store}&pos={pos}", true);

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(metadata);
        System.out.println(json);
    }

    @Test
    public void testJsonToXML() {

        Metadata metadata = new Metadata(BUILD_VERSION, "NA");

        metadata.addEndpoint("pnr", "/pnr?store={store}&pos={pos}", true);
        metadata.addEndpoint("shoppingCart", "/carts/{cartId}?store={store}&pos={pos}&language={language}", true);
        metadata.addEndpoint("ancillaries", "/ancillaries?store={storeCode}&coupon={couponCode}&cartId={cartId}&pos={posCode}", true);
        metadata.addEndpoint("formsOfPayment", "/payment-methods?store={store}&cartId={cartId}&pos={pos}", true);
        metadata.addEndpoint("purchaseOrder", "/po?store={store}&pos={pos}", true);

        String json = new Gson().toJson(metadata);
        System.out.println(json);

    }

}
