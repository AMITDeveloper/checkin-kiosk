
package com.am.checkin.web.model;

import com.aeromexico.commons.model.Ancillary;
import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.PNR;
import com.aeromexico.commons.model.PNRCollection;
import com.aeromexico.commons.model.PaidTravelerAncillary;
import com.aeromexico.commons.model.Segment;
import com.aeromexico.commons.model.TravelerAncillary;
import com.aeromexico.commons.web.types.BookedLegCheckinStatusType;
import com.aeromexico.commons.web.types.BookedLegFlightStatusType;
import com.aeromexico.commons.web.types.PaxType;
import com.am.checkin.web.util.CommonTestUtil;
import org.junit.Assert;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;

public class PNRCollectionTest {

    @Test
    public void testObjectCreation() {

        PNRCollection pnrCollection = new PNRCollection();

        //Create the PNR object
        PNR pnr = new PNR();
        pnr.setPnr("ABCDEF");
        pnrCollection.getCollection().add(pnr);

        BookedLeg bookedLeg1 = new BookedLeg();
        bookedLeg1.setScheduledDepartureTime("2016-02-03T23:02:00");
        bookedLeg1.setEstimatedDepartureTime("2016-02-03T23:02:00");
        bookedLeg1.setFlightStatus(BookedLegFlightStatusType.ON_TIME);
        bookedLeg1.setBoardingGate("72");
        bookedLeg1.setCheckinStatus(BookedLegCheckinStatusType.OPEN);

        BookedSegment bookedSegment = new BookedSegment();
        bookedLeg1.getSegments().getCollection().add(bookedSegment);

        Segment segment = new Segment();
        segment.setAircraftType("788");
        segment.setSegmentCode("MEX_CUN_AM_2016-02-03_2302");
        segment.setDepartureAirport("MEX");
        segment.setArrivalAirport("CUN");
        segment.setLayoverToNextSegmentsInMinutes(0);
        segment.setFlightDurationInMinutes(54);
        segment.setDepartureDateTime("2016-02-03T23:02:00");
        segment.setArrivalDateTime("2016-02-03T23:56:00");
        segment.setOperatingCarrier("AM");
        segment.setOperatingFlightCode("360");
        segment.setMarketingCarrier("AM");
        segment.setMarketingFlightCode("360");
        segment.setIsPremierAvailable(true);
        bookedSegment.setSegment(segment);

        //Create a booked traveler
        BookedTraveler bookedTraveler = new BookedTraveler();
        bookedTraveler.setPaxType(PaxType.getType("ADULT"));
        bookedTraveler.setFirstName("MAHMUD");
        bookedTraveler.setLastName("FATAFTA");

        //Create an ancillary of Type Ancillary
        Ancillary ancillary = new Ancillary();
//		ancillary.setAddedToCartByDefault(false);
//		ancillary.setAppliesForIndividualLegs(false);
//		ancillary.setAppliesForIndividualPassengers(false);
        //ancillary.setCurrency();
        ancillary.setType("PRIORITY_ALL_STAR");

        //Create booked traveler ancillary
        TravelerAncillary bookedTravelerAncillary = new TravelerAncillary();

        //Add ancillary to booked traveler ancillary
        bookedTravelerAncillary.setQuantity(1);
        bookedTravelerAncillary.setAncillary(ancillary);

        //Add booked ancillary to booked traveler
        bookedTraveler.getAncillaries().getCollection().add(bookedTravelerAncillary);

        //Create an ancillary of Type PaidTravelerAncillary
        PaidTravelerAncillary paidAncillary = new PaidTravelerAncillary();
        paidAncillary.setType("PAID_ANCILLARY");

        //Create booked traveler ancillary
        //bookedTravelerAncillary = new PaidTravelerAncillary();
        //Add ancillary to booked traveler ancillary
        //bookedTravelerAncillary.setQuantity(1);
        //bookedTravelerAncillary.setAncillary(paidAncillary);
        //Add paid ancillary to booked traveler
        bookedTraveler.getAncillaries().getCollection().add(paidAncillary);

        //Create the carts
        CartPNR cartPNR = new CartPNR();
        cartPNR.setLegCode("MEX_CUN_AM_2016-02-03_2302");
        cartPNR.setTravelersCode("A1_C0_I0_PH0_PC0");

        //Add traveler to cart
        cartPNR.getTravelerInfo().getCollection().add(bookedTraveler);

        //Add cart to PNR
        pnr.getCarts().getCollection().add(cartPNR);

        //Add booked legs to PNR
        pnr.getLegs().getCollection().add(bookedLeg1);

        System.out.println(pnrCollection.toString());
    }

    //@Test
    public void testObjectDeserialization() throws Exception {

        String pnrCollectionString = CommonTestUtil.fileToString(PNRCollectionTest.class.getResource("pnrWithOfferAncillaries.json"));
        PNRCollection pnrCollection = new PNRCollection(pnrCollectionString);
        pnrCollection.setCreationDate(0);
        assertNotNull(pnrCollection);

        System.out.println("Original:    " + pnrCollectionString);
        System.out.println("Deserialzed: " + pnrCollection.toString());

        JSONAssert.assertEquals(pnrCollectionString, pnrCollection.toString(), false);
    }

    //@Test
    public void testObjectDeserialization2() {
        try {
            String pnrCollectionString = CommonTestUtil.fileToString(PNRCollectionTest.class.getResource("pnrWithOfferAncillaries.json"));
            PNRCollection pnrCollection = new PNRCollection(pnrCollectionString);
            pnrCollection.setCreationDate(0);
            assertNotNull(pnrCollection);

            System.out.println("Oribinal:    " + pnrCollectionString);
            System.out.println("Deserialzed: " + pnrCollection.toString());

            JSONAssert.assertEquals(pnrCollectionString, pnrCollection.toString(), false);
        } catch (Exception ex) {
            Assert.fail("Should not throw exception");
        }
    }

}
