
package com.am.checkin.web.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.am.checkin.web.util.CommonTestUtil;
import org.junit.Before;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;

import com.aeromexico.commons.model.PNRCollection;
import com.aeromexico.dao.services.PNRLookupDao;


public class PNRLookupDaoTest {

	private PNRLookupDao pnrLookupDao;

	@Before
	public void setup() {
		pnrLookupDao = new PNRLookupDao();
	}

	/**
	 * 
	 * Adrian need to fix this test case.
	 * 
	 */
	//@Test
	public void testSavePNR() throws Exception {

		String pnrCollectionString = CommonTestUtil.fileToString( getClass().getResource("4ffeef9a-77dc-45f5-9011-55c2cb05109d2.json") );

		PNRCollection pnrCollection = new PNRCollection(pnrCollectionString);

		assertNotNull(pnrCollection);

		System.out.println( "CartId: " + pnrCollection.getCollection().get(0).getCarts().getCollection().get(0).getMeta().getCartId() );

		//Save PNR collection to database
		pnrLookupDao.savePNRCollection(pnrCollection);

		//Test reading PNR collection from database using PNR record locator
		pnrCollection = pnrLookupDao.readPNRCollectionByRecordLocator(pnrCollection.getCollection().get(0).getPnr());

		//Compare the original string from the file to the PNR collection from the database
		System.out.println( pnrCollectionString );
		System.out.println( pnrCollection.toString() );

		assertTrue( pnrCollectionString.equalsIgnoreCase( pnrCollection.toString() ) );


		//Test reading PNR by record locator cart Id
		pnrCollection = pnrLookupDao.readPNRCollectionByCartId( pnrCollection.getCollection().get(0).getCarts().getCollection().get(0).getMeta().getCartId() );
		assertNotNull( pnrCollection );

		//Compare the original string from the file to the PNR collection from the database
		System.out.println( pnrCollectionString );
		System.out.println( pnrCollection.toString() );

		JSONAssert.assertEquals(pnrCollectionString, pnrCollection.toString(), false);
	}

}
