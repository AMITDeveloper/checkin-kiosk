package com.am.checkin.web.dao;

import java.util.ArrayList;
import java.util.List;

import com.aeromexico.commons.model.CatAirportsDB;
import com.am.checkin.web.util.CommonTestUtil;
import com.google.gson.Gson;
import com.aeromexico.dao.services.IDomesticAirportsDao;

public class domesticAirportsDaoMock implements IDomesticAirportsDao {

    private CatAirportsDBCollection catAirportsDBCollection = null;

    public domesticAirportsDaoMock() {
        try {
            String catAirportsString = CommonTestUtil.fileToString(domesticAirportsDaoMock.class.getResourceAsStream("CatAirports.json"));
            catAirportsDBCollection = new Gson().fromJson(catAirportsString, CatAirportsDBCollection.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public CatAirportsDB getAirportCode(String airportCode) {
        return catAirportsDBCollection.find(airportCode);
    }

    class CatAirportsDBCollection {

        List<CatAirportsDB> collection = new ArrayList<CatAirportsDB>();

        CatAirportsDB find(String airportCode) {
            for (CatAirportsDB CatAirportsDB : collection) {
                if (CatAirportsDB.getIata().equalsIgnoreCase(airportCode)) {
                    return CatAirportsDB;
                }
            }
            return null;
        }
    }

}
