package com.am.checkin.web.dao;

import java.io.InputStream;
import java.util.Date;

import com.aeromexico.commons.model.AncillariesCollectionWrapper;
import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedLegCollection;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.PNRCollection;
import com.aeromexico.commons.model.SeatmapCollectionWrapper;
import com.aeromexico.dao.services.IPNRLookupDao;
import com.am.checkin.web.util.CommonTestUtil;
import java.util.List;

public class PNRLookupDaoMock implements IPNRLookupDao {

    private String resources = null;

    public PNRLookupDaoMock() {
        resources = "";
    }

    public PNRLookupDaoMock(String resources) {
        this.resources = resources;
    }

    @Override
    public int deleteFromPnrCollecion(String cartId) throws Exception {
        return 0;
    }

    @Override
    public int cleanPnrCollecion(Date beforeThat) throws Exception {
        return 0;
    }

    @Override
    public int cleanSeatMapCollecion(Date beforeThat) throws Exception {
        return 0;
    }

    @Override
    public int cleanAncillariesCollecion(Date beforeThat) throws Exception {
        return 0;
    }

    @Override
    public int cleanChubbAncillariesCollection(Date beforeThat) throws Exception {
        return 0;
    }
    
    @Override
    public int cleanPnrCollecion(List<String> idList) throws Exception {
        return 0;
    }

    @Override
    public int cleanPnrCollectionAfterCheckin(List<String> idList) throws Exception {
        return 0;
    }

    @Override
    public int cleanSeatMapCollecion(List<String> idList) throws Exception {
        return 0;
    }

    @Override
    public int cleanAncillariesCollecion(List<String> idList) throws Exception {
        return 0;
    }

    @Override
    public int cleanChubbAncillariesCollection(List<String> idList) throws Exception {
        return 0;
    }

    @Override
    public int cleanCollecion(Date beforeThat, String collection) throws Exception {
        return 0;
    }
    
    @Override
    public int cleanCollecion(List<String> idList, String collection) throws Exception {
        return 0;
    }

    @Override
    public int cleanCollecion(String key, String value, String collection) throws Exception {
        return 0;
    }

    @Override
    public void dropCollection(String collection) throws Exception {
        //
    }

    @Override
    public void clearCollection(String collection) throws Exception {
        //
    }

    @Override
    public String getLastDocument(String collection) throws Exception {
        return "";
    }

    @Override
    public String getLastDocument(String key, String value, String collection) throws Exception {
        return "";
    }

    @Override
    public int saveDocument(String document, String collection) throws Exception {
        return 0;
    }

    @Override
    public int saveOrUpdateDocument(String document, String key, String value, String collection) throws Exception {
        return 0;
    }

    @Override
    public boolean saveMultipleDocuments(String[] documents, String collection) throws Exception {
        return true;
    }

    @Override
    public int saveSeatMapCollection(SeatmapCollectionWrapper seatmapCollectionWrapper) throws Exception {
        return 0;
    }

    @Override
    public int saveAncillariesCollection(AncillariesCollectionWrapper ancillariesCollectionWrapper) throws Exception {
        return 0;
    }

    @Override
    public SeatmapCollectionWrapper readSeatMapCollectionByCartId(String cartId) throws Exception {
        InputStream in = getClass().getResourceAsStream(cartId + "_seatmap.json");
        return new SeatmapCollectionWrapper(CommonTestUtil.fileToString(in));
    }

    @Override
    public AncillariesCollectionWrapper readAncillariesCollectionByCartId(String cartId) throws Exception {
        InputStream in = getClass().getResourceAsStream(cartId + "_ancillaryOffer.json");
        return new AncillariesCollectionWrapper(CommonTestUtil.fileToString(in));
    }

    @Override
    public int savePNRCollection(PNRCollection pnrCollection) throws Exception {
        return 0;
    }

    @Override
    public int savePNRCollectionAfterCheckin(PNRCollection pnrCollection) throws Exception {
        return 0;
    }

    @Override
    public PNRCollection readPNRCollectionByRecordLocator(String recordLocator) throws Exception {
        return new PNRCollection(CommonTestUtil.fileToString(PNRLookupDaoMock.class.getResource(recordLocator + ".json")));
        //return PNRCollectionFactory.create( CommonTestUtil.fileToString( recordLocator + ".json" ) );
    }

    @Override
    public PNRCollection readPNRCollectionByRecordLocatorAfterCheckin(String recordLocator) throws Exception {
        return new PNRCollection(CommonTestUtil.fileToString(PNRLookupDaoMock.class.getResource(recordLocator + ".json")));
        //return PNRCollectionFactory.create( CommonTestUtil.fileToString( recordLocator + ".json" ) );
    }

    @Override
    public PNRCollection readPNRCollectionByCartId(String cartId) throws Exception {
        InputStream in = getClass().getResourceAsStream(resources + cartId + ".json");
        return new PNRCollection(CommonTestUtil.fileToString(in));
        //return PNRCollectionFactory.create( CommonTestUtil.fileToString( in ) );
    }

    @Override
    public BookedLeg getBookedLeg(String cartId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getClassOfService(String cartId) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BookedLegCollection getSegmentByCartId(String cartId) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CartPNR getCartPNRByCartId(String cardId) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

}
