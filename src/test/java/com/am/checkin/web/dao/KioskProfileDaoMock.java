
package com.am.checkin.web.dao;

import com.aeromexico.commons.model.KioskProfiles;
import com.aeromexico.dao.services.IKioskProfileDao;
import com.am.checkin.web.util.CommonTestUtil;
import com.google.gson.Gson;

import java.io.InputStream;
import java.util.List;

public class KioskProfileDaoMock implements IKioskProfileDao {

	@Override
	public KioskProfiles findProfile(String ip) throws Exception {
		InputStream in = getClass().getResourceAsStream(ip + ".json");
		String fileToString = CommonTestUtil.fileToString(in);
        return new Gson().fromJson(fileToString, KioskProfiles.class);
	}

	@Override
	public int saveKioskProfileInCollection(KioskProfiles profile) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<KioskProfiles> findListProfiles() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean deleteKioskProfileInCollection(String ip) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	

}
