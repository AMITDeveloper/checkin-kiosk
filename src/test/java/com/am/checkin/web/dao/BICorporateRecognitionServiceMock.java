package com.am.checkin.web.dao;

import com.aeromexico.commons.bi.model.BenefitsBI;
import com.aeromexico.sharedservices.services.CorporateRecognitionService;
import com.am.checkin.web.util.FileUtils;
import com.google.gson.Gson;

/**
 *
 * @author ahernandeza | Aeromexico
 */
public class BICorporateRecognitionServiceMock extends CorporateRecognitionService {

    @Override
    public String callPostLoginBIRestService() {
        return "Token aklssf4895i4jliryr85476n";
    }


    @Override
    public BenefitsBI callGetBenefitsBIService(String authorization, String spid) {
        System.out.println("callGetBenefitsBIService :: Authorization: " + authorization);
        System.out.println("callGetBenefitsBIService :: SPID: " + spid);
        String benefits = FileUtils.readFileAsString("/web/dao/corporateResponse.json");
        System.out.println("callGetBenefitsBIService :: benefits service response: " + benefits);
        BenefitsBI benefitsRS = new Gson().fromJson(benefits, BenefitsBI.class);
        return benefitsRS;
    }

}
