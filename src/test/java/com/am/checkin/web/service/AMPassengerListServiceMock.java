
package com.am.checkin.web.service;

import java.io.InputStream;

import com.aeromexico.sabre.api.acs.AMPassengerListService;
import com.am.checkin.web.util.CommonTestUtil;
import com.sabre.services.acs.bso.passengerlist.v3.ACSPassengerListRSACS;

public class AMPassengerListServiceMock extends AMPassengerListService {

	@Override
	public ACSPassengerListRSACS passengerList(String airlineCode, String flightNumber, String departureCity, String arrivalCity, String departureDate, String[] displayCodes) throws Exception {

		String fileName = airlineCode + "_" + flightNumber + "_" + departureCity + "_" + departureDate + ".xml";
        InputStream passengerListServiceIS = AMPassengerListServiceMock.class.getResourceAsStream( fileName );
        if (passengerListServiceIS == null) {
            System.out.println("**** Input file was not found: " + fileName);
            return null;
        }
        ACSPassengerListRSACS passengerListRS = CommonTestUtil.getACSPassengerListRSACS(passengerListServiceIS);
        return passengerListRS;
    }

}
