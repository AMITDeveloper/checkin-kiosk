package com.am.checkin.web.service;

import com.aeromexico.commons.web.types.PosType;
import com.am.checkin.web.dao.PNRLookupDaoMock;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.PNR;
import com.aeromexico.commons.model.PNRCollection;
import com.aeromexico.commons.web.types.BookedTravelerCheckinIneligibleReasonsType;
import com.aeromexico.dao.services.IPNRLookupDao;
import com.am.checkin.web.event.dispatcher.UpdatePNRCollectionAfterCheckinDispatcher;
import com.am.checkin.web.event.dispatcher.UpdatePNRCollectionDispatcher;
import java.util.HashSet;
import java.util.Set;

import com.am.checkin.web.util.PnrCollectionUtil;
import org.junit.Assert;
import org.junit.BeforeClass;

/**
 *
 * @author adrian
 */
public class RemoveMissingCodesTest {

    private static IPNRLookupDao pnrLookupDao;
    private static UpdatePNRCollectionDispatcher updatePNRCollectionDispatcher;
    private static UpdatePNRCollectionAfterCheckinDispatcher updatePNRCollectionAfterCheckinDispatcher;
    private static PnrCollectionService updatePnrCollectionService;

    @BeforeClass
    public static void init() {
        pnrLookupDao = new PNRLookupDaoMock();
        updatePNRCollectionDispatcher = new UpdatePNRCollectionDispatcher();
        updatePNRCollectionAfterCheckinDispatcher = new UpdatePNRCollectionAfterCheckinDispatcher();
        updatePnrCollectionService = new PnrCollectionService();
        updatePnrCollectionService.setPnrLookupDao(pnrLookupDao);
        updatePnrCollectionService.setUpdatePNRCollectionDispatcher(updatePNRCollectionDispatcher);
        updatePnrCollectionService.setUpdatePNRCollectionAfterCheckinDispatcher(updatePNRCollectionAfterCheckinDispatcher);
    }

    /*
     * Adrian need to fix this test.
     *  
     */
    //@Test
    public void testRemoveMissingCodes() throws Exception {
        String cartId = "920a37f1-65a2-41be-9a07-113eda294ff4";
        String pos = PosType.WEB.toString();
        Set<String> ineligibleReasons = new HashSet<>();
        ineligibleReasons.add(BookedTravelerCheckinIneligibleReasonsType.MISSING_GENDER.toString());
        //ineligibleReasons.add("MISSING_DOCUMENT_VERIFICATION");
        //ineligibleReasons.add("MISSING_TRAVEL_DOC");

        Set<BookedTraveler> passengersSuccessfulCheckedIn = new HashSet<>();
        BookedTraveler bookedTraveler = new BookedTraveler();
        bookedTraveler.setId("43082DAC0001");
        passengersSuccessfulCheckedIn.add(bookedTraveler);
//        bookedTraveler = new BookedTraveler();
//        bookedTraveler.setId("43082DAC0001");
//        passengersSuccessfulCheckedIn.add(bookedTraveler);
//        bookedTraveler = new BookedTraveler();
//        bookedTraveler.setId("43082DAC0001");
//        passengersSuccessfulCheckedIn.add(bookedTraveler);

        PNRCollection pnrCollection = updatePnrCollectionService.getPNRCollection(cartId, pos);
        PnrCollectionUtil.removeMissingInformation(pnrCollection.getCartPNRByCartId(cartId).getTravelerInfo().getCollection(), passengersSuccessfulCheckedIn, ineligibleReasons);

        for (PNR pnr : pnrCollection.getCollection()) {
            for (CartPNR cartPNR : pnr.getCarts().getCollection()) {
                if (cartPNR.getMeta().getCartId().equalsIgnoreCase(cartId)) {
                    for (BookedTraveler bt : cartPNR.getTravelerInfo().getCollection()) {
                        Assert.assertEquals(2, bt.getIneligibleReasons().size());
                    }
                }
            }
        }

    }
}
