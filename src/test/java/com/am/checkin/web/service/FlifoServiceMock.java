package com.am.checkin.web.service;

import com.aeromexico.sabre.api.flifo.FlifoService;
import com.aeromexico.sabre.api.session.common.ServiceCall;
import com.am.checkin.web.util.CommonUtilFlifo;
import com.sabre.webservices.sabrexml._2011._10.flifo.OTAAirFlifoRS;
import java.io.InputStream;
import java.util.List;

/**
 *
 * @author Franco
 */
public class FlifoServiceMock extends FlifoService {

    @Override
    public OTAAirFlifoRS getFlifo(String airlineCode, String flightNumber, String departureDateTime, List<ServiceCall> serviceCallList) throws Exception {
        InputStream otaAirFlifo = getClass().getResourceAsStream("AirFlifo_"+airlineCode+flightNumber+"_"+departureDateTime+".xml");
        OTAAirFlifoRS otaAirFlifoRS = CommonUtilFlifo.getOtaAirFlifoRS(otaAirFlifo);
        return otaAirFlifoRS;
    }
}
