package com.am.checkin.web.service;

import com.aeromexico.commons.model.FormOfPaymentCollection;
import com.aeromexico.commons.model.rq.PaymentMethodRQ;
import com.aeromexico.dao.test.EnvironmentTest;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;


import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class PaymentMethodsServiceTest extends EnvironmentTest  {

	@InjectMocks
	private PaymentMethodsService paymentMethodsService;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
                
                paymentMethodsService.setSetUpConfigFactory(new CheckInSetUpConfigFactoryWrapper());
	}

	@Test
	@Ignore
	public void testPaymentMethodsService() throws Exception {

		PaymentMethodRQ paymentMethodRQ = new PaymentMethodRQ();
		paymentMethodRQ.setStore("MX");
		paymentMethodRQ.setPos("WEB");

		assertNotNull(paymentMethodsService);

		FormOfPaymentCollection FormOfPaymentCollection = paymentMethodsService.getPaymentMethods(paymentMethodRQ);
		assertNotNull(FormOfPaymentCollection);

		assertTrue( FormOfPaymentCollection.getCollection().size() == 4 );
		System.out.println( FormOfPaymentCollection.toString() );
	}

}
