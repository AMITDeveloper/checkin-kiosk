package com.am.checkin.web.service;

import com.am.checkin.web.dao.BICorporateRecognitionServiceMock;
import com.aeromexico.commons.bi.model.BenefitsBI;
import com.aeromexico.commons.model.SystemPropertiesFactory;
import com.aeromexico.commons.model.corporate.CorporateRecognition;
import com.aeromexico.dao.test.EnvironmentTest;
import com.aeromexico.sharedservices.services.CorporateRecognitionService;

import com.am.checkin.web.util.FileUtils;
import com.google.gson.Gson;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;


public class CorporateRecognitionServiceTest extends EnvironmentTest {
    private CorporateRecognitionService corporateRecognitionService;

    @BeforeClass
    public static void setupProdSystemProperties() {
        System.setProperty("corporate.endpoint", "http://166.78.79.115:8443");
        System.setProperty("corporate.user", "ecommerce");
        System.setProperty("corporate.password", "password");
    }

    @Before
    public void setup() {
        corporateRecognitionService = new CorporateRecognitionService();
        SystemPropertiesFactory systemPropertiesFactory = new SystemPropertiesFactory();
        systemPropertiesFactory.getInstance();
        corporateRecognitionService.init();
    }

    @Ignore
    @Test
    public void BenefitsRS() {
        try {
            String benefits = FileUtils.readFileAsString("/web/dao/corporateResponse.json");
            System.out.println("Benefits response:" + benefits);
            BenefitsBI benefitsBI = new Gson().fromJson(benefits, BenefitsBI.class);
            assertNotNull(benefitsBI);
            System.out.println("BenefitsBI:" + benefitsBI);
        } catch (Exception e) {
            System.err.println("Error al parsear Benefits response.");
            e.printStackTrace();
        }
    }

    @Ignore
    @Test
    public void testLoginGetToken() {
        try {
            System.out.println(":: callPostLoginBIRestService ::");
            String authorization = corporateRecognitionService.callPostLoginBIRestService();
            assertNotNull(authorization);
            System.out.println("Authorization: " + authorization);
        } catch (Exception ex) {
            System.out.println("callPostLoginBIRestService: " + ex.getMessage());
            fail("Could not parse response");
        }
    }

    @Ignore
    @Test
    public void testBenefits() {
        final String SPID = "5000000000371";
        try {
            String authorization = corporateRecognitionService.callPostLoginBIRestService();
            assertNotNull(authorization);
            System.out.println("Authorization: " + authorization);
            BenefitsBI benefitsBI = corporateRecognitionService.callGetBenefitsBIService(authorization, SPID);
            assertNotNull(benefitsBI);
            System.out.println("BenefitsBI: " + benefitsBI.toString());
        } catch (Exception ex) {
            System.out.println("callPostLoginBIRestService: " + ex.getMessage());
            fail("Could not parse response");
        }
    }

    @Test
    public void callCorporateRecognitionBI() {
        BICorporateRecognitionServiceMock mockBI = new BICorporateRecognitionServiceMock();
        String spid = "5000000000317";
        String authorization = mockBI.callPostLoginBIRestService();
        CorporateRecognition corporateRecognition = null;

        if (null != authorization && !authorization.isEmpty()) {
            BenefitsBI benefitsRS = mockBI.callGetBenefitsBIService(authorization, spid);
            if (null != benefitsRS) {
                boolean isActivadedC = Boolean.parseBoolean(benefitsRS.getActivatedC() != null ? benefitsRS.getActivatedC() : "false");
                boolean isMessaginC = Boolean.parseBoolean(benefitsRS.getMessaginC() != null ? benefitsRS.getMessaginC() : "false");
                boolean iscoBrandingC = Boolean.parseBoolean(benefitsRS.getCoBrandingC() != null ? benefitsRS.getCoBrandingC() : "false");

                corporateRecognition = new CorporateRecognition();
                String message = null;
                if (isActivadedC && isMessaginC && iscoBrandingC) {
                    System.out.println("Mensaje Personalizado.");
                    String companyName = (benefitsRS.getCustomizedValueOneC() != null ? benefitsRS.getCustomizedValueOneC().trim() : "");
                    if (companyName.isEmpty()) {
                        companyName = null;
                    }
                    corporateRecognition.setCompanyName(companyName);

                    corporateRecognition.setLogo(
                            Boolean.parseBoolean(
                                    benefitsRS.getLogoOffOn() != null
                                            ? benefitsRS.getLogoOffOn()
                                            : "false")
                    );
                    message = "Aeromexico agradece a ti y a "
                            + (companyName)
                            + " por viajar con nosotros.";
                    System.out.println(message);
                } else if (isActivadedC && isMessaginC) {
                    System.out.println("Mensaje Generico.");
                    message = "Aeromexico te agradece a ti y a tu Empresa por viajar con nosotros.";
                    System.out.println(message);
                } else {
                    System.out.println("Sin accion.");
                }
                corporateRecognition.setCorporateMessage(message);
                // preferred seat
                corporateRecognition.setPreferredSeat(
                        null != benefitsRS.getPseatsc()
                                ? benefitsRS.getPseatsc()
                                : false);

                System.out.println(corporateRecognition.toString());
            }
        }

        assertNotNull(corporateRecognition);
    }
}
