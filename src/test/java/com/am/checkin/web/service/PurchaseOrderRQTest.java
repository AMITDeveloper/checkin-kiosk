package com.am.checkin.web.service;

import com.aeromexico.commons.model.PurchaseOrder;
import com.am.checkin.web.model.util.CheckPojoStructureTest;

import static org.junit.Assert.*;

import org.junit.Test;
import com.am.checkin.web.util.FileUtils;
import org.skyscreamer.jsonassert.JSONAssert;

/**
 * 
 * @author adrian
 */
public class PurchaseOrderRQTest extends CheckPojoStructureTest {

    @Test
    public void checkPurchaseOrder() throws Exception {
        String expected = FileUtils.readFileAsString("/web/model/purchaseOrder.json");

        //PurchaseOrder purchaseOrder = new Gson().fromJson(expected, PurchaseOrder.class);
        PurchaseOrder purchaseOrder = new PurchaseOrder(expected);

        JSONAssert.assertEquals(expected, gson.toJson(purchaseOrder), false); // Pass
        //JSONAssert.assertEquals(expected, pnr.toString(), false); // Pass        
    }

    @Test
    public void sanatizeDataTest() throws Exception {
        String data         = "CP^A^01B^CP^1C01^01W^021^03NOMBRE/NAME^06VUELO/FLIGHT^07CLASE^08FECHA/DATE^09DE/FROM^11OPERADO POR/OPERATED BY^12CONTROL^13A/TO^14SALA/GATE^18HORA/TIME^21FQTV:^3121MAR^32          ^34K ^37112  ^39    ^40   ^41              ^42   ^43               ^44GARDUNO DIAZ/ELEAZ^45    ^46        ^47    ^48        ^50GARDUNO DIAZ/ELEAZAR MR      ^60TIJUANA          ^64177 ^67AM ^70MEXICO CITY      ^80ZONA/ZONE^81ZONA^824       ^B0ASIENTO^B218B ^B3   ^C3-^C4-A4-^C5-^C616:25^G4ETICKET   ^I31392104474968^I40^K4AEROMEXICO          ^P0                     ^P1                     ^P2M1GARDUNO DIAZ/ELEAZAREIPUOIC TIJMEXAM 0177 080Y018B0112 162&lt;5321KR8080BAM                                        2A1392104474968 0                          N^P3M1GARDUNO DIAZ/ELEAZAREIPUOIC TIJMEXAM 0177 080Y018B0112 162&lt;5321KR8080BAM                                        2A1392104474968 0                          N^";
        String expectedData = "CP^A^01B^CP^1C01^01W^021^03NOMBRE/NAME^06VUELO/FLIGHT^07CLASE^08FECHA/DATE^09DE/FROM^11OPERADO POR/OPERATED BY^12CONTROL^13A/TO^14SALA/GATE^18HORA/TIME^21FQTV:^3121MAR^32          ^34K ^37112  ^39    ^40   ^41              ^42   ^43               ^44GARDUNO DIAZ/ELEAZ^45    ^46        ^47    ^48        ^50GARDUNO DIAZ/ELEAZAR MR      ^60TIJUANA          ^64177 ^67AM ^70MEXICO CITY      ^80ZONA/ZONE^81ZONA^824       ^B0ASIENTO^B218B ^B3   ^C3-^C4-A4-^C5-^C616:25^G4ETICKET   ^I31392104474968^I40^K4AEROMEXICO          ^P0                     ^P1                     ^P2M1GARDUNO DIAZ/ELEAZAREIPUOIC TIJMEXAM 0177 080Y018B0112 162>5321KR8080BAM                                        2A1392104474968 0                          N^P3M1GARDUNO DIAZ/ELEAZAREIPUOIC TIJMEXAM 0177 080Y018B0112 162>5321KR8080BAM                                        2A1392104474968 0                          N^";

        String generatedData = data.replaceAll("&lt;", ">");
        
        if ( expectedData.equals( generatedData )) {
        	System.out.println( "SUCCSS" );
        } else {
        	System.out.println( generatedData );
        	fail();
        }
    }

}
