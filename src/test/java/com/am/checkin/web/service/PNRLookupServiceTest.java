package com.am.checkin.web.service;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedSegment;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.Infant;
import com.aeromexico.commons.model.PNR;
import com.aeromexico.commons.model.PNRCollection;
import com.aeromexico.commons.model.Segment;
import com.aeromexico.commons.model.TravelerDocument;
import com.aeromexico.commons.model.VisaInfo;
import com.aeromexico.commons.model.rq.PnrRQ;
import com.aeromexico.dao.services.AirportCodesDao;
import com.aeromexico.dao.services.MarketFlightService;
import com.aeromexico.dao.services.PNRLookupDao;
import com.aeromexico.dao.test.EnvironmentTest;
import com.aeromexico.sabre.api.session.common.ServiceCall;
import com.aeromexico.service.AmenitiesServiceCheckInMyb;
import com.aeromexico.sharedservices.services.BaggageAllowanceService;
import com.aeromexico.sharedservices.services.TicketDocumentService;
import com.aeromexico.timezone.service.TimeZone;
import com.aeromexico.timezone.service.TimeZoneService;
import com.am.checkin.web.util.CommonTestUtil;
import com.am.checkin.web.util.TicketDocumentUtil;
import com.am.checkin.web.v2.service.ReservationLookupServiceMock;
import com.am.checkin.web.v2.service.TicketDocumentServiceMock;
import com.am.checkin.web.v2.services.AirportService;
import com.am.checkin.web.v2.services.ItineraryService;
import com.am.checkin.web.v2.util.SearchServiceUtil;
import com.sabre.ns.ticketing.dc.GetTicketingDocumentRS;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.InputStream;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@PrepareForTest(TimeZoneService.class)
public class PNRLookupServiceTest extends EnvironmentTest {

    @InjectMocks
    private PNRLookupService pnrLookupService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    public PNRCollection testPNRLookup(String recordLocator, String localTime, String pos, String store, String expectedResponse, String lastName) throws Exception {

    	PnrRQ pnrRQ = new PnrRQ();
        pnrRQ.setRecordLocator(recordLocator);
        pnrRQ.setPos(pos);
        pnrRQ.setStore(store);
        pnrRQ.setLastName(lastName);

        BaggageAllowanceService baggageAllowanceService = new BaggageAllowanceService();
        //baggageAllowanceService.setBaggageAllowanceDao(new BaggageAllowanceDaoMock());        
        pnrLookupService.setBaggageAllowanceService(baggageAllowanceService);
        pnrLookupService.setPassengerData(new AMPassengerDataServiceMock());
        FlightDetailsService flightDetailsService = new FlightDetailsService();
        flightDetailsService.setFlightDetailService(new AMFlightDetailServiceMock());
        pnrLookupService.setFlightDetailsService(flightDetailsService);
        pnrLookupService.setPnrCollectionService(new PnrCollectionServiceMock());

        AirportCodesDao mockAirportsCodesDao = mock(AirportCodesDao.class);
        when(mockAirportsCodesDao.getAirportByIata(any())).thenReturn(new AirportMock("IAH", "USA"));
        when(mockAirportsCodesDao.getAirportWeatherByIata(any())).thenReturn(new AirportWeatherMock("IAH", "USA"));

        

        //TODO add ground handling Mock.
        //GroundHandlingDao mockGroundHandlingDao = mock (GroundHandlingDao.class);
        //pnrLookupService.setGroundHandling(mockGroundHandlingDao);
        TimeZone mockTimeZone = mock(TimeZone.class);
        when(mockTimeZone.getLocalTime()).thenReturn(ZonedDateTime.now());

        PowerMockito.mockStatic(TimeZoneService.class);
        PowerMockito.when(TimeZoneService.getTimeZone(any(), any(), any())).thenReturn(mockTimeZone);

        PNRLookupDao pnrLookupDao = new PNRLookupDao();
        pnrLookupDao.setDbAPI(testDBAPI);

        TicketDocumentService ticketDocumentService = new TicketDocumentServiceMock();
        ticketDocumentService.setTicketDocService(new AMTicketDocServiceMock());

        pnrLookupService.setReservationLookupService( new ReservationLookupServiceMock() );

        pnrLookupService.setTicketDocumentService(ticketDocumentService);

        ItineraryService itineraryService = new ItineraryService();
        itineraryService.setAmenitiesServiceCheckInMyb( new AmenitiesServiceCheckInMyb() );
        AirportService airportService = new AirportService();
        airportService.setAirportCodesDao( mockAirportsCodesDao );
        itineraryService.setAirportService( airportService );
        pnrLookupService.setItineraryService( itineraryService );

        pnrLookupService.setMarketFlightService( new MarketFlightService() );

        PNRCollection pnrCollection = null;

        try {
            List<ServiceCall> serviceCallList = new ArrayList<>();
            pnrCollection = pnrLookupService.lookUpReservation(pnrRQ, serviceCallList);
            if (null != pnrCollection) {
                System.out.println("ACTUAL RESPONSE:   " + pnrCollection.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }

        assertNotNull(pnrCollection);

        //Remove cartId since it changes every time pnr collection is created.
        for (PNR pnr : pnrCollection.getCollection()) {
            for (CartPNR cartPNR : pnr.getCarts().getCollection()) {
                cartPNR.getMeta().setCartId(null);
            }
        }

        String expectedJsonString = CommonTestUtil.fileToString(PNRLookupServiceTest.class.getResourceAsStream(expectedResponse));
        System.out.println("EXPECTED RESPONSE: " + expectedJsonString);
        assertNotNull(expectedJsonString);

        /*
        //JSONAssert.assertEquals(pnrCollection.toString(), expectedJsonString, false);
        for(BookedTraveler bookedTrav :pnrCollection.getCollection().get(0).getCarts().getCollection().get(0).getTravelerInfo().getCollection()) {
			for(AbstractAncillary bookedAnc : bookedTrav.getAncillaries().getCollection()) {
        		if (bookedAnc instanceof PaidTravelerAncillary) {
        			PaidTravelerAncillary ancillaries = (PaidTravelerAncillary) bookedAnc;
        			ancillaries.setStoredId(null);
        		}
        	}
			for(AbstractAncillary bookSeat : bookedTrav.getSeatAncillaries().getCollection()) {
				if (bookSeat instanceof PaidTravelerAncillary) {
        			PaidTravelerAncillary ancillaries = (PaidTravelerAncillary) bookSeat;
        			ancillaries.setStoredId(null);
        		}
			}
        }
        pnrCollection.setCreationDate(0);
        JSONAssert.assertEquals(expectedJsonString, pnrCollection.toString(), JSONCompareMode.LENIENT );
        //assertEquals(pnrCollection.toString(), expectedJsonString);
         */
        return pnrCollection;
    }

    @Test
    public void testDOCORedresParser() {
    	String str = "/K/985322190";
    	String expectedResult = "985322190";

    	BookedTraveler bookedTraveler = new BookedTraveler();

    	CommonServiceUtil.parseDOCO(bookedTraveler, str);

    	System.out.println( "Redress number: " + bookedTraveler.getKtn() );

    	if ( ! expectedResult.equals( bookedTraveler.getKtn() ) ) {
    		fail();
    	}
    }

    @Test
    public void testDOCOVisaParser() {
    	String str = "/V/1111/YYYY/11NOV17/MX";

    	VisaInfo expectedVisaInfo = new VisaInfo("MX", "YYYY", "2017-11-11", "1111", null);

    	BookedTraveler bookedTraveler = new BookedTraveler();

    	CommonServiceUtil.parseDOCO(bookedTraveler, str);

    	System.out.println( "Actual VISA Info:  " + bookedTraveler.getVisaInfo() );
    	System.out.println( "Expeced VISA Info: " + expectedVisaInfo );

    	if ( bookedTraveler.getVisaInfo() == null ) {
    		fail();
    	}

    	if ( ! expectedVisaInfo.equals( bookedTraveler.getVisaInfo() ) ) {
    		fail();
    	}
    }

    @Test
    public void testDOCOInfantVisaParser() {
    	String str = "/V/1111/YYYY/11NOV17/MX/I";

    	VisaInfo expectedVisaInfo = new VisaInfo("MX", "YYYY", "2017-11-11", "1111", null);

    	BookedTraveler bookedTraveler = new BookedTraveler();
    	bookedTraveler.setInfant( new Infant() );

    	CommonServiceUtil.parseDOCO(bookedTraveler, str);

    	System.out.println( "Actual VISA Info:  " + bookedTraveler.getInfant().getVisaInfo() );
    	System.out.println( "Expeced VISA Info: " + expectedVisaInfo );

    	if ( bookedTraveler.getInfant().getVisaInfo() == null ) {
    		fail();
    	}

    	if ( ! expectedVisaInfo.equals( bookedTraveler.getInfant().getVisaInfo() ) ) {
    		fail();
    	}
    }

    @Test
    public void testDOCS_DB_Adult_M_DDMMMYY_Parser() {
    	String str = "DB/25JUN68/M/ALNAJAR/SAMMY";

    	BookedTraveler bookedTraveler = new BookedTraveler();

    	CommonServiceUtil.parseDOCS(null, bookedTraveler, str);

    	if ( ! "1968-06-25".equals(bookedTraveler.getDateOfBirth() ) ) {
    		fail();
    	}

    	if ( ! "M".equals(bookedTraveler.getGender().getCode() ) ) {
    		fail();
    	}
    }

    @Test
    public void testDOCS_DB_Adult_F_DDMMMYYYY_Parser() {
    	String str = "DB/25JUN1968/F/ALNAJAR/SAMMY";

    	BookedTraveler bookedTraveler = new BookedTraveler();

    	CommonServiceUtil.parseDOCS(null, bookedTraveler, str);

    	if ( ! "1968-06-25".equals(bookedTraveler.getDateOfBirth() ) ) {
    		fail();
    	}

    	if ( ! "F".equals(bookedTraveler.getGender().getCode() ) ) {
    		fail();
    	}
    }

    @Test
    public void testDOCS_DB_Adult_F_Parser() {
    	String str = "DB//F/ALNAJAR/SAMMY";

    	BookedTraveler bookedTraveler = new BookedTraveler();

    	CommonServiceUtil.parseDOCS(null, bookedTraveler, str);

    	if ( equals(bookedTraveler.getDateOfBirth() != null ) ) {
    		fail();
    	}

    	if ( ! "F".equals(bookedTraveler.getGender().getCode() ) ) {
    		fail();
    	}
    }

    @Test
    public void testDOCS_DB_Parser() {
    	String str = "DB///ALNAJAR/SAMMY";

    	BookedTraveler bookedTraveler = new BookedTraveler();

    	CommonServiceUtil.parseDOCS(null, bookedTraveler, str);

    	if ( equals(bookedTraveler.getDateOfBirth() != null ) ) {
    		fail();
    	}

    	if ( equals(bookedTraveler.getGender() != null ) ) {
    		fail();
    	}
    }

    @Test
    public void testDOCS_DB_X_Parser() {
    	String str = "DB//X/ALNAJAR/SAMMY";

    	BookedTraveler bookedTraveler = new BookedTraveler();

    	CommonServiceUtil.parseDOCS(null, bookedTraveler, str);

    	if ( equals(bookedTraveler.getDateOfBirth() != null ) ) {
    		fail();
    	}

    	if ( equals(bookedTraveler.getGender() != null ) ) {
    		fail();
    	}
    }

    @Test
    public void testDOCS_DB_X_NO_NAME_Parser() {
    	String str = "DB//X//";

    	BookedTraveler bookedTraveler = new BookedTraveler();

    	CommonServiceUtil.parseDOCS(null, bookedTraveler, str);

    	if ( equals(bookedTraveler.getDateOfBirth() != null ) ) {
    		fail();
    	}

    	if ( equals(bookedTraveler.getGender() != null ) ) {
    		fail();
    	}
    }

    @Test
    public void testDOCS_DB_Infant_FI_DDMMMYYYY_Parser() {
    	String str = "DB/25JUN2018/FI/ALNAJAR/SAMMY";

    	BookedTraveler bookedTraveler = new BookedTraveler();

    	CommonServiceUtil.parseDOCS(null, bookedTraveler, str);

    	System.out.println( bookedTraveler.getInfant() );

    	if ( bookedTraveler.getInfant() == null ) {
    		fail();
    	}

    	if ( ! "F".equals(bookedTraveler.getInfant().getGender().getCode() ) ) {
    		fail();
    	}
    }

    @Test
    public void testDOCSParser() {
    	String str = "P/US/333345678909876/US/11JAN81/M/11JAN20/TEST/PASSENGER";

    	//Expected Passport
        TravelerDocument travelDocument = new TravelerDocument();
        travelDocument.setDocumentNumber( "333345678909876" );
        travelDocument.setIssuingCountry( "US" );
        travelDocument.setExpirationDate( "2020-01-11" );
        travelDocument.setPassportScanned( false);
        travelDocument.setNationality( "US" );

    	BookedTraveler bookedTraveler = new BookedTraveler();

    	CommonServiceUtil.parseDOCS(null, bookedTraveler, str);

    	System.out.println( "Actual passport Info : " + bookedTraveler.getTravelDocument() );
    	System.out.println( "Expeced passport Info: " + travelDocument );
    	System.out.println( "Date of birth: " + bookedTraveler.getDateOfBirth() );

    	if ( ! "1981-01-11".equals( bookedTraveler.getDateOfBirth() ) ) {
    		fail();
    	}

    	if ( ! bookedTraveler.getTravelDocument().toString().equals( travelDocument.toString() ) ) {
    		fail();
    	}
    }


    //@Test
    public void testPNR_MEITRE() throws Exception {
        String recordLocator = "WEZPOL";
        String localTime = "* 1549 18DEC 2020";
        String pos = "kiosk";
        String store = "mx";
        String expectedResponse = "PNRLookupTest/expectedPNRLookupResponse_MEITRE.json";

        testPNRLookup(recordLocator, localTime, pos, store, expectedResponse, null);
    }

    //@Test-
    public void testPNR_NTWUYE_Lookup() throws Exception {

        String recordLocator = "NTWUYE";
        String localTime = "* 1549 15MAY 2016";
        String pos = "kiosk";
        String store = "mx";
        String expectedResponse = "PNRLookupTest/expectedPNRLookupResponse_NTWUYE.json";

        testPNRLookup(recordLocator, localTime, pos, store, expectedResponse, null);
    }

    /*
	 * This test case is going to fail because of the properties that are null, assert equals is going to fail.
     */
    //@Test-
    public void testPNR_JBWLVY_Lookup() throws Exception {

        String recordLocator = "JBWLVY";
        String localTime = "* 1300 27MAR 2016";
        String pos = "kiosk";
        String store = "mx";
        String expectedResponse = "PNRLookupTest/expectedPNRLookupResponse_JBWLVY.json";

        testPNRLookup(recordLocator, localTime, pos, store, expectedResponse, null);
    }

    /**
     * This test case test for mapping booked leg info from travel itinerary for
     * one-way, one segment, one leg, direct flight.
     */
    //@Test-
    public void testPNR_QDGJRB_Lookup() throws Exception {

        String recordLocator = "QDGJRB";
        String localTime = "* 1300 27MAR 2016";
        String pos = "kiosk";
        String store = "mx";
        String expectedResponse = "PNRLookupTest/expectedPNRLookupResponse_QDGJRB.json";

        testPNRLookup(recordLocator, localTime, pos, store, expectedResponse, null);
    }

    /**
     * This test case test for mapping booked leg info from travel itinerary for
     * one-way, one leg, two segment (non-direct)
     */
    //@Test-
    public void testPNR_SVBJVO_Lookup() throws Exception {

        String recordLocator = "SVBJVO";
        String localTime = "* 1300 03APR 2016";
        String pos = "kiosk";
        String store = "mx";
        String expectedResponse = "PNRLookupTest/expectedPNRLookupResponse_SVBJVO.json";

        testPNRLookup(recordLocator, localTime, pos, store, expectedResponse, null);
    }

    //@Test
    public void testLivePNRLookupByVCR() throws Exception {

        String recordLocator = "1392194616598";
        String localTime = "* 1300 03APR 2016";
        String pos = "kiosk";
        String store = "mx";
        String expectedResponse = "PNRLookupTest/expectedPNRLookupResponse_SVBJVO.json";

        testPNRLookup(recordLocator, localTime, pos, store, expectedResponse, null);
    }

    /**
     *
     * PNR: UXOHGN VCR: 1392194667308
     *
     * one adult: HEATHROW/HELEN (E97CD0600001) 16-APR-1986 F HEATHROW/HELEN
     *
     * one infant 08-JUN-2015 MI HEATHROW/RYAN
     *
     * one segment, one leg, one-way
     *
     * domestic flight AM 0162 2016-05-27 12:00 pm MEX - CUN E90 booking class:
     * T Cabin Y
     *
     * @throws Exception
     *
     */
    //@Test-
    public void testPNR_UXOHGN_Lookup() throws Exception {

        //TravelItinerary_UXOHGN.xml
        String recordLocator = "UXOHGN";
        String localTime = "* 1300 26MAY 2016";
        String pos = "kiosk";
        String store = "mx";
        String expectedResponse = "PNRLookupTest/expectedPNRLookupResponse_UXOHGN.json";

        testPNRLookup(recordLocator, localTime, pos, store, expectedResponse, null);
    }

    //@Test-
    public void testPNR_VNDKNN_Lookup() throws Exception {

        //TravelItinerary_VNDKNN.xml
        String recordLocator = "VNDKNN";
        String localTime = "* 1300 06JUN 2016";
        String pos = "kiosk";
        String store = "MEX";
        String expectedResponse = "PNRLookupTest/expectedPNRLookupResponse_VNDKNN.json";

        testPNRLookup(recordLocator, localTime, pos, store, expectedResponse, null);
    }

    //@Test-
    public void testPNR_ZGKBPJ_Lookup() throws Exception {

        //TravelItinerary_ZGKBPJ.xml
        String recordLocator = "ZGKBPJ";
        String localTime = "* 1300 06JUN 2016";
        String pos = "kiosk";
        String store = "MX";
        String expectedResponse = "PNRLookupTest/expectedPNRLookupResponse_ZGKBPJ.json";

        testPNRLookup(recordLocator, localTime, pos, store, expectedResponse, null);
    }

    //@Test
    public void testPNR_YXJLJX_Lookup() throws Exception {

        //Flight - two segments - first one on Time -  second flight on Future
        String recordLocator = "YXJLJX";
        String localTime = "* 1100 13JUN 2016";
        String pos = "kiosk";
        String store = "MX";
        String expectedResponse = "PNRLookupTest/expectedPNRLookupResponse_YXJLJX.json";

        testPNRLookup(recordLocator, localTime, pos, store, expectedResponse, null);
    }

    //@Test-
    public void testPNR_HGCQDS_Lookup() throws Exception {

        //Flight - two segments - first one flight on Future -  second one flight on Future
        //FLIGHT NOT INITIALIZED 
        String recordLocator = "HGCQDS";
        String localTime = "* 1100 13JUN 2016";
        String pos = "kiosk";
        String store = "MX";
        String expectedResponse = "PNRLookupTest/expectedPNRLookupResponse_HGCQDS.json";

        testPNRLookup(recordLocator, localTime, pos, store, expectedResponse, null);
    }

    //@Test-
    public void testPNR_MGNUZR_Lookup() throws Exception {

        //Flight - Unpaid Seat.
        String recordLocator = "MGNUZR";
        String localTime = "* 1300 06JUN 2016";
        String pos = "kiosk";
        String store = "MX";
        String expectedResponse = "PNRLookupTest/expectedPNRLookupResponse_MGNUZR.json";

        testPNRLookup(recordLocator, localTime, pos, store, expectedResponse, null);
    }

    /**
     *
     * @throws Exception
     */
    //@Test-
    public void testPNR_ALBFXU_Lookup() throws Exception {

        //TravelItinerary_UXOHGN.xml
        String recordLocator = "UXOHGN";
        String localTime = "* 1300 26MAY 2016";
        String pos = "kiosk";
        String store = "mx";
        String expectedResponse = "PNRLookupTest/expectedPNRLookupResponse_UXOHGN.json";

        testPNRLookup(recordLocator, localTime, pos, store, expectedResponse, null);
    }

    /**
     *
     * @throws Exception
     */
//    @Test
//    public void testPNR_NJFIXW_Lookup() throws Exception {
//
//        //TravelItinerary_UXOHGN.xml
//        String recordLocator = "NJFIXW";
//        String localTime = "* 1300 20DEC 2016";
//        String pos = "kiosk";
//        String store = "mx";
//        String expectedResponse = "PNRLookupTest/expectedPNRLookupResponse_UXOHGN.json";
//
//        testPNRLookup(recordLocator, localTime, pos, store, expectedResponse, null);
//    }
    //I comment this test becouse is not validating coupon. All test cases are parsing this value.
    //@Test
    public void testCouponNumberParsing() throws Exception {

        //TTY 
        String recordLocator = "NBWIET";
        String localTime = "* 2000 12DEC 2016";
        String pos = "kiosk";
        String store = "MX";
        String expectedResponse = "PNRLookupTest/expectedPNRLookupResponse_NBWIET.json";

        testPNRLookup(recordLocator, localTime, pos, store, expectedResponse, null);
    }

    //@Test-
    public void testCouponNumberParsing01() throws Exception {
        List<String> vcrList = new ArrayList<String>();
        vcrList.add("1392198707317");
        String specialServiceText = "AM HK1 LAPMEX2071V06JAN/1392198707317C2";
        String couponTransformed = pnrLookupService.getCouponFromTKNE(specialServiceText, vcrList);
        String couponExpected = "2";
        assertEquals(couponExpected, couponTransformed);
    }

    //@Test-
    public void testCouponNumberParsing02() throws Exception {
        List<String> vcrList = new ArrayList<String>();
        vcrList.add("2351167003171");
        String specialServiceText = "AM HK1 JFKMEX0409V10JAN.2351167003171C2";
        String couponTransformed = pnrLookupService.getCouponFromTKNE(specialServiceText, vcrList);
        String couponExpected = "2";
        assertEquals(couponExpected, couponTransformed);
    }

    //@Test-
    public void testCouponNumberParsing03() throws Exception {
        List<String> vcrList = new ArrayList<String>();
        vcrList.add("2351167003171");
        String specialServiceText = "AM HK1 JFKMEX0409V10JAN.2351167003171C2/170-171";
        String couponTransformed = pnrLookupService.getCouponFromTKNE(specialServiceText, vcrList);
        String couponExpected = "2";
        assertEquals(couponExpected, couponTransformed);
    }

    //@Test-
    public void testCouponNumberParsing04() throws Exception {
        List<String> vcrList = new ArrayList<String>();
        vcrList.add("1397913059277");
        String specialServiceText = "AM HK1 MEXMXL0198Q 16DEC/1397913059277C2";
        String couponTransformed = pnrLookupService.getCouponFromTKNE(specialServiceText, vcrList);
        String couponExpected = "2";
        assertEquals(couponExpected, couponTransformed);
    }

    //@Test-
    public void testCouponNumberParsing05() throws Exception {
        List<String> vcrList = new ArrayList<String>();
        vcrList.add("1397913059277");
        String specialServiceText = "AM HK1 MXLMEX0199L 05DEC/1397913059277C1";
        String couponTransformed = pnrLookupService.getCouponFromTKNE(specialServiceText, vcrList);
        String couponExpected = "1";
        assertEquals(couponExpected, couponTransformed);
    }

    //@Test-
    public void testCouponNumberParsing06() throws Exception {
        List<String> vcrList = new ArrayList<String>();
        vcrList.add("1397913059277");
        String specialServiceText = "AM HK1 MXLMEX0199L 05DEC/1397913059277C1";
        String couponTransformed = pnrLookupService.getCouponFromTKNE(specialServiceText, vcrList);
        String couponExpected = "1";
        assertEquals(couponExpected, couponTransformed);
    }

    //@Test-
    public void testPNR_XKUDGT_Lookup() throws Exception {

        //Flight - Unpaid Seat.
        String recordLocator = "XKUDGT";
        String localTime = "* 0633 18DEC 2016";
        String pos = "kiosk";
        String store = "MX";
        String expectedResponse = "PNRLookupTest/expectedPNRLookupResponse_XKUDGT.json";
        String xmlGetTicketingDocumentRS = "./XKUDGT_GetTicketingDocumentRS.xml";
        PNRCollection pnrCollection = testPNRLookup(recordLocator, localTime, pos, store, expectedResponse, null);

        setCouponNumber(pnrCollection, xmlGetTicketingDocumentRS);
    }

    //@Test-
    public void testPNR_DFQCTG_Lookup() throws Exception {

        //Flight - Unpaid Seat.
        String recordLocator = "DFQCTG";
        String localTime = "* 0633 18DEC 2016";
        String pos = "kiosk";
        String store = "MX";
        String expectedResponse = "PNRLookupTest/expectedPNRLookupResponse_DFQCTG.json";
        String xmlGetTicketingDocumentRS = "./DFQCTG_GetTicketingDocumentRS.xml";
        PNRCollection pnrCollection = testPNRLookup(recordLocator, localTime, pos, store, expectedResponse, null);

        setCouponNumber(pnrCollection, xmlGetTicketingDocumentRS);
    }

    //@Test-
    public void testPNR_ZYQFTX() throws Exception {
        String recordLocator = "ZYQFTX";
        String localTime = "* 0900 08FEB 2017";
        String pos = "kiosk";
        String store = "mx";
        String expectedResponse = "PNRLookupTest/expectedPNRLookupResponse_ZYQFTX.json";

        testPNRLookup(recordLocator, localTime, pos, store, expectedResponse, null);
    }

    //@Test
    //TODO: please review and fix this testCase. 
    public void testPNR_MRAKTN() throws Exception {
        String recordLocator = "MRAKTN";
        String localTime = "* 0900 10FEB 2017";
        String pos = "kiosk";
        String store = "mx";
        String expectedResponse = "PNRLookupTest/expectedPNRLookupResponse_ZYQFTX.json";

        testPNRLookup(recordLocator, localTime, pos, store, expectedResponse, null);
    }

    //@Test-
    public void testPNR_NMENRZ() throws Exception {
        String recordLocator = "NMENRZ";
        String localTime = "* 1100 12MAY 2017";
        String pos = "kiosk";
        String store = "mx";
        String expectedResponse = "PNRLookupTest/expectedPNRLookupResponse_NMENRZ.json";

        testPNRLookup(recordLocator, localTime, pos, store, expectedResponse, null);
    }

    //@Test-
    public void testPNR_PNR_Pattern() throws Exception {

        PnrRQ pnrRQ = new PnrRQ();
        pnrRQ.setRecordLocator("a1b1c2");
        pnrRQ.setPos("WEB");
        pnrRQ.setStore("mx");
        pnrRQ.setLastName("ANY");

        PNRLookupService pnrLookupService = new PNRLookupService();
        PnrRQ updatePnrRQ = null;

        try {
            updatePnrRQ = SearchServiceUtil.validateRequest(pnrRQ);
        } catch (GenericException ex) {
            System.out.println(ex.getMessage());
        }

        if (updatePnrRQ == null) {
            fail();
        } else {
            System.out.println(updatePnrRQ.prettyPrint());
        }
    }

    /**
     *
     * @param pnrCollection
     * @param xmlGetTicketingDocumentRS
     */
    private void setCouponNumber(PNRCollection pnrCollection, String xmlGetTicketingDocumentRS) {
        if (null != pnrCollection && null != pnrCollection.getCollection() && !pnrCollection.getCollection().isEmpty()) {
            GetTicketingDocumentRS ticketingDocumentRS = null;

            try {
                PNR pnr = pnrCollection.getCollection().get(0);

                for (CartPNR cartPNR : pnr.getCarts().getCollection()) {
                    BookedLeg bookedLeg = pnr.getBookedLegByLegCode(cartPNR.getLegCode());

                    for (BookedTraveler bookedTraveler : cartPNR.getTravelerInfo().getCollection()) {
                        for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {
                            if (null != bookedSegment.getSegment()) {
                                Segment segment = bookedSegment.getSegment();

                                String couponNumber;
                                if (null == segment.getCouponNumber() || segment.getCouponNumber().isEmpty()) {
                                    if (null == ticketingDocumentRS) {
                                        ticketingDocumentRS = getTicketingDocumentRS(xmlGetTicketingDocumentRS);
                                    }
                                    couponNumber = TicketDocumentUtil.getCoupon(
                                            ticketingDocumentRS, segment,
                                            bookedTraveler.getTicketNumber()
                                    );

                                    segment.setCouponNumber(couponNumber);

                                    System.out.println("Coupon number: " + couponNumber);
                                } else {
                                    System.out.println("Normal coupon number: " + segment.getCouponNumber());
                                }
                            } else {
                                System.out.println("Itinerary is null");
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                System.out.println("Error getting coupon: " + ex.getMessage());
            }

            System.out.println("ACTUAL RESPONSE:   " + pnrCollection.toString());
        } else {
            System.out.println("PNR not found");
        }
    }

    /**
     *
     * @param filename
     * @return
     */
    private GetTicketingDocumentRS getTicketingDocumentRS(String filename) {
        InputStream ticketingDocumentIS = PNRLookupServiceTest.class.getResourceAsStream(filename);

        GetTicketingDocumentRS ticketingDocumentRS = null;
        JAXBContext ctx = null;

        try {
            ctx = JAXBContext.newInstance(
                    com.aeromexico.sabre.api.ticketdoc.secext.ObjectFactory.class,
                    com.sabre.ns.ticketing.dc.ObjectFactory.class,
                    com.sabre.services.stl.v01.ObjectFactory.class
            );
        } catch (JAXBException e) {
            e.printStackTrace();
            return null;
        }

        try {
            Unmarshaller unmarshaller = ctx.createUnmarshaller();
            JAXBElement<GetTicketingDocumentRS> enhancedSeatMapJE;
            enhancedSeatMapJE = (JAXBElement<GetTicketingDocumentRS>) unmarshaller.unmarshal(ticketingDocumentIS);
            ticketingDocumentRS = enhancedSeatMapJE.getValue();
        } catch (JAXBException e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to parse: " + e.getMessage(), e);
        }

        return ticketingDocumentRS;
    }

    @Test
    public void testOverbookingPercent() {

        double booked = 148;
        double authorized = 144;

        double percent = ((booked - authorized) / authorized) * 100.0;

        if (percent > 3.0) {
            System.out.println("IS over booked by 3% " + percent);
        } else {
            System.out.println("NOT over booked by 3% " + percent);
        }
    }
}
