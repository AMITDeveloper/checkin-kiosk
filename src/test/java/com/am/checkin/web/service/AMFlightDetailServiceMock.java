
package com.am.checkin.web.service;

import java.io.InputStream;

import com.aeromexico.sabre.api.acs.AMFlightDetailService;
import com.aeromexico.sabre.api.session.common.ServiceCall;

import com.am.checkin.web.util.CommonTestUtil;
import com.sabre.services.acs.bso.flightdetail.v3.ACSFlightDetailRSACS;
import java.util.List;

public class AMFlightDetailServiceMock extends AMFlightDetailService {

	@Override
	public ACSFlightDetailRSACS flightDetail(String airlineCode, String flightNumber, String departureCity, String departureDate, List<ServiceCall> serviceCallList) throws Exception {
		String dataFileName = airlineCode.toUpperCase() + ( flightNumber.length() < 4 ? "0" + flightNumber : flightNumber ) + "_" + departureDate + ".xml";
		InputStream flightDetailsIS = AMFlightDetailServiceMock.class.getResourceAsStream( dataFileName );
		ACSFlightDetailRSACS flightDetailRS = CommonTestUtil.getACSFlightDetailRSACS(flightDetailsIS);
		return flightDetailRS;
	}

}
