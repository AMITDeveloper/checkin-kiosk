package com.am.checkin.web.service;

import com.aeromexico.commons.model.rq.PnrRQ;
import com.aeromexico.sabre.api.session.common.ServiceCall;
import com.am.checkin.web.util.CommonTestUtil;
import com.am.checkin.web.v2.services.TravelItineraryService;
import com.sabre.services.res.tir.travelitinerary.TravelItineraryReadRS;

import java.io.InputStream;
import java.util.List;

public class TravelItineraryServiceMock extends TravelItineraryService {

    @Override
    public TravelItineraryReadRS getTravelItineraryReadRS(PnrRQ pnrRQ, List<ServiceCall> serviceCallList) throws Exception {
        InputStream travelItineraryIS = getClass().getResourceAsStream(pnrRQ.getRecordLocator() + "_TravelItineraryReadRS.xml");
        TravelItineraryReadRS travelItineraryReadRS = CommonTestUtil.getTravelItineraryReadRS(travelItineraryIS);
        return travelItineraryReadRS;
    }
}
