package com.am.checkin.web.service;

import com.aeromexico.commons.model.Warning;
import com.aeromexico.commons.model.WarningCollection;
import com.aeromexico.sabre.sabre.api.issuebagtag.AMIssueBagTagService;
import com.aeromexico.sabre.sabre.api.issuebagtag.model.BagRouteInfo;
import com.aeromexico.sabre.sabre.api.issuebagtag.model.IssueBagReq;
import com.google.gson.JsonParseException;
import com.sabre.services.checkin.issuebagtag.v4.IssueBagTagRSACS;
import com.sabre.services.stl.v3.CompletionCodes;
import com.sabre.services.stl.v3.ErrorMessage;
import com.sabre.services.stl.v3.ErrorOrSuccessCode;
import com.sabre.services.stl.v3.SystemNameAndId;
import com.sabre.services.stl.v3.SystemSpecificResults;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.IOException;
import java.math.BigInteger;

public class IssueBagTagServiceTest {

	private IssueBagTagService issueBagTagService;

	@Before
	public void setup() {

		issueBagTagService = new IssueBagTagService();

		// issueBagTagService.setIssueBagTagService(new AMIssueBagTagServiceMock());

	}

	@Test
	public void testDateConversion() throws Exception {

		String dateValue = "2019-03-27T21:45:00";
		
		System.out.println( issueBagTagService.getDepartureTimeFomated( dateValue ) );
	}

	@Test
	public void testInvalidDateError() throws Exception {
		WarningCollection warningCollection = new WarningCollection();

		Warning warning = new Warning();
		warning.setCode("204050");
		warningCollection.addToList(warning);

		warning = new Warning();
		warning.setCode("204050");
		warningCollection.addToList(warning);

		warning = new Warning();
		warning.setCode("204051");
		warningCollection.addToList(warning);

		warning = new Warning();
		warning.setCode("204051");
		warningCollection.addToList(warning);

		Assert.assertEquals(2, warningCollection.getCollection().size());
	}

	/*
	 * Testing
	 */
	@Test
	public void testAMIssueBagTagServiceMock() throws ClassNotFoundException, JsonParseException, IOException {

		/*
		 * IssueBagRP getBagTag( BookedTraveler bookedTravelerRQ, IssueBagRQ issueBagRQ,
		 * PNRCollection pnrCollection
		 */
		IssueBagReq issueBagReq = new IssueBagReq();
		BagRouteInfo bagRouteInfo = new BagRouteInfo();

		AMIssueBagTagServiceMock amIssueBagTagServiceMock = new AMIssueBagTagServiceMock();

		IssueBagTagRSACS issueBagTagRSACS = new IssueBagTagRSACS();
		AMIssueBagTagService amIssueBagTagService = new AMIssueBagTagService();

		bagRouteInfo.setOpCarrier("AM");
		bagRouteInfo.setOpflightNumber("0910");
		bagRouteInfo.setDepartureDate("2020-01-07");
		bagRouteInfo.setBookingClass("Y");
		bagRouteInfo.setOrigin("MEX");

		issueBagReq.setBagRouteInfo(bagRouteInfo);

		SampleIssueBAgTagRSACSResponse(issueBagTagRSACS);

		amIssueBagTagServiceMock.getIssueBagTag(issueBagReq, null, null);

	}

	public void SampleIssueBAgTagRSACSResponse(IssueBagTagRSACS issueBagTagRSACS) {

		com.sabre.services.stl.v3.ResultRecord resultRecord = new com.sabre.services.stl.v3.ResultRecord();
		SystemNameAndId systemNameAndId = new SystemNameAndId();
		SystemSpecificResults systemSpecificResults = new SystemSpecificResults();
		ErrorMessage errorMessage = new ErrorMessage();
		BigInteger code = new BigInteger("367");

		XMLGregorianCalendar result;
		try {
			result = DatatypeFactory.newInstance().newXMLGregorianCalendar("2020-01-06T09:34:45.208Z");
			resultRecord.setTimeStamp(result);
		} catch (DatatypeConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		systemNameAndId.setValue("CKI-WS");
		resultRecord.setStatus(ErrorOrSuccessCode.BUSINESS_LOGIC_ERROR);
		resultRecord.setCompletionStatus(CompletionCodes.INCOMPLETE);
		resultRecord.setSystem(systemNameAndId);
		resultRecord.setMessageId("ID-ckivlc027-sabre-com-1578002050865-0-1404983");
		systemSpecificResults.setRecordID("ID-ckivlc027-sabre-com-1578002050865-0-1404978");
		errorMessage.setCode(code);
		errorMessage.setValue("!BAGGAGE CHECK-IN NOT PERMITTED - INVALID DATE");
		systemSpecificResults.setErrorMessage(errorMessage);
		resultRecord.getSystemSpecificResults().add(systemSpecificResults);

		AMIssueBagTagService amIssueBagTagService = new AMIssueBagTagService();
		issueBagTagRSACS.setResult(resultRecord);

		System.out.println(amIssueBagTagService.toString(issueBagTagRSACS));

	}

}
