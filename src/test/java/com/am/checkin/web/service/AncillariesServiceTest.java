package com.am.checkin.web.service;

import com.aeromexico.commons.model.AncillaryOfferCollection;
import com.aeromexico.commons.model.CurrencyValue;
import com.aeromexico.commons.model.rq.AncillaryRQ;
import com.aeromexico.dao.model.AncillaryEntry;
import com.aeromexico.dao.services.AncillariesDao;
import com.aeromexico.dao.test.EnvironmentTest;
import com.am.checkin.web.dao.PNRLookupDaoMock;
import com.am.checkin.web.util.FileUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sabre.webservices.pnrbuilder.AncillaryPricePNRB;
import com.sabre.webservices.pnrbuilder.AncillaryTaxPNRB;
import com.sabre.webservices.pnrbuilder.OptionalAncillaryServicesInformationDataPNRB;
import com.sabre.webservices.pnrbuilder.PricedAncillaryServicePNRB;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AncillariesServiceTest extends EnvironmentTest {

    private static AncillariesService ancillariesService;

    @Before
    public void setup() throws Exception {
        ancillariesService = new AncillariesService();
        ancillariesService.init();
        ancillariesService.getSetUpConfigFactory().init();

        PNRLookupDaoMock pnrLookupDaoMock = new PNRLookupDaoMock();
        ancillariesService.setPnrLookupDao(pnrLookupDaoMock);
        AncillariesDao ancillariesDaoMock = mock(AncillariesDao.class);
        String ancillariesValue = FileUtils.readFileAsString("/web/service/ancillaries/ancillariesMYB.json");
        Type listType = new TypeToken<List<AncillaryEntry>>() {
        }.getType();
        ArrayList<AncillaryEntry> ancillariesMYB = new Gson().fromJson(ancillariesValue, listType);
        when(ancillariesDaoMock.getAncillaryEntries()).thenReturn(ancillariesMYB);
    }

    

    @Test
    public void testGetCurrencyValue () {

    	PricedAncillaryServicePNRB pricedAncillary = buildAncillaryPrice();
    	
    	CurrencyValue currency = ancillariesService.getCurrencyValue(pricedAncillary);
    	
    	System.out.println( currency );
    }

    private PricedAncillaryServicePNRB buildAncillaryPrice() {

    	/*
                     <ns3:optionalAncillaryServiceInformation>
                        <ns3:SegmentNumber>1</ns3:SegmentNumber>
                        <ns3:EquivalentPrice>
                           <ns3:Price>978</ns3:Price>
                           <ns3:Currency>MXN</ns3:Currency>
                        </ns3:EquivalentPrice>

                        <ns3:TTLPrice>
                           <ns3:Price>1135</ns3:Price>
                           <ns3:Currency>MXN</ns3:Currency>
                        </ns3:TTLPrice>

                        <ns3:PortionOfTravelIndicator>P</ns3:PortionOfTravelIndicator>

                        <ns3:OriginalBasePrice>
                           <ns3:Price>55.00</ns3:Price>
                           <ns3:Currency>USD</ns3:Currency>
                        </ns3:OriginalBasePrice>

                        <ns3:RefundIndicator>N</ns3:RefundIndicator>
                        <ns3:CommisionIndicator>N</ns3:CommisionIndicator>
                        <ns3:InterlineIndicator>N</ns3:InterlineIndicator>
                        <ns3:FeeApplicationIndicator>4</ns3:FeeApplicationIndicator>
                        <ns3:PassengerTypeCode>ADT</ns3:PassengerTypeCode>
                        <ns3:BoardPoint>MEX</ns3:BoardPoint>
                        <ns3:OffPoint>EZE</ns3:OffPoint>

                        <ns3:Taxes>
                           <ns3:Tax>
                              <ns3:TaxAmount>157</ns3:TaxAmount>
                              <ns3:TaxCode>MXA</ns3:TaxCode>
                           </ns3:Tax>
                        </ns3:Taxes>

                        <ns3:SoftMatchIndicator>false</ns3:SoftMatchIndicator>
                        <ns3:SimultaneousTicketIndicator>X</ns3:SimultaneousTicketIndicator>
                        <ns3:TravelDateEffective>1980-01-01</ns3:TravelDateEffective>
                        <ns3:LatestTravelDatePermitted>9999-12-31</ns3:LatestTravelDatePermitted>

                     </ns3:optionalAncillaryServiceInformation>
    	*/

    	PricedAncillaryServicePNRB PricedAncillary = new PricedAncillaryServicePNRB();

    	OptionalAncillaryServicesInformationDataPNRB optionalAncillaryServicesInformationDataPNRB = new OptionalAncillaryServicesInformationDataPNRB();
    	optionalAncillaryServicesInformationDataPNRB.getSegmentNumber().add( new Byte("1") );

    	AncillaryPricePNRB equivalentPrice = new AncillaryPricePNRB();
    	equivalentPrice.setCurrency( "MXN" );
    	equivalentPrice.setPrice( new BigDecimal(978) );
    	optionalAncillaryServicesInformationDataPNRB.setEquivalentPrice(equivalentPrice);

    	AncillaryPricePNRB ttlPrice = new AncillaryPricePNRB();
    	ttlPrice.setCurrency( "MXN" );
    	ttlPrice.setPrice(  new BigDecimal(1135) );
    	optionalAncillaryServicesInformationDataPNRB.setTTLPrice(ttlPrice);

    	AncillaryPricePNRB originalBasePrice = new AncillaryPricePNRB();
    	originalBasePrice.setCurrency( "USD" );
    	originalBasePrice.setPrice(  new BigDecimal(55.00) );
    	optionalAncillaryServicesInformationDataPNRB.setOriginalBasePrice(originalBasePrice);

    	AncillaryTaxPNRB tax = new AncillaryTaxPNRB();
    	tax.setTaxAmount( new BigDecimal(157) );
    	tax.setTaxCode( "MXA" );
    	com.sabre.webservices.pnrbuilder.AncillaryServicesUpdatePNRB.Taxes taxes = new com.sabre.webservices.pnrbuilder.AncillaryServicesUpdatePNRB.Taxes();
    	taxes.getTax().add(tax);
    	optionalAncillaryServicesInformationDataPNRB.setTaxes(taxes);

    	PricedAncillary.getOptionalAncillaryServiceInformation().add(optionalAncillaryServicesInformationDataPNRB);

    	return PricedAncillary;
    }
}
