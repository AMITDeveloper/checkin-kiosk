package com.am.checkin.web.service;

import com.aeromexico.codes.impl.CodesGenerator;
import com.aeromexico.commons.model.PNRCollection;
import org.apache.commons.io.IOUtils;

import java.nio.charset.Charset;
import java.util.List;

import static org.junit.Assert.fail;

public class MobileBoardingPassServiceTest {

    public void testMobileBoardingPasses() {
        try {
            String pnrCollectionString = IOUtils.toString(MobileBoardingPassServiceTest.class.getResourceAsStream("boardingPassJson.json"), Charset.defaultCharset());
            PNRCollection pnrCollection = new PNRCollection(pnrCollectionString);
            MobileBoardingPassService mobileBoardingPassService = new MobileBoardingPassService();
            mobileBoardingPassService.init();
            List<byte[]> mbp = mobileBoardingPassService.getMobileBoardingPass(pnrCollection);
            for (byte[] imageContent : mbp) {
                String imageBase64 = new CodesGenerator().convertImageToBase64(imageContent);
                System.out.println(imageBase64);
            }
        } catch(Exception ex) {
            fail("Should not throw exception: " + ex);
        }
    }
}
