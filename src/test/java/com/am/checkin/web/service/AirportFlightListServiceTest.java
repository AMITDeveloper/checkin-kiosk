
package com.am.checkin.web.service;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.aeromexico.commons.model.AirportCollection;
import com.aeromexico.commons.model.rq.AirportFlightListRQ;

public class AirportFlightListServiceTest {

	@Test
	public void test() throws Exception {
		AirportFlightListService airportFlightListService = new AirportFlightListService();
		airportFlightListService.setAirportFlightList( new AMAirportFlightListMock() );

		AirportFlightListRQ airportFlightListRQ = new AirportFlightListRQ();
		airportFlightListRQ.setOrigin("MEX");
		AirportCollection airportCollection = airportFlightListService.getAirportList(airportFlightListRQ);
		
		assertNotNull( airportCollection );
		
		System.out.println( airportCollection.toString() );
	}

}
