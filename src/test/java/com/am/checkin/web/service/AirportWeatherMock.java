
package com.am.checkin.web.service;

import java.math.BigDecimal;
import com.aeromexico.commons.model.weather.Airport;
import com.aeromexico.commons.model.weather.AirportWeather;

public class AirportWeatherMock extends AirportWeather {

	public AirportWeatherMock(String code, String region) {

		Airport airport = new Airport();
		airport.setCode( code );
		airport.setTitle( code );
		airport.setLatitude ( BigDecimal.valueOf(29.9843998) );
		airport.setLongitude ( BigDecimal.valueOf(-95.34140015) );
		airport.setRegion( region );
		airport.setCity( "Houston" );
		airport.setName( "George Bush Intercontinental Houston Airport" );
		airport.setSkyteam( false );
		airport.setMobileCheckIn( false );
		airport.setCountry( "US" );
		airport.setCountryCode( "US-TX" );

		super.setAirport( airport );
	}

}
