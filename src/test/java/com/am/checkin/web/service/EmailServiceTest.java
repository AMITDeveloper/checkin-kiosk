package com.am.checkin.web.service;


import com.aeromexico.dao.test.EnvironmentTest;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.fail;

public class EmailServiceTest extends EnvironmentTest {

    private EmailService emailService;

    @Before
    public void setup() {
        emailService = new EmailService();
    }

    @Test
    @Ignore
    public void testDownloadPDFFile() {
        try {
            File pdfFile = emailService.downloadBoardingPass("FQADYP", "ALL", "en");
        } catch (Exception ex) {
            ex.printStackTrace();
            fail("Failed downloading already created pdf file");
        }
    }
}
