package com.am.checkin.web.service;

import com.aeromexico.commons.exception.model.GenericException;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.Warning;
import com.aeromexico.commons.model.WarningCollection;
import com.aeromexico.commons.model.rq.ShoppingCartRQ;
import com.aeromexico.dao.client.exception.MongoDisconnectException;
import com.aeromexico.dao.services.IPNRLookupDao;
import com.aeromexico.dao.test.EnvironmentTest;
import com.aeromexico.sabre.api.acs.AMPassengerSecurityService;
import com.aeromexico.sabre.api.passenger.AMPassengerDetailsService;
import com.aeromexico.sabre.api.model.SabrePassengerDTO;
import com.aeromexico.sabre.api.session.common.ServiceCall;
import com.aeromexico.sabre.api.updatereservation.AMUpdateReservationService;
import com.am.checkin.web.dao.PNRLookupDaoMock;
import com.am.checkin.web.event.dispatcher.UpdatePNRCollectionAfterCheckinDispatcher;
import com.am.checkin.web.event.dispatcher.UpdatePNRCollectionDispatcher;
import com.am.checkin.web.util.FileUtils;
import com.google.gson.JsonParseException;
import com.sabre.services.sp.pd.v3_3.PassengerDetailsRS;
import com.sabre.services.stl_payload.v02_01.ApplicationResults;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.Ignore;
import org.mockito.Mockito;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Ignore
public class ShoppingCartServiceTest extends EnvironmentTest {

    private List<ServiceCall> serviceCallList;

    private ShoppingCartService shoppingCartService;
    private IPNRLookupDao pnrLookupDao;
    private UpdatePNRCollectionDispatcher updatePNRCollectionDispatcher;
    private UpdatePNRCollectionAfterCheckinDispatcher pnrCollectionAfterCheckinDispatcher;
    private PnrCollectionService updatePnrCollectionService;
    private AMPassengerSecurityService passengerSecurityService;
    private AEServiceMock aeServiceMock;

    @Before
    public void setup() {
        SeatsService seatsService = new SeatsService();
        ShoppingCartAncillaries shoppingCartAncillaries = new ShoppingCartAncillaries();
        
        serviceCallList = new ArrayList<>();
        pnrLookupDao = new PNRLookupDaoMock();
        shoppingCartService = new ShoppingCartService();
        
        AMEditPassCharServiceMock editPassCharServiceMock = new AMEditPassCharServiceMock();
        
        shoppingCartService.setEditPassCharService(editPassCharServiceMock);
        shoppingCartService.setPnrLookupDao(pnrLookupDao);
        seatsService.setPnrLookupDao(pnrLookupDao);
        shoppingCartAncillaries.setPnrLookupDao(pnrLookupDao);
        shoppingCartService.setShoppingCartAncillaries(shoppingCartAncillaries);
        aeServiceMock = new AEServiceMock();
        aeServiceMock.setPnrLookupDao(pnrLookupDao);

        seatsService.setAeService(aeServiceMock);

        seatsService.setEditPassCharService(editPassCharServiceMock);

        AMUpdateReservationService updateReservationService;
        updateReservationService = new AMUpdateReservationServiceMock();
        
        shoppingCartService.setUpdateReservationService(updateReservationService);        

        passengerSecurityService = new AMPassengerSecurityServiceMock();
        shoppingCartService.setAMPassengerSecurityService(passengerSecurityService);

        updatePNRCollectionDispatcher = new UpdatePNRCollectionDispatcher();
        pnrCollectionAfterCheckinDispatcher = new UpdatePNRCollectionAfterCheckinDispatcher();
        updatePnrCollectionService = new PnrCollectionService();
        updatePnrCollectionService.setPnrLookupDao(pnrLookupDao);
        updatePnrCollectionService.setUpdatePNRCollectionDispatcher(updatePNRCollectionDispatcher);
        updatePnrCollectionService.setUpdatePNRCollectionAfterCheckinDispatcher(pnrCollectionAfterCheckinDispatcher);

        shoppingCartService.setUpdatePnrCollectionService(updatePnrCollectionService);

        seatsService.setUpdatePnrCollectionService(updatePnrCollectionService);

        shoppingCartService.setSeatsService(seatsService);

    }

    @Test
    public void testWarningCollection() throws Exception {
        WarningCollection warningCollection = new WarningCollection();

        Warning warning = new Warning();
        warning.setCode("204050");
        warningCollection.addToList(warning);

        warning = new Warning();
        warning.setCode("204050");
        warningCollection.addToList(warning);

        warning = new Warning();
        warning.setCode("204051");
        warningCollection.addToList(warning);

        warning = new Warning();
        warning.setCode("204051");
        warningCollection.addToList(warning);

        Assert.assertEquals(2, warningCollection.getCollection().size());
    }

    /*
	 * Testing add a valid FF for one passenger
     */
    @Test
    public void testShoppingCartFF() throws ClassNotFoundException, JsonParseException, IOException {

        String body = FileUtils.readFileAsString("/web/service/ShoppingCartPutTest/ShoppingCartPutRQ_ff.json");
        System.out.println("String: " + body);

        ShoppingCartRQ shoppingCartRQ = new ShoppingCartRQ();

        shoppingCartRQ.setCartId("4ffeef9a-77dc-45f5-9011-55c2cb05109d");
        shoppingCartRQ.setStore("MX");
        shoppingCartRQ.setPos("KIOSK");
        shoppingCartRQ.setRequest(body);

        CartPNR cartPNR = null;
        try {
        AMPassengerDetailsService AMPassengerDetailsServiceMock = mock(AMPassengerDetailsService.class);
        SabrePassengerDTO sabrePassengerDTO = new SabrePassengerDTO(Mockito.anyString());
        PassengerDetailsRS passengerDetailsRS = new PassengerDetailsRS();
        ApplicationResults applicationResults = new ApplicationResults();
        passengerDetailsRS.setApplicationResults(applicationResults);
        when(AMPassengerDetailsServiceMock.updatePassengerLoyalty(sabrePassengerDTO)).thenReturn(passengerDetailsRS);
        shoppingCartService.setPassengerDetailsService(AMPassengerDetailsServiceMock);


            cartPNR = shoppingCartService.putShoppingCart(shoppingCartRQ, serviceCallList);
        } catch (MongoDisconnectException me) {
            me.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertNotNull(cartPNR);

        // FF
        assertTrue(cartPNR.getTravelerInfo().getCollection().get(0).getFrequentFlyerNumber().compareTo(shoppingCartRQ
                .getCartPNRUpdate().getTravelerInfo().getCollection().get(0).getFrequentFlyerNumber()) == 0);
    }

    /*
	 * Testing update of an invalid FF for one passenger
     */
    @Test
    public void testShoppingCartInvaidFF() throws ClassNotFoundException, JsonParseException, IOException {

        String body = FileUtils.readFileAsString("/web/service/ShoppingCartPutTest/ShoppingCartPutRQ_invalid_ff.json");
        System.out.println("Request: " + body);

        ShoppingCartRQ shoppingCartRQ = new ShoppingCartRQ();

        shoppingCartRQ.setCartId("4ffeef9a-77dc-45f5-9011-55c2cb05109d");
        shoppingCartRQ.setStore("MX");
        shoppingCartRQ.setPos("KIOSK");
        shoppingCartRQ.setRequest(body);

        CartPNR cartPNR = null;
        try {
        AMPassengerDetailsService AMPassengerDetailsServiceMock = mock(AMPassengerDetailsService.class);
        SabrePassengerDTO sabrePassengerDTO = new SabrePassengerDTO(Mockito.anyString());
        PassengerDetailsRS passengerDetailsRS = new PassengerDetailsRS();
        when(AMPassengerDetailsServiceMock.updatePassengerLoyalty(sabrePassengerDTO)).thenReturn(passengerDetailsRS);
        shoppingCartService.setPassengerDetailsService(AMPassengerDetailsServiceMock);


            cartPNR = shoppingCartService.putShoppingCart(shoppingCartRQ, serviceCallList);
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertNotNull(cartPNR);
        System.out.println("Response: " + cartPNR);
    }

    /*
	 *
	 * Testing update Emergency Contact for one passenger
     */
    @Test
    public void testShoppingCartEmergencyContact() throws ClassNotFoundException, JsonParseException, IOException {

        String body = FileUtils.readFileAsString("/web/service/ShoppingCartPutTest/ShoppingCartPutRQ_EmergencyContact.json");
        System.out.println("String: " + body);

        ShoppingCartRQ shoppingCartRQ = new ShoppingCartRQ();

        shoppingCartRQ.setCartId("4ffeef9a-77dc-45f5-9011-55c2cb05109d");
        shoppingCartRQ.setStore("MX");
        shoppingCartRQ.setPos("KIOSK");
        shoppingCartRQ.setRequest(body);

        CartPNR cartPNR = null;

        try {
            cartPNR = shoppingCartService.putShoppingCart(shoppingCartRQ, serviceCallList);
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertNotNull(cartPNR);
        System.out.println(cartPNR.toString());
    }

    /*
	 *
	 * Testing update Timatic information for one passenger
     */
    @Test
    public void testShoppingTimaticInfo() throws ClassNotFoundException, JsonParseException, IOException {

        String body = FileUtils.readFileAsString("/web/service/ShoppingCartPutTest/ShoppingCartPutRQ_timaticInfo.json");
        System.out.println("String: " + body);

        ShoppingCartRQ shoppingCartRQ = new ShoppingCartRQ();

        shoppingCartRQ.setCartId("4ffeef9a-77dc-45f5-9011-55c2cb05109d");
        shoppingCartRQ.setStore("MX");
        shoppingCartRQ.setPos("KIOSK");
        shoppingCartRQ.setRequest(body);
        
        System.out.println( shoppingCartRQ.getCartPNRUpdate().getTravelerInfo().getCollection().get(0).getTimaticInfo() );

        CartPNR cartPNR = null;

        try {
            cartPNR = shoppingCartService.putShoppingCart(shoppingCartRQ, serviceCallList);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //assertNotNull(cartPNR);
        //System.out.println(cartPNR.toString());
    }

    /*
	 * Testing update free seat for one passenger
     */
    @Test
    @Ignore
    public void testShoppingCartfreeSeat() {

        String body = FileUtils.readFileAsString("/web/service/ShoppingCartPutTest/ShoppingCartPutRQ_freeSeat.json");
        System.out.println("RQ: " + body);

        ShoppingCartRQ shoppingCartRQ = new ShoppingCartRQ();

        shoppingCartRQ.setCartId("4ffeef9a-77dc-45f5-9011-55c2cb05109d");
        shoppingCartRQ.setStore("MX");
        shoppingCartRQ.setPos("KIOSK");
        shoppingCartRQ.setRequest(body);

        CartPNR cartPNR = null;

        try {
            cartPNR = shoppingCartService.putShoppingCart(shoppingCartRQ, serviceCallList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertNotNull(cartPNR);
        System.out.println("Response : " + cartPNR.toString());
    }

    /*
	 * Testing update free seat for one passenger
     */
    @Test
    @Ignore
    public void testShoppingCartfreeSeat2() {

        String body = FileUtils.readFileAsString("/web/service/ShoppingCartPutTest/ShoppingCartPutRQ_freeSeat2.json");
        System.out.println("String: " + body);

        ShoppingCartRQ shoppingCartRQ = new ShoppingCartRQ();

        shoppingCartRQ.setCartId("4ffeef9a-77dc-45f5-9011-55c2cb05109d");
        shoppingCartRQ.setStore("MX");
        shoppingCartRQ.setPos("KIOSK");
        shoppingCartRQ.setRequest(body);

        CartPNR cartPNR = null;

        try {
            cartPNR = shoppingCartService.putShoppingCart(shoppingCartRQ, serviceCallList);
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertNotNull(cartPNR);
    }

    /*
	 * Testing add paid seat for one passenger
     */
    @Test
    @Ignore
    public void testShoppingCartpaidSeat() {

        String body = FileUtils.readFileAsString("/web/service/ShoppingCartPutTest/ShoppingCartPutRQ_paidSeat.json");
        System.out.println("RQ: " + body);

        ShoppingCartRQ shoppingCartRQ = new ShoppingCartRQ();

        shoppingCartRQ.setCartId("4ffeef9a-77dc-45f5-9011-55c2cb05109d");
        shoppingCartRQ.setStore("MX");
        shoppingCartRQ.setPos("KIOSK");
        shoppingCartRQ.setRequest(body);

        CartPNR cartPNR = null;

        try {
            cartPNR = shoppingCartService.putShoppingCart(shoppingCartRQ, serviceCallList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertNotNull(cartPNR);
        System.out.println("Response: " + cartPNR.toString());
    }

    /*
	 * Testing update gender for one passenger
     */
    @Test
    public void testShoppingCartGENDER() throws ClassNotFoundException, JsonParseException, IOException {

        String body = FileUtils.readFileAsString("/web/service/ShoppingCartPutTest/ShoppingCartPutRQ_gender.json");
        System.out.println("String: " + body);

        ShoppingCartRQ shoppingCartRQ = new ShoppingCartRQ();

        shoppingCartRQ.setCartId("4ffeef9a-77dc-45f5-9011-55c2cb05109d");
        shoppingCartRQ.setStore("MX");
        shoppingCartRQ.setPos("KIOSK");
        shoppingCartRQ.setRequest(body);

        CartPNR cartPNR = null;

        try {
            cartPNR = shoppingCartService.putShoppingCart(shoppingCartRQ, serviceCallList);
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertNotNull(cartPNR);

        // GENDER
        assertTrue(cartPNR.getTravelerInfo().getCollection().get(0).getGender().compareTo(
                shoppingCartRQ.getCartPNRUpdate().getTravelerInfo().getCollection().get(0).getGender()) == 0);
    }

    /*
	 * Testing add residence for one passenger
     */
    @Test
    public void testShoppingCartResidence() throws ClassNotFoundException, JsonParseException, IOException {

        String body = FileUtils
                .readFileAsString("/web/service/ShoppingCartPutTest/ShoppingCartPutRQ_ResidenceAddress.json");
        System.out.println("String: " + body);

        ShoppingCartRQ shoppingCartRQ = new ShoppingCartRQ();

        shoppingCartRQ.setCartId("4ffeef9a-77dc-45f5-9011-55c2cb05109d");
        shoppingCartRQ.setStore("MX");
        shoppingCartRQ.setPos("KIOSK");
        shoppingCartRQ.setRequest(body);

        CartPNR cartPNR = null;

        try {
            cartPNR = shoppingCartService.putShoppingCart(shoppingCartRQ, serviceCallList);
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertNotNull(cartPNR);
        System.out.println(cartPNR.toString());
    }

    /*
	 * Testing add destination for one passenger
     */
    @Test
    public void testShoppingCartDestination() throws ClassNotFoundException, JsonParseException, IOException {

        String body = FileUtils
                .readFileAsString("/web/service/ShoppingCartPutTest/ShoppingCartPutRQ_DestinationAddress.json");
        System.out.println("RQ: " + body);

        ShoppingCartRQ shoppingCartRQ = new ShoppingCartRQ();

        shoppingCartRQ.setCartId("4ffeef9a-77dc-45f5-9011-55c2cb05109d");
        shoppingCartRQ.setStore("MX");
        shoppingCartRQ.setPos("KIOSK");
        shoppingCartRQ.setRequest(body);

        CartPNR cartPNR = null;

        try {
            cartPNR = shoppingCartService.putShoppingCart(shoppingCartRQ, serviceCallList);
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("response - " + cartPNR);
        assertNotNull(cartPNR);
    }

    /*
	 * Testing add ExtraBagsAncillaryMap for one passenger
     */
    @Test
    public void testShoppingCartAddAncillaries() throws ClassNotFoundException, JsonParseException, IOException {
        // add ancillaries
        String body = FileUtils
                .readFileAsString("/web/service/ShoppingCartPutTest/ShoppingCartPutRQ_ExtraBagsAncillary.json");
        System.out.println("RQ- CartPNRUpdate: " + body);

        ShoppingCartRQ shoppingCartRQ = new ShoppingCartRQ();

        shoppingCartRQ.setCartId("4ffeef9a-77dc-45f5-9011-55c2cb05109d");
        shoppingCartRQ.setStore("MX");
        shoppingCartRQ.setPos("KIOSK");
        shoppingCartRQ.setRequest(body);

        CartPNR cartPNR = null;

        try {
            cartPNR = shoppingCartService.putShoppingCart(shoppingCartRQ, serviceCallList);
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertNotNull(cartPNR);
        System.out.println("Response with ancillary: ");
        System.out.println(cartPNR.toString());
    }

    /*
	 * Testing remove ExtraBagsAncillaryMap for one passenger
     */
    @Test
    public void testShoppingCartRemoveAncillaries() throws ClassNotFoundException, JsonParseException, IOException {

        String body = FileUtils
                .readFileAsString("/web/service/ShoppingCartPutTest/ShoppingCartPutRQ_ExtraBagsAncillaryRemove.json");
        System.out.println("RQ- CartPNRUpdate: " + body);

        ShoppingCartRQ shoppingCartRQ = new ShoppingCartRQ();

        shoppingCartRQ.setCartId("4ffeef9a-77dc-45f5-9011-55c2cb05109d1");
        shoppingCartRQ.setStore("MX");
        shoppingCartRQ.setPos("KIOSK");
        shoppingCartRQ.setRequest(body);

        CartPNR cartPNR = null;

        try {
            cartPNR = shoppingCartService.putShoppingCart(shoppingCartRQ, serviceCallList);
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("Response without ancillary: ");
        System.out.println(cartPNR.toString());
        assertNotNull(cartPNR);
    }

    /*
	 * Testing remove ExtraBagsAncillaryMap for one passenger
     */
    @Test
    public void testShoppingCartRemoveAncillaryInvalid()
            throws ClassNotFoundException, JsonParseException, IOException {

        String body = FileUtils.readFileAsString(
                "/web/service/ShoppingCartPutTest/ShoppingCartPutRQ_ExtraBagsAncillaryRemoveInvalid.json");
        System.out.println("RQ- CartPNRUpdate: " + body);

        ShoppingCartRQ shoppingCartRQ = new ShoppingCartRQ();

        shoppingCartRQ.setCartId("4ffeef9a-77dc-45f5-9011-55c2cb05109d1");
        shoppingCartRQ.setStore("MX");
        shoppingCartRQ.setPos("KIOSK");
        shoppingCartRQ.setRequest(body);

        try {
            shoppingCartService.putShoppingCart(shoppingCartRQ, serviceCallList);
        } catch (GenericException e) {
            System.out.println(e.toString());
            if (!e.toString().equals(
                    "{\"status\":\"UNAUTHORIZED\",\"errorCodeType\":\"ID_ANCILLARY_NOT_EXIST\",\"msg\":\"code: 041 message: The ID ancillary not exit.\",\"stackTrace\":[],\"suppressedExceptions\":[]}")) {
                fail();
            }
        } catch (Exception e) {
            fail();
        }
    }

    /*
	 * Testing update travel document for one passenger
     */
    @Test
    public void testShoppingCartTravelDocument() throws ClassNotFoundException, JsonParseException, IOException {

        String body = FileUtils.readFileAsString("/web/service/ShoppingCartPutTest/ShoppingCartPutRQ_docs.json");
        System.out.println("RQ: " + body);

        ShoppingCartRQ shoppingCartRQ = new ShoppingCartRQ();

        shoppingCartRQ.setCartId("4ffeef9a-77dc-45f5-9011-55c2cb05109d");
        shoppingCartRQ.setStore("MX");
        shoppingCartRQ.setPos("KIOSK");
        shoppingCartRQ.setRequest(body);

        CartPNR cartPNR = null;

        try {
            cartPNR = shoppingCartService.putShoppingCart(shoppingCartRQ, serviceCallList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertNotNull(cartPNR);
        System.out.println("Response with Travel Document: ");
        System.out.println(cartPNR.toString());
    }

    @Test
    public void testShoppingCartTravelDocumentSameInfo()
            throws ClassNotFoundException, JsonParseException, IOException {

        String body = FileUtils.readFileAsString("/web/service/ShoppingCartPutTest/ShoppingCartPutRQ_docs.json");
        System.out.println("RQ: " + body);

        ShoppingCartRQ shoppingCartRQ = new ShoppingCartRQ();

        shoppingCartRQ.setCartId("4ffeef9a-77dc-45f5-9011-55c2cb05109d3");
        shoppingCartRQ.setStore("MX");
        shoppingCartRQ.setPos("KIOSK");
        shoppingCartRQ.setRequest(body);

        CartPNR cartPNR = null;

        try {
            cartPNR = shoppingCartService.putShoppingCart(shoppingCartRQ, serviceCallList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertNotNull(cartPNR);
        System.out.println("Response with Travel Document: ");
        System.out.println(cartPNR.toString());
    }

    /*
	 * Testing add travel document for Infant
     */
    @Test
    public void testShoppingCartTravelDocumentInfant() throws ClassNotFoundException, JsonParseException, IOException {

        String body = FileUtils.readFileAsString("/web/service/ShoppingCartPutTest/ShoppingCartPutRQ_docsInfant.json");
        System.out.println("RQ: " + body);

        ShoppingCartRQ shoppingCartRQ = new ShoppingCartRQ();

        shoppingCartRQ.setCartId("4ffeef9a-77dc-45f5-9011-55c2cb05109d");
        shoppingCartRQ.setStore("MX");
        shoppingCartRQ.setPos("KIOSK");
        shoppingCartRQ.setRequest(body);

        CartPNR cartPNR = null;

        try {
            cartPNR = shoppingCartService.putShoppingCart(shoppingCartRQ, serviceCallList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //assertNotNull(cartPNR);
        //System.out.println("Response with Travel Document - Infant: ");
        //System.out.println(cartPNR.toString());
    }

    /*
	 * Testing with passengers selects for checkIn
     */
    @Test
    public void testShoppingCart_PassengerSelection() throws ClassNotFoundException, JsonParseException, IOException {

        String body = FileUtils
                .readFileAsString("/web/service/ShoppingCartPutTest/ShoppingCartPutRQ_PassengerSelection.json");
        System.out.println("RQ - CartPNRUpdate: " + body);

        ShoppingCartRQ shoppingCartRQ = new ShoppingCartRQ();

        shoppingCartRQ.setCartId("4ffeef9a-77dc-45f5-9011-55c2cb05109d2");
        shoppingCartRQ.setStore("MX");
        shoppingCartRQ.setPos("KIOSK");
        shoppingCartRQ.setRequest(body);

        CartPNR cartPNR = null;

        try {
            cartPNR = shoppingCartService.putShoppingCart(shoppingCartRQ, serviceCallList);
        } catch (Exception e) {
            System.out.println(e.toString());
            fail();
        }
        System.out.println("reponse - " + cartPNR.toString());
        assertNotNull(cartPNR);
    }

    /*
	 * Testing change seat after check-in: Free to paid-seat
	 * 
     */
    @Test
    @Ignore
    public void testShoppingCart_ChangeSeatAfterCheckIn_free_to_paid()
            throws ClassNotFoundException, JsonParseException, IOException {

        String body = FileUtils
                .readFileAsString("/web/service/ShoppingCartPutTest/ShoppingCartPutRQ_ChangeSeat_free_paid.json");
        System.out.println("RQ- CartPNRUpdate: " + body);

        ShoppingCartRQ shoppingCartRQ = new ShoppingCartRQ();
        shoppingCartRQ.setCartId("dc0c6728-9486-4512-a9cf-41100ce119c5");
        shoppingCartRQ.setStore("MX");
        shoppingCartRQ.setPos("KIOSK");
        shoppingCartRQ.setRequest(body);

        CartPNR cartPNR = null;

        try {
            cartPNR = shoppingCartService.putShoppingCart(shoppingCartRQ, serviceCallList);
        } catch (Exception e) {
            System.out.println(e.toString());
            fail();
        }
        System.out.println("reponse - " + cartPNR.toString());
        assertNotNull(cartPNR);
    }

    /*
	 * Testing change seat after check-in: Free to paid-seat Two passengers
	 * 
     */
    @Test
    @Ignore
    public void testShoppingCart_ChangeSeatAfterCheckIn_free_to_paid_2p()
            throws ClassNotFoundException, JsonParseException, IOException {

        String body = FileUtils
                .readFileAsString("/web/service/ShoppingCartPutTest/ShoppingCartPutRQ_ChangeSeat_free_paid_2p.json");
        System.out.println("RQ- CartPNRUpdate: " + body);

        ShoppingCartRQ shoppingCartRQ = new ShoppingCartRQ();
        shoppingCartRQ.setCartId("07b4a75f-275c-4906-9244-bb95ed3c6560");
        shoppingCartRQ.setStore("MX");
        shoppingCartRQ.setPos("KIOSK");
        shoppingCartRQ.setRequest(body);

        CartPNR cartPNR = null;

        try {
            cartPNR = shoppingCartService.putShoppingCart(shoppingCartRQ, serviceCallList);
        } catch (Exception e) {
            System.out.println(e.toString());
            fail();
        }
        System.out.println("reponse - " + cartPNR.toString());
        assertNotNull(cartPNR);
    }

    /*
	 * Testing change seat after check-in: Free to paid-seat Two passengers
	 * 
     */
    @Test
    public void testShoppingCart_RemoveUpgrade_not_selected()
            throws ClassNotFoundException, JsonParseException, IOException {

        String body = FileUtils
                .readFileAsString("/web/service/ShoppingCartPutTest/ShoppingCartPutRQ_RemoveUpgrade.json");
        System.out.println("RQ- CartPNRUpdate: " + body);

        ShoppingCartRQ shoppingCartRQ = new ShoppingCartRQ();
        shoppingCartRQ.setCartId("812139a9-1a41-4f46-9caf-7107662b93dd");
        shoppingCartRQ.setStore("MX");
        shoppingCartRQ.setPos("KIOSK");
        shoppingCartRQ.setRequest(body);

        CartPNR cartPNR = null;

        try {
            cartPNR = shoppingCartService.putShoppingCart(shoppingCartRQ, serviceCallList);
        } catch (Exception e) {
            System.out.println(e.toString());
            fail();
        }
        System.out.println("reponse - " + cartPNR.toString());
        assertNotNull(cartPNR);
    }

    /*
	 * Testing change seat after check-in: Free to paid-seat Two passengers
	 * 
     */
    @Test
    public void testShoppingCart_RemoveUpgrade_selected()
            throws ClassNotFoundException, JsonParseException, IOException {

        String body = FileUtils
                .readFileAsString("/web/service/ShoppingCartPutTest/ShoppingCartPutRQ_RemoveUpgrade.json");
        System.out.println("RQ- CartPNRUpdate: " + body);

        ShoppingCartRQ shoppingCartRQ = new ShoppingCartRQ();
        shoppingCartRQ.setCartId("17afe75b-ba1e-417f-9851-4e39baa39f3f");
        shoppingCartRQ.setStore("MX");
        shoppingCartRQ.setPos("KIOSK");
        shoppingCartRQ.setRequest(body);

        CartPNR cartPNR = null;

        try {
            cartPNR = shoppingCartService.putShoppingCart(shoppingCartRQ, serviceCallList);
        } catch (Exception e) {
            System.out.println(e.toString());
            fail();
        }
        System.out.println("reponse - " + cartPNR.toString());
        assertNotNull(cartPNR);
    }

    /*
	 * /kiosk/api/v1/checkin/carts/ f87be316-37e4-4568-a248-00f4d37507c7?
	 * timeOfRequest=1480575782096& kioskId=MAHMUDFATAF73DA&
	 * uiVersion=3.0.10-495& pos=kiosk& store=mx
	 * 
     */
    @Test
    public void testShoppingCart_UpgradeTotal() throws ClassNotFoundException, JsonParseException, IOException {
        String body = FileUtils
                .readFileAsString("/web/service/ShoppingCartPutTest/ShoppingCartPutRQ_UpgradeTotal.json");
        System.out.println("RQ - CartPNRUpdate: " + body);

        ShoppingCartRQ shoppingCartRQ = new ShoppingCartRQ();
        shoppingCartRQ.setCartId("21016fdc-4217-4507-8633-42031e190182");
        shoppingCartRQ.setStore("MX");
        shoppingCartRQ.setPos("KIOSK");
        shoppingCartRQ.setRequest(body);

        CartPNR cartPNR = null;

        try {
            cartPNR = shoppingCartService.putShoppingCart(shoppingCartRQ, serviceCallList);
        } catch (Exception e) {
            System.out.println(e.toString());
            fail();
        }
        System.out.println("reponse - " + cartPNR.toString());
        assertNotNull(cartPNR);
    }

    /*
	 * /kiosk/api/v1/checkin/carts/ f87be316-37e4-4568-a248-00f4d37507c7?
	 * timeOfRequest=1480575782096& kioskId=MAHMUDFATAF73DA&
	 * uiVersion=3.0.10-495& pos=kiosk& store=mx
	 * 
     */
    @Test
    @Ignore
    public void testShoppingCart_AddPaidSeatWithExistingPaidAE()
            throws ClassNotFoundException, JsonParseException, IOException {

        String body = FileUtils.readFileAsString(
                "/web/service/ShoppingCartPutTest/ShoppingCartPutRQ_AddPaidSeatWithExistingPaidAE.json");
        System.out.println("RQ- CartPNRUpdate: " + body);

        ShoppingCartRQ shoppingCartRQ = new ShoppingCartRQ();
        shoppingCartRQ.setCartId("268ed013-a04d-400b-8fc6-207bf7c1eb97");
        shoppingCartRQ.setStore("MX");
        shoppingCartRQ.setPos("KIOSK");
        shoppingCartRQ.setRequest(body);

        CartPNR cartPNR = null;

        try {
            cartPNR = shoppingCartService.putShoppingCart(shoppingCartRQ, serviceCallList);
        } catch (Exception e) {
            System.out.println(e.toString());
            fail();
        }
        System.out.println("reponse - " + cartPNR.toString());
        assertNotNull(cartPNR);
    }

    /*
	 * /kiosk/api/v1/checkin/carts/ f87be316-37e4-4568-a248-00f4d37507c7?
	 * timeOfRequest=1480575782096& kioskId=MAHMUDFATAF73DA&
	 * uiVersion=3.0.10-495& pos=kiosk& store=mx
	 * 
     */
    // @Test
    // ***
    // *** This is not a complete test case, please do not activate.
    // ***
    public void testShoppingCart_AddBagAfterCheckIn() throws ClassNotFoundException, JsonParseException, IOException {

        String body = FileUtils
                .readFileAsString("/web/service/ShoppingCartPutTest/ShoppingCartPutRQ_AddBagAfterCheckIn.json");
        System.out.println("RQ- CartPNRUpdate: " + body);

        ShoppingCartRQ shoppingCartRQ = new ShoppingCartRQ();
        shoppingCartRQ.setCartId("e1f0e56c-60d9-4802-8ff8-6bef17da4430");
        shoppingCartRQ.setStore("MX");
        shoppingCartRQ.setPos("KIOSK");
        shoppingCartRQ.setRequest(body);

        CartPNR cartPNR = null;

        try {
            cartPNR = shoppingCartService.putShoppingCart(shoppingCartRQ, serviceCallList);
        } catch (Exception e) {
            System.out.println(e.toString());
            fail();
        }
        System.out.println("reponse - " + cartPNR.toString());
        assertNotNull(cartPNR);
    }

    /*
	 * /kiosk/api/v1/checkin/carts/ e349e5e4-2c13-4cc3-bf23-76e29c37d4d7?
	 * timeOfRequest=1480575782096& kioskId=MAHMUDFATAF73DA&
	 * uiVersion=3.0.10-495& pos=kiosk& store=mx
	 * 
     */
    @Test
    public void testShoppingCart_AddBagAfterCheckIn2() throws ClassNotFoundException, JsonParseException, IOException {

        String body = FileUtils
                .readFileAsString("/web/service/ShoppingCartPutTest/ShoppingCartPutRQ_AddBagAfterCheckIn2.json");
        System.out.println("RQ- CartPNRUpdate: " + body);

        ShoppingCartRQ shoppingCartRQ = new ShoppingCartRQ();
        shoppingCartRQ.setCartId("e349e5e4-2c13-4cc3-bf23-76e29c37d4d7");
        shoppingCartRQ.setStore("MX");
        shoppingCartRQ.setPos("KIOSK");
        shoppingCartRQ.setRequest(body);

        CartPNR cartPNR = null;

        try {
            cartPNR = shoppingCartService.putShoppingCart(shoppingCartRQ, serviceCallList);
        } catch (Exception e) {
            System.out.println(e.toString());
            fail();
        }
        System.out.println("reponse - " + cartPNR.toString());
        assertNotNull(cartPNR);
    }

    /*
	 * /kiosk/api/v1/checkin/carts/ e349e5e4-2c13-4cc3-bf23-76e29c37d4d7?
	 * timeOfRequest=1480575782096& kioskId=MAHMUDFATAF73DA&
	 * uiVersion=3.0.10-495& pos=kiosk& store=mx
	 * 
     */
    @Test
    public void testShoppingCart_AddBagToUS() throws ClassNotFoundException, JsonParseException, IOException {

        String body = FileUtils.readFileAsString("/web/service/ShoppingCartPutTest/testShoppingCart_AddBagToUS.json");
        System.out.println("RQ- CartPNRUpdate: " + body);

        ShoppingCartRQ shoppingCartRQ = new ShoppingCartRQ();
        shoppingCartRQ.setCartId("fc878263-bd8b-4f27-9055-68de3c4760f0");
        shoppingCartRQ.setStore("MX");
        shoppingCartRQ.setPos("KIOSK");
        shoppingCartRQ.setRequest(body);

        CartPNR cartPNR = null;

        try {
            cartPNR = shoppingCartService.putShoppingCart(shoppingCartRQ, serviceCallList);
        } catch (Exception e) {
            System.out.println(e.toString());
            fail();
        }
        System.out.println("reponse - " + cartPNR.toString());
        assertNotNull(cartPNR);
    }

    /*
	 * /checkIn/carts/ad7c6ca2-e4b1-4cdc-9c74-d048f417cd66?timeOfRequest=1507155310245&kioskId=LAB-KIOSK2&uiVersion=3.3.1-667&location=MEX&pos=kiosk&store=mx&paymentEnabled=true
     */
    @Test
    public void testShoppingCart_AddScannedPassport() throws ClassNotFoundException, JsonParseException, IOException {

        String body = FileUtils.readFileAsString("/web/service/ShoppingCartPutTest/testShoppingCart_AddScannedPassport.json");
        System.out.println("RQ- CartPNRUpdate: " + body);

        ShoppingCartRQ shoppingCartRQ = new ShoppingCartRQ();
        shoppingCartRQ.setCartId("ad7c6ca2-e4b1-4cdc-9c74-d048f417cd66");
        shoppingCartRQ.setStore("MX");
        shoppingCartRQ.setPos("KIOSK");
        shoppingCartRQ.setRequest(body);

        CartPNR cartPNR = null;

        try {
            cartPNR = shoppingCartService.putShoppingCart(shoppingCartRQ, serviceCallList);
        } catch (Exception e) {
            System.out.println(e.toString());
            fail();
        }
        System.out.println("reponse - " + cartPNR.toString());
        assertNotNull(cartPNR);
    }

}
