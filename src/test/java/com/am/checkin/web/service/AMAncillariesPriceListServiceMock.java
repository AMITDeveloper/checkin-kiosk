package com.am.checkin.web.service;

import com.aeromexico.sabre.api.ancillaries.AMAncillariesPriceListService;
import com.am.checkin.web.util.CommonTestUtil;
import com.sabre.webservices.pnrbuilder.GetPriceListRQ;
import com.sabre.webservices.pnrbuilder.GetPriceListRS;
import com.sabre.webservices.pnrbuilder.ItineraryCriteria;

import java.io.InputStream;

public class AMAncillariesPriceListServiceMock extends AMAncillariesPriceListService {

    @Override
    public GetPriceListRS ancillaryListPrices(GetPriceListRQ priceListRQ, String cityCode, String aaaComand) throws Exception {

        if(priceListRQ.getItineraryCriteria() != null && !priceListRQ.getItineraryCriteria().isEmpty()) {
            ItineraryCriteria.SegmentCriteria segmentCriteria = priceListRQ.getItineraryCriteria().get(0).getSegmentCriteria().get(0);
            String fileName = segmentCriteria.getDepartureAirportCode() + "_" + segmentCriteria.getArrivalAirportCode() + "_" + segmentCriteria.getClassOfService() + ".xml";

            InputStream ancillaryIS = AMAncillariesPriceListServiceMock.class.getResourceAsStream("ancillaries/" + fileName);

            if ( ancillaryIS == null ) {
    			System.out.println( "**** Input file was not found: " + fileName );
    			return null;
    		}

            return CommonTestUtil.getGetPriceListRS(ancillaryIS);
        }

        return null;
    }

}
