
package com.am.checkin.web.service;

import com.aeromexico.sabre.api.acs.AMAirportFlightList;
import com.aeromexico.commons.model.Airport;
import com.aeromexico.commons.model.AirportCollection;

import com.am.checkin.web.util.CommonTestUtil;
import com.google.gson.Gson;
import com.sabre.services.acs.bso.airportflightlist.v3.ACSAirportFlightListRSACS;
import com.sabre.services.acs.bso.airportflightlist.v3.AirportFlightACS;
import com.sabre.services.acs.bso.airportflightlist.v3.AirportFlightListACS;

public class AMAirportFlightListMock extends AMAirportFlightList {

	private AirportCollection airportCollection = null;

	public AMAirportFlightListMock() {
		try {
			String airportsString = CommonTestUtil.fileToString( AMAirportFlightListMock.class.getResourceAsStream("airports.json") );
			airportCollection = new Gson().fromJson(airportsString, AirportCollection.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public ACSAirportFlightListRSACS airportFlightList(String airlineCode, String departureCity, String departureDate, boolean allFlights) throws Exception {

		ACSAirportFlightListRSACS airportFlightListRSACS = new ACSAirportFlightListRSACS();
		airportFlightListRSACS.setAirportFlightList( new AirportFlightListACS() );

		for ( Airport airport: airportCollection.getCollection() ) {
			AirportFlightACS airportFlightACS = new AirportFlightACS();
			airportFlightACS.setDestination( airport.getCode() );
			airportFlightListRSACS.getAirportFlightList().getAirportFlight().add(airportFlightACS);
		}

		com.sabre.services.stl.v3.ResultRecord resultRecord = new com.sabre.services.stl.v3.ResultRecord();
		resultRecord.setCompletionStatus( com.sabre.services.stl.v3.CompletionCodes.COMPLETE );
		airportFlightListRSACS.setResult(resultRecord);

		return airportFlightListRSACS;
	}

}




