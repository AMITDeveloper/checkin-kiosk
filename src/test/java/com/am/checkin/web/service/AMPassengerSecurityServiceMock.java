
package com.am.checkin.web.service;

import com.aeromexico.sabre.api.acs.AMPassengerSecurityService;
import com.sabre.services.checkin.updatesecurityinfo.v4.UpdateSecurityInfoRQType;
import com.sabre.services.checkin.updatesecurityinfo.v4.UpdateSecurityInfoRSType;
import com.sabre.services.stl.v3.ErrorOrSuccessCode;
import com.sabre.services.stl.v3.ResultRecord;

public class AMPassengerSecurityServiceMock extends AMPassengerSecurityService {

	@Override
	public UpdateSecurityInfoRSType passengerSecurity(UpdateSecurityInfoRQType passengerSecurityRQ) {
		UpdateSecurityInfoRSType response = new UpdateSecurityInfoRSType();
		ResultRecord resultRecord = new ResultRecord();
		resultRecord.setStatus(ErrorOrSuccessCode.SUCCESS);
		response.setResult(resultRecord);
		return response;
	}

}
