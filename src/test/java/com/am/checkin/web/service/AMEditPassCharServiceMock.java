
package com.am.checkin.web.service;

import com.aeromexico.sabre.api.acs.AMEditPassCharService;
import com.sabre.services.acs.bso.editpassengercharacteristics.v3.ACSEditPassengerCharacteristicsRQACS;
import com.sabre.services.acs.bso.editpassengercharacteristics.v3.ACSEditPassengerCharacteristicsRSACS;
import com.sabre.services.stl.v3.ErrorOrSuccessCode;
import com.sabre.services.stl.v3.ResultRecord;

public class AMEditPassCharServiceMock extends AMEditPassCharService {

	@Override
	public ACSEditPassengerCharacteristicsRSACS editPassengerChararcteristics(ACSEditPassengerCharacteristicsRQACS editPassengerCharacteristicsRQ) {
		ACSEditPassengerCharacteristicsRSACS response = new ACSEditPassengerCharacteristicsRSACS();
		ResultRecord resultRecord = new ResultRecord();
		resultRecord.setStatus(ErrorOrSuccessCode.SUCCESS);
		response.setResult(resultRecord);
		return response;
	
	}

}
