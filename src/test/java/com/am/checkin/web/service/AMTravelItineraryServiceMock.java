package com.am.checkin.web.service;

import com.aeromexico.sabre.api.session.common.ServiceCall;
import com.aeromexico.sabre.api.travelitinerary.AMTravelItineraryService;
import com.am.checkin.web.util.CommonTestUtil;
import com.sabre.services.res.tir.travelitinerary.TravelItineraryReadRS;

import java.io.InputStream;
import java.util.List;

public class AMTravelItineraryServiceMock extends AMTravelItineraryService {

    @Override
    public TravelItineraryReadRS TravelItinerary(String recordLocator) throws Exception {
        return TravelItinerary(recordLocator, null);
    }

    @Override
    public TravelItineraryReadRS TravelItinerary(String recordLocator, List<ServiceCall> serviceCallList) throws Exception {
        InputStream travelItineraryIS = getClass().getResourceAsStream(recordLocator + "_TravelItineraryReadRS.xml");
        TravelItineraryReadRS travelItineraryReadRS = CommonTestUtil.getTravelItineraryReadRS(travelItineraryIS);
        return travelItineraryReadRS;
    }

}
