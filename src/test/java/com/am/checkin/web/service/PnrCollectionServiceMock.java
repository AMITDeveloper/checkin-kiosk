package com.am.checkin.web.service;

import com.aeromexico.commons.model.PNRCollection;
import java.io.IOException;

/**
 *
 * @author adrian
 */
public class PnrCollectionServiceMock extends PnrCollectionService {

    @Override
    public PNRCollection createOrUpdatePnrCollectionAfterCheckin(
            PNRCollection pnrCollection, String recordLocator, String cartId, boolean create
    ) throws ClassNotFoundException, IOException {
        return pnrCollection;
    }

}
