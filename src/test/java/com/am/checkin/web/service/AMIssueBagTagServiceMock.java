package com.am.checkin.web.service;

import java.io.InputStream;

import com.aeromexico.sabre.sabre.api.issuebagtag.AMIssueBagTagService;
import com.aeromexico.sabre.sabre.api.issuebagtag.model.IssueBagReq;
import com.am.checkin.web.util.CommonTestUtil;
import com.sabre.services.checkin.issuebagtag.v4.IssueBagTagRSACS;

public class AMIssueBagTagServiceMock extends AMIssueBagTagService {

	/***
	 * Input file from: Airline_Flight_BookingClass_DepartureDate_Origin.xml
	 */
	@Override
	public IssueBagTagRSACS getIssueBagTag(IssueBagReq issueBagReq, String bagTagPrinterCommand, String cityCodeCommandString) {

		String inputFileName = issueBagReq.getBagRouteInfo().getOpCarrier() + "_" +
				issueBagReq.getBagRouteInfo().getOpflightNumber() + "_" +
				issueBagReq.getBagRouteInfo().getBookingClass() + "_" + 
				issueBagReq.getBagRouteInfo().getDepartureDate() + "_" + 
				issueBagReq.getBagRouteInfo().getOrigin() + 
				".xml";

		InputStream passengerDataIS = AMIssueBagTagServiceMock.class.getResourceAsStream( inputFileName );

		if (passengerDataIS == null) {
            System.out.println("**** Input file was not found: " + inputFileName + ".xml");
            return null;
        }
		
		IssueBagTagRSACS issueBagTagRSACS = CommonTestUtil.getIssueBagTagRSACS(passengerDataIS);
		

		return issueBagTagRSACS;
	}

}
