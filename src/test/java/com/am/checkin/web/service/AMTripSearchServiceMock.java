package com.am.checkin.web.service;

import com.aeromexico.sabre.api.session.common.ServiceCall;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.aeromexico.sabre.api.tripsearch.AMTripSearchService;
import java.util.List;

public class AMTripSearchServiceMock extends AMTripSearchService {

    private String timeToReturn = null;

    public AMTripSearchServiceMock() {
    }

    public AMTripSearchServiceMock(String timeToReturn) {
        this.timeToReturn = timeToReturn;
    }

    @Override
    public Date getActualLocalTimeByLocation(String airportIATACode, List<ServiceCall> serviceCallList) {
        return getDateTimeFormated(timeToReturn);
    }

    private Date getDateTimeFormated(String sabreResponse) {
        SimpleDateFormat displayFormat = new SimpleDateFormat("yyyy ddMMM HHmm", Locale.ENGLISH);
        Date dateTransformed = null;
        String rawDateTiem = sabreResponse.replace("*", "").replaceFirst(" ", "").replace(" ", ",");
        String splitedDate[] = rawDateTiem.split(",");
        if (splitedDate.length == 3) {
            try {
                dateTransformed = displayFormat.parse(splitedDate[2] + " " + splitedDate[1] + " " + splitedDate[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return dateTransformed;
    }

    /**
     * @return the timeToReturn
     */
    public String getTimeToReturn() {
        return timeToReturn;
    }

    /**
     * @param timeToReturn the timeToReturn to set
     */
    public void setTimeToReturn(String timeToReturn) {
        this.timeToReturn = timeToReturn;
    }

}
