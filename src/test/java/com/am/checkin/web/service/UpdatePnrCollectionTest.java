package com.am.checkin.web.service;

import com.aeromexico.commons.model.utils.CurrencyUtil;
import org.junit.Test;

import java.math.BigDecimal;

/**
 *
 * @author adrian
 */
public class UpdatePnrCollectionTest {

    @Test
    public void checkBigdecimalFormat() throws Exception {
        BigDecimal number;
        number = CurrencyUtil.getAmount(new BigDecimal("10.9"), "USD");
        System.out.println(number.toString());
        number = CurrencyUtil.getAmount(new BigDecimal("10"), "USD");
        System.out.println(number.toString());
        number = CurrencyUtil.getAmount(new BigDecimal("10.0"), "USD");
        System.out.println(number.toString());
        number = CurrencyUtil.getAmount(new BigDecimal("10.01"), "USD");
        System.out.println(number.toString());
        number = CurrencyUtil.getAmount(new BigDecimal("10."), "USD");
        System.out.println(number.toString());
    }

}
