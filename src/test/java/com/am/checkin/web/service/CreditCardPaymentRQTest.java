package com.am.checkin.web.service;

import com.aeromexico.commons.model.CreditCardPaymentSummary;
import com.am.checkin.web.model.util.CheckPojoStructureTest;
import org.junit.Test;
import static com.am.checkin.web.model.util.CheckPojoStructureTest.gson;
import com.am.checkin.web.util.FileUtils;
import org.skyscreamer.jsonassert.JSONAssert;

/**
 * 
 * @author adrian
 */
public class CreditCardPaymentRQTest extends CheckPojoStructureTest {

    //@Test
    public void checkCreditCardPaymentRequest() throws Exception {
        String expected = FileUtils.readFileAsString("/web/model/creditCardPaymentSummary.json");

        CreditCardPaymentSummary creditCardPaymentSummary = gson.fromJson(expected, CreditCardPaymentSummary.class);

        JSONAssert.assertEquals(expected, gson.toJson(creditCardPaymentSummary), true); // Pass
    }

    @Test
    public void checkCreditCardPaymentRequestBad() throws Exception {
        //contains extra field
        String expected = FileUtils.readFileAsString("/web/model/creditCardPaymentSummary_bad.json");

        CreditCardPaymentSummary creditCardPaymentSummary = gson.fromJson(expected, CreditCardPaymentSummary.class);

        JSONAssert.assertNotEquals(expected, gson.toJson(creditCardPaymentSummary), true); // Pass
    }

    @Test
    public void checkCreditCardPaymentRequestBad2() throws Exception {
        //contains extra field
        String actual = FileUtils.readFileAsString("/web/model/creditCardPaymentSummary_bad.json");

        //just the right fields
        String expected = FileUtils.readFileAsString("/web/model/creditCardPaymentSummary.json");

        JSONAssert.assertNotEquals(expected, actual, true); // Pass
    }

}
