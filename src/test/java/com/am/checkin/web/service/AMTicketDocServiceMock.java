package com.am.checkin.web.service;

import com.aeromexico.sabre.api.session.common.ServiceCall;
import java.io.InputStream;

import com.aeromexico.sabre.api.ticketdoc.AMTicketingDocumentService;
import com.am.checkin.web.util.CommonTestUtil;
import com.sabre.ns.ticketing.dc.GetTicketingDocumentRS;
import java.util.List;

public class AMTicketDocServiceMock extends AMTicketingDocumentService {

    @Override
    public GetTicketingDocumentRS ticketDocByVCR(String vcr, List<ServiceCall> serviceCallList) throws Exception {

        InputStream ticketingDocumentIS = AMPassengerDataServiceMock.class.getResourceAsStream("vcr_" + vcr + ".xml");
        if (ticketingDocumentIS == null) {
            System.out.println("**** Input file was not found: vcr_" + vcr + ".xml");
            return null;
        }

        GetTicketingDocumentRS getTicketingDocumentRS = CommonTestUtil.getGetTicketingDocumentRS(ticketingDocumentIS);

        return getTicketingDocumentRS;
    }

}
