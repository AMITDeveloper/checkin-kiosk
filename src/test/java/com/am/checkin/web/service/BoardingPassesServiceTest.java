package com.am.checkin.web.service;

import com.aeromexico.commons.model.*;
import com.aeromexico.commons.model.rq.BoardingPassesRQ;
import com.aeromexico.commons.web.types.SeatSectionCodeType;
import com.aeromexico.dao.services.IPNRLookupDao;
import com.aeromexico.dao.test.EnvironmentTest;
import com.aeromexico.sabre.api.acs.AMReprintBPService;
import com.aeromexico.sabre.api.models.dcci.PassengerDetailsResponse;
import com.aeromexico.sabre.api.sabrecommand.AMSabreCommandService;
import com.aeromexico.sabre.api.session.SessionCreateService;
import com.aeromexico.sabre.api.sessionpool.config.Mgr;
import com.aeromexico.sabre.api.sessionpool.factory.SabreSessionFactory;
import com.aeromexico.sabre.api.sessionpool.manager.SabreSessionPoolManager;
import com.aeromexico.sabre.api.sessionpool.service.SabreSessionService;
import com.am.checkin.web.dao.PNRLookupDaoMock;
import com.google.gson.Gson;
import com.sabre.services.acs.bso.reprintbp.v3.ItineraryACS;
import com.sabre.services.acs.bso.reprintbp.v3.PassengerInfoACS;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import static org.junit.Assert.fail;

/**
 * @author Carlos Luna
 */
@Ignore
public class BoardingPassesServiceTest extends EnvironmentTest {

    private IPNRLookupDao pnrLookupDao;

    private BoardingPassesService boardingService;

    private BoardingPassesRQ boardingPassesRQ = new BoardingPassesRQ();

    private PNRCollection pnrCollection;

    private PNR pnr;

    @Before
    public void init() {
        SystemProperties systemProperties = setUpConfigFactory.getSystemPropertiesFactory().getInstance();

        pnrLookupDao = new PNRLookupDaoMock();

        boardingService = new BoardingPassesService();
        boardingService.setSetUpConfigFactory(setUpConfigFactory);

        AMReprintBPService reprintService = new AMReprintBPService();
        Mgr mgr = new Mgr(false);
        mgr.setDefaultEndpoint(systemProperties.getSabreEndpoint());
        mgr.setUsername(systemProperties.getSabreEprUser());
        mgr.setPassword(systemProperties.getSabreEprPassword());
        mgr.setAgentId(systemProperties.getSabreAgentId());
        SabreSessionPoolManager sabreSessionPoolManager = new SabreSessionPoolManager();
        SabreSessionFactory sabreSessionFactory = new SabreSessionFactory();
        SessionCreateService sessionCreateService = new SessionCreateService();
        sessionCreateService.init();
        SabreSessionService sabreSessionService = new SabreSessionService();
        sabreSessionService.setMgr(mgr);
        sabreSessionService.setSessionCreateService(sessionCreateService);
        sabreSessionFactory.setSabreSessionService(sabreSessionService);
        sabreSessionPoolManager.setSabreSessionFactory(sabreSessionFactory);
        sabreSessionPoolManager.init();
        reprintService.setMgr(mgr);
        AMSabreCommandService sabreCommandService = new AMSabreCommandService();
        sabreCommandService.setMgr(mgr);
        sabreCommandService.setSabreSessionPoolManager(sabreSessionPoolManager);
        sabreCommandService.init();
        reprintService.setSabreCommandService(sabreCommandService);
        reprintService.setSabreSessionPoolManager(sabreSessionPoolManager);
        reprintService.init();
        boardingService.setPnrLookupDao(pnrLookupDao);
        boardingService.setReprintService(reprintService);
        boardingPassesRQ.setPos("WEB");
        boardingPassesRQ.setStore("MEX");
        boardingPassesRQ.setLanguage("ES");
        boardingPassesRQ.setLegCode("MEX_AM_0174_2016-06-22");


        //DigitalSignatureSabreClient digitalSignatureSabreClient = new DigitalSignatureSabreClient();
        //boardingService.setDigitalSignatureSabreClient(digitalSignatureSabreClient);
//        List<String> arrayPax = new ArrayList<>();
//        arrayPax.add("16853AA90002");
//        arrayPax.add("16853AA90001");
//        boardingPassesRQ.setPaxId(arrayPax);
        String pos = "WEB";
        boardingPassesRQ.setRecordLocator("HVJSDK");

        try {
            pnrCollection = boardingService.findInMongoPnrCollection(boardingPassesRQ.getRecordLocator(), pos);
            pnr = pnrCollection.getPNRByRecordLocator(boardingPassesRQ.getRecordLocator());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetPnr() {
        Assert.assertNotNull("Cannot find the PNR", pnr);
        Assert.assertNotNull("The PNR don't contains any cart", pnr.getCarts());
        Assert.assertNotNull("The PNR don't contains any leg", pnr.getLegs());
    }

    @Test
    public void testBoardingPasses() {
        BoardingPassCollection boardingPasses = null;
        try {
            boardingPasses = boardingService.getBoardingPasses(boardingPassesRQ);
        } catch (Exception e) {
            System.out.println("Error to get Boarding Passes");
            e.printStackTrace();
        }
//        System.out.println("Boarding Response: " + boardingPasses.toString());
//        System.out.println("Starting with the asserts");
//        Assert.assertNotNull(boardingPasses);
//        for (BoardingPass boardingPass : boardingPasses.getCollection()) {
//            System.out.println("Validating objects");
//			Assert.assertNotNull(boardingPass.getBarcodeImage());
//			Assert.assertNotNull(boardingPass.getQrCodeImage());
//			Assert.assertNotNull(boardingPass.getSegmentStatus());
//        }
        Assert.assertNull(boardingPasses);
    }

    @Test
    public void testSegmentStatusValid() {
        SegmentStatus segmetStatus = null;
        for (BookedLeg bookedLeg : pnr.getLegs().getCollection()) {
            for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {
                try {
                    segmetStatus = boardingService.getSegmetStatus(bookedLeg, bookedSegment.getSegment().getSegmentCode());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Assert.assertNotNull("The segmentStatus is null", segmetStatus);
                Assert.assertNotNull("The BoardingTerminal is null", segmetStatus.getBoardingTerminal());
                Assert.assertNotNull("The BoardingTime is null", segmetStatus.getBoardingTime());
                Assert.assertNotNull("The EstimatedDepartureTime is null", segmetStatus.getEstimatedDepartureTime());
                Assert.assertNotNull("The Status is null", segmetStatus.getStatus());
                Assert.assertNotNull("The Segment not contain any segment", segmetStatus.getSegment());
            }
        }
    }

    @Test
    public void testSegmentData() {
        SegmentStatus segmetStatus = null;
        for (BookedLeg bookedLeg : pnr.getLegs().getCollection()) {
            for (BookedSegment bookedSegment : bookedLeg.getSegments().getCollection()) {
                try {
                    segmetStatus = boardingService.getSegmetStatus(bookedLeg, bookedSegment.getSegment().getSegmentCode());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Assert.assertNotNull("The segmentStatus is null", segmetStatus);
                Assert.assertNotNull("The segment is null", segmetStatus.getSegment());
                Assert.assertNotNull("The AircraftType is null", segmetStatus.getSegment().getAircraftType());
                Assert.assertNotNull("The ArrivalAirport is null", segmetStatus.getSegment().getArrivalAirport());
                Assert.assertNotNull("The DepartureAirport is null", segmetStatus.getSegment().getDepartureAirport());
                Assert.assertNotNull("The DepartureDateTime is null", segmetStatus.getSegment().getDepartureDateTime());
                Assert.assertNotNull("The FlightDurationInMinutes is null", segmetStatus.getSegment().getFlightDurationInMinutes());
                Assert.assertNotNull("The MarketingCarrier is null", segmetStatus.getSegment().getMarketingCarrier());
                Assert.assertNotNull("The MarketingFlightCode is null", segmetStatus.getSegment().getMarketingFlightCode());
                Assert.assertNotNull("The OperatingCarrier is null", segmetStatus.getSegment().getOperatingCarrier());
                Assert.assertNotNull("The OperatingFlightCode is null", segmetStatus.getSegment().getOperatingFlightCode());
                Assert.assertNotNull("The SegmentCode is null", segmetStatus.getSegment().getSegmentCode());
            }
        }
    }

    @Test
    @Ignore
    public void testBoardingSelected() {
        for (BookedLeg bookedLeg : pnr.getLegs().getCollection()) {
            for (CartPNR cartPnr : pnr.getCarts().getCollection()) {
                BoardingPassCollection boardingPasses = null;
                try {
                    boardingPasses = boardingService.getKioskBoardingSelected(boardingPassesRQ, bookedLeg, cartPnr, null);
                } catch (Exception e) {
                    System.out.println("Error to find the passengers selected");
                    e.printStackTrace();
                }
                Assert.assertNotNull("The boarding collection is empty", boardingPasses);
                Assert.assertTrue("The collection don't have elements", !boardingPasses.getCollection().isEmpty());
                for (BoardingPass boarding : boardingPasses.getCollection()) {
//					Assert.assertNotNull("The boarding element don't have barcode", boarding.getBarcode());
//					Assert.assertNotNull("The boarding element don't have pectabFormat", boarding.getPectabFormat());
//					Assert.assertNotNull("The boarding element don't have FirstName", boarding.getFirstName());
                    Assert.assertNotNull("The boarding element don't have LastName", boarding.getLastName());
                    Assert.assertNotNull("The boarding element don't have ControlCode", boarding.getControlCode());
                    Assert.assertNotNull("The boarding element don't have TicketNumber", boarding.getTicketNumber());

                }
            }
        }
    }

    @Test
    @Ignore
    public void testListPassengers() {
        List<PassengerInfoACS> listPassengerInfo = null;
        for (CartPNR cartPNR : pnr.getCarts().getCollection()) {
            for (BookedLeg bookedLeg : pnr.getLegs().getCollection()) {
                for (BookedSegment segment : bookedLeg.getSegments().getCollection()) {
                    try {
                        listPassengerInfo = boardingService.getListPassengerInfo(cartPNR, boardingPassesRQ, segment.getSegment().getSegmentCode());
                    } catch (Exception e) {
                        System.out.println("Error to get the passenger info list");
                        e.printStackTrace();
                    }
                    Assert.assertNotNull("The passenger collection is empty", listPassengerInfo);
                    Assert.assertTrue("The passenger list don't have elements", !listPassengerInfo.isEmpty());
                    for (PassengerInfoACS passenger : listPassengerInfo) {
                        Assert.assertNotNull("The Lastname passenger is empty", passenger.getLastName());
                        Assert.assertNotNull("The passenger id is empty", passenger.getPassengerID());
                    }
                }
            }
        }
    }

    @Test
    public void testItineraryCollection() {
        List<ItineraryACS> itineraryFromPNR = null;
        for (BookedLeg bookedLeg : pnr.getLegs().getCollection()) {
            try {
                itineraryFromPNR = boardingService.getItineraryFromPNR(pnr, bookedLeg.getSegments().getLegCode());
            } catch (Exception e) {
                System.out.println("Cannot get the itinerary");
                e.printStackTrace();
            }
            Assert.assertNotNull("The itinerary collection is empty", itineraryFromPNR);
            Assert.assertTrue("The itinerary list don't have elements", !itineraryFromPNR.isEmpty());
            for (ItineraryACS itinerary : itineraryFromPNR) {
                Assert.assertNotNull("The airline is empty", itinerary.getAirline());
                Assert.assertNotNull("The departuredate id is empty", itinerary.getDepartureDate());
                Assert.assertNotNull("The destination id is empty", itinerary.getDestination());
                Assert.assertNotNull("The flight id is empty", itinerary.getFlight());
                Assert.assertNotNull("The origin id is empty", itinerary.getOrigin());
            }
        }
    }

    @Test
    public void testValidateReprint() {
        WarningCollection warningCollection = new WarningCollection();
        CartPNR reprintInformation = new CartPNR();
        for (CartPNR cartPNR : pnr.getCarts().getCollection()) {
            try {
                boardingService.getReprintInformation(
                        pnr, boardingPassesRQ, cartPNR, warningCollection
                );
            } catch (Exception e) {
                System.out.println("Cannot get the complete cart information");
                e.printStackTrace();
            }
            Assert.assertNull("The cart collection is empty", reprintInformation);
//			Assert.assertNotNull("The itinerary list don't have elements", reprintInformation.getTravelerInfo());
//			for(BookedTraveler bookedTraveler: reprintInformation.getTravelerInfo().getCollection()) {
//				Assert.assertNotNull("The SegmentDocument list don't have elements", bookedTraveler.getSegmentDocumentsList());
//				Assert.assertNotNull("The ticketnummber is null", bookedTraveler.getTicketNumber());
//				Assert.assertNotNull("The LastName is null", bookedTraveler.getLastName());
//				Assert.assertNotNull("The FirstName is null", bookedTraveler.getFirstName());
//				Assert.assertNotNull("The BookingClasses list don't have elements", bookedTraveler.getBookingClasses());
//				Assert.assertNotNull("The Id is null", bookedTraveler.getId());
//			}
        }
    }

    @Test
    public void testFillDataBoarding() {
        List<BoardingPassWeb> fillDataBoardingPass = null;
        for (CartPNR cartPnr : pnr.getCarts().getCollection()) {
            for (BookedLeg bookedLeg : pnr.getLegs().getCollection()) {
                for (BookedTraveler bookedTraveler : cartPnr.getTravelerInfo().getCollection()) {
                    try {
                        fillDataBoardingPass = boardingService.fillDataBoardingPassForWeb(bookedTraveler, boardingPassesRQ.getPos(), bookedLeg, null);
                    } catch (Exception e) {
                        System.out.println("Cannot get the boarding passes information");
                        e.printStackTrace();
                    }
                    Assert.assertNotNull("The boarding collection is empty", fillDataBoardingPass);
                    Assert.assertTrue("The collection don't have elements", fillDataBoardingPass.isEmpty());
                    for (BoardingPass boarding : fillDataBoardingPass) {
//						Assert.assertNotNull("The boarding element don't have barcode", boarding.getBarcode());
//						Assert.assertNotNull("The boarding element don't have pectabFormat", boarding.getPectabFormat());
                        Assert.assertNotNull("The boarding element don't have FirstName", boarding.getFirstName());
                        Assert.assertNotNull("The boarding element don't have LastName", boarding.getLastName());
                        Assert.assertNotNull("The boarding element don't have ControlCode", boarding.getControlCode());
                        Assert.assertNotNull("The boarding element don't have TicketNumber", boarding.getTicketNumber());

                    }
                }
            }
        }

    }

    @Test
    public void testFillInfant() {
        for (CartPNR cartPnr : pnr.getCarts().getCollection()) {
            for (BookedTraveler bookedTraveler : cartPnr.getTravelerInfo().getCollection()) {
                if (bookedTraveler.getInfant() == null) {
                    Assert.assertNull("The infant collection is not null empty", bookedTraveler.getInfant());
                } else {
                    Assert.assertNotNull("The infant collection is empty", bookedTraveler.getInfant());
                    Assert.assertTrue("The collection for infant don't have elements", !bookedTraveler.getInfant().getSegmentDocumentsList().isEmpty());
//					int index = 0;
//					BoardingPass fillDataInfant = null;
//					for (BookingClass bookingClass : bookedTraveler.getBookingClasses().getCollection()) {
//						try {
//							fillDataInfant = boardingService.fillDataInfant(bookedTraveler, bookingClass.getSegmentCode(), index);
//						} catch (Exception e) {
//							System.out.println("The Infant information is missing");
//							e.printStackTrace();
//						}
//						index++;
//						Assert.assertNotNull("The boarding element don't have barcode", fillDataInfant.getBarcode());
//						Assert.assertNotNull("The boarding element don't have pectabFormat", fillDataInfant.getPectabFormat());
//						Assert.assertNotNull("The boarding element don't have FirstName", fillDataInfant.getFirstName());
//						Assert.assertNotNull("The boarding element don't have LastName", fillDataInfant.getLastName());
//						Assert.assertNotNull("The boarding element don't have ControlCode", fillDataInfant.getControlCode());
//						Assert.assertNotNull("The boarding element don't have TicketNumber", fillDataInfant.getTicketNumber());
//					}
                }
            }
        }
    }

    @Test
    public void testSeatInformation() {
        for (CartPNR cartPnr : pnr.getCarts().getCollection()) {
            for (BookedTraveler bookedTraveler : cartPnr.getTravelerInfo().getCollection()) {
                for (BookingClass bookingClass : bookedTraveler.getBookingClasses().getCollection()) {
                    Seat seatFromCart = boardingService.getSeatFromCart(bookedTraveler.getSegmentChoices().getCollection(), bookingClass.getSegmentCode());
                    if (boardingService.isFirstClassCabin(bookedTraveler.getBookingClasses().getCollection().get(0).getBookingCabin())) {
                        seatFromCart.setSectionCode(SeatSectionCodeType.FIRST_CLASS);
                    } else {
                        seatFromCart.setSectionCode(SeatSectionCodeType.COACH);
                    }
                    Assert.assertNotNull("The characteristics for seat are null", seatFromCart.getSeatCharacteristics());
                    Assert.assertTrue("The collection don't have elements", !seatFromCart.getSeatCharacteristics().isEmpty());
                    Assert.assertNotNull("The seatcode element is null", seatFromCart.getSeatCode());
                    Assert.assertNotNull("The section code is null", seatFromCart.getSectionCode());
                }
            }
        }
    }

    @Test
    public void testMultipleCheckIns() {
        BoardingPassResponse webBoardingPasses = null;
        boardingPassesRQ.setPos("WEB");
        try {
            webBoardingPasses = boardingService.getWebBoardingPasses(
                    boardingPassesRQ,
                    boardingService.getPnrCollection(boardingPassesRQ.getRecordLocator(), boardingPassesRQ.getPos())
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assert.assertNull("The boarding collection is empty", webBoardingPasses);
    }

    @Test
    public void testDigitalSignatureService() {
        BoardingPassesService service = new BoardingPassesService();
        try {
            PassengerDetailsResponse response = new Gson().fromJson("{\"reservation\":{\"id\":\"res1\",\"version\":\"500ac36aca626caed20c4d0b913788a14b0f1b92cc0f5cc7\",\"passengers\":{\"passenger\":[{\"id\":\"p01.01\",\"syntheticIdentifier\":\"PsWBFKQNHRlzjzcNQjbStFzaBBjnO+xLAzgM29b3VgK3B2+xP+uxNEC5sK1LpO0z\",\"personName\":{\"first\":\"JOHN\",\"last\":\"DOE\",\"raw\":\"DOE/JOHN\"},\"type\":{\"value\":\"ADULT\"},\"contactDetails\":{\"address\":[{\"id\":\"a1\",\"country\":\"US\",\"type\":\"RESIDENCE\"}]},\"emergencyContact\":[{}],\"passengerDocument\":[{\"document\":{\"id\":\"id1\",\"number\":\"G123123123\",\"personName\":{\"first\":\"JOHN\",\"last\":\"DOE\"},\"nationality\":\"US\",\"dateOfBirth\":\"1990-04-22\",\"issuingCountry\":\"US\",\"expiryDate\":\"2030-01-01\",\"gender\":\"MALE\",\"type\":\"PASSPORT\"}}],\"ticket\":[{\"ticketNumber\":{\"number\":\"1392115746422\"},\"issued\":true,\"ticketCoupon\":[{\"couponNumber\":\"1\",\"fareBasisCode\":\"Y912T2\",\"segmentRef\":\"s1\",\"status\":\"OK\"}]}],\"passengerSegments\":{\"passengerSegment\":[{\"id\":\"p01.01.s1\",\"passengerFlight\":[{\"id\":\"p01.01.s1.f1\",\"flightRefs\":[\"f1\"],\"seat\":{\"value\":\"10C\"},\"checkedIn\":true,\"boardingPass\":{\"source\":\"HOST\",\"priorityVerificationCard\":false,\"recordLocator\":\"CLLXNR\",\"ticketNumber\":{\"airlineAccountingCode\":\"139\",\"serialNumber\":\"2115746422\",\"checkDigit\":\"5\",\"number\":\"13921157464225\"},\"ticketCouponNumber\":\"1\",\"personName\":{\"first\":\"JOHN\",\"last\":\"DOE\"},\"flightDetail\":{\"id\":\"bpf1\",\"estimatedDepartureTime\":\"2020-05-28T12:10:00-07:00\",\"estimatedArrivalTime\":\"2020-05-28T17:55:00-05:00\",\"departureFlightScheduleStatus\":\"ON_TIME\",\"boardingTime\":\"2020-05-28T11:25:00-07:00\",\"departureTerminal\":\"2\",\"departureGate\":\"26\",\"departureCountry\":\"US\",\"arrivalCountry\":\"MX\",\"commuter\":false,\"airline\":\"AM\",\"flightNumber\":\"645\",\"departureAirport\":\"LAX\",\"departureTime\":\"2020-05-28T12:10:00-07:00\",\"arrivalAirport\":\"MEX\",\"arrivalTime\":\"2020-05-28T17:55:00-05:00\",\"operatingAirline\":\"AM\",\"operatingFlightNumber\":\"645\",\"equipment\":\"738\"},\"fareInfo\":{\"bookingClass\":\"Y\"},\"seat\":{\"value\":\"10C\"},\"deck\":{\"value\":\"MAIN\",\"code\":\"L\"},\"group\":\"4\",\"zone\":\"4\",\"checkInSequenceNumber\":\"00001\",\"barCode\":\"M1DOE/JOHN            ECLLXNR LAXMEXAM 0645 149Y010C0001 11F>30B  M     AM 0E             0^160MEUCIQD0zRXToT6wmlKCeUXii2v6PuHUr6kpD/NujEeQ9PQFmwIgBaJ/YlI3DoHFxc7stBgUXFhSGwjEAZZOwRaE8fMIQNY=\",\"formattedBoardingPass\":{},\"agent\":{\"sign\":\"WCI\",\"city\":\"HDQ\",\"country\":\"US\"},\"supplementaryData\":{\"dhsStatus\":0,\"exclusiveWaitingArea\":false,\"loungeAccess\":false},\"displayData\":{\"operatingAirlineName\":\"AEROMEXICO\",\"boardingTime\":\"112500\",\"estimatedDepartureDate\":\"2020-05-28\",\"estimatedDepartureTime\":\"121000\",\"departureAirportName\":\"LOS ANGELES\",\"scheduledArrivalTime\":\"175500\",\"arrivalAirportName\":\"MEXICO CITY\",\"ticketTypeText\":\"ELECTRONIC\",\"documentTypeText\":\"BOARDING PASS\",\"pingTipText\":\"N\",\"premiumText\":\"PREMIUM\",\"agentCityName\":\"LOS ANGELES\"}}}],\"airExtra\":[{\"id\":\"ae1\",\"syntheticIdentifier\":\"cfVKNA92/qoWW/bbxBPVHA==\",\"ancillary\":{\"id\":\"anc1\",\"group\":\"SA\",\"reasonForIssuance\":{\"value\":\"AIR_TRANSPORTATION\",\"code\":\"A\"},\"commercialName\":\"ASIENTO PAGADO PAIDSEAT\",\"airline\":\"AM\",\"vendor\":\"ATP\",\"electronicMiscDocType\":{\"value\":\"FLIGHT_COUPON_ASSOCIATED\",\"code\":\"2\"},\"ancillaryRules\":{\"commissionable\":false,\"interlineable\":false,\"paperTicketRequired\":false},\"subCode\":\"0B5\"},\"paymentStatus\":{\"value\":\"FULFILLED\",\"statusCode\":\"HI\"},\"specialServiceDescription\":{\"code\":\"SEAT\"},\"type\":\"ANCILLARY\",\"quantity\":1}],\"segmentRef\":\"s1\"}]},\"eligibilities\":{\"eligibility\":[{\"reason\":[{\"category\":\"ALLOWED_BAG_TAG_ISSUANCE_PRIOR_TO_PAYMENT\",\"message\":\"Passenger is eligible to have bag tag issued prior to payment\"}],\"type\":\"BAGGAGE_CHECK_IN\",\"notEligible\":false}]},\"weightCategory\":\"ADULT_MALE\",\"fiscalCode\":\"ADT\",\"nameNumber\":\"01.01\"},{\"id\":\"p02.01\",\"syntheticIdentifier\":\"+oNZyZSnbwH1eKXsL2ziVVzaBBjnO+xLAzgM29b3VgIzlI/nlIxITlp+SslhImVq\",\"personName\":{\"first\":\"JANE\",\"last\":\"DOE\",\"raw\":\"DOE/JANE\"},\"type\":{\"value\":\"ADULT\"},\"contactDetails\":{\"address\":[{\"id\":\"a2\",\"country\":\"US\",\"type\":\"RESIDENCE\"}]},\"emergencyContact\":[{}],\"passengerDocument\":[{\"document\":{\"id\":\"id2\",\"number\":\"G123123123\",\"personName\":{\"first\":\"JANE\",\"last\":\"DOE\"},\"nationality\":\"US\",\"dateOfBirth\":\"1995-01-13\",\"issuingCountry\":\"US\",\"expiryDate\":\"2030-01-01\",\"gender\":\"FEMALE\",\"type\":\"PASSPORT\"}}],\"ticket\":[{\"ticketNumber\":{\"number\":\"1392115746423\"},\"issued\":true,\"ticketCoupon\":[{\"couponNumber\":\"1\",\"fareBasisCode\":\"Y912T2\",\"segmentRef\":\"s1\",\"status\":\"OK\"}]}],\"passengerSegments\":{\"passengerSegment\":[{\"id\":\"p02.01.s1\",\"passengerFlight\":[{\"id\":\"p02.01.s1.f1\",\"flightRefs\":[\"f1\"],\"seat\":{\"value\":\"10D\"},\"checkedIn\":true,\"boardingPass\":{\"source\":\"HOST\",\"priorityVerificationCard\":false,\"recordLocator\":\"CLLXNR\",\"ticketNumber\":{\"airlineAccountingCode\":\"139\",\"serialNumber\":\"2115746423\",\"checkDigit\":\"6\",\"number\":\"13921157464236\"},\"ticketCouponNumber\":\"1\",\"personName\":{\"first\":\"JANE\",\"last\":\"DOE\"},\"flightDetail\":{\"id\":\"bpf2\",\"estimatedDepartureTime\":\"2020-05-28T12:10:00-07:00\",\"estimatedArrivalTime\":\"2020-05-28T17:55:00-05:00\",\"departureFlightScheduleStatus\":\"ON_TIME\",\"boardingTime\":\"2020-05-28T11:25:00-07:00\",\"departureTerminal\":\"2\",\"departureGate\":\"26\",\"departureCountry\":\"US\",\"arrivalCountry\":\"MX\",\"commuter\":false,\"airline\":\"AM\",\"flightNumber\":\"645\",\"departureAirport\":\"LAX\",\"departureTime\":\"2020-05-28T12:10:00-07:00\",\"arrivalAirport\":\"MEX\",\"arrivalTime\":\"2020-05-28T17:55:00-05:00\",\"operatingAirline\":\"AM\",\"operatingFlightNumber\":\"645\",\"equipment\":\"738\"},\"fareInfo\":{\"bookingClass\":\"Y\"},\"seat\":{\"value\":\"10D\"},\"deck\":{\"value\":\"MAIN\",\"code\":\"L\"},\"group\":\"4\",\"zone\":\"4\",\"checkInSequenceNumber\":\"00002\",\"barCode\":\"M1DOE/JANE            ECLLXNR LAXMEXAM 0645 149Y010D0002 11F>30B  M     AM 0E             0^160MEQCIEsPpYOhVRa4vdh4fNMO42LAqj/nj0Bvb72BNLp+wPA4AiBYYVgWxKpB9WEEj0LxzIQzLTXQOoyMLKUB+8TqUqmIJA==\",\"formattedBoardingPass\":{},\"agent\":{\"sign\":\"WCI\",\"city\":\"HDQ\",\"country\":\"US\"},\"supplementaryData\":{\"dhsStatus\":0,\"exclusiveWaitingArea\":false,\"loungeAccess\":false},\"displayData\":{\"operatingAirlineName\":\"AEROMEXICO\",\"boardingTime\":\"112500\",\"estimatedDepartureDate\":\"2020-05-28\",\"estimatedDepartureTime\":\"121000\",\"departureAirportName\":\"LOS ANGELES\",\"scheduledArrivalTime\":\"175500\",\"arrivalAirportName\":\"MEXICO CITY\",\"ticketTypeText\":\"ELECTRONIC\",\"documentTypeText\":\"BOARDING PASS\",\"pingTipText\":\"N\",\"premiumText\":\"PREMIUM\",\"agentCityName\":\"LOS ANGELES\"}}}],\"airExtra\":[{\"id\":\"ae2\",\"syntheticIdentifier\":\"TGwGn+YY0z0qyfBp1xvRuA==\",\"ancillary\":{\"id\":\"anc2\",\"group\":\"SA\",\"reasonForIssuance\":{\"value\":\"AIR_TRANSPORTATION\",\"code\":\"A\"},\"commercialName\":\"ASIENTO PAGADO PAIDSEAT\",\"airline\":\"AM\",\"vendor\":\"ATP\",\"electronicMiscDocType\":{\"value\":\"FLIGHT_COUPON_ASSOCIATED\",\"code\":\"2\"},\"ancillaryRules\":{\"commissionable\":false,\"interlineable\":false,\"paperTicketRequired\":false},\"subCode\":\"0B5\"},\"paymentStatus\":{\"value\":\"FULFILLED\",\"statusCode\":\"HI\"},\"specialServiceDescription\":{\"code\":\"SEAT\"},\"type\":\"ANCILLARY\",\"quantity\":1}],\"segmentRef\":\"s1\"}]},\"eligibilities\":{\"eligibility\":[{\"reason\":[{\"category\":\"ALLOWED_BAG_TAG_ISSUANCE_PRIOR_TO_PAYMENT\",\"message\":\"Passenger is eligible to have bag tag issued prior to payment\"}],\"type\":\"BAGGAGE_CHECK_IN\",\"notEligible\":false}]},\"weightCategory\":\"ADULT_FEMALE\",\"fiscalCode\":\"ADT\",\"nameNumber\":\"02.01\"}]},\"itinerary\":{\"itineraryPart\":[{\"id\":\"ip1\",\"segment\":[{\"id\":\"s1\",\"status\":{\"value\":\"CONFIRMED\",\"code\":\"HK\"},\"flightDetail\":[{\"id\":\"f1\",\"estimatedDepartureTime\":\"2020-05-28T12:10:00-07:00\",\"estimatedArrivalTime\":\"2020-05-28T17:55:00-05:00\",\"departureFlightScheduleStatus\":\"ON_TIME\",\"boardingTime\":\"2020-05-28T11:25:00-07:00\",\"departureTerminal\":\"2\",\"departureGate\":\"26\",\"departureCountry\":\"US\",\"arrivalCountry\":\"MX\",\"commuter\":false,\"airline\":\"AM\",\"flightNumber\":\"645\",\"departureAirport\":\"LAX\",\"departureTime\":\"2020-05-28T12:10:00-07:00\",\"arrivalAirport\":\"MEX\",\"arrivalTime\":\"2020-05-28T17:55:00-05:00\",\"operatingAirline\":\"AM\",\"operatingAirlineName\":\"AEROMEXICO\",\"operatingFlightNumber\":\"645\",\"equipment\":\"738\"}],\"fareInfo\":{\"bookingClass\":\"Y\"},\"baggageCheckInRules\":{\"petAllowed\":true,\"lateCheckIn\":false,\"homePrintedBagTagRestricted\":{\"type\":\"RESTRICTED\",\"message\":\"Home Printed Bag Tag is not allowed for given airline.\"}},\"number\":\"3\"}],\"type\":\"OUTBOUND\"}]},\"pricing\":{\"airExtraPricings\":{\"airExtraPricing\":[{\"id\":\"apr1\",\"airExtraRefs\":[\"ae1\"],\"pricingLevel\":\"SEGMENT\",\"unitFee\":{\"total\":{\"equivAmount\":{\"value\":0,\"currency\":\"MXN\"}},\"base\":{\"amount\":{\"value\":0,\"currency\":\"MXN\"},\"equivAmount\":{\"value\":0,\"currency\":\"MXN\"}}},\"totalFee\":{\"total\":{\"equivAmount\":{\"value\":0,\"currency\":\"MXN\"}},\"base\":{\"amount\":{\"value\":0,\"currency\":\"MXN\"},\"equivAmount\":{\"value\":0,\"currency\":\"MXN\"}}}},{\"id\":\"apr2\",\"airExtraRefs\":[\"ae2\"],\"pricingLevel\":\"SEGMENT\",\"unitFee\":{\"total\":{\"equivAmount\":{\"value\":0,\"currency\":\"MXN\"}},\"base\":{\"amount\":{\"value\":0,\"currency\":\"MXN\"},\"equivAmount\":{\"value\":0,\"currency\":\"MXN\"}}},\"totalFee\":{\"total\":{\"equivAmount\":{\"value\":0,\"currency\":\"MXN\"}},\"base\":{\"amount\":{\"value\":0,\"currency\":\"MXN\"},\"equivAmount\":{\"value\":0,\"currency\":\"MXN\"}}}}]}},\"recordLocator\":\"CLLXNR\"},\"results\":[{\"designator\":{\"value\":\"p01.01.s1\",\"context\":\"PASSENGER_SEGMENT\"},\"status\":[{\"message\":\"Boarding pass reprint is successful. \",\"type\":\"SUCCESS\"}]},{\"designator\":{\"value\":\"p02.01.s1\",\"context\":\"PASSENGER_SEGMENT\"},\"status\":[{\"message\":\"Boarding pass reprint is successful. \",\"type\":\"SUCCESS\"}]}]}", PassengerDetailsResponse.class);
            PNRCollection pnrCollection = new PNRCollection("{\"_warnings\":{\"_meta\":{\"class\":\"WarningCollection\"},\"_collection\":[]},\"_meta\":{\"class\":\"PNRCollection\"},\"_collection\":[{\"_meta\":{\"class\":\"PNR\"},\"pnr\":\"QXBPDR\",\"carts\":{\"_meta\":{\"class\":\"CartPNRCollection\"},\"_collection\":[{\"_meta\":{\"class\":\"CartPNR\",\"cartId\":\"fa588b74-2192-43b1-aff3-222f036146ba\",\"cartExpirationTimeInSeconds\":900,\"total\":{\"currency\":{\"_meta\":{\"class\":\"CurrencyValue\"},\"currencyCode\":\"MXN\",\"base\":0,\"taxDetails\":{},\"totalTax\":0,\"total\":0},\"points\":0}},\"legCode\":\"LAX_AM_0643_2017-09-29\",\"travelersCode\":\"A1_C0_I0_PH0_PC0\",\"travelerInfo\":{\"_meta\":{\"class\":\"BookedTravelerCollection\"},\"_collection\":[{\"_meta\":{\"class\":\"BookedTraveler\"},\"checkinStatus\":true,\"isEligibleToCheckin\":true,\"isSelectedToCheckin\":false,\"missingCheckinRequiredFields\":[],\"infant\":null,\"paxType\":\"ADULT\",\"couponCode\":\"3\",\"ticketNumber\":\"1398643351054\",\"ancillaries\":{\"_meta\":{\"class\":\"BookedTravelerAncillaryCollection\"},\"_collection\":[{\"type\":\"0CD\",\"groupCode\":\"BG\",\"used\":false,\"emdConsummedAtIssuance\":null,\"quantity\":1,\"legCode\":\"LAX_AM_0643_2017-09-29\",\"_meta\":{\"class\":\"PaidTravelerAncillary\"}}]},\"segmentChoices\":{\"_meta\":{\"class\":\"BookedSegmentChoiceCollection\"},\"_collection\":[{\"_meta\":{\"class\":\"PaidSegmentChoice\"},\"seat\":{\"subjectToSeatSelectionFee\":false,\"_meta\":{\"class\":\"SeatChoice\"},\"code\":\"10A\",\"seatCharacteristics\":[]},\"fareType\":null,\"segmentCode\":\"LAX_MEX_AM_2017-09-29_1625\"},{\"_meta\":{\"class\":\"PaidSegmentChoice\"},\"seat\":{\"subjectToSeatSelectionFee\":false,\"_meta\":{\"class\":\"SeatChoice\"},\"code\":\"12A\",\"seatCharacteristics\":[]},\"fareType\":null,\"segmentCode\":\"MEX_BOG_AM_2017-09-30_0830\"}]},\"bookingClasses\":{\"_meta\":{\"class\":\"BookingClassMap\"},\"_collection\":[{\"segmentCode\":\"LAX_MEX_AM_2017-09-29_1625\",\"bookingClass\":\"N\",\"bookingCabin\":\"Y\",\"passengerId\":\"16B897EB0301\"},{\"segmentCode\":\"MEX_BOG_AM_2017-09-30_0830\",\"bookingClass\":\"R\",\"bookingCabin\":\"Y\",\"passengerId\":\"16B897EB0501\"}]},\"ineligibleReasons\":[\"DOCUMENT_VERIFICATION\"],\"nameRefNumber\":\"01.01\",\"passengerType\":\"F\",\"checkedBags\":0,\"checkinNumber\":null,\"selectee\":false,\"overBookingInfo\":null,\"displayName\":\"ALVARO GUILLERMO CORDOBA CELY\",\"firstName\":\"ALVARO GUILLERMO\",\"middleName\":null,\"lastName\":\"CORDOBA CELY\",\"id\":\"16B897EB0301\",\"phones\":null,\"email\":null,\"ssrRemarks\":{\"_meta\":{\"class\":\"SSRRemarks\"},\"ssrCodes\":[\"M\",\"OB\",\"WB\",\"ET\",\"PRCH\",\"DOCS\",\"DOCA\",\"ETI\",\"AE\",\"WCHS\"]},\"freeBaggageAllowance\":{\"personal\":1,\"carryOn\":1,\"checked\":1,\"checkedMaxWeight\":23,\"checkedWeightUnit\":\"KG\"},\"freeBaggageAllowancePerLegsArray\":[{\"_meta\":{\"class\":\"FreeBaggageAllowancePerLeg\"},\"legCode\":\"LAX_AM_0643_2017-09-29\",\"freeBaggageAllowance\":{\"personal\":1,\"carryOn\":1,\"checked\":1,\"checkedWeightCollection\":[{\"checkedFreeWeight\":50.7,\"checkedMaxWeight\":99.0,\"checkedWeightUnit\":\"lb\"},{\"checkedFreeWeight\":23.0,\"checkedMaxWeight\":45.0,\"checkedWeightUnit\":\"kg\"}]}}],\"gender\":\"M\",\"dateOfBirth\":\"1946-12-17\",\"frequentFlyerProgram\":null,\"frequentFlyerNumber\":null,\"corporateFrequentFlyerNumber\":null,\"redressNumber\":null,\"ktn\":null,\"travelDocument\":{\"_meta\":{\"class\":\"TravelerDocument\"},\"documentNumber\":\"A0444548\",\"issuingCountry\":\"CO\",\"expirationDate\":\"2023-03-13\",\"passportScanned\":false,\"nationality\":\"CO\"},\"countryOfResidence\":\"US\",\"destinationAddress\":null,\"declinedEmergencyContact\":false,\"emergencyContact\":null,\"benefit\":null,\"isOverBookingEligible\":false,\"isSkyPriority\":false}]},\"itineraryDependentTravelerFields\":null,\"upsellOffers\":null,\"_warnings\":null}]},\"legs\":{\"_meta\":{\"class\":\"BookedLegCollection\"},\"_collection\":[{\"_meta\":{\"class\":\"BookedLeg\"},\"estimatedDepartureTime\":null,\"scheduledDepartureTime\":null,\"boardingTime\":null,\"boardingTerminal\":\"\",\"boardingGate\":\"-\",\"cabin\":null,\"flightStatus\":\"FLOWN\",\"checkinStatus\":\"CLOSED_INTERNATIONAL_WINDOW\",\"manageStatus\":null,\"segments\":{\"_meta\":{\"class\":\"BookedSegmentCollection\"},\"_collection\":[{\"_meta\":{\"class\":\"BookedSegment\"},\"checkinIneligibleReasons\":[],\"segment\":{\"_meta\":{\"class\":\"Segment\"},\"segmentCode\":\"BOG_MEX_AM_2017-09-16_0945\",\"departureAirport\":\"BOG\",\"departureDateTime\":\"2017-09-16T09:45:00\",\"arrivalAirport\":\"MEX\",\"arrivalDateTime\":\"2017-09-16T14:45:00\",\"flightDurationInMinutes\":0,\"layoverToNextSegmentsInMinutes\":0,\"aircraftType\":\"7S8\",\"operatingCarrier\":\"AM\",\"operatingFlightCode\":\"0718\",\"marketingCarrier\":\"AM\",\"marketingFlightCode\":\"0718\",\"amenitiesPremiumCabin\":{\"complimentaryFeatures\":[\"SNP\",\"SD\"],\"paidFeatures\":[]},\"amenitiesMainCabin\":{\"complimentaryFeatures\":[\"SN\",\"SD\"],\"paidFeatures\":[]},\"isPremierAvailable\":false,\"boardingTime\":null,\"boardingTerminal\":\"\",\"boardingGate\":\"-\",\"capacity\":null,\"cabin\":null,\"flightStatus\":\"FLOWN\"}},{\"_meta\":{\"class\":\"BookedSegment\"},\"checkinIneligibleReasons\":[],\"segment\":{\"_meta\":{\"class\":\"Segment\"},\"segmentCode\":\"MEX_LAX_AM_2017-09-16_2125\",\"departureAirport\":\"MEX\",\"departureDateTime\":\"2017-09-16T21:25:00\",\"arrivalAirport\":\"LAX\",\"arrivalDateTime\":\"2017-09-16T23:30:00\",\"flightDurationInMinutes\":0,\"layoverToNextSegmentsInMinutes\":0,\"aircraftType\":\"7S8\",\"operatingCarrier\":\"AM\",\"operatingFlightCode\":\"0630\",\"marketingCarrier\":\"AM\",\"marketingFlightCode\":\"0630\",\"amenitiesPremiumCabin\":{\"complimentaryFeatures\":[\"HCP\",\"ADP\"],\"paidFeatures\":[]},\"amenitiesMainCabin\":{\"complimentaryFeatures\":[\"OC\",\"SD\"],\"paidFeatures\":[]},\"isPremierAvailable\":false,\"boardingTime\":null,\"boardingTerminal\":\"\",\"boardingGate\":\"-\",\"capacity\":null,\"cabin\":null,\"flightStatus\":\"FLOWN\"}}],\"legCode\":\"BOG_AM_0718_2017-09-16\",\"legType\":\"CONNECTING\",\"totalFlightTimeInMinutes\":0}},{\"_meta\":{\"class\":\"BookedLeg\"},\"estimatedDepartureTime\":\"2017-09-29T16:25:00\",\"scheduledDepartureTime\":\"2017-09-29T16:25:00\",\"boardingTime\":\"15:40\",\"boardingTerminal\":\"2\",\"boardingGate\":\"23\",\"cabin\":null,\"flightStatus\":\"ON_TIME\",\"checkinStatus\":\"OPEN\",\"manageStatus\":null,\"segments\":{\"_meta\":{\"class\":\"BookedSegmentCollection\"},\"_collection\":[{\"_meta\":{\"class\":\"BookedSegment\"},\"checkinIneligibleReasons\":[],\"segment\":{\"_meta\":{\"class\":\"Segment\"},\"segmentCode\":\"LAX_MEX_AM_2017-09-29_1625\",\"departureAirport\":\"LAX\",\"departureDateTime\":\"2017-09-29T16:25:00\",\"arrivalAirport\":\"MEX\",\"arrivalDateTime\":\"2017-09-29T22:20:00\",\"flightDurationInMinutes\":235,\"layoverToNextSegmentsInMinutes\":610,\"aircraftType\":\"7S8\",\"operatingCarrier\":\"AM\",\"operatingFlightCode\":\"0643\",\"marketingCarrier\":\"AM\",\"marketingFlightCode\":\"0643\",\"amenitiesPremiumCabin\":{\"complimentaryFeatures\":[\"HCP\",\"ADP\"],\"paidFeatures\":[]},\"amenitiesMainCabin\":{\"complimentaryFeatures\":[\"OC\",\"SD\"],\"paidFeatures\":[]},\"isPremierAvailable\":false,\"boardingTime\":\"15:40\",\"boardingTerminal\":\"2\",\"boardingGate\":\"23\",\"capacity\":[{\"cabin\":\"main\",\"booked\":123.0,\"authorized\":144.0},{\"cabin\":\"premier\",\"booked\":6.0,\"authorized\":16.0}],\"cabin\":null,\"flightStatus\":\"ON_TIME\"}},{\"_meta\":{\"class\":\"BookedSegment\"},\"checkinIneligibleReasons\":[],\"segment\":{\"_meta\":{\"class\":\"Segment\"},\"segmentCode\":\"MEX_BOG_AM_2017-09-30_0830\",\"departureAirport\":\"MEX\",\"departureDateTime\":\"2017-09-30T08:30:00\",\"arrivalAirport\":\"BOG\",\"arrivalDateTime\":\"2017-09-30T13:12:00\",\"flightDurationInMinutes\":282,\"layoverToNextSegmentsInMinutes\":0,\"aircraftType\":\"7S8\",\"operatingCarrier\":\"AM\",\"operatingFlightCode\":\"0761\",\"marketingCarrier\":\"AM\",\"marketingFlightCode\":\"0761\",\"amenitiesPremiumCabin\":{\"complimentaryFeatures\":[\"HBP\",\"SD\"],\"paidFeatures\":[]},\"amenitiesMainCabin\":{\"complimentaryFeatures\":[\"HB\",\"SD\"],\"paidFeatures\":[]},\"isPremierAvailable\":false,\"boardingTime\":\"07:45\",\"boardingTerminal\":\"2\",\"boardingGate\":\"-\",\"capacity\":[{\"cabin\":\"main\",\"booked\":137.0,\"authorized\":144.0},{\"cabin\":\"premier\",\"booked\":12.0,\"authorized\":16.0}],\"cabin\":null,\"flightStatus\":\"ON_TIME\"}}],\"legCode\":\"LAX_AM_0643_2017-09-29\",\"legType\":\"CONNECTING\",\"totalFlightTimeInMinutes\":517}}]},\"boardingPassId\":\"QXBPDR\",\"overBooking\":null,\"bookedCar\":false,\"volunteerOffer\":false}]}");
            //service.createWebBoardingPasses(response, "es", "MX", pnrCollection, "LAX_AM_0643_2017-09-29", "QXBPDR", "api/v1");
        } catch (Exception ex) {
            fail("Should not throw exception");
        }
    }

    @Test
    public void testDateTimeInUTC() {

        //String departureDateTime = "2017-10-27T15:25:00";
        String departureDateTime = "2017-10-27T15:25-05";
        //SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmX");

        Calendar calendarZulu = Calendar.getInstance(TimeZone.getTimeZone("GMT"));

        try {
            calendarZulu.setTime(isoFormat.parse(departureDateTime));
        } catch (Exception e) {
            e.printStackTrace();
        }

        int rawOffSet = Integer.parseInt(String.valueOf(3600));
        int dstOffSet = Integer.parseInt(String.valueOf(-21600));

        //Use 6 hours prior to departure as a relevant date/time for apple wallet.
        calendarZulu.add(Calendar.SECOND, -(6 * 60 * 60));

        SimpleDateFormat resultFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmX");
        //resultFormat.setTimeZone(java.util.TimeZone.getTimeZone("GMT"));

        String result = resultFormat.format(calendarZulu.getTime());

        System.out.println(result);
    }
}
