package com.am.checkin.web.service;

import com.aeromexico.sabre.api.acs.AMPassengerDataService;
import com.aeromexico.sabre.api.session.common.ServiceCall;
import com.am.checkin.web.util.CommonTestUtil;
import com.sabre.services.checkin.getpassengerdata.v4.GetPassengerDataRSACS;

import java.io.InputStream;
import java.util.List;

public class AMPassengerDataServiceMock extends AMPassengerDataService {

    @Override
    public GetPassengerDataRSACS passengerDataByPNRLocator(String recordLocator, boolean checkInRequirement, boolean includeTimaticInfo, List<ServiceCall> serviceCallList) throws Exception {

        InputStream passengerDataIS = AMPassengerDataServiceMock.class.getResourceAsStream(recordLocator + ".xml");
        if (passengerDataIS == null) {
            System.out.println("**** Input file was not found: " + recordLocator + ".xml");
            return null;
        }
        GetPassengerDataRSACS passengerDataRS = CommonTestUtil.getACSPassengerDataRSACS(passengerDataIS);
        return passengerDataRS;
    }

    @Override
    public GetPassengerDataRSACS passengerDataByItineraryAndPassengerInfo(String recordLocator, String airline, String flightNumber, String departureDate, String origin, String lastName, String firstName, boolean checkInRequirement, List<ServiceCall> serviceCallList) throws Exception {

        InputStream passengerDataIS = AMPassengerDataServiceMock.class.getResourceAsStream(recordLocator + ".xml");
        if (passengerDataIS == null) {
            System.out.println("**** Input file was not found: " + recordLocator + ".xml");
            return null;
        }
        GetPassengerDataRSACS passengerDataRS = CommonTestUtil.getACSPassengerDataRSACS(passengerDataIS);
        return passengerDataRS;
    }

    @Override
    public GetPassengerDataRSACS passengerDataBySegment(String airline, String flightNumber, String departureDate, String origin, List<String> listLastName, boolean checkInRequirement, List<ServiceCall> serviceCallList) throws Exception {

    	InputStream passengerDataIS = AMPassengerDataServiceMock.class.getResourceAsStream( airline + "_" + flightNumber + "_" + origin + "_" + departureDate + ".xml" );

    	if (passengerDataIS == null) {
            System.out.println("**** Input file was not found: " + airline + "_" + flightNumber + "_" + origin + "_" + departureDate + ".xml");
            return null;
        }

    	GetPassengerDataRSACS passengerDataRS = CommonTestUtil.getACSPassengerDataRSACS(passengerDataIS);
        return passengerDataRS;
    }

}
