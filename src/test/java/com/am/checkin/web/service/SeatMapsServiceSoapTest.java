package com.am.checkin.web.service;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.aeromexico.commons.exception.model.BrandedFaresException;
import com.aeromexico.commons.model.AbstractSeatMap;
import com.aeromexico.commons.model.SeatMap;
import com.aeromexico.commons.model.SeatmapCollection;
import com.aeromexico.commons.model.SeatmapRow;
import com.aeromexico.commons.model.SeatmapSeat;
import com.aeromexico.commons.model.SeatmapSection;
import com.aeromexico.commons.seatmaps.util.SeatMapUtil;
import com.aeromexico.commons.web.types.SeatSectionCodeType;
import com.aeromexico.commons.web.types.SeatmapStatusType;
import com.am.checkin.web.util.FileUtils;
import com.google.gson.JsonParseException;

/**
 * test methods to probe rules to block certain cabins in seatmaps, according to
 * the new rules of branded fares
 *
 *
 */
public class SeatMapsServiceSoapTest {

    @Test
    public void testLockSeatMapContigoAmPlus()
            throws BrandedFaresException, ClassNotFoundException, JsonParseException, IOException {

        List<SeatSectionCodeType> sectionCodeTypeList = new ArrayList<>();
        sectionCodeTypeList.add(SeatSectionCodeType.AM_PLUS);

        String responseSeatMaps = FileUtils.readFileAsString("/web/service/gdl_laxTContigo.json");

        SeatmapCollection seatmapCollection = new SeatmapCollection(responseSeatMaps);

        String segmentCodes = "GDL_LAX_AM_2018-02-08_0925";

        SeatMapUtil.lockSeatMap(segmentCodes, seatmapCollection, sectionCodeTypeList);

        List<AbstractSeatMap> abstractSeatMapList = seatmapCollection.getCollection();
        abstractSeatMapList.forEach(abstractSeatMapItem -> {
            if (abstractSeatMapItem instanceof SeatMap) {
                SeatMap seatMap = null;
                seatMap = (SeatMap) abstractSeatMapItem;
                for (SeatmapSection seatmapSection : seatMap.getSections().getCollection()) {
                    if (SeatSectionCodeType.AM_PLUS == seatmapSection.getCode()) {
                        for (SeatmapRow seatmapRow : seatmapSection.getRows().getCollection()) {
                            for (SeatmapSeat seatmapSeat : seatmapRow.getSeats().getCollection()) {
                                assertTrue(SeatmapStatusType.UNAVAILABLE.equals(seatmapSeat.getStatus()));
                            }
                        }
                    }
                }
            }
        });
    }

    @Test
    public void testLockSeatMapParamsNull()
            throws BrandedFaresException, ClassNotFoundException, JsonParseException, IOException {

        List<SeatSectionCodeType> sectionCodeTypeList = new ArrayList<>();
        sectionCodeTypeList.add(SeatSectionCodeType.AM_PLUS);

        SeatMapUtil.lockSeatMap(null, null, null);

    }

    @Test
    public void testLockSeatMapFirstCl()
            throws BrandedFaresException, ClassNotFoundException, JsonParseException, IOException {

        List<SeatSectionCodeType> sectionCodeTypeList = new ArrayList<>();
        sectionCodeTypeList.add(SeatSectionCodeType.FIRST_CLASS);

        String responseSeatMaps = FileUtils.readFileAsString("/web/service/MEX_CUN_laxTlockCabin.json");

        SeatmapCollection seatmapCollection = new SeatmapCollection(responseSeatMaps);

        String segmentCode = "MEX_CUN_AM_2018-02-19_0105";

        SeatMapUtil.lockSeatMap(segmentCode, seatmapCollection, sectionCodeTypeList);

        List<AbstractSeatMap> abstractSeatMapList = seatmapCollection.getCollection();
        abstractSeatMapList.forEach(abstractSeatMapItem -> {
            if (abstractSeatMapItem instanceof SeatMap) {
                SeatMap seatMap = null;
                seatMap = (SeatMap) abstractSeatMapItem;
                for (SeatmapSection seatmapSection : seatMap.getSections().getCollection()) {
                    if (SeatSectionCodeType.FIRST_CLASS == seatmapSection.getCode()) {
                        for (SeatmapRow seatmapRow : seatmapSection.getRows().getCollection()) {
                            for (SeatmapSeat seatmapSeat : seatmapRow.getSeats().getCollection()) {
                                assertTrue(SeatmapStatusType.UNAVAILABLE == seatmapSeat.getStatus());
                            }
                        }
                    }
                }
            }
        });
    }

}
