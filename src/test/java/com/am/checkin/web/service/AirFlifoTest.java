package com.am.checkin.web.service;

import com.aeromexico.commons.model.FlightStatusCollection;
import com.aeromexico.commons.model.SegmentStatus;
import com.aeromexico.commons.model.SegmentStatusCollection;
import com.aeromexico.commons.model.rq.AirFlifoRQ;
import com.aeromexico.dao.test.EnvironmentTest;
import com.am.checkin.web.util.AirFlifoUtil;
import org.hibernate.validator.internal.util.Contracts;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public class AirFlifoTest extends EnvironmentTest {

    private AirFlifoService airFlifoService;

    @Before
    public void before() {
        airFlifoService = new AirFlifoService();

        airFlifoService.setSetUpConfigFactory(new CheckInSetUpConfigFactoryWrapper());
        //airFlifoService.setSystemPropertiesFactory(systemPropertiesFactory);
        airFlifoService.init();

        airFlifoService.setFlifoService(new FlifoServiceMock());
        airFlifoService.setScheduleService(new ScheduleServiceMock());
    }

    @Test
    @Ignore
    public void testFlightStatus() throws Exception {

        AirFlifoRQ airFlifoRQ = new AirFlifoRQ();
        airFlifoRQ.setStore("MX");
        airFlifoRQ.setPos("WEB");
        airFlifoRQ.setDate("2016-08-01");
        airFlifoRQ.setFlight("930");

        FlightStatusCollection flightStatusCollection = airFlifoService.callAirFlifo(airFlifoRQ);

        assertNotNull(flightStatusCollection);

        SegmentStatusCollection segmentStatusCollection = flightStatusCollection.getCollection().get(0);
        SegmentStatus segmentStatus = segmentStatusCollection.getCollection().get(0);

        assertNotNull(segmentStatus.getStatus());
        Contracts.assertNotEmpty(segmentStatus.getStatus(), "Status empty");
    }

    @Test
    @Ignore
    public void testFlightStatusINT() throws Exception {

        AirFlifoRQ airFlifoRQ = new AirFlifoRQ();
        airFlifoRQ.setStore("MX");
        airFlifoRQ.setPos("WEB");
        airFlifoRQ.setDate("2016-08-01");
        airFlifoRQ.setFlight("1");

        FlightStatusCollection flightStatusCollection = airFlifoService.callAirFlifo(airFlifoRQ);

        assertNotNull(flightStatusCollection);

        SegmentStatusCollection segmentStatusCollection = flightStatusCollection.getCollection().get(0);
        SegmentStatus segmentStatus = segmentStatusCollection.getCollection().get(0);

        assertNotNull(segmentStatus.getStatus());
        Contracts.assertNotEmpty(segmentStatus.getStatus(), "Status empty");
    }

    @Test
    @Ignore
    public void testFlightStatusDOM() throws Exception {

        AirFlifoRQ airFlifoRQ = new AirFlifoRQ();
        airFlifoRQ.setStore("MX");
        airFlifoRQ.setPos("WEB");
        airFlifoRQ.setDate("2016-08-01");
        airFlifoRQ.setFlight("2416");

        FlightStatusCollection flightStatusCollection = airFlifoService.callAirFlifo(airFlifoRQ);

        assertNotNull(flightStatusCollection);

        SegmentStatusCollection segmentStatusCollection = flightStatusCollection.getCollection().get(0);
        SegmentStatus segmentStatus = segmentStatusCollection.getCollection().get(0);

        assertNotNull(segmentStatus.getStatus());
        Contracts.assertNotEmpty(segmentStatus.getStatus(), "Status empty");
        Contracts.assertTrue(segmentStatus.getStatus().equals("CANCELLED"), "Status is correct");
    }

    @Test
    @Ignore
    public void testFlightStatusWithConection() {

        AirFlifoRQ airFlifoRQ = new AirFlifoRQ();
        airFlifoRQ.setStore("MX");
        airFlifoRQ.setPos("WEB");
        airFlifoRQ.setDate("2016-08-01");
        airFlifoRQ.setOrigin("MEX");
        airFlifoRQ.setDestination("ICN");

        try {
            FlightStatusCollection flightStatusCollection = airFlifoService.callAirFlifo(airFlifoRQ);

            assertNotNull(flightStatusCollection);

            SegmentStatusCollection segmentStatusCollection = flightStatusCollection.getCollection().get(0);
            SegmentStatus segmentStatus = segmentStatusCollection.getCollection().get(0);

            assertNotNull(segmentStatus.getStatus());
            Contracts.assertNotEmpty(segmentStatus.getStatus(), "Status empty");
        } catch (Exception ex) {
            fail("Should not throw exception");
        }
    }

    @Test
    @Ignore
    public void testFlightStatusOtherAirLine() throws Exception {

        AirFlifoRQ airFlifoRQ = new AirFlifoRQ();
        airFlifoRQ.setStore("MX");
        airFlifoRQ.setPos("WEB");
        airFlifoRQ.setDate("2016-08-02");
        airFlifoRQ.setOrigin("MEX");
        airFlifoRQ.setDestination("CDG");

        FlightStatusCollection flightStatusCollection = airFlifoService.callAirFlifo(airFlifoRQ);

        assertNotNull(flightStatusCollection);

        SegmentStatusCollection segmentStatusCollection = flightStatusCollection.getCollection().get(0);
        SegmentStatus segmentStatus = segmentStatusCollection.getCollection().get(0);

        assertNotNull(segmentStatus.getStatus());
        Contracts.assertNotEmpty(segmentStatus.getStatus(), "Status empty");
    }

    @Test
    @Ignore
    public void testTimeZonedStatus() {
        try {
            String originTimeZone = "America/Tijuana";
            String destTimeZone = "America/Mexico_City";

            String departure = "2018-09-25 11:28:00"; //Here is 13:28
            String arrival = "2018-09-25 18:00:00";
            String now = "2018-09-25 13:26:00";

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            Date dateDeparture = formatter.parse(departure);
            Date dateArrival = formatter.parse(arrival);
            Date dateNow = formatter.parse(now);

            String result = AirFlifoUtil.calculateStatus(dateNow, dateDeparture,
                    dateArrival, originTimeZone, destTimeZone);
            Assert.assertEquals(result, "ON_TIME");

            now = "2018-09-25 13:29:00";
            dateNow = formatter.parse(now);
            result = AirFlifoUtil.calculateStatus(dateNow, dateDeparture,
                    dateArrival, originTimeZone, destTimeZone);
            Assert.assertEquals(result, "FLOWN");

            now = "2018-09-25 19:29:00";
            dateNow = formatter.parse(now);
            result = AirFlifoUtil.calculateStatus(dateNow, dateDeparture,
                    dateArrival, originTimeZone, destTimeZone);
            Assert.assertEquals(result, "ARRIVED");

        } catch (Exception ex) {
            fail("Should not throw exception");
        }
    }

}
