
package com.am.checkin.web.service;

import static org.junit.Assert.fail;

import org.junit.Test;

public class VolunteeredListServiceTest {

	@Test
	public void testGetVolunteeredCount() throws Exception {
		PassengersListService volunteeredListService = new PassengersListService();
		volunteeredListService.setPassengerListService( new AMPassengerListServiceMock() );

		int volunteeredCount = volunteeredListService.getVolunteeredCount("AM", "465", "MEX", "27MAY");

		System.out.println( "There are ( " + volunteeredCount + " ) on the volunteer list.");

		if ( volunteeredCount != 1 ) {
			fail("Expecting only one passenger on the volunteer list, but there are: " + volunteeredCount);
		}
	}

}
