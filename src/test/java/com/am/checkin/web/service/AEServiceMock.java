package com.am.checkin.web.service;

import com.aeromexico.commons.model.BookedLeg;
import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.PNRCollection;
import com.aeromexico.commons.model.TravelerAncillary;
import com.aeromexico.sabre.api.session.common.ServiceCall;
import java.util.List;

public class AEServiceMock extends AEService {

    @Override
    public void addNewAncillaryToReservation(
            BookedTraveler bookedTraveler,
            TravelerAncillary travelerAncillary,
            BookedLeg bookedLeg,
            String recordLocator,
            String cartId,
            String store,
            List<ServiceCall> serviceCallList,
            String pos
    ) throws Exception {
        System.out.println("Ancillary created!!");
    }

    @Override
    public boolean removeUnpaidAncillariesForUnassignedSeats(
            PNRCollection pnrCollection, 
            List<ServiceCall> serviceCallList
    ) throws Exception {
        return true;
    }

}
