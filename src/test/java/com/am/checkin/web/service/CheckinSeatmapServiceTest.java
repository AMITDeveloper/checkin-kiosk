package com.am.checkin.web.service;

import com.am.checkin.web.dao.PNRLookupDaoMock;
import com.aeromexico.commons.model.*;
import com.aeromexico.commons.model.rq.SeatmapCheckInRQ;
import com.aeromexico.commons.seatmaps.init.InfantSeatValidator;
import com.aeromexico.commons.web.types.SeatCharacteristicsType;
import com.aeromexico.commons.web.types.SeatmapStatusType;
import com.am.checkin.web.util.CommonTestUtil;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import java.io.InputStream;
import java.util.Date;

import static org.junit.Assert.*;

@Ignore
public class CheckinSeatmapServiceTest {

	@InjectMocks
	private SeatMapsService seatmapService;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	private SeatmapCollection setupSeatmapFromFile(String cartId) {
		SeatmapCheckInRQ seatmapRQ = new SeatmapCheckInRQ();
		seatmapRQ.setStore("MEX");
		seatmapRQ.setPos("KIOSK");
		seatmapRQ.setCartId(cartId);
		PNRLookupDaoMock pnrLookupDaoMock = new PNRLookupDaoMock("seatmap/");
		seatmapService.setPnrLookupDao(pnrLookupDaoMock);
		//seatmapService.setSeatMapCommonService(new AMSeatMapServiceMock());
		InfantSeatValidator infantSeatValidator = new InfantSeatValidator();
		infantSeatValidator.readInitFile();
		//seatmapService.setInfantSeatValidator(infantSeatValidator);

		SeatmapCollection seatmapCollection = null;
		try {
			seatmapCollection = seatmapService.getSeatMaps(seatmapRQ);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return seatmapCollection;
	}

    private void verifyExitRow(SeatmapRow row, boolean isExitRow) {
        for(SeatmapSeat seat : row.getSeats().getCollection()) {
			if(seat.getChoice() instanceof SeatChoice) {
				if (isExitRow) {
					assertTrue("Row: " + row.getCode(), ((SeatChoice) (seat.getChoice())).getSeatCharacteristics().contains(SeatCharacteristicsType.EXIT_ROW));
				} else {
					assertFalse("Row: " + row.getCode(), ((SeatChoice) (seat.getChoice())).getSeatCharacteristics().contains(SeatCharacteristicsType.EXIT_ROW));
				}

			} else if (seat.getChoice() instanceof SeatChoiceUpsell) {
				if (isExitRow) {
					assertTrue("Row: " + row.getCode(), ((SeatChoiceUpsell) (seat.getChoice())).getSeatCharacteristics().contains(SeatCharacteristicsType.EXIT_ROW));
				} else {
					assertFalse("Row: " + row.getCode(), ((SeatChoiceUpsell) (seat.getChoice())).getSeatCharacteristics().contains(SeatCharacteristicsType.EXIT_ROW));
				}

			}
        }
    }

	@Test
	public void test_737_J_CheckIn_Seatmap() throws Exception {
		SeatmapCollection seatmapCollection = setupSeatmapFromFile("737-MEX-MTY-AM-910-J-C");
		System.out.println(new Date().toString());
		assertNotNull(seatmapCollection);

		String seatmapCollectionString = seatmapCollection.toString();
		System.out.println("Result  :" + seatmapCollectionString);
		assertNotNull(seatmapCollectionString);

		assertEquals("Wrong segment amount", 1, seatmapCollection.getCollection().size());

		//6 cabins: first, amplus, prefered, coach, prefered, coach
		AbstractSeatMap abstractSeaMap = seatmapCollection.getCollection().get(0);
		SeatMap seatMap = new SeatMap();
		if(abstractSeaMap instanceof SeatMap){
			seatMap = (SeatMap) abstractSeaMap;
		}

		assertEquals("Cabin count", 6, seatMap.getSections().getCollection().size());

		// 3 rows on First
		SeatmapSection firstSection = seatMap.getSections().getCollection().get(0);
		assertEquals("First section row count", 3, firstSection.getRows().getCollection().size());

		for(SeatmapRow row : firstSection.getRows().getCollection()) {
			//4 seats per row on First
			assertEquals("Wrong seat count row: " + row.getCode(), 4, row.getSeats().getCollection().size());
			for(SeatmapSeat seat : row.getSeats().getCollection()) {
				if(seat.getChoice() instanceof SeatChoice) {
					SeatChoice choice = (SeatChoice)seat.getChoice();
					//A & F should be aisle
					if(seat.getCode().contains("A") || seat.getCode().contains("F")) {
						assertTrue("Should be aisle" + seat.getCode(), choice.getSeatCharacteristics().contains(SeatCharacteristicsType.W));
					}
					//B & E should be window
					if(seat.getCode().contains("B") || seat.getCode().contains("E")) {
						assertTrue("Should be window" + seat.getCode(), choice.getSeatCharacteristics().contains(SeatCharacteristicsType.A));
					}
				}
			}
		}

		SeatmapSection coachSection = seatMap.getSections().getCollection().get(1);

		//All coach should be unavailable
		for(SeatmapRow row : coachSection.getRows().getCollection()) {
			for(SeatmapSeat seat : row.getSeats().getCollection()) {
				assertEquals("Coach cabin should be unavailable", SeatmapStatusType.UNAVAILABLE, seat.getStatus());
			}
		}

		InputStream expJsonIn = CheckinSeatmapServiceTest.class.getResourceAsStream("seatmap\\checkin_737-J-C.json");
		String expectedJsonString = CommonTestUtil.fileToString(expJsonIn);
		System.out.println("Expected:" + expectedJsonString);

		assertNotNull(expectedJsonString);
		assertTrue(expectedJsonString.equalsIgnoreCase(seatmapCollectionString));
	}

	@Test
	public void test_737_Y_CheckIn_Seatmap() throws Exception {

		SeatmapCollection seatmapCollection = setupSeatmapFromFile("737-MEX-MTY-AM-910-Y-M");
		assertNotNull(seatmapCollection);

		String seatmapCollectionString = seatmapCollection.toString();
		System.out.println("Result: " + seatmapCollectionString);

		assertNotNull(seatmapCollectionString);

		//6 sections: : First, AMPlus, Preferred, coach, preferred, coach
		AbstractSeatMap abstractSeaMap = seatmapCollection.getCollection().get(0);
		SeatMap seatMap = new SeatMap();
		if(abstractSeaMap instanceof SeatMap){
			seatMap = (SeatMap) abstractSeaMap;
		}
		assertEquals(4, seatMap.getSections().getCollection().size());

		SeatmapSection firstSection = seatMap.getSections().getCollection().get(0);

		for(SeatmapRow row : firstSection.getRows().getCollection()) {
			for(SeatmapSeat seat : row.getSeats().getCollection()) {
				//All seats on first class should be unavailable
				assertEquals(SeatmapStatusType.UNAVAILABLE, seat.getStatus());
			}
		}

		int totalRows = 0;
		for(int i = 1; i <= 3; i++) {
			totalRows += seatMap.getSections().getCollection().get(i).getRows().getCollection().size();
			SeatmapSection section = seatMap.getSections().getCollection().get(i);
			//6 seats per row on coach except row 12
			for(SeatmapRow row : section.getRows().getCollection()) {
				if(!row.getCode().equals("12")) {
					assertEquals("Wrong seat count row: " + row.getCode(), 6, row.getSeats().getCollection().size());
				} else {
					//Row 12  has only 4 seats
					assertEquals("should have 4 seats: " + row.getCode(), 4, row.getSeats().getCollection().size());
				}
				//Row 13 does not exist
				if(row.getCode().equals("13")) {
					fail("Row 13 should not exist");
				}
				//Row 23 Seat E is a wheelchair space, should be unavailable
				/*if(row.getCode().equals("23")) {
					assertEquals(SeatmapStatusType.UNAVAILABLE, row.getSeats().getCollection().get(4).getStatus());
				}*/
                // Exit rows
                if(row.getCode().equals("12") || row.getCode().equals("14")) {
                    verifyExitRow(row, true);
                } else {
                    verifyExitRow(row, false);
                }
            }
		}

		//19 Rows on coach (from 6 to 25 without 13)
		assertEquals("Wrong total rows", 19, totalRows);

		InputStream expJsonIn = CheckinSeatmapServiceTest.class.getResourceAsStream("seatmap\\checkin_737-Y-M.json");
		String expectedJsonString = CommonTestUtil.fileToString(expJsonIn);
		System.out.println("Expected:" + expectedJsonString);

		assertNotNull(expectedJsonString);
		assertTrue(expectedJsonString.equalsIgnoreCase(seatmapCollectionString));
	}

	@Test
	public void test_738_J_CheckIn_Seatmap() throws Exception {

		SeatmapCollection seatmapCollection = setupSeatmapFromFile("738-GDL-ORD-AM-660-J-C");

		System.out.println(new Date().toString());
		assertNotNull(seatmapCollection);

		String seatmapCollectionString = seatmapCollection.toString();
		System.out.println("Result: " + seatmapCollectionString);

		assertNotNull(seatmapCollectionString);

		//One segment
		assertEquals("Wrong segment count", 1, seatmapCollection.getCollection().size());

		//6 cabins
		AbstractSeatMap abstractSeaMap = seatmapCollection.getCollection().get(0);
		SeatMap seatMap = new SeatMap();
		if(abstractSeaMap instanceof SeatMap){
			seatMap = (SeatMap) abstractSeaMap;
		}
		assertEquals("Wrong cabin count", 6, seatMap.getSections().getCollection().size());

		//4 rows on First
		SeatmapSection firstSection = seatMap.getSections().getCollection().get(0);
		assertEquals(4, firstSection.getRows().getCollection().size());

		for(SeatmapRow row : firstSection.getRows().getCollection()) {
			//4 seats per row on First
			assertEquals(4, row.getSeats().getCollection().size());
			for(SeatmapSeat seat : row.getSeats().getCollection()) {
				if(seat.getChoice() instanceof SeatChoice) {
					SeatChoice choice = (SeatChoice)seat.getChoice();
					//A & F should be window
					if(seat.getCode().contains("A") || seat.getCode().contains("F")) {
						assertTrue("Should be window seat: " + seat, choice.getSeatCharacteristics().contains(SeatCharacteristicsType.W));
					}
					//B & E should be aisle
					if(seat.getCode().contains("B") || seat.getCode().contains("E")) {
						assertTrue(choice.getSeatCharacteristics().contains(SeatCharacteristicsType.A));
					}
				}
			}
		}

		SeatmapSection coachSection = seatMap.getSections().getCollection().get(1);

		for(SeatmapRow row : coachSection.getRows().getCollection()) {
			for(SeatmapSeat seat : row.getSeats().getCollection()) {
				//All seats on economy class should be unavailable
				assertEquals(SeatmapStatusType.UNAVAILABLE, seat.getStatus());
			}
		}

		InputStream expJsonIn = CheckinSeatmapServiceTest.class.getResourceAsStream("seatmap\\checkin_738-J-C.json");
		String expectedJsonString = CommonTestUtil.fileToString(expJsonIn);
		System.out.println("Expected:" + expectedJsonString);

		assertNotNull(expectedJsonString);
		assertTrue(expectedJsonString.equalsIgnoreCase(seatmapCollectionString));
	}

	@Test
	public void test_738_Y_CheckIn_Seatmap() throws Exception {

		SeatmapCollection seatmapCollection = setupSeatmapFromFile("738-GDL-ORD-AM-660-Y-M");

		System.out.println(new Date().toString());
		assertNotNull(seatmapCollection);

		String seatmapCollectionString = seatmapCollection.toString();
		System.out.println("Result  :" + seatmapCollectionString);

		assertNotNull("No result", seatmapCollectionString);

		//One segment
		assertEquals("Wrong segment total", 1, seatmapCollection.getCollection().size());

		//6 sections: : First, AMPlus, Preferred, coach, preferred, coach
		AbstractSeatMap abstractSeaMap = seatmapCollection.getCollection().get(0);
		SeatMap seatMap = new SeatMap();
		if(abstractSeaMap instanceof SeatMap){
			seatMap = (SeatMap) abstractSeaMap;
		}
		assertEquals("Wrong section count", 2, seatMap.getSections().getCollection().size());

		SeatmapSection firstSection = seatMap.getSections().getCollection().get(0);

		for(SeatmapRow row : firstSection.getRows().getCollection()) {
			for(SeatmapSeat seat : row.getSeats().getCollection()) {
				//All seats on first class should be unavailable
				assertEquals("First class should be unavailable", SeatmapStatusType.UNAVAILABLE, seat.getStatus());
			}
		}

		int totalRows = 0;
		for(int i = 1; i <= 5; i++) {
			totalRows += seatMap.getSections().getCollection().get(i).getRows().getCollection().size();
			SeatmapSection section = seatMap.getSections().getCollection().get(i);
			//6 seats per row on coach except row 12
			for(SeatmapRow row : section.getRows().getCollection()) {
				if(!row.getCode().equals("29")) {
					assertEquals("Wrong seat count row: " + row.getCode(), 6, row.getSeats().getCollection().size());
				}
                if(row.getCode().equals("13")) {
                    fail("No row 13");
                }
                /*if(row.getCode().equals("23")) {
                    assertEquals("Seat 23E is wheelchair", SeatmapStatusType.UNAVAILABLE, row.getSeats().getCollection().get(4));
                }*/
                // Exit rows
                if(row.getCode().equals("14") || row.getCode().equals("15")) {
                    verifyExitRow(row, true);
                } else {
                    verifyExitRow(row, false);
                }
            }
		}

		//24 Rows on coach (from 6 to 25 without 13)
		assertEquals("Wrong total rows", 24, totalRows);

    	InputStream expJsonIn = CheckinSeatmapServiceTest.class.getResourceAsStream("seatmap\\checkin_738-Y-M.json");
		String expectedJsonString = CommonTestUtil.fileToString(expJsonIn);
		System.out.println("Expected:" + expectedJsonString);

		assertNotNull(expectedJsonString);
		assertTrue(expectedJsonString.equalsIgnoreCase(seatmapCollectionString));
	}

    @Test
	public void test_777_J_CheckIn_Seatmap() throws Exception {
		SeatmapCollection seatmapCollection = setupSeatmapFromFile("777-EZE-MEX-AM-29-J-C");

		System.out.println(new Date().toString());
		assertNotNull(seatmapCollection);

		String seatmapCollectionString = seatmapCollection.toString();
		System.out.println("Result  :" + seatmapCollectionString);

		assertNotNull(seatmapCollectionString);

		//6 cabins
		AbstractSeatMap abstractSeaMap = seatmapCollection.getCollection().get(0);
		SeatMap seatMap = new SeatMap();
		if(abstractSeaMap instanceof SeatMap){
			seatMap = (SeatMap) abstractSeaMap;
		}
		assertEquals(6, seatMap.getSections().getCollection().size());

		//7 rows on First
		SeatmapSection firstSection = seatMap.getSections().getCollection().get(0);
		assertEquals(7, firstSection.getRows().getCollection().size());

		for(SeatmapRow row : firstSection.getRows().getCollection()) {
			//7 seats per row on First
			assertEquals(7, row.getSeats().getCollection().size());
			for(SeatmapSeat seat : row.getSeats().getCollection()) {
				if(seat.getChoice() instanceof SeatChoice) {
					SeatChoice choice = (SeatChoice)seat.getChoice();
					//A & J should be window
					if(seat.getCode().contains("A") || seat.getCode().contains("J")) {
						assertTrue(choice.getSeatCharacteristics().contains(SeatCharacteristicsType.W));
					}
					//C, D, F, G should be aisle
					if(seat.getCode().contains("C") || seat.getCode().contains("D") || seat.getCode().contains("F") || seat.getCode().contains("G")) {
						assertTrue(choice.getSeatCharacteristics().contains(SeatCharacteristicsType.A));
					}
				}
			}
		}

		SeatmapSection coachSection = seatMap.getSections().getCollection().get(1);

		for(SeatmapRow row : coachSection.getRows().getCollection()) {
			for(SeatmapSeat seat : row.getSeats().getCollection()) {
				//All seats on economy class should be unavailable
				assertEquals(SeatmapStatusType.UNAVAILABLE, seat.getStatus());
			}
		}

		InputStream expJsonIn = CheckinSeatmapServiceTest.class.getResourceAsStream("seatmap\\checkin_777-J-C.json");
		String expectedJsonString = CommonTestUtil.fileToString(expJsonIn);
		System.out.println("Expected:" + expectedJsonString);

		assertNotNull(expectedJsonString);
		assertTrue(expectedJsonString.equalsIgnoreCase(seatmapCollectionString));
	}

	@Test
	public void test_777_Y_CheckIn_Seatmap() throws Exception {
		SeatmapCollection seatmapCollection = setupSeatmapFromFile("777-EZE-MEX-AM-29-Y-M");
		assertNotNull(seatmapCollection);

		String seatmapCollectionString = seatmapCollection.toString();
		System.out.println("Result: " + seatmapCollectionString);

		assertNotNull(seatmapCollectionString);

		//6 sections: : First, AMPlus, Preferred, coach, preferred, coach
		AbstractSeatMap abstractSeaMap = seatmapCollection.getCollection().get(0);
		SeatMap seatMap = new SeatMap();
		if(abstractSeaMap instanceof SeatMap){
			seatMap = (SeatMap) abstractSeaMap;
		}
		assertEquals(6, seatMap.getSections().getCollection().size());

		SeatmapSection firstSection = seatMap.getSections().getCollection().get(0);

		for(SeatmapRow row : firstSection.getRows().getCollection()) {
			for(SeatmapSeat seat : row.getSeats().getCollection()) {
				//All seats on first class should be unavailable
				assertEquals(SeatmapStatusType.UNAVAILABLE, seat.getStatus());
			}
		}

		int totalRows = 0;
		for(int i = 1; i <= 5; i++) {
			totalRows += seatMap.getSections().getCollection().get(i).getRows().getCollection().size();
			SeatmapSection section = seatMap.getSections().getCollection().get(i);
			//9 seats per row on coach except row 12
			for(SeatmapRow row : section.getRows().getCollection()) {
				if(row.getCode().equals("22") || row.getCode().equals("35")) {
					assertEquals("Wrong seat count row: " + row.getCode(), 3, row.getSeats().getCollection().size());
				} else if(row.getCode().equals("23")){
                    assertEquals("Wrong seat count row: " + row.getCode(), 6, row.getSeats().getCollection().size());
                } else {
                    assertEquals("Wrong seat count row: " + row.getCode(), 9, row.getSeats().getCollection().size());
                }
                //No row 13
                if(row.getCode().equals("13")) {
                    fail("No row 13");
                }
                //Row 23 Seat E is a wheelchair space, should be unavailable
                /*if(row.getCode().equals("23")) {
                    assertEquals(SeatmapStatusType.UNAVAILABLE, row.getSeats().getCollection().get(4).getStatus());
                }
                */
                if(row.getCode().equals("23")) {
                    verifyExitRow(row, true);
                } else {
                    verifyExitRow(row, false);
                }
			}
		}

		//27 Rows on coach (from 6 to 25 without 13)
		assertEquals(27, totalRows);

		InputStream expJsonIn = CheckinSeatmapServiceTest.class.getResourceAsStream("seatmap\\checkin_777-Y-M.json");
		String expectedJsonString = CommonTestUtil.fileToString(expJsonIn);
		System.out.println("Expected:" + expectedJsonString);

		assertNotNull(expectedJsonString);
		assertTrue(expectedJsonString.equalsIgnoreCase(seatmapCollectionString));

	}

	@Test
	public void test_788_J_CheckIn_Seatmap() throws Exception {

		SeatmapCollection seatmapCollection = setupSeatmapFromFile("788-MEX-LHR-AM-7-J-C");

		System.out.println(new Date().toString());
		assertNotNull(seatmapCollection);

		String seatmapCollectionString = seatmapCollection.toString();
		System.out.println("Result  :" + seatmapCollectionString);

		assertNotNull(seatmapCollectionString);

		//One segment
		assertEquals(1, seatmapCollection.getCollection().size());

		//5 class: First and coach
		AbstractSeatMap abstractSeaMap = seatmapCollection.getCollection().get(0);
		SeatMap seatMap = new SeatMap();
		if(abstractSeaMap instanceof SeatMap){
			seatMap = (SeatMap) abstractSeaMap;
		}
		assertEquals("Wrong class count", 5, seatMap.getSections().getCollection().size());

		//6 rows on First
		SeatmapSection firstSection = seatMap.getSections().getCollection().get(0);
		assertEquals(6, firstSection.getRows().getCollection().size());

		for(SeatmapRow row : firstSection.getRows().getCollection()) {
			//6 seats per row on First, except on row 5
			if(!row.getCode().equals("5")) {
				assertEquals("Wrong seat count row: " + row.getCode(), 6, row.getSeats().getCollection().size());
			} else {
				assertEquals("Wrong seat count row: " + row.getCode(), 2, row.getSeats().getCollection().size());
			}
			for(SeatmapSeat seat : row.getSeats().getCollection()) {
				if(seat.getChoice() instanceof SeatChoice) {
					SeatChoice choice = (SeatChoice)seat.getChoice();
					//A & J should be window
					if(seat.getCode().contains("A") || seat.getCode().contains("J")) {
						assertTrue("Should be Window: " + seat.getCode(), choice.getSeatCharacteristics().contains(SeatCharacteristicsType.W));
					}
					//all others should be aisle
					else  {
						assertTrue(choice.getSeatCharacteristics().contains(SeatCharacteristicsType.A));
					}
				}
			}
		}

		SeatmapSection coachSection = seatMap.getSections().getCollection().get(1);

		for(SeatmapRow row : coachSection.getRows().getCollection()) {
			for(SeatmapSeat seat : row.getSeats().getCollection()) {
				//All seats on economy class should be unavailable
				assertEquals(SeatmapStatusType.UNAVAILABLE, seat.getStatus());
			}
		}

		InputStream expJsonIn = CheckinSeatmapServiceTest.class.getResourceAsStream("seatmap\\checkin_788-J-C.json");
		String expectedJsonString = CommonTestUtil.fileToString(expJsonIn);
		System.out.println("Expected:" + expectedJsonString);

		assertNotNull(expectedJsonString);
		assertTrue(expectedJsonString.equalsIgnoreCase(seatmapCollectionString));

	}

	@Test
	public void test_788_Y_CheckIn_Seatmap() throws Exception {
		SeatmapCollection seatmapCollection = setupSeatmapFromFile("788-MEX-LHR-AM-7-Y-Y");

		System.out.println(new Date().toString());
		assertNotNull(seatmapCollection);

		String seatmapCollectionString = seatmapCollection.toString();
		System.out.println("Result  :" + seatmapCollectionString);

		assertNotNull(seatmapCollectionString);

		//One segment
		assertEquals(1, seatmapCollection.getCollection().size());

		//5 sections: : First, AMPlus, coach, preferred, coach
		AbstractSeatMap abstractSeaMap = seatmapCollection.getCollection().get(0);
		SeatMap seatMap = new SeatMap();
		if(abstractSeaMap instanceof SeatMap){
			seatMap = (SeatMap) abstractSeaMap;
		}
		assertEquals(5, seatMap.getSections().getCollection().size());

		SeatmapSection firstSection = seatMap.getSections().getCollection().get(0);

		for(SeatmapRow row : firstSection.getRows().getCollection()) {
			for(SeatmapSeat seat : row.getSeats().getCollection()) {
				//All seats on first class should be unavailable
				assertEquals(SeatmapStatusType.UNAVAILABLE, seat.getStatus());
			}
		}

		int totalRows = 0;
		for(int i = 1; i <= 4; i++) {
			totalRows += seatMap.getSections().getCollection().get(i).getRows().getCollection().size();
			SeatmapSection section = seatMap.getSections().getCollection().get(i);
			//9 seats per row on coach except row 12
			for(SeatmapRow row : section.getRows().getCollection()) {
				if(row.getCode().equals("22") || row.getCode().equals("33")) {
					assertEquals("Wrong seat count row: " + row.getCode(), 3, row.getSeats().getCollection().size());
				} else if(row.getCode().equals("32")) {
                    assertEquals("Wrong seat count row: " + row.getCode(), 7, row.getSeats().getCollection().size());
                } else {
                    assertEquals("Wrong seat count row: " + row.getCode(), 9, row.getSeats().getCollection().size());
                }
                //Row 13 does not exist
                if(row.getCode().equals("13")) {
                    fail("No row 13");
                }
                //Row 12  has only 4 seats

			}
		}

		//25 Rows on coach (from 8 to 33 without 13)
		assertEquals(25, totalRows);

        //Seat 23 is a wheelchair space, should be unavailable
		//assertEquals(SeatmapStatusType.UNAVAILABLE, seatMap.getSections().getCollection().get(5).getRows().getCollection().get(7).getSeats().getCollection().get(4));

		InputStream expJsonIn = CheckinSeatmapServiceTest.class.getResourceAsStream("seatmap\\checkin_788-Y-Y.json");
		String expectedJsonString = CommonTestUtil.fileToString(expJsonIn);
		System.out.println("Expected:" + expectedJsonString);

		assertNotNull(expectedJsonString);
        System.out.println(StringUtils.difference(expectedJsonString, seatmapCollectionString));
		assertTrue(expectedJsonString.equalsIgnoreCase(seatmapCollectionString));

	}

	@Test
	public void test_783_J_CheckIn_Seatmap() throws Exception {
		SeatmapCollection seatmapCollection = setupSeatmapFromFile("783-MEX-NRT-AM-58-J-C");

		System.out.println(new Date().toString());
		assertNotNull(seatmapCollection);

		String seatmapCollectionString = seatmapCollection.toString();
		System.out.println("Result  :" + seatmapCollectionString);

		assertNotNull(seatmapCollectionString);

		//One segment
		assertEquals(1, seatmapCollection.getCollection().size());

		//6 cabins: First, amplus, preferred, coach, preferred, coach
		AbstractSeatMap abstractSeaMap = seatmapCollection.getCollection().get(0);
		SeatMap seatMap = new SeatMap();
		if(abstractSeaMap instanceof SeatMap){
			seatMap = (SeatMap) abstractSeaMap;
		}
		assertEquals("Wrong class count", 6, seatMap.getSections().getCollection().size());

		//6 rows on First
		SeatmapSection firstSection = seatMap.getSections().getCollection().get(0);
		assertEquals(6, firstSection.getRows().getCollection().size());

		for(SeatmapRow row : firstSection.getRows().getCollection()) {
			//6 seats per row on First, except on row 5
			if(!row.getCode().equals("5")) {
				assertEquals("Wrong seat count row: " + row.getCode(), 6, row.getSeats().getCollection().size());
			} else {
				assertEquals("Wrong seat count row: " + row.getCode(), 2, row.getSeats().getCollection().size());
			}
			for(SeatmapSeat seat : row.getSeats().getCollection()) {
				if(seat.getChoice() instanceof SeatChoice) {
					SeatChoice choice = (SeatChoice)seat.getChoice();
					//A & J should be window
					if(seat.getCode().contains("A") || seat.getCode().contains("J")) {
						assertTrue("Seat should be window: " + seat.getCode(), choice.getSeatCharacteristics().contains(SeatCharacteristicsType.W));
					}
					//all others should be aisle
					else  {
						assertTrue("Seat should be aisle: " + seat.getCode(),choice.getSeatCharacteristics().contains(SeatCharacteristicsType.A));
					}
				}
			}
		}

		SeatmapSection coachSection = seatMap.getSections().getCollection().get(1);

		for(SeatmapRow row : coachSection.getRows().getCollection()) {
			for(SeatmapSeat seat : row.getSeats().getCollection()) {
				//All seats on economy class should be unavailable
				assertEquals(SeatmapStatusType.UNAVAILABLE, seat.getStatus());
			}
		}

		InputStream expJsonIn = CheckinSeatmapServiceTest.class.getResourceAsStream("seatmap\\checkin_783-J-C.json");
		String expectedJsonString = CommonTestUtil.fileToString(expJsonIn);
		System.out.println("Expected:" + expectedJsonString);

		assertNotNull(expectedJsonString);
		assertTrue(expectedJsonString.equalsIgnoreCase(seatmapCollectionString));

	}

	@Test
	public void test_783_Y_CheckIn_Seatmap() throws Exception {
		SeatmapCollection seatmapCollection = setupSeatmapFromFile("783-MEX-NRT-AM-58-Y-M");

		System.out.println(new Date().toString());
		assertNotNull(seatmapCollection);

		String seatmapCollectionString = seatmapCollection.toString();
		System.out.println("Result  :" + seatmapCollectionString);

		assertNotNull(seatmapCollectionString);
/*
		//One segment
		assertEquals(1, seatmapCollection.getCollection().size());

		//6 sections: : First, AMPlus, Preferred, coach, preferred, coach
		SeatMap seatMap = seatmapCollection.getCollection().get(0);
		assertEquals(6, seatMap.getSections().getCollection().size());

		SeatmapSection firstSection = seatMap.getSections().getCollection().get(0);

		for(SeatmapRow row : firstSection.getRows().getCollection()) {
			for(SeatmapSeat seat : row.getSeats().getCollection()) {
				//All seats on first class should be unavailable
				assertEquals(SeatmapStatusType.UNAVAILABLE, seat.getStatus());
			}
		}

		int totalRows = 0;
		for(int i = 1; i <= 6; i++) {
			totalRows += seatMap.getSections().getCollection().get(i).getRows().getCollection().size();
			SeatmapSection section = seatMap.getSections().getCollection().get(i);
			//6 seats per row on coach except row 12
			for(SeatmapRow row : section.getRows().getCollection()) {
				if(!row.getCode().equals("29")) {
					assertEquals("Wrong seat count row: " + row.getCode(), 6, row.getSeats().getCollection().size());
				}
			}
		}

		//24 Rows on coach (from 6 to 25 without 13)
		assertEquals(24, totalRows);

		//Row 12  has only 4 seats
		assertEquals(4, seatMap.getSections().getCollection().get(3).getRows().getCollection().get(0).getSeats().getCollection().size());

		//Row 13 does not exist
		assertEquals("No row 13", "14", seatMap.getSections().getCollection().get(4).getRows().getCollection().get(0).getCode());

		//Seat 23 is a wheelchair space, should be unavailable
		assertEquals(SeatmapStatusType.UNAVAILABLE, seatMap.getSections().getCollection().get(5).getRows().getCollection().get(7).getSeats().getCollection().get(4));
*/
		InputStream expJsonIn = CheckinSeatmapServiceTest.class.getResourceAsStream("seatmap\\checkin_783-Y-M.json");
		String expectedJsonString = CommonTestUtil.fileToString(expJsonIn);
		System.out.println("Expected:" + expectedJsonString);

		assertNotNull(expectedJsonString);
		assertTrue(expectedJsonString.equalsIgnoreCase(seatmapCollectionString));

	}



	@Test
	public void test_787_Y_CheckIn_Seatmap() throws Exception {

		SeatmapCollection seatmapCollection = setupSeatmapFromFile("787-MEX-JFK-AM-408-Y-M");

		System.out.println(new Date().toString());
		assertNotNull(seatmapCollection);

		String seatmapCollectionString = seatmapCollection.toString();
		System.out.println("Result  :" + seatmapCollectionString);

		assertNotNull(seatmapCollectionString);

		InputStream expJsonIn = CheckinSeatmapServiceTest.class.getResourceAsStream("seatmap\\checkin_787-Y-M.json");
		String expectedJsonString = CommonTestUtil.fileToString(expJsonIn);
		System.out.println("Expected:" + expectedJsonString);

		assertNotNull(expectedJsonString);
		assertTrue(expectedJsonString.equalsIgnoreCase(seatmapCollectionString));
	}

	@Test
	public void test_787_J_CheckIn_Seatmap() throws Exception {

		SeatmapCollection seatmapCollection = setupSeatmapFromFile("787-MEX-JFK-AM-408-J-C");

		System.out.println(new Date().toString());
		assertNotNull(seatmapCollection);

		String seatmapCollectionString = seatmapCollection.toString();
		System.out.println("Result  :" + seatmapCollectionString);

		assertNotNull(seatmapCollectionString);

		InputStream expJsonIn = CheckinSeatmapServiceTest.class.getResourceAsStream("seatmap\\checkin_787-J-C.json");
		String expectedJsonString = CommonTestUtil.fileToString(expJsonIn);
		System.out.println("Expected:" + expectedJsonString);

		assertNotNull(expectedJsonString);
		assertTrue(expectedJsonString.equalsIgnoreCase(seatmapCollectionString));
	}

	@Test
	public void test_772_CheckIn_Seatmap() throws Exception {

		SeatmapCollection seatmapCollection = setupSeatmapFromFile("772-MAD-MEX-AM-0002-Y-M");

		System.out.println(new Date().toString());
		assertNotNull(seatmapCollection);

		String seatmapCollectionString = seatmapCollection.toString();
		System.out.println("Result  :" + seatmapCollectionString);

		assertNotNull(seatmapCollectionString);

		InputStream expJsonIn = CheckinSeatmapServiceTest.class.getResourceAsStream("seatmap\\checkin_772-Y-M.json");
		String expectedJsonString = CommonTestUtil.fileToString(expJsonIn);
		System.out.println("Expected:" + expectedJsonString);

		assertNotNull(expectedJsonString);
		assertTrue(expectedJsonString.equalsIgnoreCase(seatmapCollectionString));
	}

	@Test
	public void test_7S8_Y_CheckIn_Seatmap() throws Exception {
	}

	@Test
	public void test_7S8_J_CheckIn_Seatmap() throws Exception {
	}

	@Test
	public void test_E70_Y_CheckIn_Seatmap() throws Exception {

		SeatmapCollection seatmapCollection = setupSeatmapFromFile("E70-MEX-VER-AM-549-Y-M");

		System.out.println(new Date().toString());
		assertNotNull(seatmapCollection);

		String seatmapCollectionString = seatmapCollection.toString();
		System.out.println("Result  :" + seatmapCollectionString);

		assertNotNull(seatmapCollectionString);

		//3 sections: AMPlus, Preferred, coach
		AbstractSeatMap abstractSeaMap = seatmapCollection.getCollection().get(0);
		SeatMap seatMap = new SeatMap();
		if(abstractSeaMap instanceof SeatMap){
			seatMap = (SeatMap) abstractSeaMap;
		}
		assertEquals(3, seatMap.getSections().getCollection().size());

		int totalRows = 0;
		for(int i = 0; i <= 2; i++) {
			totalRows += seatMap.getSections().getCollection().get(i).getRows().getCollection().size();
			SeatmapSection section = seatMap.getSections().getCollection().get(i);
			//4 seats per row on coach
			for(SeatmapRow row : section.getRows().getCollection()) {
				assertEquals(4, row.getSeats().getCollection().size());
                //No row 13
                if(row.getCode().equals("13")) {
                    fail("No row 13");
                }
			}
		}

		//19 Rows total, no First class (no 13)
		assertEquals("Total rows", 19, totalRows);
		//1 row amplus
		assertEquals("Total amPlus rows", 1, seatMap.getSections().getCollection().get(0).getRows().getCollection().size());
		//4 rows preferred
		assertEquals("Total preferred rows",4, seatMap.getSections().getCollection().get(1).getRows().getCollection().size());
		//14 rows coach
		assertEquals("Total coach rows",14, seatMap.getSections().getCollection().get(2).getRows().getCollection().size());

		InputStream expJsonIn = CheckinSeatmapServiceTest.class.getResourceAsStream("seatmap\\checkin_E70-Y-M.json");
		String expectedJsonString = CommonTestUtil.fileToString(expJsonIn);
		System.out.println("Expected:" + expectedJsonString);

		assertNotNull(expectedJsonString);
		assertTrue(expectedJsonString.equalsIgnoreCase(seatmapCollectionString));
	}

	@Test
	public void test_E90_Y_CheckIn_Seatmap() throws Exception {

		SeatmapCollection seatmapCollection = setupSeatmapFromFile("E90-MEX-LAP-AM-2070-Y-M");

		System.out.println(new Date().toString());
		assertNotNull(seatmapCollection);

		String seatmapCollectionString = seatmapCollection.toString();
		System.out.println("Result  :" + seatmapCollectionString);

		assertNotNull(seatmapCollectionString);

		//One segment
		assertEquals(1, seatmapCollection.getCollection().size());

		//6 sections: : First, AMPlus, Preferred, coach
		AbstractSeatMap abstractSeaMap = seatmapCollection.getCollection().get(0);
		SeatMap seatMap = new SeatMap();
		if(abstractSeaMap instanceof SeatMap){
			seatMap = (SeatMap) abstractSeaMap;
		}
		assertEquals(4, seatMap.getSections().getCollection().size());

		SeatmapSection firstSection = seatMap.getSections().getCollection().get(0);

		for(SeatmapRow row : firstSection.getRows().getCollection()) {
			for(SeatmapSeat seat : row.getSeats().getCollection()) {
				//All seats on first class should be unavailable
				assertEquals(SeatmapStatusType.UNAVAILABLE, seat.getStatus());
			}
		}

		int totalRows = 0;
		for(int i = 1; i <= 3; i++) {
			totalRows += seatMap.getSections().getCollection().get(i).getRows().getCollection().size();
			SeatmapSection section = seatMap.getSections().getCollection().get(i);
			//4 seats per row on coach
			for(SeatmapRow row : section.getRows().getCollection()) {
				assertEquals(4, row.getSeats().getCollection().size());
				for(SeatmapSeat seat : row.getSeats().getCollection()) {
					if (seat.getChoice() instanceof SeatChoice) {
						SeatChoice choice = (SeatChoice) seat.getChoice();
						//A & D should window
						if (seat.getCode().contains("A") || seat.getCode().contains("D")) {
							assertTrue(choice.getSeatCharacteristics().contains(SeatCharacteristicsType.W));
						}
						//B & C should be aisle
						if (seat.getCode().contains("B") || seat.getCode().contains("C")) {
							assertTrue(choice.getSeatCharacteristics().contains(SeatCharacteristicsType.A));
						}
					}
				}
                //No row 13
                if(row.getCode().equals("13")) {
                    fail("No row 13");
                }
                //Row 12 is exit
                if(row.getCode().equals("12")) {
                    verifyExitRow(row,true);
                } else {
                    verifyExitRow(row,false);
                }

			}
		}

		//22 Rows on coach (from 6 to 25 without 13)
		assertEquals(22, totalRows);

    	InputStream expJsonIn = CheckinSeatmapServiceTest.class.getResourceAsStream("seatmap\\checkin_E90-Y-M.json");
		String expectedJsonString = CommonTestUtil.fileToString(expJsonIn);
		System.out.println("Expected:" + expectedJsonString);

		assertNotNull(expectedJsonString);
		assertTrue(expectedJsonString.equalsIgnoreCase(seatmapCollectionString));
	}

	@Test
	public void test_E90_J_CheckIn_Seatmap() throws Exception {

		SeatmapCollection seatmapCollection = setupSeatmapFromFile("E90-MEX-LAP-AM-2070-J-J");

		System.out.println(new Date().toString());
		assertNotNull(seatmapCollection);

		String seatmapCollectionString = seatmapCollection.toString();
		System.out.println("Result  :" + seatmapCollectionString);

		assertNotNull(seatmapCollectionString);

		//One segment
		assertEquals(1, seatmapCollection.getCollection().size());

		//6 cabins:
		AbstractSeatMap abstractSeaMap = seatmapCollection.getCollection().get(0);
		SeatMap seatMap = new SeatMap();
		if(abstractSeaMap instanceof SeatMap){
			seatMap = (SeatMap) abstractSeaMap;
		}
		assertEquals(6, seatMap.getSections().getCollection().size());

		// 4 rows on First
		SeatmapSection firstSection = seatMap.getSections().getCollection().get(0);
		assertEquals(4, firstSection.getRows().getCollection().size());

		//first row has two seats
		assertEquals("First row should have 2 seats", 2, firstSection.getRows().getCollection().get(0).getSeats().getCollection().size());

		for(SeatmapRow row : firstSection.getRows().getCollection()) {
			//3 seats per on #2 - #4 rows
            if(row.getCode().equals("1")) {
                assertEquals("Wrong seat count on row: " + row.getCode(), 2, row.getSeats().getCollection().size());
            } else {
                assertEquals("Wrong seat count on row: " + row.getCode(), 3, row.getSeats().getCollection().size());
            }

			for(SeatmapSeat seat : row.getSeats().getCollection()) {
				if(seat.getChoice() instanceof SeatChoice) {
					SeatChoice choice = (SeatChoice)seat.getChoice();
					//A & D should be aisle and window
					if(seat.getCode().contains("A") || seat.getCode().contains("D")) {
						assertTrue("Should be Window: " + seat.getCode(), choice.getSeatCharacteristics().contains(SeatCharacteristicsType.W));
					}
					//A & E should be aisle
					if(seat.getCode().contains("A") || seat.getCode().contains("C")) {
						assertTrue("Should be aisle: " + seat.getCode(), choice.getSeatCharacteristics().contains(SeatCharacteristicsType.A));
					}
				}
			}
		}

		SeatmapSection coachSection = seatMap.getSections().getCollection().get(1);

		for(SeatmapRow row : coachSection.getRows().getCollection()) {
			for(SeatmapSeat seat : row.getSeats().getCollection()) {
				//All seats on economy class should be unavailable
				assertEquals(SeatmapStatusType.UNAVAILABLE, seat.getStatus());
			}
		}

		InputStream expJsonIn = CheckinSeatmapServiceTest.class.getResourceAsStream("seatmap\\checkin_E90-J-J.json");
		String expectedJsonString = CommonTestUtil.fileToString(expJsonIn);
		System.out.println("Expected:" + expectedJsonString);

		assertNotNull(expectedJsonString);
		assertTrue(expectedJsonString.equalsIgnoreCase(seatmapCollectionString));
	}

	@Test
	public void test_ER4_CheckIn_Seatmap() throws Exception {

		SeatmapCollection seatmapCollection = setupSeatmapFromFile("ER4-ACA-MEX-302-Y-M");

		System.out.println(new Date().toString());
		assertNotNull(seatmapCollection);

		String seatmapCollectionString = seatmapCollection.toString();
		System.out.println("Result: " + seatmapCollectionString);

		assertNotNull(seatmapCollectionString);

		//One segment
		assertEquals(1, seatmapCollection.getCollection().size());

		//4 sections: prefered, coach, Preferred, coach
		AbstractSeatMap abstractSeaMap = seatmapCollection.getCollection().get(0);
		SeatMap seatMap = new SeatMap();
		if(abstractSeaMap instanceof SeatMap){
			seatMap = (SeatMap) abstractSeaMap;
		}
		assertEquals("Wrong total sections", 4, seatMap.getSections().getCollection().size());

		int totalRows = 0;
		for(int i = 0; i <= 3; i++) {
			totalRows += seatMap.getSections().getCollection().get(i).getRows().getCollection().size();
			SeatmapSection section = seatMap.getSections().getCollection().get(i);
			//1 seat in row {1,2};  3 seats per row on coach starting from row 3
			for(SeatmapRow row : section.getRows().getCollection()) {
				if(row.getCode().equals("1") || row.getCode().equals("2")) {
					assertEquals("Wrong total seats" + row.getCode(), 1, row.getSeats().getCollection().size());
				} else {
					assertEquals("Wrong total seats" + row.getCode(), 3, row.getSeats().getCollection().size());
				}
			}
		}

		//18 Rows total, no First class
		assertEquals("Wrong total rows", 18, totalRows);

		//Row 13 does not exist
		assertEquals("No row 13", "14", seatMap.getSections().getCollection().get(3).getRows().getCollection().get(0).getCode());

		InputStream expJsonIn = CheckinSeatmapServiceTest.class.getResourceAsStream("seatmap\\checkin_ER4-Y-M.json");
		String expectedJsonString = CommonTestUtil.fileToString(expJsonIn);
		System.out.println("Expected:" + expectedJsonString);

		assertNotNull(expectedJsonString);
		assertTrue(expectedJsonString.equalsIgnoreCase(seatmapCollectionString));
	}

}
