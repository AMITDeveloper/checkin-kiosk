
package com.am.checkin.web.service;

import java.io.InputStream;

import com.aeromexico.sabre.api.seatmap.AMSeatMapService;
import com.am.checkin.web.util.CommonTestUtil;
import com.sabre.stl.merchandising.v5.EnhancedSeatMapRS;

public class AMSeatMapServiceMock extends AMSeatMapService {

	@Override
	public EnhancedSeatMapRS getSeatMap(String airlineCode, String flightNumber, String departureCity, String destinationCity, String departureDate, String arrivalDate, String currency, String cabinClass, boolean isCheckIn, String pseudoCityCode, String cityCode, String cityCodeCommand) throws Exception {
		InputStream enhancedSeatMapIS = AMSeatMapServiceMock.class.getResourceAsStream( "seatmap/" + airlineCode + flightNumber + "_" + departureCity + "_" + destinationCity + "_" + cabinClass + ".xml" );
		EnhancedSeatMapRS enhancedSeatMapRS = CommonTestUtil.getEnhancedSeatMap(enhancedSeatMapIS);
		return enhancedSeatMapRS;
	}

}
