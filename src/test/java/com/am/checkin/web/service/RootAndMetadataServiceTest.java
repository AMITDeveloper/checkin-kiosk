package com.am.checkin.web.service;

import com.aeromexico.commons.model.Metadata;
import com.aeromexico.dao.test.EnvironmentTest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.Test;

public class RootAndMetadataServiceTest extends EnvironmentTest {

    @Test
    public void testRootAndMetadataService() {

        MetadataFactory metadataFactory = new MetadataFactory();
        metadataFactory.setSystemPropertiesFactory(setUpConfigFactory.getSystemPropertiesFactory());

        RootAndMetadataService rootAndMetadataService = new RootAndMetadataService();
        rootAndMetadataService.setMetadataFactory(metadataFactory);

        Metadata metadata = rootAndMetadataService.getRootAndMetadata();

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(metadata);
        System.out.println(json);
    }

}
