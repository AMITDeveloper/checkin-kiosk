package com.am.checkin.web.service;

import com.aeromexico.commons.model.weather.DailyForecasts;
import com.aeromexico.dao.factories.AirportWeatherWrapperFactory;
import com.aeromexico.dao.factories.AirportWrapperFactory;
import com.aeromexico.dao.services.SetUpCacheData;
import com.aeromexico.dao.test.EnvironmentTest;
import com.aeromexico.weather.service.WeatherService;
import org.junit.BeforeClass;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class WeatherCityServiceTest extends EnvironmentTest {

    @BeforeClass
    public static void before() throws RuntimeException {
        SetUpCacheData setUpCacheData = new SetUpCacheData();
        setUpCacheData.setDbAPI(testDBAPI);

        AirportWrapperFactory airportWrapperFactory = new AirportWrapperFactory();
        airportWrapperFactory.setCache(setUpCacheData);

        AirportWeatherWrapperFactory airportWeatherWrapperFactory = new AirportWeatherWrapperFactory();
        airportWeatherWrapperFactory.setCache(setUpCacheData);
    }

    @Test
    public void getWeather1() throws Exception {
        String city = "CUN";
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        Calendar now = Calendar.getInstance();
//        int hour = now.get(Calendar.HOUR);
//        System.out.println("hour: " + hour);
//
//        if (hour > 10 && Calendar.PM == now.get(Calendar.AM_PM)) {
//            now.add(Calendar.DAY_OF_MONTH, 1);
//        }
//        now.set(Calendar.HOUR, 0);
//        now.set(Calendar.MINUTE, 0);
//        now.set(Calendar.AM_PM, Calendar.PM);

        String arrivalDate = formatter.format(now.getTime());
        System.out.println("arrivalDate: " + arrivalDate);
        boolean isLanguageMX = false;

        DailyForecasts dailyForecasts = WeatherService.getWeather(city, arrivalDate, isLanguageMX);
        //assertNotNull(dailyForecasts);
        System.out.println(dailyForecasts);
    }

    @Test
    public void getWeather2() throws Exception {
        String city = "MEX";
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String arrivalDate = formatter.format(new Date());
        boolean isLanguageMX = false;

        DailyForecasts dailyForecasts = WeatherService.getWeather(city, arrivalDate, isLanguageMX);
        //assertNotNull(dailyForecasts);
        System.out.println(dailyForecasts);
    }


}
