package com.am.checkin.web.service;

import com.aeromexico.sabre.api.schedule.ScheduleService;
import com.am.checkin.web.util.CommonUtilFlifo;
import com.sabre.webservices.sabrexml._2011._10.schedule.OTAAirScheduleRS;
import java.io.InputStream;

/**
 *
 * @author Franco
 */
public class ScheduleServiceMock extends ScheduleService{

    @Override
    public OTAAirScheduleRS getSchedule(String departure, String arrival, String departureDateTime, String cityCode, String cityCodeCommand) throws Exception {
        InputStream otaAirFlifo = getClass().getResourceAsStream("AirSchedule_"+departure+arrival+".xml");
        OTAAirScheduleRS otaAirFlifoRS = CommonUtilFlifo.getOtaAirScheduleRS(otaAirFlifo);
        return otaAirFlifoRS;
    }
}
