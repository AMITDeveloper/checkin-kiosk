
package com.am.checkin.web.service;

import java.math.BigDecimal;

import com.aeromexico.webe4.web.airport.model.Airport;

public class AirportMock extends Airport {

	public AirportMock(String airportCode, String regionKey) {

		//private ObjectId _id;
	    //private Integer id;
	    //private String airportDesc;
	    //private List<String> fliesToAirportCode;

		/*
		"airport" : { 
			"title" : "IAH", 
			"code" : "IAH", 
			"region" : "USA", 
			"skyteam" : "false", 
			"mobileCheckin" : "false", 
			"country" : "US", 
			"countryCode" : "US-TX", 
			"city" : "Houston", 
			"nearbyAirports" : "", 
			"name" : "George Bush Intercontinental Houston Airport", 
		*/

		super.setAirportCode(airportCode);
		super.setLatitude ( BigDecimal.valueOf(29.9843998) );
		super.setLongitude ( BigDecimal.valueOf(-95.34140015) );
		super.setRegionKey(regionKey);
	}
}
