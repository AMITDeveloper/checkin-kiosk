package com.am.checkin.web.util;

import com.am.checkin.web.service.PectabReceiptService;
import com.aeromexico.commons.model.PectabReceipt;
import com.aeromexico.commons.model.PectabReceiptData;
import com.aeromexico.commons.model.ReceiptEmdsWrapper;
import com.aeromexico.commons.web.types.LanguageType;
import com.aeromexico.commons.web.util.TimeUtil;
import com.am.checkin.web.messages.ReceiptPectabMessageProvider;
import java.util.Date;
import org.junit.Test;

/**
 *
 * @author adrian
 */
public class PectabReceiptUtilTest {

    @Test
    public void getPectab() {

        Date d = new Date();

        String date = TimeUtil.getStrTime(d.getTime(), "MMyy");//"04AGO2016";
        String time = TimeUtil.getStrTime(d.getTime(), "HH:mm:ss");//"09:40:20";
        String folio = "XXXXXXXXMEXAT";
        String cardNumber = "XXXX1110";
        String expiration = "1015";
        String cardName = "MASTERCARD";
        String total = "MXN 18376";
        String cardHolder = "ROBERTO ZOZAYA";
        String alabel = "XXXAPPLABEL";
        String aprname = "XXXXXXXX XXXX";
        String arqc = "XXXARQCDATA";
        String aid = "XXXXXXXXXXXXXXXX";
        String merchant = "9578139";
        String installment = "00";
        String authorization = "005345";
        String recordLocator = "PWZRTW";

        ReceiptEmdsWrapper receiptEmdsWrapper = new ReceiptEmdsWrapper();

        PectabReceiptData pectabReceiptData = new PectabReceiptData(
                date, folio, cardNumber, expiration, cardName,
                total, recordLocator, cardHolder, alabel, aprname, arqc,
                aid, merchant, installment, authorization, true,
                receiptEmdsWrapper
        );

        ReceiptPectabMessageProvider receiptPectabMessageProvider = new ReceiptPectabMessageProvider();
        
        PectabReceiptService pectabReceiptService = new PectabReceiptService();
        pectabReceiptService.setReceiptPectabMessageProvider(receiptPectabMessageProvider);

        LanguageType languageTypelocal = LanguageType.EN;

        PectabReceipt pectabReceipt = pectabReceiptService.getPectabReceipt(pectabReceiptData, languageTypelocal, "MXN");

        System.out.println(pectabReceipt.toString());

        //validate
    }
}
