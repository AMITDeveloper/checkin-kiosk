
package com.am.checkin.web.util;

import com.sabre.ns.ticketing.dc.GetTicketingDocumentRS;
import com.sabre.services.acs.bso.flightdetail.v3.ACSFlightDetailRSACS;
import com.sabre.services.acs.bso.passengerlist.v3.ACSPassengerListRSACS;
import com.sabre.services.checkin.getpassengerdata.v4.GetPassengerDataRSACS;
import com.sabre.services.checkin.issuebagtag.v4.IssueBagTagRSACS;
import com.sabre.services.res.tir.travelitinerary.TravelItineraryReadRS;
import com.sabre.stl.merchandising.v5.EnhancedSeatMapRS;
import com.sabre.webservices.pnrbuilder.GetPriceListRS;
import com.sabre.webservices.pnrbuilder.getreservation.GetReservationRS;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * 
 * This class loads the xml resource file
 * 
 * @author
 *
 */
public class CommonTestUtil {

	public static IssueBagTagRSACS getIssueBagTagRSACS(InputStream in) {

		IssueBagTagRSACS issueBagTagRSACS;
		JAXBContext ctx = null;

		try {
			ctx = JAXBContext
					.newInstance(com.sabre.services.checkin.issuebagtag.v4.ObjectFactory.class.getPackage().getName());

		} catch (JAXBException e) {
			e.printStackTrace();
			return null;
		}

		try {
			Unmarshaller unmarshaller = ctx.createUnmarshaller();
			@SuppressWarnings("unchecked")
			JAXBElement<IssueBagTagRSACS> issueBagTagRSACSJE = (JAXBElement<IssueBagTagRSACS>) unmarshaller
					.unmarshal(in);
			issueBagTagRSACS = issueBagTagRSACSJE.getValue();

		} catch (JAXBException e) {
			throw new RuntimeException("Failed to parse: " + e.getMessage(), e);
		}

		return issueBagTagRSACS;
	}

	public static ACSPassengerListRSACS getACSPassengerListRSACS(InputStream in) {

		ACSPassengerListRSACS passengerListRSACS;
		JAXBContext ctx = null;

		try {
			ctx = JAXBContext.newInstance(
					com.sabre.services.acs.bso.passengerlist.v3.ObjectFactory.class.getPackage().getName());
		} catch (JAXBException e) {
			e.printStackTrace();
			return null;
		}

		try {
			Unmarshaller unmarshaller = ctx.createUnmarshaller();
			@SuppressWarnings("unchecked")
			JAXBElement<ACSPassengerListRSACS> passengerListResponseJE = (JAXBElement<ACSPassengerListRSACS>) unmarshaller
					.unmarshal(in);
			passengerListRSACS = passengerListResponseJE.getValue();

		} catch (JAXBException e) {
			throw new RuntimeException("Failed to parse: " + e.getMessage(), e);
		}

		return passengerListRSACS;
	}

	public static ACSFlightDetailRSACS getACSFlightDetailRSACS(InputStream in) {

		ACSFlightDetailRSACS flightDetailRSACS;
		JAXBContext ctx = null;

		try {
			ctx = JAXBContext
					.newInstance(com.sabre.services.acs.bso.flightdetail.v3.ObjectFactory.class.getPackage().getName());
		} catch (JAXBException e) {
			e.printStackTrace();
			return null;
		}

		try {

			Unmarshaller unmarshaller = ctx.createUnmarshaller();
			@SuppressWarnings("unchecked")
			JAXBElement<ACSFlightDetailRSACS> fligthDetailRequestJE = (JAXBElement<ACSFlightDetailRSACS>) unmarshaller
					.unmarshal(in);
			flightDetailRSACS = fligthDetailRequestJE.getValue();

		} catch (JAXBException e) {
			throw new RuntimeException("Failed to parse: " + e.getMessage(), e);
		}

		return flightDetailRSACS;
	}

	public static EnhancedSeatMapRS getEnhancedSeatMap(InputStream enhancedSeatMapIS) {

		EnhancedSeatMapRS enhancedSeatMapRS = null;
		JAXBContext ctx = null;

		try {
			ctx = JAXBContext.newInstance(com.sabre.stl.merchandising.v5.ObjectFactory.class,
					org.opentravel.common.v02.ObjectFactory.class,
					com.sabre.services.stl_payload.v02_00.ObjectFactory.class);
		} catch (JAXBException e) {
			e.printStackTrace();
			return null;
		}

		try {
			Unmarshaller unmarshaller = ctx.createUnmarshaller();
			@SuppressWarnings("unchecked")
			JAXBElement<EnhancedSeatMapRS> enhancedSeatMapJE = (JAXBElement<EnhancedSeatMapRS>) unmarshaller
					.unmarshal(enhancedSeatMapIS);
			enhancedSeatMapRS = enhancedSeatMapJE.getValue();
			// enhancedSeatMapRS =
			// (EnhancedSeatMapRS)unmarshaller.unmarshal(enhancedSeatMapIS);
		} catch (JAXBException e) {
			throw new RuntimeException("Failed to parse: " + e.getMessage(), e);
		}

		return enhancedSeatMapRS;
	}

	public static GetTicketingDocumentRS getGetTicketingDocumentRS(InputStream in) {

		GetTicketingDocumentRS getTicketingDocumentRS = null;
		JAXBContext ctx = null;

		try {
			ctx = JAXBContext
					.newInstance(com.sabre.ns.ticketing.dc.GetTicketingDocumentRS.class.getPackage().getName());
		} catch (JAXBException e) {
			e.printStackTrace();
			return null;
		}

		try {
			Unmarshaller unmarshaller = ctx.createUnmarshaller();
			// Object object = unmarshaller.unmarshal(in);
			@SuppressWarnings("unchecked")
			JAXBElement<GetTicketingDocumentRS> getTicketingDocumentRSJE = (JAXBElement<GetTicketingDocumentRS>) unmarshaller
					.unmarshal(in);
			getTicketingDocumentRS = getTicketingDocumentRSJE.getValue();
		} catch (JAXBException e) {
			throw new RuntimeException("Failed to parse: " + e.getMessage(), e);
		}

		return getTicketingDocumentRS;
	}

	public static GetPassengerDataRSACS getACSPassengerDataRSACS(InputStream in) {

		GetPassengerDataRSACS passengerDataRSACS = null;
		JAXBContext ctx = null;

		try {
			ctx = JAXBContext.newInstance(GetPassengerDataRSACS.class.getPackage().getName());
		} catch (JAXBException e) {
			e.printStackTrace();
			return null;
		}

		try {
			Unmarshaller unmarshaller = ctx.createUnmarshaller();
			@SuppressWarnings("unchecked")
			JAXBElement<GetPassengerDataRSACS> passengerDataRSACSJE = (JAXBElement<GetPassengerDataRSACS>) unmarshaller
					.unmarshal(in);
			passengerDataRSACS = passengerDataRSACSJE.getValue();
		} catch (JAXBException e) {
			throw new RuntimeException("Failed to parse: " + e.getMessage(), e);
		}

		return passengerDataRSACS;
	}

	public static TravelItineraryReadRS getTravelItineraryReadRS(InputStream in) {

		TravelItineraryReadRS travelItineraryReadRS = null;
		JAXBContext ctx = null;

		try {
			ctx = JAXBContext.newInstance(
					com.sabre.services.res.tir.travelitinerary.TravelItineraryReadRS.class.getPackage().getName());
		} catch (JAXBException e) {
			e.printStackTrace();
			return null;
		}

		try {
			Unmarshaller unmarshaller = ctx.createUnmarshaller();
			// JAXBElement<TravelItineraryReadRS> travelItineraryReadRSJE =
			// (JAXBElement<TravelItineraryReadRS>) unmarshaller.unmarshal(in);
			// travelItineraryReadRS = travelItineraryReadRSJE.getValue();
			travelItineraryReadRS = (TravelItineraryReadRS) unmarshaller.unmarshal(in);
		} catch (JAXBException e) {
			throw new RuntimeException("Failed to parse: " + e.getMessage(), e);
		}

		return travelItineraryReadRS;
	}

	public static GetReservationRS getReservationRS(InputStream in) {

		GetReservationRS getReservationRS = null;
		JAXBContext ctx = null;

		System.out.println( com.sabre.webservices.pnrbuilder.getreservation.GetReservationRS.class.getPackage().getName() );
		try {
			ctx = JAXBContext.newInstance(com.sabre.webservices.pnrbuilder.getreservation.GetReservationRS.class.getPackage().getName());
		} catch (JAXBException e) {
			e.printStackTrace();
			return null;
		}

		try {
			Unmarshaller unmarshaller = ctx.createUnmarshaller();

			// JAXBElement<GetReservationRS> travelItineraryReadRSJE = (JAXBElement<GetReservationRS>) unmarshaller.unmarshal(in);
			// getReservationRS = travelItineraryReadRSJE.getValue();

			getReservationRS = (GetReservationRS) unmarshaller.unmarshal(in);
		} catch (JAXBException e) {
			throw new RuntimeException("Failed to parse: " + e.getMessage(), e);
		}

		return getReservationRS;
	}



	public static GetPriceListRS getGetPriceListRS(InputStream in) {

		GetPriceListRS getPriceListRS = null;
		JAXBContext ctx = null;

		try {
			ctx = JAXBContext.newInstance(com.sabre.webservices.pnrbuilder.GetPriceListRS.class.getPackage().getName());
		} catch (JAXBException e) {
			e.printStackTrace();
			return null;
		}

		try {
			Unmarshaller unmarshaller = ctx.createUnmarshaller();
			// JAXBElement<GetPriceListRS> getPriceListRSJE = (JAXBElement<GetPriceListRS>)
			// unmarshaller.unmarshal(in);
			// getPriceListRS = getPriceListRSJE.getValue();
			getPriceListRS = (GetPriceListRS) unmarshaller.unmarshal(in);
		} catch (JAXBException e) {
			throw new RuntimeException("Failed to parse: " + e.getMessage(), e);
		}

		return getPriceListRS;
	}

	public static String fileToString(URL fileName) throws Exception {
		return fileToString(fileName.getFile());
	}

	public static String fileToString(String fileName) throws Exception {
		return fileToString(new FileInputStream(fileName));
	}

	public static String fileToString(InputStream fis) throws Exception {

		BufferedReader in = new BufferedReader(new InputStreamReader(fis, "ISO-8859-1"));
		StringBuilder strBuf = new StringBuilder();

		String buf = in.readLine();

		while (buf != null) {
			strBuf.append(buf.trim());
			buf = in.readLine();
		}

		try {
			in.close();
		} catch (Exception e) {
			// ignore exception
		}

		try {
			fis.close();
		} catch (Exception e) {
			// ignore exception
		}

		return strBuf.toString();
	}

}
