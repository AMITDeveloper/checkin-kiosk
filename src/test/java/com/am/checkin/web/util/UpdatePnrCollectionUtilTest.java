/**
 *
 */
package com.am.checkin.web.util;

import com.aeromexico.commons.model.BookedLeg;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import com.aeromexico.commons.model.BookedTraveler;
import com.aeromexico.commons.model.CartPNR;
import com.aeromexico.commons.model.PNR;
import com.aeromexico.commons.model.PNRCollection;
import com.aeromexico.commons.model.WarningCollection;
import com.aeromexico.dao.services.IPNRLookupDao;
import com.am.checkin.web.dao.PNRLookupDaoMock;
import com.am.checkin.web.event.dispatcher.UpdatePNRCollectionAfterCheckinDispatcher;
import com.am.checkin.web.event.dispatcher.UpdatePNRCollectionDispatcher;
import com.am.checkin.web.service.PnrCollectionService;
import com.sabre.services.acs.bso.checkinpassenger.v3.ACSCheckInPassengerRSACS;

/**
 * @author Carlos Luna
 *
 */
public class UpdatePnrCollectionUtilTest {

    private IPNRLookupDao pnrLookupDao;
    private UpdatePNRCollectionDispatcher updatePNRCollectionDispatcher;
    private UpdatePNRCollectionAfterCheckinDispatcher pnrCollectionAfterCheckinDispatcher;
    private PnrCollectionService updatePnrCollectionService;

    @Before
    public void init() {
        pnrLookupDao = new PNRLookupDaoMock();
        updatePNRCollectionDispatcher = new UpdatePNRCollectionDispatcher();
        pnrCollectionAfterCheckinDispatcher = new UpdatePNRCollectionAfterCheckinDispatcher();
        updatePnrCollectionService = new PnrCollectionService();
        updatePnrCollectionService.setPnrLookupDao(pnrLookupDao);
        updatePnrCollectionService.setUpdatePNRCollectionDispatcher(updatePNRCollectionDispatcher);
        updatePnrCollectionService.setUpdatePNRCollectionAfterCheckinDispatcher(pnrCollectionAfterCheckinDispatcher);
    }

    @Test
    public void testUpdateCheckIn() {

        PNRCollection pnrAfterCheckin = null;
        Set<BookedTraveler> passengersSuccessfulCheckedIn = null;
        List<ACSCheckInPassengerRSACS> acsCheckInPassengerRSACSList = null;
        List<BookedTraveler> bookedTravelerList = null;

        try {
            pnrAfterCheckin = pnrLookupDao.readPNRCollectionByRecordLocatorAfterCheckin("HPASVW");
        } catch (Exception e) {
            System.out.println("Cannot get the information of the pnr");
            System.out.println(e.getMessage());
        }

        if (null != pnrAfterCheckin) {

            for (PNR pnr : pnrAfterCheckin.getCollection()) {
                for (CartPNR cartPnr : pnr.getCarts().getCollection()) {
                    passengersSuccessfulCheckedIn = new HashSet<>(cartPnr.getTravelerInfo().getCollection());
                    bookedTravelerList = cartPnr.getTravelerInfo().getCollection();

                    try {
                        BookedLeg bookedLeg = pnr.getBookedLegByLegCode(cartPnr.getLegCode());
                        System.out.println("Trying to save information for after checkin");
                        PnrCollectionUtil.updateAfterSuccessCheckin(
                                passengersSuccessfulCheckedIn,
                                acsCheckInPassengerRSACSList,
                                bookedTravelerList,
                                bookedLeg,
                                "WEB",
                                pnr.getPnr(),
                                pnr.isTsaRequired(),
                                new WarningCollection()
                        );
                    } catch (Exception e) {
                        System.out.println("The information retrieve cannot be udpated");
                        System.out.println(e.getMessage());
                    }

                }
            }

        }
    }

}
