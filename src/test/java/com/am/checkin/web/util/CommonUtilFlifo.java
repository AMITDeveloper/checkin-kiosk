package com.am.checkin.web.util;

import com.sabre.webservices.sabrexml._2011._10.flifo.OTAAirFlifoRS;
import com.sabre.webservices.sabrexml._2011._10.schedule.OTAAirScheduleRS;
import java.io.InputStream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author Franco
 */
public class CommonUtilFlifo {

    public static OTAAirFlifoRS getOtaAirFlifoRS(InputStream in) {
        OTAAirFlifoRS travelItineraryReadRS = null;
        JAXBContext ctx = null;

        try {
            ctx = JAXBContext.newInstance(com.sabre.webservices.sabrexml._2011._10.flifo.ObjectFactory.class.getPackage().getName());
        } catch (JAXBException e) {
            e.printStackTrace();
            return null;
        }

        try {
            Unmarshaller unmarshaller = ctx.createUnmarshaller();
            travelItineraryReadRS = (OTAAirFlifoRS) unmarshaller.unmarshal(in);
        } catch (JAXBException e) {
            throw new RuntimeException("Failed to parse: " + e.getMessage(), e);
        }

        return travelItineraryReadRS;
    }
    
    public static OTAAirScheduleRS getOtaAirScheduleRS(InputStream in) {
        OTAAirScheduleRS travelItineraryReadRS = null;
        JAXBContext ctx = null;

        try {
            ctx = JAXBContext.newInstance(com.sabre.webservices.sabrexml._2011._10.schedule.ObjectFactory.class.getPackage().getName());
        } catch (JAXBException e) {
            e.printStackTrace();
            return null;
        }

        try {
            Unmarshaller unmarshaller = ctx.createUnmarshaller();
            travelItineraryReadRS = (OTAAirScheduleRS) unmarshaller.unmarshal(in);
        } catch (JAXBException e) {
            throw new RuntimeException("Failed to parse: " + e.getMessage(), e);
        }

        return travelItineraryReadRS;
    }

}
