package com.am.checkin.web.util;

import com.aeromexico.sabre.api.acs.AMPassengerDataService;
import com.am.checkin.web.service.AMPassengerDataServiceMock;
import com.sabre.services.checkin.getpassengerdata.v4.GetPassengerDataRSACS;

import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author adrian
 */
public class MarshalTest {

    @Test
    @Ignore
    public void test() throws Exception {
        String recordLocator = "JBWLVY";
        AMPassengerDataService passengerDataServiceMock = new AMPassengerDataServiceMock();

        GetPassengerDataRSACS passengerDataRSACS = passengerDataServiceMock.passengerDataByPNRLocator(recordLocator, false, false);

        String passengerDataRSStr = passengerDataServiceMock.toString(passengerDataRSACS);
        System.out.println(passengerDataRSStr);

        for (int i = 0; i < 500; ++i) {
            Runnable runner = new Runnable() {
                @Override
                public void run() {
                    try {
                        String name = Thread.currentThread().getName();
                        GetPassengerDataRSACS passengerDataRSLocal = passengerDataServiceMock.passengerDataByPNRLocator(recordLocator, false, false, null);
                        passengerDataRSLocal.getPassengerDataResponseList().getPassengerDataResponse().get(0).setFirstName(name);
                        System.out.println(name + ": " + passengerDataServiceMock.toString(passengerDataRSLocal));
                    } catch (Exception ex) {
                        System.out.println(ex.getMessage());
                    }
                }
            };
            new Thread(runner, "TestThread" + i).start();
        }

    }
}
