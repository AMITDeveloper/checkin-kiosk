
package com.am.checkin.web.util;

import com.aeromexico.dao.util.CommonUtil;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class CommonUtilTest {

	@Test
	public void removePrefixTest() {

		String firstName = CommonUtil.removePrefix( "MAHMUD MR" );
		assertTrue(firstName.equals("MAHMUD"));
		System.out.println( firstName );

		firstName = CommonUtil.removePrefix( "MAHMUD MSTR" );
		assertTrue(firstName.equals("MAHMUD"));
		System.out.println( firstName );

		firstName = CommonUtil.removePrefix( "JIN MRS" );
		assertTrue(firstName.equals("JIN"));
		System.out.println( firstName );

		firstName = CommonUtil.removePrefix( "JIN MS" );
		assertTrue(firstName.equals("JIN"));
		System.out.println( firstName );

		firstName = CommonUtil.removePrefix( "LILA MISS" );
		assertTrue(firstName.equals("LILA"));
		System.out.println( firstName );

		firstName = CommonUtil.removePrefix( "LILA MSS" );
		assertTrue(firstName.equals("LILA"));
		System.out.println( firstName );

		firstName = CommonUtil.removePrefix( "ANY NAME" );
		assertTrue(firstName.equals("ANY NAME"));
		System.out.println( firstName );
	}

}
