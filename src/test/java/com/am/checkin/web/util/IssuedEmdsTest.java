package com.am.checkin.web.util;

import com.aeromexico.commons.model.EmdItem;
import com.aeromexico.commons.model.IssuedEmds;
import com.aeromexico.commons.web.types.EmdReceiptType;
import java.math.BigDecimal;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author adrian
 */
public class IssuedEmdsTest {

    @Test
    public void testLoadData() {
        IssuedEmds issuedEmds = new IssuedEmds(3);
        EmdItem emdItem = new EmdItem();
        emdItem.setAmount(BigDecimal.ONE);
        emdItem.setQuantity(1);
        emdItem.setName("Baggage");

        issuedEmds.add("1398212225227", emdItem, EmdReceiptType.EMD_OTHER);

        emdItem = new EmdItem();
        emdItem.setAmount(BigDecimal.ONE);
        emdItem.setQuantity(1);
        emdItem.setName("Baggage");

        issuedEmds.add("1398212225226", emdItem, EmdReceiptType.EMD_OTHER);

        emdItem = new EmdItem();
        emdItem.setAmount(BigDecimal.ONE);
        emdItem.setQuantity(1);
        emdItem.setName("Baggage");

        issuedEmds.add("1398212225225", emdItem, EmdReceiptType.EMD_OTHER);

        emdItem = new EmdItem();
        emdItem.setAmount(BigDecimal.ONE);
        emdItem.setQuantity(1);
        emdItem.setName("Baggage");

        issuedEmds.add("1398212225224", emdItem, EmdReceiptType.EMD_OTHER);

        String emdsStr = issuedEmds.getIssuedEmdsStr(EmdReceiptType.EMD_OTHER);

        System.out.println(emdsStr);

        assertTrue("1398212225227, 1398212225226, 1398212225225|1398212225224".equalsIgnoreCase(emdsStr));

        if (emdsStr.contains("|")) {
            String[] parts = emdsStr.split("\\|");

            assertTrue(parts.length == 2);
            System.out.println(parts[0]);
            System.out.println(parts[1]);
        }

        IssuedEmds issuedEmdsBG = new IssuedEmds(3);
        emdItem = new EmdItem();
        emdItem.setAmount(BigDecimal.ONE);
        emdItem.setQuantity(1);
        emdItem.setName("Baggage");

        issuedEmdsBG.add("1398212225227", emdItem, EmdReceiptType.EMD_EXTRA_BAGGAGE);

        emdItem = new EmdItem();
        emdItem.setAmount(BigDecimal.ONE);
        emdItem.setQuantity(1);
        emdItem.setName("Baggage");

        issuedEmdsBG.add("1398212225226", emdItem, EmdReceiptType.EMD_EXTRA_BAGGAGE);

        emdItem = new EmdItem();
        emdItem.setAmount(BigDecimal.ONE);
        emdItem.setQuantity(1);
        emdItem.setName("Baggage");

        issuedEmdsBG.add("1398212225225", emdItem, EmdReceiptType.EMD_EXTRA_BAGGAGE);

        String emdsStrBG = issuedEmdsBG.getIssuedEmdsStr(EmdReceiptType.EMD_EXTRA_BAGGAGE);

        System.out.println(emdsStrBG);

        assertTrue("1398212225227, 1398212225226, 1398212225225".equalsIgnoreCase(emdsStrBG));

        IssuedEmds issuedEmdsSA = new IssuedEmds(3);
        emdItem = new EmdItem();
        emdItem.setAmount(BigDecimal.ONE);
        emdItem.setQuantity(1);
        emdItem.setName("Baggage");

        issuedEmdsSA.add("1398212225227", emdItem, EmdReceiptType.EMD_SEAT);

        emdItem = new EmdItem();
        emdItem.setAmount(BigDecimal.ONE);
        emdItem.setQuantity(1);
        emdItem.setName("Baggage");

        issuedEmdsSA.add("1398212225226", emdItem, EmdReceiptType.EMD_SEAT);

        String emdsStrSA = issuedEmdsSA.getIssuedEmdsStr(EmdReceiptType.EMD_SEAT);

        System.out.println(emdsStrSA);

        assertTrue("1398212225227, 1398212225226".equalsIgnoreCase(emdsStrSA));
    }

}
