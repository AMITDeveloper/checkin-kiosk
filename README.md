# Check-in kiosk API - maven project

This project is set of JSON WEB services to support an airline check-in through a kiosk. A typical kiosk web application [AM-Kiosk](https://cert-am-kiosk.amlab7.com/).

This project was designed, developed and tested for [wildfly](https://www.wildfly.org/)

## Getting Started

To get you started you can simply clone the https://gitlab.com/AMITDeveloper/checkin-kiosk repository and install the dependencies:

### Prerequisites 1
The project is a Java project. So, it required an Orable JDK to compile and produce the runtime war file.

You need to make sure that Java JDK (NOT JRE) is installed and the properly configured. If you don't have Java JDK you can get git from
[Oracle Java](https://www.oracle.com/java/technologies/javase-downloads.html). You need the latest release of Java version 8

### Prerequisites 2
This is also a Maven project.

You need to make sure that Apache MAVEN is installed and the properly configured. If you don't have Maven you can get git from
[Apache Maven](https://maven.apache.org/). It is recommended to use the latest release Maven.

### Clone checkin-kiosk

Clone the checkin-kiosk repository using [git][git]:

```
git clone https://gitlab.com/AMITDeveloper/checkin-kiosk
cd CheckIn
mvn clean install
```

### Install Dependencies

There are two major dependencies in this project: 
- [AM-SabreAPI](https://gitlab.com/AMITDeveloper/am-sabreapi)
- [AM-CommonsAPI](https://gitlab.com/AMITDeveloper/am-commons-api)


*Note that the `checkin-kiosk` project and the other two application dependencies `AM-SabreAPI` and `AM-CommonsAPI` would all normally be running from the same branch and the POM.xml file for three projects should be the same.*

### Run the Application

....


Now browse to the app root & metadata at `http://localhost:8080/checkIn`.


## Testing

There are two kinds of tests in the check-in kiosk application: Unit tests and End to End tests.

### Running Unit Tests

TODO...
